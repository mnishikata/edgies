//
//  PointToMilTransformer.h
//  sticktotheedge
//
//  Created by __Name__ on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ThemeListToTitleTransformer : NSValueTransformer {

}
+(Class)transformedValueClass;
+(BOOL)allowsReverseTransformation;
-(id)transformedValue:(id)value;

@end
