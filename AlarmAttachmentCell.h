//
//  MNIconizedCell.h
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "AttachmentCellConverter.h"
#import "ButtonAttachmentCell.h"

@protocol AlarmAttachmentCellOwnerTextView;

#define SIMPLE_BEEP_SOUND_PATH @"ALARM_ATTACHMENT_CELL_DEFAULT_BEEP"
#define NONE_SOUND_PATH @"ALARM_ATTACHMENT_CELL_NO_BEEP"


@interface AlarmAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{

	//NSString* name;
	//NSColor* color;
	
	int alarmMode;
	
	NSDate* alarmDate;
	NSTimer* alarmTimer;
	
	NSString* alarmSoundPath;
	BOOL alarmLoop;
}
+(NSAttributedString*)newAlarmWithDate:(NSDate*)date alarmMode:(int)mode alarmSoundPath:(NSString*)path alarmLoop:(BOOL)loop;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
-(NSImage*)image;
- (id)initWithAttachment:(NSTextAttachment*)anAttachment;
- (id)copyWithZone:(NSZone *)zone;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)action:(id)sender;
-(int)alarmMode;
-(void)setAlarmMode:(int)mode;
-(NSDate*)alarmDate;
-(void)setAlarmDate:(NSDate*)newAlarmDate;
- (void) dealloc ;
-(void)fire;
-(BOOL)state;
-(void)setState:(BOOL)newState;
-(void)setButtonSize:(float)size;
-(void)useAsDefault;
-(void)loadDefaults;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <AlarmAttachmentCellOwnerTextView> *)aTextView;
-(NSString*)description;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context;
-(NSDictionary*)pasteboardRepresentation:(id)context;


@end


@protocol AlarmAttachmentCellOwnerTextView 
//-(void)faviconAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)alarmAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)alarmAttachmentCellArrayInSelectedRange;
-(void)openAlarmInspector;
-(void)insertAlarm:(id)sender;

@end

