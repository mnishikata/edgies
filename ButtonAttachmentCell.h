//
//  ButtonAttachmentCell.h
//  sticktotheedge
//
//  Created by __Name__ on 07/05/21.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>




@interface ButtonAttachmentCell : NSButtonCell <NSTextAttachmentCell> {


	id attachment;

	
	
	/// attribute
	
	float	buttonSize;
	int		buttonPosition;
	BOOL	showButtonTitle;
	BOOL	showBezel;

	BOOL	lock;
	/// etc
	
	NSToolTipTag toolTipTag;

	NSTextView* textView ;

	
}



- (id)initWithAttachment:(NSTextAttachment*)anAtachment;
-(void)setObserveKeyPaths;
-(void)loadDefaults;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)untilMouseUp;
-(NSString*)targetPath;
-(BOOL)textView:(NSTextView  *)textView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString;
-(BOOL)canAcceptDrop;
-(void)activateInspectorForTextView:(NSTextView  *)aTextView;
-(void)useAsDefault;
-(NSImage*)buttonImageWithAlpha:(float)alpha;
-(NSData*)imageData;
- (NSSize)cellSize;
-(NSSize)titleSize;
-(NSRect)buttonFrame:(NSRect)cellFrame;
-(NSRect)iconFrame:(NSRect)cellFrame;
-(NSRect)titleFrame:(NSRect)cellFrame;
-(NSDictionary*)titleAttribute;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(unsigned)charIndex layoutManager:(NSLayoutManager *)layoutManager;
- (NSRect)drawTitle:(NSAttributedString*)title withFrame:(NSRect)frame inView:(NSView*)controlView;
- (void)drawImage:(NSImage*)image withFrame:(NSRect)frame inView:(NSView*)controlView;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)aTextView;
-(void)action:(id)sender;
-(BOOL)removeFromTextView;
-(void)setButtonSize:(float)size;



@end


@interface ButtonAttachmentCell (Protocol)

- (NSPoint)cellBaselineOffset;
- (void)setAttachment:(NSTextAttachment *)anAttachment;
- (NSTextAttachment *)attachment;
- (BOOL)wantsToTrackMouse;

@end


@protocol ButtonAttachmentCellOwnerTextView 
-(void)buttonAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
-(void)buttonAttachmentCellClicked:(ButtonAttachmentCell*)cell;
-(void)buttonAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)buttonAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)buttonAttachmentCellArrayInSelectedRange;
-(void)openButtonInspector;
- (BOOL)shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString;


@end

