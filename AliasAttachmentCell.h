//
//  AliasAttachmentCell.h
//  sticktotheedge
//
//  Created by __Name__ on 06/08/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ButtonAttachmentCell.h"
#import "NDAlias.h"
#import "AttachmentCellConverter.h"

@interface AliasAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{


	BOOL empty;
}

+(NSAttributedString*)aliasAttachmentCellAttributedStringWithPath:(NSString*)path;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)setTargetPath:(NSString*)path;
-(NSString*)targetPath;
-(NSAttributedString*)targetLink;
-(void)revealInFinder;
-(void)open;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)textView;
-(BOOL)isEmpty;
-(void)setEmpty:(BOOL)flag;
-(void)useAsDefault;
-(void)loadDefaults;
-(BOOL)substantiateAliasWithNDAlias:(NDAlias*)alias;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context; //withimage ;
-(NSDictionary*)pasteboardRepresentation:(id)context;
-(NSString*)writeToFileAt:(NSURL*)url ;
-(BOOL)canAcceptDrop;


@end

@protocol AliasAttachmentCellOwnerTextView 
//-(void)aliasAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)aliasAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)aliasAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)aliasAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)aliasAttachmentCellArrayInSelectedRange;
-(void)openAliasInspector;


@end



/*
@protocol AttachmentCellConverting 
-(NSString*)stringRepresentation:(id)context;
-(NSAttributedString*)attributedStringWithoutImageRepresentation:(id)context; //without image 
-(NSAttributedString*)attributedStringRepresentation:(id)context; //without image 
-(NSImage*)imageRepresentation:(id)context;


@end
 */