/* GraphicInspector */

#import <Cocoa/Cocoa.h>
#import "GraphicResizableTextView.h"
#import "MNGraphicAttachmentCell.h"


@interface GraphicInspector : NSObject
{
    IBOutlet id panel;
	
	
	NSArray* /*ButtonAttachmentCell**/ activeCells;
}

+(GraphicInspector*)sharedGraphicInspector;
- (id) init ;
-(BOOL)multipleSelection;
-(BOOL)noSelection;
-(void)showGraphicInspector;
-(float)zoomScalePercent;
-(void)setZoomScalePercent:(float)scale;
-(id)angle;
-(void)setAngle:(NSNumber*)size;
-(void)dealloc;
-(void)selectionChanged:(NSNotification*)notification;
-(void)updateInspector;
-(void)activateForTextView:(NSTextView  <GraphicAttachmentCellOwnerTextView> *)view;
-(IBAction)rotate:(id)sender;
-(IBAction)fitToWindow:(id)sender;
-(IBAction)fitToDocumentWidth:(id)sender;

@end
