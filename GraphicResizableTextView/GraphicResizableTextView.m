#import "GraphicResizableTextView.h"
#import "MNGraphicAttachmentCell.h"
#import "GraphicInspector.h"


@implementation GraphicResizableTextView


#pragma mark -



#pragma mark <GraphicAttachmentCellOwnerTextView>

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	
	id something = NULL;
	if( charIndex != NSNotFound )
	{
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:charIndex effectiveRange:NULL];
		
	}
	
	
	
	if(  [ [something attachmentCell] isKindOfClass: [MNGraphicAttachmentCell class]] )
	{
		[self setSelectedRange:NSMakeRange(charIndex,1) ];
		
		[ [something attachmentCell] customizeContextualMenu:aContextMenu for:self];
		
	}
	
	return aContextMenu;
}











-(void)graphicAttachmentCellUpdated:(MNGraphicAttachmentCell*)cell
{
	NSLayoutManager* lm = [self layoutManager];
	NSRange fullRange = NSMakeRange(0,[[self textStorage] length]);
	
	[lm invalidateGlyphsForCharacterRange:fullRange
						   changeInLength:0 
					 actualCharacterRange:nil];
	
	[lm invalidateLayoutForCharacterRange:fullRange isSoft:NO
					 actualCharacterRange:nil];
	
	//[[self layoutManager] invalidateDisplayForCharacterRange:NSMakeRange(0,[[self textStorage] length]) ];
	[self display];
}



-(void)graphicAttachmentCellDragged:(MNGraphicAttachmentCell*)cell atCharacterIndex:(unsigned)index
{
	//NSLog(@"dragged");
	
	NSEvent* theEvent = [[self window] 
				nextEventMatchingMask:NSAnyEventMask
							untilDate:[NSDate dateWithTimeIntervalSinceNow:5.0]
							   inMode:NSEventTrackingRunLoopMode
							  dequeue:YES];
	
	if( [theEvent type] != NSLeftMouseDragged ) return;
	
	
	//----------
	
	
	[self dragSelectionWithEvent:theEvent offset:NSMakeSize(0,0) slideBack:YES];	
	
}

-(NSArray*)graphicAttachmentCellArrayInRange:(NSRange)range
{
	if( range.length == 0 ) return nil;
	
	
	NSMutableArray* cells = [NSMutableArray array] ;
	
	
	unsigned location;
	for( location = range.location; location < NSMaxRange(range); )
	{
		NSRange effectiveRange;
		id attachment =  [[self textStorage] attribute:NSAttachmentAttributeName
											   atIndex:location
										effectiveRange:&effectiveRange] ;
		if( attachment != nil )
		{
			NSCell* cell = [attachment attachmentCell];
			if( [cell isKindOfClass:[MNGraphicAttachmentCell class]] )
				[cells addObject:[attachment attachmentCell]];
		}
		
		location = NSMaxRange(effectiveRange);
	}
	
	return cells;	
}


-(NSArray*)graphicAttachmentCellArrayInSelectedRange
{
	NSRange range = [self selectedRange];
	return [self graphicAttachmentCellArrayInRange:range];
}

-(void)openGraphicInspector
{
	[[GraphicInspector sharedGraphicInspector] activateForTextView:self];
}


@end
