/* GraphicResizableTextView */

#import <Cocoa/Cocoa.h>
#import "ZoomTextView.h"
//#import "NSClipView(extention).h"
#import "MNGraphicAttachmentCell.h"


/*
typedef enum {
	NoKnob = 0,
	CornerKnob,
	RightKnob,
	BottomKnob
		
} DRAGMODE;

*/
@interface GraphicResizableTextView : ZoomTextView <GraphicAttachmentCellOwnerTextView>
{

	
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)graphicAttachmentCellUpdated:(MNGraphicAttachmentCell*)cell;
-(void)graphicAttachmentCellDragged:(MNGraphicAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)graphicAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)graphicAttachmentCellArrayInSelectedRange;
-(void)openGraphicInspector;


@end
