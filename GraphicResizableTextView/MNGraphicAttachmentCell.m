//
//  NSTextAttachmentCell(GraphicExtention).m
//  SampleApp
//
//  Created by __Name__ on 06/05/01.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "MNGraphicAttachmentCell.h"
#import "GraphicInspector.h"
#import "FileLibrary.h"
#import "EdgiesLibrary.h"
#import "NSString (extension).h"


#define MARGIN 0

extern BOOL UD_EXPORT_CONVERTED_IMAGE;
extern BOOL UD_DELETE_GRAPHICS_AFTER_DRAGGING_OUT;

@implementation MNGraphicAttachmentCell

+(MNGraphicAttachmentCell*)graphicAttachmentCellWithImage:(NSImage*)image
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file.tiff"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MNGraphicAttachmentCell* aCell = [[[MNGraphicAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setImage: image];
	
	
	return aCell;
}

+(MNGraphicAttachmentCell*)graphicAttachmentCellWithImage:(NSImage*)image andOriginalFilePath:(NSString*)originalPath
{
	//NSLog(@"creating tiff from original path %@",originalPath);
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file.tiff"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MNGraphicAttachmentCell* aCell = [[[MNGraphicAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setImage: image];
	
	//
	BOOL isDir;
	if( [[NSFileManager defaultManager] fileExistsAtPath:originalPath isDirectory:&isDir] )
	{
		if( !isDir )
		{
			[aCell setOriginalPath:originalPath];
		}
	}
	
	
	return aCell;
}

+(NSAttributedString*)graphicAttachmentCellAttributedStringWithImage:(NSImage*)image
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file.tiff"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MNGraphicAttachmentCell* aCell = [[[MNGraphicAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setAttachment:anAttachment];
	[aCell setImage: image];
	[wrapper setIcon: image];
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}


+(NSAttributedString*)graphicAttachmentCellAttributedStringWithImage:(NSImage*)image  andOriginalFilePath:(NSString*)originalPath
{
	//NSLog(@"creating tiff attr string from original path %@",originalPath);

	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file.tiff"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MNGraphicAttachmentCell* aCell = [[[MNGraphicAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setAttachment:anAttachment];
	[aCell setImage: image];
	[wrapper setIcon: image];
	
	
	//
	BOOL isDir;
	if( [[NSFileManager defaultManager] fileExistsAtPath:originalPath isDirectory:&isDir] )
	{
		if( !isDir )
		{
			[aCell setOriginalPath: originalPath];
			//[aCell setTitle: [originalPath lastPathComponent]];
		}
	}
	
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}



- (id)initWithAttachment:(NSTextAttachment*)attachment
{
    self = [super init];
    if (self) {
		
		angle = 0;
		zoomScale = 1.0;
		originalData = nil;
		originalFilename = nil;
		
		[self setObserveKeyPaths];
	}
    return self;
}

- (void) dealloc {
	[self removeObserveKeyPaths];
	[super dealloc];
}

-(void)setObserveKeyPaths
{
	[self addObserver:self forKeyPath:@"angle" options:NSKeyValueChangeNewKey context:nil];
	[self addObserver:self forKeyPath:@"zoomScale" options:NSKeyValueChangeNewKey context:nil];
	
	
}
-(void)removeObserveKeyPaths
{
	[self removeObserver:self forKeyPath:@"angle" ];
	[self removeObserver:self forKeyPath:@"zoomScale" ];
	
	
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	

	
	if( textView != nil )
	{
		if( [textView conformsToProtocol:@protocol(GraphicAttachmentCellOwnerTextView)] )
			[textView graphicAttachmentCellUpdated:self ];
		
		//redraw here NSTextAttachmentCell
		
		
	}
	
	
}


- (NSPoint)cellBaselineOffset
{
	return NSMakePoint(0,0);
}
- (void)setAttachment:(NSTextAttachment *)anAttachment
{
	attachment = anAttachment; // is not retained
	[anAttachment setAttachmentCell:self];
	
	
	NSFileWrapper* wrapper = [anAttachment fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		
}

- (NSTextAttachment *)attachment
{
	return attachment;
}




- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	// [encoder encodeObject: myButton forKey: @"button"];
	[encoder encodeFloat: zoomScale forKey: @"zoomScale"];
	[encoder encodeFloat: angle forKey: @"angle"];

	if( originalData )
		[encoder encodeObject:originalData forKey:@"originalData"];
	
	if( originalFilename )
		[encoder encodeObject:originalFilename forKey:@"originalFilename"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		
		textView = nil;
		toolTipTag = 0;
		//[self loadDefaults];
		
		if( [coder containsValueForKey:@"zoomScale"] )
			zoomScale	=   [coder decodeFloatForKey: @"zoomScale"];
		else
			zoomScale = [self zoomScale_];
		
		if( [coder containsValueForKey:@"angle"] )
			angle	=   [coder decodeFloatForKey: @"angle"];
		else
			angle = [self angle_];
		
		
		
		if( [coder containsValueForKey:@"originalData"] )
			originalData	=   [[coder decodeObjectForKey: @"originalData"] retain];
		else originalData = nil;
		
		if( [coder containsValueForKey:@"originalFilename"] )
			originalFilename	=   [[coder decodeObjectForKey: @"originalFilename"] retain];
		else originalFilename = nil;
		
		[self setObserveKeyPaths];
		
	}
	return self;
	
	
}



-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <GraphicAttachmentCellOwnerTextView> *)aTextView
{
	int piyo;
	NSMenuItem* customMenuItem;
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"GraphicAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"GraphicAttachmentCellItem"];
		[aContextMenu addItem:customMenuItem ];
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Graphic Inspector",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"GraphicAttachmentCellItem"];
		[customMenuItem setTarget: aTextView];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(openGraphicInspector)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem release];	
		
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Shrink width to window width",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"GraphicAttachmentCellItem"];
		[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
			[customMenuItem setAction: @selector(fitToWindow)];
			
			[aContextMenu addItem:customMenuItem ];
			[customMenuItem release];	
			
}




- (NSSize)cellSize
{
	
	NSSize size = [self originalSize];


	float scale = zoomScale;
	
	size.width = (int) ( (float)size.width * scale ) + MARGIN*2;
	size.height = (int) ( (float)size.height * scale ) + MARGIN*2 ;
	
	if( angle == 90 || angle == 270 )
	{
		int _va = size.width;
		size.width = size.height;
		size.height = _va;
	}
	return size;
}
#pragma mark Actions
-(void)rotate
{
	[self willChangeValueForKey:@"angle"];
	
	angle += 90;
	
	if( angle >= 360 ) angle -= 360;
	
	[self didChangeValueForKey:@"angle"];

}

-(void)fitToWindow
{
	if( textView == nil ) return;
	
	NSSize sizeToFit = [[textView superview] frame].size;
	
	
	//fit horizontaly
	if( [self cellSize].width > sizeToFit.width )
	{
		float scale = sizeToFit.width/[self cellSize].width;
		

		[self willChangeValueForKey:@"zoomScale"];
		zoomScale = scale;
		[self didChangeValueForKey:@"zoomScale"];

		[textView graphicAttachmentCellUpdated:self ];

	}

				
}







- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView
{
	if( [aView conformsToProtocol:@protocol(GraphicAttachmentCellOwnerTextView)] )
		textView = aView;
	
	
	[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];

	NSRect myRect = {0,0,0,0};
	myRect.size = [self originalSize];
	
	[[self image] setFlipped:YES];
	

	
	//
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: cellFrame.origin.x + cellFrame.size.width/2
					 yBy: cellFrame.origin.y + cellFrame.size.height/2 ]; 
	
	// rotate axis
	[affine rotateByDegrees:angle];
	[affine scaleBy: zoomScale ];
	[affine concat];	
	
	NSRect rotatedCellFrame = cellFrame;
	if( angle == 90 || angle == 270 )
	{
		rotatedCellFrame.size.width = cellFrame.size.height;
		rotatedCellFrame.size.height = cellFrame.size.width;
	}
	// draw
	[[self image] drawAtPoint:NSMakePoint( (-rotatedCellFrame.size.width/2+ MARGIN)  / zoomScale , (-rotatedCellFrame.size.height/2 + MARGIN) / zoomScale )
					 fromRect:myRect operation:NSCompositeSourceOver fraction:1.0];

	//[[attr string] drawAtPoint:NSMakePoint( -attrSize.width/2 , -attrSize.height/2) withAttributes:attributes];		
	[context restoreGraphicsState];

	
	///
	if( [aView isKindOfClass:[NSTextView class]] )
	{
	NSTextStorage* ts = [ (NSTextView*)aView textStorage];
	NSRange sr = [ (NSTextView*)aView selectedRange];
	NSAttributedString* attr = [ts attributedSubstringFromRange:sr];
	if( ! [attr containsAttachments] )	return;
	
	unsigned hoge;
	for( hoge = sr.location; hoge  < NSMaxRange(sr); hoge++ )
	{
		id attachment = [ts attribute:NSAttachmentAttributeName atIndex:hoge effectiveRange:NULL];
		
		if( [attachment attachmentCell] == self )
		{
			NSColor* color = [NSColor selectedTextBackgroundColor ];
			
			[[color colorWithAlphaComponent:0.5] set];
			
			[NSBezierPath fillRect:cellFrame];
			
		}
	}		
	}
	
	 
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView characterIndex:(unsigned)charIndex
{
	[self drawWithFrame:cellFrame inView:aView];
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(unsigned)charIndex layoutManager:(NSLayoutManager *)layoutManager
{
	[self drawWithFrame:cellFrame inView:controlView];
}

/////////////

-(NSSize)originalSize
{
	return [[self image] size];
		
}


- (BOOL)wantsToTrackMouse
{
	return YES;
}

- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)untilMouseUp

{
	//NSLog(@"mouseDown");
	
	if( [aTextView conformsToProtocol:@protocol(GraphicAttachmentCellOwnerTextView)] )
		textView = aTextView;
	
	
	
	
	BOOL result = NO;

	NSDate *endDate;
	NSPoint currentPoint = [theEvent locationInWindow];
	BOOL done = NO;
	
	[textView setSelectedRange:NSMakeRange(charIndex,1)];
	
	//NSLog(@"%d",[theEvent clickCount]);
	
	if( [theEvent clickCount] >= 2 )
	{
		//NSLog(@"double clck");
		
		[[GraphicInspector sharedGraphicInspector] activateForTextView:aTextView];
		return NO;
	}
	
	
	BOOL trackContinously = [self startTrackingAt:currentPoint inView:aTextView];
	// catch next mouse-dragged or mouse-up event until timeout
	BOOL mouseIsUp = NO;
	BOOL mouseIsDragged = NO;
	
	NSEvent *event;
	while (!done) { // loop ...
		
		
		
		
		NSPoint lastPoint = currentPoint;
		endDate = [NSDate distantFuture];
		
		event = [NSApp nextEventMatchingMask:(NSLeftMouseUpMask|NSLeftMouseDraggedMask) untilDate:endDate inMode:NSEventTrackingRunLoopMode dequeue:YES];
		
		if (event) { // mouse event
			
			currentPoint = [event locationInWindow];
			if (trackContinously) { // send continueTracking.../stopTracking...
				if (![self continueTracking:lastPoint at:currentPoint inView:aTextView]) {
					done = YES;
					[self stopTracking:lastPoint at:currentPoint inView:aTextView mouseIsUp:mouseIsUp];
				}
				
			}
			
			
			mouseIsUp = ([event type] == NSLeftMouseUp);
			mouseIsDragged = ([event type] == NSLeftMouseDragged );
			
			done = done || mouseIsUp || mouseIsDragged;
			if (untilMouseUp) {
				result = mouseIsUp;
			} else {
				
			}
			
			// check, if the mouse left our cell rect
			result = NSPointInRect([aTextView convertPoint:currentPoint fromView:nil], cellFrame);
			if (!result) {
				done = YES;
				//NSLog(@"mouse is up outside rect");
			}
			
			
			if (done && result && !mouseIsDragged   ) {
				// do nothing
				//[NSApp sendAction:[self action] to:[self target] from:self];
			}
		} 
		
	} // ... while (!done)
	
	if( mouseIsDragged )
	{
		//NSLog(@"mouseIsDragged");
		if( [textView conformsToProtocol:@protocol(GraphicAttachmentCellOwnerTextView)] )
			[textView graphicAttachmentCellDragged:self atCharacterIndex:charIndex];
	}
	
	[aTextView display];
	
	return result;
}

/*
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)flag
{
	////NSLog(@"%d", [theEvent clickCount] );
	
	
	if([theEvent clickCount] > 1 )
	{

		[[[GraphicInspector sharedGraphicInspector] graphicInspectorPanel] makeKeyAndOrderFront:self];
		return NO;
	}
		
	////NSLog(@"track");
	//[self setHighlighted:YES];
	if( [aView isKindOfClass:[NSTextView class]] )
	[(NSTextView*) aView setSelectedRange:NSMakeRange(charIndex,1)];


	return YES;
}*/

#pragma mark legacy
-(float)zoomScale_
{
	float scale = 1.0;
	
	NSString* title = [self title];
	NSArray* array = [title componentsSeparatedByString:@","];
	
	if( [array count] > 0 )
		scale = [[array objectAtIndex:0] floatValue];
	
	if( scale <= 0  ) scale = 1.0;
	
	return scale;
	
}

-(float)angle_
{
	int anAngle = 0;
	
	NSString* title = [self title];
	
	NSArray* array = [title componentsSeparatedByString:@", "];
	if( [array count] > 1 )
		anAngle = [[array objectAtIndex:1] floatValue];
	
	return anAngle;
}
/*
-(void)setScale:(float)scale andAngle:(float)anAngle
{
	NSImage* image = [[[self image] copy] autorelease];
	NSString* title = [NSString stringWithFormat:@"%f, %f",scale,anAngle];
	[self setTitle:title];
	[self setImage:image];
}*/

-(void)setOriginalPath:(NSString*)originalPath
{
	
	[originalData release];
	originalData = [[NSData dataWithContentsOfFile:originalPath] retain];
	[originalFilename release];
	originalFilename = [[originalPath lastPathComponent] retain];
}

#pragma mark @protocol AttachmentCellConverting 

-(int)stringRepresentation:(NSDictionary*)context
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
		
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:@" "];
		
	return 0;
}




-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:@" "];
	
	return 0;
	
}

-(int)attributedStringRepresentation:(NSDictionary*)context //withimage 
{
	
	///NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	//unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	//add icon
	[self setHighlighted:NO];
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		[wrapper setIcon:[self imageRepresentation:nil  ]];
	
		
	return 0;
		
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	NSImage* image = [self imageRepresentation:nil  ];
	if( image == nil )
	{
		//NSLog(@"[self imageRepresentation  ] is nil");
		return nil;
	}
	NSDictionary* val = [NSDictionary dictionaryWithObjectsAndKeys:
		[image TIFFRepresentation], NSTIFFPboardType,
		@"MNGraphicAttachmentCell",  NSFilesPromisePboardType, nil];
	
	
	return val;
}


-(NSImage*)imageRepresentation:(id)context
{
	//NSLog(@"UD_EXPORT_CONVERTED_IMAGE %d",UD_EXPORT_CONVERTED_IMAGE);
	if( !UD_EXPORT_CONVERTED_IMAGE ) {
		NSImage* image = [self image];
		[image setFlipped:YES];
		return image;
	}
	
	NSSize size = [self cellSize];
	NSImage* image  =[[[NSImage alloc] initWithSize:size] autorelease];
	[image setFlipped:YES];
	[image lockFocus];
	
	[self drawWithFrame:NSMakeRect(0,0,size.width,size.height) inView:nil];
	[image unlockFocus];

	return image;
}

-(NSString*)writeToFileAt:(NSURL*)url 
{
	NSString* filename = originalFilename;
	
	
	if( filename == nil || [filename isEqualToString:@""] )
			filename = @"Edgies Picture.tiff";
	filename = [filename uniqueFilenameForFolder:[url path]];
	
	
	
	BOOL success;
	
	
	if( originalData && !UD_EXPORT_CONVERTED_IMAGE )
	{
		[originalData writeToFile:[[url path] stringByAppendingPathComponent: filename] atomically:YES];
	}else
	{
	  dropTIFF( [[self imageRepresentation:nil] TIFFRepresentation]
							   ,[url path]
							 ,filename );
		
	}
	/*
	 if( success ) //rename
	 {
		 NSString* oldname = [[url path] stringByAppendingPathComponent:[self targetPath]];
		 
		 [[NSFileManager defaultManager] movePath:(NSString *)source toPath:(NSString *)destination handler:(id)handler
	}

*/
	
	if( success && UD_DELETE_GRAPHICS_AFTER_DRAGGING_OUT )
		[self removeFromTextView];
	
	return ( success ? filename : nil );
}

-(BOOL)canAcceptDrop
{
	return NO;
}


-(BOOL)removeFromTextView
{
	//NSLog(@"removeFromTextView");
	
	if( textView == nil ) return NO;
	if( ! [textView respondsToSelector:@selector(textStorage) ] ) return NO;
				
	NSTextStorage* textStorage = [[textView textStorage] retain];
	
	unsigned hoge = 0 ;
	TIMEOUT_WHILE(10)
	{
		////NSLog(@"hoge %d , %@",hoge, NSStringFromRange([self fullRange]));
		NSRange range;
		
		NSDictionary* dic = [textStorage attributesAtIndex:hoge effectiveRange:&range ];
		
		id object = [dic objectForKey:NSAttachmentAttributeName ];
		if( [object attachmentCell] == self )
		{
			[textStorage replaceCharactersInRange:NSMakeRange(hoge,1)  withString:@""];
			
			
		}
		else
		{
			hoge = NSMaxRange(range);
		}
		
		
		if( hoge >= [textStorage length] ) 
			break;
	}
	
	[textView graphicAttachmentCellUpdated:self ];
	
	[textStorage release];
	return YES;
}


@end
