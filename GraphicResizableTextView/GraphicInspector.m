#import "GraphicInspector.h"
#import "MemoManager.h"

@implementation GraphicInspector

+(GraphicInspector*)sharedGraphicInspector
{
	return [[NSApp delegate] graphicInspector];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		[NSBundle loadNibNamed:@"GraphicInspector"  owner:self];
		
		//[panel setFloatingPanel:NO];
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(selectionChanged:) 
													 name:NSTextViewDidChangeSelectionNotification object:NULL];
		
		[panel setFloatingPanel:YES];
	}
	return self;
}


-(BOOL)multipleSelection
{
	if( [activeCells count] >1) return YES;
	return NO;
}
-(BOOL)noSelection
{
	if( [activeCells count] == 0 ) return YES;
	return NO;
}
-(void)showGraphicInspector
{
	[panel orderFront:self];	
	[self updateInspector];
}


-(float)zoomScalePercent
{
	if( [activeCells count] != 1) return nil;
	
	
	return 	 [[[activeCells objectAtIndex:0] valueForKey:@"zoomScale"] floatValue] * 100;
	
}
-(void)setZoomScalePercent:(float)scale
{
	
	[activeCells setValue:[NSNumber numberWithFloat: scale/100] forKey:@"zoomScale"];
}
-(id)angle
{
	if( [activeCells count] != 1) return nil;
	
	
	return 	 [[activeCells objectAtIndex:0] valueForKey:@"angle"];
	
}
-(void)setAngle:(NSNumber*)size
{
	
	[activeCells setValue:size forKey:@"angle"];
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	
	[super dealloc];
}


-(void)selectionChanged:(NSNotification*)notification
{
	if( ! [panel isVisible] ) return;
	
	NSTextView* view = [notification object];
	if( view == nil ) return;
	
	if( ! [view conformsToProtocol:@protocol(GraphicAttachmentCellOwnerTextView)] ) return;
	
	
	[self activateForTextView:view];
	
	
	////NSLog(@"selectionChanged %@",[view className]);
	
	
}


-(void)updateInspector
{	  
	//NSLog(@"updateInspector");
	
	if( ! [panel isVisible] ) return;
	
	[self willChangeValueForKey:@"zoomScalePercent"];
	[self didChangeValueForKey:@"zoomScalePercent"];
	
	[self willChangeValueForKey:@"angle"];
	[self didChangeValueForKey:@"angle"];
	
	[self willChangeValueForKey:@"angle"];
	[self didChangeValueForKey:@"angle"];
	
	[self willChangeValueForKey:@"noSelection"];
	[self didChangeValueForKey:@"noSelection"];	
	
	[self willChangeValueForKey:@"multipleSelection"];
	[self didChangeValueForKey:@"multipleSelection"];	
	
}


-(void)activateForTextView:(NSTextView  <GraphicAttachmentCellOwnerTextView> *)view
{
	
	if( [view conformsToProtocol:@protocol(GraphicAttachmentCellOwnerTextView)] )
	{
		//NSRange range = [view selectedRange];
		
		[activeCells release];
		activeCells = [[NSMutableArray arrayWithArray: [view graphicAttachmentCellArrayInSelectedRange]] retain];
		
		
		[self updateInspector];
		
		//[objectController setContent:sender];
		[self showGraphicInspector];
	}else
	{
		[activeCells release];
		activeCells = nil;
	}
}


#pragma mark Actions

-(IBAction)rotate:(id)sender
{
	[activeCells makeObjectsPerformSelector:@selector(rotate)];
	
}

-(IBAction)fitToWindow:(id)sender
{
	[activeCells makeObjectsPerformSelector:@selector(fitToWindow)];
	
}

-(IBAction)fitToDocumentWidth:(id)sender
{
	[activeCells makeObjectsPerformSelector:@selector(fitToDocumentWidth)];
	
}

@end
