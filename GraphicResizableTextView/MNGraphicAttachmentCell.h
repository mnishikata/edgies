//
//  NSTextAttachmentCell(GraphicExtention).h
//  SampleApp
//
//  Created by __Name__ on 06/05/01.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

/*
 
 title „Å´„ÄÅ„Çπ„Ç±„Éº„É´ float as nsstring,  ËßíÂ∫¶„ÄÄint as nsstring 
 „Çí„Ç≥„É≥„Éû„ÅßÂå∫Âàá„Å£„Å¶ÂÖ•„Çå„Çã„ÄÇ„Çπ„Ç±„Éº„É´ÔºùÔºê„ÅØ„Çπ„Ç±„Éº„É´Ôºë„Å®„Åô„Çã
 
 
 */

#import <Cocoa/Cocoa.h>
#import "AttachmentCellConverter.h"


@interface MNGraphicAttachmentCell : NSTextAttachmentCell <AttachmentCellConverting>
{
	id attachment;

	float zoomScale;
	float angle;
	
	NSToolTipTag toolTipTag;

	NSTextView* textView;
	
	NSData* originalData;
	NSString* originalFilename;
	
	
}
+(MNGraphicAttachmentCell*)graphicAttachmentCellWithImage:(NSImage*)image;
+(NSAttributedString*)graphicAttachmentCellAttributedStringWithImage:(NSImage*)image;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
-(void)setObserveKeyPaths;
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
- (NSPoint)cellBaselineOffset;
- (void)setAttachment:(NSTextAttachment *)anAttachment;
- (NSTextAttachment *)attachment;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)textView;
- (NSSize)cellSize;
-(void)rotate;
-(void)fitToWindow;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView characterIndex:(unsigned)charIndex;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(unsigned)charIndex layoutManager:(NSLayoutManager *)layoutManager;
-(NSSize)originalSize;
- (BOOL)wantsToTrackMouse;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)untilMouseUp;
-(float)zoomScale_;
-(float)angle_;
-(void)setScale:(float)scale andAngle:(float)angle;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context; //withimage ;
-(NSDictionary*)pasteboardRepresentation:(id)context;
-(NSImage*)imageRepresentation:(id)context;
-(NSString*)writeToFileAt:(NSURL*)url ;
-(BOOL)canAcceptDrop;
-(BOOL)removeFromTextView;

@end



@protocol GraphicAttachmentCellOwnerTextView 

-(void)graphicAttachmentCellUpdated:(MNGraphicAttachmentCell*)cell;
//-(void)graphicAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)graphicAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)graphicAttachmentCellArrayInSelectedRange;
-(void)openGraphicInspector;


@end
