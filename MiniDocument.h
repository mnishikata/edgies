//
//  MyDocument.h
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//


#import <Cocoa/Cocoa.h>
#import "NSTextView (coordinate extension).h"
#import "MNTabWindow.h"
#import "MiniDocument.h"
#import "ButtonAttachmentTextView.h"
#import "ColorPanelOption.h"
#import "NDAlias.h"

#define SyncRecordIdentifierKey @"SyncRecordIdentifierKey"

@interface MiniDocument : NSObject
{	
	NSString* filePathOfMe;
	NDAlias* fileAliasOfMe;
	
	
	IBOutlet ButtonAttachmentTextView* textView;
	IBOutlet MNTabWindow* window;
	IBOutlet id property;
	IBOutlet id propertyTitleField;

	IBOutlet id discloseButton;
	IBOutlet id closeSheet;

	IBOutlet id objectController;

	
// sync
	NSString * recordIdentifier;
//property
	BOOL propertyDisclosed;
	NSString* p_title;
	
	NSString* p_subject;
	NSString* p_author;
	NSString* p_company;
	NSString* p_copyright;
	NSArray* p_keywords;
	NSString* p_comment;
	NSString* creationDateStr;   //  creation date  == id
	NSColor* documentColor;
	
	//
	BOOL hidden;

	
	//
	BOOL isDocumentEdited;


}
- (id)init;
-(void)showWindow;
-(void)close;
-(void)showSheet;
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo;
-(IBAction)sheetButtonClicked:(id)sender;
-(void)dealloc;
-(void)setDefaultTitle;
-(NSDictionary*)documentProperty;
-(NSString*)string;
-(NSString*)p_title;
-(NSString*)p_subject;
-(NSString*)p_author;
-(NSString*)p_company;
-(NSString*)p_copyright;
-(NSArray*)p_keywords;
-(NSString*)p_comment;
-(NSString*)p_creationDate;
-(NSDate*)creationDate;
-(NSString*)p_color;
-(void)setP_title:(NSString*)newTitle;
-(BOOL)isHidden;
- (void)setValue:(id)value forKey:(NSString *)key;
-(void)setHidden:(BOOL)flag;
-(void)setFilePath:(NSString*)path;
-(NSString*)filePath;
-(NSData*)documentAttributes;
-(void)setDocumentAttributes:(NSData*)data;
-(BOOL)setCreationDateAtPath:(NSString*)path;
-(void)setDocumentColor:(NSColor*)color;
-(NSColor*)documentColor;
-(TAB_POSITION)tabPosition;
-(IBAction)print:(id)sender;
- (void)export;
-(IBAction)export:(id)sender;
-(NSString*)exportInFolder:(NSString*)folderToSave plainText:(BOOL)flag preferredName:(NSString*)name changeFilename:(BOOL)changeFilename;
-(void)documentEdited;
-(NSTextView*)textView;
-(void)save;
-(void)saveIfNeeded;
-(void)redrawWindow;
-(MNTabWindow*)window;
-(NSString*)name;
-(IBAction)discloseClicked:(id)sender;
-(void)showProperty;
- (void)propertyDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo;
-(IBAction)propertyCloseClicked:(id)sender;
-(void)setTitle:(NSString*)newTitle;
-(NSComparisonResult)positionCompare:(MiniDocument*)anotherDoc;
- (NSRect)window:(NSWindow *)window willPositionSheet:(NSWindow *)sheet usingRect:(NSRect)rect;


@end
