//
//  TabCursor.h
//  sticktotheedge
//
//  Created by __Name__ on 06/06/17.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface TabCursor : NSObject {

	NSWindow* bottomWindow;
	NSWindow* leftWindow;
	NSWindow* rightWindow;
	
	NSWindow* topWindow;

	
	NSWindow* timerWindow;
	

	//hot spot
	NSPoint topHotSpot;
	NSPoint topHotSpot_second;

	NSPoint bottomHotSpot;
	NSPoint leftHotSpot;
	NSPoint rightHotSpot;
	
	//timer
	
	NSTimer* followTiemr;

}
- (id) init ;
-(void)showBottomCursor;
-(void)closeBottomCursor;
-(void)showTopCursor;
-(void)closeTopCursor;
-(void)showLeftCursor;
-(void)closeLeftCursor;
-(void)showRightCursor;
-(void)closeRightCursor;

@end
