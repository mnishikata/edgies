/* AttributeSafeTextView */
//

#import <Cocoa/Cocoa.h>
#import "NSTextView (coordinate extension).h"


@interface AttributeSafeTextView : NSTextView
{
	NSMutableArray* impersistentAttributes;

}
- (id)initWithFrame:(NSRect)frame ;
- (void)awakeFromNib;
-(void)dealloc;
-(void)addImpersistentAttribute:(NSString*)attribute;
-(NSData*)portableDateFromRange:(NSRange)range;
-(NSMutableDictionary*)attributesDataFromRange:(NSRange)range;
-(NSMutableAttributedString*)unpackPortable:(NSData*)portableData;
-(IBAction)copyAsPlainText:(id)sender;
- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard types:(NSArray *)types;
-(NSAttributedString*)expandDropAnythingArchive:(NSAttributedString*)org;
-(id)contentsInPaseteboard:(NSPasteboard *)pboard;
- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard;  //Paste;
- (BOOL)resignFirstResponder;
- (void)setContinuousSpellCheckingEnabled:(BOOL)flag;
-(IBAction)deleteColor:(id)sender;

@end

