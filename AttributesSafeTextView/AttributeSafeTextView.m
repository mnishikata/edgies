#import "AttributeSafeTextView.h"
#import "GraphicResizableTextView.h"
#import "ModifierKeyWell.h"
#import "MiniDocument.h"

#import "AppController.h"

#import "MNGraphicAttachmentCell.h"
#import "FileLibrary.h"


#import "SaferTextStorage.h"
#import "KeyModifierManager.h"



#define AVAILABLE_TYPES [NSArray arrayWithObjects: @"DropAnythingPboardType", @"EdgiesTextPboardType", NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, NSTIFFPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType", NSFilenamesPboardType,   nil]



#define DROP_ANYTHING_PBTYPE @"DropAnythingPboardType"
#define DropAnythingAttributeName @"DropAnythingAttributeName"

//#define PREF [[[NSApplication sharedApplication] delegate] preferenceController]


#define AttributeSafeTextViewPboardType @"EdgiesTextPboardType"


//#define alphaLock 0x400
//#define controlKey 0x1000
//#define optionKey 0x800
//#define shiftKey 0x200
//#define cmdKey 0x100


@implementation AttributeSafeTextView


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		
		[self awakeFromNib];
    }
    return self;
}



- (void)awakeFromNib
{
	
	
	//change textstorage to saferTextStorage
	SaferTextStorage* saferTextStorage = [[[SaferTextStorage alloc] init] autorelease];
	NSLayoutManager* lm = [[[NSLayoutManager alloc] init] autorelease];


	
	[[self textContainer] replaceLayoutManager:lm];
	
	
	
	
	impersistentAttributes = [[NSMutableArray alloc] init];
	
	[self addImpersistentAttribute:NSCursorAttributeName];
	[self addImpersistentAttribute:NSToolTipAttributeName];
	[self addImpersistentAttribute:NSLinkAttributeName];
	[self addImpersistentAttribute:DropAnythingAttributeName];


/*
	TemporaryAttributesTextStorage* ts = [[[TemporaryAttributesTextStorage alloc] init] autorelease];
	[ [self  layoutManager] replaceTextStorage:ts];
*/
	/*
	//NSLog(@"awake");

	 if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		 [super awakeFromNib];
	 }
*/
	
	
	/// check user default
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	BOOL flag = [ud boolForKey:@"isContinuousSpellChecking"];
	
	[self  setContinuousSpellCheckingEnabled:flag];
	
	
}
-(void)dealloc
{
	/*
	//remove scratch file if exists
	NSString* path = [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Kojo/GraphicAttachment.tiff"];
	
	[[NSFileManager defaultManager] removeFileAtPath:path handler:NULL];

	 */
	
	[impersistentAttributes release];
	[super dealloc];
}





-(void)addImpersistentAttribute:(NSString*)attribute
{
	if( [impersistentAttributes indexOfObject:attribute] == NSNotFound )
		[impersistentAttributes addObject:attribute];
}


/*
- (void)setTypingAttributes:(NSDictionary *)attributes
{
	
	// deleting impersisntent attirbutes
	
	
	////NSLog(@"setTypingAttributes");
	
	NSMutableDictionary* _attributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
	
	[_attributes removeObjectsForKeys: impersistentAttributes ];
	
	
	
	
	[super setTypingAttributes:[NSDictionary dictionaryWithDictionary:_attributes]];
}
*/



-(IBAction)copyAsPlainText:(id)sender
{
	NSRange selRange = [self selectedRange];
	
	NSPasteboard* pboard = [NSPasteboard pasteboardWithName:NSGeneralPboard];
	
	[pboard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
	
	//NSString* aStr = [[[self textStorage] string] substringWithRange:selRange];
	NSString* aStr = [[[self convertCheckboxesInRange:selRange] string] substringWithRange:selRange];
	
	[pboard setString:aStr forType:NSStringPboardType];  //export string	
	
	
}


- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard types:(NSArray *)types
{
	
	
	
	NSData* aDragginData;
	NSRange selRange = [self selectedRange];
	
	
	////NSLog(@"%d",GetCurrentKeyModifiers( ));
	
	///check key modifier ... when dragging out 
	
	int modKey = GetCurrentKeyModifiers( );
	if(    ( (modKey | 1024) == (0x200|0x800 | 1024)  ) )
	{
		[pboard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
		
		NSString* aStr = [[[self convertCheckboxesInRange:selRange] string] substringWithRange:selRange];
		[pboard setString:aStr forType:NSStringPboardType];  //export string	
		
		return YES;
	}
	
	
	
	[pboard declareTypes:types owner:self];
	
	
	
	
	
	//TIFF or Drop Anything
	NSRange range = [self selectedRange];
	////NSLog(@"%d",range.length);
	if( range.length == 1 )
	{
		////NSLog(@"%@",[[[self textStorage] attributesAtIndex:range.location
		//								   effectiveRange:nil] description]);
		
		
		id something = [[self textStorage] attribute:NSAttachmentAttributeName
											 atIndex:range.location effectiveRange:NULL];
	
		if( [[something attachmentCell] isKindOfClass:[MNGraphicAttachmentCell class]]  )
		{
			
			id furtherSomething = [[self textStorage] attribute:DropAnythingAttributeName
														atIndex:range.location effectiveRange:NULL];
			if(  furtherSomething != nil )
			{
				
				
				////NSLog(@"here");
				
				NSDictionary* pbDic = [NSKeyedUnarchiver unarchiveObjectWithData:furtherSomething];
				
				NSArray* types = [pbDic allKeys];
				[pboard declareTypes:types owner:self];
				////NSLog(@"%@", [[pboard types]description] );
				
				int hoge;
				for( hoge = 0; hoge < [types count]; hoge++ )
				{
					NSString* type = [types objectAtIndex:hoge];
					////NSLog(@"Reading %@",type);
					
					[pboard setData:[pbDic objectForKey:type] forType:type];
				}
				
				
				//NSData* _rtfdData = [self portableDateFromRange:range ];
				//[pboard setData:_rtfdData forType:DROP_ANYTHING_PBTYPE];
				return YES;	
				
			}
			
			/*else // tiff
			{
				////NSLog(@"write tiff");
				
				NSImage* image =  [[something attachmentCell] image];
				
				NSData* tiffData = [image TIFFRepresentation];
				

				[pboard declareTypes:[NSArray arrayWithObject:NSTIFFPboardType ] owner:self];

				[pboard setData:tiffData forType:NSTIFFPboardType];
				return YES;	
			}*/
			
		}
		
		
	}
	
	
	
	// write to paste board
	
	NSAttributedString* attr = [[self textStorage] attributedSubstringFromRange:selRange];
	aDragginData =  [NSKeyedArchiver archivedDataWithRootObject:attr];
		
	[pboard setData:aDragginData forType:AttributeSafeTextViewPboardType];
	
	
	
	
	NSDictionary* docAttr = nil;
	
	
	 if( [[self window] respondsToSelector:@selector(ownerDocument)] )
	 {
		 id ownerDocument = [(MNTabWindow*) [self window] ownerDocument];
		 if( [ownerDocument isKindOfClass:[MiniDocument class]] )				
			 docAttr = [ownerDocument documentProperty];
		 
		 
	 }
	 
	//rtfd  //
	 
	 if( preferenceBoolValueForKey(@"convertCheckbox") )
	 {
		 NSMutableAttributedString* attr = [self convertCheckboxesInRange:selRange];
		 attr = [self ndaliasProofAttributedString:[attr attributedSubstringFromRange:selRange]];
		 
		aDragginData = [attr RTFDFromRange:NSMakeRange(0,[[attr string] length])
						documentAttributes:docAttr ];
	 }
	 else
	 {
		 NSMutableAttributedString* attr = 
		 [self ndaliasProofAttributedString:[[self textStorage]
								attributedSubstringFromRange:selRange]];
		 
		 aDragginData = [attr RTFDFromRange:NSMakeRange(0,[[attr string] length])
						 documentAttributes:docAttr ];
		 
	 }
	 
	[pboard setData:aDragginData forType:NSRTFDPboardType]; 
	
	
	//rtf
	
	NSMutableAttributedString* attr = [self convertCheckboxesInRange:selRange];
	attr = [self ndaliasProofAttributedString:[attr attributedSubstringFromRange:selRange]];
			
	aDragginData = [attr RTFFromRange:NSMakeRange(0,[[attr string] length])
						   documentAttributes:docAttr ];
	
	
	[pboard setData:aDragginData forType:NSRTFPboardType]; 
	
	//String does not work ????****
	NSString* aStr = [[[self convertCheckboxesInRange:selRange] string] substringWithRange:selRange];
	[pboard setString:aStr forType:NSStringPboardType];  //export string
	
	
	
	
	
	
	return YES;	
}


-(NSAttributedString*)expandDropAnythingArchive:(NSAttributedString*)org
{
	if( [org containsAttachments] == NO ) return org;
	
	NSRange range = {0,0};
	NSRange fullRange = NSMakeRange(0, [[org string] length] );
	NSMutableAttributedString* newAttr = [[[NSMutableAttributedString alloc] init] autorelease];
	
	unsigned hoge;
	for( hoge = 0; hoge < [[org string] length];  )
	{
		id something = [org attribute:DropAnythingAttributeName
							  atIndex:hoge
				longestEffectiveRange:&range
							  inRange:fullRange];
		
		
		if( something != nil )
		{
			
		}
		
		else
		{
			[newAttr appendAttributedString:[org attributedSubstringFromRange:range] ];
		}
		
		
	}
	
	return newAttr;
}

-(id)contentsInPaseteboard:(NSPasteboard *)pboard
{
	BOOL returnStatus = NO;
	
	
	if( [pboard availableTypeFromArray:AVAILABLE_TYPES] == DROP_ANYTHING_PBTYPE  ) 
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:DROP_ANYTHING_PBTYPE];
		if(aData == NULL) returnStatus = NO;
		
		else
		{
			 
			NSMutableAttributedString* anAttr = [NSKeyedUnarchiver unarchiveObjectWithData:aData];
			return anAttr;
		}
	}
	
	else if( [pboard availableTypeFromArray:AVAILABLE_TYPES] == AttributeSafeTextViewPboardType  )
	{	
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:AttributeSafeTextViewPboardType];
		if(aData == NULL) returnStatus = NO;
		
		else
		{
			NSMutableAttributedString* anAttr = [NSKeyedUnarchiver unarchiveObjectWithData:aData];
			return anAttr;
		}
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSRTFDPboardType)
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:NSRTFDPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTFD:aData documentAttributes:NULL];
		
		//paste 
		[anAttr autorelease];
		return anAttr;
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSRTFPboardType)
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:NSRTFPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
		
		//paste 
		[anAttr autorelease];
		return anAttr;
		
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSStringPboardType)
	{
		//get array data and text data from pboard 
		NSString* aStr = [pboard stringForType:NSStringPboardType];
		
		//paste 
		return aStr;
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSTIFFPboardType)
	{
		//get array data and text data from pboard 
		NSData* tiffData = [pboard dataForType: NSTIFFPboardType ];
		NSImage* image = [[[NSImage alloc] initWithData:tiffData] autorelease];
		
		
		MNGraphicAttachmentCell* newCell = 
			[[[MNGraphicAttachmentCell alloc] initImageCell: image ] autorelease];

		
		
		NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
		//add files
		
		
		NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
		
		[anAttachment setAttachmentCell:newCell]; //convert


		
		
		
		[wrapper setFilename:@"image.tiff"];
		[wrapper setPreferredFilename:[wrapper filename]];//self rename
			
			[wrapper setIcon:image ];
			
			NSMutableAttributedString* aStr = (NSMutableAttributedString*)[NSMutableAttributedString attributedStringWithAttachment:anAttachment];
			

			
		
		
		//paste 
		return aStr;
		
	}
	
	return NULL;
}

- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard  //Paste
{
	BOOL returnStatus = NO;
	
	id contents = [self contentsInPaseteboard:pboard];
	
	if( contents != NULL )
	{
		
		//-(void)fitGraphicsToWindowWidthInRange:(NSRange)aRange


		if(    [MOD_PRESSED: @"modPasteAsPlainText"]    )
		{
			if( [contents isKindOfClass:[NSAttributedString class]] )
			{
				contents = [contents string];
			}
			
			//NSLog(@"modPasteAsPlainText is pressed");
		}
		
		

		
		
		[self pasteText:contents];
		
		
		if( [self respondsToSelector:@selector(shrinkGraphicsToWindowWidthInRange:)] )
		{
			[self shrinkGraphicsToWindowWidthInRange:NSMakeRange([self selectedRange].location-[contents length], [contents length])];

		}
				
				
		returnStatus = YES;
		
	
	}
	else
		returnStatus = [super readSelectionFromPasteboard:pboard];

	
	return returnStatus;
}
@end



@interface NSTextView (BasicExtension)

@end


@implementation NSTextView (BasicExtension)

- (BOOL)resignFirstResponder
{	
	BOOL flag = YES;//[super resignFirstResponder];
	
	// Inputting Kanji
	if( [self hasMarkedText] )
		flag = NO;
	
		
	return flag;
	
}

- (void)setContinuousSpellCheckingEnabled:(BOOL)flag
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setBool:flag forKey:@"isContinuousSpellChecking"];

	
	[super  setContinuousSpellCheckingEnabled:flag];
}


/////
-(IBAction)deleteColor:(id)sender
{
	NSRange range = [self selectedRange];
	
	if( range.length != 0 )
	{
		[[self textStorage] removeAttribute:NSForegroundColorAttributeName range:range];
		[[self textStorage] removeAttribute:NSBackgroundColorAttributeName range:range];
	
	}else
	{
		NSMutableDictionary* typingAttr = 
		[NSMutableDictionary dictionaryWithDictionary:[self typingAttributes]];
		
		[typingAttr removeObjectForKey:NSForegroundColorAttributeName];
		[typingAttr removeObjectForKey:NSBackgroundColorAttributeName];

		[self setTypingAttributes:typingAttr ];
		
		
	}
	
	
}



@end
