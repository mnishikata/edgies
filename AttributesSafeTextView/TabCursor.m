//
//  TabCursor.m
//  sticktotheedge
//
//  Created by __Name__ on 06/06/17.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "TabCursor.h"
#import "ScreenUtil.h"
#import "EdgiesLibrary.h"
#import "ThemeManager.h"
//#define MENUBAR_HEIGHT 22

@implementation TabCursor
- (id) init {
	self = [super init];
	if (self != nil) {
		
		
		/*
		NSBundle* myBundle;
		myBundle = [NSBundle bundleForClass:[self class]];
		NSString* path;
		
		path = [myBundle pathForResource:@"cursorLeft" ofType:@"tiff"];
		NSImage* _cursorLeft = [[NSImage alloc] initWithContentsOfFile:path ];
		
		path = [myBundle pathForResource:@"cursorRight" ofType:@"tiff"];
		NSImage* _cursorRight = [[NSImage alloc] initWithContentsOfFile:path ];
		
		path = [myBundle pathForResource:@"cursorBottom" ofType:@"tiff"];
		NSImage* _cursorBottom = [[NSImage alloc] initWithContentsOfFile:path ];
		
		
		path = [myBundle pathForResource:@"cursorTop" ofType:@"tiff"];
		NSImage* _cursorTop = [[NSImage alloc] initWithContentsOfFile:path ];
		
		
		
		path = [myBundle pathForResource:@"Doggie" ofType:@"gif"];
		NSImage* _timerImage = [[NSImage alloc] initWithContentsOfFile:path ];

		*/

		
		//
		NSImage* _cursorLeft = [[ThemeManager sharedManager] cursorLeft];
		NSImage* _cursorRight = [[ThemeManager sharedManager] cursorRight];
		NSImage* _cursorBottom = [[ThemeManager sharedManager] cursorBottom];
		NSImage* _cursorTop = [[ThemeManager sharedManager] cursorTop];

		
		// bottom
		
		NSRect frame = NSMakeRect(0,0,100,20);
		bottomWindow = [[NSWindow alloc] initWithContentRect: frame
													styleMask:NSBorderlessWindowMask 
													  backing:NSBackingStoreBuffered defer:NO];
		
		[bottomWindow setLevel:kCGDraggingWindowLevel];
		[bottomWindow setBackgroundColor:[NSColor clearColor]];
		[bottomWindow setOpaque:NO];

		
		[bottomWindow setIgnoresMouseEvents:YES];
		[bottomWindow setHasShadow:YES];
		
		NSImageView* imvb = [[NSImageView alloc] initWithFrame:frame];
		[imvb setAutoresizingMask: NSViewHeightSizable | NSViewWidthSizable];
		[imvb setImage: _cursorBottom];
		[bottomWindow setContentView:imvb];
		
		bottomHotSpot =NSMakePoint( 50, 1 );
		
		// Left
		
		frame = NSMakeRect(0,0,20,100);
		leftWindow = [[NSWindow alloc] initWithContentRect: frame
												   styleMask:NSBorderlessWindowMask 
													 backing:NSBackingStoreBuffered defer:NO];
		
		[leftWindow setLevel:kCGDraggingWindowLevel];
		[leftWindow setBackgroundColor:[NSColor clearColor]];
		[leftWindow setOpaque:NO];
		
		
		[leftWindow setIgnoresMouseEvents:YES];
		[leftWindow setHasShadow:YES];
		
		NSImageView* imvl = [[NSImageView alloc] initWithFrame:frame];
		[imvl setAutoresizingMask: NSViewHeightSizable | NSViewWidthSizable];
		[imvl setImage: _cursorLeft];
		[leftWindow setContentView:imvl];
		
		
		leftHotSpot = NSMakePoint( 0,50 );

		
		// right
		
		frame = NSMakeRect(0,0,20,100);
		rightWindow = [[NSWindow alloc] initWithContentRect: frame
												 styleMask:NSBorderlessWindowMask 
												   backing:NSBackingStoreBuffered defer:NO];
		
		[rightWindow setLevel:kCGDraggingWindowLevel];
		[rightWindow setBackgroundColor:[NSColor clearColor]];
		[rightWindow setOpaque:NO];
		
		
		[rightWindow setIgnoresMouseEvents:YES];
		[rightWindow setHasShadow:YES];
		
		NSImageView* imvr = [[NSImageView alloc] initWithFrame:frame];
		[imvr setAutoresizingMask: NSViewHeightSizable | NSViewWidthSizable];
		[imvr setImage: _cursorRight];
		[rightWindow setContentView:imvr];
		
		
		rightHotSpot = NSMakePoint( 19,50);
		
		// top
		
		frame = NSMakeRect(0,0,100,20);
		topWindow = [[NSWindow alloc] initWithContentRect: frame
												   styleMask:NSBorderlessWindowMask 
													 backing:NSBackingStoreBuffered defer:NO];
		
		[topWindow setLevel:kCGDraggingWindowLevel];
		[topWindow setBackgroundColor:[NSColor clearColor]];
		[topWindow setOpaque:NO];
		
		
		[topWindow setIgnoresMouseEvents:YES];
		[topWindow setHasShadow:YES];
		
		NSImageView* imvt = [[NSImageView alloc] initWithFrame:frame];
		[imvt setAutoresizingMask: NSViewHeightSizable | NSViewWidthSizable];
		[imvt setImage: _cursorTop];
		[topWindow setContentView:imvt];
		
		topHotSpot =NSMakePoint( 50, 20 + MENUBAR_HEIGHT );
		topHotSpot_second =NSMakePoint( 50, 20 );
		
		
		
		
		// timer
		/*
		frame = NSMakeRect(0,0,48,48);
		timerWindow = [[NSWindow alloc] initWithContentRect: frame
												  styleMask:NSBorderlessWindowMask 
													backing:NSBackingStoreBuffered defer:NO];
		
		[timerWindow setLevel:kCGDraggingWindowLevel];
		[timerWindow setBackgroundColor:[NSColor clearColor]];
		[timerWindow setOpaque:NO];
		
		
		[timerWindow setIgnoresMouseEvents:YES];
		[timerWindow setHasShadow:YES];
		
		NSImageView* imvtt = [[NSImageView alloc] initWithFrame:frame];
		[imvtt setAutoresizingMask: NSViewHeightSizable | NSViewWidthSizable];
		[imvtt setImage: _timerImage ];
		[timerWindow setContentView:imvtt];
		
*/
		
	}
	return self;
}

-(void)showBottomCursor
{
	NSPoint ml = [NSEvent mouseLocation];
	
	if( ! mouseIsOnTheBottomEdge() )
	{
		[bottomWindow orderOut:self];
		return;
	}
	
	NSPoint origin = NSMakePoint( ml.x - bottomHotSpot.x ,  ml.y - bottomHotSpot.y  );
	[bottomWindow setFrameOrigin: origin ];
	
	[bottomWindow orderFront:self];
	
	
	if( [followTiemr isValid] ) [followTiemr invalidate];
	[followTiemr release];
	followTiemr = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(showBottomCursor)
												 userInfo:nil repeats:NO] retain];
}

-(void)closeBottomCursor
{
	[bottomWindow orderOut:self];
	[followTiemr invalidate];
	
}

-(void)showTopCursor
{
	NSPoint ml = [NSEvent mouseLocation];
	
	if( ! mouseIsOnTheTopEdge() )
	{
		[topWindow orderOut:self];
		return;
	}
	
	NSPoint myHotSpot = topHotSpot;
	
	if(  screenWhereMouseIs() !=  MENUBAR_SCREEN  ) 
		myHotSpot = topHotSpot_second;
	
	NSPoint origin = NSMakePoint( ml.x - myHotSpot.x ,  ml.y - myHotSpot.y  );
	[topWindow setFrameOrigin: origin ];
	
	[topWindow orderFront:self];
	
	if( [followTiemr isValid] ) [followTiemr invalidate];
	[followTiemr release];
	followTiemr = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(showTopCursor)
												  userInfo:nil repeats:NO] retain];
}

-(void)closeTopCursor
{
	[topWindow orderOut:self];
	[followTiemr invalidate];
	
}



-(void)showLeftCursor
{
	NSPoint ml = [NSEvent mouseLocation];
	
	if( ! mouseIsOnTheLeftEdge() )
	{
		[leftWindow orderOut:self];
		return;
	}

	
	NSPoint origin = NSMakePoint( ml.x - leftHotSpot.x ,  ml.y - leftHotSpot.y  );
	[leftWindow setFrameOrigin: origin ];
	
	[leftWindow orderFront:self];
	
	if( [followTiemr isValid] ) [followTiemr invalidate];

	[followTiemr release];
	followTiemr = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(showLeftCursor)
												  userInfo:nil repeats:NO] retain];
}

-(void)closeLeftCursor
{
	[leftWindow orderOut:self];
	[followTiemr invalidate];
	
}

-(void)showRightCursor
{
	NSPoint ml = [NSEvent mouseLocation];
	
	if( ! mouseIsOnTheRightEdge() )
	{
		[rightWindow orderOut:self];
		return;
	}

	
	NSPoint origin = NSMakePoint( ml.x - rightHotSpot.x ,  ml.y - rightHotSpot.y  );
	[rightWindow setFrameOrigin: origin ];
	
	[rightWindow orderFront:self];

	if( [followTiemr isValid] ) [followTiemr invalidate];

	[followTiemr release];
	followTiemr = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(showRightCursor)
												  userInfo:nil repeats:NO] retain];
}

-(void)closeRightCursor
{
	[rightWindow orderOut:self];
	[followTiemr invalidate];

}



@end
