//
//  NSWindow+SpacesTigerPatch.m
//  sticktotheedge
//
//  Created by __Name__ on 08/05/20.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "NSWindow+SpacesTigerPatch.h"


@implementation NSWindow (SpacesTigerPatch)


- (void)setSpacesBehavior:(unsigned long int)collectionBehavior
{
	if( [self respondsToSelector:@selector(setCollectionBehavior:)] )
	{
		[self setCollectionBehavior:(unsigned long int)collectionBehavior];
	}
	
}


@end
