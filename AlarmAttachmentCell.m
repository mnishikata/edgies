//
//  MNIconizedCell.m
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//


#import "AlarmAttachmentCell.h"
#import "AlarmInspector.h"


#define DEFAULT_ATTRIBUTES  [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11.0], NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, NULL]

#define SOUND_FOLDER_1 @"/Library/Sounds/"
#define SOUND_FOLDER_2 [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Sounds/"]
#define SOUND_FOLDER_3 @"/System/Library/Sounds/"

@implementation AlarmAttachmentCell

static NSImage* validImage = nil;
static NSImage* invalidImage = nil;


+(NSAttributedString*)newAlarmWithDate:(NSDate*)date alarmMode:(int)mode alarmSoundPath:(NSString*)path alarmLoop:(BOOL)loop
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	AlarmAttachmentCell* aCell = [[[AlarmAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	[aCell setAttachment: anAttachment];

	[aCell loadDefaults];
	

	[aCell setAlarmDate:date];
	[aCell setAlarmMode:mode];
	[aCell setAlarmSoundPath:path];
	[aCell setAlarmLoop:loop];

	
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[aCell image  ]];
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	



 - (void)__drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
 
{
	if( validImage == nil ) validImage = [[NSImage imageNamed:@"alarm_valid"] retain];
	if( invalidImage == nil ) invalidImage = [[NSImage imageNamed:@"alarm_invalid"] retain];

	///
	
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: 0
					 yBy: cellFrame.origin.y + cellFrame.size.height ]; 
	
	// rotate axis
	//[affine rotateByDegrees:180];
	[affine scaleXBy:1.0 yBy:-1.0];
	[affine concat];
	
	
	
	
	cellFrame.origin.y = 0;

	
	
	BOOL state = [self state];
	
	if( state ) [validImage drawAtPoint:cellFrame.origin 
							   fromRect:NSMakeRect(0,0,16,16)
							  operation:NSCompositeSourceOver fraction:1.0];
	else [invalidImage drawAtPoint:cellFrame.origin 
						fromRect:NSMakeRect(0,0,16,16)
					   operation:NSCompositeSourceOver fraction:1.0];
		
	
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:[self description] attributes:DEFAULT_ATTRIBUTES] autorelease];


	[attr drawAtPoint: NSMakePoint(cellFrame.origin.x + 16, cellFrame.origin.y)];

	[context restoreGraphicsState];

}
-(NSSize)cellSize
{
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:[self description] attributes:DEFAULT_ATTRIBUTES] autorelease];

	NSSize size = NSZeroSize;
	
	size.width = (int)[attr size].width + 16 + 8;
	size.height = [attr size].height;

	if( size.height < 16 ) size.height = 16;
	
	return size;	
}

-(NSImage*)__image
{

	if( alarmDate == nil ) return nil;
	
	NSRect frame = NSZeroRect;
	frame.size = [self cellSize];
	//NSRect imageRect = [self imageRectForBounds:frame ];


	//frame.size = imageRect.size;
	
	NSImage* img = [[[NSImage alloc] initWithSize:frame.size] autorelease];
	
	[img setFlipped:YES];
	[img lockFocus];
	[self __drawWithFrame:frame inView:nil];
	[img unlockFocus];

	return img;
	
}




- (id)initWithAttachment:(NSTextAttachment*)anAttachment
{

	
    self = [super initWithAttachment:anAttachment];
    if (self) {


		alarmTimer  = nil;
		
		alarmDate = nil;
		alarmMode = 0;
		
		alarmLoop = NO;
		alarmSoundPath = [SIMPLE_BEEP_SOUND_PATH retain];
		
		buttonSize = 16;
		buttonPosition = 0;
		showButtonTitle = NO;
		showBezel = NO;
		
		[self loadDefaults];

		

    }
    return self;
}




- (id)copyWithZone:(NSZone *)zone
{
	//AlarmAttachmentCell* cell = [[AlarmAttachmentCell alloc] initWithAttachment:[self attachment]]
	
	AlarmAttachmentCell* cell = [super copyWithZone:(NSZone *)zone];
	
	[cell setAlarmMode:[self alarmMode]];
	[cell setAlarmDate:[self alarmDate]];

	[cell setAlarmSoundPath:[self alarmSoundPath]];
	[cell setAlarmLoop:[self alarmLoop]];		
	
	return cell;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
    [encoder encodeObject: alarmDate forKey: @"alarmDate"];
    [encoder encodeInt: alarmMode forKey: @"alarmMode"];
    [encoder encodeObject: alarmSoundPath forKey: @"alarmSoundPath"];
    [encoder encodeBool: alarmLoop forKey: @"alarmLoop"];

	
    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		if( [coder containsValueForKey:@"alarmDate"] )
			[self setAlarmDate:   [coder decodeObjectForKey:@"alarmDate"] ];

		if( [coder containsValueForKey:@"alarmMode"] )
			[self setAlarmMode:   [coder decodeIntForKey:@"alarmMode"] ];

		if( [coder containsValueForKey:@"alarmSoundPath"] )
			[self setAlarmMode:   [coder decodeObjectForKey:@"alarmSoundPath"] ];

		if( [coder containsValueForKey:@"alarmLoop"] )
			[self setAlarmLoop:   [coder decodeBoolForKey:@"alarmLoop"] ];

		

		[self setBezelStyle:NSRecessedBezelStyle];
		
	}
	return self;
	
	
}

-(void)action:(id)sender
{
	//NSLog(@"action"); 
	if( [textView conformsToProtocol:@protocol(AlarmAttachmentCellOwnerTextView)] )
	{
		
		[[AlarmInspector sharedAlarmInspector]  modifyAlarm:self];


	}
	
	[super action:sender];
	
}

-(NSString*)alarmSoundPath
{
	return alarmSoundPath;	
}
-(void)setAlarmSoundPath:(NSString*)str
{
	[alarmSoundPath release];
	
	alarmSoundPath = [str retain];
	
	[[AlarmInspector sharedAlarmInspector] updateInspector];
	
}



-(BOOL)alarmLoop
{
	return alarmLoop;	
}
-(void)setAlarmLoop:(BOOL)flag
{
	alarmLoop = flag;
	[[AlarmInspector sharedAlarmInspector] updateInspector];
	
}



-(int)alarmMode
{
	return alarmMode;	
}
-(void)setAlarmMode:(int)mode
{
	alarmMode = mode;
	[[AlarmInspector sharedAlarmInspector] updateInspector];

}

-(NSDate*)alarmDate
{
	return alarmDate;	
}

-(void)setAlarmDate:(NSDate*)newAlarmDate
{
	
	if( [newAlarmDate isKindOfClass:[NSDate class]] )
	{

		[alarmDate release];
		alarmDate = [newAlarmDate retain];
		

		if( alarmTimer != nil )
		{
			[alarmTimer invalidate];
			[alarmTimer release];
			alarmTimer = nil;
		}
		
		
		NSDate* date = [NSDate date];
		if( [alarmDate compare: date] ==  NSOrderedDescending )
		{
			alarmTimer = [NSTimer scheduledTimerWithTimeInterval:[alarmDate timeIntervalSinceDate:date]
														  target:self
														selector:@selector(fire) 
														userInfo:nil repeats:NO];
			
			[alarmTimer retain];
		}
		
		
		[self setImage:[self __image]];
	}
	
	
	[[AlarmInspector sharedAlarmInspector] updateInspector];

	
	
	if( textView != nil )
	{
		if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
			[textView buttonAttachmentCellUpdated:self ];
		
		//redraw here NSTextAttachmentCell
		
		
	}
	
}




-(NSString*)alarmSoundName
{
	// Beep
	if( [alarmSoundPath isEqualToString: SIMPLE_BEEP_SOUND_PATH] )
	{
		return NSLocalizedString(SIMPLE_BEEP_SOUND_PATH,@"");
		
	}else 	if( [alarmSoundPath isEqualToString: NONE_SOUND_PATH] )
	{
		return NSLocalizedString(NONE_SOUND_PATH,@"");
	}
	
	else 
	{
	return [[alarmSoundPath lastPathComponent] stringByDeletingPathExtension];
		
	}
}





-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	[[AlarmInspector sharedAlarmInspector]  modifyAlarm:self];
}


- (void) dealloc {
	
	//NSLog(@"cell dealloc");
	
	if( alarmTimer != nil )
	{
		[alarmTimer invalidate];
		[alarmTimer release];
		alarmTimer = nil;
	}
	
	[alarmSoundPath release];
	
	[super dealloc];
}

-(BOOL)textView:(NSTextView* )aTextView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString
{
	if( alarmTimer != nil )
	{
		[alarmTimer invalidate];
		[alarmTimer release];
		alarmTimer = nil;
	}
	
	return [super textView:aTextView shouldChangeTextInRange:affectedCharRange  replacementString:replacementString];
}


-(void)fire
{
	
	[alarmTimer release];
	alarmTimer = nil;
	
	
	//check if I am really in textView
	NSRange fullRange = NSMakeRange(0, [[textView textStorage] length] );
	NSArray* array = [textView alarmAttachmentCellArrayInRange:fullRange];
	
	if( ![array containsObject: self] ) return;
	
	//search index
	
	unsigned location;
	for( location = 0; location < NSMaxRange(fullRange); )
	{
		NSRange effectiveRange;
		id attachment =  [[textView textStorage] attribute:NSAttachmentAttributeName
											   atIndex:location
										effectiveRange:&effectiveRange] ;
		if( attachment != nil )
		{
			NSCell* cell = [attachment attachmentCell];
			if( cell == self )
				break;
		}
		
		location = NSMaxRange(effectiveRange);
	}
	
	[[textView window] openTab];
	[textView setSelectedRange:NSMakeRange(location,1)];
	[textView scrollRangeToVisible: NSMakeRange(location,1) ];
	
	NSString* message = [NSString stringWithFormat:NSLocalizedString(@"Alarm message for %@",@""), [[textView window] title]];
	

	if( alarmMode == 0 )
	{
		
	}else
	{
		//[NSApp activateIgnoringOtherApps:YES];
		
		// Activate?
		ProcessSerialNumber psn = { 0, kCurrentProcess };
		
		SetFrontProcess(&psn);
		
	}
	
	// Beep
	NSSound *sound = nil;
	
	if( [alarmSoundPath isEqualToString: SIMPLE_BEEP_SOUND_PATH] )
	{
		NSBeep();
	}else 	if( [alarmSoundPath isEqualToString: NONE_SOUND_PATH] )
	{
		
	}else
	{
		sound = [[NSSound alloc] initWithContentsOfFile:alarmSoundPath byReference: NO];
		if( alarmLoop ) [sound setDelegate:self];
		if( sound ) [sound play];

	}
	
	
	
	NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Alarm",@"")  defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:message];
	
	[[alert window] setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	

	
	if( [[textView window] attachedSheet] )
	{
		NSRunAlertPanel(@"Alarm Alert",@"The current sheet needs to be closed to show alarm sheet.",nil,nil,nil);
		[NSApp endSheet:[[textView window] attachedSheet] returnCode:0];
		[[[textView window] attachedSheet] orderOut:self];
	}
	
	
	
	[alert beginSheetModalForWindow: [textView window] modalDelegate:self didEndSelector:@selector(alarmDidEnd:returnCode:contextInfo:) contextInfo:sound];
	

	
	[self setImage:[self __image]];

}
- (void)alarmDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	
	[(NSSound*)contextInfo stop];
	[contextInfo release];
}

- (void)sound:(NSSound *)sound didFinishPlaying:(BOOL)aBool
{
	if( aBool && [[textView window] attachedSheet] )
	{
		[sound play];
	}else
	{
		[sound setDelegate:nil];
	}
}

-(BOOL)state
{
	NSDate* date = [NSDate date];
	if( [alarmDate compare: date] ==  NSOrderedDescending )
		return YES; // = ON
	
	return NO;
	
	
}
-(void)setState:(BOOL)newState
{
// do nothing	
}



-(void)setButtonSize:(float)size
{
	[self willChangeValueForKey:@"buttonSize"];
	buttonSize = size;
	[self didChangeValueForKey:@"buttonSize"];

	[[AlarmInspector sharedAlarmInspector] updateInspector];
	
}

-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	/*
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"AlarmAttachmentCell_buttonSize"];
	[ud setInteger:[self state] forKey:@"AlarmAttachmentCell_state"];

	[ud setObject:[NSArchiver archivedDataWithRootObject: color] forKey:@"AlarmAttachmentCell_color"];

	
	[ud synchronize];
	 */
}


-(void)loadDefaults
{
	
	
	showBezel = YES;
	showButtonTitle = NO;
	
	
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"AlarmAttachmentCell_alarmMode"];
	alarmMode = (value != nil ? [value intValue] : 0); 
	
	value = [ud valueForKey: @"AlarmAttachmentCell_alarmSoundPath"];
	if( value ) alarmSoundPath = [value retain]; 

	value = [ud valueForKey: @"AlarmAttachmentCell_alarmLoop"];
	alarmLoop = (value != nil ? [value boolValue] : NO); 

	
	[self setTitle: @"Alarm"];

	[self setBezelStyle:NSRecessedBezelStyle];
	lock = NO;
	
}

-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <AlarmAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
	
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"AlarmAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"AlarmAttachmentCellItem"];
		[aContextMenu addItem:customMenuItem ];
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Alarm Setting...",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"AlarmAttachmentCellItem"];
		[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
			[customMenuItem setAction: @selector(action:)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem autorelease];	
						
}

+(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <AlarmAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
		BOOL clickedOnCell = NO;
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"AlarmAttachmentCellItemClassMethodMenu"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}else if( [[customMenuItem representedObject] isEqualToString:@"AlarmAttachmentCellItem"]  )
			{
				clickedOnCell= YES;
			}
				
	}
		
		if( clickedOnCell ) return;
		
		
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Insert Alarm...",@"")
												action:nil keyEquivalent:@"A"];
	[customMenuItem setKeyEquivalentModifierMask:1048576];
	[customMenuItem setRepresentedObject:@"AlarmAttachmentCellItemClassMethodMenu"];

	[customMenuItem setTarget: nil];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(insertAlarm:)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
			
}
/*
+(void)insertAlarm:(id)sender
{
	[NSApp tryToPerform:@selector(insertAlarm:) with:self];
	
}
*/
-(NSString*)description
{
	NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init]  autorelease];
	[dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
	
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle ];
	
	
	NSString *formattedDateString = [dateFormatter stringFromDate:alarmDate];
	return formattedDateString;
}


#pragma mark -
#pragma mark @protocol AttachmentCellConverting 
-(int)stringRepresentation:(NSDictionary*)context
{
	return [self attributedStringWithoutImageRepresentation:context];
}
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];

	
	
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:[self description] attributes:DEFAULT_ATTRIBUTES] autorelease];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withAttributedString:attr];
	
	
	
	
	
	return 0;
	
}
-(int)attributedStringRepresentation:(NSDictionary*)context
{
	return [self attributedStringWithoutImageRepresentation:context];

	/*
	if( UD_CHECKBOX_CONVERT_TO_TEXT ) 
		return [self attributedStringWithoutImageRepresentation:context];
	
	
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[self image  ]];
		
	
	return 0;//
	 */
	
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	return nil;
}


@end
