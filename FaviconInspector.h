/* ButtonInspector */

#import <Cocoa/Cocoa.h>
#import "FaviconAttachmentCell.h"

@interface FaviconInspector : NSObject
{
    IBOutlet id window;
	
	
	NSArray* /*ButtonAttachmentCell**/ activeCells;
}
+(FaviconInspector*)sharedFaviconInspector;
- (id) init ;
-(BOOL)multipleSelection;
-(BOOL)noSelection;
-(void)showFaviconInspector;
-(id)imageData;
-(id)title;
-(NSString*)targetPath;
-(void)setTitle:(NSString*)title;
-(id)font;
-(void)setFont:(NSFont*)font;
-(id)buttonSize;
-(void)setButtonSize:(NSNumber*)size;
-(id)buttonPosition;
-(void)setButtonPosition:(NSNumber*)position;
-(id)showButtonTitle;
-(void)setShowButtonTitle:(id)showButtonTitle;
-(id)showBezel;
-(void)setShowBezel:(id)showBezel;
-(void)dealloc;
-(BOOL)lock;
-(void)setLock:(BOOL)flag;
-(void)selectionChanged:(NSNotification*)notification;
-(void)updateInspector;
-(void)activateForTextView:(NSTextView  <FaviconAttachmentCellOwnerTextView> *)view;
-(IBAction)useAsDefault:(id)sender;
-(IBAction)loadFavicon:(id)sender;

@end
