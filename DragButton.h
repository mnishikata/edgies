//
//  DragButton.h
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "EdgiesLibrary.h"


@interface DragButton : NSButton {

	//nib
	IBOutlet id customMenu;
	IBOutlet id titlePanel;

	BOOL openAutomatically;
	
	
	//NSImage* tabImage;
	//NSImage* tabRImage;
	



	
	TAB_POSITION tabPosition;
	NSColor* backgroundColor;
	BOOL mouseOnTab;

	NSTrackingRectTag trackingRect;
	
	NSRect customizeButtonRect;
	NSRect closeButtonRect;

	
	//NSImage* customizeButtonImage;
	//NSImage* closeButtonImage;
	
	
	NSTimer* balloonOnTimer;
	NSTimer* balloonOffTimer;
		NSTimer* windowOpenTimer;
	NSPoint balloonCursor;
	
	BOOL disableCloseButton;
	BOOL disableMenuButton;

	float balloonDelay;
	float openDelay;
	
	
	BOOL dragoutAsPlainText;
	


}
-(void)awakeFromNib;
- (BOOL)isFlipped;
-(void)setPreference;
-(void)setup; /// must be called when ready;
-(void)setupTrackRect:(BOOL)assumeInside;
-(void)endUsing;
-(void)dealloc;
-(void)adjustButtonSizeWhenWindowWasResized:(NSNotification*)notif;
-(void)drawRect:(NSRect)frame;
-(NSImage*)  modifyBalloonImageForDraggingOut:(NSImage*)balloonImage;
- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination;
-(BOOL)canStartTracking:(NSEvent*)theEvent;
-(BOOL)tearOffWindowIfNecessary:(NSPoint)currentMouseLoc fromPoint:(NSPoint)originalPoint;
-(TAB_POSITION)changePosition:(SCREEN_REGION)newRegion;;
-(BOOL)clickedOnTab:(NSEvent*)theEvent;
- (void)mouseDown:(NSEvent *)theEvent;
-(BOOL)mouseOnButton;
- (void)mouseEntered:(NSEvent *)theEvent;
-(void)openTabWithDelay;
-(void)openBalloon;
-(void)closeBalloon;
- (void)mouseExited:(NSEvent *)event;
-(void)setTabPosition:(TAB_POSITION)pos;
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender;
-(void)closeByNotification:(NSNotification*)notification;
- (void)draggingExited:(id <NSDraggingInfo>)sender;
-(void)customizeButtonClicked;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)dragAsPlainText:(NSEvent*)theEvent;
-(void)dragAsRichText:(NSEvent*)theEvent;

@end
