#import <Cocoa/Cocoa.h>


typedef enum {
	tab_bottom = 0,
	tab_top,
	tab_left,
	tab_right
	
} TAB_POSITION;

typedef enum {
	tab_closed = 0x00,
	tab_opened = 0x01,
	tab_temporarilyTornOff = 0x10,
	tab_tornOff = 0x03
	
} TAB_OPEN_STATUS;

typedef enum {
	topArea= 0,
	bottomArea,
	rightArea ,
	leftArea,
	centerArea
	
} SCREEN_REGION;



#define  DEBUG_MODE  YES



#define VERSION_STRING @"120"
// TAB
#define TAB_HEIGHT 20
#define AUTO_TEAROFF_MARGIN 120

//BASIC
#define MENUBAR_HEIGHT 22 /* [[NSApp mainMenu] menuBarHeight] this doesnt work */


#define APP_CONTROLLER [[NSApplication sharedApplication] delegate]
#define BALLOON_CONTROLLER [[[NSApplication sharedApplication] delegate] valueForKey:@"balloon"]


#define TAB_CURSOR (TabCursor*)[[[NSApplication sharedApplication] delegate] valueForKey:@"tabCursor"]

#define APPLICATION_SUPPORT_DIRECTORY 	[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/"]

#define DOCUMENT_FOLDER preferenceValueForKey(@"documentFolderPath") 
#define DEFAULT_DOCUMENT_FOLDER  [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/Documents/"]
#define DOCUMENT_FOLDER_DISABLED  [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/Documents(disabled)/"]

#define THEME_DIRECTORY [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/Themes/"]

//MACRO

#define TIMEOUT_WHILE(n) NSDate* __timeOutDate = [NSDate dateWithTimeIntervalSinceNow: n ];	while ( [__timeOutDate compare:[NSDate date]] == NSOrderedDescending )



// KEY
/*
#define alphaLock 0x400
#define controlKey 0x1000
#define optionKey 0x800
#define shiftKey 0x200
#define cmdKey 0x100
*/

///NOTIFICATION


#define TabWindowDidOpenNotification @"TabWindowDidOpenNotification"
#define TabWindowNumberDidChange @"TabWindowNumberDidChange"

#define TabWindowColorDidChange @"TabWindowColorDidChange"

#define PreferenceDidChangeNotification @"PreferenceDidChangeNotification"

#define TransparencyDidChangeNotification @"TransparencyDidChangeNotification"

#define MemoHiddenStatusDidChangeNotification @"MemoHiddenStatusDidChangeNotification"

#define DropNotificationActiveEdgies @"dropNotification Active Edgies"
#define DropNotificationInactiveEdgies @"dropNotification Inactive Edgies"
#define ItemDroppedOnFolderInEdgiesNotification @"ItemDroppedOnFolderInEdgiesNotification" // path for the dropped item
#define EdgyExportedNotification @"EdgyExportedNotification" // path for the exporte file

#define OutlineChangedNotification @"OutlineChangedNotification"


// CUSTOM VALUES

#define DROP_ANYTHING_PBTYPE @"DropAnythingPboardType"
#define DropAnythingAttributeName @"DropAnythingAttributeName"

#define MNDragOperationDropOnNDAlias NSDragOperationPrivate

// OS version issues ... Panther



#if MAC_OS_X_VERSION_MAX_ALLOWED < 1040

#define NSTitleDocumentAttribute @"NSTitleDocumentAttribute"
#define NSSubjectDocumentAttribute @"NSSubjectDocumentAttribute"
#define NSAuthorDocumentAttribute @"NSAuthorDocumentAttribute"
#define NSCompanyDocumentAttribute @"NSCompanyDocumentAttribute"
#define NSCopyrightDocumentAttribute @"NSCopyrightDocumentAttribute"
#define NSKeywordsDocumentAttribute @"NSKeywordsDocumentAttribute"
#define NSCommentDocumentAttribute @"NSCommentDocumentAttribute"
#define NSCreationTimeDocumentAttribute @"NSCreationTimeDocumentAttribute"
#else

#endif


// file entity

#define SESSION_ID @"FileEntityUtil_SESSION_ID"
#define SETTING_FILE @".FileEntityInfomation"

#define INFORMATION_LOCK @"INFORMATION_LOCK"

#define FILE_FOLDER	[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/Files/"]

#define MY_FOLDER_SUFFIX @".entity"

#define RESCUE_FOLDER	[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/Rescued Items from Edgies/"]


#define UNIQUENAME		[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ] 

#define MY_FOLDER_SUFFIX @".entity"

#define SETTING_FILE	@".FileEntityInfomation"

#define KAGEMUSHA_STRING	@"[Kagemusha] This File Is ALIAS To Original Made By Edgies.  Can Delete Safely."

#define KAGEMUSHA_EXT	@"aliasUsedByEdgies"

#define BUTTON_SIZE		48

#define BUTTONTITLE_ATTRIBUTES	[NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:11],NSFontAttributeName , NULL ]

#define BUTTONTITLE_ALIAS_ATTRIBUTES	[NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:11],NSFontAttributeName , [NSNumber numberWithFloat:0.3], NSObliquenessAttributeName,  NULL ]



#define EdgiesPreferencesChangedNotificationName @"EdgiesPreferencesChangedNotificationName"

#define appID (CFStringRef)@"com.pukeko.edgies"


#define APPLICATION_SUPPORT_DIRECTORY 	[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/"]


#define EDGIES_VERSION 120


void syncronizePreferences(void);
void postPreferencesChangedNotification(void);

id preferenceValueForKey(NSString* key);
float preferenceFloatValueForKey(NSString* key);
int preferenceIntValueForKey(NSString* key);
BOOL preferenceBoolValueForKey(NSString* key);


void setPreferenceValueForKey(id value, NSString* key);


NSString* applicationSupportFolder(void) ;

NSString* avoidNillString(NSString* string);
NSDictionary* loadPlistDictionaryFromFile(NSString* path);
NSString* _extract(NSString* identifier, NSString* oya);
double preferenceDoubleValueForKey(NSString* key);

