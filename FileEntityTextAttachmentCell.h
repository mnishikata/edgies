//
//  FileEntityTextAttachmentCell.h
//  fileEntityTextView
//
//  Created by __Name__ on 06/08/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AliasAttachmentCell.h"

@interface FileEntityTextAttachmentCell : AliasAttachmentCell {


	NSString* fileStorageFolderPath;
	
}


+(NSAttributedString*)fileEntityTextAttachmentCellAttributedStringWithPath:(NSString*)path;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(BOOL)textView:(NSTextView <AliasAttachmentCellOwnerTextView> *)textView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString;
-(void)destroy;
-(BOOL)setTargetPath:(NSString*)path;
-(void)dealloc;

-(NSString*)legacy_myFolderPath;

@end

@protocol FileEntityTextAttachmentCellOwnerTextView 
-(void)rescueFileEntityTextAttachmentCellsInRange:(NSRange)range;
-(void)rescueFileEntityTextAttachmentCells;


@end

