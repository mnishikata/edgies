//
//  PointToMilTransformer.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "ThemeListToTitleTransformer.h"
#import "EdgiesLibrary.h"


@implementation ThemeListToTitleTransformer

+(Class)transformedValueClass{
	return [NSArray class];
}

+(BOOL)allowsReverseTransformation{
	return NO;
}




-(id)transformedValue:(id)value{
	
	NSMutableArray* array = [NSMutableArray array];
	
	unsigned int i, count = [value count];
	for (i = 0; i < count; i++) {
		NSDictionary * dic = [value objectAtIndex:i];
		if( dic != nil )
		{
			id obj = [dic objectForKey: @"name" ];
			if( obj != nil )
					[array addObject: obj ];
		}
	}
	return array; 
}


@end
