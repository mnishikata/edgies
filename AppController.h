/* AppController */

#import <Cocoa/Cocoa.h>


#import "MiniDocument.h"
#import "BalloonController.h"
#import "TabCursor.h"
#import "MNTabWindow.h"

#import "ThemeManager.h"
#import "TabDocumentController.h"
#import "PreferenceController.h"
#import "KeyModifierManager.h"
#import "SoundManager.h"
#import "GraphicInspector.h"
#import "MemoManager.h"
#import "DropWindowManager.h"
#import "SyncManager.h"

#import "TagManager.h"

#import "AliasInspector.h"
#import "FaviconInspector.h"

#import "GraphicInspector.h"
#import "CheckboxInspector.h"
#import "AttachmentCellConverter.h"
#import "ClipboardArchiveInspector.h"
#import "AlarmInspector.h"
#import "TaggingInspector.h"

#import "SyncIt.h"

#import <RegistrationManager/RegistrationManager.h>


BOOL registered;
extern BOOL expired;

@interface AppController : NSObject
{
	RegistrationManager* registrationManager;
	
	//Sync
	SyncManager*			syncManager;
	SyncIt *_syncIt;
	id _recordTransformer;
	id _myDataSource;
	id _entityModel;
	
	
    ThemeManager*			themeManager;
	TabDocumentController*	tabDocumentController;
	PreferenceController*	preferenceController;
	SoundManager*			soundManager;
	KeyModifierManager*		keyModifierManager;
	MemoManager*			memoManager;
	DropWindowManager*		dropWindowManager;
	TagManager*				tagManager;
	
	// inspectors
	AttachmentCellConverter* attachmentCellConverter;
	AliasInspector* aliasInspector;
	FaviconInspector* faviconInspector;
	
	GraphicInspector* graphicInspector;
	CheckboxInspector* checkboxInspector;
	ClipboardArchiveInspector* clipboardArchiveInspector;
	AlarmInspector* alarmInspector;
	TaggingInspector* taggingInspector;
	//Attributes
	
	BOOL			showDockIcon;
	NSTimer*		dialogueTimer;

	
	
	// Objects
	NSMutableArray* documentBank;
	NSMutableArray* documentBank_all;
	NSMutableArray* documentBank_hidden;

	//NSArray*			dropWindows;
	BalloonController*	balloon;
	TabCursor*			tabCursor;
	

	//UI
	IBOutlet NSTextView* syncField;
	IBOutlet id syncIndicator;
	
	IBOutlet id theMenu;
	IBOutlet id launchAtStartupMenuItem;

}
-(void)dummy;
-(void)awakeFromNib;
-(RegistrationManager*)registrationManager;
-(SyncManager*)syncManager;
-(ThemeManager*)themeManager;
-(PreferenceController*)preferenceController;
-(TabDocumentController*)tabDocumentController;
-(SoundManager*)soundManager;
-(DropWindowManager*)dropWindowManager;
-(KeyModifierManager*)keyModifierManager;
-(AliasInspector*)aliasInspector;
-(FaviconInspector*)faviconInspector;
-(AttachmentCellConverter*)attachmentCellConverter;
-(GraphicInspector*) graphicInspector;
-(CheckboxInspector*)checkboxInspector;
-(ClipboardArchiveInspector*)clipboardArchiveInspector;
-(AlarmInspector*)alarmInspector;
-(IBAction)showPreferencePanel:(id)sender;
-(IBAction)showMemoManager:(id)sender;
-(IBAction)showGraphicInspector:(id)sender;
-(IBAction)showAliasButtonInspector:(id)sender;
-(IBAction)showFaviconInspector:(id)sender;
-(IBAction)showCheckboxInspector:(id)sender;
-(IBAction)showClipboardArchiveInspector:(id)sender;
-(IBAction)showAlarmInspector:(id)sender;
-(IBAction)arrangeToFront:(id)sender;
-(IBAction)arrangeToBack:(id)sender;
-(IBAction)bringCenter:(id)sender;
-(IBAction)navigate:(id)sender;
-(IBAction)openAll:(id)sender;
-(IBAction)closeAll:(id)sender;
-(IBAction)newDocument:(id)sender;
-(IBAction)hideMemoManagerPanel:(id)sender;
-(IBAction)colorMenuSelected:(id)sender; //responder chain last;
-(void)setupCheckboxMenu;
-(IBAction)checkForUpdates:(id)sender;
-(IBAction)sync:(id)sender;
-(IBAction)syncModifyCurrentDocument:(id)sender;
-(id)inoPlistValueForKey:(NSString*)key;
-(void)setInfoPlistValue:(id)value forKey:(NSString*)key;
-(IBAction)saveAndQuit:(id)sender;
-(void)resolutionChanged:(id)sender;
-(void)preferenceChanged:(NSNotification*)notif;
- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename;
- (void)menuNeedsUpdate:(NSMenu *)menu;
-(void)docSelectedInDockMenu:(id)sender;
+(int)OSVersion;
- (void)service_makeEdgy:(NSPasteboard *)pboard userData:(NSString *)userData error:(NSString **)error;
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification;
-(IBAction)visitWebsite:(id)sender;
-(BOOL)isLoginItem;

@end
