//
//  MyMovieView.m
//  sticktotheedge
//
//  Created by __Name__ on 08/08/21.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "MyMovieView.h"


@implementation MyMovieView

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	NSMenu *menu = [super menuForEvent:theEvent];
	
	
	if( !menu ) menu = [[NSMenu alloc] initWithTitle:@""];
	
	
	
	NSMenuItem* customMenuItem;
	
	
	/* remove Button Inspector item */
	int piyo;

	for( piyo = [menu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [menu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"MovieAttachmentCellItem"] )
			{
				[menu removeItem:customMenuItem];
			}
				
	}
	
		
		
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"MovieAttachmentCellItem"];
		[menu addItem:customMenuItem ];
		
		
		BOOL loop = [[[self movie] attributeForKey:QTMovieLoopsAttribute] boolValue];
		BOOL playSelection = [[[self movie] attributeForKey:QTMoviePlaysSelectionOnlyAttribute] boolValue];
		
		///
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Play Selection Only",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"MovieAttachmentCellItem"];
		[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
			[customMenuItem setAction: @selector(togglePlaySelection)];
			[customMenuItem setState: (playSelection? NSOnState:NSOffState)];
			
			[menu addItem:customMenuItem ];
			[customMenuItem autorelease];	
			
			///
			customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Play Loop",@"")
														action:nil keyEquivalent:@""];
			[customMenuItem setRepresentedObject:@"MovieAttachmentCellItem"];
			[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
				[customMenuItem setAction: @selector(togglePlayLoop)];
				[customMenuItem setState: (loop? NSOnState:NSOffState)];
				
				[menu addItem:customMenuItem ];
				[customMenuItem autorelease];	
				
				
				///separator
	customMenuItem = [NSMenuItem separatorItem];
	[customMenuItem setRepresentedObject:@"MovieAttachmentCellItem"];
	[menu addItem:customMenuItem ];
	
	

	
	///
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Save Movie As...",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"MovieAttachmentCellItem"];
	[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(saveAs:)];
	
	[menu addItem:customMenuItem ];
	[customMenuItem autorelease];
	
	
	
	///
	
	return menu;
}


- (BOOL)validateMenuItem:(NSMenuItem *)anItem
{
	id obj = [anItem representedObject];
	
	if( [obj isKindOfClass:[NSString class]] && [obj isEqualToString:@"MovieAttachmentCellItem"] )
	{
		return YES;	
	}
	
	return [super validateMenuItem:anItem];
}

-(void)saveAs:(id)sender
{
	[NSApp activateIgnoringOtherApps:YES];
	
	NSSavePanel* panel = [NSSavePanel savePanel];
	int ok = [panel runModal ];
	
	if( ok != NSFileHandlingPanelOKButton ) return;
	
	
	NSString* filename = [panel filename];
	
	
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],QTMovieFlatten,nil ];
	BOOL flag = [[self movie] writeToFile:filename withAttributes:attributes];
	
	if( !flag )
	{
		NSAlert *alert = [NSAlert alertWithMessageText:@"Error" defaultButton:NSLocalizedString(@"OK",@"") alternateButton:nil otherButton:nil informativeTextWithFormat:@"Saving movie failed."];
		
		
		
		
	}
}



-(void)togglePlayLoop
{
	BOOL loop = [[[self movie] attributeForKey:QTMovieLoopsAttribute] boolValue];
	[[self movie] setAttribute:[NSNumber numberWithBool:!loop] forKey:QTMovieLoopsAttribute];
}
-(void) togglePlaySelection
{
	BOOL playSelection = [[[self movie] attributeForKey:QTMoviePlaysSelectionOnlyAttribute] boolValue];
	[[self movie] setAttribute:[NSNumber numberWithBool:!playSelection] forKey:QTMoviePlaysSelectionOnlyAttribute];
	
}


@end
