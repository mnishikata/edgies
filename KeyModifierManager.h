/* KeyModifierManager */

#import <Cocoa/Cocoa.h>

#define MOD_PRESSED [KeyModifierManager sharedKeyModifierManager] keyModifierIsPressed

@interface KeyModifierManager : NSObject
{
	NSMutableDictionary* modifierKeys;

}

+(KeyModifierManager*)sharedKeyModifierManager;
- (id) init ;
-(void)dealloc;
-(void)setupKeys;
-(int)modKeyStrToTag:(NSString*)title;
-(void)clearKeys;
-(BOOL)keyModifierIsPressed:(NSString*)name;

@end
