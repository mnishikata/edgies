//
//  ThemeManager.h
//  sticktotheedge
//
//  Created by __Name__ on 07/01/26.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ThemeManager : NSObject {

	NSDictionary* themeDictionary;
	NSArray* themeList;
}
+(ThemeManager*)sharedManager;
-(NSArray*)themeList;
+(NSArray*)loadThemeList;
+(NSMutableArray*)possibleThemePaths;
+(NSAttributedString*)loadPreviewFromFile:(NSString*)path;
+(NSDictionary*)loadThemeFromFile:(NSString*)path;
+(NSAttributedString*)extract:(NSString*)identifier from:(NSAttributedString*)oya;
+(NSDictionary*)developDictionaryObjects:(NSDictionary*)rawDic;
+(NSArray*)convertColorTable:(NSAttributedString*)connectedTable;
- (id) init ;
-(void)setThemeAtPath:(NSString*)path;
-(NSImage*)cursorTop;
-(NSImage*)cursorBottom;
-(NSImage*)cursorLeft;
-(NSImage*)cursorRight;
-(NSDictionary*)titleAttributes;
-(NSDictionary*)highlightAttributes;
-(float)titleRelativeWidth;
-(NSPoint)titleOffsetForTopTab;
-(NSPoint)titleOffsetForBottomTab;
-(NSPoint)titleOffsetForLeftTab;
-(NSPoint)titleOffsetForRightTab;
-(NSImage*)tabLMask;
-(NSImage*)tabRMask;
-(NSImage*)tabMMask;
-(NSImage*)tabLOverlayForTopTab;
-(NSImage*)tabMOverlayForTopTab;
-(NSImage*)tabROverlayForTopTab;
-(NSImage*)tabLOverlayForBottomTab;
-(NSImage*)tabMOverlayForBottomTab;
-(NSImage*)tabROverlayForBottomTab;
-(NSImage*)tabLOverlayForLeftTab;
-(NSImage*)tabMOverlayForLeftTab;
-(NSImage*)tabROverlayForLeftTab;
-(NSImage*)tabLOverlayForRightTab;
-(NSImage*)tabMOverlayForRightTab;
-(NSImage*)tabROverlayForRightTab;
-(NSImage*)customizeButtonImage;
-(NSImage*)closeButtonImage;
-(NSRect)customizeButtonRectForTopTab;
-(NSRect)closeButtonRectForTopTab;
-(NSRect)customizeButtonRectForBottomTab;
-(NSRect)closeButtonRectForBottomTab;
-(NSRect)customizeButtonRectForLeftTab;
-(NSRect)closeButtonRectForLeftTab;
-(NSRect)customizeButtonRectForRightTab;
-(NSRect)closeButtonRectForRightTab;
-(NSImage*)cornerRT;
-(NSImage*)cornerLT;
-(NSImage*)cornerRB;
-(NSImage*)cornerLB;
-(NSImage*)topSide;
-(NSImage*)bottomSide;
-(NSImage*)leftSide;
-(NSImage*)rightSide;
-(NSImage*)tearOffImage;
-(NSImage*)balloonGlass;
-(NSArray*)colorTable;
-(NSMenu*)colorMenu;
-(NSArray*)colorMenuItems;


@end
