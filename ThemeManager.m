//
//  ThemeManager.m
//  sticktotheedge
//
//  Created by __Name__ on 07/01/26.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ThemeManager.h"
#import "EdgiesLibrary.h"
#import "ColorCheckbox.h"
#import "DynamicColorMenu.h"



#define BEGINLINE @"---------- %@ begin ----------\n"
#define ENDLINE @"\n---------- %@ end ----------"

#define DEFAULT_THEME_FILENAME @"EdgiesDefaultTheme"

#define ATTRIBUTES_COMPONENTS @"titleAttributes, highlightAttributes"

#define COLOR_COMPONENTS @"tintColor"

#define VALUE_COMPONENTS @"titleOffsetForTopTab, titleOffsetForBottomTab, titleOffsetForLeftTab, \
	titleOffsetForRightTab,customizeButtonRectForTopTab, closeButtonRectForTopTab, customizeButtonRectForBottomTab, \
	closeButtonRectForBottomTab, customizeButtonRectForLeftTab, closeButtonRectForLeftTab, \
	customizeButtonRectForRightTab, closeButtonRectForRightTab"

#define NUMBER_COMPONENTS @"titleRelativeWidth" //float value

#define IMAGE_COMPONENTS @"cursorTop, cursorBottom, cursorLeft, cursorRight, tabLMask, tabMMask, \
	tabRMask, customizeButtonImage, closeButtonImage, tabLOverlayForTopTab, tabMOverlayForTopTab, \
	tabROverlayForTopTab, tabLOverlayForBottomTab, tabMOverlayForBottomTab, tabROverlayForBottomTab, \
	tabLOverlayForLeftTab, tabMOverlayForLeftTab, tabROverlayForLeftTab, tabLOverlayForRightTab, \
	tabMOverlayForRightTab, tabROverlayForRightTab, cornerRT, cornerLT, cornerRB, cornerLB, topSide, \
	bottomSide, leftSide, rightSide, tearOffImage, balloonGlass"


/*
read theme file from rtfd file which must be 'developed' to convert suitable object type 
 */

@implementation ThemeManager

+(ThemeManager*)sharedManager
{
	return [[NSApp delegate] themeManager	];
}

-(NSArray*)themeList
{
	return themeList;
}
+(NSArray*)loadThemeList
{
	// array -> dictionary { key: path, name }
	NSMutableArray* array = [NSMutableArray array];
	
	
	NSString* defaultThemeFilePath = 
		[[NSBundle bundleForClass:[self class]] pathForResource:DEFAULT_THEME_FILENAME ofType:@"rtfd"];

	NSDictionary* dict = [ThemeManager loadThemeFromFile:defaultThemeFilePath];
	
	NSDictionary* aDic = [NSDictionary dictionaryWithObjectsAndKeys:[[dict objectForKey:@"name"] string],@"name",defaultThemeFilePath,@"path",nil];
	
	[array addObject: aDic];
	
	//
	
	//
	
	NSMutableArray* possibleThemes = [ThemeManager possibleThemePaths];
	
	int hoge;
	for( hoge = 0; hoge < [possibleThemes count] ; hoge ++ )
	{
		NSString* aPath = [possibleThemes objectAtIndex:hoge];
		NSDictionary* dict = [ThemeManager loadThemeFromFile:aPath];

		if( dict != nil )
		{
			id name = [[dict objectForKey:@"name"] string];
			if( name != nil && ![[[aPath lastPathComponent] stringByDeletingPathExtension] isEqualToString:DEFAULT_THEME_FILENAME])
			{
		NSDictionary* aDic = 
				[NSDictionary dictionaryWithObjectsAndKeys:name,@"name",aPath,@"path",nil];
				[array addObject: aDic];

			}
		}
		
		
	}
	
	//NSLog(@"themeList count %d",[array count]);
	return (NSArray*)array;
}

+(NSMutableArray*)possibleThemePaths
{
	
	
	NSMutableArray* returnArray = [[[NSMutableArray alloc] init] autorelease];
	

	// search resource bundle
	NSArray* paths =  [[NSBundle bundleForClass:[self class]]
						pathsForResourcesOfType:@"rtfd" inDirectory:@""];
	

	int hoge;
	for( hoge = 0 ; hoge < [paths count]; hoge++ )
	{
		NSString* item = [paths objectAtIndex:hoge];
		[returnArray addObject:item ];

	}
	
	
	
	//seach aplication support folder
	NSArray* contents = [[NSFileManager defaultManager] subpathsAtPath:THEME_DIRECTORY];
	
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		NSString* item = [contents objectAtIndex:hoge];
		
		if( [item hasSuffix:@".rtfd"] )
		{
			
			[returnArray addObject:[THEME_DIRECTORY stringByAppendingPathComponent: item] ];
		}
	}
	
	
	////NSLog(@"%@",[returnArray description]);

	return returnArray;
}

+(NSAttributedString*)loadPreviewFromFile:(NSString*)path
{
	NSAttributedString* atts =
	[ [[NSAttributedString alloc] initWithPath:path documentAttributes:NULL] autorelease];
	
	if( atts == nil ) return nil;
	
	//----
	
	id tocAttr = [ThemeManager extract:@"TOC" from:atts ];
	if( tocAttr == nil ) return nil;
	
	
	NSString* toc = [tocAttr string];
	
	NSArray* tocArray = [toc componentsSeparatedByString:@"\n"];
	
	if( [tocArray count] == 0 ) return nil;
	
	// start
	
	unsigned hoge;
	for( hoge = 0; hoge < [tocArray count]; hoge++)
	{
		NSString* key = [tocArray objectAtIndex:hoge];
		
		id obj = [ThemeManager extract:key from:atts ];
		if( obj == nil )
			return nil; //err
		
		
		if( [key isEqualToString:@"preview"] )
		{
			return  obj;
		}
			
		
	}
	
	return nil;	

}

+(NSDictionary*)loadThemeFromFile:(NSString*)path
{
	if( path == nil ) return nil;
	
	NSAttributedString* atts =
	[ [[NSAttributedString alloc] initWithPath:path documentAttributes:NULL] autorelease];
	
	if( atts == nil ) return nil;
	
	//----
	
	id tocAttr = [ThemeManager extract:@"TOC" from:atts ];
	if( tocAttr == nil ) return nil;
	
	
	NSString* toc = [tocAttr string];
	
	NSArray* tocArray = [toc componentsSeparatedByString:@"\n"];
	
	if( [tocArray count] == 0 ) return nil;
	
	// start
	NSMutableDictionary* contents = [[[NSMutableDictionary alloc] init] autorelease];
	
	unsigned hoge;
	for( hoge = 0; hoge < [tocArray count]; hoge++)
	{
		NSString* key = [tocArray objectAtIndex:hoge];
		
		id obj = [ThemeManager extract:key from:atts ];
		if( obj == nil )
			return nil; //err
		
		

		
		[contents setObject:obj forKey:key];
	}

	
	
	return contents;
}


+(NSAttributedString*)extract:(NSString*)identifier from:(NSAttributedString*)oya
{
	
	
	NSString* beginLine = [NSString stringWithFormat:BEGINLINE,identifier];
	NSString* endLine = [NSString stringWithFormat:ENDLINE,identifier];
	
	NSRange beginRange = [[oya string] rangeOfString: beginLine];
	NSRange endRange = [[oya string] rangeOfString: endLine];

	//check validity
	if( beginRange.location == NSNotFound || endRange.location == NSNotFound )
		return nil;
	
	
	//get sub attributed string
	NSRange extractRange = NSMakeRange( NSMaxRange(beginRange), endRange.location - NSMaxRange(beginRange));
	
	return [oya attributedSubstringFromRange: extractRange];
	
	
}

+(NSDictionary*)developDictionaryObjects:(NSDictionary*)rawDic
{
	// titleOffset : range string
	
	// cursorTop,Bottom,Left,Right : NSImage
	
	int hoge;
	NSArray* keys = [rawDic allKeys];
	NSMutableDictionary* returnDic = [NSMutableDictionary dictionary];
	
	for( hoge = 0; hoge < [keys count]; hoge++ )
	{
		NSString* aKey = [keys objectAtIndex:hoge];
		
		if( [ATTRIBUTES_COMPONENTS rangeOfString:aKey].location != NSNotFound ) // range
		{
			
		 	[returnDic setObject:[(NSAttributedString*)[rawDic objectForKey:aKey] attributesAtIndex:0 effectiveRange:nil] 
						  forKey:aKey];
			
			
		}else if( [COLOR_COMPONENTS rangeOfString:aKey].location != NSNotFound ) // range
		{
			
			NSColor* color = [(NSAttributedString*)[rawDic objectForKey:aKey] attribute:NSForegroundColorAttributeName
																				atIndex:0 effectiveRange:nil];
			if( !color ) color = [NSColor blackColor];
		 	[returnDic setObject:color  forKey:aKey];
			
			
		}else if( [VALUE_COMPONENTS rangeOfString:aKey].location != NSNotFound ) // range
		{
		 	[returnDic setObject:[[rawDic objectForKey:aKey] string] forKey:aKey];
		
			
		}else if( [NUMBER_COMPONENTS rangeOfString:aKey].location != NSNotFound ) // range
		{
			NSString* value = [[rawDic objectForKey:aKey] string];
			NSNumber* number = [NSNumber numberWithFloat: [value floatValue] ];
			
		 	[returnDic setObject:number forKey:aKey];
		
			
			
		}else if( [IMAGE_COMPONENTS rangeOfString:aKey].location != NSNotFound )
		{
			NSAttributedString* attr = [rawDic objectForKey:aKey];
			//get first one letter
			NSTextAttachment* attachment = [attr attribute:NSAttachmentAttributeName
												   atIndex:0 effectiveRange:nil];
			
			NSImage* image = [[attachment attachmentCell] image];
			
			if( image== nil ) 
			{
				//NSLog(@"fatal error");//****
			}
			
			[returnDic setObject:image forKey:aKey];

			
		}else // go straight
		{
			id obj = [rawDic objectForKey:aKey];
			
			if( [aKey isEqualToString:@"colorTable"] )
			{
				obj = [ThemeManager convertColorTable:obj];
			}
			
			
			[returnDic setObject: obj forKey:aKey];

		}
		
	}
	
	return (NSDictionary*)returnDic; 
}





+(NSArray*)convertColorTable:(NSAttributedString*)connectedTable
{
	
	
	NSAttributedString* attr = connectedTable;
	

	NSArray* colornameArray = [[attr string] componentsSeparatedByString:@"\n"];
	
	
	NSMutableArray* array = [NSMutableArray array];
	
	int hoge;
	for( hoge = 0; hoge < [colornameArray count]; hoge++ )
	{
		
		NSString* str = [colornameArray objectAtIndex:hoge];
		
		if( ! [str isEqualToString:@""] )
		{
			NSRange range = [[attr string] rangeOfString:str];
			
			NSAttributedString* subAttr = [attr attributedSubstringFromRange:range];
			
			NSColor* color = [subAttr attribute:NSForegroundColorAttributeName
										atIndex:0
								 effectiveRange:nil];
			color = [color colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
			
			NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys: str , @"name", color , @"color",nil ];
			[array addObject:dic];
		}
		
	}
	
	return array;
}
	
	





- (id) init {
	self = [super init];
	if (self != nil) {
		
		themeList = [[ThemeManager loadThemeList] retain];

		
		int defaultIdx = preferenceIntValueForKey( @"selectedThemeListIndex" );
		//NSLog(@"selectedThemeListIndex %d",defaultIdx);

		if( defaultIdx >= [themeList count] ) defaultIdx = 0;
		[self setThemeAtPath:[[themeList objectAtIndex: defaultIdx] objectForKey:@"path"] ];

		
	}
	return self;
}


-(void)setThemeAtPath:(NSString*)path
{
	//NSLog(@"setting theme %@",path);
	
	NSDictionary* dic = 	[ThemeManager  loadThemeFromFile:path];
	
	
	if( dic == nil )
	{
		//NSLog(@"( dic == nil )");
		
		NSString* defaultThemeFilePath = 
		[[NSBundle bundleForClass:[self class]] pathForResource:DEFAULT_THEME_FILENAME ofType:@"rtfd"];

		dic = [ThemeManager  loadThemeFromFile:defaultThemeFilePath];
	}
	
	if( dic != nil )
	{
		[themeDictionary release];
		themeDictionary = [[ThemeManager developDictionaryObjects: dic ] retain];
		
		
		////NSLog(@"set theme %@",[[themeDictionary allKeys] description]);
		
	}
}

///---- accessor
#pragma mark ----------------------

-(NSImage*)cursorTop
{
	return [themeDictionary objectForKey:@"cursorTop"];
	
}

-(NSImage*)cursorBottom
{
	return [themeDictionary objectForKey:@"cursorBottom"];
	
}

-(NSImage*)cursorLeft
{
	return [themeDictionary objectForKey:@"cursorLeft"];
	
}

-(NSImage*)cursorRight
{
	return [themeDictionary objectForKey:@"cursorRight"];
	
}

-(NSDictionary*)titleAttributes
{
	return [themeDictionary objectForKey:@"titleAttributes"];

}
-(NSDictionary*)highlightAttributes
{
	return [themeDictionary objectForKey:@"highlightAttributes"];
	
}

-(float)titleRelativeWidth
{
	return [[themeDictionary objectForKey:@"titleRelativeWidth"] floatValue];
}

-(NSPoint)titleOffsetForTopTab
{
	return NSPointFromString([themeDictionary objectForKey:@"titleOffsetForTopTab"]);
}
-(NSPoint)titleOffsetForBottomTab
{
	return NSPointFromString([themeDictionary objectForKey:@"titleOffsetForBottomTab"]);
}
-(NSPoint)titleOffsetForLeftTab
{
	return NSPointFromString([themeDictionary objectForKey:@"titleOffsetForLeftTab"]);
}
-(NSPoint)titleOffsetForRightTab
{
	return NSPointFromString([themeDictionary objectForKey:@"titleOffsetForRightTab"]);
}

/*
//-(NSSize)tabLSize
{
	return NSSizeFromString([themeDictionary objectForKey:@"tabLSize"]);
}
//-(NSSize)tabRSize
{
	return NSSizeFromString([themeDictionary objectForKey:@"tabRSize"]);
}
*/

-(NSImage*)tabLMask
{
	return [themeDictionary objectForKey:@"tabLMask"];
	
}
-(NSImage*)tabRMask
{
	return [themeDictionary objectForKey:@"tabRMask"];
	
}
-(NSImage*)tabMMask
{
	return [themeDictionary objectForKey:@"tabMMask"];
	
}
-(NSImage*)tabLOverlayForTopTab
{
	return [themeDictionary objectForKey:@"tabLOverlayForTopTab"];
	
}
-(NSImage*)tabMOverlayForTopTab
{
	return [themeDictionary objectForKey:@"tabMOverlayForTopTab"];
	
}
-(NSImage*)tabROverlayForTopTab
{
	return [themeDictionary objectForKey:@"tabROverlayForTopTab"];
	
}
-(NSImage*)tabLOverlayForBottomTab
{
	return [themeDictionary objectForKey:@"tabLOverlayForBottomTab"];
	
}
-(NSImage*)tabMOverlayForBottomTab
{
	return [themeDictionary objectForKey:@"tabMOverlayForBottomTab"];
	
}
-(NSImage*)tabROverlayForBottomTab
{
	return [themeDictionary objectForKey:@"tabROverlayForBottomTab"];
	
}
-(NSImage*)tabLOverlayForLeftTab
{
	return [themeDictionary objectForKey:@"tabLOverlayForLeftTab"];
	
}
-(NSImage*)tabMOverlayForLeftTab
{
	return [themeDictionary objectForKey:@"tabMOverlayForLeftTab"];
	
}
-(NSImage*)tabROverlayForLeftTab
{
	return [themeDictionary objectForKey:@"tabROverlayForLeftTab"];
	
}
-(NSImage*)tabLOverlayForRightTab
{
	return [themeDictionary objectForKey:@"tabLOverlayForRightTab"];
	
}
-(NSImage*)tabMOverlayForRightTab
{
	return [themeDictionary objectForKey:@"tabMOverlayForRightTab"];
	
}
-(NSImage*)tabROverlayForRightTab
{
	return [themeDictionary objectForKey:@"tabROverlayForRightTab"];
	
}





-(NSImage*)customizeButtonImage
{
	return [themeDictionary objectForKey:@"customizeButtonImage"];
	
}
-(NSImage*)closeButtonImage
{
	return [themeDictionary objectForKey:@"closeButtonImage"];
	
}

-(NSRect)customizeButtonRectForTopTab{
	return NSRectFromString([themeDictionary objectForKey:@"customizeButtonRectForTopTab"]);
}
-(NSRect)closeButtonRectForTopTab{
	return NSRectFromString([themeDictionary objectForKey:@"closeButtonRectForTopTab"]);
}
-(NSRect)customizeButtonRectForBottomTab{
	return NSRectFromString([themeDictionary objectForKey:@"customizeButtonRectForBottomTab"]);
}
-(NSRect)closeButtonRectForBottomTab{
	return NSRectFromString([themeDictionary objectForKey:@"closeButtonRectForBottomTab"]);
}
-(NSRect)customizeButtonRectForLeftTab{
	return NSRectFromString([themeDictionary objectForKey:@"customizeButtonRectForLeftTab"]);
}
-(NSRect)closeButtonRectForLeftTab{
	return NSRectFromString([themeDictionary objectForKey:@"closeButtonRectForLeftTab"]);
}
-(NSRect)customizeButtonRectForRightTab{
	return NSRectFromString([themeDictionary objectForKey:@"customizeButtonRectForRightTab"]);
}
-(NSRect)closeButtonRectForRightTab{
	return NSRectFromString([themeDictionary objectForKey:@"closeButtonRectForRightTab"]);
}


-(NSColor*)tintColor
{
	return [themeDictionary objectForKey:@"tintColor"];

}

-(NSImage*)cornerRT{
	return [themeDictionary objectForKey:@"cornerRT"];
}
-(NSImage*)cornerLT{
	return [themeDictionary objectForKey:@"cornerLT"];
}
-(NSImage*)cornerRB{
	return [themeDictionary objectForKey:@"cornerRB"];
}
-(NSImage*)cornerLB{
	return [themeDictionary objectForKey:@"cornerLB"];
}


-(NSImage*)topSide{
	return [themeDictionary objectForKey:@"topSide"];
}

-(NSImage*)bottomSide{
	return [themeDictionary objectForKey:@"bottomSide"];
}

-(NSImage*)leftSide{
	return [themeDictionary objectForKey:@"leftSide"];
}

-(NSImage*)rightSide{
	return [themeDictionary objectForKey:@"rightSide"];
}


-(NSImage*)tearOffImage{
	return [themeDictionary objectForKey:@"tearOffImage"];
}

-(NSImage*)balloonGlass{
	return [themeDictionary objectForKey:@"balloonGlass"];
}


-(NSArray*)colorTable
{
	return [themeDictionary objectForKey:@"colorTable"];

}

-(NSMenu*)colorMenu
{
	//NSLog(@"colorMenu theme ");
	
	NSArray* colorTable = [self colorTable];
	
	NSMenu* colorMenu = [[[NSMenu alloc] init  ] autorelease];
	
	int hoge;
	for( hoge = 0; hoge < [colorTable count]; hoge++ )
	{
		NSDictionary* dic = [colorTable objectAtIndex:hoge];
		NSMenuItem* mi = [[NSMenuItem alloc] initWithTitle: NSLocalizedString([dic objectForKey:@"name"] ,@"")
													action:nil keyEquivalent:@""];
		[mi setTag:1000 + hoge];
		
		NSColor* color = [dic objectForKey:@"color"];
		NSColor* frameColor = [[NSColor darkGrayColor] colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
		[mi setImage:[ColorCheckbox roundedBox:color
										  size:NSMakeSize(12,12)
										 curve:4 
									frameColor:frameColor]];// return roounded rectangle image
		[mi setRepresentedObject:color];

			
		[colorMenu addItem: mi];
			
			
	}
	
	NSMenuItem* mi = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Custom..." ,@"")
												action:nil keyEquivalent:@""]; 
	
	[mi setTag:1999 ];
	
	NSColor* color = [NSColor clearColor] ;
	NSColor* frameColor = [NSColor clearColor];
	[mi setImage:[ColorCheckbox roundedBox:color
									  size:NSMakeSize(12,12)
									 curve:4 
								frameColor:frameColor]];// return roounded rectangle image
		
		
	[colorMenu addItem: mi];
	
	
	return colorMenu;	
}



-(NSArray*)colorMenuItems
{
	//NSLog(@"colorMenu theme ");
	
	NSArray* colorTable = [self colorTable];
	
	NSMutableArray* colorMenuItems = [[[NSMutableArray alloc] init  ] autorelease];
	
	int hoge;
	for( hoge = 0; hoge < [colorTable count]; hoge++ )
	{
		NSDictionary* dic = [colorTable objectAtIndex:hoge];
		NSMenuItem* mi = [[NSMenuItem alloc] initWithTitle: NSLocalizedString([dic objectForKey:@"name"] ,@"")
													action:nil keyEquivalent:@""];
		[mi setTag:1000 + hoge];
		
		NSColor* color = [dic objectForKey:@"color"];
		NSColor* frameColor = [[NSColor darkGrayColor] colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
		[mi setImage:[ColorCheckbox roundedBox:color
										  size:NSMakeSize(12,12)
										 curve:4 
									frameColor:frameColor]];// return roounded rectangle image
		[mi setRepresentedObject:color];
			
		[colorMenuItems addObject: mi];
			
			
	}
	
	NSMenuItem* mi = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Custom..." ,@"")
												action:nil keyEquivalent:@""]; 
	
	[mi setTag:1999 ];
	
	NSColor* color = [NSColor clearColor] ;
	NSColor* frameColor = [NSColor clearColor];
	[mi setImage:[ColorCheckbox roundedBox:color
									  size:NSMakeSize(12,12)
									 curve:4 
								frameColor:frameColor]];// return roounded rectangle image
		
		
		[colorMenuItems addObject: mi];
		
		
		return colorMenuItems;	
}



@end
