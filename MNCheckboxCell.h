//
//  MNIconizedCell.h
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MNCheckboxCell : NSTextAttachmentCell {

	
	

}
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
-(NSImage*)image;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
- (NSSize)cellSize;
-(void)dealloc;


-(void)setColor:(NSColor*)newColor andName:(NSString*)name;
-(NSColor*)color;
-(NSString*)name;
- (BOOL)wantsToTrackMouse;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView untilMouseUp:(BOOL)flag;

@end
