//
//  TextView (coordinate extension).h
//  LinkTextViewSampleProject
//
//  Created by __Name__ on 12/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>



@interface NSString (extension)


// from NSString_ODCompareNumerically
//http://www.cocoabuilder.com/archive/message/cocoa/2002/1/16/61373
- (NSComparisonResult)compareNumerically:(NSString *)aString;
-(NSRange)fullRange;
-(unsigned)reverseLocation:(unsigned)locationFromLast;
-(NSString*)omitSpacesAtBothEnds;
-(NSString*)uniqueFilenameForFolder:(NSString*)folderPath;
-(NSString*)safeFilename;
-(NSString*)uniquePathForFolder ;// change path to unique path;
-(NSString*)folderPath;
-(NSString*)firstWords:(int)count;



@end
