//
//  TextView (coordinate extension).h
//  LinkTextViewSampleProject
//
//  Created by __Name__ on 12/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSTextView (coordinate_extension)

-(unsigned)charIndexAtPoint:(NSPoint)aPoint; // locate closest index;
-(unsigned)charIndexOn:(NSPoint)aPoint; // locate index ON THE MOUSE;
-(unsigned)glyphIndexForCharIndex:(unsigned)charIndex;
-(NSRect)rectForGlyphRange:(NSRange)glyphRange;
-(NSRect)rectForCharRange:(NSRange)charRange;
-(NSPoint)screenPointForCharIndex:(unsigned)charIndex;
-(NSRect)screenRectForCharRange:(NSRange)charRange;
-(NSRect)frameInScreen; // screen rect for MNTextView.  should be enclosed in NSScrollView;
-(void)pasteText:(id)aText;
-(NSRange)fullRange;
- (void)changeColor:(id)sender;

@end
