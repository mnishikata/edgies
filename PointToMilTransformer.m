//
//  PointToMilTransformer.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "PointToMilTransformer.h"
#import "EdgiesLibrary.h"

#import "PreferenceController.h"

@implementation PointToMilTransformer

+(Class)transformedValueClass{
	return [NSNumber class];
}

+(BOOL)allowsReverseTransformation{
	return YES;
}




-(id)transformedValue:(id)value{
	
	float points = [value floatValue];
	int tag =   [[PreferenceController standardUserDefaults] metricsTag] ;
	
	if( tag == 0  ) // mm
		return [NSNumber numberWithFloat:  points / 2.83 ];
	
	if( tag == 1 ) // inch
		return [NSNumber numberWithFloat:points / 72.0];

	
	return [NSNumber numberWithFloat: points ]; //point
}

-(id)reverseTransformedValue:(id)value{
	
	float mil = [value floatValue];

	int tag = preferenceIntValueForKey(@"metricsTag") ;

	
	if( tag == 0  ) // mm
		return [NSNumber numberWithFloat: mil * 2.83 ];
	
	if( tag == 1 ) // inch
		return  [NSNumber numberWithFloat: mil * 72.0 ];
	
	return [NSNumber numberWithFloat: mil ]; // point
}


@end
