#import "KeyModifierManager.h"
#import <HIToolbox/CarbonEventsCore.h>
#import "EdgiesLibrary.h"

#define KEY_LIST [NSArray arrayWithObjects:@"modDiscard", @"modDragContent", @"modDragContentAsPlainText", \
	@"modPasteAsPlainText", @"modRename", @"modTearOff", @"modShowMenu", @"modExport", @"modButton", \
	@"modLink", @"modDropAnything", @"edgeActivatingKeyModifier",nil]

@implementation KeyModifierManager


+(KeyModifierManager*)sharedKeyModifierManager
{
	return [[NSApp delegate] keyModifierManager	];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(setupKeys)
													 name:PreferenceDidChangeNotification
												   object:nil];
		
		[self setupKeys];	
	}
	return self;
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
 	[super dealloc];
}
-(void)setupKeys
{
	[self clearKeys];
	
	
	unsigned int i, count = [KEY_LIST count];
	for (i = 0; i < count; i++) {
		NSString * keyname = [KEY_LIST objectAtIndex:i];
		id keynumberStr = preferenceValueForKey(keyname);
		
		if( keynumberStr != nil )
			[modifierKeys setObject:[NSNumber numberWithInt:[self modKeyStrToTag:keynumberStr]] forKey:keyname];
	}
	
}

-(int)modKeyStrToTag:(NSString*)title
{
	

	//以前のファイルはタグで保存されていたため
	if( [title isKindOfClass:[NSNumber class]] )
	{	
		return [title intValue];
	}
	
		
	int cmd	=  0x100 ;	//kCommandUnicode
	int shift	= 0x200 ;	//kShiftUnicode
	int caps	= 0x400 ;	
	int option	= 0x800 ;	//kOptionUnicode
	int ctrl	= 0x1000;	//kControlUnicode
	int fn		= 0x20000;
	
	unichar kCommandUnicode = 0x2318;
	unichar kShiftUnicode	= 0x21E7;
	unichar kOptionUnicode	= 0x2325;
	unichar kControlUnicode	= 0x2303;
	
	NSString* fnStr = @"fn";
	NSString* ctrlStr = [NSString stringWithCharacters:&kControlUnicode  length:1];
	NSString* optionStr = [NSString stringWithCharacters:&kOptionUnicode  length:1];
	NSString* shiftStr = [NSString stringWithCharacters:&kShiftUnicode  length:1];
	NSString* cmdStr = [NSString stringWithCharacters:&kCommandUnicode  length:1];
	
	
	int tag = 0;
	if( [title rangeOfString: fnStr].length != 0 )
		tag |= fn;
	
	if( [title rangeOfString: ctrlStr].length != 0 )
		tag |= ctrl;
	
	if( [title rangeOfString: optionStr].length != 0 )
		tag |= option;
	
	if( [title rangeOfString: shiftStr].length != 0 )
		tag |= shift;
	
	if( [title rangeOfString: cmdStr].length != 0 )
		tag |= cmd;
	
	return tag;
}
-(void)clearKeys
{
	[modifierKeys release];
	modifierKeys = [NSMutableDictionary dictionary];
	[modifierKeys retain];
}
-(BOOL)keyModifierIsPressed:(NSString*)name
{
	//NSLog(@"keyModifierIsPressed %@",name);
	
	id keyvalue = [modifierKeys objectForKey:name];
	//string
	
	
	

	
	if( keyvalue == nil ) return NO;
	
	int keynumber = [keyvalue intValue];
	
	if( keynumber == 0 ) return NO;
	
	int modKeys = GetCurrentKeyModifiers( ) ;
	modKeys = modKeys & 0x21B00 ; //masking
	
	
	return ( keynumber == modKeys ? YES : NO );
}

@end
