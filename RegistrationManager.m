#import "RegistrationManager.h"
#import "EWSLib.h"
#import "validate.h"

#define APP_SERIAL @"OneRiverEdgiesSerialNumber"
#define APP_USERNAME @"OneRiverEdgiesRegisterdUsername"

@implementation RegistrationManager


- (void)awakeFromNib
{

	[okButton setEnabled:NO];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(fieldInputted:)
												 name:NSControlTextDidChangeNotification
											   object:sn];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(fieldInputted:)
												 name:NSControlTextDidChangeNotification
											   object:name];
	
    [self installEngine];
    [self checkSN];
}
- (void) dealloc {
	
	[[NSNotificationCenter defaultCenter]removeObserver:self];
	[super dealloc];
}

/*
 
 Serial Number Name: a b
 Serial Number: SPTNSDE000-2222-AD0L-C657-5252-M82A
 */
-(void)checkSN
{

	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    eSellerate_DaysSince2000 validSN;
	
     
    NSString* serialNumber = [ud objectForKey: APP_SERIAL ];
	
	NSString* username = [ud objectForKey:APP_USERNAME];

	
    if( serialNumber != nil && ![serialNumber isEqualToString:@""] && 
		username != nil && ![ username isEqualToString:@""] )
    {

		if( [self validSerialNumber:serialNumber andName:username] )
			[self setValidSerialNumberMode:YES];
			
		else
			[self setValidSerialNumberMode:NO];

	}
}

-(BOOL)validSerialNumber:(NSString*)aSn andName:(NSString*)aName
{
	eSellerate_DaysSince2000 validSN;
	

		
	validSN =  eWeb_ValidateSerialNumber ([aSn UTF8String],		// Serial number (read from User Defaults)
										  [aName UTF8String],	// No name based key in this example
										  nil,					// No extra data in this example
										  "56260");			// Publisher Key (used for stronger validation)
		
	return validSN;
		
}

-(IBAction)fieldInputted:(id)sender
{
	if( sender == name ) return;
	
	

	
	
	if( [self validSerialNumber:[sn stringValue] andName:[name stringValue]] )	
	{
		[okButton setEnabled:YES];
	}else
		[okButton setEnabled:NO];

		

	
	
}

-(void)setValidSerialNumberMode:(BOOL)flag
{
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];


	if( flag == NO )
	{
			[okButton setEnabled:NO];
		[name setEditable:YES];
		[sn setEditable:YES];
		
	}else
	{
		NSString* serialNumber = [ud objectForKey: APP_SERIAL ];
		
		NSString* username = [ud objectForKey:APP_USERNAME];

		
		[sn setStringValue:serialNumber];
		[name setStringValue:username];
		
		[sn setBezeled:NO];
		[name setBezeled:NO];
		
		[sn setDrawsBackground:NO];
		[name setDrawsBackground:NO];

		[name setEditable:NO];
		[name setSelectable:YES];

		[sn setEditable:NO];	
		[sn setSelectable:YES];

		[okButton setEnabled:YES];

		
	}
	
}

-(IBAction)okButton:(id)sender
{
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];

	
	[ud setObject:[sn stringValue] forKey: APP_SERIAL ];
	[ud setObject:[name stringValue] forKey: APP_USERNAME ];
	[ud synchronize];
	
	[self checkSN];
	
	[window orderOut:self];
}


- (void)installEngine
{
    //[eSeller installEngine];
	
	NSString *mypath;
	
	mypath = [[NSBundle mainBundle] pathForResource:@"EWSMacCompress.tar.gz" ofType:nil];
	
	/*2* Install Engine example:
		* This function will ensure that the eSellerate engine is on the users computer before attempting to do anything.
		*/
	
	// EWS SDK Install Engine From Path function     
	OSStatus error =  eWeb_InstallEngineFromPath([mypath UTF8String]);
	
	if (error < 0) 
		NSRunAlertPanel(@"Application was unable to install the eSellerate engine. Please contact the makers of GeckoSoft for further information", @"", @"OK", nil, nil);
	
}

@end
