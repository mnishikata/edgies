#import "AppController.h"

#import <SyncServices/SyncServices.h>

#import "DropView.h"
#import "ColorCheckbox.h"

#import "ScreenUtil.h"

#import "NSWorkspace+ProcessUtil.h"


#import "EdgiesLibrary.h"
#import "FileEntityUtil.h"

#import "InstallLibrary.h"
#import "AvoidNullStringValueTransformer.h"

#define LOGINWINDOW_PREFERENCES (NSString*)CFSTR("/Library/Preferences/loginwindow.plist")
#define HELPER_PATH 	[[NSBundle bundleForClass:[self class]] bundlePath]
#define HELPER_NAME @"Edgies.app"


static int OSVERSION_CACHE = 0;


@implementation AppController

+ (void)initialize {
    if (self == [AppController class]) {   
		
		
		NSValueTransformer *mbTrans = [[AvoidNullStringValueTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"AvoidNullStringValueTransformer"];
		[mbTrans release];
		
		
		
    }
}

-(void)dummy
{
}

-(void)awakeFromNib
{	
	//check double launching
	
	if( 1 != countForLaunchedProcessPIDForBundleIdentifier(@"com.pukeko.edgies") )
	{
		ProcessSerialNumber psn = { 0, kCurrentProcess };
		
		SetFrontProcess(&psn);

		
		NSAlert *alert = [NSAlert alertWithMessageText: NSLocalizedString(@"Error",@"") defaultButton:NSLocalizedString(@"Quit",@"") alternateButton: nil otherButton:nil informativeTextWithFormat:NSLocalizedString(@"Another Edgies is running.",@"")];
		[alert runModal];
		
		
		[NSApp terminate:self];
		return;
		
	}
	
	
	
	BOOL firstRunFlag = firstRun();
	if( firstRunFlag )  // if pref doesnt exist, set default
	{
		NSUpdateDynamicServices();
		installDate();
		
		// Startup?
		ProcessSerialNumber psn = { 0, kCurrentProcess };
		
		SetFrontProcess(&psn);
		
		NSAlert *alert = [NSAlert alertWithMessageText: NSLocalizedString(@"Startup Message Title",@"") defaultButton:NSLocalizedString(@"OK",@"") alternateButton: NSLocalizedString(@"Cancel",@"") otherButton:nil informativeTextWithFormat:NSLocalizedString(@"Startup Message Description",@"")];
		
		if( [alert runModal] ==  NSAlertDefaultReturn )
		{
			[self writeLoginPlist:YES];
		}
		
		
		
		//copy documents
		
		// create folder
		NSString *docsDir = APPLICATION_SUPPORT_DIRECTORY;
		
		if( ! [[NSFileManager defaultManager] directoryContentsAtPath:docsDir] )
			[[NSFileManager defaultManager] createDirectoryAtPath:docsDir attributes:NULL];
		
		//
		
		if( ! [[NSFileManager defaultManager] directoryContentsAtPath:THEME_DIRECTORY] )
			[[NSFileManager defaultManager] createDirectoryAtPath:THEME_DIRECTORY attributes:NULL];
		
		
		NSFileManager* fm = [NSFileManager defaultManager];
		
		if( ! [fm directoryContentsAtPath:DEFAULT_DOCUMENT_FOLDER ] )
		{
			[fm createDirectoryAtPath: DEFAULT_DOCUMENT_FOLDER  attributes:NULL];
			
		}
		
		//Welcome_J.edgy, Welcome.edgy
		
		NSString* path = [[NSBundle mainBundle] pathForResource:@"Welcome_J" ofType:@"edgy"];
		if( path != nil )
		{
			NSString* newPath = [DEFAULT_DOCUMENT_FOLDER stringByAppendingPathComponent:@"Welcome_J.edgy"];
			[fm copyPath:path toPath:newPath handler:nil];
		}
		
		path = [[NSBundle mainBundle] pathForResource:@"Welcome" ofType:@"edgy"];
		if( path != nil )
		{
			NSString* newPath = [DEFAULT_DOCUMENT_FOLDER stringByAppendingPathComponent:@"Welcome.edgy"];
			[fm copyPath:path toPath:newPath handler:nil];
		}
	}
	
	
	
	//setup regitration
	registrationManager = [[RegistrationManager alloc] init];
	
	NSMenu* mainMenu = [NSApp mainMenu];
	NSMenuItem* item = [mainMenu itemAtIndex:0];
	NSMenu* appMenu = [item submenu];
	
	NSMenuItem* registrationMenuItem = [[NSMenuItem alloc] initWithTitle:@"Registration"
																  action:@selector(dummy)
														   keyEquivalent:@""];
	[registrationMenuItem setTarget:self];
	[registrationMenuItem setImage: [NSImage imageNamed: @"globe"]];
	NSMenu* submenu = [registrationManager menu];
	
	[registrationMenuItem setSubmenu:submenu ];
	[appMenu insertItem:registrationMenuItem atIndex:1];
	
	[registrationMenuItem release];
	
	
	
	
	
	
	syncManager				= [[SyncManager alloc] init];
	

	themeManager			= [[ThemeManager alloc] init];
	preferenceController	= [[PreferenceController alloc] init];
	
	tabDocumentController	= [[TabDocumentController alloc] init]; 
	graphicInspector		= [[GraphicInspector alloc] init];
	soundManager			= [[SoundManager alloc] init];
	keyModifierManager		= [[KeyModifierManager alloc] init];
	memoManager				= [[MemoManager alloc] init];
	dropWindowManager		= [[DropWindowManager alloc] init];
	tagManager				= [[TagManager alloc] init];

	attachmentCellConverter = [[AttachmentCellConverter alloc] init];
	
	
	aliasInspector = [[AliasInspector alloc] init];
	faviconInspector = [[FaviconInspector alloc] init];
	
	graphicInspector = [[GraphicInspector  alloc] init];
	checkboxInspector = [[CheckboxInspector  alloc] init];
	clipboardArchiveInspector = [[ClipboardArchiveInspector alloc] init];
	alarmInspector = [[AlarmInspector alloc] init];
	taggingInspector = [[TaggingInspector alloc] init];

/////////////
	
	
	loadAdditionalUDWithFilename(@"AdditionalUD_Main");

///// Sync
	
	// Setup the SyncIt instance.
	_syncIt = [[SyncIt alloc] init];
	[_syncIt setSyncSource:self];
	[_syncIt registerSchemas];
	[_syncIt setSyncAlertHandler:self selector:@selector(_client:mightWantToSyncEntityNames:)];
	
	
	// Syncs after launching to pull any changes since last running this app.
	
	if( preferenceIntValueForKey(@"syncOption") == 2 )
	{
		[self _sync];		
	}	
	
	// Register for sync engine availability notification.
	[[NSDistributedNotificationCenter defaultCenter] addObserver:self 
														selector:@selector(_handleSyncAvailabilityChangedNotification:) 
															name:ISyncAvailabilityChangedNotification object:@"YES"];
	
	
	
	if( firstRunFlag )
	{
		[preferenceController showPreferencePanel:self];
		[preferenceController apply];
	}

	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}

- (void) dealloc
{
	[_syncIt release];

	/*
	 
	 Should release all the instances
	 
	 */
	
	[super dealloc];
}


- (void)activateStatusMenu
{
	// modify menu
	
	NSMenuItem *item = [theMenu itemAtIndex:0];
	[item setTitle:@"Edgies"];
	
	[theMenu addItem: [NSMenuItem separatorItem] ];
	
	item = [[[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Quit Edgies",@"") action:@selector(quit:) keyEquivalent:@""] autorelease];
	[theMenu addItem: item ];
	[theMenu setDelegate:self];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activate:) name:NSMenuDidEndTrackingNotification object:theMenu];
	
	// setup menu icon
    NSStatusBar *bar = [NSStatusBar systemStatusBar];
	
    NSStatusItem *theItem = [bar statusItemWithLength:NSSquareStatusItemLength];
    [theItem retain];
	
    [theItem setTitle: @""];
	[theItem setImage: [NSImage imageNamed:@"StatusBarIcon"]];

    [theItem setHighlightMode:YES];
    [theItem setMenu:theMenu];
	
	
}

-(void)activate:(id)obj
{
	//NSLog(@"activate %@",[obj name]);
	//[NSApp performSelector:@selector(activateIgnoringOtherApps:) withObject:[NSNumber numberWithBool:YES] afterDelay:1.0];

	// Transform process from background to foreground
	ProcessSerialNumber psn = { 0, kCurrentProcess };
	
	/*
	OSStatus returnCode = TransformProcessType(& psn,  
											   kProcessTransformToForegroundApplication);
	
	if( returnCode != 0)
		NSLog(@"Could not bring app to front. Error %d", returnCode);
	*/
	SetFrontProcess(&psn);

}

-(void)quit:(id)obj
{
	[NSApp terminate:self];
}

-(RegistrationManager*)registrationManager
{
	return registrationManager;
}

-(SyncManager*)syncManager
{
	return syncManager;	
}

-(ThemeManager*)themeManager
{
	return themeManager;	
}


-(PreferenceController*)preferenceController
{
	return preferenceController;
}
-(TabDocumentController*)tabDocumentController
{
	return tabDocumentController;	
}

-(SoundManager*)soundManager
{
	return soundManager;	
}

-(DropWindowManager*)dropWindowManager
{
	return dropWindowManager;
}
-(KeyModifierManager*)keyModifierManager
{
	return keyModifierManager;	
}
-(TagManager*)tagManager
{
	return tagManager;	
}




-(AliasInspector*)aliasInspector
{
	return aliasInspector;
}

-(FaviconInspector*)faviconInspector
{
	return faviconInspector;
}

-(AttachmentCellConverter*)attachmentCellConverter
{
	return attachmentCellConverter;
}

-(GraphicInspector*) graphicInspector
{
	return graphicInspector;
}

-(CheckboxInspector*)checkboxInspector
{
	return checkboxInspector;
}

-(ClipboardArchiveInspector*)clipboardArchiveInspector
{
	return clipboardArchiveInspector;
}

-(AlarmInspector*)alarmInspector
{
	return alarmInspector;
}

-(TaggingInspector*)taggingInspector
{
	return taggingInspector;
}
/// 

#pragma mark -
-(IBAction)showPreferencePanel:(id)sender
{
	[preferenceController showPreferencePanel:(id)sender];
}
-(IBAction)showMemoManager:(id)sender
{
	[memoManager showMemoManager:(id)sender];
}
-(IBAction)showGraphicInspector:(id)sender
{
	[[GraphicInspector sharedGraphicInspector] showGraphicInspector];
}
-(IBAction)showAliasButtonInspector:(id)sender
{
	[[AliasInspector  sharedAliasInspector] showAliasInspector];
}
-(IBAction)showFaviconInspector:(id)sender
{
	[[FaviconInspector  sharedFaviconInspector] showFaviconInspector];
}
-(IBAction)showCheckboxInspector:(id)sender
{
	[[CheckboxInspector  sharedCheckboxInspector] showCheckboxInspector];
}

-(IBAction)showClipboardArchiveInspector:(id)sender
{
	[[ClipboardArchiveInspector  sharedClipboardArchiveInspector] showClipboardArchiveInspector];

}

-(IBAction)showAlarmInspector:(id)sender
{
	[[AlarmInspector  sharedAlarmInspector] showAlarmInspector];
	
}

//
-(IBAction)arrangeToFront:(id)sender
{
	[tabDocumentController  arrangeToFront:(id)sender ];
}
-(IBAction)arrangeToBack:(id)sender
{
	[tabDocumentController  arrangeToBack:(id)sender ];
}
-(IBAction)bringCenter:(id)sender
{
	[tabDocumentController  bringCenter:(id)sender ];
}
-(IBAction)navigate:(id)sender
{
	[tabDocumentController  navigate:(id)sender ];
}
-(IBAction)openAll:(id)sender
{
	[tabDocumentController  openAll:(id)sender ];
}
-(IBAction)closeAll:(id)sender
{
	[tabDocumentController  closeAll:(id)sender ];
}
-(IBAction)newDocument:(id)sender
{
	[tabDocumentController  newDocument:(id)sender ];
}
-(IBAction)hideMemoManagerPanel:(id)sender
{

	if( [[NSApp keyWindow] isKindOfClass:[MNTabWindow class]] )
		[ (MNTabWindow*)[NSApp keyWindow] closeTab];
	else
		[[NSApp keyWindow] orderOut:self];
}


-(IBAction)colorMenuSelected:(id)sender //responder chain last
{
	
	[[[tabDocumentController actionForcusedDocument] window] colorMenuSelected:sender];

}



#pragma mark -







-(void)setupCheckboxMenu
{
	
	NSData* data = [[[NSData alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"checkboxTable" ofType:@"rtf"]] autorelease];
	
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil ] autorelease];
	
	NSArray* colornameArray = [[attr string] componentsSeparatedByString:@"\n"];
	
	////NSLog(@"%@",[colornameArray description]);
	
	NSMutableArray* checkboxTable = [NSMutableArray array];
	
	int hoge;
	for( hoge = 0; hoge < [colornameArray count]; hoge++ )
	{
		
		NSString* str = [colornameArray objectAtIndex:hoge];
		
		if( ! [str isEqualToString:@""] )
		{
			NSRange range = [[attr string] rangeOfString:str];
			
			NSAttributedString* subAttr = [attr attributedSubstringFromRange:range];
			
			NSColor* color = [subAttr attribute:NSForegroundColorAttributeName
										atIndex:0
								 effectiveRange:nil];
			
			NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys: str , @"name", color , @"color",nil ];
			[checkboxTable addObject:dic];
		}
		
	}
	

	//checkboxTable contains colors
	
	
	NSMenu* submenu = [[NSMenu alloc] init];
	
	//int hoge;
	for( hoge = 0; hoge < [checkboxTable count]; hoge++ )
	{
		NSDictionary* dic = [checkboxTable objectAtIndex:hoge];
		
		NSMenuItem* submenuItem = [[NSMenuItem alloc] initWithTitle:[dic objectForKey:@"name"]																   action:@selector(insertCheckbox:)
													  keyEquivalent:@""];
		[submenuItem setTag:hoge];
		[submenuItem setTarget:nil];
		[submenuItem setImage:[ColorCheckbox colorCheckbox:[dic objectForKey:@"color"] checkFlag:NO] ];
		[submenu addItem:[submenuItem autorelease]];
		
	}
	[submenu addItem:[NSMenuItem separatorItem]];
	NSMenuItem* submenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Insert Checkbox",@"")			
														 action:@selector(insertCheckbox:)
												  keyEquivalent:@"I"];
	[submenuItem setKeyEquivalentModifierMask:1048576];
	
	[submenuItem setTag:-1];
	[submenuItem setTarget:nil];

	[submenu addItem:[submenuItem autorelease]];

	
	NSMenu* mainMenu = [NSApp mainMenu];
	NSMenuItem* editMenuItem = [mainMenu itemWithTag:9];
	NSMenuItem* insertCheckboxItem = [[editMenuItem submenu] itemWithTag:9];

	[[editMenuItem submenu] setSubmenu:[submenu autorelease] forItem:insertCheckboxItem];

	/*
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Insert Checkbox",@"")
												action:@selector(insertCheckbox:) keyEquivalent:@""];
	[customMenuItem setTag:-1];
	[customMenuItem setRepresentedObject:@"MN_CheckboxTextView"];
	
	[aContextMenu addItem:customMenuItem ];
	
	[aContextMenu setSubmenu:[submenu autorelease] forItem:customMenuItem];
	
	[customMenuItem release];	
	//debug end
	
	return aContextMenu;
	 */
	
	
}


#pragma mark Action

-(IBAction)checkForUpdates:(id)sender
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"feed://www.oneriver.jp/update/news.xml"]];	
}
-(IBAction)slowSync:(id)sender
{
	[_syncIt setPreferredSyncMode:SlowSyncMode];
	[self _sync];
	//[syncManager syncDocumentFolder: DOCUMENT_FOLDER pull:YES];
}
-(IBAction)refreshSync:(id)sender
{
	[_syncIt setPreferredSyncMode:RefreshSyncMode];

	[self _sync];
	//[syncManager syncDocumentFolder: DOCUMENT_FOLDER pull:YES];
}



-(IBAction)sync:(id)sender
{
	[self _sync];
	//[syncManager syncDocumentFolder: DOCUMENT_FOLDER pull:YES];
}


-(IBAction)syncModifyCurrentDocument:(id)sender
{
	MiniDocument* doc =[tabDocumentController currentDocument];
	NSString* path = [doc filePath];
	
	if( path!= nil )
	[syncManager pushChangedFileAtPath:path name:[doc name] type:ISyncChangeTypeModify ];
	else
		NSBeep();
}


#pragma mark -





-(id)inoPlistValueForKey:(NSString*)key
{
	NSString* path;
	path = [[NSBundle bundleForClass:[self class]] pathForResource:@"appIcon" ofType:@"icns"];
	
	path = [path stringByDeletingLastPathComponent];
	path = [path stringByDeletingLastPathComponent];
	path = [path stringByAppendingPathComponent:@"Info.plist"];
	
	
	//	//NSLog(@"info.plist path %@",path);
	NSDictionary* infoPlist = [NSDictionary dictionaryWithContentsOfFile:path];
	
	return [infoPlist objectForKey:key];
	
}

-(void)setInfoPlistValue:(id)value forKey:(NSString*)key
{
	
	NSString* path;
	path = [[NSBundle bundleForClass:[self class]] pathForResource:@"appIcon" ofType:@"icns"];
	
	path = [path stringByDeletingLastPathComponent];
	path = [path stringByDeletingLastPathComponent];
	path = [path stringByAppendingPathComponent:@"Info.plist"];
	
	
	//	//NSLog(@"info.plist path %@",path);
	NSMutableDictionary* infoPlist = [NSMutableDictionary dictionaryWithContentsOfFile:path];
	
	[infoPlist setObject:value forKey:key];
	
	//	//NSLog(@"info.plist %@",[infoPlist description]);
	
	
	[infoPlist writeToFile:path atomically:YES];

}

-(IBAction)saveAllDocuments:(id)sender
{
	[[tabDocumentController documents] makeObjectsPerformSelector:@selector( saveIfNeeded ) ];

}

-(IBAction)saveAndQuit:(id)sender
{

	[[NSNotificationCenter defaultCenter] removeObserver:self];
	

	
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	//remove old  documents saved in ud
	[ud removeObjectForKey:@"documents" ];	
	

	[ud synchronize];
	
	
	
	// dock icon
	
	//showDockIcon
	BOOL flag = preferenceBoolValueForKey(@"donotShowDockIcon");
	
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	
	NSDictionary *dict = [bundle infoDictionary];
	if(  [[dict objectForKey:@"EDGIES_MENU_BAR_VERSION"] boolValue] )
	{
		flag = YES;
	}
	
	
	if( 	showDockIcon !=  flag )
	{
		

			
			// change LSUIElement only indock version
			
			[self setInfoPlistValue:[NSNumber numberWithBool: flag ] forKey:@"LSUIElement"];
			
			
			NSString* path = [[NSBundle mainBundle] bundlePath];
			
			
			NSURL* myurl = [NSURL URLWithString:path];
			
			LSRegisterURL (
						   (CFURLRef)myurl,
						   true
						   );
			
		

	}

	
	
		
	//sync
	if( preferenceIntValueForKey(@"syncOption") == 2 )
	{
		[self _sync];		
	}	
	
	
	if( !  [sender isKindOfClass:[NSNotification class]] )
	{
		[[NSApplication sharedApplication] terminate:self];
		////NSLog(@"closed by user");

	}else
	{
		////NSLog(@"closed by system");
	
	}

}




-(void)resolutionChanged:(id)sender
{
	
	//// setup dropwindows

	
	//[self setupDropWindows];
	
	
	
	
	///// reposition
	
 
	int maxScreenId = [[NSScreen screens] count] -1;
	
	NSMutableArray* documents = [[TabDocumentController sharedTabDocumentController] documents];

	int hoge;
	for( hoge = 0; hoge < [documents count]; hoge++ )
	{
		MiniDocument* doc = [documents objectAtIndex:hoge];
		

		int myId = [[doc window] screenID];
		if( myId > maxScreenId )
			[[doc window] setScreenID: maxScreenId];
		
		
		
		NSRect modifiedRect = [[doc window] solveOffScreen];
		
		if( ! NSEqualRects( modifiedRect,[[doc window] frame] ) )
		{
			[[doc window] setFrame:modifiedRect
						  display:NO animate:NO];
			
			
			//[[doc window] closeTab];
		}else
		{
			[[doc window] setFrame:[[doc window] closedFrame]
						   display:NO animate:NO];	
		}
		[[doc window] closeTab];

		

		
				
	}
	

}



-(void)preferenceChanged:(NSNotification*)notif
{
	/*
	unsigned hoge;
	for( hoge = 0 ; hoge < [documentBank count]; hoge++ )
	{
		
		MiniDocument* doc = [documentBank objectAtIndex:hoge];
		[[doc window] setPreference];
		
	}
	 */
	
	// set balloon transparency
	[balloon setAlpha: preferenceFloatValueForKey( @"sliderBalloon") / 100.0 ];

	
	[tabCursor release];
	tabCursor = [[TabCursor alloc] init];

	

	
	
	////NSLog(@"preference changed");

}






- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename
{
	BOOL flag = NO;
	NSMutableArray* documents = [[TabDocumentController sharedTabDocumentController] documents];

	////NSLog(@"opening %@",filename);
	
	int hoge;
	for( hoge = 0; hoge < 	[documents count]; hoge++ )
	{
		MiniDocument* doc = [documents objectAtIndex: hoge];
		if( [[doc filePath] isEqualToString: filename] )
		{
			
			[(MNTabWindow*)[doc window] openTab];
			flag = YES;
			break;
		}
		
	}
	
	if( flag == NO ) //not found 
	{
		
		MiniDocument* addingDoc = [tabDocumentController newDocumentForDropping];
		NSData* readDocData =[[[NSData alloc] initWithContentsOfFile:filename] autorelease];

		
		if( readDocData != nil )
		{
			
			[addingDoc setDocumentAttributes:readDocData];
			
			[addingDoc setValue:[NSNumber numberWithBool:NO] forKey:@"isDocumentEdited"];

			
			[[addingDoc window] orderFront:self];
			[(MNTabWindow*) [addingDoc window] redrawWithShadowProperly];
			
			[[addingDoc window] openTab];
			[addingDoc save];
			
			
			NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"New Memo Added" ,@"" )
											 defaultButton:NSLocalizedString(@"OK" ,@"")
										   alternateButton:@""
											   otherButton:@""
								informativeTextWithFormat:NSLocalizedString(@"The new memo was added to Edgies. The file you opend can be deleted.",@"")];
			
			
			[alert runModal];
			
		}
		
	}

	return flag;
}

//// dockmenu
- (void)menuNeedsUpdate:(NSMenu *)menu
{
	//update launchAtStartupMenuItem
	if( [launchAtStartupMenuItem menu] == menu )
	{
		[launchAtStartupMenuItem setState: ([self isLoginItem]? NSOnState:NSOffState)  ];
		
		return;
	}
	
	
	if( theMenu == menu )
	{
		/*
		 NSLog(@"activateIgnoringOtherApps");
				[NSApp activateIgnoringOtherApps:YES];
		*/
		return;
		
	}
	
	
	NSMutableArray* documents = [[TabDocumentController sharedTabDocumentController] documents];

	
	////NSLog(@"menuNeedsUpdate" );
	//add mini document file alias
	
	
	//remove first
	int hoge;
	for( hoge = [menu numberOfItems]-1; hoge >=0 ; hoge-- )
	{
		NSMenuItem* menuItem = [menu itemAtIndex:hoge];
		if( [menuItem tag] == 100 ) [menu removeItem:menuItem];
		
	}
	
	
	//add
	////NSLog(@"doc count %d, menu item %d",[documentBank count], [menu numberOfItems]);
	for( hoge = 0; hoge < [documents count]; hoge++ )
	{
		MiniDocument* doc = [documents objectAtIndex:hoge];
		
		NSString* title = [doc valueForKey:@"p_title"];
		
		NSMenuItem* menuItem = [[NSMenuItem alloc] initWithTitle:title
														  action:@selector(docSelectedInDockMenu:)
												   keyEquivalent:@""];
		[menuItem autorelease];

		
		
		/*
		NSImage* docIconInDock = [ColorCheckbox roundedBox:color
										  size:NSMakeSize(12,12)
										 curve:4 
									frameColor:frameColor];
		[menuItem setImage:docIconInDock ];
*/
			
		[menuItem setRepresentedObject:doc];
		
		[menuItem setTag: 100];
		
		[menu insertItem: menuItem atIndex:0];

	}
	
	
}



-(void)docSelectedInDockMenu:(id)sender
{
	MiniDocument* doc = [sender representedObject];
	if( ! [doc isKindOfClass:[MiniDocument class]] ) return;
	
	 
	[(MNTabWindow*) [doc window] openTab];
}


////
+(int)OSVersion
{
	if( OSVERSION_CACHE == 0 )
	{
	long SystemVersionInHexDigits;
	long MajorVersion, MinorVersion, MinorMinorVersion;
	
	Gestalt(gestaltSystemVersion, &SystemVersionInHexDigits);
	
	
	MinorMinorVersion = SystemVersionInHexDigits & 0xF;
	
	MinorVersion = (SystemVersionInHexDigits & 0xF0)/0xF;
	
	MajorVersion = ((SystemVersionInHexDigits & 0xF000)/0xF00) * 10 +
		(SystemVersionInHexDigits & 0xF00)/0xF0;
	
	 
	////NSLog(@"ver %ld", SystemVersionInHexDigits);
	////NSLog(@"%ld.%ld.%ld", MajorVersion, MinorVersion, MinorMinorVersion);	
	
	
	OSVERSION_CACHE = (int)MajorVersion*10 + MinorVersion;
	}
	
	return OSVERSION_CACHE;
}


// service

- (void)service_makeEdgy:(NSPasteboard *)pboard userData:(NSString *)userData error:(NSString **)error
{
	//NSLog(@"service");
	
	id doc = [tabDocumentController newDocumentForDropping];
	
	MNTabWindow* docWindow = (MNTabWindow*)[doc window];
	
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect frame = [docWindow frame];
	[docWindow setFrame:NSMakeRect(mouseLoc.x -frame.size.width/2,  mouseLoc.y -frame.size.height/2,
								   frame.size.width, frame.size.height)
				display:NO];
	
	[docWindow setTabPosition:[docWindow tabPosition]];
	
	
	//[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	
	//[docWindow makeFirstResponder:[doc textView]];
	
	[docWindow selectNextKeyView:self];
	

	
	//[[doc window] tearOff];
	[ (MNTabWindow*)[doc window] repositionToMouse];
	[ (MNTabWindow*)[doc window] openTab];
	
	
	[docWindow orderFront:self];
	
	
	
	
		
	id textToPaste = [attachmentCellConverter readTextFromPasteboard:pboard  draggingSource:nil];
	
	if( textToPaste == nil )
		*error = @"Fail to import to Edgies.";

	
	[[doc textView] pasteText:textToPaste];
	
	if( preferenceBoolValueForKey(@"serviceBringToFront") == YES )
		[[NSApplication sharedApplication] arrangeInFront:self];
	
	
	if(  preferenceBoolValueForKey(@"serviceKeepClosed") == YES )
		[docWindow closeTab];
	
	return;
	
}


-(void)writeLoginPlist:(BOOL)flag
{
	
	NSString *loginWindowPreferencesFilename = [NSHomeDirectory() stringByAppendingPathComponent:LOGINWINDOW_PREFERENCES];
	NSData *loginWindowPreferencesFile = [NSData dataWithContentsOfFile:loginWindowPreferencesFilename];
	if(loginWindowPreferencesFile != nil)
	{
		BOOL success;
		// Loaded OK
		NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;	// will be replaced
		NSString *errs = nil;
		NSMutableDictionary *plist = [NSPropertyListSerialization 
										  propertyListFromData:loginWindowPreferencesFile
											  mutabilityOption:NSPropertyListMutableContainersAndLeaves
														format:&format
											  errorDescription:&errs];
		if(errs == nil && plist != nil)
		{
			// Loaded OK.
			NSMutableArray *autoLaunched = [plist objectForKey: (NSString*)CFSTR("AutoLaunchedApplicationDictionary") ];
			if(autoLaunched == nil)
			{
				// Create a new array to put the app in
				autoLaunched = [NSMutableArray arrayWithCapacity:1];
				[plist setObject:autoLaunched forKey: (NSString*)CFSTR("AutoLaunchedApplicationDictionary") ];
			}
			
			// Check the array for our app
			NSEnumerator *enumAutoLaunched = [autoLaunched objectEnumerator];
			NSMutableDictionary *item = nil;
			BOOL needToSetAutoLaunch = YES;
			while(item = [enumAutoLaunched nextObject])
			{
				// See if this item is our one.
				NSString *path = [item objectForKey:@"Path"];
				if(path != nil && [path isEqual: HELPER_PATH ])
				{
					// It's there. Don't do anything
					needToSetAutoLaunch = NO;
					break;
				}
				else if(path != nil && [path hasSuffix: HELPER_NAME ])
				{
					// We'll need to adjust this path, as it points to the wrong thing
					break;
				}
			}
			
			if(needToSetAutoLaunch && flag )
			{
				// Create a new item, or just adjust the one we have?
				if(item == nil)
				{
					item = [NSMutableDictionary dictionaryWithCapacity:2];
					[autoLaunched addObject:item];
				}
				
				// Set data
				[item setObject: HELPER_PATH  forKey:@"Path"];
				[item setObject:[NSNumber numberWithBool:NO] forKey:@"Hide"];
				
				// And save it back
				NSString *errs = nil;
				NSData *newFile = [NSPropertyListSerialization dataFromPropertyList:plist format:format errorDescription:&errs];
				if(errs == nil && newFile != nil)
				{
					success =  [newFile writeToFile:loginWindowPreferencesFilename atomically:YES];
				}else
				{
					success = NO;
				}
			}if( !needToSetAutoLaunch && !flag )
			{

				[autoLaunched removeObject: item];
				
				// And save it back
				NSString *errs = nil;
				NSData *newFile = [NSPropertyListSerialization dataFromPropertyList:plist format:format errorDescription:&errs];
				if(errs == nil && newFile != nil)
				{
					success =  [newFile writeToFile:loginWindowPreferencesFilename atomically:YES];
				}else
				{
					success = NO;
				}
			}
		}
	}
}

-(void)launchAtStartupMenuItemSelected:(id)sender
{
	[self writeLoginPlist: ![self isLoginItem]];
}




-(BOOL)isLoginItem
{

	NSString *loginWindowPreferencesFilename = [NSHomeDirectory() stringByAppendingPathComponent:LOGINWINDOW_PREFERENCES];
	NSData *loginWindowPreferencesFile = [NSData dataWithContentsOfFile:loginWindowPreferencesFilename];
	if(loginWindowPreferencesFile != nil)
	{
		// Loaded OK
		NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;	// will be replaced
		NSString *errs = nil;
		NSMutableDictionary *plist = [NSPropertyListSerialization 
										  propertyListFromData:loginWindowPreferencesFile
											  mutabilityOption:NSPropertyListMutableContainersAndLeaves
														format:&format
											  errorDescription:&errs];
		if(errs == nil && plist != nil)
		{
			// Loaded OK.
			NSMutableArray *autoLaunched = [plist objectForKey: (NSString*)CFSTR("AutoLaunchedApplicationDictionary") ];
			if(autoLaunched == nil)
			{
				return NO;
			}
			
			// Check the array for our app
			NSEnumerator *enumAutoLaunched = [autoLaunched objectEnumerator];
			NSMutableDictionary *item = nil;

			while(item = [enumAutoLaunched nextObject])
			{
				// See if this item is our one.
				NSString *path = [item objectForKey:@"Path"];
				if(path != nil && [path isEqual: HELPER_PATH ])
				{
					// It's there. Don't do anything
					return YES;
				}
				else if(path != nil && [path hasSuffix: HELPER_NAME ])
				{
					// We'll need to adjust this path, as it points to the wrong thing
					
					//break;
				}
			}
			

			return NO;
		}
	}
	
	return NO;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	//if uielement is true, show menubar menu

	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	
	NSDictionary *dict = [bundle infoDictionary];
	if( [[dict objectForKey:@"EDGIES_MENU_BAR_VERSION"] boolValue] )
	{
		[self activateStatusMenu];
	}
	

	
    [NSApp setServicesProvider:self];   
	NSMutableArray* documents = [[TabDocumentController sharedTabDocumentController] documents];


	
	//////
	
	
	
	////NSLog(@"OS VERSION %d",[AppController OSVersion]);
	
	//[FileEntityUtil initialize];
	
	
	
	
	// set up children
	
	
	balloon = [[BalloonController alloc] init];
	tabCursor = [[TabCursor alloc] init];
	
	
	
	
	
	//
	
	int version = [AppController OSVersion];
	
#if MAC_OS_X_VERSION_MAX_ALLOWED < 1040
	
	if( version >= 104 )
	{
		
		NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"OS Version Error",@"")
										 defaultButton:NSLocalizedString(@"Quit",@"")
									   alternateButton:NSLocalizedString(@"Visit Website",@"")
										   otherButton:NSLocalizedString(@"",@"")
							 informativeTextWithFormat:NSLocalizedString(@"This application does not work on Mac OS X version 10.4 or later.  Check for newer version at the website.",@"")];
		int result = [alert runModal];
		
		
		if( result == NSAlertAlternateReturn )
		{
			
			[self visitWebsite:self];
			
			
		}

		[[NSApplication sharedApplication] terminate:self];
		 
	}
	
	
#endif
	
	
#if MAC_OS_X_VERSION_MAX_ALLOWED >= 1040
	
	if( version < 104 )
	{
		NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"OS Version Error",@"")
										 defaultButton:NSLocalizedString(@"Quit",@"")
									   alternateButton:NSLocalizedString(@"Visit Website",@"")
										   otherButton:NSLocalizedString(@"",@"")
							 informativeTextWithFormat:NSLocalizedString(@"This application does not work on Mac OS X version 10.3.9 or earlier.",@"")];
		int result = [alert runModal];
		
		
		if( result == NSAlertAlternateReturn )
		{
			
			[self visitWebsite:self];
			
			
		}
		
		[[NSApplication sharedApplication] terminate:self];
	}
	
	
#endif
	
	
	//Shareware dialogue
	
	registered = [[RegistrationManager sharedRegistrationManager] registered] ;
	
	if( !registered )
	{
		
		//[[RegistrationManager sharedRegistrationManager] showDemoDialogue];
		
		
		// show elapsed days
		

		NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:installDate()];
		

		
		NSArray* documents = [tabDocumentController documents];
		unsigned int i, count = [documents count];
		for (i = 0; i < count; i++) {
			MiniDocument * doc = [documents objectAtIndex:i];
			NSDate* creationDate = [doc creationDate];
			
			NSTimeInterval ti = [[NSDate date] timeIntervalSinceDate: creationDate];
			
			if( ti > elapsedTime )
				elapsedTime = ti;
		}
		
		
		int day = elapsedTime/(60*60*24);
		
		if( day > 15 || day < 0 )
		{
			expired = YES;			
		}
		else
		{
			expired = NO;			
		}
	}
	
	
	
	
	// create folder
	NSString *docsDir = APPLICATION_SUPPORT_DIRECTORY;
	
	if( ! [[NSFileManager defaultManager] directoryContentsAtPath:docsDir] )
		[[NSFileManager defaultManager] createDirectoryAtPath:docsDir attributes:NULL];
	
	//
	
	if( ! [[NSFileManager defaultManager] directoryContentsAtPath:THEME_DIRECTORY] )
		[[NSFileManager defaultManager] createDirectoryAtPath:THEME_DIRECTORY attributes:NULL];
	
	
	
	//set up scratch
	
	if( ! [[NSFileManager defaultManager] directoryContentsAtPath:DOCUMENT_FOLDER] )
	{
		[[NSFileManager defaultManager] createDirectoryAtPath:DOCUMENT_FOLDER attributes:NULL];
		
	}
	
	NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
	if( [[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:DOCUMENT_FOLDER] == NO)
	{
		
		NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Fatal Error",@"")
						defaultButton:@"OK" alternateButton:@"" otherButton:@""
			informativeTextWithFormat:NSLocalizedString(@"Documents cannot be saved!",@"")];
		
		[alert runModal];
	}
	
	
	
	
	
	
	
	///
	
	
	
	
	[[NSUserDefaultsController sharedUserDefaultsController] setAppliesImmediately:YES];
	
	
	// read saved docs
	


	NSMutableDictionary* saveddocs;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	
	BOOL flag = [ud boolForKey:@"SaveDocumentsInApplicationSupport"];
	
	
	id _data = [ud objectForKey:@"documents"];
	if( flag == NO && _data != NULL ) //read documents from ud
	{
		
		saveddocs  = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData: _data ]];
		if( saveddocs == nil ) saveddocs = [NSDictionary dictionary];
		
		NSArray* keys = [saveddocs allKeys];
		
		
		////NSLog(@"count %d",[keys count]);
		
		BOOL animateBuffer = [ud boolForKey:@"animateOpenClose"];
		[ud setBool:YES forKey:@"animateOpenClose"];
		
		unsigned hoge;
		
		for( hoge = 0; hoge < [saveddocs count]; hoge++ )
		{
			NSString* docName = [keys objectAtIndex:hoge];
			
			MiniDocument* doc = [[MiniDocument alloc] init] ;
			[documents addObject:doc]; 
			[doc release];
			
			
			[doc setDocumentAttributes:[saveddocs objectForKey:docName]];
			
			//[saveddocs removeObjectForKey:docName ];
			
			//[[doc window] openTab];
			//[[doc window] setupTabs];
			[(MNTabWindow*) [doc window] orderFront:self];
			[(MNTabWindow*) [doc window] closeTab];
			[(MNTabWindow*) [doc window] redrawWithShadowProperly];
			
			
		}
		
		[ud setBool:animateBuffer forKey:@"animateOpenClose"];
		
	}
	
	else //read from document folder here
	{
			//persistent documents are read by document controller
	
	}
	
	
	
	
	
	
	
	
	
	//[PREF setupSounds];
	
	//[self arrangeToBack:self];
	[tabDocumentController arrangeToFront:self];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveAndQuit:) name:NSApplicationWillTerminateNotification object:[NSApplication sharedApplication]];
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resolutionChanged:) name:NSApplicationDidChangeScreenParametersNotification object:[NSApplication sharedApplication]];
	

	
		//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(manageDocumentBank:) name:MemoHiddenStatusDidChangeNotification object:nil];
	
	
	
	
	// dock icon is hidden
	
	showDockIcon = [[self inoPlistValueForKey:@"LSUIElement"] boolValue];
	if( preferenceBoolValueForKey(@"donotShowDockIcon") != showDockIcon )
	{
		setPreferenceValueForKey([NSNumber numberWithBool:showDockIcon],@"donotShowDockIcon");

	}
	
	
	//initiate manager
	
	
	
	
	//[self resolutionChanged:self];
	
	
	///set menu
	//[self setupColorMenu];
	[self setupCheckboxMenu];

	
	///// preference changed observer
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(preferenceChanged:)
												 name:PreferenceDidChangeNotification
											   object:nil];
	
	
	
	
//	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	
	////NSLog(@"app controller");
	
	
}


-(IBAction)visitWebsite:(id)sender
{
	
	visitWebsite();
	
}
#pragma mark -
#pragma mark Sync
#pragma mark 


// SyncingSource Methods

// Returns an array of schema bundle paths.
- (NSArray *)schemaBundlePaths {
	return [NSArray arrayWithObject:
		[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Contents/Library/EdgiesSyncSchema.syncschema"]];
}

// Returns a unique client identifier.
- (NSString *)clientIdentifier
{
	return @"com.pukeko.edgies";
}

//Return the path to the client description property list.
- (NSString *)clientDescriptionPath
{
	return [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Contents/Library/EdgiesSyncSchema.syncschema//Contents/Resources/EdgiesMemoClientDescription.plist" ];
}

// Returns the syncable entities.
- (NSArray *)entityNames
{
	return [NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"];
}



#pragma mark -

// Transforms the sync source objects for the given entityName into sync records. 
// Invoked when the sync engine requests all records for an entity.
- (NSDictionary *)recordsForEntityName:(NSString *)entityName
{

	
	
	return [tabDocumentController recordsToSync];
	
	
}

// Returns a dictionary of changed records where the keys are the record identifiers. Uses RecordTransformer to
// to transform sync source objects to Sync Service records.
- (NSDictionary *)changedRecordsForEntityName:(NSString *)entityName
{

	
	return [tabDocumentController changedRecordsToSync];
}

// Returns an array of record identifiers of the deleted objects. Invoked when pushing deletions to the sync engine.
- (NSArray *)deletedRecordsForEntityName:(NSString *)entityName
{
	
	return [tabDocumentController deletedRecordsToSync];
}

// Inserts a new source object for the specified record, record identifier and entity name. Returns YES if 
// successful, NO otherwise.
- (BOOL)addObjectFromChange:(ISyncChange *)change forEntityName:(NSString *)entityName
{
	return [self modifyObjectFromChange:change forEntityName:entityName];
}

// Applies the change, specified by change, to the corresponding sync object. The property values in 
// [change record] are applied to the existing source object. Returns YES if successful, NO otherwise.
- (BOOL)modifyObjectFromChange:(ISyncChange *)change forEntityName:(NSString *)entityName
{
	BOOL flag = NO;
		switch ( [change type] ) {
			case ISyncChangeTypeAdd:
			{
				
				
				
				NSString *newRecordId = [change recordIdentifier];
				// recordIdentifier is like com.apple.syncservices:565E8D38-47FE-45A8-85DE-7DE47DDF7670

				[syncField insertText: [NSString stringWithFormat: @"\nadd obj  %@",newRecordId]];
				
				NSData* dataToWrite = [[change record] objectForKey: @"Data"];
				
				//NSLog(@"adding %@", [[change record] objectForKey:@"Name"] );
				
				NSString* path = [DOCUMENT_FOLDER  stringByAppendingPathComponent:[[change record] objectForKey: @"Name"]];
				path = [[path stringByAppendingPathExtension:@"edgy"] uniquePathForFolder];
				
				//write file here
				[dataToWrite writeToFile:path atomically:nil];
				
				//Open now.
				
				
				MiniDocument* addingDoc = [tabDocumentController newDocumentForDropping];
				NSData* readDocData =[[[NSData alloc] initWithContentsOfFile:path] autorelease];
				
				if( readDocData != nil )
				{
					
					[addingDoc setDocumentAttributes:readDocData];
					
					[addingDoc setValue:[NSNumber numberWithBool:NO] forKey:@"isDocumentEdited"];
					
					//[addingDoc setRecordIdentifier: [change recordIdentifier]];

					
					[[addingDoc window] orderFront:self];
					[(MNTabWindow*) [addingDoc window] redrawWithShadowProperly];
					
					[[addingDoc window] openTab];
					//[addingDoc save];
					
											
				}
					
				
				
				
				flag = YES;
				break;
			}
				
				
			case ISyncChangeTypeModify:
			{
				
				
				NSString *newRecordId = [change recordIdentifier];
				
				[syncField insertText: [NSString stringWithFormat: @"\nmodify obj %@  ",newRecordId]];

				
				NSData* dataToWrite = [[change record] objectForKey: @"Data"];
				MiniDocument *docToChange = [tabDocumentController miniDocumentWithName: [[change record] objectForKey: @"Name"]];
					
				
				if( docToChange != nil && dataToWrite != nil )
				{
					[docToChange setDocumentAttributes: dataToWrite];					
				}
				flag = YES;
				
				break;
			}
		}
	
	
		return flag;
	
}

// Deletes the record specified by record identifier. Returns YES if successful, NO otherwise.
- (BOOL)deleteObjectForRecordIdentifier:(NSString *)recordIdentifier
{
	return [tabDocumentController deleteRecordWithName: recordIdentifier];
}

// Deletes all objects for the given entity name. Returns YES if successful, NO otherwise.
- (BOOL)deleteAllObjectsForEnityName:(NSString *)entityName {

	
	NSArray* documents = [tabDocumentController documents] ;
	
	NSEnumerator *en = [documents objectEnumerator];
	MiniDocument* obj;
	while( (obj = [en nextObject]) != nil )
	{
		[tabDocumentController destroy: obj];
	}
	
	
	
	return YES;	
}

// Returns YES if the dataSource changed, NO otherwise.
- (BOOL)hasChanges
{
	return [[ tabDocumentController changedRecordsToSync] count] + 
	[[ tabDocumentController deletedRecordsToSync] count];
}


// Saving and Loading Methods

// Saves the sync source to disk. Returns YES if successful, NO otherwise.
- (BOOL)saveSource
{
	[[tabDocumentController documents] makeObjectsPerformSelector:@selector( saveIfNeeded ) ];
	return YES;
}

/*
// Writes the sync source to the specified fie URL. Returns YES if successful, NO otherwise.
- (BOOL)_writeToURL:(NSURL *)url
{
	NSData *myData = [self _dataRepresentation];
	if (![myData writeToURL:url atomically:YES])
		return NO;
	return YES;
}*/

// Reads the sync source in from the specified file URL. Returns YES if successful, NO otherwise.
/*- (BOOL)_readFromURL:(NSURL *)url
{
	NSData *myData = [[NSData alloc] initWithContentsOfURL:url];
	if (myData == nil)
		return NO;
	
	[self _loadDataRepresentation:myData];
	[myData release];
	return YES;
}*/

// Returns an NSData representation of the receiver for saving to disk. Encodes the data source and the 
// preferred sync mode.
/*
- (NSData *)_dataRepresentation
{
	// Convert the DataSource object dictionary to an NSData for archiving because it can contain
	// unsupported property list types, like instances of NSCalendarDate and custom entities.
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSArchiver archivedDataWithRootObject:[_myDataSource representation]], @"data source",
		[NSNumber numberWithInt:[_syncIt preferredSyncMode]], @"sync mode", nil];
	NSData * data = (NSData *)CFPropertyListCreateXMLData(kCFAllocatorDefault, (CFPropertyListRef)dict);
	return [data autorelease];
}*/

// Decodes the receiver from the given data that contains the data source and preferred sync mode.
// Returns YES if successful, NO otherwise.
/*- (BOOL)_loadDataRepresentation:(NSData *)data {
	NSString *error;
	NSDictionary *dict = (NSDictionary *)CFPropertyListCreateFromXMLData(kCFAllocatorDefault, (CFDataRef)data, 
																		 kCFPropertyListMutableContainersAndLeaves, 
																		 (CFStringRef *)&error);
	// Converts the NSData back into a DataSource representation that may contain non-property list types.
	[_myDataSource setRepresentation:[NSUnarchiver unarchiveObjectWithData:[dict objectForKey:@"data source"]]];
	int syncMode = [[dict objectForKey:@"sync mode"] intValue];
	[_syncIt setPreferredSyncMode:syncMode];
    return YES;
}*/


// Reverts the data source to the last saved state. Returns YES if successful, NO otherwise.
- (BOOL)revertSource
{
	[tabDocumentController readDocumentsFromPersistentStore];
	[syncField insertText: @"\nReverted to the saved file."];

	return YES;
}

// SyncingSource callback method that is invoked during a sync session when the client has finished pushing records.
- (void)clientDidFinishPushing:(ISyncClient *)client
{
	return;
}

// SyncingSource callback method that is invoked during a sync session when the client has finished pulling records.
- (void)clientDidFinishPulling:(ISyncClient *)client
{
	return;
}

// Invoked at the end of the sync session. If success is false, then error is set to the reason why.
// For example, you may implement this method to perform some computations based on any pulled records.
- (void)client:(ISyncClient *)client didFinishSyncingWithSuccess:(BOOL)success error:(NSError *)error
{
	return;
}


//
// Syncing Methods
//

// Saves the receiver's records to disk by invoking saveSource.
- (BOOL)_save {
	return [self saveSource];
}

// Syncs the reciever's records using the SyncIt object.
- (BOOL)_sync
{
	[syncField insertText: @"\n\nBEGIN SYNCING..."];

	// Start the progress indicator.
	NSProgressIndicator* progressIndicator = syncIndicator;
	[progressIndicator setUsesThreadedAnimation:YES];
	[progressIndicator setHidden:NO];
	[progressIndicator startAnimation:self];
	[progressIndicator display];
	
	
	BOOL saveSuccess = [self _save];
	if (!saveSuccess)
	{
		[syncField insertText: @"\nFailed to save before syncing."];

	}

	// Sync the receiver's changes.
	BOOL syncSuccess = [_syncIt syncIt];
	
	
	// Clear the changes so they are not synced the next time.
	if (syncSuccess == YES)
		[tabDocumentController clearAllChanges];
	 
	
	// Stop the progress indicator
	[progressIndicator stopAnimation:self];
	[progressIndicator setHidden:YES];
	
	[syncField insertText: @"\nEND SYNCING\n\n"];

	return syncSuccess;
}


/*
// Saves the receiver's records to disk whenever the user changes data.
- (void) _handleSaveRequestNotification:(NSNotification*)notification {
	[[NSRunLoop currentRunLoop] performSelector:@selector(saveAction:) 
										 target:self argument:self order:1000 modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
}*/

// Joins the current sync session.
- (void)_client:(ISyncClient *)client mightWantToSyncEntityNames:(NSArray *)entityNames
{
	//sync
	if( preferenceIntValueForKey(@"syncOption") == 2 )
	{
		[syncField insertText: @"\n_client mightWantToSyncEntityNames"];
		
		[self syncAction:self];
	}	
}


//
// Persistent Data Methods
// 

//Returns the receiver's entity model.
/*- (id)entityModel
{
    if (_entityModel != nil) 
		return _entityModel;
	NSLog(@"Loading the entity model...");
	_entityModel = [self _loadEntityModelFromFile:[DataSource defaultEntityModelFileName]];
	    
    return _entityModel;
}

// Loads the entity model for the first time. Used by the init method.
- (id)_loadEntityModelFromFile:(NSString *)fileName
{
	id resourcePath = [[NSBundle mainBundle] resourcePath];
	return [[EntityModel alloc] initWithContentsOfFile:[resourcePath stringByAppendingPathComponent:fileName]];
}

// Returns the receiver's data source. Attempts to load it from disk if it doesn't exist.
- (id)dataSource 
{
    if (_myDataSource != nil)
        return _myDataSource;
	
	NSLog(@"Creating data source...");
	_myDataSource = [[DataSource alloc] initWithModel:[self entityModel]];

	// Load the data source content (tables and records) from the backup file, if it exists
	NSString *applicationSupportFolder = [self _applicationSupportFolder];
	NSLog(@"Initializing with data from file %@", [applicationSupportFolder stringByAppendingPathComponent:[self fileName]]);
	NSURL *url = [NSURL fileURLWithPath:[applicationSupportFolder stringByAppendingPathComponent:[self fileName]]];
	if ([self _readFromURL:url] == NO) {
		NSLog(@"No backup file, requesting a refresh sync.");
		//[_syncIt setPreferredSyncMode:RefreshSyncMode];		
	}
	else {
		NSLog(@"Read in some data on init from backup file");
	}
	
    return _myDataSource;
}

// Returns the receiver's application support folder that contains the saved records.
- (NSString *)_applicationSupportFolder {
    NSString *applicationSupportFolder = nil;
    FSRef foundRef;
    OSErr err = FSFindFolder(kUserDomain, kApplicationSupportFolderType, kDontCreateFolder, &foundRef);
    if (err != noErr) {
        NSRunAlertPanel(@"Alert", @"Can't find application support folder", @"Quit", nil, nil);
        [[NSApplication sharedApplication] terminate:self];
    } else {
        unsigned char path[1024];
        FSRefMakePath(&foundRef, path, sizeof(path));
        applicationSupportFolder = [NSString stringWithUTF8String:(char *)path];
        applicationSupportFolder = [applicationSupportFolder stringByAppendingPathComponent:[self folderName]];
    }
    return applicationSupportFolder;
}

// Returns the name of the folder contained in the User's Application Support folder.
- (NSString *)folderName
{
	return @"SyncExamples";
}

// Returns the file name that contains the receiver's records.
- (NSString *)fileName
{
	return @"com.mycompany.SimpleStickies.xml";
}*/

/*
// Action method that saves the receiver's records to disk.
- (IBAction) saveAction:(id)sender {
	NSLog(@"invoking saveAction:...");
	(void) [self _save];
}
*/
// Action method that syncs the receiver's records.
- (IBAction) syncAction:(id)sender {
	(void) [self _sync];
}

// NSApplication delegate method that saves the records before terminating the application.
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    int reply = NSTerminateNow;
    
	/*
    if (_myDataSource != nil) {
		[self _save];
    }*/
	
    return reply;
}


// Deletion Support

// Returns the receiver's object that matches the specified record identifier. Typically, used to
// get a record that will be deleted.
/*- (id)objectForRecordIdentifier:(NSString *)recordIdentifier
{
	NSEnumerator *entityEnumerator = [[self entityNames] objectEnumerator];
	NSString *entityName;
	id anObject = nil;
	
	entityName = [entityEnumerator nextObject];
	while ((anObject == nil) && (entityName != nil)){
		anObject = [[self dataSource] recordWithPrimaryKey:recordIdentifier forEntityName:entityName];
		entityName = [entityEnumerator nextObject];
	}
	return anObject;
}

// Deletes the record corresponding to the specified record identifier. Typically, invoked when pulling
// deletions from the sync engine.
- (BOOL)deleteRecordForRecordIdentifier:(NSString *)recordIdentifier
{
	NSEnumerator *entityEnumerator = [[self entityNames] objectEnumerator];
	NSString *entityName;
	BOOL success = NO;
	
	entityName = [entityEnumerator nextObject];
	while ((success == NO) && (entityName != nil)){
		success = [[self dataSource] deleteRecordWithPrimaryKey:recordIdentifier forEntityName:entityName];
		entityName = [entityEnumerator nextObject];
	}
	return success;
}*/

@end

/*

if(utime([[[NSBundle mainBundle] bundlePath] fileSystemRepresentation], nil) == -1)
{
	NSLog(@"DEBUG: utime on bundlePath failed.");
	exit(0);
}

theTask = [[NSTask alloc] init];
[theTask setLaunchPath:@"/usr/bin/open"];
[theTask setArguments:[NSArray arrayWithObject:[[NSBundle mainBundle] bundlePath]]];
[theTask launch];
exit(0);
 */
