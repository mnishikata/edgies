//
//  MNZip.m
//  SampleApp
//
//  Created by __Name__ on 06/04/05.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//




#import "MNZip.h"

#import "zlib.h"


@implementation MNZip


+(NSData*)zip:(id)raw //raw is nsdata or nsfilewrapper
{
	
	
	NSString* sourceFilename = [NSString stringWithFormat:@".ts%f.mnzipdata", [NSDate  timeIntervalSinceReferenceDate ]];
	
	NSString* targetFilename = [NSString stringWithFormat:@".tt%f.mnzipdata", [NSDate  timeIntervalSinceReferenceDate ]];
	
	
	NSString* sourcePath =[NSTemporaryDirectory() stringByAppendingPathComponent: sourceFilename];
	NSString* targetPath =[NSTemporaryDirectory() stringByAppendingPathComponent: targetFilename];
	
	
	BOOL flag;
	
	
	if( [raw isKindOfClass:[NSData class]] )
		flag = [raw writeToFile:sourcePath atomically:YES];
	
	
	else 	if( [raw isKindOfClass:[NSFileWrapper class]] )
		flag = [raw writeToFile:sourcePath atomically:YES updateFilenames:YES];
	
	else return nil;
	
	if( flag == NO )
		return NULL;
	
	/* Assumes sourcePath and targetPath are both
		valid, standardized paths. */
	
	
	
	//----------------
	// Create the zip task
	NSTask * backupTask = [[NSTask alloc] init];
	[backupTask setLaunchPath:@"/usr/bin/ditto"];
	[backupTask setArguments:
		[NSArray arrayWithObjects:@"-c",/* @"-k",*/ @"-X", @"--rsrc", 
			sourcePath, targetPath, nil]];
	
	// Launch it and wait for execution
	[backupTask launch];
	[backupTask waitUntilExit];
	
	// Handle the task's termination status
	if ([backupTask terminationStatus] != 0)
		return NULL;
	
	// You *did* remember to wash behind your ears ...
	// ... right?
	[backupTask release];
	
	
	NSData* convertedData = [[[NSData alloc] initWithContentsOfFile:targetPath] autorelease];
	
	//delete scratch
	
	[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
												 source:NSTemporaryDirectory()
											destination:@"" 
												  files:[NSArray arrayWithObjects:sourceFilename, targetFilename,NULL]
													tag:NULL];
	
	
	return convertedData;
}




+(BOOL)zipFromPath:(NSString*)sourcePath toPath:(NSString*)targetPath 
{
	
	
	
	
	
	
	NSFileManager* fm = [NSFileManager defaultManager];
	if( ! [fm fileExistsAtPath:sourcePath] ) return nil;
	
	
	
	//----------------
	// Create the zip task
	NSTask * backupTask = [[NSTask alloc] init];
	[backupTask setLaunchPath: (NSString*)CFSTR("/usr/bin/ditto") ];
	
	
	[backupTask setArguments:
		[NSArray arrayWithObjects: (NSString*)CFSTR("-c"), /*(NSString*)CFSTR("-k"), */
			(NSString*)CFSTR("-X"), (NSString*)CFSTR("--rsrc"), 
			sourcePath, targetPath, nil]];
	


		
	
	// Launch it and wait for execution
	[backupTask launch];
	[backupTask waitUntilExit];
	
	// Handle the task's termination status
	if ([backupTask terminationStatus] != 0)
		return NO;
	
	// You *did* remember to wash behind your ears ...
	// ... right?
	[backupTask release];
	
	
	
	return YES;
	
}

+(BOOL)writeData:(NSData*)data inPackage:(NSString*)packagePath forFile:(NSString*)filename
{
	NSFileManager *fm = [NSFileManager defaultManager];
	BOOL isDir;
	if( ![fm fileExistsAtPath:packagePath isDirectory:&isDir] )
	{
		
		[fm createDirectoryAtPath:packagePath attributes:nil];
		
	}else if( !isDir )
	{
		[fm removeFileAtPath:packagePath handler:nil];
		[fm createDirectoryAtPath:packagePath attributes:nil];

	}
	
	NSString* targetPath = [packagePath stringByAppendingPathComponent: filename];

	return [data writeToFile:targetPath atomically:YES];
	
}
+(BOOL)existsFileInPackage:(NSString*)packagePath forFile:(NSString*)filename
{
	NSFileManager *fm = [NSFileManager defaultManager];
	BOOL isDir;
	if( ![fm fileExistsAtPath:packagePath
				  isDirectory:&isDir] ) return NO;
	
	if( !isDir ) return NO;
	
	NSString* targetPath = [packagePath stringByAppendingPathComponent: filename];
	
	return [fm fileExistsAtPath: targetPath];
}

+(NSData*)readDataInPackage:(NSString*)packagePath forFile:(NSString*)filename
{
	NSFileManager *fm = [NSFileManager defaultManager];
	BOOL isDir;
	if( ![fm fileExistsAtPath:packagePath
				  isDirectory:&isDir] ) return nil;
	
	if( !isDir ) return nil;
	
	NSString* targetPath = [packagePath stringByAppendingPathComponent: filename];
	
	if( ![fm fileExistsAtPath: targetPath] ) return nil;
	
	NSData* data = [[[NSData alloc] initWithContentsOfFile: targetPath] autorelease];
	
	return data;
	
}


+(NSFileWrapper*)readZipAtPath:(NSString*)fullPath onlyThisFile:(NSString*)filename
{

	
		
		
	
	//NSString* sourceFilename = [fullPath lastPathComponent];
	NSString* targetFilename = [NSString stringWithFormat: (NSString*)CFSTR(".%f.mnzipdata"), [NSDate  timeIntervalSinceReferenceDate ]];

	NSString* sourcePath =fullPath;
	NSString* targetPath =[NSTemporaryDirectory() stringByAppendingPathComponent: targetFilename];
	

	
	
	//Unzip
	
	//-------------------
	NSTask *cmnd=[[NSTask alloc] init];
	[cmnd setLaunchPath: (NSString*)CFSTR("/usr/bin/ditto") ];
	[cmnd setArguments:[NSArray arrayWithObjects:
		/*(NSString*)CFSTR("-v"),*/ (NSString*)CFSTR("-x"),
		/*(NSString*)CFSTR("-k"),*/ (NSString*)CFSTR("--rsrc"),sourcePath,targetPath,nil]];
	[cmnd launch];
	[cmnd waitUntilExit];
	
	// Handle the task's termination status
	if ([cmnd terminationStatus] != 0)
		return NULL;
	
	// You *did* remember to wash behind your ears ...
	// ... right?
	[cmnd release];
	
	
	

	//
	
	NSArray* contents = [[NSFileManager defaultManager] directoryContentsAtPath:targetPath];
	
	
	NSFileWrapper* wrapper = nil;
	
	if( [contents count] == 1 )
	{
		NSString* onepath;
		onepath = [targetPath stringByAppendingPathComponent:[contents lastObject]];
		
		NSData* data = [NSData dataWithContentsOfFile:onepath];
		
		wrapper = [[[NSFileWrapper alloc] initRegularFileWithContents:data  ] autorelease];
		
		
	}
	else	if( [contents count] > 1 )
	{
		
		wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
		
		unsigned hoge;
		for( hoge = 0; hoge < [contents count]; hoge++ )
		{
			NSString* onepath;
			NSString* onefilename;
			
			onefilename = [contents objectAtIndex:hoge];
			
			if( !filename || (filename && [filename isEqualToString: 	onefilename] ))
			{

				onepath = [targetPath stringByAppendingPathComponent:onefilename];
				
				NSData* data = [NSData dataWithContentsOfFile:onepath];
				
				[wrapper addRegularFileWithContents:data   preferredFilename:onefilename ];
				
			}
			
		}
	}
	
	
	
	//delete scratch
	
	[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
							 source:[targetPath stringByDeletingLastPathComponent]
						destination:(NSString*)CFSTR("") 
							  files:[NSArray arrayWithObjects: targetFilename,NULL]
								tag:NULL];
	
	
	return wrapper;
}


/*
+(NSFileWrapper*)unzip:(NSData*)zipData
{
	NSString* targetFilename = [NSString stringWithFormat: (NSString*)CFSTR(".%f.mnzipdata"), [NSDate  timeIntervalSinceReferenceDate ]];
	NSString* sourceFilename = [NSString stringWithFormat: (NSString*)CFSTR(".%f.mnzipdata"), [NSDate  timeIntervalSinceReferenceDate ]];
	
	NSString* sourcePath =[applicationSupportFolder() stringByAppendingPathComponent: sourceFilename];
	NSString* targetPath =[applicationSupportFolder() stringByAppendingPathComponent: targetFilename];
	
	
	
	BOOL flag = [zipData writeToFile:sourcePath atomically:YES];
	
	if( flag == NO )
		return NULL;
	
	
	//Unzip
	
	//-------------------
	NSTask *cmnd=[[NSTask alloc] init];
	[cmnd setLaunchPath: (NSString*)CFSTR("/usr/bin/ditto") ];
	[cmnd setArguments:[NSArray arrayWithObjects:
		 (NSString*)CFSTR("-x"), (NSString*)CFSTR("-k"), (NSString*)CFSTR("--rsrc"),sourcePath,targetPath,nil]];
	[cmnd launch];
	[cmnd waitUntilExit];
	
	// Handle the task's termination status
	if ([cmnd terminationStatus] != 0)
		return NULL;
	
	// You *did* remember to wash behind your ears ...
	// ... right?
	[cmnd release];
	
	
	
	//unzip‚Äû√Ñ√Ñ(1)‚Äû√â√Ø‚Äû√á¬∞‚Äû√á¬ß‚Äû√â¬¥‚Äû√Ö√•‚Äû√á‚àè‚Äû√â√â‚Äû√â√≥‚Äû√Ö√Ø‚Äû√á√•‚Äû√Ö¬∂‚Äû√Ö√ë‚Äû√Ö√º√Ç‚Ä†¬•√Ç√™√†‚Äû√Ö√ò‚Äû√Ñ√Ö‚Äû√â√Ø‚Äû√á¬©‚Äû√â¬¥‚Äû√â√Ñ‚Äû√Ö¬¥‚Äû√Ö‚â§‚Äû√Ö¬Æ‚Äû√Ö¬ß‚Äû√Ö‚Ä†‚Äû√Ö√´‚Äû√Ñ√Ñ(2)‚Äû√â√Ø‚Äû√á¬©‚Äû√â¬¥‚Äû√â√Ñ‚Äû√Ö√Ü√Ç‚Ä†¬•√Ç√™√†‚Äû√Ö√ò‚Äû√Ñ√Ö‚Äû√Ö√π‚Äû√Ö√Ü‚Ä∞‚àè‚â†√ã‚à´¬¥‚Äû√Ñ√á
	
	//
	
	NSArray* contents = [[NSFileManager defaultManager] directoryContentsAtPath:targetPath];
	
	
	NSFileWrapper* wrapper;
	
	if( [contents count] == 1 )
	{
		NSString* onepath;
		onepath = [targetPath stringByAppendingPathComponent:[contents lastObject]];
		
		NSData* data = [NSData dataWithContentsOfFile:onepath];
		
		wrapper = [[[NSFileWrapper alloc] initRegularFileWithContents:data  ] autorelease];
		
		
	}
	else	if( [contents count] > 1 )
	{
		
		wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
		
		unsigned hoge;
		for( hoge = 0; hoge < [contents count]; hoge++ )
		{
			NSString* onepath;
			NSString* onefilename;
			
			onefilename = [contents objectAtIndex:hoge];
			onepath = [targetPath stringByAppendingPathComponent:onefilename];
			
			NSData* data = [NSData dataWithContentsOfFile:onepath];
			
			[wrapper addRegularFileWithContents:data   preferredFilename:onefilename ];
		}
	}
	
	
	
	//delete scratch
	
	[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
												 source:applicationSupportFolder()
											destination:(NSString*)CFSTR("") 
												  files:[NSArray arrayWithObjects:sourceFilename, targetFilename,NULL]
													tag:NULL];
	
	
	return wrapper;
}
*/

/*
 - (IBAction)open:(id)sender {
	 
	 NSString* text = @"test";
	 
	 NSData* data = [text dataUsingEncoding:NSUTF8StringEncoding];
	 
	 
	 
	 //zip
	 
	 
	 NSData* convertedData = [self zip:data];
	 
	 [convertedData writeToFile:[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/zip result"] atomically:YES];
	 
	 
	 
	 
	 // zip 2
	 
	 NSFileWrapper* zipwrap = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	 
	 [zipwrap addRegularFileWithContents:data   preferredFilename:@"data1" ];
	 [zipwrap addRegularFileWithContents:data   preferredFilename:@"data2" ];
	 
	 NSData* convertedData = [self zip:zipwrap];
	 
	 [convertedData writeToFile:[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/zip result"] atomically:YES];
	 
	 
	 
	 
	 // unzip
	 
	 NSFileWrapper* wrapper = [self unzip:convertedData];
	 
	 if( [wrapper isRegularFile] )
	 {
		 //NSLog(@"this is regular file wrapper");
		 
		 
	 }
	 else
	 {
		 //NSLog(@"this is directory wrapper");
		 
		 
		 NSDictionary* regularFileWrappers = [wrapper fileWrappers];
		 
		 //NSLog(@"contains %@", [[regularFileWrappers allKeys] description] );
		 
	 }
	 
	 [wrapper  writeToFile:[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/unzip result"] atomically:YES updateFilenames:YES];
	 
 }
 
 
 
 */
@end
