//
//  FindResultTable.h
//  SampleApp
//
//  Created by __Name__ on 06/04/10.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MemoManagerTableView : NSTableView {

}
+ (void)initialize ;
-(void)drawRect:(NSRect)rect;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)addColumnWithIdentifier:(NSString*)identifier title:(NSString*)title bindToObject:(id)object ;
-(void)removeColumnWithIdentifier:(NSString*)identifier;
-(void)saveTableColumn;
-(NSString*)convertMetaAttributeNameToMiniDocumentKey:(NSString*)meta;
-(IBAction)tearOff:(id)sender;
-(IBAction)export:(id)sender;
-(IBAction)discard:(id)sender;
-(IBAction)showProperty:(id)sender;
-(IBAction)menuItemSelected:(id)sender;
-(IBAction)print:(id)sender;
-(void)changeColor:(id)sender;

@end
