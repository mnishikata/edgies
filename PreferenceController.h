/* PreferenceController */

#import <Cocoa/Cocoa.h>
#import "LinkTextView.h"
#import "NDAlias.h"
#import "NSTextView (coordinate extension).h"

//preferencePanelShowMoreOptions

//NSString* emptyOrValue(id something);

 
//extern id			APP_graphicInspectorPanel();


@interface PreferenceController : NSObject
{
	
	NSMutableDictionary* temporaryPreferencesStore;

	//////////
	//IBOutlet id defaultFont;
	//IBOutlet id pageNumberFormat;

	
	//IBOutlet id header;
	
	IBOutlet id defaultColorMenu; //used only setting menu at starting
	IBOutlet id tagTextView;
	/////
	//


	
	///registratin
	IBOutlet id registrationTab;
	IBOutlet id reg_name;
	IBOutlet id reg_nameForDisplay;
	IBOutlet id reg_sn;
	IBOutlet id reg_snForDisplay;
	IBOutlet id registrationButton;
	
	
	id crypto;
	
	
	IBOutlet id additionalUDArrayController;
	NSMutableArray* additionalUDArrayInLocal;
	NSString* additionalUDControllerFilterPredicate;
	
	// ui
	
	BOOL preferencePanelShowMoreOptions;
	
	IBOutlet id panel;
	
	IBOutlet id preferencesTab;
	
	
	IBOutlet id _tab1_height;
	IBOutlet id _tab1_height_plus;
	
	IBOutlet id _tab2_height;
	IBOutlet id _tab2_height_plus;
	
	IBOutlet id _tab3_height;
	IBOutlet id _tab3_height_plus;
	
	IBOutlet id _tab4_height;
	IBOutlet id _tab5_height;
	IBOutlet id _tab6_height;
	IBOutlet id _tab7_height;	
	IBOutlet id _tab8_height;	
	IBOutlet id _tab9_height;	
	
	IBOutlet id _tab10_height;
	IBOutlet id _tab10_height_plus;
}
+(PreferenceController*)standardUserDefaults;
+(void)initialize;
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
- (NSResponder *)nextResponder;
- (id) init ;
-(void)dealloc;
- (IBAction)revertToDefault:(id)sender;
- (IBAction)buttonClicked:(id)sender;
-(IBAction)showPreferencePanel:(id)sender;
-(void)setColorMenu;
- (void)setValue:(id)value forUndefinedKey:(NSString *)key;
- (void)setValue:(id)value forKey:(NSString *)key;
-(id)valueForKey:(NSString*)key;
-(void)popTemporaryPreferencesValues;
-(void)pushTemporaryPreferencesValues;
- (void)apply;
- (void)setNilValueForKey:(NSString *)key;
-(IBAction)defaultColorMenuChanged:(id)sender;
-(void) registerAdditionalUD:(NSArray*) array;

-(int)metricsTag;
-(void)closePanel;
- (NSArray*)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar;
- (NSArray*)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar;
- (NSArray*)toolbarSelectableItemIdentifiers:(NSToolbar*)toolbar;
- (NSToolbarItem*)toolbar:(NSToolbar*)toolbar 
			itemForItemIdentifier:(NSString*)itemId 
		willBeInsertedIntoToolbar:(BOOL)willBeInserted;
-(void)adjustViewHeight;
- (void)_showTab:(id)sender;
-(void)frameChangeTo:(NSRect)newFrame;
- (BOOL)textShouldEndEditing:(NSText *)textObject;


@end


