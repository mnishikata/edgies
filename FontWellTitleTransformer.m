//
//  PointToMilTransformer.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "FontWellTitleTransformer.h"
/*
 
 
 
 */

@implementation FontWellTitleTransformer

+(Class)transformedValueClass{
	return [NSString class];
}

+(BOOL)allowsReverseTransformation{
	return NO;
}




-(id)transformedValue:(id)value{
	
//value = dictionary  	
	
	//NSLog(@"transformer %@",value);

	
	
	NSString* name = [value objectForKey:@"name"];
	float size = [[value objectForKey:@"size"] floatValue];
	
	return [NSString stringWithFormat:@"%@ %.1f",name,size]; 
}

/*
-(id)reverseTransformedValue:(id)value{
	
	float mil = [value floatValue];
	
	int tag = preferenceIntValueForKey(@"metricsTag") ;
	
	
	if( tag == 0  ) // mm
		return [NSNumber numberWithFloat: mil * 2.83 ];
	
	if( tag == 1 ) // inch
		return  [NSNumber numberWithFloat: mil * 72.0 ];
	
	return [NSNumber numberWithFloat: mil ]; // point
}
*/

@end
