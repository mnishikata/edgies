//
//  FileEntityTextAttachmentCell.m
//  fileEntityTextView
//
//  Created by __Name__ on 06/08/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

//#import "MovieInspector.h"
#import "FileLibrary.h"
#import "NSString (extension).h"
#import "MovieAttachmentCell.h"
#import "MyMovieView.h"


extern BOOL UD_DELETE_CELLS_AFTER_DRAGGING_OUT;
extern BOOL UD_CELLS_DRAG_OUT_AS_ALIAS;

#define CONTROL_SIZE NSMakeSize(150,16)

@class MNTabWindow;

@implementation MovieAttachmentCell




+(NSAttributedString*)movieAttachmentCellAttributedStringWithPasteboard:(NSPasteboard*)pb
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MovieAttachmentCell* aCell = [[[MovieAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	

	[anAttachment setAttachmentCell:aCell ];
		
	
	[wrapper setFilename:@"file.quicktimecontrolbar"];
 	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		

		
		
		
	[aCell readMovieFromPasteboard:pb];
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


+(NSAttributedString*)movieAttachmentCellAttributedStringWithFileReference:(NSString*)path
{
	QTDataReference *ref = [QTDataReference dataReferenceWithReferenceToFile:path];
	
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MovieAttachmentCell* aCell = [[[MovieAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	
	[anAttachment setAttachmentCell:aCell ];
	
	
	[wrapper setFilename:@"file.quicktimecontrolbar"];
 	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		
		
		
		
		
	[aCell readMovieFromFileRefernce:path];
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


+(NSAttributedString*)movieAttachmentCellAttributedStringWithFileData:(NSString*)path
{
	QTDataReference *ref = [QTDataReference dataReferenceWithReferenceToFile:path];
	
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	MovieAttachmentCell* aCell = [[[MovieAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	
	[anAttachment setAttachmentCell:aCell ];
	
	
	[wrapper setFilename:@"file.quicktimecontrolbar"];
 	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		
		
		
		
	
	[aCell readMovieFromFileData:path];
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


#pragma mark -


- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView characterIndex:(unsigned)charIndex
{

	//NSLog(@"drawWithFrame");

	
	if( ([qtview superview] != aView) && ( [[aView window] isKindOfClass: [MNTabWindow class]] ) )
	{
		[aView addSubview:qtview];
	}
	
	if([qtview superview] == aView)
		[qtview setFrame:cellFrame];

	
}



- (id)initWithAttachment:(NSTextAttachment*)attachment
{
    self = [super init];
    if (self) {
		
		[self setAttachment:attachment];


		qtmov = nil;
		qtview = nil;
		qtsize = CONTROL_SIZE; 

		
		[self setTitle: NSLocalizedString(@"Movie",@"")];

		//[self setTarget:self];
		//[self setAction:@selector(openDialogue)];
	}
    return self;
}



-(void)readMovieFromPasteboard:(NSPasteboard*)pb
{
	/*
	NSData *data = [pb dataForType: QTMoviePasteboardType];
	QTDataReference *ref = [QTDataReference dataReferenceWithReferenceToData:data ];
	
	
	QTMovie *mov = [[QTMovie alloc] initWithDataReference:ref error:nil];
	*/
	
	QTMovie *mov = [[QTMovie alloc] initWithPasteboard:pb error:nil];
	//NSLog(@"reading mov");

	if( mov )
	{
		//NSLog(@"Success");

		[qtmov release];
		qtmov = mov;
		
		
		//NSLog(@"%@",[[qtmov movieAttributes] description]);
		// setup movie view
		NSDictionary* attributes = [qtmov movieAttributes];
		
		
		if( [[attributes objectForKey: QTMovieHasVideoAttribute] boolValue] )
		{
			qtsize = [[attributes objectForKey:QTMovieNaturalSizeAttribute] sizeValue];

		}
		
		NSRect frame = NSZeroRect;
		frame.size = qtsize;
		
		//NSLog(@"size %@", NSStringFromSize(qtsize));

		
		
		
		qtview = [[MyMovieView alloc] initWithFrame: frame];
		
		//[qtview setShowsResizeIndicator:YES];
		[qtview setMovie:qtmov];
		

		[qtmov setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieEditableAttribute];

		
		//NSLog(@"qtview %d",( qtview? YES:NO));
		
	}

	
}

-(void)readMovieFromFileRefernce:(NSString*)path
{
	QTDataReference *ref = [QTDataReference dataReferenceWithReferenceToFile:path];

	QTMovie *mov = [[QTMovie alloc] initWithDataReference:ref error:nil];


	
	if( mov )
	{
		
		[qtmov release];
		qtmov = mov;
		
		
		//NSLog(@"%@",[[qtmov movieAttributes] description]);
		// setup movie view
		NSDictionary* attributes = [qtmov movieAttributes];
		
		
		if( [[attributes objectForKey: QTMovieHasVideoAttribute] boolValue] )
		{
			qtsize = [[attributes objectForKey:QTMovieNaturalSizeAttribute] sizeValue];
			
		}
		
		NSRect frame = NSZeroRect;
		frame.size = qtsize;
		
		//NSLog(@"size %@", NSStringFromSize(qtsize));
		
		
		
		
		qtview = [[MyMovieView alloc] initWithFrame: frame];
		
		//[qtview setShowsResizeIndicator:YES];
		[qtview setMovie:qtmov];
		
		
		[qtmov setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieEditableAttribute];
		
		
		//NSLog(@"qtview %d",( qtview? YES:NO));
		
	}
	
	
}

-(void)readMovieFromFileData:(NSString*)path
{
	
	QTMovie *mov = [[QTMovie alloc] initWithFile:path  error:nil];
	
	
	
	if( mov )
	{
		
		[qtmov release];
		qtmov = mov;
		
		
		//NSLog(@"%@",[[qtmov movieAttributes] description]);
		// setup movie view
		NSDictionary* attributes = [qtmov movieAttributes];
		
		
		if( [[attributes objectForKey: QTMovieHasVideoAttribute] boolValue] )
		{
			qtsize = [[attributes objectForKey:QTMovieNaturalSizeAttribute] sizeValue];
			
		}
		
		NSRect frame = NSZeroRect;
		frame.size = qtsize;
		
		//NSLog(@"size %@", NSStringFromSize(qtsize));
		
		
		
		
		qtview = [[MyMovieView alloc] initWithFrame: frame];
		
		//[qtview setShowsResizeIndicator:YES];
		[qtview setMovie:qtmov];
		
		
		[qtmov setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieEditableAttribute];
		
		
		//NSLog(@"qtview %d",( qtview? YES:NO));
		
	}
	
	
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	if( qtmov )
	{
		BOOL qtloop = [[qtmov attributeForKey:QTMovieLoopsAttribute] boolValue];
		BOOL qtplayselection = [[qtmov attributeForKey:QTMoviePlaysSelectionOnlyAttribute] boolValue];

		id movieRep = [qtmov movieFormatRepresentation];
		
		if( movieRep )
		{
		
			[encoder encodeObject:movieRep forKey:@"MovieAttachmentCell_qtmov"];
			[encoder encodeBool:qtloop forKey:@"MovieAttachmentCell_qtloop"];
			[encoder encodeBool:qtplayselection forKey:@"MovieAttachmentCell_qtplayselection"];

			[encoder encodeObject:NSStringFromSize(qtsize) forKey:@"MovieAttachmentCell_qtsize"];

			
			[qtview removeFromSuperview];
		
		}else
		{
			NSLog(@"Saving error! One of the movies is empty.");
		}
	}
	
	

    return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		//NSLog(@"initWithCoder");
		//[self setAttachment:attachment];
		
		
		qtmov = nil;
		qtview = nil;
		qtsize = CONTROL_SIZE; 
		
		
		[self setTitle: NSLocalizedString(@"Movie",@"")];
		
		
		if( [coder containsValueForKey:@"MovieAttachmentCell_qtmov"] )
		{
			NSData *data = [coder decodeObjectForKey:@"MovieAttachmentCell_qtmov"] ;
			
			if( data )
			{
				qtmov = [QTMovie movieWithData:data error:nil];
				if( qtmov )
				{
					[qtmov retain];
					
					
					qtsize = [[[qtmov movieAttributes] objectForKey:QTMovieNaturalSizeAttribute] sizeValue];
					NSRect frame = NSZeroRect;
					frame.size = qtsize;
					
					
					qtview = [[MyMovieView alloc] initWithFrame: frame];
					[qtview setShowsResizeIndicator:NO];

					[qtview setMovie:qtmov];
					
					[qtmov setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieEditableAttribute];
					
				}

			}else
			{
				NSLog(@"Failed to load Movie");
			}
		}else
		{
			NSLog(@"Movie data is missing");

		}
		
		if( [coder containsValueForKey:@"MovieAttachmentCell_qtloop"] )
		{
			BOOL qtloop = [coder decodeBoolForKey:@"MovieAttachmentCell_qtloop"];
			[qtmov setAttribute:[NSNumber numberWithBool:qtloop] forKey:QTMovieLoopsAttribute];
		}
		
		if( [coder containsValueForKey:@"MovieAttachmentCell_qtplayselection"] )
		{
			BOOL qtplayselection = [coder decodeBoolForKey:@"MovieAttachmentCell_qtplayselection"];
			[qtmov setAttribute:[NSNumber numberWithBool:qtplayselection] forKey:QTMoviePlaysSelectionOnlyAttribute];

		}
		
		
		if( [coder containsValueForKey:@"MovieAttachmentCell_qtsize"] )
		{
			NSString *str = [coder decodeObjectForKey:@"MovieAttachmentCell_qtsize"] ;
			qtsize = NSSizeFromString(str);
			
		}
		
		

	}
	return self;
	
	
}

-(void)dealloc
{
	[qtview release];
	[qtmov release];
	
	[super dealloc];
}

- (NSSize)cellSize
{
	return qtsize;
}


- (BOOL)wantsToTrackMouse
{
	return YES;	
}



- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView untilMouseUp:(BOOL)flag
{
	//NSLog(@"trackMouse");
	return YES;
}


#pragma mark -





-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <MovieAttachmentCellOwnerTextView> *)textView
{
	
 }



-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	//[[MovieInspector sharedMovieInspector]  modifyTagging:self];
}

#pragma mark -
#pragma mark @protocol AttachmentCellConverting 
-(int)stringRepresentation:(NSDictionary*)context
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:@" "];
	
	return 0;
}




-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:@" "];
	
	return 0;
	
}

-(int)attributedStringRepresentation:(NSDictionary*)context //withimage 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:@" "];
	
	return 0;
		
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	
	
	return nil;
}


-(NSImage*)imageRepresentation:(id)context
{

	
	return [NSImage imageNamed:@"r129"];
}

-(NSString*)writeToFileAt:(NSURL*)url 
{
	return nil;
}

-(BOOL)canAcceptDrop
{
	return NO;
}

-(void)removeSubview
{
	//NSLog(@"removeSubview");
	[qtview removeFromSuperview];
}

-(BOOL)removeFromTextView
{
	//NSLog(@"removeFromTextView");
	[qtview removeFromSuperview];
	return YES;
}


			  
@end
