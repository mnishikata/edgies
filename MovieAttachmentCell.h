//
//  AliasAttachmentCell.h
//  sticktotheedge
//
//  Created by __Name__ on 06/08/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ButtonAttachmentCell.h"

#import "AttachmentCellConverter.h"

#import <QTKit/QTKit.h>

@protocol MovieAttachmentCellOwnerTextView ;

@interface MovieAttachmentCell : NSTextAttachmentCell <AttachmentCellConverting>{

	QTMovie *qtmov;
	QTMovieView *qtview;

	NSSize qtsize;
	
}
+(NSAttributedString*)movieAttachmentCellAttributedStringWithPasteboard:(NSPasteboard*)pb;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView characterIndex:(unsigned)charIndex;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
-(void)readMovieFromPasteboard:(NSPasteboard*)pb;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
- (NSSize)cellSize;
- (BOOL)wantsToTrackMouse;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView untilMouseUp:(BOOL)flag;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <MovieAttachmentCellOwnerTextView> *)textView;
-(void)activateInspectorForTextView:(NSTextView  *)aTextView;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context;//withimage ;
-(NSDictionary*)pasteboardRepresentation:(id)context;
-(NSImage*)imageRepresentation:(id)context;
-(NSString*)writeToFileAt:(NSURL*)url ;
-(BOOL)canAcceptDrop;
-(BOOL)removeFromTextView;



@end

@protocol MovieAttachmentCellOwnerTextView 
//-(void)aliasAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)aliasAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)aliasAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)taggingAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)taggingAttachmentCellArrayInSelectedRange;
-(void)openTaggingInspector;


@end



/*
@protocol AttachmentCellConverting 
-(NSString*)stringRepresentation:(id)context;
-(NSAttributedString*)attributedStringWithoutImageRepresentation:(id)context; //without image 
-(NSAttributedString*)attributedStringRepresentation:(id)context; //without image 
-(NSImage*)imageRepresentation:(id)context;


@end
 */