//
//  MNZip.h
//  SampleApp
//
//  Created by __Name__ on 06/04/05.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MNZip : NSObject {

	
	
}

+(NSData*)zip:(id)raw; //raw is nsdata or nsfilewrapper;
+(BOOL)zipFromPath:(NSString*)sourcePath toPath:(NSString*)targetPath ;
+(BOOL)writeData:(NSData*)data inPackage:(NSString*)packagePath forFile:(NSString*)filename;
+(BOOL)existsFileInPackage:(NSString*)packagePath forFile:(NSString*)filename;
+(NSData*)readDataInPackage:(NSString*)packagePath forFile:(NSString*)filename;
+(NSFileWrapper*)readZipAtPath:(NSString*)fullPath onlyThisFile:(NSString*)filename;
+(NSFileWrapper*)unzip:(NSData*)zipData;
- (IBAction)open:(id)sender ;

@end
