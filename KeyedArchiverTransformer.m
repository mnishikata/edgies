//
//  PointToMilTransformer.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "KeyedArchiverTransformer.h"

@implementation KeyedArchiverTransformer

+(Class)transformedValueClass{
	return [NSData class];
}

+(BOOL)allowsReverseTransformation{
	return YES;
}




-(id)transformedValue:(id)value{
	

	return [NSKeyedArchiver archivedDataWithRootObject:value ]; //
}

-(id)reverseTransformedValue:(id)value{
	
	id obj;
	
	@try {
		obj = [NSKeyedUnarchiver unarchiveObjectWithData: value ];
	}
	@catch (NSException * e) {
		obj = nil;
	}

	
	return obj; // point
}


@end
