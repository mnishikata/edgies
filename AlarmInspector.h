/* ButtonInspector */

#import <Cocoa/Cocoa.h>
#import "AlarmAttachmentCell.h"

@interface AlarmInspector : NSObject
{
    IBOutlet id window;
	IBOutlet id dialogue;
	
	
	
	NSDate* alarmDate;
	int  alarmMode;

	NSString* alarmSoundPath;
	BOOL alarmLoop;
	
	NSArray* /*ButtonAttachmentCell**/ activeCells;
}

+(AlarmInspector*)sharedAlarmInspector;
- (id) init ;
-(BOOL)multipleSelection;
-(BOOL)noSelection;
-(void)showAlarmInspector;
-(id)imageData;
-(id)title;
-(NSString*)targetPath;
-(void)setTitle:(NSString*)title;
-(id)font;
-(void)setFont:(NSFont*)font;
-(id)buttonSize;
-(void)setButtonSize:(NSNumber*)size;
-(id)buttonPosition;
-(void)setButtonPosition:(NSNumber*)position;
-(id)showButtonTitle;
-(void)setShowButtonTitle:(id)showButtonTitle;
-(id)state;
-(void)setState:(NSNumber*)state;
-(int)alarmMode;
-(void)setAlarmMode:(int)mode;
-(id)showBezel;
-(void)setShowBezel:(id)showBezel;
-(void)dealloc;
-(BOOL)lock;
-(void)setLock:(BOOL)flag;
-(void)selectionChanged:(NSNotification*)notification;
-(void)updateInspector;
-(void)activateForTextView:(NSTextView  <AlarmAttachmentCellOwnerTextView> *)view;
-(IBAction)useAsDefault:(id)sender;
-(void)insertNewAlarmForTextView:(NSTextView*)textView;
-(IBAction)buttonClicked:(id)sender;
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void  *)contextInfo;



@end
