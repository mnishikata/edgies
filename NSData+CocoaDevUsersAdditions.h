#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

@interface NSData (NSDataExtension)

- (NSRange) rangeOfNullTerminatedBytesFrom:(int)start;
- (NSData *)inflate;
- (NSData *)deflate;

@end