//
//  NSWorkspace+ProcessUtil.h
//  Jikeiretsu
//
//  Created by __Name__ on 07/04/23.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#include <CoreFoundation/CoreFoundation.h>



CFArrayRef launchedProcessPaths(void);
int launchedProcessPIDForBundleIdentifier(CFStringRef bndlid); 
Boolean hasCustomIconFileAtPath(CFStringRef filepath);
int countForLaunchedProcessPIDForBundleIdentifier(CFStringRef bndlid);
Boolean executableIsRunning(CFStringRef name);
Boolean BSDProcessIsRunning(CFStringRef processName);
