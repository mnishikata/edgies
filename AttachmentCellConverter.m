//
//  MNAttributedString.m
//  sticktotheedge
//
//  Created by __Name__ on 07/05/22.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

//#import "NSAttributedString+AttachmentCellConverting.h"
#import "AttachmentCellConverter.h"
#import "AliasAttachmentCell.h"
#import "NDAlias.h"
#import "NDAlias+AliasFile.h"

#import "MNGraphicAttachmentCell.h"
#import "FileEntityTextAttachmentCell.h"
#import "FaviconAttachmentCell.h"
#import "MovieAttachmentCell.h"

#import "NSTextView (coordinate extension).h"
#import "PreferenceController.h"
#import "EdgiesLibrary.h"
#import "InstallLibrary.h"
#import "ClipboardArchiveAttachmentCell.h"
#import "KeyModifierManager.h"

#import <HIToolbox/CarbonEventsCore.h>
#import "ModifierKeyWell.h"
#import "SpotlightUtil.h"
#import <QTKit/QTKit.h>

#define EdgiesTextPboardType @"EdgiesTextPboardType"

/*  options */
/*
//drop in
#define UD_PASTE_PICTURE_FROM_FILE YES
#define UD_MAKE_BUTTON_FROM_FILE YES
#define UD_MAKE_ALIAS_BUTTON YES
#define UD_PASTE_FILE_ICON YES
#define UD_PASTE_FILENAME YES

#define UD_MAKE_URL_BUTTON YES
#define URL_BUTTON_HAS_TITLE NO
#define UD_URL_LOAD_FAVICON YES
#define UD_MAKE_URL_TEXT_LINK YES

//drag out
#define UD_FILES_PROMISE_PRIORITY YES
#define UD_CREATE_ALIAS_RATHER_THAN_MOVE NO
#define UD_DELETE_CELLS_AFTER_DRAGGING_OUT YES
*/

// pasting 
BOOL UD_PASTE_PICTURE_FROM_FILE	= YES;
BOOL UD_PASTE_AS_PLAIN_TEXT = NO; // new
BOOL UD_PASTE_AS_ARCHIVE = NO; // new
BOOL UD_FILE_CREATION_SOUND = YES;

BOOL UD_MAKE_BUTTON_FROM_FILE	= YES;
BOOL UD_MAKE_ALIAS_BUTTON	= YES;
BOOL UD_PASTE_FILE_ICON		= NO;
BOOL UD_PASTE_FILENAME		= NO;

//favicon button
BOOL UD_MAKE_FAVICON_BUTTON		= YES;
BOOL UD_FAVICON_HAS_TITLE	= NO;
BOOL UD_URL_LOAD_FAVICON	= YES;
BOOL UD_PASTE_URL_TEXT_LINK	= YES;

//drag out
BOOL UD_FILES_PROMISE_PRIORITY			= YES;
BOOL UD_DELETE_CELLS_AFTER_DRAGGING_OUT	= YES;
BOOL UD_DELETE_GRAPHICS_AFTER_DRAGGING_OUT = YES;
BOOL UD_EXPORT_CONVERTED_IMAGE = YES;
BOOL UD_CELLS_DRAG_OUT_AS_ALIAS = NO; //NO = Move Original

BOOL UD_CHECKBOX_CONVERT_TO_TEXT = YES;

BOOL UD_PASTE_AS_WEB_ARCHIVE = YES;

@implementation AttachmentCellConverter //NSAttributedString (AttachmentCellConverting)

+(AttachmentCellConverter*)sharedAttachmentCellConverter
{
	return [[NSApp delegate] attachmentCellConverter];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		
		cellsWantToWriteToFile = [[NSMutableArray array] retain];
		
		
		// load additional ud
			
		loadAdditionalUDWithFilename(@"AdditionalUD_AttachmentCellConverter");
		
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(preferenceChanged)
													 name:PreferenceDidChangeNotification
												   object:nil];
		
		[self preferenceChanged];
	}
	return self;
}
-(void)dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}
-(void)preferenceChanged
{
	//NSLog(@"preferenceChanged");
	syncronizePreferences();
	PreferenceController* pc = [PreferenceController standardUserDefaults];
	
	UD_PASTE_PICTURE_FROM_FILE		= [[pc valueForKey:@"UD_PASTE_PICTURE_FROM_FILE"] boolValue];
	UD_MAKE_BUTTON_FROM_FILE		= [[pc valueForKey:@"UD_MAKE_BUTTON_FROM_FILE"] boolValue];
	UD_MAKE_ALIAS_BUTTON			= [[pc valueForKey:@"UD_MAKE_ALIAS_BUTTON"] boolValue];
	UD_PASTE_FILE_ICON				= [[pc valueForKey:@"UD_PASTE_FILE_ICON"] boolValue];
	UD_PASTE_FILENAME				= [[pc valueForKey:@"UD_PASTE_FILENAME"] boolValue];
	UD_MAKE_FAVICON_BUTTON			= [[pc valueForKey:@"UD_MAKE_FAVICON_BUTTON"] boolValue];
	//UD_FAVICON_HAS_TITLE			= [[pc valueForKey:@"UD_FAVICON_HAS_TITLE"] boolValue];
	UD_URL_LOAD_FAVICON				= [[pc valueForKey:@"UD_URL_LOAD_FAVICON"] boolValue];
	UD_PASTE_URL_TEXT_LINK			= [[pc valueForKey:@"UD_PASTE_URL_TEXT_LINK"] boolValue];
	UD_FILES_PROMISE_PRIORITY		= [[pc valueForKey:@"UD_FILES_PROMISE_PRIORITY"] boolValue];
	UD_DELETE_CELLS_AFTER_DRAGGING_OUT		= [[pc valueForKey:@"UD_DELETE_CELLS_AFTER_DRAGGING_OUT"] boolValue];

	UD_DELETE_GRAPHICS_AFTER_DRAGGING_OUT		= [[pc valueForKey:@"UD_DELETE_GRAPHICS_AFTER_DRAGGING_OUT"] boolValue];
	UD_EXPORT_CONVERTED_IMAGE		= [[pc valueForKey:@"UD_EXPORT_CONVERTED_IMAGE"] boolValue];
	UD_CELLS_DRAG_OUT_AS_ALIAS		= [[pc valueForKey:@"UD_CELLS_DRAG_OUT_AS_ALIAS"] boolValue];
	UD_CHECKBOX_CONVERT_TO_TEXT		= [[pc valueForKey:@"UD_CHECKBOX_CONVERT_TO_TEXT"] boolValue];

	UD_PASTE_AS_ARCHIVE 	= [[pc valueForKey:@"UD_PASTE_AS_ARCHIVE"] boolValue];
	UD_PASTE_AS_PLAIN_TEXT 	= [[pc valueForKey:@"UD_PASTE_AS_PLAIN_TEXT"] boolValue];
	UD_FILE_CREATION_SOUND = [[pc valueForKey:@"UD_FILE_CREATION_SOUND"] boolValue];

	UD_PASTE_AS_WEB_ARCHIVE	= [[pc valueForKey:@"UD_PASTE_AS_WEB_ARCHIVE"] boolValue];
	
	//NSLog(@"UD_PASTE_PICTURE_FROM_FILE %d",UD_PASTE_PICTURE_FROM_FILE);
	//NSLog(@"UD_MAKE_BUTTON_FROM_FILE %d etc",UD_MAKE_BUTTON_FROM_FILE);

}

#pragma mark-
#pragma mark Cell Converting

-(NSString*)convertAttributedStringToString:(NSAttributedString*)attr 
{
	if( attr == nil ) return nil;

	return [[self convertCellsInAttributedString:attr
						usingSelector:@selector(stringRepresentation:) ] string];
}


-(NSData*)convertAttributedStringToRTF:(NSAttributedString*)attr documentAttributes:(NSDictionary *)docAttributes
{	
	if( attr == nil ) return nil;
	
	NSAttributedString* converted = [self convertCellsInAttributedString:attr
								   usingSelector:@selector(attributedStringWithoutImageRepresentation:) ] ;
	
	if( converted == nil ) return nil;
	
	
	return [converted RTFFromRange:	NSMakeRange(0,[converted length])
				 documentAttributes:docAttributes];
}

-(NSData*)convertAttributedStringToRTFD:(NSAttributedString*)attr documentAttributes:(NSDictionary *)docAttributes
{
	if( attr == nil ) return nil;

	NSAttributedString* converted = [self convertCellsInAttributedString:attr
							   usingSelector:@selector(attributedStringRepresentation:) ] ;

	if( converted == nil ) return nil;

	return [converted RTFDFromRange:	NSMakeRange(0,[converted length])
				documentAttributes:docAttributes];
	
	
}

-(NSFileWrapper*)convertAttributedStringToRTFDFileWrapper:(NSAttributedString*)attr documentAttributes:(NSDictionary *)docAttributes;
{
	if( attr == nil ) return nil;

	NSAttributedString* converted = [self convertCellsInAttributedString:attr
														   usingSelector:@selector(attributedStringRepresentation:) ] ;
	
	if( converted == nil ) return nil;

	return [converted RTFDFileWrapperFromRange:	NSMakeRange(0,[converted length])
				documentAttributes:docAttributes];
	
}

#pragma mark -

-(NSAttributedString*)convertCellsInAttributedString:(NSAttributedString*)attr usingSelector:(SEL)selector;

// convert using selector of cell with AttachmentCellConverting protocol
// selector: stringRepresentation, attributedStringWithoutImageRepresentation, 
//   attributedStringRepresentation, imageRepresentation
//
// return value: converted substring
{
	if( attr == nil ) return nil;
	
	NSMutableAttributedString* mattr = 
		[[[NSMutableAttributedString alloc] initWithAttributedString:  attr ] autorelease];
	
	unsigned hoge = 0;		
	NSRange effectiveRange;
	while(1)
	{
		if( hoge >= [mattr length] ) break;

		//NSLog(@"-> checking %d of %d",hoge,[mattr length]-1);

		
		NSDictionary* dic = [mattr attributesAtIndex:hoge
									  effectiveRange:&effectiveRange ];
		
		id attachment = [dic objectForKey:NSAttachmentAttributeName ];
		if( attachment == nil ) goto Next;

		id cell = [attachment attachmentCell];
		if( [cell conformsToProtocol:@protocol(AttachmentCellConverting)] )
		{
			//NSLog(@"found cell");
			
			NSDictionary* dictionary = 
			[NSDictionary dictionaryWithObjectsAndKeys:
						mattr, @"MutableAttributedString",
				[NSNumber numberWithInt:hoge], @"CellIndex",nil	];
			
			[cell retain];
			int changeInLength = (int)[cell performSelector:selector withObject:dictionary ];
			[cell release];
			
			//NSLog(@"replacing");

			effectiveRange.length = changeInLength +1;
		}

		
Next:
		hoge = NSMaxRange(effectiveRange);
		
		

	}
	
	
	return (NSAttributedString*)mattr;
}



-(NSArray*)extractPasteboardRepresentationsFromCellsInAttributedString:(NSAttributedString*)attr 

	// convert using selector of cell with AttachmentCellConverting protocol
	// selector: stringRepresentation, attributedStringWithoutImageRepresentation, 
	//   attributedStringRepresentation, imageRepresentation
	//
	// return value: converted substring
{
	//NSLog(@"extractPasteboardRepresentationsFromCellsInAttributedString");
	
	
	[cellsWantToWriteToFile removeAllObjects];
	
	if( attr == nil || [attr length] == 0 ) return nil;
	
	NSMutableArray* array = [NSMutableArray array];
	
	unsigned hoge = 0;		
	NSRange effectiveRange = NSMakeRange(0,0);
	while(1)
	{
		//NSLog(@"found cell");
		
		NSDictionary* dic = [attr attributesAtIndex:hoge  effectiveRange:&effectiveRange ];
		
		id attachment = [dic objectForKey:NSAttachmentAttributeName ];
		if( attachment == nil ) goto Next;
		
		
		id cell = [attachment attachmentCell];
		if( [cell conformsToProtocol:@protocol(AttachmentCellConverting)] )
		{
			//NSLog(@"converting Cell");
			NSDictionary* dictionary  = [cell pasteboardRepresentation:nil ];
			
			if( dictionary != nil )
				[array addObject:dictionary];
			
			if( [dictionary objectForKey:NSFilesPromisePboardType] != nil )
			{
				[cellsWantToWriteToFile  addObject:cell];
				//NSLog(@"add cell");

			}
			
		}
		
		
Next:
		hoge = NSMaxRange(effectiveRange);
		if( hoge >= [attr length] ) break;
		
	}
	
	
	return (NSArray*)array;
}


-(NSDictionary*)combinePasteboardRepresentations:(NSArray*)pasteboardDictionaries
{


	NSMutableArray* pathArray = [NSMutableArray array];
	NSMutableArray* titleArray = [NSMutableArray array];
	NSMutableArray* filesPromiseArray = [NSMutableArray array];

	
	unsigned int i, count = [pasteboardDictionaries count];
	for (i = 0; i < count; i++) {
		NSDictionary * dict = [pasteboardDictionaries objectAtIndex:i];
		NSArray* keys = [dict allKeys];
		
		if( [keys containsObject:NSFilesPromisePboardType] )
		{
			[filesPromiseArray addObject:[dict objectForKey: NSFilesPromisePboardType] ];
		}
		
		if( [keys containsObject:@"Apple URL pasteboard type"] )
		{
			NSArray* obj = [dict objectForKey: @"Apple URL pasteboard type"];
			[pathArray addObject: [obj objectAtIndex:0] ];
			[titleArray addObject: [obj objectAtIndex:1] ];

		}
		
	}
	
	NSMutableDictionary* mdic = [NSMutableDictionary dictionary];

	
	if( [filesPromiseArray count] >0 )
	{
		[mdic setObject:filesPromiseArray forKey:NSFilesPromisePboardType];
	}
	
	if( [pathArray count] > 0 )
	{
		NSDictionary* piyo = [NSDictionary dictionaryWithObjectsAndKeys:
			[NSNumber numberWithInt:0], @"FolderCount", [NSNumber numberWithInt:[pathArray count]], @"TotalCount" , nil];
		
		[mdic setObject:piyo forKey:@"BookmarkStatisticsPBoardType"];

		///
		
		[mdic setObject:[NSArray arrayWithObjects: pathArray, titleArray, nil] forKey:@"WebURLsWithTitlesPboardType"];

	}
	
	return mdic;
}

#pragma mark-
#pragma mark Pboard Converting

-(BOOL)writeAttributedString:(NSAttributedString*)attr toPasteboard:(NSPasteboard *)pboard
{
	//NSLog(@"writeAttributedString");
	
	

	NSArray* pasteboardDictionaries = [self extractPasteboardRepresentationsFromCellsInAttributedString:attr];
	
	////NSLog(@"pasteboardDictionaries %@",[pasteboardDictionaries description]);
	NSDictionary* combinedReps = [NSDictionary dictionary];
	

	combinedReps = [self combinePasteboardRepresentations: pasteboardDictionaries];


	
	NSMutableArray* dynamicTypes = [NSMutableArray arrayWithArray: [combinedReps allKeys]];
	//NSLog(@"dynamicTypes %@",[dynamicTypes description]);
	
	NSArray* basicTypes = [NSArray  arrayWithObjects: @"EdgiesTextPboardType", NSRTFDPboardType , NSRTFPboardType, NSStringPboardType , nil];

	[dynamicTypes addObjectsFromArray: basicTypes];

	
	if( ! UD_FILES_PROMISE_PRIORITY  && [attr length] > 1)
	{
		[dynamicTypes removeObject:NSFilesPromisePboardType ];
		[dynamicTypes removeObject:NSTIFFPboardType ];

	}
	
	
	//************
	
	[pboard declareTypes:dynamicTypes	owner:self];
	
	// 
	NSData* data = [NSKeyedArchiver archivedDataWithRootObject:attr ];
	[pboard setData:data forType:EdgiesTextPboardType];
	
	if( [dynamicTypes containsObject:  NSRTFDPboardType ] )
		[pboard setData:[self convertAttributedStringToRTFD:attr documentAttributes:nil] forType:NSRTFDPboardType];
	
	if( [dynamicTypes containsObject:  NSRTFPboardType ] )
		[pboard setData:[self convertAttributedStringToRTF:attr documentAttributes:nil] forType:NSRTFPboardType];
	
	[pboard setString:[self convertAttributedStringToString:attr ] forType:NSStringPboardType];
	
	
	//************

	//NSLog(@"[combinedReps allKeys] %@",[[combinedReps allKeys] description]);
	unsigned int i, count = [[combinedReps allKeys] count];
	for (i = 0; i < count; i++) {
		NSString * key = [[combinedReps allKeys] objectAtIndex:i];
		[pboard setPropertyList:[combinedReps objectForKey: key] forType:key];
		
		//NSLog(@"dynamic adding %@",key);
	}
	//NSLog(@"writeAttributedString finish" );
	
	
	return YES;
}

- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination senderTextView:(NSTextView*)textView
{
	//NSLog(@"writing %d cells",[cellsWantToWriteToFile count]);
	NSMutableArray* filenameArray = [NSMutableArray array];
	
	unsigned int i, count = [cellsWantToWriteToFile count];
	for (i = 0; i < count; i++) 
	{
		id <AttachmentCellConverting> cell = [cellsWantToWriteToFile objectAtIndex:i];
		
		
		//NSLog(@"writing %@",[cell className]);
			
		NSString* filename  = [cell writeToFileAt : dropDestination  ] ;
		if( filename != nil )
		{
			[filenameArray addObject: filename];
			
		}
	}
	
	[cellsWantToWriteToFile removeAllObjects];
	
	//NSLog(@"namesOfPromisedFilesDroppedAtDestination %@",[filenameArray description] );
	return filenameArray;
}

-(id)readTextFromPasteboard:(NSPasteboard *)pboard  draggingSource:(id <NSDraggingInfo>)sender
{
	
	NSArray* types = [pboard types];
	NSData* data = nil;
	NSAttributedString* attr = nil;
	
	//NSLog(@"readAttributedStringFromPasteboard... %@",[types description]);

	/*
	 BOOL UD_PASTE_AS_PLAIN_TEXT = YES; // new
	 BOOL UD_PASTE_AS_ARCHIVE = NO; // new 
	 */
	
	
	
	if( UD_PASTE_AS_ARCHIVE || [MOD_PRESSED:@"modDropAnything"])
	{
		attr = [ClipboardArchiveAttachmentCell clipboardArchiveAttachmentCellAttributedStringWithPasteboard:pboard];
		

		
	}else if( UD_PASTE_AS_PLAIN_TEXT || [MOD_PRESSED:@"modPasteAsPlainText"])
	{
		NSString* str = [pboard stringForType:  NSStringPboardType];
		if( str != nil )
		{
			attr = [[[NSAttributedString alloc] initWithString:str] autorelease];
		}else
		{
			data = [pboard dataForType:  NSRTFPboardType];
			if( data != nil )
				attr = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil] autorelease];
			else
			{
				data = [pboard dataForType:  NSRTFDPboardType];
				if( data != nil )
					attr = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil] autorelease];
				
			}
		}
		
	}else if( [types containsObject: EdgiesTextPboardType] ) 
	{
		data = [pboard dataForType:  EdgiesTextPboardType];
		if( data == nil ) goto Done;
		
		
		attr = [NSKeyedUnarchiver unarchiveObjectWithData: data];
		//NSLog(@"readAttributedStringFromPasteboard EdgiesTextPboardType");
		
	}else if( [types containsObject: @"CorePasteboardFlavorType 0x6D6F6F76"] )
	{
		data = [pboard dataForType:  @"CorePasteboardFlavorType 0x6D6F6F76"];//moov
		if( data == nil ) goto Done;
		
		
		
		
		NSRunAlertPanel(@"Note",@"Pasting QuickTime movie is not recommended. Please note that pasting many movies will spend computer resources a lot.",@"OK",nil,nil);

		
		
		attr = [MovieAttachmentCell movieAttachmentCellAttributedStringWithPasteboard: pboard];
		
	
	}else if( [types containsObject: NSTIFFPboardType] )
	{
		
		if( sender == nil )
		NSLog(@"sender is nil");
		
		if( [types containsObject:NSFilesPromisePboardType] == nil )
			NSLog(@"NSFilesPromisePboardType is nil");

		NSString* originalTiffFilePath = nil;
		if ( [types containsObject:NSFilesPromisePboardType] && sender != nil) {
			
			NSArray* names =  [sender
                namesOfPromisedFilesDroppedAtDestination:[NSURL fileURLWithPath: NSTemporaryDirectory()]];
			
			//NSLog(@"NSTemporaryDirectory() %@",NSTemporaryDirectory());
			//NSLog(@"files promise names %@",[names description]);
			
			if( names && [names count] > 0 )
			{
				originalTiffFilePath = 	[NSTemporaryDirectory() stringByAppendingPathComponent: [names objectAtIndex:0 ] ];
			}
			
			//NSLog(@"originalTiffFilePath %@",originalTiffFilePath);

			
		}
		
		
		
		data = [pboard dataForType:  NSTIFFPboardType];
		if( data == nil ) goto Done;

		attr = [self attributedStringFromTIFFData:data originalPath:originalTiffFilePath];
		
		//return [self pastePicture:sender];
		//NSLog(@"NSTIFFPboardType");

	}
	else if( [types containsObject: @"WebURLsWithTitlesPboardType"] )  // safai  url
	{
		NSArray* array = [pboard propertyListForType:@"WebURLsWithTitlesPboardType"];
		//NSLog(@"reading WebURLsWithTitlesPboardType %@",[array description]);
		if( array == nil ) goto Done;
		
		attr = [self attributedStringFromSafariURL:array];
		//return [self pasteSafariURL:sender];
		
		//NSLog(@"WebURLsWithTitlesPboardType");

	}else if( [types containsObject:NSFilenamesPboardType] )  // file
	{
		data = [pboard dataForType:  NSFilenamesPboardType];
		if( data == nil ) goto Done;
		
		BOOL boolBuffer = UD_PASTE_PICTURE_FROM_FILE;
		if( [MOD_PRESSED:@"modLink"] ){
			UD_PASTE_PICTURE_FROM_FILE = NO;
		}
		
		NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
		attr = [self attributedStringFromFilenames:files];

		UD_PASTE_PICTURE_FROM_FILE = boolBuffer;
		//return [self pasteFilenames:sender];
		//NSLog(@"NSFilenamesPboardType");

		
	}else if( [types containsObject:@"Apple URL pasteboard type"]   ) // basic url
	{
		data = [pboard dataForType:  @"Apple URL pasteboard type"];
		if( data == nil ) goto Done;
		
		NSURL* aURL = [NSURL URLFromPasteboard:pboard];
		attr = [self attributedStringFromOtherURL:aURL];
		
		//return [self pasteOtherURL:sender];
		
		//NSLog(@"Apple URL pasteboard type");

	}else  if( [types containsObject: NSRTFDPboardType] )
	{
		data = [pboard dataForType:  NSRTFDPboardType];
		if( data == nil ) goto Done;

		attr = [[[NSAttributedString alloc] initWithRTFD:data documentAttributes:nil] autorelease];

		//NSLog(@"readAttributedStringFromPasteboard NSRTFDPboardType");

	}else if( [types containsObject: NSRTFPboardType] )
	{
		data = [pboard dataForType:  NSRTFPboardType];
		if( data == nil ) goto Done;
		
		attr = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil] autorelease];
		
		//NSLog(@"readAttributedStringFromPasteboard NSRTFPboardType");

	}else if( [types containsObject: NSStringPboardType] )
	{
		NSString* str = [pboard stringForType:  NSStringPboardType];
		if( str == nil ) goto Done;
		
		attr = [[[NSAttributedString alloc] initWithString:str] autorelease];
		
		//NSLog(@"readAttributedStringFromPasteboard NSRTFPboardType");
		
	}
	//NSLog(@"pboad nil");

	
Done:
		//NSLog(@"done");
		
	return attr;
	
	
	
}

#pragma mark Library

-(NSAttributedString*)attributedStringFromTIFFData:(NSData*)data originalPath:(NSString*)originalPath
{
	
	NSImage* image = [[[NSImage alloc] initWithData:data] autorelease];
	
	
	MNGraphicAttachmentCell* newCell = 
		[MNGraphicAttachmentCell graphicAttachmentCellWithImage: image  andOriginalFilePath: originalPath] ;
	
	
	
	
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	//add files
	
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	
	[anAttachment setAttachmentCell:newCell]; //convert
	
	
	[wrapper setFilename:@"image.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		
	[wrapper setIcon:image ];
	
	return [NSMutableAttributedString attributedStringWithAttachment:anAttachment];			
}

-(NSAttributedString*)attributedStringFromSafariURL:(NSArray*)pbArray
{
		
	/*Structure
	(
	 (
	  "http://xtramail.xtra.co.nz/", 
	  "http://www.powerlogix.com/products2/bcg4pismo/index.html"
	  ), 
	 (xtra, "PowerLogix Products - BlueChip G4 \"Pismo\"")
	 )
	
	*/

	
	
	
	NSMutableAttributedString* mattr = [[[NSMutableAttributedString alloc] init] autorelease];
	
	int hoge;
	for(hoge = 0; hoge < [[pbArray objectAtIndex:0] count]; hoge++)
	{
		NSString* path = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
		NSURL* aURL = [NSURL URLWithString:path];
		NSString* URLtitle =  [[pbArray objectAtIndex:1] objectAtIndex:hoge];
		
		if( UD_MAKE_FAVICON_BUTTON )
		{
			NSAttributedString* attr = [FaviconAttachmentCell faviconAttachmentCellAttributedStringWithPath:path 
																								   andTitle:URLtitle 
																								   hasTitle:YES//depricated
																								loadFavicon:UD_URL_LOAD_FAVICON];
			[mattr appendAttributedString: attr];
			
		}
		
		if( UD_PASTE_URL_TEXT_LINK )
		{
		
			
			if(hoge != 0)
			{
				NSAttributedString* delimiter = [[[NSAttributedString alloc] initWithString:@"\n" attributes:NULL] autorelease];
				[mattr appendAttributedString: delimiter];
			}
			
			
			//
			if([URLtitle isEqualToString:@""])  URLtitle = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
			
			NSMutableDictionary* aLinkAttr;
			aLinkAttr = [NSMutableDictionary dictionaryWithObject: aURL
														   forKey: NSLinkAttributeName];
			[aLinkAttr setObject: path  forKey: NSToolTipAttributeName];
			[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];
			
			
			NSAttributedString* str = [[[NSAttributedString alloc] initWithString:URLtitle attributes:aLinkAttr] autorelease];
			
			[mattr appendAttributedString: str];
			
		}
		//favicon 
		//[self addFaviconForLinksInRange:NSMakeRange([self selectedRange].location -[str length],1)];
		
		
		
		
	}
	

	
	return mattr;
	
}

-(NSAttributedString*)attributedStringFromFilenames:(NSArray*)files
{	
	
	NSMutableAttributedString* mattr = [[[NSMutableAttributedString alloc] init] autorelease];

	
	int hoge;
	for(hoge = 0; hoge < [files count]; hoge++)
	{
		
		NSString* path =  [files objectAtIndex:hoge];
		NSString* filename = [path lastPathComponent];
		
		
		//delimiter
		
		if(hoge != 0)
		{
			NSAttributedString* delimiter = [[[NSAttributedString alloc] initWithString:@"\n" attributes:NULL] autorelease];
			[mattr appendAttributedString: delimiter];
		}
		
		/*
		/// Read Movie
		if( [self fileIsMovie:path] )
		{
			NSAlert *alert = [NSAlert alertWithMessageText:@"Pasting Movie file" defaultButton:@"Paste icon" alternateButton:@"Paste movie reference" otherButton:nil informativeTextWithFormat:@"Choose how to paste movie file"];
			
			int result = [alert runModal];
			
			
			if( result == NSAlertDefaultReturn )
			{
				
			}else
			{
				
				if( result == NSAlertAlternateReturn )
				{
					
					NSAttributedString *atr = [MovieAttachmentCell movieAttachmentCellAttributedStringWithFileReference:path];
					
					[mattr appendAttributedString: atr];

				}else
				{
					NSAttributedString *atr = [MovieAttachmentCell movieAttachmentCellAttributedStringWithFileData:path];
					[mattr appendAttributedString: atr];

				}
				
				continue;
			}
			
			
		}
		*/
		

	
		
		// graphic 
		//NSLog(@"DEBUG HERE");
		
		NSData* _pictureData = nil; 
		NSImage* pictureImage = nil;
		 
		
		//if path is a picture data, read its binary into pictureImage. Otherwise, pictureImage = nil;
		
		if( [self fileIsPicture:path] )
		{
			@try {
				_pictureData = [[[NSData alloc] initWithContentsOfFile:path] autorelease];
				pictureImage = [[[NSImage alloc] initWithData:_pictureData] autorelease];

				_pictureData = [pictureImage TIFFRepresentation];
			}
			@catch (NSException *exception) {
				_pictureData = nil;
			}

			
		}
		
		
		
		
		if( _pictureData != nil && UD_PASTE_PICTURE_FROM_FILE && ![[[path pathExtension] lowercaseString] isEqualToString: @"pdf"])
		{
			//pasting image
			
			NSAttributedString* aStr = [MNGraphicAttachmentCell graphicAttachmentCellAttributedStringWithImage: pictureImage andOriginalFilePath: path];
				
				
			//paste icon
			[mattr appendAttributedString: aStr];			
			
			/*
			 if( [self respondsToSelector:@selector(shrinkGraphicsToWindowWidthInRange:)] )
			 {
				 [self shrinkGraphicsToWindowWidthInRange:NSMakeRange([self selectedRange].location-[aStr length], [aStr length])];
				 
			 }
			 */
			
		}else
		{

			
			//prepare alias
			
			NDAlias* anAlias = [NDAlias aliasWithContentsOfFile:path]; //assume finder alias
			if( anAlias == NULL )//if not,
				anAlias = [NDAlias aliasWithPath:path];				
			
			NSMutableDictionary* aLinkAttr;
			aLinkAttr = [NSMutableDictionary dictionaryWithObject: anAlias
														   forKey: NSLinkAttributeName];
			[aLinkAttr setObject: path  forKey: NSToolTipAttributeName];
			[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];

			
			
			if( UD_MAKE_BUTTON_FROM_FILE )
			{
				
				if( UD_MAKE_ALIAS_BUTTON )
				{
					NSAttributedString* attr = [AliasAttachmentCell aliasAttachmentCellAttributedStringWithPath:path];
					[mattr appendAttributedString: attr];
					
				}else
				{
					NSAttributedString* attr = [FileEntityTextAttachmentCell fileEntityTextAttachmentCellAttributedStringWithPath:path];
					[mattr appendAttributedString: attr];
					
				}
				
				
				
			}
			
			
			
			if( UD_PASTE_FILE_ICON )
			{
				//create icon
				NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:[anAlias path]];
				
				if( iconImage != nil )
				{
					NSData* aData = [iconImage TIFFRepresentation];
					if( aData != nil )
					{
						NSFileWrapper* aWrapper = 
						[[[NSFileWrapper alloc] initRegularFileWithContents:aData] autorelease];
						
						[aWrapper setFilename:[NSString stringWithFormat:@"%@.tiff", filename]];
						NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:aWrapper] autorelease];
						[[anAttachment fileWrapper] setPreferredFilename: [[anAttachment fileWrapper] filename]];//self rename
							
							NSAttributedString* aStr = [NSAttributedString attributedStringWithAttachment:anAttachment];
							
							//paste icon
							[mattr appendAttributedString: aStr];
					}
				}
			
			}
			
			
			
			if( UD_PASTE_FILENAME )
			{
			
				NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:filename attributes:aLinkAttr] autorelease];
				[mattr appendAttributedString: attr];
			
			}
						
			
			
		}
		
		
		
		

	}// for end
	
	return  mattr;	
}

-(NSAttributedString*)attributedStringFromOtherURL:(NSURL*)aURL
{

	NSMutableAttributedString* mattr = [[[NSMutableAttributedString alloc] init] autorelease];

	if( UD_MAKE_FAVICON_BUTTON )
	{
		
		NSAttributedString* attr = [FaviconAttachmentCell faviconAttachmentCellAttributedStringWithPath:[aURL path] 
																							   andTitle:@"" 
																							   hasTitle:YES//depricated 
																							loadFavicon:UD_URL_LOAD_FAVICON];
		[mattr appendAttributedString: attr];

		
		
	}
	
	if( UD_PASTE_URL_TEXT_LINK )
	{
		NSMutableDictionary* aLinkAttr;
		aLinkAttr = [NSMutableDictionary dictionaryWithObject: aURL
													   forKey: NSLinkAttributeName];

		[aLinkAttr setObject: [aURL absoluteString]  forKey: NSToolTipAttributeName];
		[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];
		
		
		NSAttributedString* str = [[[NSAttributedString alloc] initWithString:[aURL absoluteString] attributes:aLinkAttr] autorelease];

		[mattr appendAttributedString: str];
	}

	//favicon 
	//[self addFaviconForLinksInRange:NSMakeRange(insertionCharIndex,[str length])];
	

	
	return  mattr;
	
}

-(BOOL)fileIsPicture:(NSString*)path
{
	NSString* utistr = getUTI(path);
	//NSLog(@"utistr %@",utistr);
	BOOL flag =  UTTypeConformsTo(
						  utistr,
							kUTTypeImage )  ;
	//NSLog(@"flag %d",flag);
	return flag;
}

-(BOOL)fileIsMovie:(NSString*)path
{
	NSString* utistr = getUTI(path);
	//NSLog(@"utistr %@",utistr);
	BOOL flag =  UTTypeConformsTo(
								  utistr,
								  kUTTypeMovie )  ;
	//NSLog(@"flag %d",flag);
	return flag;
}


@end
