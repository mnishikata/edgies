#import "PreferenceController.h"
#import "PointToMilTransformer.h"
#import "KeyedArchiverTransformer.h"

#import "ThemeListToTitleTransformer.h"
#import <RegistrationManager/RegistrationManager.h>
#import "TagAttachmentTextView.h"

#import "ColorCheckbox.h"
#import "ModifierKeyWell.h"
#import "FontWell.h"
#import "ThemeManager.h"
#import "InstallLibrary.h"
//#import "EWSLib.h"
//#import "validate.h"

//#import "AQDataExtensions.h"
#import "TabDocumentController.h"

#import "EdgiesLibrary.h"
#import "SoundManager.h"

#import "MNTabWindow.h"
#import "NSTextView (coordinate extension).h"

#import <SyncServices/SyncServices.h>

#define APP_SERIAL @"OneRiverEdgiesSerialNumber"
#define APP_USERNAME @"OneRiverEdgiesRegisterdUsername"

#define DEFAULT_TAB 1

#define USE_SYNC YES



 NSString*    PreferencesGeneralIdentifier = @"PreferencesGeneral";
 NSString*    PreferencesNewMemoIdentifier = @"PreferencesNewMemo";
 NSString*    PreferencesTabActionIdentifier = @"PreferencesTabAction";
 NSString*    PreferencesEdgeActionIdentifier = @"PreferencesEdgeAction";
 NSString*    PreferencesPrintingIdentifier = @"PreferencesPrinting"; 
 NSString*    PreferencesClosingIdentifier = @"PreferencesClosing";
 NSString*    PreferencesKeyBindingIdentifier = @"PreferencesKeyBinding";
 NSString*    PreferencesAdvancedIdentifier = @"PreferencesAdvanced";
NSString*    PreferencesSyncIdentifier = @"PreferencesSync";
 NSString*    PreferencesSoundsIdentifier = @"PreferencesSounds";
NSString*    PreferencesTagIdentifier = @"PreferencesTag";


@implementation PreferenceController


+(PreferenceController*)standardUserDefaults
{
	return [[NSApp delegate] preferenceController	];
}

+(void)initialize
{
	   if ( self == [PreferenceController class] ) {
		   
		   
		   PointToMilTransformer* pointToMilTransformer = [[[PointToMilTransformer alloc] init] autorelease];

		   
		   updateMinimumMargin();
		   
		   
		   [NSValueTransformer setValueTransformer:pointToMilTransformer
										   forName:@"PointToMilTransformerName"];
		   
		   
		   ThemeListToTitleTransformer* themeListToTitleTransformer = [[[ThemeListToTitleTransformer alloc] init] autorelease];

		   [NSValueTransformer setValueTransformer:themeListToTitleTransformer
										   forName:@"ThemeListToTitleTransformer"];	

		   [self setKeys:
			[NSArray arrayWithObjects:@"selectedThemeListIndex", nil]  triggerChangeNotificationsForDependentKey:@"selectedThemePreview"];

		   
		   //
		   KeyedArchiverTransformer* keyedArchiverTransformer = [[[KeyedArchiverTransformer alloc] init] autorelease];
		   [NSValueTransformer setValueTransformer:keyedArchiverTransformer
										   forName:@"KeyedArchiverTransformer"];

		   
		   [[NSUserDefaultsController sharedUserDefaultsController] setAppliesImmediately:YES];
	   }
}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	//////NSLog(@"observeValueForKeyPath %@",keyPath );

	
	
	if( [keyPath isEqualToString:@"metricsTag"] )
	{
		updateMinimumMargin();
		
		
		[self willChangeValueForKey: @"topMargin_min" ];
		[self didChangeValueForKey: @"topMargin_min" ];
		
		[self willChangeValueForKey: @"bottomMargin_min" ];
		[self didChangeValueForKey: @"bottomMargin_min" ];
		
		[self willChangeValueForKey: @"leftMargin_min" ];
		[self didChangeValueForKey: @"leftMargin_min" ];
		
		[self willChangeValueForKey: @"rightMargin_min" ];
		[self didChangeValueForKey: @"rightMargin_min" ];
		
		
		[self willChangeValueForKey: @"topMargin" ];
		[self didChangeValueForKey: @"topMargin" ];
		
		[self willChangeValueForKey: @"bottomMargin" ];
		[self didChangeValueForKey: @"bottomMargin" ];
		
		[self willChangeValueForKey: @"leftMargin" ];
		[self didChangeValueForKey: @"leftMargin" ];
		
		[self willChangeValueForKey: @"rightMargin" ];
		[self didChangeValueForKey: @"rightMargin" ];
		
		[self willChangeValueForKey: @"pageNumberMargin" ];
		[self didChangeValueForKey: @"pageNumberMargin" ];
	}
	
	
	if( [keyPath isEqualToString:@"defaultColorWell"] )
	{
		
		[self setColorMenu];
		
	}
	
	if( [keyPath isEqualToString:@"sliderClosed"] ||  [keyPath isEqualToString:@"sliderOpened"]   )
	{
	}
	
	if( [keyPath isEqualToString:@"themeList"]  )
	{
		
		[self setColorMenu];
	}

	
	
	//[super observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context];
	
}

- (NSResponder *)nextResponder
{
	return [[NSApp delegate] nextResponderFor:self];
}


- (id) init {
	self = [super init];
	if (self != nil) {
		
		
		temporaryPreferencesStore = [[NSMutableDictionary alloc] init];
		
		additionalUDArrayInLocal = nil;
		additionalUDControllerFilterPredicate = [[NSString alloc] init];
		preferencePanelShowMoreOptions = NO;

		
		///  ********
		NSString* username;
		if( [[RegistrationManager sharedRegistrationManager] registered] )
		{
			username = preferenceValueForKey(@"OneRiverEdgiesRegisterdUsername");	
		}else
		{
			username = @"Edgies Demo User";
			
		}
		
		setPreferenceValueForKey(username,@"p_author");
			
		
		// set default values
		setDefaultPreferencesValues();
		
		//load nib

		//[self popTemporaryPreferencesValues];
		[NSBundle loadNibNamed:@"Preferences"  owner:self];


		additionalUDArrayInLocal = [[NSMutableArray alloc] init];
		
		[additionalUDArrayController setContent:additionalUDArrayInLocal];
		[additionalUDArrayController setFilterPredicate: nil];
		[additionalUDArrayController rearrangeObjects];
		
		//  setupValueObservers;
		[self addObserver:self forKeyPath:@"metricsTag" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self forKeyPath:@"defaultColorWell" options:NSKeyValueObservingOptionNew context:nil];
		
		[self addObserver:self forKeyPath:@"sliderClosed" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self forKeyPath:@"sliderOpened" options:NSKeyValueObservingOptionNew context:nil];
		
		[self addObserver:self forKeyPath:@"themeList" options:NSKeyValueObservingOptionNew context:nil];

		
		
		[PreferenceController setKeys:
			[NSArray arrayWithObjects:@"selectedThemeListIndex", nil]  triggerChangeNotificationsForDependentKey:@"selectedThemePreview"];
		
		
		//tagTextView binding
		/*
		NSNumber* BOOLYES = [NSNumber numberWithBool:YES];
		NSMutableDictionary*  dic = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"KeyedArchiverTransformer", NSValueTransformerNameBindingOption,
			BOOLYES, NSRaisesForNotApplicableKeysBindingOption, nil];
		
		
		[tagTextView bind:NSAttributedStringBinding toObject:self withKeyPath:@"tagList" options:dic];
		*/

		
		//set up preference
		
		
		[self popTemporaryPreferencesValues];

		[self apply];
		
		
			
		
		//*****
		// NSToolbarのインスタンスを作ります
		NSToolbar*  toolbar;
		toolbar = [[[NSToolbar alloc] initWithIdentifier:@"ToolbarForTabView"] autorelease];
		
		// ツールバーを設定します
		[toolbar setDelegate:self];
		[panel setToolbar:toolbar];
		//[panel setShowsToolbarButton:NO];
		
		[toolbar setSelectedItemIdentifier:PreferencesNewMemoIdentifier];
		
		id defaulttoolbaritem = [[toolbar items] objectAtIndex:0];
		[self _showTab: defaulttoolbaritem];
		
		//NSLog(@"awakefromnib finished");
	}
	return self;
}





-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	

	[super dealloc];
}


- (IBAction)revertToDefault:(id)sender
{
	unsigned int i, count = [additionalUDArrayInLocal count];

	for (i = 0; i < count; i++) {
		
		NSMutableDictionary* dictionary = [additionalUDArrayInLocal objectAtIndex:i];
		id defaultValue = [dictionary objectForKey:@"defaultValue"];

		[dictionary setObject:defaultValue forKey:@"value"];
	}	
	
	
}

- (IBAction)buttonClicked:(id)sender
{
// -100 defualt, -1 cancel 0 ok
	int tag = [sender tag];
	

	
	if( tag == -1 )
	{
		// reset
		//[self loadUserDefaults];
		
		
		if( preferenceBoolValueForKey(@"autoHide") == NO ) [[TabDocumentController sharedTabDocumentController] arrangeToFront:self];
		
		[self popTemporaryPreferencesValues];

		
		// adjust transparency
		
		NSArray* documents = [[TabDocumentController sharedTabDocumentController] visibleDocuments];
		
		unsigned int i, count = [documents count];
		for (i = 0; i < count; i++) {
			MiniDocument * obj = [documents objectAtIndex:i];
			MNTabWindow* window = [obj window];
			if( [window isOpened] || [window isTornOff] )
			{
				float alpha = [[temporaryPreferencesStore objectForKey:@"sliderOpened" ] floatValue] / 100.0;
				[[obj window] performSelector:@selector(setAlphaValue:) withObject:[NSNumber numberWithFloat:alpha]];
			}
			else
			{
				float alpha = [[temporaryPreferencesStore objectForKey:@"sliderClosed" ] floatValue] / 100.0;
				[[obj window] performSelector:@selector(setAlphaValue:) withObject:[NSNumber numberWithFloat:alpha]];

			}

		}
		[self closePanel];


	}
	
	if( tag == 0 )
	{

		
		
		[self apply];
		[self closePanel];

	}
	
	if( tag == -100 )
	{
		
		[self apply];
	}

}



-(IBAction)showPreferencePanel:(id)sender
{

	[NSApp activateIgnoringOtherApps:YES];
	
	//NSLog(@"openPreferences");
		
	//[panel center];
	[self popTemporaryPreferencesValues];

	[self setColorMenu];
	[panel center];
	[panel makeKeyAndOrderFront:self];
	[self adjustViewHeight];
	
}

-(void)setColorMenu
{
	NSMenu* colorMenu = [[ThemeManager sharedManager] colorMenu];
	NSColor* defaultColor = [NSUnarchiver unarchiveObjectWithData:[temporaryPreferencesStore objectForKey: @"defaultColorWell"] ];


	[defaultColorMenu setMenu:colorMenu];

	BOOL found = NO;
	unsigned int i, count = [colorMenu numberOfItems];
	for (i = 0; i < count; i++) {
		NSMenuItem * item = [colorMenu itemAtIndex:i ];
		NSColor* rep = [item representedObject];
		
		if( [rep isEqualTo: defaultColor ] )
		{
			found = YES;
			break;

		}

		
	}
	
	if( found )
		[defaultColorMenu selectItemAtIndex: i];
	
	else
		[defaultColorMenu selectItem:[defaultColorMenu lastItem]];	
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
}
- (void)setValue:(id)value forKey:(NSString *)key
{
	//NSLog(@"setValue %@ %@", value,key);

	
	
	
	if( [key isEqualToString:@"additionalUDControllerFilterPredicate"] )
	{
		NSPredicate* predicate;
		if( value == nil ){
			[additionalUDArrayController setContent:[additionalUDArrayController content]];
			[additionalUDArrayController setFilterPredicate:nil];
			[additionalUDArrayController rearrangeObjects];
			predicate = nil;
			
		}else
		{
			predicate = [NSPredicate predicateWithFormat:@"(category contains[wc] %@)||(title contains[wc] %@)||(comment contains[wc] %@)",value,value,value];

			[additionalUDArrayController setFilterPredicate:nil];
			[additionalUDArrayController rearrangeObjects];
		
		
			[additionalUDArrayController setFilterPredicate:predicate];
			[additionalUDArrayController rearrangeObjects];
		}
		//NSLog(@"additionalUDArrayController %d",[[additionalUDArrayController arrangedObjects] count]);

		[self willChangeValueForKey:@"additionalUDControllerFilterPredicate"];
		[additionalUDControllerFilterPredicate release];
		additionalUDControllerFilterPredicate = [value retain];
		[self didChangeValueForKey:@"additionalUDControllerFilterPredicate"];
		return;
	}
	
	
	
	if( [key isEqualToString:@"preferencePanelShowMoreOptions" ] )
	{
		[self willChangeValueForKey:key];
		preferencePanelShowMoreOptions = [value boolValue];
		[self adjustViewHeight];
		[self didChangeValueForKey:key];
		
		return;
	}
	
	
	
	
	
	[self willChangeValueForKey: key];
	if( [key isEqualToString:@"sliderClosed"]  )
	{

		NSArray* documents = [[TabDocumentController sharedTabDocumentController] visibleDocuments];
		
		unsigned int i, count = [documents count];
		for (i = 0; i < count; i++) {
			NSObject * obj = [documents objectAtIndex:i];
			float alpha = [value floatValue]/100.0;
			MNTabWindow* window = [obj window];
			
			if( ![window isOpened] && ![window isTornOff] )
				[[obj window] performSelector:@selector(setAlphaValue:) withObject:[NSNumber numberWithFloat:alpha]];
			
		}
	}

	if( [key isEqualToString:@"sliderOpened"]  )
	{
		
		NSArray* documents = [[TabDocumentController sharedTabDocumentController] visibleDocuments];
		
		unsigned int i, count = [documents count];
		for (i = 0; i < count; i++) {
			MiniDocument * obj = [documents objectAtIndex:i];
			float alpha = [value floatValue]/100.0;
			MNTabWindow* window = [obj window];
			
			if( [window isOpened] || [window isTornOff] )
			[[obj window] performSelector:@selector(setAlphaValue:) withObject:[NSNumber numberWithFloat:alpha]];

		}
	}

	[temporaryPreferencesStore setValue:(id)value forKey:(NSString *)key];
	
	[self didChangeValueForKey: key];

	
	
	if( [key isEqualToString:@"tagPrefix"] ||  [key isEqualToString:@"tagSuffix"] ||
		[key isEqualToString:@"tagSeparator"] ||[key isEqualToString:@"tagCommentPrefix"]
		|| [key isEqualToString:@"tagStartCode"] ||[key isEqualToString:@"tagEndCode"])
	{
		[self willChangeValueForKey:@"tagSample"];
		[self didChangeValueForKey:@"tagSample"];
		
	}

}
-(id)valueForKey:(NSString*)key
{
	if( [key isEqualToString:@"preferencePanelShowMoreOptions" ] )
		return [super valueForKey:(NSString*)key];
		
	if( [key isEqualToString:@"themeList"] )
		return 	[[ThemeManager sharedManager] themeList]  ;

	
	if( [key isEqualToString:@"selectedThemePreview"] )
	{
		id idxval = [self valueForKey:@"selectedThemeListIndex"];
		if( idxval == nil ) return nil;
		
		int idx = [idxval intValue];
		NSString* path =  [[[[ThemeManager sharedManager] themeList] objectAtIndex: idx]  objectForKey:@"path"];
		
		if( path == nil ) return nil;
		
		return [ThemeManager loadPreviewFromFile:path];
	}
	
	if( [key isEqualToString:@"tagSample"] )
	{
		NSString* tagPrefix = [self valueForKey:@"tagPrefix"];
		NSString* tagSuffix = [self valueForKey:@"tagSuffix"];
		NSString* tagSeparator = [self valueForKey:@"tagSeparator"];
		NSString* tagCommentPrefix = [self valueForKey:@"tagCommentPrefix"];
		NSString* tagStartCode = [self valueForKey:@"tagStartCode"];
		NSString* tagEndCode = [self valueForKey:@"tagEndCode"];

		
		
		NSString* sample = [NSString stringWithFormat:@"%@%@Sample Tag1%@%@%@Tag2%@%@%@Tag3%@%@%@Additional Finder comments here",tagStartCode, tagPrefix,tagSuffix,tagSeparator,tagPrefix,tagSuffix,tagSeparator,tagPrefix,tagSuffix, tagEndCode, tagCommentPrefix];
		return sample;
	}
	
	
	
	return [temporaryPreferencesStore objectForKey:key];
}


-(void)popTemporaryPreferencesValues
{
	//NSLog(@"popTemporaryPreferencesValues");
	
	
//temporaryPreferencesStore
	[temporaryPreferencesStore removeAllObjects];
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	[ud synchronize];
	syncronizePreferences();
	
	[temporaryPreferencesStore setDictionary: [ud dictionaryRepresentation]];


	NSArray* keys = [temporaryPreferencesStore allKeys];
	
	unsigned int i, count = [keys count];
	for (i = 0; i < count; i++) {
		NSString * key = [keys objectAtIndex:i];

		[self willChangeValueForKey:key];
		[self didChangeValueForKey:key];

	}
	
	
	//load additional ud value to additionalUDArrayInLocal
	
	count = [additionalUDArrayInLocal count];
	for (i = 0; i < count; i++) {
		NSMutableDictionary * obj = [additionalUDArrayInLocal objectAtIndex:i];
		NSString* key = [obj objectForKey:@"key"];
		
		id value = [temporaryPreferencesStore objectForKey:key];
		
		if( value != nil )
		[obj setObject:value forKey:@"value"];
	}
	

	
	
	[additionalUDArrayController rearrangeObjects];
	
	
	//
	NSData* data = [ud objectForKey:@"tagList"];
	if( data != nil )
	{
		[[tagTextView textStorage] setAttributedString: [NSKeyedUnarchiver unarchiveObjectWithData:data] ];
	}
	[tagTextView setSelectNewTag:NO];
	
	
	[self willChangeValueForKey:@"tagSample"];
	[self didChangeValueForKey:@"tagSample"];

}


-(void)pushTemporaryPreferencesValues
{
	
	[[NSUserDefaults standardUserDefaults] synchronize];
	syncronizePreferences();
	
	//
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	// check document folder.
	
	NSString* newFolder = [temporaryPreferencesStore objectForKey:@"documentFolderPath"];
	NSString* oldFolder = preferenceValueForKey(@"documentFolderPath");

	BOOL changeDocumentFolder = NO;
	
	if( ![newFolder isEqualToString: oldFolder ] )
	{
		changeDocumentFolder = YES;
		TabDocumentController *docController = [TabDocumentController sharedTabDocumentController];
		NSArray* documents = [docController documents];
		
		NSAlert *alert = [NSAlert alertWithMessageText: NSLocalizedString(@"Document folder has been changed",@"") defaultButton:NSLocalizedString(@"Move edgy files",@"") alternateButton:NSLocalizedString(@"Do not move edgy files",@"") otherButton:nil informativeTextWithFormat:NSLocalizedString(@"Do you want to move edgy files in the old folder to the new folder?",@"")];
		
		int result = [alert runModal];
		
		
		if( result == NSAlertDefaultReturn )
		{
			// Move process
			int hoge;
			
			
			for( hoge = 0; hoge < [documents count]; hoge++ )
			{
				MiniDocument *doc;

					doc = [documents objectAtIndex:hoge];
				
				NSString* filename = [doc filePath];
				
				
				
				NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
				[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath: oldFolder];
				
				
				
				
				NSString* newPath = [[newFolder stringByAppendingPathComponent: [filename lastPathComponent]] uniquePathForFolder];
				
				
				[[NSFileManager defaultManager] movePath:filename
												  toPath:newPath 
												 handler:nil];
			
			}
		}
		
		
		// close process here
		[[docController documents] makeObjectsPerformSelector:@selector(closeForceSaving)];
		
		
	}
		

	
	
	
	///Copy from temporary store to UD
	
	NSArray* keys = [temporaryPreferencesStore allKeys];
	
	unsigned int i, count = [keys count];
	for (i = 0; i < count; i++) {
		NSString * key = [keys objectAtIndex:i];
		NSObject* value = [temporaryPreferencesStore objectForKey:key];
		
		if( value != nil )
		[ud setObject:value forKey:key];
	}
	
	[ud setObject:[NSKeyedArchiver archivedDataWithRootObject:[tagTextView textStorage]]  forKey:@"tagList"];
	
	
	
	
	
	[ud synchronize];

	
	/// save additional ud in additionalUDArrayInLocal
	
	
	
	
	
	//laod list.  set user's value to the list now
	
	count = [additionalUDArrayInLocal count];
	for (i = 0; i < count; i++) {
		NSDictionary * obj = [additionalUDArrayInLocal objectAtIndex:i];
		
		NSString* key = [obj objectForKey:@"key"];
		id value = [obj objectForKey:@"value"];

		setPreferenceValueForKey(value , key);
	}
	


	syncronizePreferences();
	
	
	if( changeDocumentFolder )
	{
		///
		// reopen process here
		[[TabDocumentController sharedTabDocumentController] readDocumentsFromPersistentStore];
		
		
		
		// finish
		[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
															object:self];
		
	}

	
	
	

	[self popTemporaryPreferencesValues];//reload temporaryPreferencesStore;
}


- (void)apply
{
	
	if ([panel makeFirstResponder:panel]) {
		/* All fields are now valid; it’s safe to use fieldEditor:forObject:
		 to claim the field editor. */
	}
	else {
		/* Force first responder to resign. */
		[panel endEditingFor:nil];
	}
	
	
	
	////NSLog(@"temp %d",[[ temporaryPreferencesStore objectForKey:@"selectedThemeListIndex"] intValue]);
	////NSLog(@"ud %d",[preferenceValueForKey(@"selectedThemeListIndex") intValue]);

	//change theme
	if( preferenceValueForKey(@"selectedThemeListIndex") == nil || 
		! [[ temporaryPreferencesStore objectForKey:@"selectedThemeListIndex"] isEqualToNumber:
		preferenceValueForKey(@"selectedThemeListIndex")] )
	{
		//NSLog(@"theme changed");
		
		int idx = [[temporaryPreferencesStore objectForKey:@"selectedThemeListIndex"] intValue];
		NSArray* themeList = [[ThemeManager sharedManager] themeList];
		[[ThemeManager sharedManager] setThemeAtPath:[[themeList objectAtIndex: idx] objectForKey:@"path"] ];
		
		[[[TabDocumentController sharedTabDocumentController] documents] makeObjectsPerformSelector:@selector(redrawWindow)];
	}
	
	
	
	
	
#ifdef USE_SYNC
	
	int syncOption = [[ temporaryPreferencesStore objectForKey:@"syncOption"] intValue];
	ISyncManager *manager = [ISyncManager sharedManager];
	ISyncClient *syncClient;
	
	
	syncClient = [manager clientWithIdentifier:@"com.pukeko.edgies.sync.client"];
	

	
	[syncClient setEnabled: syncOption  forEntityNames:[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];

#endif
	
	
	
	[self pushTemporaryPreferencesValues];
	
	[[TabDocumentController sharedTabDocumentController]  arrangeToFront:self ];

	
	//NSLog(@"push finish");
	[[NSNotificationCenter defaultCenter] postNotificationName:PreferenceDidChangeNotification
														object:nil];	

	
}





- (void)setNilValueForKey:(NSString *)key
{
	//NSLog(@"setNilValueForKey %@",key);
	
}

-(IBAction)defaultColorMenuChanged:(id)sender
{

	NSColor* color = [[defaultColorMenu selectedItem] representedObject];
	
	if( [color isKindOfClass:[NSColor class]] )
	{
		[self setValue:[NSArchiver archivedDataWithRootObject: color ] forKey:@"defaultColorWell"];	
	}
	
}

-(void) registerAdditionalUD:(NSArray*) array
{
	////NSLog(@"registering count %d",[array count]);
	syncronizePreferences();
	
	unsigned int i, count = [array count];
	for (i = 0; i < count; i++) {
		NSDictionary * obj = [array objectAtIndex:i];
		
		//NSString* category = NSLocalizedString( [obj objectForKey:@"category"], @"");
		//NSString* comment = NSLocalizedString( [obj objectForKey:@"comment"], @"");
		NSString* key =   [obj objectForKey:@"key"] ;
		id defaultValue =  [obj objectForKey:@"defaultValue"]  ;
		
		
		if( preferenceValueForKey(key) == nil )
		{
			setPreferenceValueForKey(defaultValue,key);
		}
		
		
	}	
	
	
	syncronizePreferences();
	
	NSData* data = [NSPropertyListSerialization dataFromPropertyList:array
															  format:NSPropertyListXMLFormat_v1_0
													errorDescription:nil];
	
	NSMutableArray* marray = [NSPropertyListSerialization propertyListFromData:data
															  mutabilityOption:NSPropertyListMutableContainersAndLeaves
																		format:nil
															  errorDescription:nil];
	
	

	
	////NSLog(@"registering  %@",[marray description]);

	
	count = [marray count];
	for (i = 0; i < count; i++) {
		NSMutableDictionary  * dict = [marray objectAtIndex:i];
		NSString* category = NSLocalizedString( [dict objectForKey:@"category"], @"");
		NSString* comment = NSLocalizedString( [dict objectForKey:@"comment"], @"");
		NSString* title = NSLocalizedString( [dict objectForKey:@"title"], @"");

		[dict setObject:category forKey:@"category"];
		[dict setObject:comment forKey:@"comment"];
		[dict setObject:title forKey:@"title"];

	}
	
	
	
	[additionalUDArrayInLocal addObjectsFromArray: marray];

	[additionalUDArrayController rearrangeObjects];

	////NSLog(@"additionalUDArrayInLocal count %d",[additionalUDArrayInLocal count]);
	////NSLog(@"additionalUDArrayController count %d",[[additionalUDArrayController arrangedObjects] count]);


	
}

-(void)tagButton:(id)sender
{
	int tag = [sender tag];
	
	if( tag == 0 )//natural
	{
		[self setValue:@"" forKey:@"tagPrefix" ];
		[self setValue:@"" forKey:@"tagSuffix" ];
		[self setValue:@", " forKey:@"tagSeparator" ];
		[self setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[self setValue:@"" forKey:@"tagStartCode" ];
		[self setValue:@"" forKey:@"tagEndCode" ];

		[self setValue:@".*" forKey:@"tagGroupExtract" ];
		[self setValue:@"[^,\\n]+" forKey:@"tagExtract" ];

	}
	else if( tag == 1 )//tagBot
	{
		[self setValue:@"&" forKey:@"tagPrefix" ];
		[self setValue:@"" forKey:@"tagSuffix" ];
		[self setValue:@" " forKey:@"tagSeparator" ];
		[self setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[self setValue:@"" forKey:@"tagStartCode" ];
		[self setValue:@"" forKey:@"tagEndCode" ];

		[self setValue:@"&.*" forKey:@"tagGroupExtract" ];
		[self setValue:@"(?<=&).[^ \\n]*" forKey:@"tagExtract" ];			
	}
	else if( tag == 2 )//punakea
	{
		[self setValue:@"@" forKey:@"tagPrefix" ];
		[self setValue:@";" forKey:@"tagSuffix" ];
		[self setValue:@"" forKey:@"tagSeparator" ];
		[self setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[self setValue:@"###begin_tags###" forKey:@"tagStartCode" ];
		[self setValue:@"###end_tags###"  forKey:@"tagEndCode" ];

		
		[self setValue:@"###begin_tags###.*###end_tags###" forKey:@"tagGroupExtract" ];
		[self setValue:@"(?<=@)[^;]*(?=;)" forKey:@"tagExtract" ];	
		
	}
}

#pragma mark -



#pragma mark Accessor


-(int)metricsTag
{
	return [[temporaryPreferencesStore objectForKey:@"metricsTag"] intValue];
}



#pragma mark - 
#pragma mark UI

-(void)revealInFinder:(id)sender
{
	NSString *path = [temporaryPreferencesStore objectForKey:@"documentFolderPath"];
	[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath:path];
}

-(void)closePanel
{
	[panel orderOut:self];
}


#pragma mark-



#pragma mark Toolbar
//--------------------------------------------------------------//
// NSToolbar delegate methods
//--------------------------------------------------------------//


- (NSArray*)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
	// 識別子の配列を返します
    return [NSArray arrayWithObjects:
		 
		//PreferencesGeneralIdentifier, 
		PreferencesNewMemoIdentifier, 
		PreferencesTabActionIdentifier, 
		PreferencesTagIdentifier,
		PreferencesEdgeActionIdentifier, 
		
		PreferencesPrintingIdentifier, 
		PreferencesClosingIdentifier, 
		PreferencesKeyBindingIdentifier,
		PreferencesSoundsIdentifier,
		PreferencesSyncIdentifier,
		PreferencesAdvancedIdentifier,
		nil];
}

- (NSArray*)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
    return [self toolbarDefaultItemIdentifiers:toolbar];
}

- (NSArray*)toolbarSelectableItemIdentifiers:(NSToolbar*)toolbar
{
    return [self toolbarDefaultItemIdentifiers:toolbar];
}

- (NSToolbarItem*)toolbar:(NSToolbar*)toolbar 
	itemForItemIdentifier:(NSString*)itemId 
willBeInsertedIntoToolbar:(BOOL)willBeInserted
{
	// NSToolbarItem を作ります
    NSToolbarItem*  toolbarItem;
    toolbarItem = [[[NSToolbarItem alloc] initWithItemIdentifier:itemId] autorelease];
	[toolbarItem setTarget:self];
	[toolbarItem setAction:@selector(_showTab:)];
    
	// NSToolbarItemを設定します
    if ([itemId isEqualToString:PreferencesGeneralIdentifier]) {
        [toolbarItem setLabel:@"General"];
        [toolbarItem setImage:[NSImage imageNamed:@"GeneralPreferences"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:PreferencesNewMemoIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Default",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"pref1"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:PreferencesTabActionIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Tab Setting",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"pref4"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:PreferencesEdgeActionIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Edge Action",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"pref2"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:PreferencesPrintingIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Printing",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"print"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:PreferencesClosingIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"File",@"")  ];
        [toolbarItem setImage:[NSImage imageNamed:@"pref3"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:PreferencesKeyBindingIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Modifiers",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"pref5"]];
        
        return toolbarItem;
    }
	if ([itemId isEqualToString:PreferencesAdvancedIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Advanced",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"AdvancedPreferences"]];
        
        return toolbarItem;
    }
	
	if ([itemId isEqualToString:PreferencesSyncIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Sync",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"ISync"]];
        
        return toolbarItem;
    }
	
		if ([itemId isEqualToString:PreferencesSoundsIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Sounds",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"Sounds"]];
        
        return toolbarItem;
    }
	
	if ([itemId isEqualToString:PreferencesTagIdentifier]) {
        [toolbarItem setLabel: NSLocalizedString(@"Tag",@"") ];
        [toolbarItem setImage:[NSImage imageNamed:@"tagCell"]];
        
        return toolbarItem;
    }
	
		
    return nil;
}
-(void)adjustViewHeight
{
	//NSLog(@"adjustViewHeight");
	
	BOOL moreOptions = preferencePanelShowMoreOptions;
	
	
	float margin = 130;
	id identifier = [[preferencesTab selectedTabViewItem] identifier];
	
	//NSLog(@"%@",identifier);
	
	NSRect newFrame = [panel frame];
		
	
	float newHeight = newFrame.size.height; 

	
	if( [identifier isEqualToString: PreferencesGeneralIdentifier ] )
	{
	}
	
	else if( [identifier isEqualToString: PreferencesNewMemoIdentifier ] )
	{
		if( moreOptions == NO )
			newHeight = [_tab1_height frame].size.height;	
		
		if( moreOptions == YES )
			newHeight = [_tab1_height_plus frame].size.height;	
		
	}
	
	else if( [identifier isEqualToString: PreferencesTabActionIdentifier ] )
	{
		if( moreOptions == NO )
			newHeight = [_tab2_height frame].size.height;	
		
		if( moreOptions == YES )
			newHeight = [_tab2_height_plus frame].size.height;	
	}
	
	else if( [identifier isEqualToString: PreferencesEdgeActionIdentifier ] )
	{
		if( moreOptions == NO )
			newHeight = [_tab3_height frame].size.height;	
		
		if( moreOptions == YES )
			newHeight = [_tab3_height_plus frame].size.height;	
	}
	
	else if( [identifier isEqualToString: PreferencesTagIdentifier ] )
	{
		if( moreOptions == NO )
			newHeight = [_tab10_height frame].size.height;	
		
		if( moreOptions == YES )
			newHeight = [_tab10_height_plus frame].size.height;	
	}
	
	
		
	
	else if( [identifier isEqualToString: PreferencesPrintingIdentifier ] )
		newHeight = [_tab4_height frame].size.height;	
	
	else if( [identifier isEqualToString: PreferencesClosingIdentifier ] )
		newHeight = [_tab5_height frame].size.height;	
	
	else if( [identifier isEqualToString: PreferencesKeyBindingIdentifier ])
		newHeight = [_tab6_height frame].size.height;	
	
	else if( [identifier isEqualToString: PreferencesAdvancedIdentifier ] )
		newHeight = [_tab8_height frame].size.height;	
	
	
	else if( [identifier isEqualToString: PreferencesSyncIdentifier ] )
		newHeight = [_tab9_height frame].size.height;	
	
	
	else if( [identifier isEqualToString: PreferencesSoundsIdentifier ] )
		newHeight = [_tab7_height frame].size.height;	
	
	
	
	
	newFrame.size.height = newHeight + margin;
	[self frameChangeTo:newFrame];

}

- (void)_showTab:(id)sender
{
	// タブを選択します
	// このサンプルでは、NSToolbarItemの識別子と、NSTabViewItemの識別子を、
	// 同じにしてあります
	
	
	//
	
	

	id identifier = [sender itemIdentifier];
	
	[preferencesTab setHidden:YES];
		
    [preferencesTab selectTabViewItemWithIdentifier:[sender itemIdentifier]];
	
		
	[self adjustViewHeight];

	
	[preferencesTab setHidden:NO];
}

-(void)frameChangeTo:(NSRect)newFrame
{
	//NSLog(@"frameChangeTo %@",NSStringFromRect(newFrame));
	
	NSRect oldFrame = [panel frame];
	
	newFrame.origin.y = oldFrame.origin.y - (newFrame.size.height - oldFrame.size.height );
	//NSLog(@"newFrame %@",NSStringFromRect(newFrame));

	[panel setFrame:newFrame display:YES animate:YES];
	
}

- (BOOL)textShouldEndEditing:(NSText *)textObject
{
	////NSLog(@"ts ");
	return YES;	
}


@end
