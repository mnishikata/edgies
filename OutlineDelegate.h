/* OutlineDelegate */

#import <Cocoa/Cocoa.h>

@interface OutlineDelegate : NSObject
{
	IBOutlet id outlineView;
	IBOutlet id myDocument;
	IBOutlet id findResult;

	//IBOutlet id treeController;

	NSMutableArray* dataArray;
	
	id itemWaitingForResults;
	
	NSImage* docIcon;
	NSImage* folderIcon;
	NSImage* resultIcon;
	NSImage* missingIcon;
	
	
}
-(void)postOutlineChangedNotification;
- (void)awakeFromNib ;
-(void)saveDataArray;
-(void)dealloc;
-(void)updateIcons;
-(void)setMissingForItem:(NSMutableDictionary*)item;
-(void)addIconForItem:(NSMutableDictionary*)item;
-(void)updateOutlineView:(NSNotification*)notif;
- (IBAction)addFolder:(id)sender;
- (IBAction)addDocument:(id)sender;
- (IBAction)addResult:(id)sender;
-(void)addAnItemToFolder:(NSNotification*)notif;
-(void)addFile :(NSString*)path toFolderWithUniqueCode:(NSNumber*)uc;
-(id)findFolderWithUniqueCode:(NSNumber*)uc fromThisArray:(NSArray*)array;
- (IBAction)removeItem:(id)sender;
-(IBAction)setting:(id)sender;
- (void)outlineView:(NSOutlineView *)ov willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item;
- (int)outlineView:(NSOutlineView *)ov numberOfAllChildrenOfItem:(id)item ;
- (int)outlineView:(NSOutlineView *)ov numberOfChildrenOfItem:(id)item ;
- (BOOL)outlineView:(NSOutlineView *)ov isItemExpandable:(id)item ;
- (id)outlineView:(NSOutlineView *)ov child:(int)index ofItem:(id)item ;
- (id)outlineView:(NSOutlineView *)ov objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item ;
- (void)outlineView:(NSOutlineView *)ov setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item;
- (BOOL)outlineView:(NSOutlineView *)ov acceptDrop:(id <NSDraggingInfo>)info item:(id)item childIndex:(int)index;
-(BOOL)outlineViewItem:(id)rowItem1 isEqualToItem:(id)rowItem2;
-(NSMutableDictionary*)itemDictionaryForPath:(NSString*)path;
-(BOOL)thisItemIsUndeletable:(id)item;
-(void)deleteRowInOutlineView:(int)rowToBeDeleted;
-(NSMutableArray*)childrenItemsOf:(id)item;
- (BOOL)outlineView:(NSOutlineView *)ov writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard;
- (NSDragOperation)outlineView:(NSOutlineView *)ov validateDrop:(id <NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(int)index;
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item;
-(void)searchFinished;
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item;
-(void)missingItem:(id)item;
-(NSMutableArray*)expandChildrenFor:(NSMutableArray*)array;
- (void)outlineViewSelectionDidChange:(NSNotification *)aNotification;


@end
