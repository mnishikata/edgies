#import "AppDelegate.h"
#import "AvoidNullStringValueTransformer.h"


@implementation AppDelegate


+ (void)initialize {
    if (self == [AppDelegate class]) {   
		
		
		NSValueTransformer *mbTrans = [[AvoidNullStringValueTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"AvoidNullStringValueTransformer"];
		[mbTrans release];
		
		
		
    }
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	attachmentCellConverter = [[AttachmentCellConverter alloc] init];

	
	aliasInspector = [[AliasInspector alloc] init];
	faviconInspector = [[FaviconInspector alloc] init];

	graphicInspector = [[GraphicInspector  alloc] init];
	checkboxInspector = [[CheckboxInspector  alloc] init];

	NSArray* array = [[NSUserDefaults standardUserDefaults] objectForKey:@"MISC_UD_DICTIONARY"];
	
	
	NSData* data = [NSPropertyListSerialization dataFromPropertyList:array
															  format:NSPropertyListXMLFormat_v1_0
													errorDescription:nil];
	
	NSMutableArray* marray = [NSPropertyListSerialization propertyListFromData:data
															  mutabilityOption:NSPropertyListMutableContainersAndLeaves
																		format:nil
															  errorDescription:nil];
	
	
	[[MISC_UD_CONTROLLER content] removeAllObjects];
	[[MISC_UD_CONTROLLER content] addObjectsFromArray: marray];
	[MISC_UD_CONTROLLER rearrangeObjects];
}
- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	NSArray* MISC_UD_CONTROLLER_content = [MISC_UD_CONTROLLER content];
	
	
	[[NSUserDefaults standardUserDefaults] setObject:MISC_UD_CONTROLLER_content
											  forKey:@"MISC_UD_DICTIONARY"];
		
}

-(AliasInspector*)aliasInspector
{
	return aliasInspector;
}

-(FaviconInspector*)faviconInspector
{
	return faviconInspector;
}

-(AttachmentCellConverter*)attachmentCellConverter
{
	return attachmentCellConverter;
}

-(GraphicInspector*) graphicInspector
{
	return graphicInspector;
}

-(CheckboxInspector*)checkboxInspector
{
	return checkboxInspector;
}

@end
