//
//  MyDocument.m
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import "MiniDocument.h"
#import "HMBlkScroller.h"

#import "AppController.h"
#import "TabDocumentController.h"

#import "ScreenUtil.h"
#import "ZoomTextView.h"
#import "SoundManager.h"

#import "NSString (extension).h"

#import "EdgiesLibrary.h"
#import "FileLibrary.h"
#import "InstallLibrary.h"
#import "ClipboardArchiveAttachmentCell.h"

#import "NSData+CocoaDevUsersAdditions.h"

#import "SaferTextStorage.h"

//#define DEFAULT_BGCOLOR [NSColor colorWithCalibratedRed:255.0/255.0 green:243.0/255.0 blue:199.0/255.0 alpha:1.0]


@implementation MiniDocument

- (id)init
{
 
	self = [super init];
    if (self) {
		
		
		//
			creationDateStr = nil;
		documentColor = nil;
		isDocumentEdited = NO;
		propertyDisclosed = NO;
		filePathOfMe = nil;
		fileAliasOfMe = nil;
		hidden = NO;
		//
		
		
		NSColor* colorWellColor = [NSUnarchiver unarchiveObjectWithData:  preferenceValueForKey(@"defaultColorWell") ];
		
		documentColor = [ [colorWellColor colorUsingColorSpaceName: NSCalibratedRGBColorSpace] retain]; 

		
		propertyDisclosed = NO; // ??
		p_title = [[NSString alloc] init];
		p_subject  = [ preferenceValueForKey(@"p_subject") retain];
		p_author  = [   preferenceValueForKey(@"p_author")  retain];
		p_company  = [   preferenceValueForKey(@"p_company")  retain];
		p_copyright  = [  preferenceValueForKey(@"p_copyright")   retain];
		p_keywords  = [  preferenceValueForKey(@"p_keywords")   retain];
		p_comment  = [  preferenceValueForKey(@"p_comment")   retain];
		
		
		CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
		CFStringRef uuidstr = CFUUIDCreateString( kCFAllocatorDefault ,uuid);
		CFRelease(uuid);
		
		recordIdentifier = [[NSString stringWithString:uuidstr] retain];
		CFRelease(uuidstr);

		
		[NSBundle loadNibNamed:@"MiniDocument"  owner:self];
		
		NSScrollView* sc = [textView enclosingScrollView];
		HMBlkScroller* blkScroller = [[[HMBlkScroller alloc] initWithFrame:[[sc verticalScroller] frame]] autorelease];
		[blkScroller setControlSize: NSSmallControlSize ];
		[sc setVerticalScroller: blkScroller];
		
		
		
		[objectController setContent:self];

		[textView setDrawsBackground:NO];
		[[textView enclosingScrollView] setDrawsBackground:NO];
		
		

		// load ud if available
		
		NSNumber* _defaultTabPosition = preferenceValueForKey(@"userDefaultTabPosition");
		NSString* _defaultFrame = preferenceValueForKey(@"userDefaultFrame");
		
		

		
		int _tabPosition = tab_top;
		NSRect _windowFrame = [window frame];
		
		if( _defaultTabPosition != nil )
			_tabPosition = [_defaultTabPosition intValue];
		else
			_tabPosition = tab_top;
		
		
		if( _defaultFrame != nil )
			_windowFrame = NSRectFromString( _defaultFrame );
		else
			_windowFrame = NSMakeRect(0,0,300,200);
		
		
		

		

		
		// set id
    	creationDateStr = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ] retain];
		

		


		
		// set window color
		NSColor* defualt_bgColor = [NSUnarchiver unarchiveObjectWithData: preferenceValueForKey(@"defaultColorWell") ];
		
		[self setDocumentColor:defualt_bgColor ];
		
		
		[self setDefaultTitle];
		
		
		[window setFrame:_windowFrame display:NO];
		[window setTabPosition: _tabPosition];
		

		//[window autorelease];

		
		
		/// document edit
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(documentEdited) name:NSTextStorageDidProcessEditingNotification object: [textView textStorage]  ];
		
		
		isDocumentEdited = NO;
		
		

		
    }
    return self;
}
-(void)showWindow
{
	[window orderFront:self];
	
}

-(void)close
{
	
	if( ! [textView resignFirstResponder] )  
	{
		NSBeep();
		return;
	}		
	
	[objectController setContent:nil];

	
	if( preferenceBoolValueForKey(@"donotShowDiscardDialogue") == YES )
	{
		
		//[window closeTab];
		
		[[TabDocumentController sharedTabDocumentController] destroy:self];
		
		[window setDelegate:nil];
		
		[window close];
		
		
	}else
	{
		
		[self showSheet];
	}

	
}

-(void)closeForceSaving
{

	[self save];
	[objectController setContent:nil];
	
	[[[TabDocumentController sharedTabDocumentController] documents] removeObject:self];

	
	
	[window setDelegate:nil];
	
	[window close];
		
	
	
}




-(void)showSheet
{
	
	
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	

	[window setTemporarilyTornOff:YES];
	
	[closeSheet setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	[[NSApplication sharedApplication] beginSheet:closeSheet
								   modalForWindow:window
									modalDelegate:self
								   didEndSelector:@selector(sheetDidEnd: returnCode: contextInfo:)
									  contextInfo:nil];
	
	
}

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo
{
	[window setTemporarilyTornOff:NO];

	if( returnCode == 0 )
	{

		
		
	}else
	{		
		
		
		//[window closeTab];
		
		[[TabDocumentController sharedTabDocumentController] destroy:self];
		
		[window setDelegate:nil];

		[window close];
		
	}
	
}

-(IBAction)sheetButtonClicked:(id)sender
{
	// -1 cancel, 0 discard, 1 export	
	
	if( [sender tag] == 0 )
	{
		
		[closeSheet orderOut:self];
		[[NSApplication sharedApplication] endSheet:closeSheet  returnCode:1];
		
		return;
		
	}else if( [sender tag] == 1 )
	{
		[self export];	
		[closeSheet orderOut:self];
		[[NSApplication sharedApplication] endSheet:closeSheet  returnCode:1];

		return;
		
		
		
	}else	{
		
		[closeSheet orderOut:self];
		[[NSApplication sharedApplication] endSheet:closeSheet returnCode:0];
		
		return;
	}
	
	
	
	
	
}

#pragma mark -



-(void)dealloc
{
	//NSLog(@"# minidoc dealloc");
	////NSLog(@"# minidoc close ");
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	//[window setOwnerDocument:nil];
	//[window release]; //release when closed
	
	
	[p_title release];
	[p_subject release];
	[p_author release];
	[p_company  release];
	[p_copyright  release];
	[p_keywords  release];
	[p_comment release];
	
	[recordIdentifier release];
	
	//[window release];
	//[property release];
	
	[documentColor release];
	
	
	[super dealloc];
	
}

-(void)setDefaultTitle
{
		////////////
	int titleUntitled = preferenceIntValueForKey(@"titleUntitled");
		if(     titleUntitled   == 1  )
	{
			NSString* _ti = [[NSDate date]  descriptionWithCalendarFormat:
											      preferenceValueForKey(@"dateFormat")   timeZone:nil   locale:nil];
			
			[self setValue:_ti forKey:@"p_title"];
			
			
	}else if(    titleUntitled == 2  )
	{
		
		
		[self setValue: NSLocalizedString(@"Untitled", @"") forKey:@"p_title"];
		
		
		
		
	}else // if(  PREF_titleUntitled() == 0  )
	{
		int untitledCount = [[TabDocumentController sharedTabDocumentController] untitledCount];
		
		
		NSString* _ti = [NSString stringWithFormat:   NSLocalizedString(@"Untitled %d", @"")  ,untitledCount];
		[self setValue:_ti forKey:@"p_title"];
		
		
		
	}
}
#pragma mark property
-(NSDictionary*)documentProperty
{
	NSDate* creationDate = [NSDate dateWithTimeIntervalSince1970:[creationDateStr doubleValue]];

	
	NSArray* propertyKeywordsArray;

	if( ! [p_keywords isKindOfClass:[NSArray class]] )
		propertyKeywordsArray = [NSArray array];
	else
		propertyKeywordsArray = [NSArray arrayWithArray:  p_keywords];
		

	
	NSDictionary* docAttr = [NSDictionary dictionaryWithObjectsAndKeys:
		p_title,	NSTitleDocumentAttribute ,
		p_subject,	NSSubjectDocumentAttribute,
		p_author,	NSAuthorDocumentAttribute,
		p_company,	NSCompanyDocumentAttribute,
		p_copyright,	NSCopyrightDocumentAttribute,
		propertyKeywordsArray,	NSKeywordsDocumentAttribute,
		p_comment,	NSCommentDocumentAttribute,
		creationDate	,	NSCreationTimeDocumentAttribute,
		
		recordIdentifier, SyncRecordIdentifierKey,
		
		nil];
	
	
	return docAttr;
}
-(NSString*)string
{
	return [[textView textStorage] string];
}
-(NSString*)recordIdentifier
{
	return recordIdentifier;
}

-(NSString*)p_title
{
	return p_title;
}
-(NSString*)p_subject
{
	return p_subject;
}
-(NSString*)p_author
{
	return p_author;
}
-(NSString*)p_company
{
	return p_company;
}
-(NSString*)p_copyright
{
	return p_copyright;
}
-(NSArray*)p_keywords
{
	return p_keywords;
}
-(NSString*)p_comment
{
	return p_comment;//propertyKeywordsArray;
}
-(NSString*)p_creationDate
{
	NSDate* creationDate = [NSDate dateWithTimeIntervalSince1970:[creationDateStr doubleValue]];
	return [creationDate descriptionWithLocale:nil];//propertyKeywordsArray;
}
-(NSDate*)creationDate
{
	return 	[NSDate dateWithTimeIntervalSince1970:[creationDateStr doubleValue]];

}
-(NSString*)p_color
{
	NSColor* _color = [documentColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace ];
	
	float red, green, blue, alpha;
	[_color getRed:&red green:&green blue:&blue alpha:&alpha];
	
	return [NSString stringWithFormat:@"%f%f%f",red,green,blue];
}
-(void)setP_title:(NSString*)newTitle
{
	[self setTitle:newTitle];
	
}

-(void)setRecordIdentifier:(NSString*)string
{
	if( recordIdentifier != string )
	{
		[recordIdentifier release];
		recordIdentifier = [ string retain];
	}
}

-(BOOL)isHidden
{
	return hidden;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
	if( [key isEqualToString:@"hidden"] )
	{
		[self setHidden:[value boolValue]];
		
	}else
		[super setValue:value forKey:key];
}


-(void)setHidden:(BOOL)flag
{
	hidden = flag;
	
	if( flag == NO )
	{
		[window orderFront:self];
	}else
	{
		[window orderOut:self];

	}
	[[NSNotificationCenter defaultCenter] postNotificationName:MemoHiddenStatusDidChangeNotification
														object:self];
	
	////NSLog(@"MemoHiddenStatusDidChangeNotification");
}


-(void)setFilePath:(NSString*)path
{
	
	
	[fileAliasOfMe release];
	fileAliasOfMe = [NDAlias aliasWithPath: path];
	[fileAliasOfMe retain];
	
}

-(NSString*)filePath
{
	return [fileAliasOfMe path];
}

-(NSData*)documentAttributes
{
	NSMutableDictionary* dic = [NSMutableDictionary dictionary];
	
	if( recordIdentifier != nil )
		[dic setObject:recordIdentifier forKey: SyncRecordIdentifierKey];
	
	[dic setObject:documentColor forKey:@"color"];
	[dic setObject:p_title forKey:@"title"];
	[dic setObject:NSStringFromRect( [window frame] )  forKey:@"rect"];
	

	//[dic setObject:[textView portableDateFromRange:[textView fullRange]] forKey:@"content"];
	NSAttributedString *attrs = [[textView textStorage] attributedSubstringFromRange: [textView fullRange] ];
	
	NSData* data = [[NSKeyedArchiver archivedDataWithRootObject:attrs] deflate];
	[dic setObject:data forKey:@"contentArchive"];
		

	[dic setObject:[NSNumber numberWithInt:[window tabPosition]] forKey:@"tabPosition"];
	[dic setObject:[NSNumber numberWithInt:[window tabOpenStatus]] forKey:  @"tabOpenStatus"];
	[dic setObject:creationDateStr		forKey:@"name"];
	
	[dic setObject:[NSNumber numberWithFloat:[(ZoomTextView*)textView zoom]]		forKey:@"zoom"];
	

	
	[dic setObject:p_title		forKey:@"p_title"];
	[dic setObject:p_subject	forKey:@"p_subject"];
	[dic setObject:p_author		forKey:@"p_author"];
	
	[dic setObject:p_company	forKey:@"p_company"];
	[dic setObject:p_copyright	forKey:@"p_copyright"];
	[dic setObject:p_keywords	forKey:@"p_keywords"];
	[dic setObject:p_comment	forKey:@"p_comment"];
	////NSLog(@"p_subject %@\n p_author %@\n,p_company %@\n p_copyright%@\n, p_keywords %@\n p_comment%@\n",p_subject,p_author,p_company,p_copyright,[p_keywords description],p_comment );
	

	
	NSString* textContent = [[textView textStorage] string];
	textContent = [textContent stringByAppendingString:@"\n<ThisTextWasCreatedByEdgies>"];
	[dic setObject:textContent	forKey:@"spotlightTextContent"];
	
	
	[dic setObject:[NSNumber numberWithBool:hidden] forKey:@"hidden"];
	
	
	[dic setObject:VERSION_STRING forKey:@"version"];
	
	//NSLog(@"savingend");
	
	return [NSKeyedArchiver archivedDataWithRootObject:dic];
}



-(void)setDocumentAttributes:(NSData*)data
{
	//NSLog(@"setDocumentAttributes");
	
	id value;
	
	if( data == nil ) return;
	
	[window orderOut:self];
	[objectController setContent:nil];

	
	id dic = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	
	NSRect winFrame = NSRectFromString(  [dic objectForKey:@"rect"]  );
	
	[window setFrame:winFrame display:NO];
	[window setScreenID: screenIDWhereRectIs(winFrame) ];

	
	
	
	value = [dic objectForKey:@"tabPosition"];
	
	if( value != nil ) 		
	[window setTabPosition:[[dic objectForKey:@"tabPosition"] intValue]];
	

	value = [dic objectForKey:@"tabOpenStatus"];
	
	if( value != nil ) 
		[window setTabStatus: [value intValue]];
	
	[self setDocumentColor:[dic objectForKey:@"color"]];
	

	[textView setBackgroundColor:[dic objectForKey:@"color"]];
	
	[(NSClipView*) [textView  superview] setBackgroundColor:[dic objectForKey:@"color"]]; //clipview
	

	
	float _zoom;
	if( [dic objectForKey:@"zoom"] == nil )
		_zoom = 1.0;
	else
		_zoom = [[dic objectForKey:@"zoom"] floatValue];
	
	if( _zoom < 0.01 ) _zoom = 1.0;

	[ (ZoomTextView*)textView setZoom:_zoom];
	


	[self setValue:[dic objectForKey:@"title"] forKey:@"p_title"];
	

	//	NSData* data = [[NSKeyedArchiver archivedDataWithRootObject:[textView textStorage]] deflate];
	//[dic setObject:data forKey:@"contentArchive"]
	
	if( [dic objectForKey:@"content"] != nil ) // Ediges 1.1
	{

		NSAttributedString* attr =  unpackPortable( [dic objectForKey:@"content"] );
			
		
		[[textView textStorage] setAttributedString:  convertEdgies1point1(attr) ];
		
		
		
		
	}else if( [dic objectForKey:@"contentArchive"] != nil )
	{
		NSAttributedString* ts;

@try {
	ts =  [NSKeyedUnarchiver unarchiveObjectWithData:[[dic objectForKey:@"contentArchive"] inflate] ] ;
	[[textView textStorage] setAttributedString:ts];	

	
}
@catch (NSException * e) {

}


		
		//SaferTextStorage* saferTextStorage = [[[SaferTextStorage alloc] init] autorelease];
		
		//[[textView layoutManager] replaceTextStorage: saferTextStorage];

		
		
		

	}
	

	[creationDateStr release];
	creationDateStr = [dic objectForKey:@"name"];
	[creationDateStr retain];
	
	
	id va;
	
	va= [dic objectForKey:@"p_title"];
	if( va!=nil )[self setValue:va forKey:@"p_title"];
	
	va= [dic objectForKey:@"p_subject"];
	if( va!=nil )[self setValue:va forKey:@"p_subject"];
	
	va= [dic objectForKey:@"p_author"];
	if( va!=nil )[self setValue:va forKey:@"p_author"];
	
	va= [dic objectForKey:@"p_company"];
	if( va!=nil )[self setValue:va forKey:@"p_company"];
	
	va= [dic objectForKey:@"p_copyright"];
	if( va!=nil )[self setValue:va forKey:@"p_copyright"];
	
	va= [dic objectForKey:@"p_keywords"];
	if( va!=nil )[self setValue:va forKey:@"p_keywords"];
	
	va= [dic objectForKey:@"p_comment"];
	if( va!=nil )[self setValue:va forKey:@"p_comment"];
	
	
	
	hidden = [[dic objectForKey:@"hidden"] boolValue];
	
	
	va= [dic objectForKey: SyncRecordIdentifierKey ];
	if( va!=nil )[self setRecordIdentifier: va  ];

	
	//[window closeTab];
	//[window makeKeyAndOrderFront:self];
	
	////NSLog(@"opening %@",[dic objectForKey:@"title"]);
	//	[window redrawWithShadowProperly];
	
	if( !hidden)
		[window orderFront:self];
	
	
	[window redrawWithShadowProperly];
	
	[objectController setContent:self];

	
	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
}

-(BOOL)setCreationDateAtPath:(NSString*)path
{
	BOOL flag = YES;
	
	NSFileManager* fm = [NSFileManager defaultManager];
	
	NSMutableDictionary* fileAttr;
	fileAttr = [NSMutableDictionary dictionaryWithDictionary: [fm fileAttributesAtPath: path traverseLink:NO]];
	
	if( [creationDateStr doubleValue] != 0 )
	{
		NSDate* creationDate = [NSDate dateWithTimeIntervalSince1970:[creationDateStr doubleValue]];
		
		[fileAttr setObject:creationDate forKey:NSFileCreationDate];
		flag = [fm changeFileAttributes:fileAttr atPath: path];
		
	}
	
	
	return flag;
}

-(void)setDocumentColor:(NSColor*)color
{
	if( color == nil ) color = [NSColor yellowColor];
	else
	{
		//convert color space
		
		color = [color colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
		
	}
	
	//NSLog(@"setting document color %@",[color description]);

	[documentColor release];
	documentColor = [color retain];
	
	//[window setThemeColor:color];
	
	[window redrawWithShadowProperly];
		
	
	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
	//[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowColorDidChange
	//													object:self];
	
	
}

-(NSColor*)documentColor
{
	//NSLog(@"Passing document color %@",[[documentColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace] description]);
	
	
	
	
	return [documentColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
	
}
-(TAB_POSITION)tabPosition
{
	return [window tabPosition];
}

#pragma mark -
#pragma mark Action
-(IBAction)print:(id)sender
{
	[textView print:self];
	
	
}
	
	

- (void)export
{
	[self export:nil];
	

}

-(IBAction)export:(id)sender
{
	
	
	if( preferenceBoolValueForKey(@"exportWithoutDialogue") )
	{
		NSString* folderToSave = preferenceValueForKey(@"exportPath");
				
		NSString* _name = [self exportInFolder:folderToSave plainText:NO
								 preferredName:nil changeFilename:YES];
		if( _name == nil ) return ;
		return;// YES;
	}


	
	//start dialogue
	
	int levelBuffer = [window level];
	int windowLevelBuffer = preferenceIntValueForKey(@"windowLevel");
	int windowLevelOpenedBuffer = preferenceIntValueForKey(@"windowLevelOpened");
	
	[window setLevel:NSNormalWindowLevel];
	[window setValue:[NSNumber numberWithInt:  NSNormalWindowLevel] forKey: @"windowLevel"];
	[window setValue:[NSNumber numberWithInt:  NSNormalWindowLevel] forKey: @"windowLevelOpened"];
	
	[window display];


	
	
	
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];

	BOOL success = NO;
	NSSavePanel* aPanel = [NSSavePanel savePanel]; 
	if( [aPanel runModalForDirectory:nil file:p_title] == NSFileHandlingPanelOKButton )
	{ 

		NSString* filePath = [aPanel filename];
		if( ![[[filePath pathExtension] lowercaseString] isEqualToString:@"rtfd"] )
		{
			filePath = [filePath stringByAppendingPathExtension:@"rtfd"];
			filePath = [filePath uniquePathForFolder];
		}
			

		NSString* _name = [self exportInFolder:[filePath stringByDeletingLastPathComponent] plainText:NO
								 preferredName:[filePath lastPathComponent] changeFilename:YES];
		if( _name == nil ) success == NO;
		else success == YES;

	} 
	
	
	[window setLevel:levelBuffer];
	
	[window setValue:[NSNumber numberWithInt:  windowLevelBuffer] forKey: @"windowLevel"];
	[window setValue:[NSNumber numberWithInt:  windowLevelOpenedBuffer]
			forKey: @"windowLevelOpened"];
	
	
	
	
	//return success;
}

-(NSString*)stringRepresentation
{
	return [[AttachmentCellConverter sharedAttachmentCellConverter] 
			convertAttributedStringToString:[textView textStorage]];


}

-(NSFileWrapper*)RTFDRepresentation
{
	NSFileWrapper *textStorage_rtfd_wrapper = [[AttachmentCellConverter sharedAttachmentCellConverter] 
			convertAttributedStringToRTFDFileWrapper:[textView textStorage] documentAttributes:[self documentProperty]];

	return textStorage_rtfd_wrapper;
}

-(NSString*)exportInFolder:(NSString*)folderToSave plainText:(BOOL)flag preferredName:(NSString*)name changeFilename:(BOOL)changeFilename
{
	NSString* title;
	NSDictionary* docAttr = [self documentProperty];
	BOOL success = NO;
	NSString* savedPath;
	
	if( flag == NO ) // rich text
	{

		NSFileWrapper *textStorage_rtfd_wrapper = [self RTFDRepresentation];

		/*
		if(    preferenceBoolValueForKey(@"convertCheckbox")    )
		{
			NSMutableAttributedString* attr = [textView convertCheckboxesInRange:[textView fullRange]];
			attr = [textView ndaliasProofAttributedString:[attr attributedSubstringFromRange:[textView fullRange]]];
			
			textStorage_rtfd_wrapper = [attr RTFDFileWrapperFromRange:NSMakeRange(0,[[attr string] length])
							documentAttributes:docAttr ];
		}
		else
		{
			NSMutableAttributedString* attr = 
			[textView ndaliasProofAttributedString:[[textView textStorage]
								attributedSubstringFromRange:[textView fullRange]]];
			
			textStorage_rtfd_wrapper = [attr RTFDFileWrapperFromRange:NSMakeRange(0,[[attr string] length])
							documentAttributes:docAttr ];
			
		}
		*/
		
		

		
		if( name == nil ) 
			title = [NSString stringWithFormat:@"%@.rtfd",[window title]];
		else
			title = name;

		
		if( changeFilename == YES &&
			[[NSFileManager defaultManager] fileExistsAtPath:folderToSave isDirectory:nil]  );
		{
			title = [[title safeFilename] uniqueFilenameForFolder:folderToSave];
		}
			
		savedPath = [folderToSave stringByAppendingPathComponent:title];
		success = [textStorage_rtfd_wrapper writeToFile: savedPath
											   atomically:YES updateFilenames:YES]; 
		
		


		

	}else // plain text
	{
		
		NSString* str = [[AttachmentCellConverter sharedAttachmentCellConverter] 
			convertAttributedStringToString:[textView textStorage] ];

		NSData *textData = [str dataUsingEncoding:NSUTF8StringEncoding];

		
		if( name == nil ) 
			title = [NSString stringWithFormat:@"%@.txt",[window title]];
		else
			title = name;
		
		
		
		if( changeFilename == YES &&
		 [[NSFileManager defaultManager] fileExistsAtPath:folderToSave isDirectory:nil]  );
		{
			title = [[title safeFilename] uniqueFilenameForFolder:folderToSave];
		}
			
		savedPath = [folderToSave stringByAppendingPathComponent:title];
		success = [textData writeToFile: savedPath  atomically:YES  ]; 
		
		
	}
	
	
	
	if( success == NO )
	{
		NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Exporting Failed",@"") 
										 defaultButton:NSLocalizedString(@"OK",@"")
									   alternateButton:@"" otherButton:@""
							 informativeTextWithFormat:NSLocalizedString(@"The document was not exported.",@"")];
		
		[alert runModal];
		
		return nil;
	}
	
	if(   preferenceBoolValueForKey(@"exportDate") ) //set creation date
	{
		[self setCreationDateAtPath:savedPath];
	}
	
	[[SoundManager sharedSoundManager] playSoundWithName:@"exportSound"];

	[[NSNotificationCenter defaultCenter] postNotificationName:EdgyExportedNotification
														object:savedPath ];
	return title;
}


-(void)documentEdited
{
	////NSLog(@"documentEdited");
	
	if( isDocumentEdited == NO )
	{
		if( [window isOpened] )
		{
			[window setCanBecomeMainWindow:YES];
			[window makeMainWindow];
		}
	}
	
	isDocumentEdited = YES;
	
	[[TabDocumentController sharedTabDocumentController] setBusy:YES];
}



//////

-(NSTextView*)textView
{
	return textView;
}
-(void)save
{
	if( creationDateStr == NULL )
		return;

	
	
	NSString* path = [self filePath];
	
	if( path != nil )
		[self setFilePath:path];

	[[TabDocumentController sharedTabDocumentController] saveThisData:[self documentAttributes] atPath:path defaultName:p_title document:self];

	isDocumentEdited = NO;

}

-(void)saveIfNeeded
{
	if( creationDateStr == NULL )
		return;
	//creationDateStr = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ] retain];
	
	if( isDocumentEdited == YES )
	{
		//[AppController saveThisData:[self documentAttributes] withName: creationDateStr];
		////NSLog(@"saveIfNeeded");
		isDocumentEdited = NO;

		[self save];
		
	
	}
	
	
}

-(void)redrawWindow
{
	[window setupTabs];
	[window redrawWithShadowProperly];
}
-(MNTabWindow*)window
{
	return window;
}


-(NSString*)name
{
	return creationDateStr;
}
-(IBAction)discloseClicked:(id)sender
{
	
	BOOL flag = ([sender state] ==NSOnState? YES : NO);
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setBool:flag forKey:@"propertyDisclosed"];
	
	
	
	NSRect propertyRect = [property frame];
	
	
	if( [sender state] == NSOffState )
	{
		propertyRect.size.height -= 200;
		propertyRect.origin.y += 200;
		
		
	}else
	{
		propertyRect.size.height += 200;
		propertyRect.origin.y -= 200;
		
	}
	
	[property setFrame:propertyRect display:YES animate:YES];
	
	return;	
}

-(void)showProperty
{
	// default disclosure
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	BOOL flag = [ud boolForKey:@"propertyDisclosed"];

	if( flag == NO )
	{
		if( [discloseButton state] == NSOnState )
		[discloseButton performClick:self];
	}else
	{
		if( [discloseButton state] == NSOffState )
			[discloseButton performClick:self];

	}
	
	
	
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	
	

	[window setTemporarilyTornOff:YES];
	
	[window openTabBlocking:YES];
	
	[property setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	[[NSApplication sharedApplication] beginSheet:property
								   modalForWindow:window
									modalDelegate:self
								   didEndSelector:@selector(propertyDidEnd: returnCode: contextInfo:)
									  contextInfo:nil];
	
	[window makeFirstResponder: propertyTitleField ];
	
}

- (void)propertyDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)contextInfo
{
	[window setTemporarilyTornOff:NO];

	
	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
}
-(IBAction)propertyCloseClicked:(id)sender
{ 
	[property makeFirstResponder:sender];
	
	[property orderOut:self];
	[[NSApplication sharedApplication] endSheet:property returnCode:0];
	
	
	[self documentEdited];
	
	return;
	
}

-(void)setTitle:(NSString*)newTitle
{
	
	
	[p_title release];
	p_title = [newTitle retain];
	
	[window setupTabs];

	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
	
	
	if(  preferenceBoolValueForKey(@"allowRename")   == NO ) return;
	
	///
	
	
	if( fileAliasOfMe != nil )
	{
		NSString* myPath = [fileAliasOfMe path];
		
		if( myPath != nil && [[myPath lowercaseString] hasSuffix:@".edgy"] )
		{
			
			NSString* myFilename = [myPath lastPathComponent];
			NSString* newFilename = [[NSString stringWithFormat:@"%@.edgy",newTitle] safeFilename];
			
			

			
			if( ! [[myPath lastPathComponent] isEqualToString:newFilename ] )
			{
				newFilename = [newFilename uniqueFilenameForFolder:[myPath stringByDeletingLastPathComponent]];

				
				NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
				[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:DOCUMENT_FOLDER];
				
				
				FSRef myref;
				CFURLRef	url = CFURLCreateWithFileSystemPath(NULL /*allocator*/, (CFStringRef)myPath, kCFURLPOSIXPathStyle, NO /*isDirectory*/);
				if (url != NULL) {
					if (CFURLGetFSRef(url, &myref)) {
						
						
						unichar buffer[1024];
						OSErr err;
						
						[newFilename getCharacters:buffer];
						
						err = FSRenameUnicode(&myref,
											  [newFilename length],
											  (const UniChar *) buffer,
											  kTextEncodingUnknown,
											  nil);
						
						
						NSString* newPath = [[myFilename stringByDeletingLastPathComponent] stringByAppendingPathComponent: newFilename];
						////NSLog(@"new path %@",newPath);
						//[self setFilePath:newPath];
						
						
						
						if( noErr != err )
						{
							NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Renaming Failed",@"")
															 defaultButton:@"OK" alternateButton:@"" otherButton:@"" 
												 informativeTextWithFormat:NSLocalizedString(@"Renaming document was failed. The file may be locked.",@"")];
							
							int result = [alert runModal];
						}
						
						CFRelease(url);
						
					}
				}
			}
			

		}

		
	}
	
	

	
}
#pragma mark-
-(NSComparisonResult)positionCompare:(MiniDocument*)anotherDoc
{
	
	NSRect rect1 = [[self window] closedFrame];
	NSPoint point1 = NSMakePoint( rect1.origin.x + rect1.size.width/2,
								  rect1.origin.y + rect1.size.height/2 );
	
	NSRect rect2 = [[anotherDoc window] closedFrame];
	NSPoint point2 = NSMakePoint( rect2.origin.x + rect2.size.width/2,
								  rect2.origin.y + rect2.size.height/2 );
	
	
	if( [ [self window] tabPosition] == tab_top || [[self window] tabPosition] == tab_bottom )
	{
		return [[NSNumber numberWithFloat:point1.x] compare:[NSNumber numberWithFloat:point2.x]];
		
	}
	
	return [[NSNumber numberWithFloat:point2.y] compare:[NSNumber numberWithFloat:point1.y]];
	
}


- (NSRect)window:(NSWindow *)window willPositionSheet:(NSWindow *)sheet usingRect:(NSRect)rect
{
	rect.origin.y -= 25;
	return rect;
}

@end
