#import <Cocoa/Cocoa.h>


@interface ColorCheckbox : NSObject {

	
	
	NSImage* cutImage; // cut
	
	NSImage* glassImage; // reflection image
	
	NSImage* shadowImage; // shadow
	
	NSImage* checkImage; // check mark
	
}
+ (NSImage*)colorCheckbox:(NSColor* )aColor checkFlag:(BOOL)flag;
// return color checkbox image

+ (NSImage*)roundedBox:(NSColor*)aColor size:(NSSize)boxSize curve:(float)boxCurve 
			frameColor:(NSColor*)frameColor;// return roounded rectangle image
// return roounded rectangle image





+(NSColor*)convertColorSpace:(NSColor*)color;

@end
