#import "ColorCheckbox.h"

@implementation ColorCheckbox

static NSImage* cutImage; // cut
static NSImage* glassImage; // reflection image
static NSImage* shadowImage; // shadow
static NSImage* checkImage; // check mark

+(NSColor*)convertColorSpace:(NSColor*)color
{
	return 	[color colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
	
}
+(void)initialize
{
	   if ( self == [ColorCheckbox class] ) {

		  // //NSLog(@"initialize");
	   
		   NSBundle* aSelfBundle;
		   aSelfBundle =[NSBundle bundleForClass:[self class]];
		   
		   //base color 
		   
		   // cut
		   cutImage = [[NSImage alloc] initByReferencingFile:[aSelfBundle pathForResource:@"chkbxShape" ofType:@"png"]];	
		   
		  // reflection image
		   glassImage = [[NSImage alloc] initByReferencingFile:[aSelfBundle pathForResource:@"chbx_Normal" ofType:@"png"]];	
		   
		   // shadow
		   shadowImage = [[NSImage alloc] initByReferencingFile:[aSelfBundle pathForResource:@"chkbxShadow" ofType:@"png"]];	
		   
		   // check mark
		   checkImage = [[NSImage alloc] initByReferencingFile:[aSelfBundle pathForResource:@"chkbxCheck" ofType:@"png"]];	
	   
	   }
}

+ (NSImage*)colorCheckbox:(NSColor* )aColor checkFlag:(BOOL)flag  // return color checkbox image
{
	
	aColor =		[ColorCheckbox convertColorSpace:aColor];

	
	
	//preparation
	NSPoint anOrigin = NSMakePoint(0,0);
	float red, green, blue, alpha;
	[aColor getRed:&red green:&green blue:&blue alpha:&alpha];
	
	//base
	NSImage* colorImage = [[[NSImage alloc] initWithSize:NSMakeSize(16,17)] autorelease];

	// draw and composit
	
	[colorImage lockFocus];
	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBFillColor(context, red, green, blue, alpha);
	CGContextBeginPath(context);
	CGContextAddRect(context, CGRectMake(0,0,16,17));
	CGContextFillPath(context);
	
	//cut
	[cutImage compositeToPoint:anOrigin operation:NSCompositeDestinationIn];
	//reflection
	[glassImage compositeToPoint:anOrigin operation:NSCompositeSourceOver];
	// shadow
	[shadowImage compositeToPoint:anOrigin operation:NSCompositeSourceOver];
	//check mark
	if(flag == YES)
		[checkImage compositeToPoint:anOrigin operation:NSCompositeSourceOver];	
	
	
	//end
	[colorImage unlockFocus];

	
	return colorImage;
	
}

+ (NSImage*)roundedBox:(NSColor*)aColor size:(NSSize)boxSize curve:(float)boxCurve 
			frameColor:(NSColor*)frameColor// return roounded rectangle image
{
	
	//	float boxSize = 30;
	//	float boxCurve = 5;
	aColor =		[ColorCheckbox convertColorSpace:aColor];
	frameColor =	[ColorCheckbox convertColorSpace:frameColor];
	
	
	
	//
	NSImage* anImage;
	anImage = [[NSImage alloc] initWithSize:NSMakeSize(boxSize.width , boxSize.height)];
	
	float red, green, blue, alpha;
	[aColor getRed:&red green:&green blue:&blue alpha:&alpha];
	
	
	float fred, fgreen, fblue, falpha;
	[frameColor getRed:&fred green:&fgreen blue:&fblue alpha:&falpha];
	
	
	[anImage lockFocus];
	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBStrokeColor(context, red*0.85, green*0.85, blue*0.85, alpha);
	CGContextSetLineWidth(context, 2.0);
	CGContextSetRGBFillColor(context, red, green, blue, alpha);
	
	
	//Fill 
	CGContextBeginPath(context);
	
	addRoundedRectToPath(context, CGRectMake(0,0, boxSize.width, boxSize.height),boxCurve, boxCurve);	
	
	
	CGContextFillPath(context);
	
	addRoundedRectToPath(context, CGRectMake(.5,.5, boxSize.width -1, boxSize.height -1),boxCurve , boxCurve );	
	
	CGContextStrokePath(context);
	
	[anImage unlockFocus];
	
	return anImage;
	
}



+ (NSImage*)roundedBox:(NSColor*)aColor size:(NSSize)boxSize curve:(float)boxCurve  // return roounded rectangle image
{
	aColor =		[ColorCheckbox convertColorSpace:aColor];

	
//	float boxSize = 30;
//	float boxCurve = 5;
	
	//
	NSImage* anImage;
	anImage = [[NSImage alloc] initWithSize:NSMakeSize(boxSize.width , boxSize.height)];
	
	float red, green, blue, alpha;
	[aColor getRed:&red green:&green blue:&blue alpha:&alpha];
	
	
	[anImage lockFocus];
	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetRGBFillColor(context, red, green, blue, alpha);
	
	
	//Fill 
	CGContextBeginPath(context);
	addRoundedRectToPath(context, CGRectMake(0,0, boxSize.width, boxSize.height),boxCurve, boxCurve);	
	
	CGContextFillPath(context);
	
	[anImage unlockFocus];
	
	return anImage;
	
}


+ (void)roundedBoxFrame:(NSColor*)aColor frame:(NSRect)frame curve:(float)boxCurve fillColor:(NSColor*)fillColor openEnd:(int)openEnd
	//openEnd = 0  no
	//openEnd = 1 left open
	//openEnd = 2 right open
	//openEnd = 3 both open

{
	aColor =		[ColorCheckbox convertColorSpace:aColor];
	
	
	//	float boxSize = 30;
	//	float boxCurve = 5;
	
	//
	
	float red, green, blue;
	float redF, greenF, blueF;
	
	float hue, sat, brt, alpha;
	[aColor getHue:&hue saturation:&sat brightness:&brt alpha:&alpha];
	[aColor getRed:&red green:&green blue:&blue alpha:&alpha];
	

	
	[fillColor getRed:&redF green:&greenF blue:&blueF alpha:&alpha];
	
	
	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetRGBFillColor(context, redF, greenF, blueF, alpha);
	
	
	CGContextSetLineWidth ( context, 0.7 );
	
	//Fill 
	CGContextBeginPath(context);
	addRoundedRectWithOpenEndToPath(context, CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height),boxCurve, boxCurve, openEnd);	
	
	CGContextFillPath(context);
	
	
	
	//draw
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	
	
	CGContextSetLineWidth ( context, 0.7 );
	
	//stroke 
	CGContextBeginPath(context);
	addRoundedRectWithOpenEndToPath(context, CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height),boxCurve, boxCurve, openEnd);	
	
	CGContextStrokePath(context);
	
}




@end
