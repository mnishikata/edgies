//
//  MNIconizedCell.h
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "AttachmentCellConverter.h"
#import "ButtonAttachmentCell.h"
@interface CheckboxAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{

	NSString* name;
	NSColor* color;
	

}
+(NSAttributedString*)newCheckbox;
+(NSAttributedString*)checkboxWithColor:(NSColor*)color;
+(NSAttributedString*)checkboxWithColor:(NSColor*)color state:(int)state;
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
-(NSImage*)image;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
- (id)copyWithZone:(NSZone *)zone;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)action:(id)sender;
-(NSColor*)color;
-(NSString*)name;
-(void)setName:(NSString*)newName;
-(void)setColor:(NSColor*)newColor;
-(void)setState:(int)state;
-(void)setButtonSize:(float)size;
-(void)useAsDefault;
-(void)loadDefaults;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)textView;
-(NSColor*)legacyColor;
-(NSString*)legacyName;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context;
-(NSDictionary*)pasteboardRepresentation:(id)context;


@end


@protocol CheckboxAttachmentCellOwnerTextView 
//-(void)faviconAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)checkboxAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)checkboxAttachmentCellArrayInSelectedRange;
-(void)openCheckboxInspector;


@end

