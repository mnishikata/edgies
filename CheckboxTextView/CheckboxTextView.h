/* CheckboxTextView */

#import <Cocoa/Cocoa.h>
#import "GraphicResizableTextView.h"
#import "CheckboxAttachmentCell.h"
@interface CheckboxTextView : GraphicResizableTextView <CheckboxAttachmentCellOwnerTextView, ButtonAttachmentCellOwnerTextView>

{
	NSArray* checkboxTable;
}
- (id)initWithFrame:(NSRect)frame ;
- (void)awakeFromNib;
-(void)buttonAttachmentCellUpdated:(ButtonAttachmentCell*)cell;;
-(void)buttonAttachmentCellClicked:(ButtonAttachmentCell*)cell;;
-(void)buttonAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;;
-(NSArray*)checkboxAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)checkboxAttachmentCellArrayInSelectedRange;
-(void)openCheckboxInspector;
-(void)insertCheckbox:(id)sender;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;

@end
