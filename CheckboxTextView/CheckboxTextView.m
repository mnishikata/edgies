
#import "CheckboxTextView.h"
#import "CheckboxAttachmentCell.h"
#import "ColorCheckbox.h"
#import "EdgiesLibrary.h"
#import "CheckboxInspector.h"
#import "NSTextView (coordinate extension).h"

@implementation CheckboxTextView


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		
		[self awakeFromNib];
		
	}
    return self;
}




- (void)awakeFromNib
{
	
	NSAttributedString* attr;
	
	
	NSData* data = [[[NSData alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"checkboxTable" ofType:@"rtf"]] autorelease];
	
	attr = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil ] autorelease];
	
	NSArray* colornameArray = [[attr string] componentsSeparatedByString:@"\n"];
	
	////NSLog(@"%@",[colornameArray description]);
	
	NSMutableArray* array = [NSMutableArray array];
	
	int hoge;
	for( hoge = 0; hoge < [colornameArray count]; hoge++ )
	{
		
		NSString* str = [colornameArray objectAtIndex:hoge];
		
		if( ! [str isEqualToString:@""] )
		{
			NSRange range = [[attr string] rangeOfString:str];
			
			NSAttributedString* subAttr = [attr attributedSubstringFromRange:range];
			
			NSColor* color = [subAttr attribute:NSForegroundColorAttributeName
										atIndex:0
								 effectiveRange:nil];
			
			NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys: str , @"name", color , @"color",nil ];
			[array addObject:dic];
		}
		
	}
	
	[checkboxTable release];
	checkboxTable = [NSArray arrayWithArray:array];
	[checkboxTable retain];
	
	
	
	
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}


/*

	
	
 */

#pragma mark @protocol ButtonAttachmentCellOwnerTextView 


-(void)buttonAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
{
	NSLayoutManager* lm = [self layoutManager];
	NSRange fullRange = NSMakeRange(0,[[self textStorage] length]);
	
	[lm invalidateGlyphsForCharacterRange:fullRange
						   changeInLength:0 
					 actualCharacterRange:nil];
	
	[lm invalidateLayoutForCharacterRange:fullRange isSoft:NO
					 actualCharacterRange:nil];
	
	//[[self layoutManager] invalidateDisplayForCharacterRange:NSMakeRange(0,[[self textStorage] length]) ];
	[self display];
}

-(void)buttonAttachmentCellClicked:(ButtonAttachmentCell*)cell;
{
	//NSLog(@"click");
}
-(void)buttonAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
{
	//NSLog(@"dragged");
	
	NSEvent* theEvent = [[self window] 
				nextEventMatchingMask:NSAnyEventMask
							untilDate:[NSDate dateWithTimeIntervalSinceNow:5.0]
							   inMode:NSEventTrackingRunLoopMode
							  dequeue:YES];
	
	if( [theEvent type] != NSLeftMouseDragged ) return;
	
	
	//----------
	
	
	[self dragSelectionWithEvent:theEvent offset:NSMakeSize(0,0) slideBack:YES];	
	
}

-(NSArray*)checkboxAttachmentCellArrayInRange:(NSRange)range
{
	if( range.length == 0 ) return nil;
	
	
	NSMutableArray* cells = [NSMutableArray array] ;
	
	
	unsigned location;
	for( location = range.location; location < NSMaxRange(range); )
	{
		NSRange effectiveRange;
		id attachment =  [[self textStorage] attribute:NSAttachmentAttributeName
											   atIndex:location
										effectiveRange:&effectiveRange] ;
		if( attachment != nil )
		{
			NSCell* cell = [attachment attachmentCell];
			if( [cell isKindOfClass:[CheckboxAttachmentCell  class]] )
				[cells addObject:[attachment attachmentCell]];
		}
		
		location = NSMaxRange(effectiveRange);
	}
	
	return cells;	
}


-(NSArray*)checkboxAttachmentCellArrayInSelectedRange
{
	NSRange range = [self selectedRange];
	return [self checkboxAttachmentCellArrayInRange:range];
}

-(void)openCheckboxInspector
{
	[[CheckboxInspector sharedCheckboxInspector] activateForTextView:self];
}


-(void)insertCheckbox:(id)sender
{
	
	int colorTag = [sender tag];
	NSAttributedString* checkboxAttr;
	
	if( colorTag >= 0 && colorTag < [checkboxTable count] )
	{
		NSDictionary* dic = [checkboxTable objectAtIndex:colorTag];
		
		NSColor* color = [dic objectForKey:@"color"];
		
		checkboxAttr = [CheckboxAttachmentCell checkboxWithColor:color];
	}
	
	else
	{
		checkboxAttr = [CheckboxAttachmentCell newCheckbox];
	}
	
	

	
	[self insertText:checkboxAttr];
		
}


- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	/* remove MN item */
	int piyo;
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
		if( [[customMenuItem representedObject]  isEqualToString:@"MN_CheckboxTextView"] )
		{
			[aContextMenu removeItem:customMenuItem];
		}
		
	}
	
	
	
	
	//add separator
	customMenuItem = [NSMenuItem separatorItem];
	[customMenuItem setRepresentedObject:@"MN_CheckboxTextView"];
	[aContextMenu addItem:customMenuItem ];
	
	
	//
	
	NSMenu* submenu = [[NSMenu alloc] init];

	
	NSAttributedString* defaultCheckbox = [CheckboxAttachmentCell newCheckbox];
	NSDictionary* dic = [NSDictionary dictionaryWithObject:[NSFont menuFontOfSize:14.0] forKey:NSFontAttributeName];
	NSAttributedString* attr2 = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Default Color",@"") attributes:dic];
	NSMutableAttributedString* mattr = [[NSMutableAttributedString alloc] initWithAttributedString:defaultCheckbox];
	
	[mattr appendAttributedString: attr2];
	
	NSMenuItem* submenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Default Checkbox",@"")																   action:@selector(insertCheckbox:)
												  keyEquivalent:@"I"];
	[submenuItem setKeyEquivalentModifierMask:1048576];
	[submenuItem setTag:-1];
	[submenuItem setTarget:self];
	[submenuItem setAttributedTitle:mattr];
	[submenu addItem:submenuItem];
	[submenuItem release];
	
	
	
	int hoge;
	for( hoge = 0; hoge < [checkboxTable count]; hoge++ )
	{
		NSDictionary* dic = [checkboxTable objectAtIndex:hoge];
	
		submenuItem = [[NSMenuItem alloc] initWithTitle:[dic objectForKey:@"name"]																   action:@selector(insertCheckbox:)
										keyEquivalent:@""];
		[submenuItem setTag:hoge];
		[submenuItem setTarget:self];
		[submenuItem setImage:[ColorCheckbox colorCheckbox:[dic objectForKey:@"color"] checkFlag:NO] ];
		[submenu addItem:submenuItem];
		[submenuItem release];
	
	}
	
	
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Insert Checkbox",@"")
												action:@selector(insertCheckbox:) keyEquivalent:@""];
	[customMenuItem setTag:-1];
	[customMenuItem setRepresentedObject:@"MN_CheckboxTextView"];

	[aContextMenu addItem:customMenuItem ];
	
	[aContextMenu setSubmenu:[submenu autorelease] forItem:customMenuItem];

	[customMenuItem release];	
	//debug end
	
	
	
	
///// cell	
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	
	id something = NULL;
	if( charIndex != NSNotFound )
	{
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:charIndex effectiveRange:NULL];
		
	}
	
	
	
	if(  [ [something attachmentCell] isKindOfClass: [CheckboxAttachmentCell class]] )
	{
		[self setSelectedRange:NSMakeRange(charIndex,1) ];
		
		[ [something attachmentCell] customizeContextualMenu:aContextMenu for:self];
		
	}
	
	
	
	
	
	return aContextMenu;
}


@end
