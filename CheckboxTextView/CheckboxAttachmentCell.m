//
//  MNIconizedCell.m
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//


#import "CheckboxAttachmentCell.h"
#import "ColorCheckbox.h"
#import "CheckboxInspector.h"
#import "SoundManager.h"


#define DEFAULT_ATTRIBUTES  [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11.0], NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, NULL]

extern BOOL UD_CHECKBOX_CONVERT_TO_TEXT;

@implementation CheckboxAttachmentCell

+(NSAttributedString*)newCheckbox
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	CheckboxAttachmentCell* aCell = [[[CheckboxAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	[aCell setAttachment: anAttachment];

	[aCell loadDefaults];
	

	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[aCell image  ]];
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


+(NSAttributedString*)checkboxWithColor:(NSColor*)color
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
		
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	CheckboxAttachmentCell* aCell = [[[CheckboxAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	

	[aCell setColor:color ];
	[aCell setName:@""];
	[aCell setAttachment: anAttachment];
	
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[aCell image  ]];
		
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


+(NSAttributedString*)checkboxWithColor:(NSColor*)color state:(int)state
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	CheckboxAttachmentCell* aCell = [[[CheckboxAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	
	[aCell setColor:color ];
	[aCell setName:@""];
	[aCell setAttachment: anAttachment];
	[aCell setState:state];
	
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		[wrapper setIcon:[aCell image  ]];
		
		return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	



 - (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
 
{
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: 0
					 yBy: cellFrame.origin.y + cellFrame.size.height ]; 
	
	// rotate axis
	//[affine rotateByDegrees:180];
	[affine scaleXBy:1.0 yBy:-1.0];
	[affine concat];
	
	
	
	
cellFrame.origin.y = 0;
		
	NSImage* image = [ColorCheckbox colorCheckbox:[self color] checkFlag:( [self state]==NSOnState? YES:NO )];
	//[image compositeToPoint:point operation:NSCompositeSourceOver];
	
	//cellFrame.origin = NSZeroPoint;

	[image drawInRect:cellFrame fromRect:NSMakeRect(0,0,[image size].width, [image size].height) operation:NSCompositeSourceOver fraction:1.0];

	
	/*
	NSAttributedString* name = [[[NSAttributedString alloc] initWithString:[self name]
																attributes:DEFAULT_ATTRIBUTES] autorelease];

	point.x += 17;
	point.y -= [name size].height;
	*/
//	[name drawAtPoint:point];
	
	
	[context restoreGraphicsState];
}

-(NSImage*)image
{
	NSRect frame = NSZeroRect;
	frame.size = [self cellSize];
	NSImage* img = [[[NSImage alloc] initWithSize:frame.size] autorelease];
	
	[img setFlipped:YES];
	[img lockFocus];
	[self drawWithFrame:frame inView:nil];
	[img unlockFocus];

	return img;
	
}




- (id)initWithAttachment:(NSTextAttachment*)anAttachment
{
    self = [super initWithAttachment:anAttachment];
    if (self) {

		color = [[NSColor blueColor] retain];
		[self setState:NSOffState];
		name = [@"Check" retain];
		
		
		buttonSize = 16;
		buttonPosition = 0;
		showButtonTitle = NO;
		showBezel = NO;
		[self setState:NSOffState];
		
		[self loadDefaults];
		

    }
    return self;
}




- (id)copyWithZone:(NSZone *)zone
{
	//CheckboxAttachmentCell* cell = [[CheckboxAttachmentCell alloc] initWithAttachment:[self attachment]]
	
	CheckboxAttachmentCell* cell = [super copyWithZone:(NSZone *)zone];
	
	[cell setName:[self name]];
	[cell setColor:[self color]];

		
	
	return cell;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
    [encoder encodeObject: color forKey: @"color"];
    [encoder encodeObject: name forKey: @"name"];
	[encoder encodeInt: [self state] forKey: @"state"];

	
    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		if( [coder containsValueForKey:@"color"] )
			color	=   [[coder decodeObjectForKey:@"color"] retain];
		else
			color = [[self legacyColor] retain];

		if( [coder containsValueForKey:@"name"] )
				name	=   [[coder decodeObjectForKey:@"name"] retain];
		else 
				name = [[self legacyName] retain];

		
		if( [coder containsValueForKey:@"state"] )
			[self setState:[coder decodeIntForKey:@"state"]];

		
	}
	return self;
	
	
}

-(void)action:(id)sender
{
	//NSLog(@"action"); 
	if( [textView conformsToProtocol:@protocol(CheckboxAttachmentCellOwnerTextView)] )
	{
		int currentState = [self state];
		
		if( currentState == NSOnState ) 
			[self setState:NSOffState];
		else
			[self setState:NSOnState];

	}
	
	[super action:sender];
	
}






-(NSColor*)color
{
	return color;
	
}

-(NSString*)name
{
	
	return name;
	
}
-(void)setName:(NSString*)newName
{
	if( [newName isKindOfClass:[NSString class]] )
	{
		[name release];
		name = [newName retain];
	}
}

-(void)setColor:(NSColor*)newColor
{
	
	if( [newColor isKindOfClass:[NSColor class]] )
	{

		[color release];
		color = [newColor retain];
		

	}
	[[CheckboxInspector sharedCheckboxInspector] updateInspector];

	
	
	if( textView != nil )
	{
		if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
			[textView buttonAttachmentCellUpdated:self ];
		
		//redraw here NSTextAttachmentCell
		
		
	}
	
}


-(void)setState:(int)state
{
	[super setState:state];

	
	[[CheckboxInspector sharedCheckboxInspector] updateInspector];
	
	
	
	if( textView != nil )
	{
		if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
			[textView buttonAttachmentCellUpdated:self ];
		
		//redraw here NSTextAttachmentCell
		
		
	}
	
}

-(void)setButtonSize:(float)size
{
	[self willChangeValueForKey:@"buttonSize"];
	buttonSize = size;
	[self didChangeValueForKey:@"buttonSize"];

	[[CheckboxInspector sharedCheckboxInspector] updateInspector];
	
}

-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"CheckboxAttachmentCell_buttonSize"];
	[ud setInteger:[self state] forKey:@"CheckboxAttachmentCell_state"];

	[ud setObject:[NSArchiver archivedDataWithRootObject: color] forKey:@"CheckboxAttachmentCell_color"];

	
	[ud synchronize];
}


-(void)loadDefaults
{
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"CheckboxAttachmentCell_buttonSize"];
	buttonSize = (value != nil ? [value floatValue] : 16); 
	
	
	value = [ud valueForKey: @"CheckboxAttachmentCell_state"];
	[self setState:  (value != nil ? [value intValue] : NSOffState) ]; 
		
	value = [ud valueForKey: @"CheckboxAttachmentCell_color"];
	if( value != nil ){
		NSColor* color = [NSUnarchiver unarchiveObjectWithData: value];
		if( color != nil && [color isKindOfClass:[NSColor class]] )
			[self setColor:color];
		
	}
	
	[self setTitle: @"Checkbox"];

	
	lock = NO;
}

-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <CheckboxAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
	
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"CheckboxAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"CheckboxAttachmentCellItem"];
		[aContextMenu addItem:customMenuItem ];
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Checkbox Inspector",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"CheckboxAttachmentCellItem"];
		[customMenuItem setTarget: aTextView];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(openCheckboxInspector)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem autorelease];	
						
}




#pragma mark Legacy
-(NSColor*)legacyColor
{
	
	NSString* colorname = [self title];
	NSArray* array = [colorname componentsSeparatedByString:@","];
	
	float red = [[array objectAtIndex:0] floatValue];
	float green = [[array objectAtIndex:1] floatValue];
	float blue = [[array objectAtIndex:2] floatValue];
	float alpha = [[array objectAtIndex:3] floatValue];
	
	
	return  [NSColor colorWithCalibratedRed:red green:green blue:blue alpha:alpha];
	
}

-(NSString*)legacyName
{
	
	NSString* colorname = [self title];
	NSArray* array = [colorname componentsSeparatedByString:@","];
	
	if( [array count] > 4 )
		return  [array objectAtIndex:4];
	
	else return @"";
	
}

#pragma mark -
#pragma mark @protocol AttachmentCellConverting 
-(int)stringRepresentation:(NSDictionary*)context
{
	return [self attributedStringWithoutImageRepresentation:context];
}
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];

	
	
	NSString* checkStr;
	
	if( [self state] == NSOnState ) 
	{
		unichar blankChar = 0x2611;
		checkStr = [[NSString alloc] initWithCharacters:&blankChar length:1];
	}
	else{
		unichar blankChar = 0x2610;
		checkStr = [[NSString alloc] initWithCharacters:&blankChar length:1];
	}

	
	
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:checkStr];
	[mattr addAttribute:NSForegroundColorAttributeName value:[self color] range:NSMakeRange(index,1)];
	
	
	
	
	
	return 0;
	
}
-(int)attributedStringRepresentation:(NSDictionary*)context
{
	if( UD_CHECKBOX_CONVERT_TO_TEXT ) 
		return [self attributedStringWithoutImageRepresentation:context];
	
	
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[self image  ]];
		
	
	return 0;//
	
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	return nil;
}


@end
