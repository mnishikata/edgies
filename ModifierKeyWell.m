#import "ModifierKeyWell.h"
#import <HIToolbox/CarbonEventsCore.h>




@implementation ModifierKeyWell

- (void) awakeFromNib
{
	
	[self setTarget : self];
	[self setAction : @selector(_pushed:)];
}

- (void)_pushed:(NSEvent *)theEvent
{
	int modKeys = GetCurrentKeyModifiers( ) ;

	
	[self setTagAndTitleFromModKeysCombination:modKeys];
	
	
	
}



-(BOOL)isEqualToKeyModifier:(int)modKey
{
	modKey = modKey & 0x21B00 ; //masking
	
	return ( [self tag] == modKey ? YES : NO );
}

-(BOOL)isPressed
{
	if( [self tag] == 0 ) return NO;
	
	int modKeys = GetCurrentKeyModifiers( ) ;
	modKeys = modKeys & 0x21B00 ; //masking

	
	return ( [self tag] == modKeys ? YES : NO );
	 
}

-(void)setTagAndTitleFromModKeysCombination:(int)modKeys
{
	modKeys = modKeys & 0x21F00; //masking
	
	BOOL cmd	= ( modKeys & 0x100 ) >> 8;	//kCommandUnicode
	BOOL shift	= ( modKeys & 0x200 ) >> 9;	//kShiftUnicode
	BOOL caps	= ( modKeys & 0x400 );	
	BOOL option	= ( modKeys & 0x800 ) >> 11;	//kOptionUnicode
	BOOL ctrl	= ( modKeys & 0x1000 ) >> 12;	//kControlUnicode
	BOOL fn		= ( modKeys & 0x20000 ) >> 17;
	
	NSMutableString* title = [[[NSMutableString alloc] init] autorelease];
	
	
	unichar kCommandUnicode = 0x2318;
	unichar kShiftUnicode	= 0x21E7;
	unichar kOptionUnicode	= 0x2325;
	unichar kControlUnicode	= 0x2303;
	
	NSString* fnStr = @"fn";
	NSString* ctrlStr = [NSString stringWithCharacters:&kControlUnicode  length:1];
	NSString* optionStr = [NSString stringWithCharacters:&kOptionUnicode  length:1];
	NSString* shiftStr = [NSString stringWithCharacters:&kShiftUnicode  length:1];
	NSString* cmdStr = [NSString stringWithCharacters:&kCommandUnicode  length:1];
	
	if( fn )	[title appendString:fnStr ]; 
	if( ctrl )	[title appendString:ctrlStr ]; 
	if( option )	[title appendString:optionStr ]; 
	if( shift )	[title appendString:shiftStr ]; 
	if( cmd )	[title appendString:cmdStr ]; 
	
	if( [title isEqualToString:@""] )
			[title setString:  NSLocalizedString(@"<N/A>",@"") ];
	
	
	//[self setTitle:title];		
	[self setTag:modKeys];
	
	
	
	//TITLE BINDING
	
	NSDictionary* observerInfo = [self infoForBinding:@"title"];
	id observer = [observerInfo objectForKey:@"NSObservedObject"];
	if( observer != nil ) [observer setValue:title forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];
	
	
}


-(void)setTitle:(NSString*)title
{
	//以前のファイルはタグで保存されていたため
	if( [title isKindOfClass:[NSNumber class]] )
	{	
		[self setTagAndTitleFromModKeysCombination:[title intValue]];
		return;
	}
		
		
	int cmd	=  0x100 ;	//kCommandUnicode
	int shift	= 0x200 ;	//kShiftUnicode
	int caps	= 0x400 ;	
	int option	= 0x800 ;	//kOptionUnicode
	int ctrl	= 0x1000;	//kControlUnicode
	int fn		= 0x20000;

	unichar kCommandUnicode = 0x2318;
	unichar kShiftUnicode	= 0x21E7;
	unichar kOptionUnicode	= 0x2325;
	unichar kControlUnicode	= 0x2303;
	
	NSString* fnStr = @"fn";
	NSString* ctrlStr = [NSString stringWithCharacters:&kControlUnicode  length:1];
	NSString* optionStr = [NSString stringWithCharacters:&kOptionUnicode  length:1];
	NSString* shiftStr = [NSString stringWithCharacters:&kShiftUnicode  length:1];
	NSString* cmdStr = [NSString stringWithCharacters:&kCommandUnicode  length:1];
	
	
	int tag = 0;
	if( [title rangeOfString: fnStr].length != 0 )
		tag |= fn;
	
	if( [title rangeOfString: ctrlStr].length != 0 )
		tag |= ctrl;
	
	if( [title rangeOfString: optionStr].length != 0 )
		tag |= option;

	if( [title rangeOfString: shiftStr].length != 0 )
		tag |= shift;
	
	if( [title rangeOfString: cmdStr].length != 0 )
		tag |= cmd;
	
	
	[self setTag:tag];
	[super setTitle: title ];
}
@end
