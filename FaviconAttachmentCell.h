//
//  FaviconAttachmentCell.h
//  sticktotheedge
//
//  Created by __Name__ on 07/05/23.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ButtonAttachmentCell.h"
#import "AttachmentCellConverter.h"
#import <WebKit/WebKit.h>

@interface FaviconAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{

	WebView* aWebView;
	BOOL iconLoaded;
	BOOL titleLoaded;
}
+(NSAttributedString*)faviconAttachmentCellAttributedStringWithPath:(NSString*)path andTitle:(NSString*)title hasTitle:(BOOL)hasTitle loadFavicon:(BOOL)loadFavicon;
- (id)initWithAttachment:(NSTextAttachment*)anAttachment;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)dealloc;
-(void)loadFavicon;
- (void)webView:(WebView *)sender didReceiveIcon:(NSImage *)image forFrame:(WebFrame *)frame;
- (void)webView:(WebView *)sender didReceiveTitle:(NSString *)title forFrame:(WebFrame *)frame;
-(void)setTargetPath:(NSString*)path;
-(NSString*)targetPath;
-(NSAttributedString*)targetLink;
-(void)open;
-(void)useAsDefault;
-(void)loadDefaults;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)aTextView;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(NSRect)buttonFrame:(NSRect)cellFrame;
-(int)attributedStringRepresentation:(NSDictionary*)context; //withimage ;
-(NSDictionary*)pasteboardRepresentation:(id)context;
-(NSString*)writeToFileAt:(NSURL*)url    ;
-(BOOL)canAcceptDrop;

@end


@protocol FaviconAttachmentCellOwnerTextView 
//-(void)faviconAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)faviconAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)faviconAttachmentCellArrayInSelectedRange;
-(void)openFaviconInspector;


@end
