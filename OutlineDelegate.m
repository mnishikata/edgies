#import "OutlineDelegate.h"
#import "ImageAndTextCell.h"
#import "NDAlias.h"
#import "SearchAgent.h"
#import "EdgiesLibrary.h"

#define OutlineRowType @"outline"
#define OutlineIndexesType @"outlineIndexes"
#define OutlineRowTypes [NSArray arrayWithObjects:@"outline", @"outlineIndexes", @"foundItem", NSFilenamesPboardType, nil]
#define FoundItem @"foundItem"

#define UNIQUECODE [NSNumber numberWithDouble: [[NSDate date] timeIntervalSince1970]]

#define ROOT_ACCEPTS_ITEMS NO

/*
 
 NSMutableArray dataArray  (organiserArray in AppController)
 
 @"name"
 @"ndalias"
 @"kind" {folder, item, result, missing, etc}
 @"children"
 @"uniqueCode"
 
 
 @"queryFormat"
 
 @"icon" // volatile
 
 @"undeletable" {YES}
 @"immovable" {YES}
 @"donotAcceptDrop" {YES}
 @"dropNotification" {notificationName}

 
 */


@implementation OutlineDelegate

-(void)postOutlineChangedNotification
{

	[[NSNotificationCenter defaultCenter] postNotificationName:OutlineChangedNotification object:outlineView];
}



- (void)awakeFromNib {
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	id savedDataArrayData = [ud objectForKey:@"MemoListOutlineViewData"];
	if( savedDataArrayData != nil )
	{
		id savedDataArray = [NSUnarchiver unarchiveObjectWithData:savedDataArrayData ];
		
		dataArray = [NSMutableArray arrayWithArray: savedDataArray];
		
		[dataArray retain];
	
	}else
	{
		dataArray = [NSMutableArray array];
		NSString* organiserPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"memolist" ofType:@"plist"];
		NSString* organiserStr = [[NSString alloc] initWithContentsOfFile:organiserPath];
		
		NSArray* array = [organiserStr propertyList];
		
		[dataArray removeAllObjects];
		[dataArray addObjectsFromArray: array];
		
		[dataArray retain];
	
	}
	
	
	[outlineView  registerForDraggedTypes:OutlineRowTypes];
	
	
	//[treeController setContent:dataArray];

	// table cell
	
	NSTableColumn *tableColumn = nil;
	ImageAndTextCell *imageAndTextCell = nil;
//	NSButtonCell *buttonCell = nil;
	
	// Insert custom cell types into the table view, the standard one does text only.
	// We want one column to have text and images, and one to have check boxes.
	tableColumn = [outlineView tableColumnWithIdentifier: @"C1"];
	imageAndTextCell = [[[ImageAndTextCell alloc] init] autorelease];
	[imageAndTextCell setEditable: YES];
	
	docIcon = [[NSImage alloc] initByReferencingFile:
		[[NSBundle bundleForClass:[self class]] pathForResource:@"memoImage" ofType:@"tiff"]];
	
	folderIcon = [[NSImage alloc] initByReferencingFile:
		[[NSBundle bundleForClass:[self class]] pathForResource:@"folder16" ofType:@"tiff"]];
	
	resultIcon = [[NSImage alloc] initByReferencingFile:
		[[NSBundle bundleForClass:[self class]] pathForResource:@"resultIcon" ofType:@"tiff"]];
	
	missingIcon = [[NSImage alloc] initByReferencingFile:
		[[NSBundle bundleForClass:[self class]] pathForResource:@"missing" ofType:@"tiff"]];

	[imageAndTextCell setImage:docIcon ];
	
	[tableColumn setDataCell:imageAndTextCell];
	//[self updateIcons];

	
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateOutlineView:)
													 name:OutlineChangedNotification
												   object:nil];
		
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(addAnItemToFolder:)
													 name:ItemDroppedOnFolderInEdgiesNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(addAnItemToFolder:)
													 name:EdgyExportedNotification
												   object:nil];
		
		
		[outlineView reloadData];

}

-(void)saveDataArray
{
	id data = [NSArchiver archivedDataWithRootObject: dataArray];
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setObject:data forKey:@"MemoListOutlineViewData"];
	
	[ud synchronize];
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[docIcon release];
	[folderIcon release];
	[resultIcon release];
	[missingIcon release];
	
	[dataArray release];
	
	[super dealloc];
}

-(void)updateIcons
{
	int hoge;
	for( hoge = 0; hoge < [dataArray count]; hoge++)
	{
		id item = [dataArray objectAtIndex:hoge];
		[self addIconForItem:item];
		
	}
	
	
}

-(void)setMissingForItem:(NSMutableDictionary*)item
{
	[item setObject:@"missing" forKey:@"kind"];
	[item removeObjectForKey:@"icon"];
	[outlineView reloadData];
}

-(void)addIconForItem:(NSMutableDictionary*)item
{
	if( [[item objectForKey:@"kind"]  isEqualToString:@"item"] )
	{
		NDAlias* alias = [item objectForKey:@"ndalias"];
		NSString* path = [alias path];	
		
		if( path == nil )
		{
			[self setMissingForItem:item];
		}
		else
		{
			//create icon
			NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:path ];
			
			if( iconImage != nil )
			{
				[iconImage setSize:NSMakeSize(16,16)];
				[item setObject:iconImage forKey:@"icon"];
				
			}
			
		}
		
		
	}
}


-(void)updateOutlineView:(NSNotification*)notif
{
	if( [notif object] == outlineView ) return;
	[outlineView reloadData];
	
}
- (IBAction)addFolder:(id)sender
{

	NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
		@"Untitled Folder",@"name",
		@"folder",@"kind",
		UNIQUECODE,@"uniqueCode",
		[NSMutableArray array], @"children",
		@"NO",@"undeletable",
		@"YES",@"immovable", nil];
	
	[dataArray addObject:dic];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];
}
- (IBAction)addDocument:(id)sender
{
	
	
	
	NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
		@"An Item",@"name",
		@"item",@"kind",
		UNIQUECODE,@"uniqueCode",nil];
	
	[dataArray addObject:dic];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];


}

- (IBAction)addResult:(id)sender
{
	
	NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
		[myDocument queryTitle], @"name",
		@"result",@"kind",
		UNIQUECODE,@"uniqueCode",
		[myDocument dataOfType:nil error:nil ], @"queryFormat", nil];
	
	[dataArray addObject:dic];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];

}

-(void)addAnItemToFolder:(NSNotification*)notif
{
	NSString* name = [notif name];
	id path = [notif object];
	
	if( ! [path isKindOfClass:[NSString class]] )
		return;
	
	if( [name isEqualToString:ItemDroppedOnFolderInEdgiesNotification ] )
	{
		[self addFile:path toFolderWithUniqueCode:[NSNumber numberWithDouble:1000000004.0 ]];
	}
	
	else if( [name isEqualToString:EdgyExportedNotification ] )
	{
		[self addFile:path toFolderWithUniqueCode:[NSNumber numberWithDouble:1000000003.0 ]];
	}
	
}

-(void)addFile :(NSString*)path toFolderWithUniqueCode:(NSNumber*)uc
{
	
	id item = [self findFolderWithUniqueCode:uc fromThisArray:dataArray];
	
	
	if( item != nil )
	{
	
	NSMutableDictionary* dic = [self itemDictionaryForPath:path];
	
	
	
	
		[[item objectForKey:@"children"] addObject: dic  ];
	
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];
	}else
	{
		////NSLog(@" is nill");
	}
}

-(id)findFolderWithUniqueCode:(NSNumber*)uc fromThisArray:(NSArray*)array
{
	int hoge;

	for( hoge = 0; hoge  < [array count]; hoge++ )
	{
		
		id item = [array objectAtIndex:hoge];
		
		////NSLog(@"%f %f",[uc doubleValue], [[item objectForKey:@"uniqueCode"] doubleValue]);

		
		if( [[item objectForKey:@"uniqueCode"] doubleValue] == [uc doubleValue] )
			return item;
		
		else if( [item objectForKey:@"children"] != nil )
		{
			item = [self findFolderWithUniqueCode:uc fromThisArray:[item objectForKey:@"children"]];
			
			if( item != nil ) 
				return item;
		}
			
	}
	return nil;
}

- (IBAction)removeItem:(id)sender
{
	
	
	[self deleteRowInOutlineView:[outlineView selectedRow]];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];

}

-(IBAction)setting:(id)sender
{
	id item = [outlineView itemAtRow:[outlineView selectedRow]];

	if( [[item objectForKey:@"kind"] isEqualToString:@"result"] )
	{

			[[NSApp delegate] beginModal:item forWindow:[myDocument windowForSheet ]];
		
	}
	
}


- (void)outlineView:(NSOutlineView *)ov willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	

	if( [[item objectForKey:@"kind"] isEqualToString:@"result"] )
		[cell setImage:resultIcon];
	
	else if( [[item objectForKey:@"kind"] isEqualToString:@"folder"] )
		[cell setImage:folderIcon];
	
	else if( [[item objectForKey:@"kind"] isEqualToString:@"missing"] )
		[cell setImage:missingIcon];
	
	else if( [[item objectForKey:@"kind"] isEqualToString:@"item"] )
	{
		id icon =  [item objectForKey:@"icon"];
		
		if( icon != nil )
		{
			[icon setSize:NSMakeSize(16,16)];
			[cell setImage:icon];
		}
		else
		{
			[self addIconForItem:item];
			icon =  [item objectForKey:@"icon"];
			if( icon != nil )
			{
				[icon setSize:NSMakeSize(16,16)];
				[cell setImage:icon];
			}
			else
				[self setMissingForItem: item]; //unknown
		}
	}
	

}

- (int)outlineView:(NSOutlineView *)ov numberOfAllChildrenOfItem:(id)item 
{
	int counter = 0;
	
	if(item == nil)
	{
		
		unsigned hoge;
		for( hoge = 0; hoge < [dataArray count]; hoge ++ )
		{
			
			counter += [self outlineView:ov  numberOfAllChildrenOfItem:[dataArray objectAtIndex:hoge]];
			
			counter++;
			
		}
		
	}else
	{
		id childrenArray = [item objectForKey:@"children"];
		
		if( childrenArray != nil )
		{
			unsigned hoge;
			for( hoge = 0; hoge < [childrenArray count]; hoge ++ )
			{
				
				counter += [self outlineView:ov  numberOfAllChildrenOfItem:[childrenArray objectAtIndex:hoge]];
				
				counter++;
				
			}	
		}
		
		
	}
	
    return counter;
}


- (int)outlineView:(NSOutlineView *)ov numberOfChildrenOfItem:(id)item 
{
	
    return (item == nil) ? [dataArray count] : [[item objectForKey:@"children"] count];
}

- (BOOL)outlineView:(NSOutlineView *)ov isItemExpandable:(id)item 
{
    if (item == nil) return YES;
	
	if( [[item objectForKey:@"children"] count] > 0  )
		return YES;
	
	if( [[item objectForKey:@"kind"] isEqualToString:@"folder"]   )
		return YES;
	
	if( [[item objectForKey:@"kind"] isEqualToString:@"result"]   )
		return NO;
	
	return NO;
	
}

- (id)outlineView:(NSOutlineView *)ov child:(int)index ofItem:(id)item 
{
    if (item == nil) return [dataArray objectAtIndex:index];
	
	return [[item objectForKey:@"children"] objectAtIndex:index];
	
}

- (id)outlineView:(NSOutlineView *)ov objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item 
{
    return (item == nil) ? @"" : [item objectForKey:@"name"];
}


- (void)outlineView:(NSOutlineView *)ov setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
	
	[item setObject:object forKey:@"name"];
	
}



- (BOOL)outlineView:(NSOutlineView *)ov acceptDrop:(id <NSDraggingInfo>)info item:(id)item childIndex:(int)index
{
	if( item != nil && ! [[item objectForKey:@"kind"] isEqualToString:@"folder"] ) 
	{
		if( [item objectForKey:@"dropNotification"] != nil  )
		{
			NSString* notificationName = [item objectForKey:@"dropNotification"];
			/*
			[[NSNotificationCenter defaultCenter] postNotificationName:notificationName
																object:item];
			
			////NSLog(@"post notification %@",notificationName);
			*/
			
			NSPasteboard* pb = [info draggingPasteboard];
			
			
			
			
			//// drag from table


			if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:FoundItem] ) //from table
				
			{
				
				NSData* data = [pb dataForType:FoundItem];
				if( data == nil ) return NO;
				
				NSArray* array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
				
				int hoge;
				for( hoge = 0; hoge < [array count]; hoge ++ )
				{
					
					//
					NSString* path = [[array objectAtIndex:hoge] objectForKey:kMDItemPath];
					
					
					[[NSNotificationCenter defaultCenter] postNotificationName:notificationName
																		object:path];
					
					////NSLog(@"post notification %@ %@",notificationName,path);
				
				
				}				
				return YES;
			}
			
			
			
			else if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:NSFilenamesPboardType] )
			{
				NSArray *files = [pb propertyListForType:NSFilenamesPboardType];
				if( files == nil || [files count] == 0 ) return NO;
				
				
				int hoge;
				for( hoge = 0; hoge < [files count]; hoge ++ )
				{
					
					//
					NSString* path = [files objectAtIndex:hoge] ;
					
					
					[[NSNotificationCenter defaultCenter] postNotificationName:notificationName
																		object:path];
					
					////NSLog(@"post notification %@ %@",notificationName,path);
					
					
				}

				
				return YES;
				
				
			}
			
			

			
		}else
		{
			return NO;
		}
	}
	
	
	
	
	
	NSPasteboard* pb = [info draggingPasteboard];

	

				
	//// drag from table
	
	/*
	 			name, @"filename",
	 path, @"path",
	 displayname, @"displayname",
	 score, @"score",
	 
	 */
	if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:FoundItem] ) //from table
		
	{

		NSData* data = [pb dataForType:FoundItem];
		if( data == nil ) return NO;
		
		NSArray* array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
			
		int hoge;
		for( hoge = 0; hoge < [array count]; hoge ++ )
		{

			//
			NSString* path = [[array objectAtIndex:hoge] objectForKey:kMDItemPath];
			
			
			
			NSMutableDictionary* dic = [self itemDictionaryForPath:path];
			
			
			
			
			
			//create icon
			[self addIconForItem:dic];
			
			
			
			
			
			if( item == nil )
			{
				if( index == -1 )
					[dataArray addObject:dic];
				else
				{
					[dataArray insertObject:dic atIndex:index ];

				}
				
				
				
			}else
			{
				
				id child = [item objectForKey:@"children"];
				
				if( child == nil )
				{
					child = [NSMutableArray array];
					[item setObject:child forKey:@"children"];

				}
				
				if( index == -1 )
					[child addObject:dic];
				else
				{
					[child insertObject:dic atIndex:index];
					
				}
				

			}
				
		
			
		}
		[outlineView reloadData];

		
		[self postOutlineChangedNotification];

		return YES;
	}
	
	
	
	else if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:NSFilenamesPboardType] )
	{
		NSArray *files = [pb propertyListForType:NSFilenamesPboardType];
		if( files == nil || [files count] == 0 ) return NO;
		
		
		int hoge;
		for( hoge = 0; hoge < [files count]; hoge ++ )
		{
			
			//
			NSString* path = [files objectAtIndex:hoge] ;

			
			
			
			NSMutableDictionary* dic = [self itemDictionaryForPath:path];
			
			
			
			//create icon
			[self addIconForItem:dic];
			
			
			
			
			
			if( item == nil )
			{
				if( index == -1 )
					[dataArray addObject:dic];
				else
				{
					[dataArray insertObject:dic atIndex:index ];

				}
				
			}else
			{
				
				id child = [item objectForKey:@"children"];
				
				if( child == nil )
				{
					child = [NSMutableArray array];
					[item setObject:child forKey:@"children"];
					
				}

				if( index == -1 )
					[child addObject:dic];
				else
				{
					[child insertObject:dic atIndex:index];
					
				}
			}
			
			

		}

		[outlineView reloadData];

		
		[self postOutlineChangedNotification];
		
		return YES;
		
		
	}
	
	
	
	//// drag from outline
	
	[item retain];
	
	
	
	NSDragOperation op = [info draggingSourceOperationMask];
	NSArray* movedItems = [NSArray array];
	
	if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:OutlineRowType] )
	{
		
		movedItems = [NSKeyedUnarchiver unarchiveObjectWithData:[pb dataForType:OutlineRowType]];
		
		
		// update uniqueCode
		
		int hoge;
		for( hoge = 0; hoge < [movedItems count]; hoge++ )
		{
			id hogeItem = [movedItems objectAtIndex:hoge];
			[hogeItem setObject:UNIQUECODE forKey:@"uniqueCode"];
		}
		
		
	}		
	
	
	
	//add
	
	if( item != NULL )
	{
		NSMutableArray* childrenArray = [ item objectForKey:@"children" ];
		
		if( index != -1 )
		{
			int hoge;
			for( hoge = [movedItems count]-1; hoge >= 0 ; hoge-- )
			{
				[childrenArray insertObject:[movedItems objectAtIndex:hoge]  atIndex:index];
				
			}
			
			
			
			
		}else // -1 
		{
			
			[childrenArray addObjectsFromArray:movedItems ];
		}
		
		
		
		//update unique code
		
		int level	= [outlineView levelForRow:[outlineView rowForItem:item]];
		
		[item setObject:UNIQUECODE forKey:@"uniqueCode"];
		
		
		int hoge;
		for( hoge = [outlineView rowForItem:item]; hoge >= 0 ; hoge-- )
		{
			////NSLog(@"checking %d",hoge);
			
			id piyoItem = [outlineView itemAtRow:hoge];
			int newLevel	= [outlineView levelForRow:hoge];
			
			if( newLevel < level ) // parent
			{
				[piyoItem setObject:UNIQUECODE forKey:@"uniqueCode"];
				level = newLevel;
				////NSLog(@"update leve %d",level);
			}
			
			if( newLevel == 0 ) break;
		}
		
		
	}
	else if( item == NULL )
	{
		if( index == -1 ) index = [dataArray count];
		int hoge;
		for( hoge = [movedItems count]-1; hoge >= 0 ; hoge-- )
		{
			[dataArray insertObject:[movedItems objectAtIndex:hoge]  atIndex:index];
			
		}
		
	}
	
	
	
	//remove
	if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:OutlineRowType] )
	{///delete
	 //delete items in array : [NSKeyedUnarchiver unarchiveObjectWithData:[pb dataForType:OutlineRowType]]
		
		NSArray* itemsToBeDeleted = [NSKeyedUnarchiver unarchiveObjectWithData:[pb dataForType:OutlineRowType]];
		
		int deleteCount;
		for( deleteCount = 0; deleteCount < [itemsToBeDeleted count]; deleteCount++)
		{
			
			id itemToBeDeleted = [itemsToBeDeleted objectAtIndex:deleteCount];
			
			int rowInView;
			for( rowInView = 0; rowInView < [outlineView numberOfRows]; rowInView++ )
			{
				
				if( [self outlineViewItem:[outlineView itemAtRow:rowInView] isEqualToItem:itemToBeDeleted] )
				{
					[self deleteRowInOutlineView:rowInView];
					break;
				}
			}
			
			
		}
	}
	
	
	//
	//set selection
	[outlineView reloadData];
	
	NSMutableIndexSet* indexes = [NSMutableIndexSet indexSet];
	
	
	int hoge;
	for( hoge = 0; hoge < [ movedItems count]; hoge++ )
	{
		[indexes addIndex:[outlineView rowForItem:[movedItems objectAtIndex:hoge] ]];
		
	}
	////NSLog(@"selection %@",[indexes description]);
	[outlineView selectRowIndexes:indexes byExtendingSelection:NO];
	
	//[self outlineViewSelectionDidChange:NULL];
	
	
	
//	[self updateOutlineMemo];
	
	[outlineView reloadData];
	[item release];
	
	[self postOutlineChangedNotification];

	return YES;
	
}






-(BOOL)outlineViewItem:(id)rowItem1 isEqualToItem:(id)rowItem2
{
	if( [[rowItem1 objectForKey:@"uniqueCode" ] doubleValue] == [[rowItem2 objectForKey:@"uniqueCode" ] doubleValue] )
		return YES;
	return NO;
	
}


-(NSMutableDictionary*)itemDictionaryForPath:(NSString*)path
{
	NSString* filename = [path lastPathComponent];
	NDAlias* alias = [NDAlias aliasWithPath:path];
	
	
	NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
		filename,@"name",
		@"item",@"kind",
		alias,@"ndalias",
		UNIQUECODE,@"uniqueCode",nil];
	

	return dic;
}

-(BOOL)thisItemIsUndeletable:(id)item
{
	BOOL flag = [[item objectForKey:@"undeletable"] isEqualToString:@"YES"];
	
	if( flag == YES ) return YES;
	

	id child = [item objectForKey:@"children"];
	
	if( child != nil )
	{
		int hoge;
		for( hoge = 0 ; hoge < [child count]; hoge++ )
		{
		
			id aChild = [child objectAtIndex:hoge];
			if( [self thisItemIsUndeletable:aChild] )
			{
				return YES;
				
			}
			
		}
		
		
	}
	
	return NO;
}

-(void)deleteRowInOutlineView:(int)rowToBeDeleted
{
	if( rowToBeDeleted < 0 || rowToBeDeleted == NSNotFound ) return;
	
	
	id  itemToBeDeleted = [outlineView itemAtRow:rowToBeDeleted];
	int level	= [outlineView levelForRow:rowToBeDeleted];
	
	
	// check if it can be deleted
	
	if( [self thisItemIsUndeletable: itemToBeDeleted] )
	{
		return;
		
	}


	
	// tell parent to remove one of the children
	NSMutableArray* parentArray;
	if( level == 0 ) // root
	{
		parentArray = dataArray;
		
	}else
	{
		for(  ; rowToBeDeleted >= 0; rowToBeDeleted-- )
		{
			if( level != [outlineView levelForRow:rowToBeDeleted] )
			{//found parent
				parentArray = [[outlineView itemAtRow:rowToBeDeleted] objectForKey:@"children"];
				break;
			}
		}
	}
	
	[parentArray removeObject:itemToBeDeleted];
}


-(NSMutableArray*)childrenItemsOf:(id)item
{
	NSMutableArray* array = [NSMutableArray array];
	
	[array addObject:item];
	
	unsigned hoge;
	
	for( hoge = 0; hoge < [[item objectForKey:@"children"] count]; hoge++ )
	{
		[array addObjectsFromArray:[self childrenItemsOf:[[item objectForKey:@"children"] objectAtIndex:hoge]]  ];
	}
	
	return array;
}

- (BOOL)outlineView:(NSOutlineView *)ov writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard
{
	return NO; //###
	
	
	
	
	int hoge;
	for( hoge = 0; hoge < [items count];hoge++ )
	{
		id item = [items objectAtIndex:hoge];
		
		if( [[item objectForKey:@"immovable"] isEqualToString:@"YES"] )
			return NO;

	}
	
	[pboard declareTypes:OutlineRowTypes owner:self];
	////NSLog(@"writeItems");

	
	[pboard setData:[NSKeyedArchiver archivedDataWithRootObject:items] forType:OutlineRowType];
	//[pboard setPropertyList: indexes  forType:OutlineIndexesType];
	
	
	
	return YES;
}

- (NSDragOperation)outlineView:(NSOutlineView *)ov validateDrop:(id <NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(int)index
{
	
	NSPasteboard *pboard = [info draggingPasteboard];
	NSDragOperation op = [info draggingSourceOperationMask];
	

	if( item == nil && ROOT_ACCEPTS_ITEMS == NO ) return NSDragOperationNone;
	
	
	if( item != nil &&
		! [[item objectForKey:@"kind"] isEqualToString:@"folder"]&&
		 [item objectForKey:@"dropNotification"] == nil ) 
		return NSDragOperationNone;
	
	if( [[item objectForKey:@"donotAcceptDrop"] isEqualToString:@"YES"] ) 
		return NSDragOperationNone;

	
	if ( [pboard availableTypeFromArray:OutlineRowTypes] != nil) 
		if( [ov isEqual: [info draggingSource] ] )	return NSDragOperationGeneric;  //From self
		else	return NSDragOperationCopy; //from other
	
	

	
	
	return NSDragOperationNone;
}

///
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item
{
	
	if( [[item objectForKey:@"kind"] isEqualToString:@"result"]  )
	{
		
		// get result for item here
		
		NSData* data = [item objectForKey:@"queryFormat"];
		if( data != nil )
		{
		
		itemWaitingForResults = item;
		[itemWaitingForResults retain];
		

		
		[myDocument startQueryUsingData:data clearResultsFirst:YES];
		
		NSLock* lock = [myDocument searchLock];
		
		[lock lock]; // wait until finish
		
		[self searchFinished];
		////NSLog(@"startQueryUsingData");

		
		}
		 
		return YES;
		
	}
	
	
	return YES;
	
}

-(void)searchFinished
{
	////NSLog(@"searchFinished");
	

	NSMutableArray* array = [NSMutableArray arrayWithArray:[findResult content]];
	
	int hoge;
	for( hoge = 0; hoge < [array count]; hoge++)
	{
		id item = [array objectAtIndex:hoge];
		[item setObject:@"item" forKey:@"kind"];

		NSString* path = [item objectForKey:kMDItemPath];
		
		
		NSString* filename = [path lastPathComponent];
		NDAlias* alias = [NDAlias aliasWithPath:path];
		[item setObject:alias forKey:@"ndalias"];
		[item setObject:UNIQUECODE forKey:@"uniqueCode"];
		[item setObject:filename forKey:@"name"];

		
		
		
	}
	
	[itemWaitingForResults setObject:array forKey:@"children"];
	
	//[outlineView expandItem:itemWaitingForResults];
	
	[outlineView reloadData];
	
	[itemWaitingForResults release];
	itemWaitingForResults = nil;
	
}



- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item
{
	if( [[item objectForKey:@"kind"] isEqualToString:@"result"] )
	{
		// delete children
		[item setObject:[NSMutableArray array] forKey:@"children"];
	}
	
	
	return YES;
	
}

-(void)missingItem:(id)item
{
	[findResult removeObjects:[findResult content]];
	[findResult rearrangeObjects];
}

-(NSMutableArray*)expandChildrenFor:(NSMutableArray*)array
{
	NSMutableArray* returnValue = [NSMutableArray array];
	int hoge;
	for( hoge = 0; hoge < [array count]; hoge++ )
	{
		
		NSMutableDictionary* item = [array objectAtIndex:hoge];
		
		[returnValue addObject:item];
		id childrenArray = [item objectForKey:@"children"];
		if( childrenArray != nil  )
		{
			[returnValue addObjectsFromArray:[self expandChildrenFor:childrenArray ]];
		}
		
	}
	
	return returnValue;
}



#pragma mark -

- (void)outlineViewSelectionDidChange:(NSNotification *)aNotification
{
	////NSLog(@"outlineViewSelectionDidChange");
	
	id item = [outlineView itemAtRow:[outlineView selectedRow]];
	
	if( [[item objectForKey:@"kind"] isEqualToString:@"item"] )
	{

		[findResult removeObjects:[findResult content]];
		[findResult rearrangeObjects];
		
		
		NDAlias* alias = [item objectForKey:@"ndalias"];
		NSString* path = [alias path];
		
		
		if( path == nil )
		{
			[self setMissingForItem: item ];
			[self missingItem:item ];
			return;
		}
		
		/* debug */
	//	//NSLog(@"debug");
	//	[self  addFile :path toFolderWithUniqueCode:[NSNumber numberWithDouble:1000000003.0]];
	//	return ;
		
		
		MDItemRef mditem =  MDItemCreate (nil,path);
			
		
		if( mditem != nil )
		{
			NSMutableDictionary* attributes = [myDocument extractAttributesFrom: mditem];
			
			[attributes setObject:[NSNumber numberWithFloat:1.0] forKey:kMDQueryResultContentRelevance];
			
			[findResult addObject:attributes];
			
			
			[findResult rearrangeObjects];
			
			[myDocument displayWithFile:path];

			CFRelease(mditem);
		}else
		{

			[self setMissingForItem: item ];
			[self missingItem:item ];
			return;

			
		}

		
	}else 	if( [[item objectForKey:@"kind"] isEqualToString:@"folder"] )
	{
		[findResult removeObjects:[findResult content]];
		[findResult rearrangeObjects];
		
		NSArray* children = [item objectForKey:@"children"];
		
		NSArray* wholeChildren = [self expandChildrenFor:children];
		
		int hoge;
		for( hoge = 0; hoge < [wholeChildren count]; hoge++ )
		{
			NSDictionary* item = [wholeChildren objectAtIndex:hoge];
			
			if( [[item objectForKey:@"kind"] isEqualToString:@"item"] )
			{
				NDAlias* alias = [item objectForKey:@"ndalias"];
				NSString* path = [alias path];
				
				if( path == nil )
				{
					[self setMissingForItem: item ];
					[self missingItem:item ];
				}else
				{
					MDItemRef mditem =  MDItemCreate (nil,path);
					
					if( mditem != nil )
					{
						NSMutableDictionary* attributes = [myDocument extractAttributesFrom: mditem];
						[attributes setObject:[NSNumber numberWithFloat:1.0] forKey:kMDQueryResultContentRelevance];

						[findResult addObject:attributes];
						CFRelease(mditem);
					}else
					{
						[self setMissingForItem: item ];
						[self missingItem:item ];
						
					}
				}
			}else	if( [[item objectForKey:@"kind"] isEqualToString:@"result"] )
			{
				NSData* data = [item objectForKey:@"queryFormat"];
				if( data != nil )
					[myDocument startQueryUsingData:data clearResultsFirst:NO ];
				
			}

		}
		
		[findResult rearrangeObjects];

	}else 	if( [[item objectForKey:@"kind"] isEqualToString:@"result"] )
	{
		NSData* data = [item objectForKey:@"queryFormat"];
		if( data != nil )
			[myDocument startQueryUsingData:data clearResultsFirst:YES ];
		else
			[myDocument setDefault];
		
		
	}else 	if( [[item objectForKey:@"kind"] isEqualToString:@"missing"] )
	{


			[self missingItem:item ];

	}
	
	
}



@end
