/* ButtonInspector */

#import <Cocoa/Cocoa.h>
#import "TaggingAttachmentCell.h"

@interface TaggingInspector : NSObject
{
    IBOutlet id window;
	IBOutlet id dialogue;

	IBOutlet id skipDialogue;
	IBOutlet id fileLocationWell;

	IBOutlet id textView;
	
	NSString* fileLocationWellString;
	
	NSArray* /*ButtonAttachmentCell**/ activeCells;
}

+(TaggingInspector*)sharedTaggingInspector;
- (id) init ;
-(BOOL)multipleSelection;
-(BOOL)noSelection;
-(void)showTaggingInspector;
-(id)imageData;
-(id)title;
-(NSString*)targetPath;
-(void)setTitle:(NSString*)title;
-(id)font;
-(void)setFont:(NSFont*)font;
-(id)buttonSize;
-(void)setButtonSize:(NSNumber*)size;
-(id)buttonPosition;
-(void)setButtonPosition:(NSNumber*)position;
-(id)showButtonTitle;
-(void)setShowButtonTitle:(id)showButtonTitle;
-(id)showBezel;
-(void)setShowBezel:(id)showBezel;
-(void)dealloc;
-(BOOL)lock;
-(void)setLock:(BOOL)flag;
-(void)selectionChanged:(NSNotification*)notification;
-(void)updateInspector;
-(void)activateForTextView:(NSTextView  <TaggingAttachmentCellOwnerTextView> *)view;
-(IBAction)useAsDefault:(id)sender;


@end
