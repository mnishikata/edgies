#import <HIToolbox/CarbonEventsCore.h>

#import "FileEntityTextView.h"
#import "FileEntityTextAttachmentCell.h"
#import "NSTextView (coordinate extension).h"
#import "FileEntityUtil.h"

#import "DEFINITION.h"

#import "NDAlias.h"


@implementation FileEntityTextView


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		
		[self awakeFromNib];
		
	}
    return self;
}
- (void)awakeFromNib
{
	
	[self setDelegate:self];
	
	/*
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(rescueItemsBeforeDiscarding)
												 name:NSWindowWillCloseNotification 
											   object:[self window]];
*/
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}


-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];

	[super dealloc];
}

-(void)rescueItemsBeforeDiscarding
{
	//NSLog(@"rescueItemsBeforeDiscarding" );

			
		unsigned hoge;
		for( hoge = 0; hoge < NSMaxRange( [self fullRange] );  )
		{
			NSRange range;
			
			NSDictionary* dic = [[self textStorage] attributesAtIndex:hoge
												longestEffectiveRange:&range 
															  inRange:[self fullRange]];
			
			id object = [dic objectForKey:NSAttachmentAttributeName ];
			if( object != nil )
			{
				
				id aCell = [object attachmentCell];
				if( [aCell isKindOfClass:[FileEntityTextAttachmentCell class]] )
				if( ! [aCell isEmpty] && ! [FileEntityUtil fileAtTargetPathIsKagemusha:[aCell targetPath] ])
					{
						
		if( ! [[NSFileManager defaultManager] fileExistsAtPath: RESCUE_FOLDER ] )
			[[NSFileManager defaultManager] createDirectoryAtPath: RESCUE_FOLDER attributes:NULL];
					
			
		
		NSString* _target = 				[aCell myFolderPath];
		NSString* _dest = [RESCUE_FOLDER stringByAppendingPathComponent:[_target lastPathComponent] ];
			
		_dest = [_dest uniquePathForFolder];
		

		[[NSFileManager defaultManager]
							movePath:_target
							  toPath:_dest
							 handler:nil];

		
						
					}else if(  [FileEntityUtil fileAtTargetPathIsKagemusha:[aCell targetPath] ] )
					{						
						//remove alias file

						[[NSFileManager defaultManager]
							removeFileAtPath:[aCell targetPath] handler:nil ];
					}
						
			}
				
				hoge = NSMaxRange(range);
		}
			
				
	
}


-(unsigned)mouseOnFileEntity /// return index or NSNotFound
{
	id cell = nil;
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	NSDictionary* dict;
	if( charIndex != NSNotFound )
	{
		
		dict = [[self textStorage] attributesAtIndex:charIndex
									  effectiveRange:nil];
		
		if( [dict objectForKey:NSAttachmentAttributeName] != nil )
		{
			cell = [[dict objectForKey:NSAttachmentAttributeName] attachmentCell]; 
			
			if( ! [cell isKindOfClass:[FileEntityTextAttachmentCell class]] )
			{
				cell = nil;
			}
		}

	}

	if( cell == nil ) return NSNotFound;
	else return charIndex;
}
-(FileEntityTextAttachmentCell* )mouseOnFileEntityCell /// return index or NSNotFound
{
	id cell = nil;
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	NSDictionary* dict;
	if( charIndex != NSNotFound )
	{
		
		dict = [[self textStorage] attributesAtIndex:charIndex
									  effectiveRange:nil];
		
		if( [dict objectForKey:NSAttachmentAttributeName] != nil )
		{
			cell = [[dict objectForKey:NSAttachmentAttributeName] attachmentCell]; 
			
			if( ! [cell isKindOfClass:[FileEntityTextAttachmentCell class]] )
			{
				cell = nil;
			}
		}
		
	}
	
	if( cell == nil ) return nil;
	else return cell;
}

-(void)insertFileEntity:(NSString*)filepath  copy:(BOOL)flag
{
	
		//paste
	[self pasteText: [self fileEntityWithPath:filepath copy:flag ] ];
		
}


-(NSAttributedString*)fileEntityWithPath:(NSString*)filepath  copy:(BOOL)flag
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	//add files
	
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	
	
	
	FileEntityTextAttachmentCell* aCell = [[[FileEntityTextAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	
	
	[aCell substantiateWithFileAtPath: filepath  copy:flag];
	
	[anAttachment setAttachmentCell:aCell ];
	
	
	
	
	[wrapper setFilename:@"file.tiff"];
 	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		
	[wrapper setIcon:[aCell image  ]];
	
	NSMutableAttributedString* aStr = (NSMutableAttributedString*)[NSMutableAttributedString attributedStringWithAttachment:anAttachment];
	
	//[aStr addAttribute:NSToolTipAttributeName value:[aCell name] range:NSMakeRange(0,1)];
	
	
	
	///
	
	NDAlias* anAlias;
	
	if( [FileEntityUtil fileAtTargetPathIsKagemusha: [aCell targetPath] ] )
	{
		
		anAlias = [NDAlias aliasWithPath: filepath];
		
		
	}else{
		
		anAlias	= [NDAlias aliasWithPath: [aCell targetPath] ];
	}
	
	NSMutableDictionary* aLinkAttr;
				aLinkAttr = [NSMutableDictionary dictionaryWithObject: anAlias
															   forKey: NSLinkAttributeName];

				
	[aStr addAttributes:aLinkAttr range:NSMakeRange(0 , 1)];
	
	
	//paste
	return (NSAttributedString*)aStr;
		
}




//- (BOOL)shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString

- (BOOL)textView:(NSTextView *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString
{


	if( affectedCharRange.length == 0 )
		return YES;
	
	
	BOOL flag = YES;
	
	unsigned hoge;
	for( hoge = affectedCharRange.location; hoge < NSMaxRange( affectedCharRange );  )
	{
		NSRange range;
		
		NSDictionary* dic = [[self textStorage] attributesAtIndex:hoge
											longestEffectiveRange:&range 
														  inRange:[self fullRange]];
		
		id object = [dic objectForKey:NSAttachmentAttributeName ];
		if( object != nil )
		{
			
			id aCell = [object attachmentCell];
			if( [aCell isKindOfClass:[FileEntityTextAttachmentCell class]] )
			{				
				if( [aCell isEmpty] == NO && ![FileEntityUtil fileAtTargetPathIsKagemusha:[aCell targetPath]])
				{
					
					flag = NO;
					break;
				}
				
				
				if( [FileEntityUtil fileAtTargetPathIsKagemusha:[aCell targetPath]] )
				{

					[[NSFileManager defaultManager]
							removeFileAtPath:[aCell targetPath]
									 handler:nil];

				}
			}
					
		}
			
			hoge = NSMaxRange(range);
	}
	
	if( flag == NO )
		NSBeep();
		
	return flag;
		
}


- (void)textView:(NSTextView *)aTextView clickedOnCell:(id <NSTextAttachmentCell>)attachmentCell inRect:(NSRect)cellFrame
{
	//NSLog(@"clickedOnCell"); //not called
	
}

-(void)mouseDown:(NSEvent*)theEvent
{
	//NSLog(@"mouseDown");
	

	
	FileEntityTextAttachmentCell* cell = nil;
	
	BOOL startDragging = NO;
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	NSDictionary* dict;
	if( charIndex != NSNotFound && charIndex < NSMaxRange([self fullRange]) )
	{
	
		dict = [[self textStorage] attributesAtIndex:charIndex
									  effectiveRange:nil];

		if( [dict objectForKey:NSAttachmentAttributeName] != nil )
		{
			cell = [[dict objectForKey:NSAttachmentAttributeName] attachmentCell]; 
			
			if( ! [cell isKindOfClass:[FileEntityTextAttachmentCell class]] )
			{
				cell = nil;
			}else
			{
				
				NSRect cellFrame = [self rectForCharRange:NSMakeRange(charIndex,1)];
				

				
				NSPoint mLocInSelf = [self convertPoint:mouseLoc fromView:[[self window] contentView]];
					
				NSPoint mouseLocInCell = NSMakePoint( -cellFrame.origin.x + mLocInSelf.x,
													  -cellFrame.origin.y + mLocInSelf.y);
					
				if( [cell pointInButtonArea:mouseLocInCell] )
				{
					[cell setPressed:YES view:self] ; //press button
				}
				else
				{
				//	NSRange selectedRange = [self selectedRange];

			//if( !(selectedRange.location <= charIndex && charIndex < NSMaxRange(selectedRange)) )

					[self setSelectedRange:NSMakeRange( charIndex,1 )];
				}
				
				
			}
		}
	}
	
	if( cell == nil ) {
		[super mouseDown:theEvent];
		return;
	}

	
		
	TIMEOUT_WHILE(30)
	{
		theEvent = [[self window] 
				nextEventMatchingMask:NSAnyEventMask
							untilDate:[NSDate dateWithTimeIntervalSinceNow:30.0]
							   inMode:NSEventTrackingRunLoopMode
							  dequeue:YES];
		
		if( theEvent == nil )
		{
			break;
		}
		
		
		
		
		

		
		
		if ([theEvent type] == NSLeftMouseUp  )
		{		
			break;
		}


		if( [theEvent type] == NSLeftMouseDragged && cell != nil )
		{
			startDragging = YES;
			break;
			
		}
	}

	
	if( cell != nil && startDragging == NO && [cell isPressed]  )
	{
		if( [FileEntityUtil fileAtTargetPathIsKagemusha:[cell targetPath] ] )
		{
		//	SystemSoundPlay(17); 

			[self clickedOnLink:[[self textStorage] attribute:NSLinkAttributeName
													  atIndex:charIndex
											   effectiveRange:nil]
													  atIndex:charIndex];
			
			
		}else
			[cell execute];
		
			
	}

	//NSLog(@"end loop");
	[cell setPressed:NO view:self];

	
	
	if( startDragging == YES &&  [cell targetPath] != nil ) 
	{
		
		NSRect contentRect;
		NSPoint mLoc = [[self window] mouseLocationOutsideOfEventStream];
		
		contentRect.origin = [[[self window] contentView] convertPoint:mLoc toView:self ];
		

		
		NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard];
		[pb declareTypes:[NSArray arrayWithObject:NSFilenamesPboardType]  owner:self];
		
		
		NSArray* array;
		
		//
		

		if( ! [FileEntityUtil fileAtTargetPathIsKagemusha:[cell targetPath] ] ) //move mode
		{

			
			array = [NSArray arrayWithObject: [cell targetPath]];
			
		}else //alias mode
		{
			/*
			NSString* originalPath = 
			[[NSFileManager defaultManager] pathContentOfSymbolicLinkAtPath:[cell targetPath]];
			--> not work*/
			
			// use NDAlias in this mode
			//charIndex
			
			id someLink = [[self textStorage] attribute:NSLinkAttributeName 
												atIndex:charIndex 
								  longestEffectiveRange:nil inRange:[self fullRange]];
			
			if ([someLink isKindOfClass: [NDAlias class]])	
			{
				NSString* originalPath = [someLink path];
				
				if( originalPath == nil )
					return;
				
				//NSLog(@"originalpath %@",originalPath);
				array = [NSArray arrayWithObject: originalPath];

		
			
			}else	return;
		}
		
		
		[pb setPropertyList:[[array description] propertyList] forType:NSFilenamesPboardType];
		
		
		
		[cell setBeingDragged:YES];
		draggingCell = [cell retain];
		draggingCellCharIndex = charIndex;


		
		[self dragImage:[cell image]
					 at:contentRect.origin
				 offset:NSMakeSize(0,0)
				  event:theEvent
			 pasteboard:[NSPasteboard pasteboardWithName:NSDragPboard]
				 source:self
			  slideBack:YES];	
		
		[self display];
		
		

	}
	

	
	//
}

- (unsigned int)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	return NSDragOperationCopy | NSDragOperationLink | NSDragOperationMove;
}


- (void)draggedImage:(NSImage *)anImage endedAt:(NSPoint)aPoint operation:(NSDragOperation)operation
{
	//NSLog(@"%d",operation);
	
	
	if( operation == NSDragOperationMove )
	{


		if( draggingCell != nil &&
			[FileEntityUtil fileAtTargetPathIsKagemusha:[draggingCell targetPath]] )
		{

			[[NSFileManager defaultManager]
							removeFileAtPath:[draggingCell targetPath]
									 handler:nil];

				
		}
		
		
		[self deleteEmptyCell];
		//[draggingCell setBeingDragged:NO];

		
		
		
	}
	
	else
	{
		if( draggingCell == nil || [draggingCell isEmpty] )
		{
			
			
		}else
		{
			
		//[draggingCell setBeingDragGGed:NO];
		//[self executeDelete:NO];
		}
		

	}
//	[FileEntityUtil lockFileFolder:[draggingCell myFolderPath]];
	[draggingCell release];
	draggingCell = nil;

	[self display];
}

-(void)deleteEmptyCell
{//NSLog(@"deleteEmptyCell");
	
	[self setAllowsUndo:NO];
	
	unsigned hoge;
	for( hoge = 0; hoge < NSMaxRange( [self fullRange] );  )
	{
		NSRange range;
		
		NSDictionary* dic = [[self textStorage] attributesAtIndex:hoge
											longestEffectiveRange:&range 
														  inRange:[self fullRange]];
		
		id object = [dic objectForKey:NSAttachmentAttributeName ];
		if( object != nil )
		{
			
			id aCell = [object attachmentCell];
			if( [aCell isKindOfClass:[FileEntityTextAttachmentCell class]] )
				if( [aCell isEmpty] )
				{
					//[aCell setImage:nil];
	
					[self  replaceCharactersInRange:NSMakeRange(hoge,1)
										 withString:@" "];
					[[self textStorage] removeAttribute:NSLinkAttributeName range:NSMakeRange(hoge,1)];
					
					
					NSRange bufRange = [self selectedRange];
					[self setSelectedRange:NSMakeRange(hoge,1) ];
					[self pasteText:@""];
					[self setSelectedRange:bufRange];

				}
			
		}
		
		hoge = NSMaxRange(range);
	}

		[self setAllowsUndo:YES];

		
		[self display];
		//NSLog(@"deleteEmptyCell end");
}


/*
-(void)executeDelete:(BOOL)confirm
{
	if( confirm == NO ) //cancel
	{
			[[self textStorage] removeAttribute:DeleteReserveAttributeName
										  range:[self fullRange]];
		
	}else
	{
		unsigned hoge;
		for( hoge = 0; hoge < NSMaxRange( [self fullRange] );  )
		{
			NSRange range;

			NSDictionary* dic = [[self textStorage] attributesAtIndex:hoge
							longestEffectiveRange:&range 
										  inRange:[self fullRange]];
			
			if( [dic objectForKey:DeleteReserveAttributeName ] != nil )
			{
				
				
				
			}
				
			hoge = NSMaxRange(range);
			
		}
	}
}
*/
/*

- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender
{
	if(  [self mouseOnFileEntity] != NSNotFound  )
	{
		return NSDragOperationNone;
	}

	else
		return [super draggingUpdated:sender];
}
*/


- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
	

	
	int modKey = GetCurrentKeyModifiers( );

	
	NSPasteboard *pboard = [sender draggingPasteboard];
	

	
	if( ! [[pboard types] containsObject:NSFilenamesPboardType] &&
		! [[pboard types] containsObject:NSFilesPromisePboardType])
		return [super performDragOperation:sender];

		
	
	
	
	
	if( ! [[sender draggingSource] isKindOfClass:[FileEntityTextView class]] ) 
	{
		// when dragged from finder, if command is not presed,
		
		if ( (modKey | 1024) != (0x100 | 1024)  )  //command 
		{
			//NSLog(@"copy");
			return [super performDragOperation:sender];
		}
		
		
	}

	
	BOOL copyFlag = ( (modKey | 1024) == (2048 | 1024)  );
				
				
	
	
	if( [[pboard types] containsObject:NSFilenamesPboardType] || 
		[[pboard types] containsObject:NSFilesPromisePboardType])
	{
		[self setAllowsUndo:NO];

		
		
		
		
		NSArray *files;
		
		if( [[pboard types] containsObject:NSFilesPromisePboardType] )
		{
			//NSLog(@"promise");
			
			files = [sender
                namesOfPromisedFilesDroppedAtDestination:[NSURL fileURLWithPath: FILE_FOLDER]];
			
			//NSLog(@"promise end");

			
			
			NSMutableArray* files_withFolder = [NSMutableArray array];
			int piyo;
			for( piyo = 0; piyo < [files count]; piyo++)
			{
				[files_withFolder addObject: [FILE_FOLDER stringByAppendingPathComponent:[files objectAtIndex:piyo]]];
			}
			
			files = [NSArray arrayWithArray: files_withFolder];
			
			
			//NSLog(@"%@",[files description]);
			
		}else
		{
		
			files = [pboard propertyListForType:NSFilenamesPboardType];
		}
		
		int hoge;
		for( hoge = 0; hoge < [files count]; hoge++ )
		 {
		
			//[self insertFileEntity: [files objectAtIndex: hoge] copy:copyFlag ];	
			NSAttributedString* entity = [self fileEntityWithPath:[files objectAtIndex: hoge] copy:copyFlag ];
			
			//insertion point
			NSPoint aPoint = [sender draggingLocation]; //point
			unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
			if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;
			
			[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
			[self pasteText:entity];
			
			NSAttributedString* _atr = [[[NSAttributedString alloc] initWithString:@"\n"] autorelease];
			[self pasteText:_atr];

		 }
		
		[self setAllowsUndo:YES];

		
		return YES;
	}
	else
	 
	return [super performDragOperation:sender];
	
}



- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	
	
	unsigned index = [self mouseOnFileEntity];
	
	if( index != NSNotFound )
	{
		
		
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"Show/Hide Button Title"
													action:@selector(menu_selectedShowHideButtonTitle:) keyEquivalent:@""];
		[customMenuItem setTag:(signed int)index ];

		[aContextMenu insertItem :customMenuItem  atIndex:0];
		
		[customMenuItem autorelease];	
	}
	
	return aContextMenu;
}

-(void)menu_selectedShowHideButtonTitle:(id)sender
{
	unsigned index = (unsigned int)[sender tag];
	
	id attachment = [[self textStorage] attribute:NSAttachmentAttributeName
									atIndex:index
							 effectiveRange:nil];
	
	id cell = [attachment attachmentCell];
	
	if( ! [cell isKindOfClass:[FileEntityTextAttachmentCell class]] )
	{
		NSBeep();
		return;
	}
	
	[cell setShowButtonTitle: ! [cell showButtonTitle]];
	
	[[self window] redrawWithShadowProperly];
	[self display];
	
}


@end
