#import "DynamicColorMenu.h"
#import "ThemeManager.h"

@implementation DynamicColorMenu



- (void)menuNeedsUpdate:(NSMenu *)menu
{
	//NSLog(@"menuNeedsUpdate");
	
	unsigned int i, count = [menu numberOfItems];
	for (i = 0; i < count; i++) {
		[menu removeItemAtIndex:0];
	}
	
	
	NSArray* items = [[ThemeManager sharedManager] colorMenuItems];
	
	count = [items count];
	for (i = 0; i < count; i++) {
		NSMenuItem * item = [items objectAtIndex:i];
		[item setTarget:nil];
		[item setAction:@selector(colorMenuSelected:)];
		[menu addItem:item];
	}
}/*
- (int)numberOfItems{

	NSLog(@"numberOfItems");
	return [super numberOfItems];
}*/
- (void)awakeFromNib

{
	if( [self delegate] != self )
	[self setDelegate:self];
	/*
	unsigned int i, count = [super numberOfItems];
	for (i = 0; i < count; i++) {
		[super removeItemAtIndex:0];
	}
	
	
	NSArray* items = [[ThemeManager sharedManager] colorMenuItems];
	
	count = [items count];
	for (i = 0; i < count; i++) {
		NSMenuItem * item = [items objectAtIndex:i];
		[super addItem:item];
	}
	*/
}

@end
