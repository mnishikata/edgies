//
//  ZoomTextView.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/11.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "ZoomTextView.h"

#import "AppController.h"

#define CONTAINER_MAX 100000



@implementation ZoomTextView


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

		[self awakeFromNib];
		
	}
    return self;
}
- (void)awakeFromNib
{

	/*
	[self setHorizontallyResizable:YES];
	[self setVerticallyResizable:YES]; 
	[self setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];

	
	
	[[self textContainer] setWidthTracksTextView:NO];
	
	[[self enclosingScrollView] setHasVerticalScroller:YES];
	[[self enclosingScrollView] setHasHorizontalScroller:NO];
	*/
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	zoomFactor = [ud floatForKey:@"ZoomTextView_DefaultZoomFactor"];

	
	if( zoomFactor == 0 )
    zoomFactor = 1.0;
	
	[self setZoom:zoomFactor];
	
	/*
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(trackWidth:)
												 name:NSViewFrameDidChangeNotification
											   object:[self enclosingScrollView]];
	
	
	*/
	
	/// check user default
	
	BOOL flag = [ud boolForKey:@"isContinuousSpellChecking"];
	
	[self  setContinuousSpellCheckingEnabled:flag];
	
	
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}

- (void)setContinuousSpellCheckingEnabled:(BOOL)flag
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setBool:flag forKey:@"isContinuousSpellChecking"];
	
	
	[super  setContinuousSpellCheckingEnabled:flag];
}


-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

- (BOOL)acceptsFirstResponder {
	
	
	if( preferenceBoolValueForKey(@"orderFrontAutomatically") ) 
		[NSApp activateIgnoringOtherApps:YES];
	
	return YES;
}

-(void)setZoom:(float)newZoomFactor
{
	
	if( newZoomFactor < 0.01 ) newZoomFactor = 1.0;
	
	if( newZoomFactor > 200 ) newZoomFactor = 1.0;
	/*
	zoomFactor = newZoomFactor;
	
	NSScrollView* scrollView = [self enclosingScrollView];
	
	
	NSSize 	originalSize = [self frame].size;
	
	NSSize  newSize;
	newSize.width = originalSize.width / zoomFactor;
	newSize.height = originalSize.height / zoomFactor;	
	
	
	//before zooming, retrieve the top most character
	NSRange glyhRange = [[self layoutManager] 
					glyphRangeForBoundingRect:[scrollView documentVisibleRect] 
							  inTextContainer: [self textContainer]];
	NSRange charRange = [[self layoutManager]
					characterRangeForGlyphRange:glyhRange actualGlyphRange:NULL];
	
	
	//zoom
	[self  setBoundsSize:newSize];
	[scrollView setDocumentView:self];
	
	[self trackWidth:NULL];
	
	// scroll to visible the character retrieved before
	[self scrollRangeToVisible:charRange];
	 */
	

	
/// Simple zoom
		
		NSSize 	originalSize = [self frame].size;
		//float zoom = [sender floatValue];
		NSSize  newSize;
		newSize.width = originalSize.width / newZoomFactor;
		newSize.height = originalSize.height / newZoomFactor;	
		
		NSScrollView* scrollView = [self enclosingScrollView];
		
		//before zooming, retrieve the top most character
		NSRange glyhRange = [[self layoutManager] glyphRangeForBoundingRect:[scrollView documentVisibleRect] 
																inTextContainer: [self textContainer]];
		NSRange charRange = [[self layoutManager] characterRangeForGlyphRange:glyhRange actualGlyphRange:NULL];
		
		
		//zoom
		[self  setBoundsSize:newSize];
		[scrollView setDocumentView:self];
		
	
		
		// scroll to visible the character retrieved before
		[self scrollRangeToVisible:charRange ];
		

	
	
}


-(float)zoom
{
	return zoomFactor;
}


- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	/* remove MN item */
	int piyo;
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
		if( [[customMenuItem representedObject]  isEqualToString:@"MN_ZoomTextView"] )
		{
			[aContextMenu removeItem:customMenuItem];
		}
		
	}
	

		
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"MN_ZoomTextView"];
		[aContextMenu addItem:customMenuItem ];
		
		
		
		
		NSMenu* submenu = [[NSMenu alloc] init];
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"25%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:25];
		[customMenuItem setTarget:self];

		if( zoomFactor*100 == 25 ) [customMenuItem setState:NSOnState];

		[submenu addItem:[customMenuItem autorelease]];
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"50%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:50];
		[customMenuItem setTarget:self];
		
		if( zoomFactor*100 == 50 ) [customMenuItem setState:NSOnState];

		
		[submenu addItem:[customMenuItem autorelease]];
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"75%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:75];
		[customMenuItem setTarget:self];
		
		if( zoomFactor*100 == 75 ) [customMenuItem setState:NSOnState];

		
		[submenu addItem:[customMenuItem autorelease]];
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"100%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:100];
		[customMenuItem setTarget:self];
		
		if( zoomFactor*100 == 100 ) [customMenuItem setState:NSOnState];

		[submenu addItem:[customMenuItem autorelease]];
		
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"125%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:125];
		[customMenuItem setTarget:self];
		
		if( zoomFactor*100 == 125 ) [customMenuItem setState:NSOnState];

		[submenu addItem:[customMenuItem autorelease]];
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"150%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:150];
		[customMenuItem setTarget:self];
		
		if( zoomFactor*100 == 150 ) [customMenuItem setState:NSOnState];

		[submenu addItem:[customMenuItem autorelease]];
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:@"200%"																   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:200];
		[customMenuItem setTarget:self];
		
		if( zoomFactor*100 == 200 ) [customMenuItem setState:NSOnState];

		[submenu addItem:[customMenuItem autorelease]];
		
		
		
		//

		
		[submenu addItem:[NSMenuItem separatorItem]];
		
		
		
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Set as Default",@"")															   action:@selector(menu_selectedZoomTextView:)
											 keyEquivalent:@""];
		[customMenuItem setTag:-1];
		[customMenuItem setTarget:self];
		
		
		[submenu addItem:[customMenuItem autorelease]];
		
		
		
		
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Zoom",@"")
													action:@selector(menu_selectedZoomTextView:) keyEquivalent:@""];
		[customMenuItem setTag:0];

		[customMenuItem setRepresentedObject:@"MN_ZoomTextView"];
		[aContextMenu addItem:customMenuItem ];
		[aContextMenu setSubmenu:[submenu autorelease] forItem:customMenuItem];
		[customMenuItem autorelease];	
		

	return aContextMenu;
}


-(void)menu_selectedZoomTextView:(id)sender
{
	////NSLog(@"menu selecteed in zoomtv");

	if( [sender tag] > 0 )
	{
		zoomFactor = (float)[sender tag] / 100.0;
		[self setZoom:  zoomFactor ];
	}
	
	if( [sender tag] == -1 )
	{

		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		[ud setFloat:zoomFactor forKey:@"ZoomTextView_DefaultZoomFactor"];
		
	}
}

#pragma mark -
#pragma mark Color Panel Patch

//static BOOL MNIAMCHANGINGBACKGROUNDCOLORDONTBOTHERME = NO;

/*
- (void)setSelectedRange:(NSRange)charRange
{
	MNIAMCHANGINGBACKGROUNDCOLORDONTBOTHERME = NO;
	[super setSelectedRange:charRange];
}
 */
- (void)setTypingAttributes:(NSDictionary *)attributes
{
	//if( MNIAMCHANGINGBACKGROUNDCOLORDONTBOTHERME ) return;
	
	
	NSMatrix* matrix = [[[NSColorPanel sharedColorPanel] accessoryView] viewWithTag:100];
/*
	if( [[matrix cellWithTag:1] state] == NSOnState )
	{
		MNIAMCHANGINGBACKGROUNDCOLORDONTBOTHERME = YES;
		return;
	}
	
	else*/
	
	int version = [AppController OSVersion];
	
	
	if( version >= 105 )
	
		
	{
		[[matrix cellWithTag:0] setState:NSOnState];
		[[matrix cellWithTag:1] setState:NSOffState];
		[[matrix cellWithTag:2] setState:NSOffState];
	}
	
	[super setTypingAttributes: attributes];
}


@end
