//
//  MNIconizedCell.h
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "AttachmentCellConverter.h"
#import "ButtonAttachmentCell.h"

@protocol TagAttachmentCellOwnerTextView;

@interface TagAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{

	//NSString* name;
	//NSColor* color;
	
	//int alarmMode;
	
	//NSDate* alarmDate;
	//NSTimer* alarmTimer;
	

}
+(NSAttributedString*)newTagWithString:(NSString*)string  selected:(BOOL)selectNewTag;
- (void)__drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
-(NSSize)cellSize;
-(NSImage*)__image;
- (id)initWithAttachment:(NSTextAttachment*)anAttachment;
- (id)copyWithZone:(NSZone *)zone;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)untilMouseUp;
-(void)action:(id)sender;
-(void)activateInspectorForTextView:(NSTextView  *)aTextView;
- (void) dealloc ;
-(BOOL)textView:(NSTextView* )aTextView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString;
-(void)setState:(int)state;
-(void)setButtonSize:(float)size;
-(void)useAsDefault;
-(void)loadDefaults;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TagAttachmentCellOwnerTextView> *)aTextView;
-(void)editTag:(id)sender;
+(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TagAttachmentCellOwnerTextView> *)aTextView;
-(NSString*)description;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context ;//without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context;
-(NSDictionary*)pasteboardRepresentation:(id)context;



@end


@protocol TagAttachmentCellOwnerTextView 
//-(void)faviconAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)tagAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)tagAttachmentCellArrayInSelectedRange;

@end

