//
//  FindResultTable.m
//  SampleApp
//
//  Created by __Name__ on 06/04/10.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "MemoManagerTableView.h"

#import "SearchResultTableSizeValueTransformer.h"
#import "SearchResultTableDateCell.h"
#import "SearchResultTableScoreCell.h"
//#import "SearchResultTableIconCell.h"
#import "MNTabWindow.h"
#import "MemoManager.h"

@implementation MemoManagerTableView


+ (void)initialize {
    if (self == [MemoManagerTableView class]) {   
		
		
		NSValueTransformer *mbTrans = [[SearchResultTableSizeValueTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"SearchResultTableSizeValueTransformer"];
		[mbTrans release];
		
		
		
    }
}



-(void)drawRect:(NSRect)rect
{
	[super drawRect:rect];
	
	// draw selected row count number
	NSIndexSet* selectedRowIndexes =   [self selectedRowIndexes];
	if( [ selectedRowIndexes count ] > 0 )
	{
		unsigned hoge;
		for( hoge = 0; hoge < [self numberOfRows] ; hoge++ )
		{
			if( [selectedRowIndexes containsIndex:hoge] )
			{
				[[self delegate]  tableView:self willDisplayCell:NULL 
							   forTableColumn:[self tableColumnWithIdentifier:@"p_color" ] row:hoge];
			}
		}
	}
	
	
}
- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	NSPoint mouseLoc = [[self window]  mouseLocationOutsideOfEventStream]; 
	mouseLoc = [self convertPoint:mouseLoc fromView:[[self window] contentView]];
	
	
	NSRange range;
	int columnIndex;
	int rowIndex;
	
	range = [self columnsInRect:NSMakeRect(mouseLoc.x, mouseLoc.y, 1,1)];
	if( range.length == 0 ) columnIndex = -1;
	else
		columnIndex = range.location;
	
	range = [self rowsInRect:NSMakeRect(mouseLoc.x, mouseLoc.y, 1,1)];
	if( range.length == 0 ) rowIndex = -1;
	else
		rowIndex = range.location;
	
	
	////NSLog(@"c%d,r %d", columnIndex, rowIndex);
	
	[[self delegate] showPopupMenuForRow:rowIndex];
	
	return nil;
	
}


-(void)addColumnWithIdentifier:(NSString*)identifier title:(NSString*)title bindToObject:(id)object 
	/*
	 identifier : metadata attribute
	 title : if this is nil, it is set automatically
	 transformer : can be nil
	 
	 */
{
	
	if( identifier == nil ) return;
	if( [identifier isEqualToString:@""] ) return;
	
	
	
	[self setColumnAutoresizingStyle:NSTableViewNoColumnAutoresizing];
	
	
	
	
	// object = arrayController, keyPath = @"arrangedObjects.kmditemsomething"
	
	NSTableColumn* tb = [self tableColumnWithIdentifier:identifier];
	if( tb == nil )
	{
		NSTableColumn* tb = [[[NSTableColumn alloc] initWithIdentifier:identifier] autorelease];
		
		/*
		if( [identifier isEqualToString:kMDQueryResultContentRelevance] )
			title = NSLocalizedString(@"Score",@"");
		else if( [identifier isEqualToString:@"IconColumn.SpotInsideInternalAttribute"] )
			title = NSLocalizedString(@"",@"");
		*/
		

			
		if( title == nil )
		{
			title = identifier;
		}
		

		
		
		
		
		
		
		//check attribute type
		//NSDictionary* metadic = MDSchemaCopyMetaAttributesForAttribute(identifier);
		////NSLog(@"%@",metadic);
		//kMDAttributeType
		//(CFNumberRef, CFTypeId)
		
		/*
		 CFTypeId type = [[dic objectForKey:@"kMDAttributeType"] intValue];
		 if( type == CFDataGetTypeID() ) //data
		 {
			 
		 }*/
		
		
		//Customise cell
		
		NSNumber* BOOLYES = [NSNumber numberWithBool:YES];
		NSMutableDictionary*  dic = [NSMutableDictionary dictionaryWithObjectsAndKeys: BOOLYES, NSAllowsEditingMultipleValuesSelectionBindingOption,
			BOOLYES, NSCreatesSortDescriptorBindingOption,
			BOOLYES, NSRaisesForNotApplicableKeysBindingOption, nil];
		
		
		
		////NSLog(@"%@",[metadic description]);
		
		if( [title isEqualToString:@"p_color"] ) title = @"";

		
		if( [identifier isEqualToString: @"p_keywords" ] )//multiple
		{
			NSTokenFieldCell* cell = [[[NSTokenFieldCell alloc] init ] autorelease];
			[cell setControlSize:NSSmallControlSize];
			[cell setFont:[NSFont systemFontOfSize:[NSFont systemFontSizeForControlSize:NSSmallControlSize]]];
			[tb setDataCell:cell];	
			
		}else if( [identifier isEqualToString:@"creationDate"] )
		{
			SearchResultTableDateCell* cell = [[[SearchResultTableDateCell alloc] init ] autorelease];
			[cell setFont:[NSFont systemFontOfSize:[NSFont systemFontSizeForControlSize:NSSmallControlSize]]];
			
			[tb setDataCell:cell];
			
		}else if( [identifier isEqualToString: @"hidden" ] )
		{
			NSButtonCell* cell = [[[NSButtonCell alloc] init ] autorelease];
			[cell setButtonType:NSSwitchButton]; 
			[cell setTitle:@""];
			[tb setDataCell:cell];
			
			[dic setObject:NSNegateBooleanTransformerName forKey:NSValueTransformerNameBindingOption ];

			title = NSLocalizedString(@"Enabled",@"");
		}else 
		{
			NSTextFieldCell* cell = [[[NSTextFieldCell alloc] init ] autorelease];
			[cell setFont:[NSFont systemFontOfSize:[NSFont systemFontSizeForControlSize:NSSmallControlSize]]];
			[tb setDataCell:cell];
			
		}
		
		
		[[tb headerCell] setTitle:title];
		[[tb dataCell] setLineBreakMode:NSLineBreakByTruncatingTail];
		
		
		/*
		if( [identifier isEqualToString: @"IconColumn.SpotInsideInternalAttribute" ] )
		{
			SearchResultTableIconCell* cell = [[[SearchResultTableIconCell alloc] init ] autorelease];
			[cell setOwnerTableView:self andCacheDictionary:iconCache];
			
			[tb setDataCell:cell];
			
			keyPath = @"arrangedObjects.kMDItemPath";
			
		}*/
		
		////
		
		
		//if( metadic != nil ) CFRelease(metadic);
		
		
		

		
		
		//Customise Transformer
		
		/*
		if( [identifier isEqualToString: kMDItemFSSize ] ) 
		{
			[dic setObject:@"SearchResultTableSizeValueTransformer" forKey:NSValueTransformerNameBindingOption ];
		}*/
		
		
		
		//bind 
		//identifier = [self convertMetaAttributeNameToMiniDocumentKey:identifier];
		NSString* keyPath = [NSString stringWithFormat:@"arrangedObjects.%@",identifier];

		
		
		[tb bind:@"value" toObject:object withKeyPath:keyPath options :dic];
		
		
		//[tb setSize:NSMakeSize(30,12)];/// #$%
		[self addTableColumn:tb];
		[tb setEditable:YES];
		
		
		NSArray* editableColumns = [NSArray arrayWithObjects:@"p_title", @"p_subject", @"p_author", @"p_company", @"p_copyright", @"p_keywords", @"p_comment", @"hidden", nil ];
		if( [editableColumns containsObject: identifier ] ) 
			[[tb dataCell] setEditable:YES];
		
		
		//set width		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];	
		NSString* idKey = [NSString stringWithFormat:@"MemoListTableColumnSetting.%@",identifier];

		NSDictionary* udic = [ud objectForKey:idKey];
		if( udic != nil )
		{
			float width = [[udic objectForKey:@"width"] floatValue];
			[tb setWidth:width];
		}	
		
	}
	
	
	[self reloadData];
	[self setColumnAutoresizingStyle:NSTableViewUniformColumnAutoresizingStyle];
}


-(void)removeColumnWithIdentifier:(NSString*)identifier
{
	NSTableColumn* tb = [self tableColumnWithIdentifier:identifier];
	
	if( tb != nil )
	{
		[tb unbind:@"value"];
		[self removeTableColumn:tb];
	}
}


-(void)saveTableColumn
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSArray* columns = [self tableColumns];
	int hoge;
	for( hoge = 0; hoge < [self numberOfColumns]; hoge++ )
	{
		NSTableColumn* tb = [columns objectAtIndex:hoge];
		float width = [tb width];
		int idx = [self columnWithIdentifier:[tb identifier]];
		
		
		NSString* idKey = [NSString stringWithFormat:@"MemoListTableColumnSetting.%@",[tb identifier]];
		
		NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:width],@"width", [NSNumber numberWithInt:idx],@"index",nil];
		[ud setObject:dic forKey:idKey];
	}
	
}



-(IBAction)tearOff:(id)sender
{
	[[[ (MemoManager*)[self delegate] selectedDocument] window] tearOff:sender];
}

-(IBAction)export:(id)sender
{
	[[[ (MemoManager*)[self delegate] selectedDocument] window] export:sender];
	
}
-(IBAction)discard:(id)sender
{
	[[ (MemoManager*)[self delegate] selectedDocument]  close];
}
-(IBAction)showProperty:(id)sender
{
	[[[ (MemoManager*)[self delegate] selectedDocument] window] showProperty:sender];
	
}
-(IBAction)menuItemSelected:(id)sender
{
	[[[ (MemoManager*)[self delegate] selectedDocument] window] menuItemSelected:sender];
	
}
-(IBAction)print:(id)sender
{
	[[[ (MemoManager*)[self delegate] selectedDocument] window] print:sender];
	
}
-(void)changeColor:(id)sender
{
	[[[ (MemoManager*)[self delegate] selectedDocument] window] changeColor:sender];
	
}


@end
