//
//  SyncManager.h
//  sticktotheedge
//
//  Created by __Name__ on 07/01/31.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <SyncServices/SyncServices.h>


@interface SyncManager : NSObject {
	NSLock* lock;
}
+(SyncManager*)sharedSyncManager;
- (id) init ;
-(void)syncDocumentFolder:(NSString*)documentFolderPath pull:(BOOL)pull;
-(void)pushChangedFileAtPath:(NSString*)edgyDocumentPath name:(NSString*)name type:(ISyncChangeType)type ;
-(void)pushChangedFileAtPathThread:(NSDictionary*)threadDictionary;

@end
