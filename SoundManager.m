#import "SoundManager.h"
#import "EdgiesLibrary.h"

#define SOUND_LIST [NSArray arrayWithObjects:@"soundOpen", @"soundClose", @"soundTearOff", @"soundClickButton", \
	@"soundReleaseButton", @"soundBalloonOpen", @"soundBalloonClose", @"closeSound", @"exportSound", nil]

#define SOUND_LOCATION [NSArray arrayWithObjects:@"soundOpenPath", @"soundClosePath", @"soundTearOffPath", @"soundClickButtonPath", \
	@"soundReleaseButtonPath", @"soundBalloonOpenPath", @"soundBalloonClosePath", @"closeSoundPath", @"exportSoundPath", nil]

@implementation SoundManager

+(SoundManager*)sharedSoundManager
{
	return [ [NSApp delegate] soundManager	];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(setupSounds)
													 name:PreferenceDidChangeNotification
												   object:nil];
		
		[self setupSounds];	
	}
	return self;
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
 	[super dealloc];
}

-(void)setupSounds
{
	[self clearSounds];
	
	
	unsigned int i, count = [SOUND_LIST count];
	for (i = 0; i < count; i++) {
		NSString * soundname = [SOUND_LIST objectAtIndex:i];
		NSString* soundPath = preferenceValueForKey([SOUND_LOCATION objectAtIndex:i]);
		NSSound* sound = [[[NSSound alloc] initWithContentsOfFile: soundPath   byReference:NO] autorelease];

		if( sound != nil )
			[soundFiles setObject:sound forKey:soundname];
	}
	
}

-(void)playSoundWithName:(NSString*)soundName
{
	//NSLog(@"playSoundWithName %@", soundName);
	
	
	if(  preferenceBoolValueForKey(soundName) != YES ) return;
	
	
	if( [soundName isEqualToString:@"closeSound"] && preferenceBoolValueForKey(@"soundDiscardMemoUseDefault") )
	{
		SystemSoundPlay(15);
		return;

	}
	
	
	if( [soundName isEqualToString:@"exportSound"] && preferenceBoolValueForKey(@"soundExportUseDefault") )
	{
		SystemSoundPlay(1);
		return;
		
	}
	
	
	
	id soundfile = [soundFiles objectForKey:soundName];
	//NSLog(@"soundfile %@",soundfile);
	if( soundfile == nil ) return;
	
	[soundfile play];
	
}

-(void)clearSounds
{
	[soundFiles release];
	soundFiles = [NSMutableDictionary dictionary];
	[soundFiles retain];
}
@end
