/* FileEntityTextView */
#import "AttributeSafeTextView.h"
#import "ZoomTextView.h"
#import "FileEntityTextAttachmentCell.h"

#import <Cocoa/Cocoa.h>

@interface FileEntityTextView : ZoomTextView
{
	id draggingCell;
	
	unsigned draggingCellCharIndex;
}
- (id)initWithFrame:(NSRect)frame ;
- (void)awakeFromNib;
-(void)dealloc;
-(void)rescueItemsBeforeDiscarding;
-(unsigned)mouseOnFileEntity;// /// return index or NSNotFound // fileentity or alias;
-(FileEntityTextAttachmentCell* )mouseOnFileEntityCell;// /// return index or NSNotFound;
-(NSAttributedString*)fileEntityWithPath:(NSString*)filepath  copy:(BOOL)flag;
-(NSAttributedString*)aliasCellWith:(NDAlias*)alias;
- (BOOL)textView:(NSTextView *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString;
- (void)textView:(NSTextView *)aTextView clickedOnCell:(id <NSTextAttachmentCell>)attachmentCell inRect:(NSRect)cellFrame;
-(void)mouseDown:(NSEvent*)theEvent;
- (unsigned int)draggingSourceOperationMaskForLocal:(BOOL)isLocal;
- (void)draggedImage:(NSImage *)anImage endedAt:(NSPoint)aPoint operation:(NSDragOperation)operation;
-(void)deleteEmptyCell;
-(void)executeDelete:(BOOL)confirm;
- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender;
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)menu_selectedShowHideButtonTitle:(id)sender;


@end
