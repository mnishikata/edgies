/* ButtonInspector */

#import <Cocoa/Cocoa.h>
#import "ClipboardArchiveAttachmentCell.h"

@interface ClipboardArchiveInspector : NSObject
{
    IBOutlet id window;
	
	
	NSArray* /*ButtonAttachmentCell**/ activeCells;
}

+(ClipboardArchiveInspector*)sharedClipboardArchiveInspector;
- (id) init ;
-(BOOL)multipleSelection;
-(BOOL)noSelection;
-(void)showClipboardArchiveInspector;
-(id)imageData;
-(id)title;
-(void)setTitle:(NSString*)title;
-(id)font;
-(void)setFont:(NSFont*)font;
-(NSAttributedString*)attributedStringValue;
-(id)buttonSize;
-(void)setButtonSize:(NSNumber*)size;
-(id)buttonPosition;
-(void)setButtonPosition:(NSNumber*)position;
-(id)showButtonTitle;
-(void)setShowButtonTitle:(id)showButtonTitle;
-(id)showBezel;
-(void)setShowBezel:(id)showBezel;
-(void)dealloc;
-(BOOL)lock;
-(void)setLock:(BOOL)flag;
-(void)selectionChanged:(NSNotification*)notification;
-(void)updateInspector;
-(void)activateForTextView:(NSTextView  <ClipboardArchiveAttachmentCellOwnerTextView> *)view;
-(IBAction)useAsDefault:(id)sender;

@end
