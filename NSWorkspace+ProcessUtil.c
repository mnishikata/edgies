//
//  NSWorkspace+ProcessUtil.m
//  Jikeiretsu
//
//  Created by __Name__ on 07/04/23.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "NSWorkspace+ProcessUtil.h"
#import <IOKit/IOCFBundle.h>
#import <CoreServices/CoreServices.h>
#import <Carbon/Carbon.h>

#include <libc.h>
#include <sys/attr.h>
#include <stdio.h>
#include <stdlib.h>
#include <mach/mach.h>
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/sysctl.h>

typedef struct kinfo_proc kinfo_proc;

static int GetBSDProcessList(kinfo_proc **procList, size_t *procCount);

#define MSTR(mask) { mask, #mask }
#define ISSET(bit) ( bit ? "YES" : "NO" )
#define HASCUSTOMICON(bit)  ( bit ? true : false )
const struct ffbit {
	u_int32_t mask;
	char *description;
} ffarray[] = {
	MSTR(kIsOnDesk),
	MSTR(kColor),
	MSTR(kIsShared),
	MSTR(kHasNoINITs),
	MSTR(kHasBeenInited),
	MSTR(kHasCustomIcon),
	MSTR(kIsStationery),
	MSTR(kNameLocked),
	MSTR(kHasBundle),
	MSTR(kIsInvisible),
	MSTR(kIsAlias)
};



struct fileinfobuf {
	u_int32_t info_length;
	union {
		u_int32_t padding[8];
		struct {
			u_int32_t type;
			u_int32_t creator;
			u_int16_t fdFlags;
			u_int16_t location;
			u_int32_t padding[4];
		} info;
	} data;
};

struct filesizebuf {
	u_int32_t info_length;
	off_t dataForkSize;
	off_t rsrcForkSize;
};





//ProcessUtil
CFArrayRef copyLaunchedProcessPaths(void) {
	
	CFMutableArrayRef array = CFArrayCreateMutable (
											nil,
											0,
											&kCFTypeArrayCallBacks
											);
	
    ProcessSerialNumber psn = {0, kNoProcess};
    FSRef ref;
    UInt8 cPath[PATH_MAX]; // cPath will be a UTF-8 encoded C string
    OSErr err;
    do {
        err = GetNextProcess(&psn);
        if (err == noErr) {
            GetProcessBundleLocation(&psn, &ref);
            FSRefMakePath(&ref, cPath, PATH_MAX);
			
			//Create CFString
			CFStringRef string = CFStringCreateWithFileSystemRepresentation (
																			 nil,
																			 (char *)cPath
																			 );

			CFArrayAppendValue(array,string);
        }
    } while (err != procNotFound);
	
    return array;
}
//


Boolean executableIsRunning(CFStringRef name) {
	//  NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
    ProcessSerialNumber psn = {0, kNoProcess};
	//  FSRef ref;
	//  UInt8 cPath[PATH_MAX]; // cPath will be a UTF-8 encoded C string
    OSErr err;
    do {
        err = GetNextProcess(&psn);
        if (err == noErr) {
			CFStringRef aName;
			if( CopyProcessName ( &psn, &aName ) == noErr )
			{
				CFComparisonResult result =	CFStringCompare(name,aName,0);
				if( kCFCompareEqualTo == result )
				{
					return true;
				}
			}
			
						
        }
    } while (err != procNotFound);
    return false;
}


int launchedProcessPIDForBundleIdentifier(CFStringRef bndlid) {
  //  NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
    ProcessSerialNumber psn = {0, kNoProcess};
  //  FSRef ref;
  //  UInt8 cPath[PATH_MAX]; // cPath will be a UTF-8 encoded C string
    OSErr err;
    do {
        err = GetNextProcess(&psn);
        if (err == noErr) {
			
			CFDictionaryRef dict = ProcessInformationCopyDictionary(&psn, kProcessDictionaryIncludeAllInformationMask);
			
			CFStringRef identifier = CFDictionaryGetValue(dict,kIOBundleIdentifierKey);
							
			if( identifier != nil && CFGetTypeID(identifier) == CFStringGetTypeID() )
			{
				CFComparisonResult result =	CFStringCompare(identifier,bndlid,0);
					
				
				if( kCFCompareEqualTo == result )
				{
					int value = 0;

					CFNumberRef num = CFDictionaryGetValue(dict,CFSTR("pid"));
					if( num != nil && CFGetTypeID(num) == CFNumberGetTypeID() )
					{
						CFNumberGetValue (
										  num,
										  kCFNumberIntType,
										  &value
										  );
						
						if( dict != nil ) CFRelease(dict);
						return value;
					}
				}
			}
			
			if( dict != nil ) CFRelease(dict);
			
        }
    } while (err != procNotFound);
    return 0x7fffffff;
}

int countForLaunchedProcessPIDForBundleIdentifier(CFStringRef bndlid) {

	int count = 0;
    ProcessSerialNumber psn = {0, kNoProcess};
    OSErr err;
    do {
        err = GetNextProcess(&psn);
        if (err == noErr) {
			
			CFDictionaryRef dict = ProcessInformationCopyDictionary(&psn, kProcessDictionaryIncludeAllInformationMask);
			
			CFStringRef identifier = CFDictionaryGetValue(dict,kIOBundleIdentifierKey);
			
			if( identifier != nil && CFGetTypeID(identifier) == CFStringGetTypeID() )
			{
				CFComparisonResult result =	CFStringCompare(identifier,bndlid,0);
				
				
				if( kCFCompareEqualTo == result )
					count++;
			}
			
			if( dict != nil ) CFRelease(dict);

			
        }
    } while (err != procNotFound);
    return count;
}

Boolean hasCustomIconFileAtPath(CFStringRef filepath)
{
	struct attrlist alist;
	struct fileinfobuf finfo;
	
//	struct attrlist blist;
//	struct filesizebuf fsize;
	
//	int i;
	int err;
	
	
	CFIndex maxBuffer = CFStringGetMaximumSizeOfFileSystemRepresentation(filepath);
	
	char path[maxBuffer];
	
	Boolean flag = CFStringGetFileSystemRepresentation (
														filepath,
														path,
														maxBuffer
														);
	
	if( !flag ) return false;

	
	
	
	
	
	alist.bitmapcount = 5;
	alist.reserved = 0;
	alist.commonattr = ATTR_CMN_FNDRINFO;
	alist.volattr = 0;
	alist.dirattr = 0;
	alist.fileattr = 0;
	alist.forkattr = 0;
	
	
	/* last argument is whether to resolve symlinks. 0=yes, 1=no */
	err = getattrlist(path, &alist, &finfo, sizeof(finfo),
					  FSOPT_NOFOLLOW);
	
	
	if(err) {
		//fprintf(stderr, "Can't get file information for %s\n", path);
		return false;
	}
	
	//	printf("           Type: %4.4s\n", &finfo.data.info.type);
	//	printf("        Creator: %4.4s\n", &finfo.data.info.creator);
	
	flag =  HASCUSTOMICON(finfo.data.info.fdFlags & ffarray[5].mask );
	//NSLog(@"Has Custom Icon %d",  flag);
	return flag;
	
	
		  /*
		   for(i=0; i < sizeof(ffarray)/sizeof(struct ffbit); i++) {
			   printf("i %d %15s: %s\n", i, ffarray[i].description,
					  ISSET(finfo.data.info.fdFlags & ffarray[i].mask));
			   
			   // uncomment this if you want to turn all flags on 
			   // finfo.data.info.fdFlags |= ffarray[i].mask;
			   
			   
		   }*/
	
	/* uncomment this if you want to set the type */
	// finfo.data.info.type = 'cool';
	
	/* uncomment this if you want to write back attributes */
	/*
	 err = setattrlist(path, &alist, &finfo.data, sizeof(finfo.data), 1);
	 if(err) {
		 fprintf(stderr, "Error while setting file information for %s\n", 
				 path);
		 exit(1);
	 }
	 */
	
	
	/*
	 blist.bitmapcount = 5;
	 blist.reserved = 0;
	 blist.commonattr = 0;
	 blist.volattr = 0;
	 blist.dirattr = 0;
	 blist.fileattr = ATTR_FILE_DATALENGTH | ATTR_FILE_RSRCLENGTH;
	 blist.forkattr = 0;
	 
	 
	 // last argument is whether to resolve symlinks. 0=yes, 1=no 
	 err = getattrlist(path, &blist, &fsize, sizeof(fsize),
					   FSOPT_NOFOLLOW);
	 if(err) {
		 fprintf(stderr, "Can't get file size information for %s\n", path);
		 exit(1);
	 }
	 
	 
	 printf("    Data fork length: %20qu (0x%016qX) bytes\n",
			fsize.dataForkSize, fsize.dataForkSize);
	 printf("Resource fork length: %20qu (0x%016qX) bytes\n",
			fsize.rsrcForkSize, fsize.rsrcForkSize);
	 
	 */
	
	//return YES;
}

Boolean BSDProcessIsRunning(CFStringRef processName)
{
	kinfo_proc* procList = nil;
	size_t procCount = 0;
	
	
	if( GetBSDProcessList(&procList, &procCount) == 0 )
	{
		
		int num = 0;
		for( num = 0; num < procCount; num++ )
		{
			//printf("stat %s %d\n", procList[num].kp_proc.p_comm, procList[num].kp_proc.p_stat);
			
			CFStringRef name = CFStringCreateWithFileSystemRepresentation(nil,procList[num].kp_proc.p_comm);
			
			CFComparisonResult result =	CFStringCompare(name,processName,0);
			
			if( name ) CFRelease(name);
			
			if( (kCFCompareEqualTo == result ) && (procList[num].kp_proc.p_stat != 5))
			{
				return true;
			}
			
		
		
		
		}
		if( procList )
			free(procList);
							 
	}				 
	
	
	return false;

	// Add your subclass-specific initialization here.
	// If an error occurs here, send a [self release] message and return nil.
    

}

static int GetBSDProcessList(kinfo_proc **procList, size_t *procCount)
// システム上のすべての BSD プロセスのリストを返す
// このルーチンは、このリストの割り当てを行って *procList に入れ
// エントリの数を *procCount に返す
// このリストは自分で解放しなければならない（System フレームワークの free を使用）
// 成功時、関数は 0 を返す
// エラー発生時には、関数は BSD の errno の値を返す
{
    int                 err;
    kinfo_proc *        result;
    bool                done;
    static const int    name[] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };
    // name を const として宣言するので、sysctl に渡すときにキャストする必要がある
    // プロトタイプに const が含まれていないからだ
    size_t              length;
	
    assert( procList != NULL);
    assert(*procList == NULL);
    assert(procCount != NULL);
	
    *procCount = 0;
	
    // result == NULL と length == 0 として sysctl を呼び出すことにより開始。
    // これは成功し、length が適切な長さに設定される。
    // その後そのサイズのバッファを割り当て、sysctl を再び呼び出す
    // これが成功すれば処理は終了である。ENOMEM エラーで失敗した場合は
    // バッファを破棄してループする必要がある。
    // このループでは、再び NULL を指定して、sysctl を呼び出す必要があることに注意。
    // これは必要な処理である。ENOMEM エラーの場合、length は、
    // 返されるべきデータ量ではなく
    // 返されたデータ量に設定されるからだ
	
    result = NULL;
    done = false;
    do {
        assert(result == NULL);
		
        // バッファを NULL にして sysctl を呼び出す
		
        length = 0;
        err = sysctl( (int *) name, (sizeof(name) / sizeof(*name)) - 1,
                      NULL, &length,
                      NULL, 0);
        if (err == -1) {
            err = errno;
        }
		
        // 上記の呼び出しの結果に基づき適切なサイズの
        // バッファを割り当てる
		
        if (err == 0) {
            result = malloc(length);
            if (result == NULL) {
                err = ENOMEM;
            }
        }
		
        // この新しいバッファを使って sysctl を再び呼び出す
        // ENOMEM エラーを受け取った場合は、バッファを破棄しもう一度やり直す
		
        if (err == 0) {
            err = sysctl( (int *) name, (sizeof(name) / sizeof(*name)) - 1,
                          result, &length,
                          NULL, 0);
            if (err == -1) {
                err = errno;
            }
            if (err == 0) {
                done = true;
            } else if (err == ENOMEM) {
                assert(result != NULL);
                free(result);
                result = NULL;
                err = 0;
            }
        }
    } while (err == 0 && ! done);
	
    // クリーンナップ処理を行い、事後状態を設定する
	
    if (err != 0 && result != NULL) {
        free(result);
        result = NULL;
    }
    *procList = result;
    if (err == 0) {
        *procCount = length / sizeof(kinfo_proc);
    }
	
    assert( (err == 0) == (*procList != NULL) );
	
    return err;
}
