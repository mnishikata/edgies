//
//  MNTabWindow.h
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "EdgiesLibrary.h"

#import "MNTabWindowViewAnimation.h"

//
// tabOpenStatus 0 NO , 1 YES, 2 Temporarily, 3 floating
//

@interface MNTabWindow : NSPanel {

	IBOutlet id mainContent; //dragView

	IBOutlet id dragButton_top;
	IBOutlet id dragButton_bottom;
	IBOutlet id dragButton_left;
	IBOutlet id dragButton_right;
	IBOutlet id customMenu;
	
	IBOutlet id sheet;
	IBOutlet id ownerDocument;

	
	//status
	
	TAB_OPEN_STATUS		tabOpenStatus;
	BOOL				canBecomeMainWindow;
	TAB_POSITION		tabPosition;
	int screenID;
	
	
	
	// preferences
	
	float transparencyWhenClosed;
	float transparencyWhenOpened;
	
	int windowLevel;
	int windowLevelOpened;
	NSTimeInterval animationResizeTime;

	
	
	
	
	
	NSTimer* closeTimer;
	
	NSDictionary* customizingFridge;	
	
	NSRect phantomOpenFrame;
	NSRect phantom_actualOpenFrame;
	

	MNTabWindowViewAnimation* viewAnimation;
	
	
	int demoCount;
	
	
	// Order Out Override
	BOOL isVisible;

	
}
- (id)initWithContentRect:(NSRect)contentRect 
				styleMask:(unsigned int)styleMask 
				  backing:(NSBackingStoreType)backingType 
					defer:(BOOL)flag;
	
	- (void)performClose:(id)sender;
-(void)setPreference:(NSNotification*)notification;
-(NSRect)tabBoundsInScreen;
-(NSScreen*)screenWhereTabIs;
-(int)edgeThatThisWindowBelongsTo; /// return unique number depending on screen edge;
-(void)setScreenID:(int)idNum;
-(NSRect)solveOffScreen ;//  タブが画面外にいるのを解決;
-(TAB_POSITION)tabPosition;
-(BOOL)isOpened;
-(BOOL)isTornOff;
-(void)setTornOff:(BOOL)flag;
-(NSRect)hideFrame;
-(NSRect)closedFrame;
-(void)windowDidClosed;
-(void)removeObserver;
-(void)showMenu;
-(IBAction)menuItemSelected:(id)sender;
-(IBAction)colorMenuSelected:(id)sender;
-(IBAction)showProperty:(id)sender;
-(IBAction)setAsDefaultSize:(id)sender;
-(void)close;
-(void)setNormalLevel;
-(void)setFloatingLevel;
-(void)setHiddenLevel;
-(void)setTemporarilyTornOff:(BOOL)flag;
-(void)reposition;
-(NSRect)frameForProposedFrame:(NSRect)frame;
-(void)setTabPosition:(TAB_POSITION)pos;
-(void)setupTabs;
-(TAB_OPEN_STATUS)tabOpenStatus;
-(void)setTabStatus:(TAB_OPEN_STATUS)status;
-(id)ownerDocument;
-(NSImage*)balloonImage;
- (void)setFrameUsingAnimation:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation endSelector:(SEL)endSel  canStopPreviousAnimation:(BOOL)stopAnimation duration:(NSTimeInterval)duration;
- (void)setFrameUsingAnimation:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation canStopPreviousAnimation:(BOOL)flag  duration:(NSTimeInterval)duration;
- (NSTimeInterval)animationResizeTime:(NSRect)newWindowFrame;
- (NSSize)minSize;
- (void) setStickyWhenExpose;
- (void) setHideWhenExpose:(BOOL)flag;
-(void)changeColor:(id)sender;
-(NSString*)title;
-(void)redrawWithShadowProperly;
-(BOOL)mouseOnWindow;
-(BOOL)mouseOnTab;
-(NSButton*)tabButton;
- (BOOL)canBecomeKeyWindow;
- (BOOL)canBecomeMainWindow;
- (void)setCanBecomeMainWindow:(BOOL)flag;
- (void)resignKeyWindow;
-(void)openTab;
-(void)openTabBlocking:(BOOL)flag;
-(void)closeTab;
-(void)closeTabBlocking:(BOOL)flag;
-(void)tearOffWithoutMovement;
-(void)tearOffWithMovement;
-(IBAction)tearOff:(id)sender;
-(void)toggleTab;
-(IBAction)align:(id)sender;
-(IBAction)alignIgnoringMe:(id)sender;
-(void)closeTimer;
-(void)closeTabIfOtherAppFrontmost:(NSNotification*)notif;
-(void)closeTabCalledByNotification:(NSNotification*)notif;
-(void)repositionToMouse;
-(NSRect)visibleFrame;

@end
