#import "AlarmInspector.h"
#import "AlarmAttachmentCell.h"

#define SOUND_FOLDER_1 @"/Library/Sounds/"
#define SOUND_FOLDER_2 [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Sounds/"]
#define SOUND_FOLDER_3 @"/System/Library/Sounds/"


#define SIMPLE_BEEP_SOUND_PATH @"ALARM_ATTACHMENT_CELL_DEFAULT_BEEP"
#define NONE_SOUND_PATH @"ALARM_ATTACHMENT_CELL_NO_BEEP"


@implementation AlarmInspector

+(AlarmInspector*)sharedAlarmInspector
{
	return [[NSApp delegate] alarmInspector];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		
		NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
		
		activeCells = nil;
		alarmDate = nil;
		
		if( [ud objectForKey:@"AlarmAttachmentCell_alarmMode"] )
			alarmMode = [ud integerForKey:@"AlarmAttachmentCell_alarmMode"];
		else 
			alarmMode = 0;
		
		
		if( [ud objectForKey:@"AlarmAttachmentCell_alarmSoundPath"] )
			alarmSoundPath = [[ud objectForKey:@"AlarmAttachmentCell_alarmSoundPath"] retain];
		else 
			alarmSoundPath = [SIMPLE_BEEP_SOUND_PATH retain];
		
		
		if( [ud objectForKey:@"AlarmAttachmentCell_alarmLoop"] )
			alarmLoop = [ud integerForKey:@"AlarmAttachmentCell_alarmLoop"];
		else 
			alarmLoop = NO;
		
		
		[NSBundle loadNibNamed:@"AlarmInspector" owner:self];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(selectionChanged:)
													 name:NSTextViewDidChangeSelectionNotification
												   object:nil];
		[window setFloatingPanel:YES];

	}
	return self;
}
-(BOOL)multipleSelection
{
	if( [activeCells count] >1) return YES;
	return NO;
}
-(BOOL)noSelection
{
	if( [activeCells count] == 0 ) return YES;
	return NO;
}
-(void)showAlarmInspector
{
	[window orderFront:self];	
	[self updateInspector];
}
-(id)imageData
{
	if( [activeCells count] == 1) return [[activeCells objectAtIndex:0] valueForKey:@"imageData"];

	else return nil;
}
-(id)title
{
	if( [activeCells count] != 1) return @"Multiple Selection";

	return [[activeCells objectAtIndex:0] valueForKey:@"title"];
}
-(NSString*)targetPath
{
	if( [activeCells count] != 1) return @"";
	
	return [[activeCells objectAtIndex:0] valueForKey:@"targetPath"];
}
-(void)setTitle:(NSString*)title
{
	[activeCells setValue:title forKey:@"title"];
}

-(id)font
{
	if( [activeCells count] == 0) return nil;

	return [[activeCells objectAtIndex:0] valueForKey:@"font"];
	
}
-(void)setFont:(NSFont*)font
{
	[activeCells setValue:font forKey:@"font"];
}

-(id)buttonSize
{
	if( [activeCells count] != 1) return nil;

	
	return 	 [[activeCells objectAtIndex:0] valueForKey:@"buttonSize"];

}
-(void)setButtonSize:(NSNumber*)size
{
	
	[activeCells setValue:size forKey:@"buttonSize"];
}
-(id)buttonPosition
{
	if( [activeCells count] != 1) return nil;

	return 	 [[activeCells objectAtIndex:0] valueForKey:@"buttonPosition"];
}
-(void)setButtonPosition:(NSNumber*)position
{
	[activeCells setValue:position forKey:@"buttonPosition"];
}
-(id)showButtonTitle
{
	if( [activeCells count] != 1) return nil;

	return  [[activeCells objectAtIndex:0] valueForKey:@"showButtonTitle"];
}
-(void)setShowButtonTitle:(id)showButtonTitle
{
	[activeCells setValue:showButtonTitle forKey:@"showButtonTitle"];
}





/*
-(id)state
{
	if( [activeCells count] == 0) return nil;
	
	return [[activeCells objectAtIndex:0] valueForKey:@"state"];
	
}
-(void)setState:(NSNumber*)state
{
	[activeCells setValue:state forKey:@"state"];
}


-(int)alarmMode
{
	if( [activeCells count] == 0) return nil;
	
	return [[activeCells objectAtIndex:0] valueForKey:@"alarmMode"];
	
	
}
-(void)setAlarmMode:(int)mode
{
	[activeCells setValue:mode forKey:@"alarmMode"];

}*/

//		[self setBordered:showBezel];
-(id)showBezel
{
	if( [activeCells count] != 1) return nil;
	
	return  [[activeCells objectAtIndex:0] valueForKey:@"showBezel"];
}
-(void)setShowBezel:(id)showBezel
{
	[activeCells setValue:showBezel forKey:@"showBezel"];
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	
	[super dealloc];
}

-(BOOL)lock
{
	BOOL flag = NO;
	unsigned int i, count = [activeCells count];
	for (i = 0; i < count; i++) {
		NSObject * obj = [activeCells objectAtIndex:i];
		if( [[obj valueForKey:@"lock"] boolValue] == YES )
		{
			flag = YES;
			break;
		}
	}
	
	return  flag;
}
-(void)setLock:(BOOL)flag
{
	[activeCells setValue:[NSNumber numberWithBool:flag] forKey:@"lock"];

}

-(void)selectionChanged:(NSNotification*)notification
{
	if( ! [window isVisible] ) return;
	
	NSTextView* view = [notification object];
	if( view == nil ) return;

	if( ! [view conformsToProtocol:@protocol(AlarmAttachmentCellOwnerTextView)] ) return;

	
	[self activateForTextView:view];
	
	
	////NSLog(@"selectionChanged %@",[view className]);


}

-(void)updateInspector
{	  
	if( ! [window isVisible] ) return;

	[self willChangeValueForKey:@"title"];
	[self didChangeValueForKey:@"title"];
	
	[self willChangeValueForKey:@"font"];
	[self didChangeValueForKey:@"font"];
	
	[self willChangeValueForKey:@"buttonSize"];
	[self didChangeValueForKey:@"buttonSize"];
	
	[self willChangeValueForKey:@"buttonPosition"];
	[self didChangeValueForKey:@"buttonPosition"];
	
	[self willChangeValueForKey:@"imageData"];
	[self didChangeValueForKey:@"imageData"];
	
	[self willChangeValueForKey:@"showButtonTitle"];
	[self didChangeValueForKey:@"showButtonTitle"];

	[self willChangeValueForKey:@"showBezel"];
	[self didChangeValueForKey:@"showBezel"];
		
	[self willChangeValueForKey:@"targetPath"];
	[self didChangeValueForKey:@"targetPath"];	
	
	[self willChangeValueForKey:@"lock"];
	[self didChangeValueForKey:@"lock"];	
	
	[self willChangeValueForKey:@"noSelection"];
	[self didChangeValueForKey:@"noSelection"];	
	
	[self willChangeValueForKey:@"multipleSelection"];
	[self didChangeValueForKey:@"multipleSelection"];	

	[self willChangeValueForKey:@"state"];
	[self didChangeValueForKey:@"state"];	


	
}



-(void)activateForTextView:(NSTextView  <AlarmAttachmentCellOwnerTextView> *)view
{
		
	if( [view conformsToProtocol:@protocol(AlarmAttachmentCellOwnerTextView)] )
	{
//		NSRange range = [view selectedRange];
		
		[activeCells release];
		activeCells = [[NSMutableArray arrayWithArray: [view alarmAttachmentCellArrayInSelectedRange]] retain];
		
			
		[self updateInspector];

		//[objectController setContent:sender];
		[self showAlarmInspector];
	}else
	{
		[activeCells release];
		activeCells = nil;
	}
}

-(IBAction)useAsDefault:(id)sender
{
	if( [activeCells count] > 0 )
		[[activeCells objectAtIndex:0] useAsDefault];
}

#pragma mark Dialogue

-(void)insertNewAlarmForTextView:(NSTextView*)textView
{
	[self willChangeValueForKey:@"alarmDate"];
	//alarmMode
	[alarmDate release];
	alarmDate = [[NSDate date] retain];
	[self didChangeValueForKey:@"alarmDate"];

	
	
	[dialogue setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	[NSApp beginSheet:dialogue modalForWindow:[textView window] modalDelegate:self
	   didEndSelector:@selector(sheetDidEndForNewAlarm:returnCode:contextInfo:) contextInfo:textView];
}
-(IBAction)buttonClicked:(id)sender
{
	[dialogue orderOut:self];
	[NSApp endSheet:dialogue returnCode:[sender tag]];
	

}
- (void)sheetDidEndForNewAlarm:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{
	
	if( returnCode == 1 )
	{
		NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
		[ud setInteger:alarmMode forKey:@"AlarmAttachmentCell_alarmMode"];
		[ud setBool:alarmLoop forKey:@"AlarmAttachmentCell_alarmLoop"];
		[ud setObject:alarmSoundPath forKey:@"AlarmAttachmentCell_alarmSoundPath"];

		[[NSUserDefaults standardUserDefaults] synchronize];
		
		NSAttributedString* attr = [AlarmAttachmentCell newAlarmWithDate:alarmDate
															   alarmMode:alarmMode 
														  alarmSoundPath:alarmSoundPath 
															   alarmLoop:alarmLoop
			
			];
		
		
		
		[(NSTextView*)contextInfo insertText:attr];
		
	}
}
//
-(void)modifyAlarm:(AlarmAttachmentCell*)cell
{
	[self willChangeValueForKey:@"alarmDate"];
	[self willChangeValueForKey:@"alarmMode"];
	[self willChangeValueForKey:@"alarmSoundPath"];
	[self willChangeValueForKey:@"alarmLoop"];
	[self willChangeValueForKey:@"alarmSoundName" ];

	[alarmDate release];
	alarmDate = [[cell alarmDate] copy];
	
	alarmMode = [cell alarmMode];
	
	[alarmSoundPath release];
	alarmSoundPath = [[cell alarmSoundPath] retain];
	
	alarmLoop = [cell alarmLoop];

	[self didChangeValueForKey:@"alarmDate"];
	[self didChangeValueForKey:@"alarmMode"];
	[self didChangeValueForKey:@"alarmSoundPath"];
	[self didChangeValueForKey:@"alarmLoop"];
	[self didChangeValueForKey:@"alarmSoundName" ];

	[dialogue setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	[NSApp beginSheet:dialogue modalForWindow:[[cell textView] window] modalDelegate:self
	   didEndSelector:@selector(sheetDidEndForModifingAlarm:returnCode:contextInfo:) contextInfo:cell];
}

- (void)sheetDidEndForModifingAlarm:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{

	if( returnCode == 1 )
	{
		[(AlarmAttachmentCell*)contextInfo setAlarmDate:alarmDate];
		[(AlarmAttachmentCell*)contextInfo setAlarmMode:alarmMode];
		[(AlarmAttachmentCell*)contextInfo setAlarmLoop:alarmLoop];
		[(AlarmAttachmentCell*)contextInfo setAlarmSoundPath:alarmSoundPath];

		
		NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
		[ud setInteger:alarmMode forKey:@"AlarmAttachmentCell_alarmMode"];
		[ud setBool:alarmLoop forKey:@"AlarmAttachmentCell_alarmLoop"];
		[ud setObject:alarmSoundPath forKey:@"AlarmAttachmentCell_alarmSoundPath"];
		
		[[NSUserDefaults standardUserDefaults] synchronize];

		

	}
}


#pragma mark Add on

- (void)menuNeedsUpdate:(NSMenu *)menu 
	// Modify mounted volume list
{
	
	NSMutableArray *soundPaths = [[[NSMutableArray alloc] init] autorelease];
	
	
	
	
	
	
	NSBundle *myBundle = [NSBundle bundleForClass:[self class]];		
	[soundPaths addObjectsFromArray: [myBundle pathsForResourcesOfType:@"aiff" inDirectory:nil]];
	[soundPaths addObjectsFromArray: [myBundle pathsForResourcesOfType:@"wav" inDirectory:nil]];
	
	
	//seach aplication support folder
	NSArray* contents = [[NSFileManager defaultManager] subpathsAtPath: SOUND_FOLDER_1];
	
	
	
	
	//////NSLog(@"%@",[contents description]);
	int hoge;
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		
		NSString* item = [contents objectAtIndex:hoge];
		
		if( [item hasSuffix:@".aiff"] || [item hasSuffix:@".wav"] )
		{
			NSString* itemPath = [SOUND_FOLDER_1 stringByAppendingPathComponent:item];
			[soundPaths addObject : itemPath];
		}
	}
	
	
	/*
	
	// another folder
	contents = [[NSFileManager defaultManager] subpathsAtPath: SOUND_FOLDER_2];
	
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		
		NSString* item = [contents objectAtIndex:hoge];
		
		if( [item hasSuffix:@".aiff"] || [item hasSuffix:@".wav"] )
		{
			NSString* itemPath = [SOUND_FOLDER_2 stringByAppendingPathComponent:item];
			[soundPaths addObject : itemPath];
		}
	}
	*/
	
	// system folder
	contents = [[NSFileManager defaultManager] subpathsAtPath: SOUND_FOLDER_3];
	
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		
		NSString* item = [contents objectAtIndex:hoge];
		
		if( [item hasSuffix:@".aiff"] || [item hasSuffix:@".wav"] )
		{
			NSString* itemPath = [SOUND_FOLDER_3 stringByAppendingPathComponent:item];
			[soundPaths addObject : itemPath];
		}
	}
	
	
	/// 1. delete existing menu
	
	
	unsigned int i, count = [menu numberOfItems];
	for (i = 0; i < count; i++) {
		[menu removeItemAtIndex:0];
	}
	
	
	/// 2. customize menu
	
	
	NSMenuItem* menuItem = [[NSMenuItem alloc] initWithTitle:
		
		NSLocalizedStringFromTableInBundle( @"Choose sound file...",nil,[NSBundle bundleForClass:[self class]],@"")
		
													  action:@selector(chooseSound:)
											   keyEquivalent:@""];
	[menuItem autorelease];
	[menuItem setTarget:self];
	
	[menuItem setRepresentedObject:[NSNull null]];
	
	[menuItem setTag: 0];
	
	[menu insertItem: menuItem atIndex:0];
	
	[menu insertItem: [NSMenuItem separatorItem] atIndex:0];
	
	
	/// 3. sound
	
	for( hoge = 0; hoge < [soundPaths count]; hoge++ )
	{
		
		NSString* path = [soundPaths objectAtIndex:hoge];
		NSString* title = [[path stringByDeletingPathExtension] lastPathComponent];
		
		NSMenuItem* menuItem = [[NSMenuItem alloc] initWithTitle:title
														  action:@selector(soundSelected:)
												   keyEquivalent:@""];
		[menuItem autorelease];
		[menuItem setTarget:self];
		
		
		
		/*
		 NSImage* docIconInDock = [ColorCheckbox roundedBox:color
													   size:NSMakeSize(12,12)
													  curve:4 
												 frameColor:frameColor];
		 [menuItem setImage:docIconInDock ];
		 */
		
		[menuItem setRepresentedObject:path];
		
		[menuItem setTag: 100];
		
		[menu insertItem: menuItem atIndex:0];
		
	}
	
	[menu insertItem: [NSMenuItem separatorItem] atIndex:0];

	
	//defualt beep
	menuItem = [[NSMenuItem alloc] initWithTitle:
		
		NSLocalizedStringFromTableInBundle( @"Default Beep",nil,[NSBundle bundleForClass:[self class]],@"")
		
										  action:@selector(soundSelected:)
								   keyEquivalent:@""];
	[menuItem autorelease];
	[menuItem setTarget:self];
	
	[menuItem setRepresentedObject:SIMPLE_BEEP_SOUND_PATH];
	
	[menuItem setTag: 0];
	
	[menu insertItem: menuItem atIndex:0];
	
	/// no beep
	menuItem = [[NSMenuItem alloc] initWithTitle:
		
		NSLocalizedStringFromTableInBundle( @"No Sound",nil,[NSBundle bundleForClass:[self class]],@"")
		
										  action:@selector(soundSelected:)
								   keyEquivalent:@""];
	[menuItem autorelease];
	[menuItem setTarget:self];
	
	[menuItem setRepresentedObject:NONE_SOUND_PATH];
	
	[menuItem setTag: 0];
	
	[menu insertItem: menuItem atIndex:0];
	
	
	
}







-(void)soundSelected:(id)sender
{
	NSString* path = [sender representedObject];
		
	[self willChangeValueForKey:@"alarmSoundName" ];

	[self setValue:path forKey:@"alarmSoundPath"];
	
	NSSound *sound = [[[NSSound alloc] initWithContentsOfFile:path byReference:NO] autorelease];
	[sound play];
	
	[self didChangeValueForKey:@"alarmSoundName" ];

}

-(void)chooseSound:(id)sender
{
	NSOpenPanel* aPanel = (NSOpenPanel*)[NSOpenPanel openPanel]; 
	
	[aPanel setPrompt:
		
		NSLocalizedStringFromTableInBundle( @"Choose aiff file",nil,[NSBundle bundleForClass:[self class]],@"")
		
		];
	[aPanel setCanChooseFiles:YES];
	[aPanel setCanChooseDirectories:NO];
	[aPanel setCanCreateDirectories:NO];
	//[aPanel setRequiredFileType:@"aiff"];
	
	
	if( [aPanel runModalForDirectory:nil file:nil types:nil] == NSFileHandlingPanelOKButton )
	{ 
		
		//TITLE BINDING
		
		/*
		 NSDictionary* observerInfo = [self infoForBinding:@"title"];
		 id observer = [observerInfo objectForKey:@"NSObservedObject"];
		 if( observer != nil ) [observer setValue:[aPanel filename] forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];
		 */
		NSString* path = [aPanel filename];
		
		[self willChangeValueForKey:@"alarmSoundName" ];

		[self setValue:path forKey:@"alarmSoundPath" ];
		[self didChangeValueForKey:@"alarmSoundName" ];

		return ;
	} 
	
}

-(NSString*)alarmSoundName
{
	// Beep
	if( [alarmSoundPath isEqualToString: SIMPLE_BEEP_SOUND_PATH] )
	{
		return NSLocalizedString(SIMPLE_BEEP_SOUND_PATH,@"");
		
	}else 	if( [alarmSoundPath isEqualToString: NONE_SOUND_PATH] )
	{
		return NSLocalizedString(NONE_SOUND_PATH,@"");
	}
	
	else 
	{
		return [[alarmSoundPath lastPathComponent] stringByDeletingPathExtension];
		
	}
}



@end
