//
//  MNTabWindow.m
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//


#include <ApplicationServices/ApplicationServices.h>
#import "MNTabWindow.h"
#import "TabDocumentController.h"
#import "ScreenUtil.h"
#import "CGSPrivate.h"
#import "AppController.h"
#import "NSString (extension).h"
#import "SoundManager.h"
#import "DragButton.h"
#import "InstallLibrary.h"
#import <RegistrationManager/RegistrationManager.h>

#define MARGIN 5


#define TITLE_ATTRIBUTE [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12],NSFontAttributeName , NULL ]



extern BOOL registered;
extern BOOL expired;


@implementation MNTabWindow




- (id)initWithContentRect:(NSRect)contentRect 
				styleMask:(unsigned int)styleMask 
				  backing:(NSBackingStoreType)backingType 
					defer:(BOOL)flag{
//
	//NSLog(@"initWithContentRect");
	
	
	tabOpenStatus = tab_closed;
	canBecomeMainWindow = NO;
	tabPosition = tab_top;
	transparencyWhenClosed = 1.0;
	transparencyWhenOpened = 1.0;
	windowLevel = NSNormalWindowLevel;
	windowLevelOpened = NSNormalWindowLevel;
	closeTimer = nil;
	customizingFridge = nil;
	screenID = screenIDWhereMouseIs();
	isVisible = YES;

	NSArray* array = [NSScreen screens];
	NSRect biggestRect = NSZeroRect;
	unsigned int i, count = [array count];
	for (i = 0; i < count; i++) {
		NSScreen * aScreen = [array objectAtIndex:i];
		biggestRect = NSUnionRect( biggestRect,[aScreen frame]);
	}
	
	
//
	if( contentRect.size.width < 50 ) contentRect.size.width = 50;
	if( contentRect.size.height < 50 ) contentRect.size.height = 50;

//
	
	
	phantomOpenFrame = NSZeroRect;
	phantom_actualOpenFrame = NSZeroRect;
	
    self = [super initWithContentRect: contentRect
							styleMask: /*NSBorderlessWindowMask |*/ NSNonactivatingPanelMask
							  backing:NSBackingStoreBuffered defer:YES];	
    if (self) {
		
		
		//[NSBundle loadNibNamed:@"MNTabWindow"  owner:self];
		
		
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
		
		[self setLevel:   preferenceIntValueForKey(@"windowLevel")   ]; //NSModalPanelWindowLevel];
		[self setBackgroundColor:[NSColor clearColor]];
		[self setOpaque:NO];
		
		transparencyWhenClosed = 0.7;
		transparencyWhenOpened = 0.9;

		[self setPreference:nil];
		

		
		[self setDelegate:self];
		//[self setReleasedWhenClosed:YES];
		//**** 個々が原因？ ***
		//[self setReleasedWhenClosed:NO];

		
	/// remove and add

		//////
		tabOpenStatus = tab_closed;
	
		
		
		canBecomeMainWindow = NO;
		
		screenID =  screenIDWhereRectIs(contentRect);
		
		
		[self setHideWhenExpose:YES];
		
		
		
		NSMenu* colorMenu = [[ThemeManager sharedManager] colorMenu];
		int hoge;
		for( hoge = 0; hoge < [colorMenu numberOfItems]; hoge++ )
		{
			NSMenuItem* mi = [[(NSMenuItem*)[colorMenu itemAtIndex:hoge] copy] autorelease];
			
			[mi setAction: @selector(menuItemSelected:)];
			[customMenu insertItem: mi  atIndex: 3+hoge ];
		}
		
		
		
		

		
		
		////pref notification
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(setPreference:)
													 name:PreferenceDidChangeNotification
												   object:nil ];
		

		
		//[self setupBadge];
		
		//[self startUsing];
		
		
		
		
		/*
		// add expose window
		
		NSWindow* window = [[NSWindow alloc] initWithContentRect:[self frame]
													   styleMask:NSTitledWindowMask
														 backing:NSBackingStoreBuffered 
														   defer:NO];
		
		[window setLevel:kCGMinimumWindowLevel];
		
		[self addChildWindow:window ordered:NSOrderedAscending];
		 */

		
		
		
		/* Leopard */
			[self setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];
	//	[self retain];
	}
    return self;
}

-(void)awakeFromNib
{
	[ownerDocument retain]; 
}

- (void)performClose:(id)sender
{
	//[self close];
	[ownerDocument close];
	//[self release];
	
}


-(void)setPreference:(NSNotification*)notification
{

	[dragButton_top setPreference];
	[dragButton_bottom  setPreference];
	[dragButton_left  setPreference];
	[dragButton_right  setPreference];
	
	
	transparencyWhenClosed = preferenceFloatValueForKey(@"sliderClosed") / 100.0;
	transparencyWhenOpened = preferenceFloatValueForKey(@"sliderOpened") / 100.0;
	
	animationResizeTime = 0.2 /  preferenceFloatValueForKey(@"openCloseSpeed");



	////NSLog(@"%f,%f",transparencyWhenClosed,transparencyWhenOpened);
	
	if(  tabOpenStatus == tab_opened  )
		[self setAlphaValue:transparencyWhenOpened];
	
	else
		[self setAlphaValue:transparencyWhenClosed];
	
	windowLevel = preferenceIntValueForKey(@"windowLevel");
	windowLevelOpened = preferenceIntValueForKey(@"windowLevelOpened");
	
	if(  tabOpenStatus == tab_opened  )
		[self setLevel:windowLevelOpened];
	else
		[self setLevel:windowLevel];
	
	

	
	
	[self flushWindow];
}






-(NSRect)tabBoundsInScreen
{
	NSRect _tabRect = NSZeroRect;
	
	if( tabPosition == tab_top )
	{
		//NSRect _fr = [dragButton_top frame];
		//_tabPoint =  NSMakePoint( NSMaxX(_fr) , NSMaxY(_fr)  );
		
		_tabRect = [dragButton_top frame];
		
	}
	else if( tabPosition == tab_bottom )
	{
		//_tabPoint = [dragButton_bottom frame].origin;
		
		_tabRect = [dragButton_bottom frame];
		
		
	}
	else if( tabPosition == tab_left )
	{
		//_tabPoint = [dragButton_left frame].origin;
		
		_tabRect = [dragButton_left frame];
		
	}
	else if( tabPosition == tab_right )
	{
		//NSRect _fr = [dragButton_right frame];
		//_tabPoint =  NSMakePoint( NSMaxX(_fr) , NSMaxY(_fr) );
		
		
		_tabRect = [dragButton_right frame];
		
	}
	
	// convert coordinate system of _tabPoint 
	//_tabPoint = [self convertBaseToScreen:_tabPoint];
	_tabRect.origin = [self convertBaseToScreen:_tabRect.origin];
	
	return _tabRect;
}


-(NSScreen*)screenWhereTabIs
{
	/*
	NSScreen* screen = [ScreenUtil  screenWhereRectIs: [self tabBoundsInScreen] ];
	
	return screen;*/
	
	//screenID
	
	NSArray* screens =  [NSScreen screens];
	
	return [screens objectAtIndex:screenID];
	
	
}


-(int)edgeThatThisWindowBelongsTo /// return unique number depending on screen edge
// return value = screenNumber * 0x10 + tabPosition
{
	/*
	NSScreen* screen = [self screen ];
	NSArray* sa = [NSScreen screens];

	
	int screenID = [sa indexOfObject:screen ];
	*/
	////NSLog(@"edgeThatThisWindowBelongsTo %d", screenID*0x10 + tabPosition);
	
	return screenID*0x10 + tabPosition;
	
}
-(void)setScreenID:(int)idNum
{
	screenID = idNum;
}
-(int)screenID
{
	return screenID;	
}

-(NSRect)solveOffScreen //  タブが画面外にいるのを解決
{
	NSRect tabFrame = [self tabBoundsInScreen];
	
	NSArray* sa = [NSScreen screens];
	
	
	int closestWindowIdx = 0;
	float distanceFromTabToScreenCenter = INFINITY;
	NSPoint tabCenter = NSMakePoint(tabFrame.origin.x + tabFrame.size.width/2,
									tabFrame.origin.y + tabFrame.size.height/2);
	
	int hoge;
	BOOL flag = NO;
	for( hoge = 0; hoge < [sa count]; hoge++ )
	{
		NSScreen* screen = [sa objectAtIndex:hoge];
		
		NSRect screenFrame = [screen frame];
		
		if( NSIntersectsRect(screenFrame, tabFrame) )
		{
			flag = YES;
			break;
		}
		
		//find closest window

		NSPoint screenCenter =  NSMakePoint(screenFrame.origin.x + screenFrame.size.width/2,
											screenFrame.origin.y + screenFrame.size.height/2);
		
		float _distance = (screenCenter.x - tabCenter.x)*(screenCenter.x - tabCenter.x)
			+ (screenCenter.y - tabCenter.y)*(screenCenter.y - tabCenter.y);
		
		if( _distance < distanceFromTabToScreenCenter )
		{
			distanceFromTabToScreenCenter = _distance;
			closestWindowIdx = hoge;
		}
		
	}
	
	if( flag == NO ) // completely out of screen
	{
		NSScreen* closestScreen = [sa objectAtIndex:screenID];
		NSRect closestScreenFrame = [closestScreen frame];

		NSPoint newTabFrameOrigin = NSZeroPoint;
		
		
		if( tabPosition == tab_bottom )
		{
			float mid = closestScreenFrame.origin.x + closestScreenFrame.size.width /2;
			
			float mbarMargin = 0.0;
			if( closestScreen == MENUBAR_SCREEN )
				mbarMargin = MENUBAR_HEIGHT;
				
			if( tabCenter.x < mid ) //hidari awase
			{
				 newTabFrameOrigin = NSMakePoint( closestScreenFrame.origin.x, 
														 NSMaxY(closestScreenFrame)- TAB_HEIGHT -mbarMargin);
			

				
			}else  // migi awase
			{
				 newTabFrameOrigin = NSMakePoint( NSMaxX(closestScreenFrame)-tabFrame.size.width,
														 NSMaxY(closestScreenFrame)- TAB_HEIGHT -mbarMargin);

				
			}
			
			
		}
		
		else if( tabPosition == tab_top )
		{
			
			float mid = closestScreenFrame.origin.x + closestScreenFrame.size.width /2;
			
			if( tabCenter.x < mid ) //hidari awase
			{
				 newTabFrameOrigin = NSMakePoint( closestScreenFrame.origin.x, 
														 closestScreenFrame.origin.y );

				
				
			}else  // migi awase
			{
				 newTabFrameOrigin = NSMakePoint( NSMaxX(closestScreenFrame)-tabFrame.size.width,
														 closestScreenFrame.origin.y );

				
			}
		}
		else if( tabPosition == tab_left )
		{
			
			float mid = closestScreenFrame.origin.y + closestScreenFrame.size.height /2;
			
			if( tabCenter.y < mid ) //shita awase
			{
				newTabFrameOrigin = NSMakePoint( NSMaxX(closestScreenFrame)-TAB_HEIGHT, 
														 closestScreenFrame.origin.y );

				
				
			}else  // ue awase
			{
				newTabFrameOrigin = NSMakePoint( NSMaxX(closestScreenFrame)-TAB_HEIGHT,
														 NSMaxY(closestScreenFrame)- tabFrame.size.height );

				
			}
			
		}
		else if( tabPosition == tab_right )
		{
			float mid = closestScreenFrame.origin.y + closestScreenFrame.size.height /2;
			
			if( tabCenter.y < mid ) //shita awase
			{
				newTabFrameOrigin = NSMakePoint( closestScreenFrame.origin.x, 
														 closestScreenFrame.origin.y );

				
				
			}else  // ue awase
			{
				newTabFrameOrigin = NSMakePoint(  closestScreenFrame.origin.x,
														 NSMaxY(closestScreenFrame)- tabFrame.size.height );
			}

		}
		
		float difX = newTabFrameOrigin.x - tabFrame.origin.x;
		float difY = newTabFrameOrigin.y - tabFrame.origin.y;
		
		NSRect newFrame = [self frame];
		newFrame.origin.x += difX;
		newFrame.origin.y += difY;
		
		return newFrame;
		//[self setFrameOrigin:newWindowOrigin];
	}
	return [self frame];
}

#pragma mark -


-(TAB_POSITION)tabPosition
{
	return tabPosition;
}
-(BOOL)isOpened
{
	return (tabOpenStatus == tab_opened ? YES :NO );
}
-(BOOL)isTornOff
{
	return (tabOpenStatus == tab_tornOff ? YES : NO);	
}
-(void)setTornOff:(BOOL)flag
{
	if( flag == YES )	[self tearOffWithoutMovement];
	else	[self closeTab];
}

-(NSRect)hideFrame
{
	
	
	NSScreen* screen = [self screenWhereTabIs ];
	
	
	NSRect screenRect = [screen frame];
	int menubar_margin = 0;
	
	if( screen == MENUBAR_SCREEN )
		menubar_margin = MENUBAR_HEIGHT;
	
	NSRect closedFrame = NSZeroRect;
	NSRect winRect = [self frame];
	
	if( ! NSEqualRects(phantomOpenFrame, NSZeroRect) && NSEqualRects(phantom_actualOpenFrame,winRect) )
		winRect = phantomOpenFrame;
	
	
	if( tabPosition == tab_bottom )
	{
		closedFrame = NSMakeRect(winRect.origin.x, NSMaxY(screenRect) -menubar_margin - 2,
								 winRect.size.width, winRect.size.height	);
		
		
	}
	
	else if( tabPosition == tab_top )
	{
		closedFrame = NSMakeRect(winRect.origin.x, screenRect.origin.y - winRect.size.height + 2,
								 winRect.size.width, winRect.size.height	);
		
		
	}
	else if( tabPosition == tab_left )
	{
		closedFrame = NSMakeRect( NSMaxX(screenRect) - 2, winRect.origin.y, 
								  winRect.size.width, winRect.size.height	);
		
		
	}
	else if( tabPosition == tab_right )
	{
		closedFrame = NSMakeRect(  screenRect.origin.x - winRect.size.width + 2, winRect.origin.y,
								   winRect.size.width, winRect.size.height	);
		
	}
	
	return closedFrame;
}


-(NSRect)closedFrame
{
	
	
	NSScreen* screen;
	screen = [self screenWhereTabIs ];	
	
	
	
	
	NSRect screenRect = [screen frame];
	int menubar_margin = 0;
	
	if( screen == MENUBAR_SCREEN )
		menubar_margin = MENUBAR_HEIGHT;
	
	NSRect closedFrame = NSZeroRect;
	NSRect winRect = [self frame];
	
	if( ! NSEqualRects(phantomOpenFrame, NSZeroRect) && NSEqualRects(phantom_actualOpenFrame,winRect) )
		winRect = phantomOpenFrame;
	
	
	if( tabPosition == tab_bottom )
	{
		closedFrame = NSMakeRect(winRect.origin.x, NSMaxY(screenRect) -menubar_margin - TAB_HEIGHT,
								 winRect.size.width, winRect.size.height	);
		
		
	}
	
	else if( tabPosition == tab_top )
	{
		closedFrame = NSMakeRect(winRect.origin.x, screenRect.origin.y - winRect.size.height + TAB_HEIGHT,
								 winRect.size.width, winRect.size.height	);
		
		
	}
	else if( tabPosition == tab_left )
	{
		closedFrame = NSMakeRect( NSMaxX(screenRect) - TAB_HEIGHT, winRect.origin.y, 
								  winRect.size.width, winRect.size.height	);
		
		
	}
	else if( tabPosition == tab_right )
	{
		closedFrame = NSMakeRect(  screenRect.origin.x - winRect.size.width + TAB_HEIGHT, winRect.origin.y,
								   winRect.size.width, winRect.size.height	);
		
	}
	
	return closedFrame;
}




-(void)windowDidClosed
{
	////NSLog(@"windowDidClosed");
	
	[mainContent setHidden:YES];

	
	[dragButton_top mouseExited:NULL];
	[dragButton_bottom mouseExited:NULL];
	[dragButton_left mouseExited:NULL];
	[dragButton_right mouseExited:NULL];
	
	
	[self setAlphaValue:transparencyWhenClosed];
	
	[self redrawWithShadowProperly];


	
	//save
	if( preferenceBoolValueForKey(@"saveEdgyFilesEachTime") ) 
		[ownerDocument saveIfNeeded];
	
	tabOpenStatus = tab_closed;

}


-(void)removeObserver
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:TabWindowDidOpenNotification
												  object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:NSWindowDidResignKeyNotification
												  object:nil];	
}






-(void)showMenu
{	
	if( tabOpenStatus == tab_tornOff )
		[[customMenu itemWithTag:1] setState:NSOnState];

	else
		[[customMenu itemWithTag:1] setState:NSOffState];

	
	NSEvent* ev = [NSEvent mouseEventWithType:NSRightMouseDown location:[self mouseLocationOutsideOfEventStream] modifierFlags:0 timestamp:0 windowNumber:[self windowNumber] context:NULL eventNumber:0 clickCount:1 pressure:1.0];
	
	
	
	[NSMenu popUpContextMenu:customMenu withEvent:ev forView:mainContent];
}

-(IBAction)menuItemSelected:(id)sender
{
	int tag = [sender tag];
	
	
	
	if( tag == 0 )
	{
		[ownerDocument showProperty];
		return;
		
	}else if( tag == 1 )
	{
		if( [sender state] == NSOnState )
		{
			tabOpenStatus = tab_closed;
			[sender setState:NSOffState];
			
			[self closeTab];
		}
		else
		{
			[self openTab];
			tabOpenStatus = tab_tornOff;
			[sender setState:NSOnState];

			[self tearOffWithMovement];
		}
		
		
	
	}else if( tag == 7 )
	{

		[ownerDocument print:self];
		
		
		
	}else if( tag == 8 )
	{
		
	}else if( tag == 9 )
	{
		[self performClose:self];
	}else if( tag == 10 )
	{
		[self tryToPerform:@selector(newDocument:) with:NULL];
		
	}else if( tag == 11 )
	{

	}else if( tag == 12 )
	{
		//export
		[ownerDocument export];

	}else if( tag == 20 )
	{
		// set memo color and size as default
		// tabPosition
		// [mainContent themeColor]
		// [self frame]
		
		[self setAsDefaultSize:(id)sender];

		
		
	}else if( tag == 30 )
	{
		

	}
	else if( tag == 40 ) 
	{
		//Revert 
		[[TabDocumentController sharedTabDocumentController] revert: [ownerDocument filePath]
																doc: ownerDocument ];
		
	}else if( tag >= 1000 && tag < 1999 )
	{
		//color
		
		NSArray* colorTable = [[ThemeManager sharedManager] colorTable];
		
		tag = tag - 1000;
		
		NSColor* color = [[colorTable objectAtIndex:tag] objectForKey:@"color"];
		
		[ownerDocument setDocumentColor:color];		
		
	}else if( tag == 1999 )
	{
		NSMatrix* matrix = [[[NSColorPanel sharedColorPanel] accessoryView] viewWithTag:100];
		
		if( [[matrix  cellWithTag:2] state] != NSOnState ) 
		{
			[[matrix  cellWithTag:0] setState:NSOffState];
			[[matrix  cellWithTag:1] setState:NSOffState];
			
			[[matrix  cellWithTag:2] setState:NSOnState];
		}
		
		[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
		[[NSApplication sharedApplication] orderFrontColorPanel:self];
		[self makeKeyWindow];
		[self makeFirstResponder: [ownerDocument textView]];

	}
	
	else if( tag >= 999 )
	{
	//	[self debug];
	}
	
}
-(IBAction)colorMenuSelected:(id)sender
{
	int tag = [sender tag];
	
	if( tag >= 1000 && tag < 1999 )
	{
		//color
		
		NSArray* colorTable = [[ThemeManager sharedManager] colorTable];
		
		tag = tag - 1000;
		
		NSColor* color = [[colorTable objectAtIndex:tag] objectForKey:@"color"];
		
		[ownerDocument setDocumentColor:color];		
		
	}else if( tag == 1999 )
	{
		NSMatrix* matrix = [[[NSColorPanel sharedColorPanel] accessoryView] viewWithTag:100];
		
		if( [[matrix  cellWithTag:2] state] != NSOnState ) 
		{
			[[matrix  cellWithTag:0] setState:NSOffState];
			[[matrix  cellWithTag:1] setState:NSOffState];
			
			[[matrix  cellWithTag:2] setState:NSOnState];
		}
		
		[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
		[[NSApplication sharedApplication] orderFrontColorPanel:self];
		[self makeKeyWindow];
		[self makeFirstResponder: [ownerDocument textView]];
		
	}
	
	
}


-(IBAction)showProperty:(id)sender
{
	[ownerDocument showProperty];
	
}

-(IBAction)setAsDefaultSize:(id)sender
{
	setPreferenceValueForKey( [NSNumber numberWithInt:tabPosition],@"userDefaultTabPosition");
	setPreferenceValueForKey(NSStringFromRect([self frame]),@"userDefaultFrame");
	syncronizePreferences();
}

- (void) dealloc {
	//NSLog(@"window dealloc");
	[super dealloc];
}

-(void)close
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	


	[dragButton_top endUsing];
	[dragButton_bottom endUsing];
	[dragButton_left endUsing];
	[dragButton_right endUsing];
	
	if( [closeTimer isValid] ){
		[closeTimer invalidate];
		[closeTimer release];
		closeTimer = nil;
	}

	
	//[customMenu release];
	
		
	// [sheet release];
		
	
	if(  viewAnimation != nil && [viewAnimation isAnimating] )
		[viewAnimation stopAnimation];
	

	[viewAnimation setOwner:nil];
	[viewAnimation setEndSelecor:nil];

	[viewAnimation release];
	viewAnimation = nil;
	
	
	[self setDelegate:nil];
	[ownerDocument release];
	ownerDocument = nil;

	[super close];
	
}




-(void)setNormalLevel
{
//	[self orderFront:self]; 
	
	if( tabOpenStatus == tab_closed  )
		[self setLevel:windowLevel];
	else
		[self setLevel:windowLevelOpened];
	
	if( tabOpenStatus != tab_tornOff )
		[self setHideWhenExpose:YES];
}

-(void)setFloatingLevel
{
	[self setLevel:NSStatusWindowLevel+2];

}




-(void)setHiddenLevel
{
	if( tabOpenStatus == tab_opened  )
	{
		[self setLevel:windowLevelOpened];
	}
	else
	{
		//[self orderOut:self]; 
		[self setLevel:kCGDesktopWindowLevel ];
	}
	
	if( tabOpenStatus != tab_tornOff )
		[self setHideWhenExpose:YES];
	
}

-(void)setTemporarilyTornOff:(BOOL)flag
{
	if( flag == YES )
	{
		if( tabOpenStatus == tab_closed )
			[mainContent setHidden:NO];
		
		tabOpenStatus |= tab_temporarilyTornOff;
		
		
	} else
	{
		tabOpenStatus &= 0x0f;
		
		if( tabOpenStatus == tab_closed )
			[mainContent setHidden:YES];

		
		[self reposition];
	
	}
}

-(void)reposition
{
	
	NSRect frame = [self frameForProposedFrame: [self frame]];
	[self setFrameOrigin: frame.origin ];
	[self redrawWithShadowProperly];
	
	//windowFrame = convertRectToStickOnEdgeWithEdgeMargin(windowFrame,screenWhereMouseIs(),
	//															 region,margin);
				
				
				
}

-(NSRect)frameForProposedFrame:(NSRect)frame
{
	

	if( tabOpenStatus == tab_tornOff )//|| !(tabOpenStatus & tab_temporarilyTornOff)  )
		return frame;
	
	
	
	SCREEN_REGION region;
	int margin;

	
	
	if( tabPosition == tab_top )
	{
		region = bottomArea;
		if( [self isOpened] == YES ) margin =  - TAB_HEIGHT;
		else	margin = - (frame.size.height - TAB_HEIGHT ) ;
	
	}
	
	else 	if( tabPosition == tab_bottom )
	{
		region = topArea;
		
		if( [self isOpened] == YES )	margin = -TAB_HEIGHT;
		else								margin = - (frame.size.height - TAB_HEIGHT );
		
		
	}else	if( tabPosition == tab_left )
	{
		region = rightArea;
		
		if( [self isOpened] == YES )	margin = - TAB_HEIGHT;
		else								margin = - (frame.size.width - TAB_HEIGHT );
		
	}else	if( tabPosition == tab_right )
	{
		region = leftArea;
		
		if( [self isOpened] == YES )	margin = - TAB_HEIGHT;
		else								margin = - (frame.size.width - TAB_HEIGHT );
		
	}
	
	
	
	// âÊñ Ç…Ç≠Ç¡Ç¬Ç≠
	////NSLog(@"%@",NSStringFromRect(frame));
	NSRect windowFrame = convertRectToStickOnEdgeWithEdgeMargin(frame,screenWithScreenID(screenID),
														 region,margin);
	////NSLog(@"%@",NSStringFromRect(windowFrame));

	return windowFrame;
	
}

-(void)setTabPosition:(TAB_POSITION)pos
{
	if( pos < 0 || pos > 3 ) pos = 1;
	
	tabPosition = pos;
	[self setupTabs];
	
}

-(void)setupTabs
{
	[dragButton_top			setTabPosition: tab_top];
	[dragButton_bottom		setTabPosition: tab_bottom];
	[dragButton_left		setTabPosition: tab_left];
	[dragButton_right		setTabPosition: tab_right];
	
	
	[dragButton_top			setHidden:YES];
	[dragButton_bottom		setHidden:YES];
	[dragButton_left		setHidden:YES];
	[dragButton_right		setHidden:YES];
	
	
	if( tabPosition == tab_bottom )
		[dragButton_bottom		setHidden:NO];
	else if( tabPosition == tab_top )
		[dragButton_top		setHidden:NO];
	else if( tabPosition == tab_left )
		[dragButton_left		setHidden:NO];
	else if( tabPosition == tab_right )
		[dragButton_right		setHidden:NO];
	
	
	
	
	[self redrawWithShadowProperly];
	
	
}
-(TAB_OPEN_STATUS)tabOpenStatus
{
	return tabOpenStatus;
}
-(void)setTabStatus:(TAB_OPEN_STATUS)status
{
	tabOpenStatus = status;
	
	if( tabOpenStatus == tab_closed )
	{
		[mainContent setHidden:YES];
		
	}else
	{
		[mainContent setHidden:NO];
		
	}
	
	[self redrawWithShadowProperly];
}

#pragma mark Accessor

-(id)ownerDocument
{
	return ownerDocument;
}


-(NSImage*)balloonImage
{
	//get balloon image
	
	NSColor* _color = [[ownerDocument textView] backgroundColor];
	[[ownerDocument textView] setBackgroundColor:[NSColor clearColor]];
	
	id cv = [[ownerDocument textView] superview];
	
	NSImage* balloonImage = [[[NSImage alloc] initWithSize:[cv frame].size ] autorelease];
	
	[balloonImage setFlipped:YES];
	[balloonImage lockFocus];
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: 0 yBy: -[cv bounds].origin.y ]; 
	[affine concat];	
	
	[[ownerDocument textView] drawRect:[cv bounds]];
	
	[affine translateXBy: 0 yBy: +[cv bounds].origin.y ]; 
	
	
	[affine concat];
	
	
	[balloonImage unlockFocus];
	
	[[ownerDocument textView] setBackgroundColor:_color];	
	
	
	return balloonImage;
}


#pragma mark UI
- (void)setFrameUsingAnimation:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation endSelector:(SEL)endSel  canStopPreviousAnimation:(BOOL)stopAnimation duration:(NSTimeInterval)duration
{
	
	if( performAnimation == NO )
	{
		[super setFrame:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation];
		if( endSel != nil )
			[self performSelector:endSel];
		return;
	}
	
	//MNTabWindowViewAnimation
	if(  viewAnimation != nil && [viewAnimation isAnimating] )
	{
		if( stopAnimation == NO )
			return;
		
		[viewAnimation stopAnimation];
		
	}
	
	
	[viewAnimation release];
	
	
	////NSLog(@"duratin %f",[self animationResizeTime:windowFrame]);
	viewAnimation = [[MNTabWindowViewAnimation alloc] initWithDuration:duration
														animationCurve:NSAnimationEaseInOut];
	
	[viewAnimation setAnimationBlockingMode: NSAnimationNonblocking];
	[viewAnimation setFrameRate:0.0];
	[viewAnimation setAnimationCurve: NSAnimationEaseInOut];
	
	[viewAnimation setStartFrame:[self frame]];
	[viewAnimation setEndFrame:windowFrame ];
	[viewAnimation setOwner:self ];
	
	
	[viewAnimation setEndSelecor:endSel ];

	
	[viewAnimation startAnimation];
	
	//[super setFrame:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation];
	
	
}
- (void)setFrameUsingAnimation:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation canStopPreviousAnimation:(BOOL)flag  duration:(NSTimeInterval)duration
{

	[self setFrameUsingAnimation:(NSRect)windowFrame 
						 display:(BOOL)displayViews 
						 animate:(BOOL)performAnimation 
					 endSelector:nil 
		canStopPreviousAnimation:flag  
						duration:(NSTimeInterval)duration];
}

- (NSTimeInterval)animationResizeTime:(NSRect)newWindowFrame
{

	return animationResizeTime; 
}




- (NSSize)minSize
{
	NSSize size = {50,50};
	/*
	 if( tabPosition == tab_bottom || tabPosition == tab_top )
	 size.width = [dragButton_top frame].size.width;
	 
	 else if( tabPosition == tab_right || tabPosition == tab_left )
	 size.width = [dragButton_top frame].size.height;
	 */
	return size;
}





typedef int CGSConnectionID;
typedef int CGSWindowID;

- (void) setStickyWhenExpose
{
	BOOL flag = YES;
	
	CGSConnectionID cid;
	CGSWindowID wid;
	
	wid = [self windowNumber];
	cid = _CGSDefaultConnection();
	CGSWindowTag tags[2] = { 0, 0 };
	
	if (!CGSGetWindowTags(cid, wid, tags, 32)) {
		if (flag) {
			tags[0] = tags[0] | 0x00000800;
		} else {
			tags[0] = tags[0] & ~0x00000800;
		}
		CGSSetWindowTags(cid, wid, tags, 32);
	}
}



- (void) setHideWhenExpose:(BOOL)flag
{
	//[self setStickyWhenExpose];
	//return;
	
	
	CGSConnectionID cid;
	CGSWindowID wid;
	
	wid = [self windowNumber];
	cid = _CGSDefaultConnection();
	CGSWindowTag tags[2] = { 0, 0 };
	
	if (!CGSGetWindowTags(cid, wid, tags, 32)) {
		if (flag) {
			tags[0] = tags[0] | 0x00000002;
		} else {
			tags[0] = tags[0] & ~0x00000002;
		}
		CGSSetWindowTags(cid, wid, tags, 32);
	}
	
	
//	[[[self childWindows] lastObject] setLevel:kCGMinimumWindowLevel];
}




 
 
 -(void)changeColor:(id)sender
 {
	 
	 
	 
	 NSMatrix* matrix = [[[NSColorPanel sharedColorPanel] accessoryView] viewWithTag:100];
	 
	 if( [[matrix  cellWithTag:2] state] != NSOnState )  return;
	 
	 
	 if( ownerDocument != NULL )
		 [ownerDocument setDocumentColor : [sender color]];
	 
 }

-(NSString*)title
{
	return [ownerDocument p_title];
}
 
 -(void)redrawWithShadowProperly
 {
	if( ![self isVisible] ) return;

	[self flushWindow];
	 
	[self setHasShadow:NO];
	 
	[self display];
	 
	 [self flushWindow];
	 
	[self setHasShadow:YES];
	 
	 [self display];	

	 
	 
	 //[self alignmentOption2];
 }

 
-(BOOL)mouseOnWindow
{

	return NSPointInRect( [NSEvent mouseLocation] ,[self frame]);
}

-(BOOL)mouseOnTab
{
	if( tabPosition == tab_bottom )
		return [dragButton_bottom mouseOnButton];
	else if( tabPosition == tab_top )
		return [dragButton_top	mouseOnButton];
	else if( tabPosition == tab_left )
		return [dragButton_left mouseOnButton];
	else if( tabPosition == tab_right )
		return [dragButton_right mouseOnButton];
	
	return NO;
}


-(NSButton*)tabButton
{
	if( tabPosition == tab_bottom )
		return dragButton_bottom;
	else if( tabPosition == tab_top )
		return dragButton_top;
	else if( tabPosition == tab_left )
		return dragButton_left;
	else if( tabPosition == tab_right )
		return dragButton_right;
	
	return nil;
}


 
///////
#pragma mark Key & Main

- (BOOL)canBecomeKeyWindow
{
	return YES;
}

- (BOOL)canBecomeMainWindow
{
	
	if( tabOpenStatus != tab_tornOff )
		[self setHideWhenExpose:YES];
	return canBecomeMainWindow;
}

- (void)setCanBecomeMainWindow:(BOOL)flag
{
	canBecomeMainWindow = flag;
}

- (void)resignKeyWindow
{
	
	//	if( tabOpenStatus == 0 && [self attachedSheet] == nil)
	//		[self closeTab];
	
	[super resignKeyWindow];
	
//	if( [ownerDocument isHidden] )
//		[self orderOut:self];
}

#pragma mark Actions
-(void)openTab
{
	[self openTabBlocking:NO];
}

-(void)openTabBlocking:(BOOL)flag
{
	//debug
	
	////NSLog(@"id %d", [self edgeThatThisWindowBelongsTo]);
	////NSLog(@"siblings %d of %d",[[APP_CONTROLLER  siblingsFor:ownerDocument] indexOfObject:ownerDocument]+1, [[APP_CONTROLLER  siblingsFor:ownerDocument] count]);
	
	////NSLog(@"openTab");
	
	
	if( ownerDocument == nil ) return;
	
	
	if( tabOpenStatus != tab_closed ) return;
	
	
	
	
	[[TabDocumentController sharedTabDocumentController] setBusy:YES];
	
	if( ! [self isVisible] )
		[self orderFront:self];
	
	[self setLevel:windowLevelOpened];
	
	
	[self setAlphaValue:transparencyWhenOpened];
	
	
	BOOL animate =   preferenceBoolValueForKey(@"animateOpenClose")  ;
	
	//	[subContent setHidden:YES];	
	[mainContent setHidden:NO];
	//	[self setHasShadow:YES];
	[self display];
	
	//NSRect screenRect = [[self screen] frame];
	
	
	
	
	NSScreen* screen = [self screenWhereTabIs ];
	
	NSRect screenRect = [screen frame];
	
	int menubar_margin = 0;
	
	if( screen == MENUBAR_SCREEN )
		menubar_margin = MENUBAR_HEIGHT;
	
	
	NSRect winRect = [self frame];
	NSRect newRect = NSZeroRect;
	
	if( tabPosition == tab_bottom )
		newRect = NSMakeRect(winRect.origin.x, NSMaxY(screenRect) - winRect.size.height - menubar_margin + TAB_HEIGHT,
							 winRect.size.width, winRect.size.height	);
	
	
	
	
	else if( tabPosition == tab_top )
		newRect = NSMakeRect( winRect.origin.x,    screenRect.origin.y - TAB_HEIGHT ,
							  winRect.size.width, winRect.size.height	);
	
	
	else if( tabPosition == tab_left )
		newRect = NSMakeRect( NSMaxX(screenRect) - winRect.size.width + TAB_HEIGHT, winRect.origin.y,
							  winRect.size.width, winRect.size.height	);
	
	
	else if( tabPosition == tab_right )
		newRect = NSMakeRect( screenRect.origin.x - TAB_HEIGHT, winRect.origin.y, 
							  winRect.size.width, winRect.size.height	);
	
	
	
	// create phantom rect if necessay
	phantomOpenFrame = NSZeroRect;
	phantomOpenFrame = newRect;
	
	
	newRect = convertRectNotToStickOutFromScreenWithEdgeMargin(newRect,screen,-20);
	
	
	
	
	phantom_actualOpenFrame = newRect;
	
	if( NSEqualRects(newRect ,phantomOpenFrame) )
		phantomOpenFrame = NSZeroRect;
	
	
	
	
	// move window
	
	////NSLog(@"open tab frame %@",NSStringFromRect(newRect));
	
	//[self adjustWindowPosition];
	
				
	
	
	
	
	
	[self setHasShadow:NO];
	[self display];
	[self setHasShadow:YES];
	[self display];
	[self flushWindow];
	[self resetCursorRects];	
	
	
	
	
	[[SoundManager sharedSoundManager] playSoundWithName:@"soundOpen"];
	
	
	
	
	tabOpenStatus = tab_opened;

	
	
	if( flag == NO )
	{
		//
		//- (void)setFrameUsingAnimation:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation endSelector:(SEL)endSel  canStopPreviousAnimation:(BOOL)stopAnimation duration:(NSTimeInterval)duration

		

		[self setFrameUsingAnimation:newRect  display:NO animate:animate 
						 endSelector:@selector(redrawWithShadowProperly)
			canStopPreviousAnimation:YES duration:animationResizeTime];
		
		
		
	}
	else
	{
		
		[self setFrame:newRect  display:NO animate:animate ];
		


		
	}
	
	
	if(   preferenceBoolValueForKey(@"closeAutomatically")    )
	{
		
		//post notification
		
		[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowDidOpenNotification
															object:self];
		
		// notification observer
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(closeTabCalledByNotification:)
													 name:TabWindowDidOpenNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(closeTabIfOtherAppFrontmost:)
													 name:NSWindowDidResignKeyNotification
												   object:nil];
		
	}
	
	
}




-(void)closeTab
{
	[self closeTabBlocking:NO];
	
	
	if( !registered )
	{
		registered = [[RegistrationManager sharedRegistrationManager] registered] ;
		if( !registered )
		{
				
			if( expired )
			{
				demoCount ++;
				
				if( demoCount >= 2 || demoCount < 0)
				{

					[[RegistrationManager sharedRegistrationManager] showDemoDialogue];
					demoCount = 0;
				}
			}
		}
	}	
}


-(void)closeTabBlocking:(BOOL)flag
{
	
	
	if( ownerDocument == nil ) return;
	
	if( tabOpenStatus == tab_closed ) return;
	
	//[self aboutInputtingTitleIfSo];
	
	
	if( [self attachedSheet] )
		return;
	
	
	//check if inputting kanji
	
	if( ! [[ownerDocument textView] resignFirstResponder] )  
	{
		return;
	}	
	
	
	
	NSMatrix* matrix = [[[NSColorPanel sharedColorPanel] accessoryView] viewWithTag:100];
	
	int version = [AppController OSVersion];
	
	
	if( version >= 105 )
		
		
	{
		[[matrix cellWithTag:0] setState:NSOnState];
		[[matrix cellWithTag:1] setState:NSOffState];
		[[matrix cellWithTag:2] setState:NSOffState];
	}
	
	
	//	//NSLog(@"title change %d %d",[PREF titleUntitled], [PREF titleWord])
	// set title
	if(    preferenceIntValueForKey(@"titleUntitled")    == 2 )
		if(    preferenceBoolValueForKey(@"updateTitle")   == YES || [[self title] isEqualToString: NSLocalizedString(@"Untitled", @"") ] ) 
		{
			NSTextView* tv = [ownerDocument textView];
			
			
			NSString* _str = [[[tv textStorage] string] firstWords:   preferenceIntValueForKey(@"titleWord")    ];
			
			if( ! [_str isEqualToString: [self title] ] ) // change title
			{
				
				int alignOpt =   preferenceIntValueForKey(@"alignment")    ;// change alignment temporarily in order not to align when changing the title 
				
				setPreferenceValueForKey([NSNumber numberWithInt:0],@"alignment") ;   
				syncronizePreferences();

				if( [_str length] > 1 )
				{
					[[self ownerDocument] setValue:_str forKey:@"p_title"];
										
				}
				setPreferenceValueForKey([NSNumber numberWithInt:alignOpt],@"alignment") ;   
				syncronizePreferences();

			}
			
		}
			
			
			//
			
			
			[self setLevel:windowLevel];
	
	
	BOOL animate =   preferenceBoolValueForKey(@"animateOpenClose");
	
	
	[self makeFirstResponder:NULL];
	[self setCanBecomeMainWindow:NO];
	
	
	
	
	
	
	[[SoundManager sharedSoundManager] playSoundWithName:@"soundClose"];
	
	

	
	
	
	NSRect closedFrame = [self closedFrame];
	
	if( flag == NO )
	{
		[self setFrameUsingAnimation:closedFrame
							 display:NO 
							 animate:animate 
						 endSelector:@selector(windowDidClosed) 
			canStopPreviousAnimation:YES
							duration:animationResizeTime];
		
	}
	else
	{
		[self setFrame:closedFrame
			   display:NO animate:animate ];
		
		[self windowDidClosed];
	}
	
	

	
	
	
	
	
				
	//	//NSLog(@"obs");
	//remove observer
	
	[self removeObserver];
	
	phantomOpenFrame = NSZeroRect;
	
	
	if( [ownerDocument isHidden] )
		[self orderOut:self];
	
	//align //dragbuttonでもやってるけど、tearoffから復帰したときにこれが有効
	if( preferenceIntValueForKey(@"alignment") == 2 )
	[self align:self];
	
}



-(void)tearOffWithoutMovement
{
	[self removeObserver];
	
	
	[self setLevel:windowLevelOpened];
	[self setAlphaValue:transparencyWhenOpened];

	
	
	
	[mainContent setHidden:NO];

	[self alignIgnoringMe:self]; // align 

	
	
	if( tabOpenStatus == tab_tornOff ) return;
	else tabOpenStatus = tab_tornOff;
	
	
	
	
	[[SoundManager sharedSoundManager] playSoundWithName:@"soundTearOff"];

	
	
	
	//-(void)redrawWithShadowProperly
	{
		
		
		[self flushWindow];
		
		[self setHasShadow:NO];
		
		[self display];
		
		[self flushWindow];
		
		[self setHasShadow:YES];
		
		[self display];	
		

	}	
	
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	[self orderFront:self];
	
	[self setHideWhenExpose:NO];
	
}


-(void)tearOffWithMovement
{
	[self removeObserver];

	
	[self setLevel:windowLevelOpened];
	[self setAlphaValue:transparencyWhenOpened];
	

	[mainContent setHidden:NO];

	
	
	
		[[SoundManager sharedSoundManager] playSoundWithName:@"soundTearOff"];

	
	
	
	

	NSRect winRect = [self frame];
	
	int MAR = 10;
	
	if( tabPosition == tab_bottom )
	{
		
		[self setFrame:NSMakeRect(winRect.origin.x, winRect.origin.y -MAR,
								  winRect.size.width, winRect.size.height	)
			   display:YES animate:YES];
	}
	
	else if( tabPosition == tab_top )
	{
		
		[self setFrame:NSMakeRect(winRect.origin.x,  winRect.origin.y +MAR,
								  winRect.size.width, winRect.size.height	)
			   display:YES animate:YES];
		
	}
	else if( tabPosition == tab_left )
	{
		
		[self setFrame:NSMakeRect( winRect.origin.x -MAR, winRect.origin.y, 
								   winRect.size.width, winRect.size.height	)
			   display:YES animate:YES];
		
	}
	else if( tabPosition == tab_right )
	{
		
		[self setFrame:NSMakeRect(  winRect.origin.x + MAR, winRect.origin.y,
									winRect.size.width, winRect.size.height	)
			   display:YES animate:YES];
	}
	
				
	[self alignIgnoringMe:self]; // align 

	
	tabOpenStatus = tab_tornOff;
	
	[self setHasShadow:YES];
	[self display];
	[self resetCursorRects];	
	
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	[self orderFront:self];

	[self setHideWhenExpose:NO];

}


-(IBAction)tearOff:(id)sender
{
	
	
	[self openTab];
	[self alignIgnoringMe:self]; // align 
	
	
	tabOpenStatus = tab_tornOff;
	
	[self tearOffWithMovement];	
}



-(void)toggleTab
{
	
	
	if( tabOpenStatus == tab_opened  ||  tabOpenStatus == tab_tornOff )	[self closeTab];
	else				[self openTab];
	
}


-(IBAction)align:(id)sender
{
	//NSLog(@"align - tabOpenStatus %d",tabOpenStatus);
	
	/*
	 NSPoint origin = [self frame].origin;
	 
	 origin.x = 4000;
	 origin.y =  4000;
	 [self setFrameOrigin:origin];
	 //$$$#####
	 //sample 
	 return;
	 */
	
	if( tabOpenStatus == tab_tornOff ) return;
	
	int idx = [[[TabDocumentController sharedTabDocumentController] siblingsFor:ownerDocument] indexOfObject:ownerDocument] ;
	
	////NSLog(@"align %d , %d",[self edgeThatThisWindowBelongsTo] , idx);
	
	//NSLog(@"align - idx %d",idx);

	
	[[TabDocumentController sharedTabDocumentController] alignTabOnEdge:[self edgeThatThisWindowBelongsTo]
																	 to: idx option:0];
	
	
}


-(IBAction)alignIgnoringMe:(id)sender
{
	/*
	 NSPoint origin = [self frame].origin;
	 
	 origin.x = 4000;
	 origin.y =  4000;
	 [self setFrameOrigin:origin];
	 //$$$#####
	 //sample 
	 return;
	 */
	
	if( tabOpenStatus == tab_tornOff ) return;
	if( preferenceIntValueForKey(@"alignment") == 0 ) return;

	
	int idx = [[[TabDocumentController sharedTabDocumentController] siblingsFor:ownerDocument] indexOfObject:ownerDocument] ;
	
	////NSLog(@"align %d , %d",[self edgeThatThisWindowBelongsTo] , idx);
	
	
	
	[[TabDocumentController sharedTabDocumentController] alignTabOnEdge:[self edgeThatThisWindowBelongsTo]
																	 to: idx option:1];
	
	
}



-(void)closeTimer
{
	
	////NSLog(@"close timer ? tabOpenStatus %d ,  %d",tabOpenStatus,[self canBecomeMainWindow]);
 
	NSPoint mouseLoc = [NSEvent mouseLocation];
	if( ! NSPointInRect( mouseLoc , [self frame] ) && 
		! [self canBecomeMainWindow] &&  
		tabOpenStatus == tab_opened &&
		![viewAnimation isAnimating] )
	{
		[self closeTab];
	}
	
	else if( tabOpenStatus == tab_opened && ! [self canBecomeMainWindow] )
	{
		////NSLog(@"timer");
		[closeTimer release];
		closeTimer = 		
			[NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(closeTimer) userInfo:NULL repeats:NO];
		[closeTimer retain];
	
	}
}
-(void)closeTabIfOtherAppFrontmost:(NSNotification*)notif
{
	////NSLog(@"closeTabIfOtherAppFrontmost");
	
	
	NSWindow* appsKeywindow = [[NSApplication sharedApplication] keyWindow];	
	if( appsKeywindow == nil && ! [[notif object] isSheet] )
		[self closeTab];
	
	else if( self == appsKeywindow && self == [notif object])
		[self closeTab];
}
-(void)closeTabCalledByNotification:(NSNotification*)notif
{
	if( [notif object] != self && ![[notif object] isSheet] )
		[self closeTab];
}


-(void)repositionToMouse
{
	
	//(1) calc tab center relative position
	NSPoint targetPoint = NSZeroPoint;
	NSRect frame = [self frame];
	int margin;
	if( tabPosition == tab_top )
	{
		targetPoint.x = frame.size.width /2;
		targetPoint.y = frame.size.height - 10;
		
		if( tabOpenStatus == tab_opened )
			margin = -20;
		else
			margin = - (frame.size.height - 20 );
	}
	
	
	else 	if( tabPosition == tab_bottom )
	{
		targetPoint.x = frame.size.width /2;
		targetPoint.y = 10;
		
		if(  tabOpenStatus == tab_opened  )
			margin = -20;
		else
			margin = - (frame.size.height - 20 );
		
		
	}else	if( tabPosition == tab_left )
	{
		targetPoint.x = 10;
		targetPoint.y = frame.size.height /2;
		
		if(  tabOpenStatus == tab_opened  )
			margin = -20;
		else
			margin = - (frame.size.width - 20 );
		
	}else	if( tabPosition == tab_right )
	{
		targetPoint.x = frame.size.width -10;
		targetPoint.y = frame.size.height /2;
		
		if(  tabOpenStatus == tab_opened  )
			margin = -20;
		else
			margin = - (frame.size.height - 20 );
		
	}
	
	
	//
	frame = convertRectWithPointToMouse(targetPoint,frame);
	
	
	/*
	 frame = [ScreenUtil rect:frame
		 sticksOnEdgeOfScreen:[ScreenUtil screenWhereMouseIs]
						 side:[ScreenUtil regionWhereMouseIs]
				   edgeMargin:margin ];
	 */
	
	[self setFrame:frame display:NO];
	//[self setFrame:frame display:YES animate:YES];
	
	
	/*
	 
	 
	 NSPoint mouseLoc = [NSEvent mouseLocation];
	 
	 NSRect winRect = [self frame];
	 if( tabPosition == tab_bottom ||  tabPosition == tab_top )
	 {
		 winRect.origin.x = mouseLoc.x - winRect.size.width /2;
		 
		 
	 }else 	if( tabPosition == tab_right || tabPosition == tab_left )
	 {
		 winRect.origin.y = mouseLoc.y - winRect.size.height /2;
		 
	 }
	 
	 [self setFrame:winRect display:NO];
	 */
	
}

#pragma mark Override

-(IBAction)orderOut:(id)sender
{

	[self setAlphaValue:0.0];
	
	isVisible = NO;

}
-(IBAction)makeKeyAndOrderFront:(id)sender
{
	if( [super isVisible] )
	{
		if( tabOpenStatus == tab_closed )
			[self setAlphaValue:transparencyWhenClosed];
		else
			[self setAlphaValue:transparencyWhenOpened];
		
		
		isVisible = YES;
	}else
	{
		[super orderFront:self];
		isVisible = YES;
	}
	
	[super makeKeyWindow];
}


-(IBAction)orderFront:(id)sender
{
	if( [super isVisible] )
	{
		if( tabOpenStatus == tab_closed )
			[self setAlphaValue:transparencyWhenClosed];
		else
			[self setAlphaValue:transparencyWhenOpened];


		
		isVisible = YES;
	}else
	{
		[super orderFront:self];
		isVisible = YES;
	}
}
-(BOOL)isVisible
{
	return isVisible;
}

-(BOOL)isOutOfScreen
{
	NSRect frame = [self frame];
	
	
	return isVisible;
}



@end
