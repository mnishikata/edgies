//
//  SearchAgent.h
//  SpotInside
//
//  Created by __Name__ on 06/11/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SearchAgent : NSObject {
	//MDQueryRef _query;
	id endTarget;
	SEL endSelector;
	NSArray* keysToRetrieve;
	MDQueryRef _query;
	
	NSLock* lock;
	BOOL cancel;
	
	int searchCount;
}
- (id)initWithEndTarget:(id)target andSelector:(SEL)selector keys:(NSArray*)keys;
-(void)end;
-(int)searchCount;
-(void)dealloc;
-(NSString*)escape:(NSString*)str;
-(void)search:(NSString*)query format:(NSString*)format scope:(NSArray*)scope cancelPreviousSearch:(BOOL)cancelFlag;
-(void)finish:(NSNotification*)notif;
-(void)finishFinished:(NSMutableArray*)findResult;
-(NSMutableArray*)processFoundData:(NSNotification*)notif;
-(NSMutableDictionary*)extractAttributesFrom:(MDItemRef)ref;
-(void)stopSearch;

@end
