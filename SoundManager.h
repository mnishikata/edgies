/* SoundManager */

#import <Cocoa/Cocoa.h>

@interface SoundManager : NSObject
{
	NSMutableDictionary* soundFiles;
}
+(SoundManager*)sharedSoundManager;
- (id) init ;
-(void)dealloc;
-(void)setupSounds;
-(void)playSoundWithName:(NSString*)soundName;
-(void)clearSounds;

@end
