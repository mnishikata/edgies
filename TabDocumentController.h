/* TabDocumentController */

#import <Cocoa/Cocoa.h>
#import "MiniDocument.h"

@interface TabDocumentController : NSObject
{
	NSMutableArray* documents;
		int				untitledCount;
	MiniDocument*	currentDocument;

	NSTimer*		autoHideTimer;
	
	BOOL			isBusy;

	BOOL	autoHide;
	double sliderHideDelay;
	
	NSMutableDictionary* changedDocouments;
	NSMutableArray* deletedDocouments;

	
	//Revert
	IBOutlet id revertTextView;
	IBOutlet id revertButton;
	IBOutlet id revertPopup;
	IBOutlet id revertWindow;
	
	NSMutableDictionary* revertableDocs;
}

+(TabDocumentController*)sharedTabDocumentController;
- (id) init ;
- (NSResponder *)nextResponder;
-(void)awakeFromNib;
-(void)dealloc;
-(void)saveDocumentsToPersistentStoreBeforeQuitting;
-(void)readDocumentsFromPersistentStore;
-(void)preferenceChanged;
-(MiniDocument*)actionForcusedDocument;
-(MiniDocument*)currentDocument;
-(void)setCurrentDocument:(MiniDocument*)object;
-(NSMutableArray*)documents;
-(NSWindow*)mouseOnVisibleButton;
-(NSArray*)visibleDocuments;
-(NSArray*)hiddenDocuments;
-(BOOL)isBusy;
-(void)setBusy:(BOOL)flag;
-(MiniDocument*)newDocumentForDropping;
-(IBAction)newDocument:(id)sender;
-(IBAction)openAll:(id)sender;
-(IBAction)closeAll:(id)sender;
-(int)untitledCount;
-(void)insertHeaderIn:(MiniDocument*)doc;
-(void)destroy:(MiniDocument*)doc;
-(void)saveThisData:(NSData*)data atPath:(NSString*)path defaultName:(NSString*)name document:(MiniDocument*)doc;
-(void)saveThisData:(NSData*)data atPath:(NSString*)path defaultName:(NSString*)name document:(MiniDocument*)doc sync:(BOOL)sync backup:(BOOL)backupFlag backupCount:(int)backupCount;
-(NSDictionary*)recordsToSync;
-(void)addChangedRecord:(MiniDocument*)doc;
-(NSDictionary*)changedRecordsToSync;
-(void)addDeletedRecord:(MiniDocument*)doc;
-(NSArray*)deletedRecordsToSync;
-(MiniDocument*)miniDocumentWithName:(NSString*)name;
-(BOOL)deleteRecordWithName:(NSString*)name;
-(void)clearAllChanges;
-(NSArray*)documentsAtEdge:(int)edgeID;
-(NSArray*)siblingsFor:(MiniDocument*)doc;
-(NSArray*)rightSiblingsFor:(MiniDocument*)doc;
-(NSArray*)leftSiblingsFor:(MiniDocument*)doc;
-(MiniDocument*)documentToRight:(MiniDocument*)doc;
-(MiniDocument*)documentToLeft:(MiniDocument*)doc;
-(MiniDocument*)findClosestTo:(NSRect)baseRect inSiblings:(NSArray*)siblings ;
-(void)autoHide;
-(IBAction)arrangeToFront:(id)sender;
-(IBAction)arrangeToFrontDocuments:(NSArray*)documentsToMove;
-(IBAction)arrangeToBack:(id)sender;
-(IBAction)arrangeToBackDocuments:(NSArray*)documentsToMove;
-(IBAction)bringCenter:(id)sender;
-(void)alignTabOnEdge:(int)edgeID to:(int)centerIdx option:(int)option;
-(void)rearrangeTabsOnEdge:(int)edgeID fromIndex:(int)fromIdx toIndex:(int)toIdx  movingDocument:(MiniDocument*)doc originalRect:(NSRect)rect;
-(IBAction)navigate:(id)sender;
-(BOOL)writeDocumentData:(NSData*)data AtPath:(NSString*)path forDocument:(MiniDocument*)doc backup:(BOOL)backupFlag backupCount:(int)backupCount;

@end
