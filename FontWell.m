#import "FontWell.h"
#import "FontWellTitleTransformer.h"

@implementation FontWell
static NSMutableArray*  _fontWells = nil;






- (void) _pushed : (id) sender
{
	int		state = [self state];

	if (state == NSOnState) {
		[self activate];
	}
	if (state == NSOffState) {
		[self deactivate];
	}
}

- (void) awakeFromNib
{
	if (!_fontWells) {
		_fontWells = [[NSMutableArray array] retain];
	}
	if (![_fontWells containsObject : self]) {
		[_fontWells addObject : self];
	}

	[self setTarget : self];
	[self setAction : @selector(_pushed:)];
}

- (void) dealloc
{
	[super dealloc];
	[_fontWells removeObject : self];
}

- (void) activate
{
	NSFontManager	*fm_ = 	[NSFontManager sharedFontManager];

	[fm_ setSelectedFont : [self font] isMultiple : NO];
	[fm_ setDelegate : self];
    [fm_ orderFrontFontPanel : self];

	[[NSFontPanel sharedFontPanel] setDelegate : self];	// in order to catch the notification of closing font panel.

	NSEnumerator	*enumerator = [_fontWells objectEnumerator];
	FontWell		*fontWell;
	while (fontWell = [enumerator nextObject]) {
		if (fontWell != self) {
			[fontWell deactivate];
		}
	}
	
	[[self window] makeFirstResponder : self];	// in order to response changeFont: message by own.
}

- (void) deactivate
{
	[self setState : NSOffState];
	[[self window] makeFirstResponder : nil];
}

/*

-(void)setFontValue:(NSFont*)newFont
{
	NSFont* font = newFont;
	id fm = [NSFontManager sharedFontManager];
	NSFont* convertedFont = [fm convertFont : font];
	
	NSFont* resizedFont = [fm fontWithFamily : [convertedFont familyName] 
										  traits : [fm traitsOfFont : convertedFont] 
										  weight : [fm weightOfFont : convertedFont]
											size : [font pointSize]];
	
	//[[[self window] delegate] changeFontOf : [self tag]
	//									To : convertedFont];
	
	[self setFont : resizedFont];
	[self _updateTitleWithFont : convertedFont];
	

}*/

-(NSFont*)fontValue
{
	NSFont* font = [NSFont fontWithName:[[self font] fontName] size:_ps];
	
	if( font == nil )
		font = [NSFont userFontOfSize:0.0];
	
	return font;
}


#pragma mark NSFontManager Delegate



- (void) changeFont : (id) sender
{
	NSFont* font = [self font];
	NSFont* convertedFont = [sender convertFont : font];
/*
	NSFont* resizedFont = [sender fontWithFamily : [convertedFont familyName] 
										  traits : [sender traitsOfFont : convertedFont] 
										  weight : [sender weightOfFont : convertedFont]
											size : [convertedFont pointSize]];
*/
	//[[[self window] delegate] changeFontOf : [self tag]
	//									To : convertedFont];

	//[self setFont : convertedFont];
	

	
	
	NSString* name = [convertedFont fontName];
	float size = [convertedFont pointSize];
	
	//Observer

	
	//BINDING
	NSDictionary* observerInfo;
	id observer;
	
	observerInfo = [self infoForBinding:@"fontName"];
	observer = [observerInfo objectForKey:@"NSObservedObject"];
	if( observer != nil ) [observer setValue:name forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];

	observerInfo = [self infoForBinding:@"fontSize"];
	observer = [observerInfo objectForKey:@"NSObservedObject"];
	if( observer != nil ) [observer setValue:[NSNumber numberWithFloat:size] forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];
	
	observerInfo = [self infoForBinding:@"font"];
	observer = [observerInfo objectForKey:@"NSObservedObject"];
	if( observer != nil ) [observer setValue:convertedFont forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];
	
	
	//[self setValue:[convertedFont fontName] forKey:@"fontName"];
	//[self setValue:[NSNumber numberWithFloat: [convertedFont pointSize]] forKey:@"fontSize"];

}
-(void)setFont:(NSFont*)font
{

	
	// set title
	NSString* name = [font fontName];
	float size = [font pointSize];
	NSString* title = [NSString stringWithFormat:@"%@ %.1f",name,size]; 
	
	[self setTitle: title];
	
	
	[super setFont:font];
}
#pragma mark NSFontPanel (NSWindow) Delegate

- (void) windowWillClose : (NSNotification *) aNotification
{
	if ([self state] == NSOnState) [self deactivate];
}
@end
