//
//  MNTabWindowViewAnimation.m
//  sticktotheedge
//
//  Created by __Name__ on 06/10/24.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "MNTabWindowViewAnimation.h"
#import "MNTabWindow.h"
#import "CoreGraphicsServices.h"
#import "ScreenUtil.h"
@implementation MNTabWindowViewAnimation



- (id) init {
	self = [super init];
	if (self != nil) {
		ownerWindow = nil;
	}
	return self;
}

-(void)setStartFrame:(NSRect)rect
{
	startFrame = rect;	
}

-(void)setEndFrame:(NSRect)rect
{
	endFrame = rect;	
}

-(void)setOwner:(NSWindow*)window
{
	[ownerWindow release];
	ownerWindow = window;
	[ownerWindow retain];
}
-(void)setEndSelecor:(SEL)endSel
{
	endSelector = endSel;
}

-(void)setCurrentProgress:(NSAnimationProgress)progress
{
	if( ownerWindow == nil ) return;
	if( ![ownerWindow isVisible] ) return;
	
	
	
	
	////NSLog(@"%f",progress);
	
	if( progress != 1.0 )
	progress = sin( 3.14/2 * progress );
	////NSLog(@"%f %@ %@",progress, NSStringFromRect(endFrame), NSStringFromRect(startFrame));

	NSRect currentRect = NSZeroRect;
	
	float difX =	(  endFrame.origin.x -  startFrame.origin.x )*progress;
	currentRect.origin.x = startFrame.origin.x + difX;
	
	float difY =	(  endFrame.origin.y -  startFrame.origin.y )*progress;
	currentRect.origin.y = startFrame.origin.y + difY;
	
	
	
	float x = currentRect.origin.x;
	float y = currentRect.origin.y;
	
	

	
	// *** CONVERT COORDINATE ***
	y =  [MENUBAR_SCREEN frame].size.height -y - [ownerWindow frame].size.height;

	 

	CGPoint  aPoint = CGPointMake(x,y);
	
	CGSMoveWindow(_CGSDefaultConnection(), [ownerWindow windowNumber], &aPoint);
	
	
	
	
	//[ownerWindow setFrameOrigin: currentRect.origin];
	
	
	
	
	
	if( progress == 1.0 )
	{
		CGSFlushWindow(_CGSDefaultConnection(), [ownerWindow windowNumber], 0);

		[ownerWindow setFrameOrigin: currentRect.origin];

		
		//if tab is completely out of the screen,
		
		NSRect modifiedRect = [(MNTabWindow*)ownerWindow solveOffScreen];
		
		if( ! NSEqualRects( modifiedRect,[ownerWindow frame] ) )
			[ownerWindow setFrame:modifiedRect
						  display:NO animate:YES];
		
		
		/*
		NSRect tabRect = [(MNTabWindow*)ownerWindow tabBoundsInScreen];
		NSScreen* myScreen = [ownerWindow screen];
		
		if( myScreen == nil || ! NSIntersectsRect( [myScreen frame], tabRect) )
		{
			[ownerWindow setFrame:startFrame
						  display:NO animate:YES];
			
		}
		*/
		
		
		
		
		////NSLog(@"end");
		
		if( endSelector != nil  )
		{
			////NSLog(@"calling selector");

			 [ownerWindow performSelector:endSelector ];
		}
	}

	//[super setCurrentProgress:progress];
}

-(void)dealloc
{
	[ownerWindow release];
	ownerWindow = nil;
	endSelector = nil;
	
	[super dealloc];
}

@end
