#import <SyncServices/SyncServices.h>
#import "AppController.h"


#define SCRATCH_FOLDER [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/Documents/"]



@implementation AppController

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	ISyncManager *manager = [ISyncManager sharedManager];
	ISyncClient *syncClient;
	
	
	
// ******  register client ***** //
	

	
	NSString* schemaPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"EdgiesSyncSchema" ofType:@"syncschema"];
	NSString* schemaPlist = [[NSBundle bundleForClass:[self class]] pathForResource:@"EdgiesMemoClientDescription" ofType:@"plist"];
		
	schemaPath = [[[[schemaPlist stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
	schemaPath = [NSString stringWithFormat:@"%@/EdgiesSyncSchema.syncschema",schemaPath];

	
	NSLog(@"schemaPath %@",schemaPath);
	NSLog(@"schemaPlist %@",schemaPlist);
	
	
	
	
	// Register the schema.
	[manager registerSchemaWithBundlePath:schemaPath ];
	
	NSLog(@"register");
	
	
	// See if our client has already registered
	if (!(syncClient = [manager clientWithIdentifier:@"com.pukeko.edgies.sync.client"])) {
		// and if it hasn't, register the client.
		// We use the filename as part of the identifier, so that each file represents a separate sync client
		syncClient = [manager registerClientWithIdentifier:@"com.pukeko.edgies.sync.client" descriptionFilePath:schemaPlist];
	}
	
	
	NSLog(@"register finished");

	
	
	
	
	
	// ******  initialize client ***** //

	NSLog(@"initialize");

	ISyncSession *syncSession;
	
	// Open a session with the sync server
	syncSession = [ISyncSession beginSessionWithClient:syncClient
		 entityNames:[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]
	/* How long I'm willing to wait - pretty much forever */ beforeDate:[NSDate distantFuture]];
	
	// If we failed to get a sync session, we either timed out, or syncing isn't enabled.
	if (!syncSession) {
		if (![[ISyncManager sharedManager] isEnabled])
			NSLog(@"Syncing is currently disabled");
		else
			NSLog(@"Zen Sync timed out in beginSession");
		return ;
	}
	
	
	[syncClient setShouldReplaceClientRecords:YES forEntityNames:[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];
	
	
	
	/*
	// If we're refreshing our data, it's okay to overwrite previous client ids.
	if ( !_isRefresh ) {
		// Using our custom data, determine the highest record Id the server should know about
		NSNumber *highestServerId = (NSNumber *)[syncClient objectForKey:@"HighestId"];
		//   If we've got a higher recordId in our local document, update our info on the server
		if ([highestServerId intValue] < _highestLocalId) {
			[syncClient setObject:[NSNumber numberWithInt:_highestLocalId] forKey:@"HighestId"];
		} else {
			_highestLocalId = [highestServerId intValue];
		}
	}
	*/
	
	
	
	// Using the custom data, get the number of the last sync, so we can bump it and store it
	int serverLastSync = [(NSNumber *)[syncClient objectForKey:@"LastSyncNumber"] intValue];
	//_lastSyncNumber = (_lastSyncNumber > serverLastSync) ? _lastSyncNumber + 1 : serverLastSync + 1;
	NSLog(@"serverLastSync %d",serverLastSync);
	
	
	/*
	// If we've been asked to do a refresh sync, inform the server, and skip the push phase altogether
	if ( _isRefresh ) {
		[syncSession clientDidResetEntityNames:[NSArray arrayWithObject:ISyncEntity_Com_MyCompany_SyncExamples_Zensync_entity]];
	} else if ( ! _changes ) {
		// If there were no change lines (not even blanks), we're pushing everything (slow sync).
		// Tell the server our request, and let it determine if we can.
		[syncSession clientWantsToPushAllRecordsForEntityNames:
			[NSArray arrayWithObject:ISyncEntity_Com_MyCompany_SyncExamples_Zensync_entity]];
	}
	return syncSession;
	 */
	
	
	[syncSession clientWantsToPushAllRecordsForEntityNames:
		[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];
	
	
	
	
//*** push ***/
	NSLog(@"push");

	
	/// prepare data
	NSMutableDictionary* allRecords = [NSMutableDictionary dictionary];  // should be document_bank_all
	
	

	
	// Ask the server how it wants us to sync
	if ([syncSession shouldPushAllRecordsForEntityName:
		@"com.pukeko.edgies.sync.entity"]) 
	{
			
		NSArray* contents = [[NSFileManager defaultManager] subpathsAtPath:SCRATCH_FOLDER];
		int hoge;
		for( hoge = 0 ; hoge < [contents count]; hoge++ )
		{
			
			NSString* item = [contents objectAtIndex:hoge];
			
			if( [item hasSuffix:@".edgy"] )
			{
				
				NSString* itemPath = [SCRATCH_FOLDER stringByAppendingPathComponent:item];
				
				NSData* readDocData =[[[NSData alloc] initWithContentsOfFile:itemPath] autorelease];
				if( readDocData != nil )
				{
					id dic = [NSKeyedUnarchiver unarchiveObjectWithData:readDocData];
					NSString* name = [dic objectForKey:@"name"];
					
					
					NSDictionary* _aRecord = [NSDictionary dictionaryWithObjectsAndKeys:
						name, @"Name", 
						readDocData,@"Data",
						@"com.pukeko.edgies.sync.entity", ISyncRecordEntityNameKey, nil];

					
					NSLog(@"pushing all records ... %@",name);
					
					[syncSession pushChangesFromRecord:_aRecord
										withIdentifier:name];
					
					
				}
				
				
			}
		}
		

		
		

		
	} else if ([syncSession shouldPushChangesForEntityName:
		@"com.pukeko.edgies.sync.entity"]) 
	{
		// If the server would rather have us push up our changes, pull any changes from our change store, and push them
		//    Those that were specified with add / delete / modify, push as ISyncChange s
		//    All others, push as modified records

		/*
				[syncSession pushChange:[ISyncChange changeWithType:ISyncChangeTypeAdd
												   recordIdentifier:_ID HERE
															changes:_CHANGED RECORD HERE];
					
					or ISyncChangeTypeDelete  ISyncChangeTypeModify 				
		*/
		
		
		NSLog(@"pushin changed records ... do nothing here");

	}
	// If the server wouldn't let us push anything, do nothing
	
	
	
	
	
// *** pull *** 

	

	BOOL shouldPull = [syncSession shouldPullChangesForEntityName:@"com.pukeko.edgies.sync.entity"];
	
	NSLog(@"pull %d", shouldPull);

		
	// If we're allowed to pull changes, tell the engine to prepare them.  If not, we're still okay
	// (one example is when we're supposed to push the truth.  We won't be asked to pull changes in that case)
	if (shouldPull) 
	{
		BOOL serverResponse = [syncSession prepareToPullChangesForEntityNames:
			[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]
		  /* How long I'm willing to wait - pretty much forever */ beforeDate:[NSDate distantFuture]];
		
		// If the server tells us the mingling didn't go okay, bail out of this methood
		if ( ! serverResponse ) {
			[syncSession cancelSyncing];
			return ;
		}
		
		
		// Determine if the server will push everything
		if ([syncSession shouldReplaceAllRecordsOnClientForEntityName:
			@"com.pukeko.edgies.sync.entity"])
			
		{
			NSLog(@"shouldReplaceAllRecordsOnClientForEntityName");
			// And if so, whack our data
			[allRecords removeAllObjects];
		}
		
		
		// Enumerate over the available changes
		NSEnumerator *changeEnumerator = [syncSession changeEnumeratorForEntityNames:
			[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];
		
		ISyncChange *currentChange;
		
		// Apply each change to the data store, and report success or failure back to the sync server
		while (( currentChange = (ISyncChange *)[changeEnumerator nextObject] )) {
			switch ( [currentChange type] ) {
				case ISyncChangeTypeDelete:
				{
					NSLog(@"delete %@", [currentChange recordIdentifier] );
					
					[allRecords removeObjectForKey:[currentChange recordIdentifier]];
					[syncSession clientAcceptedChangesForRecordWithIdentifier:[currentChange recordIdentifier]
															  formattedRecord:nil
														  newRecordIdentifier:nil];
					break;
				}
					
					
				case ISyncChangeTypeAdd:
				{
					NSLog(@"add obj %@ ",[[currentChange record] description]);

					
					NSString *newRecordId = @"000x";
					[syncSession clientAcceptedChangesForRecordWithIdentifier:[currentChange recordIdentifier]
															  formattedRecord:nil
														  newRecordIdentifier:newRecordId];
					[allRecords setObject:[currentChange record] forKey:newRecordId];
					break;
				}
					
					
				case ISyncChangeTypeModify:
				{
					NSLog(@"modify obj %@  for key %@",[[currentChange record] description],[currentChange recordIdentifier]);
					[allRecords setObject:[currentChange record] forKey:[currentChange recordIdentifier]];
					[syncSession clientAcceptedChangesForRecordWithIdentifier:[currentChange recordIdentifier]
															  formattedRecord:nil
														  newRecordIdentifier:nil];
					break;
				}
			}
		}
		// If we were asked to pull something from the server, we got changes.
		// Once all have been applied, tell the server we're done modifying our data, for better or worse
		[syncSession clientCommittedAcceptedChanges];
	}	
	
	/*
	// Update our HighestId info (we might have incremented it while applying an add from the server)
	[syncClient setObject:[NSNumber numberWithInt:_highestLocalId] forKey:@"HighestId"];
	
	// Update our LastSyncNumber info, now that we know the sync was successful
	[syncClient setObject:[NSNumber numberWithInt:_lastSyncNumber] forKey:@"LastSyncNumber"];
	 
	 */
	
	// tell the sync server we're done with this session
	[syncSession finishSyncing];
	
	NSLog(@"return YES");

	return ;

	
}
	
/*
 load edgies
 
 
#define SCRATCH_FOLDER [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Edgies/Documents/"]

 
 NSArray* contents = [[NSFileManager defaultManager] subpathsAtPath:SCRATCH_FOLDER];
 int hoge;
 for( hoge = 0 ; hoge < [contents count]; hoge++ )
 {
	 
	 NSString* item = [contents objectAtIndex:hoge];
	 
	 if( [item hasSuffix:@".edgy"] )
	 {
		 
		 NSString* itemPath = [SCRATCH_FOLDER stringByAppendingPathComponent:item];
		 
		 NSData* readDocData =[[[NSData alloc] initWithContentsOfFile:itemPath] autorelease];
		 if( readDocData != nil )
		 {
			 id dic = [NSKeyedUnarchiver unarchiveObjectWithData:readDocData];
			 NSString* name = [dic objectForKey:@"name"];
			 

			 
			 
			 
		 }
		 
		 
	 }
 }
 

 
 
 */


@end
