//
//  main.m
//  gomisync
//
//  Created by Masatoshi Nishikata on 07/01/28.
//  Copyright __MyCompanyName__ 2007. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
