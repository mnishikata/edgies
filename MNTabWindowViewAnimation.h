//
//  MNTabWindowViewAnimation.h
//  sticktotheedge
//
//  Created by __Name__ on 06/10/24.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MNTabWindowViewAnimation : NSAnimation {

	NSRect startFrame;
	NSRect endFrame;
	NSWindow* ownerWindow;
	SEL endSelector;

}
-(void)setStartFrame:(NSRect)rect;
-(void)setEndFrame:(NSRect)rect;
-(void)setOwner:(NSWindow*)window;
-(void)setEndSelecor:(SEL)endSel;
-(void)setCurrentProgress:(NSAnimationProgress)progress;

@end
