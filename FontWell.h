/* FontWell */

#import <Cocoa/Cocoa.h>

/*
 
 BINDING:
 
 fontName
 fontSize
 
 
 
 */



@interface FontWell : NSButton
{
	float _ps;
}

- (void) _pushed : (id) sender;
- (void) awakeFromNib;
- (void) dealloc;
- (void) activate;
- (void) deactivate;
-(NSFont*)fontValue;
- (void) changeFont : (id) sender;
-(void)setFont:(NSFont*)font;
- (void) windowWillClose : (NSNotification *) aNotification;


@end
/*
@interface NSObject(FontWellDelegate)
// You should use item's tag to specify which Fontwell is the target.
- (NSFont *) getFontOf : (int) tagNum;
- (void) changeFontOf : (int) tagNum To: (NSFont *) newFont;
@end*/
