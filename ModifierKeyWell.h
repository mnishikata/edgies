/* ModifierKeyWell */

#import <Cocoa/Cocoa.h>

@interface ModifierKeyWell : NSButton
{
}
- (void) awakeFromNib;
- (void)_pushed:(NSEvent *)theEvent;
-(BOOL)isEqualToKeyModifier:(int)modKey;
-(BOOL)isPressed;
-(void)setTagAndTitleFromModKeysCombination:(int)modKeys;
-(void)setTitle:(NSString*)title;

@end
