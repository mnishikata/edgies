#import "TaggingInspector.h"
#import "ButtonAttachmentCell.h"
#import "ButtonAttachmentTextView.h"
#import "TagAttachmentCell.h"
#import "TagAttachmentTextView.h"

@implementation TaggingInspector

+(TaggingInspector*)sharedTaggingInspector
{
	return [[NSApp delegate] taggingInspector];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		activeCells = nil;

		[NSBundle loadNibNamed:@"TaggingInspector" owner:self];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(selectionChanged:)
													 name:NSTextViewDidChangeSelectionNotification
												   object:nil];
		[window setFloatingPanel:YES];

	}
	return self;
}
-(BOOL)multipleSelection
{
	if( [activeCells count] >1) return YES;
	return NO;
}
-(BOOL)noSelection
{
	if( [activeCells count] == 0 ) return YES;
	return NO;
}
-(void)showTaggingInspector
{
	[window orderFront:self];	
	[self updateInspector];
}
-(id)imageData
{
	if( [activeCells count] == 1) return [[activeCells objectAtIndex:0] valueForKey:@"imageData"];

	else return nil;
}
-(id)title
{
	if( [activeCells count] != 1) return @"Multiple Selection";

	return [[activeCells objectAtIndex:0] valueForKey:@"title"];
}
-(NSString*)targetPath
{
	if( [activeCells count] != 1) return @"";
	
	return [[activeCells objectAtIndex:0] valueForKey:@"targetPath"];
}
-(void)setTitle:(NSString*)title
{
	[activeCells setValue:title forKey:@"title"];
}
-(void)setTargetPath:(NSString*)path
{
	[activeCells setValue:path forKey:@"targetPath"];
	
}
-(id)font
{
	if( [activeCells count] == 0) return nil;

	return [[activeCells objectAtIndex:0] valueForKey:@"font"];
	
}
-(void)setFont:(NSFont*)font
{
	[activeCells setValue:font forKey:@"font"];
}

-(id)buttonSize
{
	if( [activeCells count] != 1) return nil;

	
	return 	 [[activeCells objectAtIndex:0] valueForKey:@"buttonSize"];

}
-(void)setButtonSize:(NSNumber*)size
{
	
	[activeCells setValue:size forKey:@"buttonSize"];
}
-(id)buttonPosition
{
	if( [activeCells count] != 1) return nil;

	return 	 [[activeCells objectAtIndex:0] valueForKey:@"buttonPosition"];
}
-(void)setButtonPosition:(NSNumber*)position
{
	[activeCells setValue:position forKey:@"buttonPosition"];
}
-(id)showButtonTitle
{
	if( [activeCells count] != 1) return nil;

	return  [[activeCells objectAtIndex:0] valueForKey:@"showButtonTitle"];
}
-(void)setShowButtonTitle:(id)showButtonTitle
{
	[activeCells setValue:showButtonTitle forKey:@"showButtonTitle"];
}



//		[self setBordered:showBezel];
-(id)showBezel
{
	if( [activeCells count] != 1) return nil;
	
	return  [[activeCells objectAtIndex:0] valueForKey:@"showBezel"];
}
-(void)setShowBezel:(id)showBezel
{
	[activeCells setValue:showBezel forKey:@"showBezel"];
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	
	[super dealloc];
}

-(BOOL)lock
{
	BOOL flag = NO;
	unsigned int i, count = [activeCells count];
	for (i = 0; i < count; i++) {
		NSObject * obj = [activeCells objectAtIndex:i];
		if( [[obj valueForKey:@"lock"] boolValue] == YES )
		{
			flag = YES;
			break;
		}
	}
	
	return  flag;
}
-(void)setLock:(BOOL)flag
{
	[activeCells setValue:[NSNumber numberWithBool:flag] forKey:@"lock"];

}

-(void)selectionChanged:(NSNotification*)notification
{
	if( ! [window isVisible] ) return;
	
	NSTextView* view = [notification object];
	if( view == nil ) return;

	if( ! [view conformsToProtocol:@protocol(TaggingAttachmentCellOwnerTextView)] ) return;

	
	[self activateForTextView:view];
	
	
	////NSLog(@"selectionChanged %@",[view className]);


}

-(void)updateInspector
{	  
	if( ! [window isVisible] ) return;

	[self willChangeValueForKey:@"title"];
	[self didChangeValueForKey:@"title"];
	
	[self willChangeValueForKey:@"font"];
	[self didChangeValueForKey:@"font"];
	
	[self willChangeValueForKey:@"buttonSize"];
	[self didChangeValueForKey:@"buttonSize"];
	
	[self willChangeValueForKey:@"buttonPosition"];
	[self didChangeValueForKey:@"buttonPosition"];
	
	[self willChangeValueForKey:@"imageData"];
	[self didChangeValueForKey:@"imageData"];
	
	[self willChangeValueForKey:@"showButtonTitle"];
	[self didChangeValueForKey:@"showButtonTitle"];

	[self willChangeValueForKey:@"showBezel"];
	[self didChangeValueForKey:@"showBezel"];
		
	[self willChangeValueForKey:@"targetPath"];
	[self didChangeValueForKey:@"targetPath"];	
	
	[self willChangeValueForKey:@"lock"];
	[self didChangeValueForKey:@"lock"];	
	
	[self willChangeValueForKey:@"noSelection"];
	[self didChangeValueForKey:@"noSelection"];	
	
	[self willChangeValueForKey:@"multipleSelection"];
	[self didChangeValueForKey:@"multipleSelection"];	

	
}



-(void)activateForTextView:(NSTextView  <TaggingAttachmentCellOwnerTextView> *)view
{
		
	if( [view conformsToProtocol:@protocol(TaggingAttachmentCellOwnerTextView)] )
	{
//		NSRange range = [view selectedRange];
		
		[activeCells release];
		activeCells = [[NSMutableArray arrayWithArray: [view taggingAttachmentCellArrayInSelectedRange]] retain];
		
			
		[self updateInspector];

		//[objectController setContent:sender];
		[self showTaggingInspector];
	}else
	{
		[activeCells release];
		activeCells = nil;
	}
}

-(IBAction)useAsDefault:(id)sender
{
	if( [activeCells count] > 0 )
		[[activeCells objectAtIndex:0] useAsDefault];
}


//

-(void)setupTagList:(TaggingAttachmentCell*)cell
{
	id obj = preferenceValueForKey(@"tagList");
	NSAttributedString* as=nil;
	if( obj ) as  = [NSKeyedUnarchiver unarchiveObjectWithData:obj];
	
	if( as )
	{
		[[textView textStorage] setAttributedString: as];
	}
	
	//NSLog(@"setting %@",[[cell tagTitles] description]);
	[textView selectTags:[cell tagTitles] addWhenNotInList:YES];
	
	
	[skipDialogue setState: ([cell skipDialogue]? NSOnState: NSOffState) ];
	[self setValue:[cell targetPath] forKey:@"fileLocationWellString"  ];

}

-(void)insertNewTaggingForTextView:(NSTextView*)textView
{

	
	NSAttributedString* attr = [TaggingAttachmentCell taggingAttachmentCellAttributedString];
	[textView insertText:attr];

	//NSDictionary* dict = [attr attributesAtIndex:0 longestEffectiveRange:nil inRange:NSMakeRange(0,1)];
	
	//[self modifyTagging: [[dict objectForKey:NSAttachmentAttributeName ] attachmentCell]];
	
}
-(IBAction)buttonClicked:(id)sender
{
	[textView didChangeText];


	[dialogue orderOut:self];
	[NSApp endSheet:dialogue returnCode:[sender tag]];
	
	
}
- (void)sheetDidEndForNewTagging:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{
	
	if( returnCode == 1 )
	{
					
	}
}
//
-(void)modifyTagging:(TaggingAttachmentCell*)cell
{
	
	if( [dialogue isVisible] ) return;

	
	[self setupTagList:cell];

	[activeCells release];
	activeCells = [NSArray arrayWithObject:cell];
	[activeCells retain];
	
	[cell retain];
	
	[dialogue setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	[NSApp beginSheet:dialogue modalForWindow:[[cell textView] window] modalDelegate:self
	   didEndSelector:@selector(sheetDidEndForModifingTagging:returnCode:contextInfo:) contextInfo:cell];
}

- (void)sheetDidEndForModifingTagging:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{
	
	if( returnCode == 1 )
	{
		
		//NSLog(@"sheetDidEndForModifingTagging");
		
		NSArray* selectedTagTitles = [TagAttachmentTextView selectedTagsInAtributedString:[textView textStorage]];
		//NSLog(@"selectedTagTitles %@",[selectedTagTitles description]);
		[(TaggingAttachmentCell*)contextInfo setTagTitles: selectedTagTitles];
		
		//[(AlarmAttachmentCell*)contextInfo setAlarmMode:alarmMode];
		
		//[[NSUserDefaults standardUserDefaults] setInteger:alarmMode forKey:@"AlarmAttachmentCell_alarmMode"];
		
		
		[(TaggingAttachmentCell*)contextInfo setSkipDialogue: ([skipDialogue state] == NSOnState? YES:NO ) ];
		[(TaggingAttachmentCell*)contextInfo setTargetPath: fileLocationWellString ];

		
		
		
	}
	
	[contextInfo release];
}




@end
