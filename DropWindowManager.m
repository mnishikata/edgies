#import "DropWindowManager.h"
#import "EdgiesLibrary.h"
#import "TabDocumentController.h"
#import "TabCursor.h"
#import "KeyModifierManager.h"
#import "InstallLibrary.h"
#import "AppController.h"

#define DROP_MARGIN 1
#define CORNER_MARGIN 10


@implementation DropWindowManager

+(DropWindowManager*)sharedDropWindowManager
{
	return [[NSApp delegate] dropWindowManager];
}
- (id) init {
	self = [super init];
	if (self != nil) {
		
		active = YES;

		callingTabTimer = nil;
		activateTimer = nil;
		
		[self deactivate];
			
		[self setupDropWindows];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWindows) name:NSApplicationDidChangeScreenParametersNotification object:[NSApplication sharedApplication]];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateWindows)
													 name:PreferenceDidChangeNotification
												   object:nil];	
	
		[self setupDropWindows];
		
		
		[NSTimer scheduledTimerWithTimeInterval:0.3 target:self
									   selector:@selector(timer) userInfo:nil repeats:YES];
		
	}
	return self;
}

-(void)timer
{

	
	
	
	if(  mouseIsOnTheEdges() && GetCurrentButtonState() == 0 )
	{
		
		
		
		if( edgeActivatingKeySwitch && !disableModifierToBringTabs )
			if( ! [MOD_PRESSED:@"edgeActivatingKeyModifier"] )
				return;//goto Skip;// not allowd

		
		if( lastEdgeEntering == 0 )
		{
			//enterd
			

			if(   alignOption   == 0 )
			{
				
				[self bringToFront];
				
			}else if( alignOption == 1 )
			{
				if( [callingTabTimer isValid] )
					[callingTabTimer invalidate];
				[callingTabTimer release];
				callingTabTimer = [NSTimer scheduledTimerWithTimeInterval: sliderClickDelay  / 1000.0
												 target:self selector:@selector(callTabs) userInfo:NULL repeats:NO];			
	
				[callingTabTimer retain];
			}else if(   alignOption   == 2 ) //delay
			{
				[self moveToBackmost];
				
				if( [callingTabTimer isValid] )
					[callingTabTimer invalidate];
				[callingTabTimer release];
				callingTabTimer = [NSTimer scheduledTimerWithTimeInterval:  sliderClickDelay  / 1000.0
												 target:self selector:@selector(callTabs) userInfo:NULL repeats:NO];			

				[callingTabTimer retain];
			}
				
			
			
			lastEdgeEntering = [NSDate timeIntervalSinceReferenceDate];

			
		}
		else
		{
			if( [NSDate timeIntervalSinceReferenceDate] > lastEdgeEntering + 10 )
				return;//goto Skip;
		}
		
		
	
		
					/*
		NSWindow* window = [[TabDocumentController sharedTabDocumentController] mouseOnVisibleButton];
		
		if( window == nil )
		{
			if( ! [activateTimer isValid] )
			{
				activateTimer = [NSTimer scheduledTimerWithTimeInterval:  sliderClickDelay   / 1000.0
											 target:self selector:@selector(activate) userInfo:NULL repeats:NO];
				[activateTimer retain];
			}
			
		}else
		{
			
			[self deactivate];

		}
	
		[NSTimer scheduledTimerWithTimeInterval:0.01 target:self
									   selector:@selector(timer) userInfo:nil repeats:NO];

			
		return;
					 */
	}else
	{
		/*
		
		if( active )
			[self deactivate];
		*/
		lastEdgeEntering = 0;	

	}
	
	/*
Skip:
	[NSTimer scheduledTimerWithTimeInterval:0.3 target:self
								   selector:@selector(timer) userInfo:nil repeats:NO];
	 */

}
-(void)callTabs
{
	[callingTabTimer release]; callingTabTimer = nil;

				
	if( ! mouseIsOnTheEdges()  ) return;

	
	[self bringToFront];

}

-(void)activate
{
//	activeTimerReserved = NO;

	[activateTimer release]; activateTimer = nil;
	
	if( ! mouseIsOnTheEdges()   ) return;
	//NSLog(@"Activating");

	if( active ) return;
	
	if( edgeActivatingKeySwitch  && !disableModifierToBringTabs )
		if( ! [MOD_PRESSED:@"edgeActivatingKeyModifier"] )
			return;// not allowd
	//NSLog(@"Activate");

	[self activateMain];
}
-(void)activateMain
{

	unsigned int i, count = [dropWindows count];
	for (i = 0; i < count; i++) {
		NSWindow * window = [dropWindows objectAtIndex:i];
		
		
		[window orderFront:self];
		[window setIgnoresMouseEvents:NO];
		
		NSRect frame = [window frame];
		[window setFrameOrigin:NSMakePoint(frame.origin.x-1, frame.origin.y)];
		[window setFrame:frame display:YES animate:YES];
		//[window setFrame:frame display:YES animate:YES];

		
	}
	active = YES;
	


	int screenID = screenIDWhereMouseIs();
	SCREEN_REGION region = regionIDAtMousePoint();
	
	if( screenID == 0 && edgeTop && region == topArea )
		[ TAB_CURSOR showTopCursor];
	
	if( screenID == 0 && edgeBottom && region == bottomArea )
		[ TAB_CURSOR showBottomCursor];

	if( screenID == 0 && edgeLeft && region == leftArea )
		[ TAB_CURSOR showLeftCursor];
	
	if( screenID == 0 && edgeRight && region == rightArea )
		[ TAB_CURSOR showRightCursor];
	
	
	
	if( screenID > 0 && edgeTopSecond && region == topArea )
		[ TAB_CURSOR showTopCursor];

	
	if( screenID > 0 && edgeBottomSecond && region == bottomArea )
		[ TAB_CURSOR showBottomCursor];

	if( screenID > 0 && edgeLeftSecond && region == leftArea )
		[ TAB_CURSOR showLeftCursor];

	if( screenID > 0 && edgeRightSecond && region == rightArea )
		[ TAB_CURSOR showRightCursor];

	
}
-(void)deactivate
{
	//NSLog(@"Deactivating");

	if( !active ) return;
	
	
	
	if( [callingTabTimer isValid] )
	{
		[callingTabTimer invalidate];
		[callingTabTimer release]; 
		callingTabTimer = nil;
	}
	
	if( [activateTimer isValid] )
	{
		[activateTimer invalidate];
		[activateTimer release]; 
		activateTimer = nil;
	}
				
				
	
	
	//NSLog(@"Deactivate");
	
//	int style = regionIDAtMousePoint();

	unsigned int i, count = [dropWindows count];
	for (i = 0; i < count; i++) {
		NSWindow * window = [dropWindows objectAtIndex:i];
		
		
		[window orderOut:self];
		[window setIgnoresMouseEvents:YES];
		
		

	}
	active = NO;

	[ TAB_CURSOR closeLeftCursor ];
	
	
	[ TAB_CURSOR closeRightCursor ];
	
	[ TAB_CURSOR closeBottomCursor];
	
	[ TAB_CURSOR closeTopCursor];
}
-(void)bringToFront
{
	NSArray* documentsToMove;
	
	if( preferenceBoolValueForKey(@"edgeEffectiveIndependently") )
	{
		screenIDWhereRectIs( );
		int edgeID = screenIDWhereMouseIs()*0x10  + regionIDAtMousePoint();
		
		documentsToMove = 
			[[TabDocumentController sharedTabDocumentController] documentsAtEdge:edgeID];
	}else
	{
		documentsToMove = 
		[[TabDocumentController sharedTabDocumentController] visibleDocuments];
	}
	
	[[TabDocumentController sharedTabDocumentController] arrangeToFrontDocuments:documentsToMove ];
	
	
}

-(void)moveToBackmost
{
	NSArray* documentsToMove;
	
	if( preferenceBoolValueForKey(@"edgeEffectiveIndependently") )
	{
		int edgeID = screenIDWhereMouseIs()*0x10  + regionIDAtMousePoint();
		
		documentsToMove = 
			[[TabDocumentController sharedTabDocumentController] documentsAtEdge:edgeID];
	}else
	{
		documentsToMove = 
		[[TabDocumentController sharedTabDocumentController] visibleDocuments];
	}
	
	
	
	[[TabDocumentController sharedTabDocumentController] arrangeToBackDocuments:documentsToMove];
	
}

-(void)dealloc
{
	[timer invalidate];
	[timer release];
	
	[callingTabTimer invalidate];
	[callingTabTimer release];
	
	[activateTimer invalidate];
	[activateTimer release];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
 	[super dealloc];
}


-(void)updateWindows
{
	int hoge;
	for( hoge = 0; hoge < [dropWindows count]; hoge++)
	{
		NSWindow* win = [dropWindows objectAtIndex:hoge];
		[win close];
		
	}
	[self setupDropWindows];
}

/*
-(void)preferenceChanged
{
	
	BOOL edgeEffect	= preferenceBoolValueForKey(@"edgeEffect");
	BOOL edgeTop	= preferenceBoolValueForKey(@"edgeTop");
	BOOL edgeBottom	= preferenceBoolValueForKey(@"edgeBottom");
	BOOL edgeRight	= preferenceBoolValueForKey(@"edgeRight");
	BOOL edgeLeft	= preferenceBoolValueForKey(@"edgeLeft");
	BOOL edgeTopSecond	= preferenceBoolValueForKey(@"edgeTopSecond");
	BOOL edgeBottomSecond	= preferenceBoolValueForKey(@"edgeBottomSecond");
	BOOL edgeRightSecond	= preferenceBoolValueForKey(@"edgeRightSecond");
	BOOL edgeLeftSecond	= preferenceBoolValueForKey(@"edgeLeftSecond");
	
	
	int piyo;
	for( piyo = 0 ; piyo < [dropWindows count]; piyo++)
	{
		NSPanel* _dropWindow = [dropWindows objectAtIndex:piyo];
		[_dropWindow makeKeyAndOrderFront:self];
		[_dropWindow flushWindow];
	}
	
	
	
	for( piyo = 0 ; piyo < [dropWindows count]; piyo++)
	{
		NSPanel* _dropWindow = [dropWindows objectAtIndex:piyo];
		
		if( [_dropWindow screen] ==   MENUBAR_SCREEN )
		{
			if( [[_dropWindow contentView] viewWithTag:tab_bottom] != nil )
				[[[_dropWindow contentView] viewWithTag:tab_bottom] setCreateNewMemo: edgeTop & edgeEffect ];
			
			if( [[_dropWindow contentView] viewWithTag:tab_top] != nil )
				
				[[[_dropWindow contentView] viewWithTag:tab_top] setCreateNewMemo: edgeBottom & edgeEffect ];
			
			if( [[_dropWindow contentView] viewWithTag:tab_left] != nil )
				
				[[[_dropWindow contentView] viewWithTag:tab_left] setCreateNewMemo: edgeRight & edgeEffect ];
			
			if( [[_dropWindow contentView] viewWithTag:tab_right] != nil )
				
				[[[_dropWindow contentView] viewWithTag:tab_right] setCreateNewMemo: edgeLeft & edgeEffect ];
			
		}else
		{
			
			if( [[_dropWindow contentView] viewWithTag:tab_bottom] != nil )
				[[[_dropWindow contentView] viewWithTag:tab_bottom] setCreateNewMemo: edgeTopSecond & edgeEffect ];
			
			if( [[_dropWindow contentView] viewWithTag:tab_top] != nil )
				
				[[[_dropWindow contentView] viewWithTag:tab_top] setCreateNewMemo: edgeBottomSecond & edgeEffect ];
			
			if( [[_dropWindow contentView] viewWithTag:tab_left] != nil )
				
				[[[_dropWindow contentView] viewWithTag:tab_left] setCreateNewMemo: edgeRightSecond & edgeEffect ];
			
			if( [[_dropWindow contentView] viewWithTag:tab_right] != nil )
				
				[[[_dropWindow contentView] viewWithTag:tab_right] setCreateNewMemo: edgeLeftSecond & edgeEffect ];
			
			
		}
		
	}	
}*/
-(NSPanel*)createDropWindowWithFrame:(NSRect)frame dropViewStyle:(int)style createNewMemo:(BOOL)createNewMemo screenID:(int)screenNum
{
	
	NSPanel* dropWindow  = [[NSPanel alloc]   initWithContentRect: frame
														styleMask:NSBorderlessWindowMask | NSNonactivatingPanelMask
														  backing:NSBackingStoreBuffered defer:NO];	

		[dropWindow setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];
	
	[dropWindow autorelease];
	
	DropView* dv = [[[DropView alloc] initWithFrame: NSMakeRect(0,0,frame.size.width,frame.size.height)] autorelease];
	[dv setAutoresizingMask: NSViewWidthSizable |  NSViewHeightSizable ];
	[[dropWindow contentView] addSubview:dv ];
	
	
	[dv setStyle:style];
	[dv setCreateNewMemo:createNewMemo];
	[dv setScreenID:screenNum];
	
	[dropWindow setLevel: NSStatusWindowLevel+1];

	[dropWindow setBackgroundColor:[NSColor clearColor]];
	[dropWindow setOpaque:NO];
	[dropWindow setHasShadow:NO];
	[dropWindow flushWindow];
	
	
	[dropWindow setFrame: frame  display:YES];
	
	[dv setHidden:YES];
	[dropWindow display];
	[dv setHidden:NO];
	[dropWindow display];
	
	[dropWindow orderFront:self];
	return dropWindow;
}

-(void)tile
{
	//NSLog(@"tile");
	
	[ TAB_CURSOR closeLeftCursor ];
	[ TAB_CURSOR closeRightCursor ];
	[ TAB_CURSOR closeBottomCursor];
	[ TAB_CURSOR closeTopCursor];
	
	
	unsigned int i, count = [dropWindows count];
	for (i = 0; i < count; i++) {
		NSWindow * window = [dropWindows objectAtIndex:i];
		DropView* view = [[[window contentView] subviews] objectAtIndex:0];
		int style = [view style];
		int screenID = [view screenID];

		NSRect frame = [[[NSScreen screens] objectAtIndex:screenID] frame];

		
		if( style == tab_top )
		{
			[window setFrame:NSMakeRect( frame.origin.x +CORNER_MARGIN, frame.origin.y,
										 frame.size.width -CORNER_MARGIN*2 ,DROP_MARGIN)
					 display:YES];
			
			
		}
		
		if( style == tab_bottom )
		{
			[window setFrame:NSMakeRect( frame.origin.x + CORNER_MARGIN, NSMaxY( frame ) -DROP_MARGIN,
										 frame.size.width -CORNER_MARGIN*2 ,DROP_MARGIN)
					 display:YES];
			
			
		}
		
		if( style == tab_left )
		{
			[window setFrame:NSMakeRect( NSMaxX(frame)-DROP_MARGIN, frame.origin.y + CORNER_MARGIN , 
										 DROP_MARGIN,frame.size.height -CORNER_MARGIN*2)
					 display:YES];
			
			
		}
		
		if( style == tab_right )
		{
			[window setFrame:NSMakeRect( frame.origin.x, frame.origin.y + CORNER_MARGIN, 
										 DROP_MARGIN,frame.size.height -CORNER_MARGIN*2)
					 display:YES];
			
			
		}
		
		
		[window orderFront:self];
		[window setIgnoresMouseEvents:NO];
		active = YES;
	}
}

-(void)setupDropWindows
{
	//NSLog(@"setupDropWindows");
	alignOption = preferenceIntValueForKey(@"bringToFront");
	edgeActivatingKeySwitch = preferenceBoolValueForKey(@"edgeActivatingKeySwitch") ;
	disableModifierToBringTabs = preferenceBoolValueForKey(@"disableModifierToBringTabs") ;

	
	sliderClickDelay = preferenceFloatValueForKey(@"sliderClickDelay");
	//edgeHideOffEdge tsukawanai
	
	 edgeEffect	= preferenceBoolValueForKey(@"edgeEffect");

	//BOOL edgeHideOffEdge = preferenceBoolValueForKey(@"edgeHideOffEdge");
	
	 edgeTop =  preferenceBoolValueForKey(@"edgeTop");
	 edgeTopSecond =  preferenceBoolValueForKey(@"edgeTopSecond");

	 edgeBottom =  preferenceBoolValueForKey(@"edgeBottom");
	 edgeBottomSecond =  preferenceBoolValueForKey(@"edgeBottomSecond");

	 edgeRight =  preferenceBoolValueForKey(@"edgeRight");
	 edgeRightSecond =  preferenceBoolValueForKey(@"edgeRightSecond");
	 edgeLeft =  preferenceBoolValueForKey(@"edgeLeft");
	 edgeLeftSecond =  preferenceBoolValueForKey(@"edgeLeftSecond");

	
	
	NSMutableArray* dropWindowsTemp = [NSMutableArray array];
	
	
	NSArray* screenArray = [NSScreen screens];
	int hoge;


	NSScreen* sc = [screenArray objectAtIndex:0];
	
	
	NSRect frame = [sc frame];
	
	
	////
	if(  edgeTop )
	{
		NSPanel* dropWindow_top = 
			[self createDropWindowWithFrame:
				NSMakeRect( frame.origin.x + CORNER_MARGIN, NSMaxY( frame ) -DROP_MARGIN,
							frame.size.width -CORNER_MARGIN*2 ,DROP_MARGIN)
							  dropViewStyle:tab_bottom
							  createNewMemo:  edgeTop & edgeEffect  
									screenID:0];
		[dropWindowsTemp addObject: dropWindow_top];
	}
	
	if(  edgeBottom )
	{
		NSPanel* dropWindow_bottom = 
			[self createDropWindowWithFrame:
				NSMakeRect( frame.origin.x +CORNER_MARGIN, frame.origin.y,
							frame.size.width -CORNER_MARGIN*2 ,DROP_MARGIN)
							  dropViewStyle:tab_top
							  createNewMemo:  edgeBottom & edgeEffect 
								   screenID:0];


		[dropWindowsTemp addObject: dropWindow_bottom];
	}
	
	if(  edgeLeft)
	{
		NSPanel* dropWindow_left = 
			[self createDropWindowWithFrame:
				NSMakeRect( frame.origin.x, frame.origin.y + CORNER_MARGIN, 
							DROP_MARGIN,frame.size.height -CORNER_MARGIN*2)
							  dropViewStyle:tab_right
							  createNewMemo:  edgeLeft & edgeEffect  
								   screenID:0];


		[dropWindowsTemp addObject: dropWindow_left];
	}
	
	
	if(  edgeRight)
	{
		NSPanel* dropWindow_right = 
			[self createDropWindowWithFrame:
				NSMakeRect( NSMaxX(frame)-DROP_MARGIN, frame.origin.y + CORNER_MARGIN , 
							DROP_MARGIN,frame.size.height -CORNER_MARGIN*2)
							  dropViewStyle:tab_left
							  createNewMemo:  edgeRight & edgeEffect  
								   screenID:0];


		[dropWindowsTemp addObject: dropWindow_right];
	}
		
	
//second monitor

	for( hoge = 1; hoge < [screenArray count]; hoge++ )
	{
		NSScreen* sc = [screenArray objectAtIndex:hoge];
		
		
		NSRect frame = [sc frame];
		
		
		////
		if(  edgeTopSecond)
		{		
			NSPanel* dropWindow_top = 
			[self createDropWindowWithFrame:
				NSMakeRect( frame.origin.x + CORNER_MARGIN, NSMaxY( frame ) -DROP_MARGIN,
							frame.size.width -CORNER_MARGIN*2 ,DROP_MARGIN)
							  dropViewStyle:tab_bottom
							  createNewMemo:  edgeTopSecond & edgeEffect  
								   screenID:hoge];



			[dropWindowsTemp addObject: dropWindow_top];
		}
		
		if(  edgeBottomSecond)
		{
			NSPanel* dropWindow_bottom = 
			[self createDropWindowWithFrame:
				NSMakeRect( frame.origin.x +CORNER_MARGIN, frame.origin.y,
							frame.size.width -CORNER_MARGIN*2 ,DROP_MARGIN)
							  dropViewStyle:tab_top
							  createNewMemo:  edgeBottomSecond & edgeEffect  
								   screenID:hoge];



			[dropWindowsTemp addObject: dropWindow_bottom];
		}
		
		if(  edgeLeftSecond)
		{
		
			NSPanel* dropWindow_left = 
			[self createDropWindowWithFrame:
				NSMakeRect( frame.origin.x, frame.origin.y + CORNER_MARGIN, 
							DROP_MARGIN,frame.size.height -CORNER_MARGIN*2)
							  dropViewStyle:tab_right
							  createNewMemo:  edgeLeftSecond & edgeEffect  
								   screenID:hoge];



			[dropWindowsTemp addObject: dropWindow_left];
		}
		
		if(  edgeRightSecond)
		{
			NSPanel* dropWindow_right = 
			[self createDropWindowWithFrame:
				NSMakeRect( NSMaxX(frame)-DROP_MARGIN, frame.origin.y + CORNER_MARGIN , 
							DROP_MARGIN,frame.size.height -CORNER_MARGIN*2)
							  dropViewStyle:tab_left
							  createNewMemo:  edgeRightSecond & edgeEffect  
								   screenID:hoge];



			[dropWindowsTemp addObject: dropWindow_right];
		}
		
	}
	
	
	////
	
	
	[dropWindows release];
	dropWindows = [NSArray arrayWithArray: dropWindowsTemp];
	
	[dropWindows retain];
	
	
	
	
}

@end
