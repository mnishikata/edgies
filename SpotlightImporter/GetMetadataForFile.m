//
//  GetMetadataForFile.c
//  Kojo Spotlight Importer
//
//  Created by __Name__ on 06/04/04.
//  Copyright (c) 2006 __MyCompanyName__. All rights reserved.
//

#include <CoreFoundation/CoreFoundation.h>
#import <CoreData/CoreData.h>
#import <Cocoa/Cocoa.h>
#define TEXT_FILENAME @"DOC.txt"
#define DOC_FILENAME @"DOC.edgydata"

#import "MNZip.h"


//==============================================================================
//
//	Get metadata attributes from document files
//
//	The purpose of this function is to extract useful information from the
//	file formats for your document, and set the values into the attribute
//  dictionary for Spotlight to include.
//
//==============================================================================




	Boolean GetMetadataForFile(void* thisInterface, 
							   CFMutableDictionaryRef attributes, 
							   CFStringRef contentTypeUTI,
							   CFStringRef pathToFile)

// do not use autorelease

{

		NSAutoreleasePool *pool;
		
		pool = [[NSAutoreleasePool alloc] init];
		
		
		Boolean success=NO;
//
		
		/*
		 kMDItemTextContent
		kMDItemTitle
		kMDItemDescription
		kMDItemAuthors
		kMDItemOrganizations
		kMDItemCopyright
		kMDItemKeywords
		kMDItemComment
		kMDItemContentCreationDate
		 kMDItemDisplayName
		 */
			
		NSData* data;
		id dic;
		
		NSData* readDocData = nil;
		
		//TRY TO READ AS ZIP
		
		//NSLog(@"Read ds zip");
		NSData *docData = [MNZip readDataInPackage:pathToFile forFile: DOC_FILENAME ];
		
		
		if( docData )
		{
			data = docData;
		}
		
		
		else  // Read as old version
		{
			data =[[[NSData alloc] initWithContentsOfFile:pathToFile] autorelease];
		}
		
		
		
		
		
		
		if( data != nil )
		{
			@try {
				dic = [NSKeyedUnarchiver unarchiveObjectWithData:data];
			}
			@catch (NSException * e) {
				dic = nil;
			}

			
		}
		
		if( dic != nil )
		{
		
		//
			id str = [dic objectForKey:@"spotlightTextContent"];
		
			if( str != nil )
			[attributes setObject: str forKey:kMDItemTextContent];
		//
			
			
			str = [dic objectForKey:@"p_title"];
			
			if( str != nil )
			{
				[attributes setObject: str forKey:kMDItemTitle];
				[attributes setObject: str forKey:kMDItemDisplayName];

			}
			
			
			/////////////////////////////////////
			str = [dic objectForKey:@"p_subject"];
			
			if( str != nil )
				[attributes setObject: str forKey:kMDItemDescription];
			
			/////////////////////////////////////
			str = [dic objectForKey:@"p_author"];
			
			if( str != nil )
				[attributes setObject: [NSArray arrayWithObject: str] forKey:kMDItemAuthors];
			
			/////////////////////////////////////
			str = [dic objectForKey:@"p_company"];
			
			if( str != nil )
				[attributes setObject: [NSArray arrayWithObject: str] forKey:kMDItemOrganizations];
			
			/////////////////////////////////////
			str = [dic objectForKey:@"p_copyright"];
			
			if( str != nil )
				[attributes setObject: str forKey:kMDItemCopyright];
			
			/////////////////////////////////////
			str = [dic objectForKey:@"p_keywords"];
			
			if( str != nil )
				[attributes setObject: str forKey:kMDItemKeywords];
			
			/////////////////////////////////////
			str = [dic objectForKey:@"p_comment"];
			
			if( str != nil )
				[attributes setObject: str forKey:kMDItemComment];
			
			
			/////////////////////////////////////
			str = [dic objectForKey:@"name"];
			
			
			if( str != nil )
			{
				NSDate* creationDate = [NSDate dateWithTimeIntervalSince1970:[str doubleValue]];
				[attributes setObject: creationDate forKey:kMDItemContentCreationDate];
			
			}
			

			
			
			
		success = YES;
		}
		//
	
			
		
		[pool release];
		return YES;


}




