//
//  NSWindow+Transforms.m
//  Rotated Windows
//
//  Created by Wade Tregaskis on Fri May 21 2004.
//
//  Copyright (c) 2004 Wade Tregaskis. All rights reserved.
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//    * Neither the name of Wade Tregaskis nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#import "NSWindow+Transforms.h"
#import "SoundManager.h"

@implementation NSWindow (Transforms)


- (NSTimeInterval)animationResizeTime:(NSRect)newWindowFrame
{
	return 0.27;
}




// Need to test these methods on a dual-display config - I suspect they're very very wrong.  I don't think each screen has a unique co-ordinate system - we *should* actually be returning the absolute co-ords, not adjusting them for whatever screen we're on... maybe.

- (NSPoint)windowToScreenCoordinates:(NSPoint)point {
    NSPoint result;
    NSRect screenFrame = [[[NSScreen screens] objectAtIndex:0] frame];
    
    //result = [self convertBaseToScreen:point]; // Doesn't work... it looks like the y co-ordinate is not inverted as necessary
    
    result.x = screenFrame.origin.x + _frame.origin.x + point.x;
    result.y = screenFrame.origin.y + screenFrame.size.height - (_frame.origin.y + point.y);
    
    return result;
}

- (NSPoint)screenToWindowCoordinates:(NSPoint)point { // Doesn't work with the transform stuff
    NSPoint result;
    NSRect screenFrame = [[self screen] frame];
    CGPoint a, b;
    
    result.x = point.x - _frame.origin.x - screenFrame.origin.x;
    result.y = _frame.origin.y + _frame.size.height - point.y - screenFrame.origin.y;
    
    a.x = point.x, a.y = point.y;
    
    CGAffineTransform original;
    CGSGetWindowTransform(_CGSDefaultConnection(), _windowNum, &original);
    
    b = CGPointApplyAffineTransform(a, original);
    
    return NSMakePoint(b.x, b.y);
    //return result;
}

- (void)rotate:(double)radians {
    [self rotate:radians about:NSMakePoint(_frame.size.width / 2.0, _frame.size.height / 2.0)];
}

- (void)rotate:(double)radians about:(NSPoint)point {
    CGAffineTransform original;
    NSPoint rotatePoint = [self windowToScreenCoordinates:point];
        
    CGSGetWindowTransform(_CGSDefaultConnection(), _windowNum, &original);
    
    original = CGAffineTransformTranslate(original, rotatePoint.x, rotatePoint.y);
    original = CGAffineTransformRotate(original, -radians);
    original = CGAffineTransformTranslate(original, -rotatePoint.x, -rotatePoint.y);
    
    CGSSetWindowTransform(_CGSDefaultConnection(), _windowNum, original);
}

- (void)scaleX:(double)x Y:(double)y {
    [self scaleX:x Y:y about:NSMakePoint(_frame.size.width / 2.0, _frame.size.height / 2.0)];
}

-(void)balloonAnimation:(NSRect)fromFrame to:(NSRect)toFrame speed:(float)speed
{
	
	
//	float zoomFactor = (float)fromFrame.size.width / (float)toFrame.size.width ;
	
	int count = 0;
		[self reset];
		

	NSPoint mousePoint = [self mouseLocationOutsideOfEventStream];
		
	[self scaleX: .06
			   Y: .06
		   about:mousePoint];

	[self orderFront:self];
	
	while( count<20 )
	{
		mousePoint = [self mouseLocationOutsideOfEventStream];
		
			//[self reset];
		[self scaleX: 1.09
				   Y: 1.09
			   about: mousePoint ];

 		[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.004 / speed]];
 		count++;
		
		
	}

	[[SoundManager sharedSoundManager] playSoundWithName:@"soundBalloonOpen"];



	[self reset];
	[self scaleX: 1.05
			   Y: 1.05
		   about: mousePoint ];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.015 / speed]];
	[self reset];
}


-(void)balloonCloseAnimationWithSpeed:(float)speed
{
	
	[self reset];

	[[SoundManager sharedSoundManager] playSoundWithName:@"soundBalloonClose"];

	
	float alpha = [self alphaValue];
	[self setAlphaValue:alpha*0.75];
	
	NSRect frame = [self frame];
	
	[self scaleX: 1.02
			   Y: 1.02
		   about: NSMakePoint(frame.size.width/2, frame.size.height/2) ];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01 / speed]];
	
	
	[self setAlphaValue:alpha*0.50];
	[self scaleX: 1.07
			   Y: 1.07
		   about: NSMakePoint(frame.size.width/2, frame.size.height/2) ];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.02 / speed]];
	[self reset];
	[self setAlphaValue:alpha ];

}



- (void)scaleX:(double)x Y:(double)y about:(NSPoint)point {
    CGAffineTransform original;
    NSPoint scalePoint = [self windowToScreenCoordinates:point];
    
    CGSGetWindowTransform(_CGSDefaultConnection(), _windowNum, &original);
    
    original = CGAffineTransformTranslate(original, scalePoint.x, scalePoint.y);
    original = CGAffineTransformScale(original, 1.0 / x, 1.0 / y);
    original = CGAffineTransformTranslate(original, -scalePoint.x, -scalePoint.y);
    
    CGSSetWindowTransform(_CGSDefaultConnection(), _windowNum, original);
}

- (void)shearX:(double)x Y:(double)y {
    [self shearX:x Y:y about:NSMakePoint(_frame.size.width / 2.0, _frame.size.height / 2.0)];
}

- (void)shearX:(double)x Y:(double)y about:(NSPoint)point {
    CGAffineTransform original, shear;
    NSPoint shearPoint = [self windowToScreenCoordinates:point];
    
    CGSGetWindowTransform(_CGSDefaultConnection(), _windowNum, &original);
    
    original = CGAffineTransformTranslate(original, shearPoint.x, shearPoint.y);
    
    shear.a = 1.0;
    shear.b = tan(y);
    shear.c = -tan(x);
    shear.d = 1.0;
    shear.tx = 0.0;
    shear.ty = 0.0;
    
    original = CGAffineTransformConcat(CGAffineTransformInvert(shear), original);

    original = CGAffineTransformTranslate(original, -shearPoint.x, -shearPoint.y);
    
    CGSSetWindowTransform(_CGSDefaultConnection(), _windowNum, original);
}

- (void)flipXAbout:(NSPoint)point {
    CGAffineTransform original;
    NSPoint shearPoint = [self windowToScreenCoordinates:point];
    
    CGSGetWindowTransform(_CGSDefaultConnection(), _windowNum, &original);
    
    original = CGAffineTransformTranslate(original, shearPoint.x, shearPoint.y);
    original.a = -original.a;
    original = CGAffineTransformTranslate(original, -shearPoint.x, -shearPoint.y);
    
    CGSSetWindowTransform(_CGSDefaultConnection(), _windowNum, original);
}

- (void)flipYAbout:(NSPoint)point {
    CGAffineTransform original;
    NSPoint shearPoint = [self windowToScreenCoordinates:point];
    
    CGSGetWindowTransform(_CGSDefaultConnection(), _windowNum, &original);
    
    original = CGAffineTransformTranslate(original, shearPoint.x, shearPoint.y);
    original.d = -original.d;
    original = CGAffineTransformTranslate(original, -shearPoint.x, -shearPoint.y);
    
    CGSSetWindowTransform(_CGSDefaultConnection(), _windowNum, original);
}

- (void)reset {
    // Note that this is not quite perfect... if you transform the window enough it may end up anywhere on the screen, but resetting it plonks it back where it started, which may correspond to it's most-logical position at that point in time.  Really what needs to be done is to reset the current transform matrix, in all places except it's translation, such that it stays roughly where it currently is.

    NSPoint point = [self windowToScreenCoordinates:NSMakePoint(0.0, _frame.size.height)]; // Get the screen position of the top left corner, by which our window is positioned
    
    CGSSetWindowTransform(_CGSDefaultConnection(), _windowNum, CGAffineTransformMakeTranslation(-point.x, -point.y));
}

- (void)test {
    /*const int JUNK_SIZE = 800;
    float junk[JUNK_SIZE];
    int i;
    
    for (i = 0; i < JUNK_SIZE; ++i) {
        junk[i] = i; // (float)random() / INT_MAX;
    }
    
    NSBeep();
    
    int err = CGSSetWindowWarp(_CGSDefaultConnection(), _windowNum, 8, 8, junk);
    //CGSFlushWindow(_CGSDefaultConnection(), _windowNum, 0);
    CGSSynchronizeWindow(_CGSDefaultConnection(), _windowNum);
    
    //NSLog(@"err = %d (0x%x)\n", err, err);*/
    
    CGSAnimationRef ref;
    int err;
    
    NSBeep();
    
    err = CGSCreateGenieWindowAnimation(_CGSDefaultConnection(), _windowNum, _windowNum, &ref);
    
   // //NSLog(@"err = %d (0x%x)\n", err, err);
}

@end
