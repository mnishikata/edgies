//
//  DragView.h
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef enum {
	d_rightBottom = 0,
	d_leftBottom,
	d_rightTop,
	d_leftTop
	
} DRAGMODE;

@interface DragView : NSView {



}
-(void)awakeFromNib;
-(void)dealloc;
-(void)drawRect:(NSRect)frame;
- (void)mouseDown:(NSEvent *)theEvent;
-(void)trackMouse:(DRAGMODE)mode;


@end
