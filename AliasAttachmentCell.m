//
//  FileEntityTextAttachmentCell.m
//  fileEntityTextView
//
//  Created by __Name__ on 06/08/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//
#import "AliasAttachmentCell.h"
#import "AliasInspector.h"
#import "FileLibrary.h"
#import "NSString (extension).h"
#import <WebKit/WebKit.h>
#import "TagManager.h"
extern BOOL UD_DELETE_CELLS_AFTER_DRAGGING_OUT;
extern BOOL UD_CELLS_DRAG_OUT_AS_ALIAS;


@implementation AliasAttachmentCell


+(NSAttributedString*)aliasAttachmentCellAttributedStringWithPath:(NSString*)path
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	AliasAttachmentCell* aCell = [[[AliasAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setTargetPath: path];
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	

#pragma mark -
- (id)initWithAttachment:(NSTextAttachment*)attachment
{
    self = [super initWithAttachment:(NSTextAttachment*)attachment];
    if (self) {
		
		empty = YES;
		[self loadDefaults];

		[self setTarget:self];
		[self setAction:@selector(open)];
	}
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	
	[encoder encodeBool:empty forKey:@"AliasAttachmentCell_empty"];
	
    return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		empty = YES;
		
		if( [coder containsValueForKey:@"AliasAttachmentCell_empty"] )
			empty = [coder decodeBoolForKey:@"AliasAttachmentCell_empty"];
		
		else
		{
			// legacy support
			NSString* myTitle = [self title];
			NSString* deleteLastLetter = [myTitle substringToIndex:  [myTitle length]-1 ];
			[self setTitle:deleteLastLetter];			
		}
		
		[self setTarget:self];
		[self setAction:@selector(open)];
	}
	return self;
	
	
}


#pragma mark -



-(void)setTargetPath:(NSString*)path
{
	
	if( ! [[NSFileManager defaultManager] fileExistsAtPath: path ] )
	{
		NSBeep();
		return;
	}

	[self setTitle:[path lastPathComponent]];
	NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:path];
	[self setImage:iconImage];	
	
	NSRect bounds = NSZeroRect;
	bounds.size = [self cellSize];
	
	NSRect imageRect = [self imageRectForBounds:bounds ];
	[[self image] setSize: imageRect.size];

	NDAlias* alias = [NDAlias aliasWithPath:path];
	[self setRepresentedObject:alias];
	
	[self setEmpty:NO];
	
}

-(NSString*)targetPath
{
	id aliasObj = [self representedObject] ;
	
	if( ![aliasObj isKindOfClass:[NDAlias class]] ) return nil;

	
	
	return [ (NDAlias*)aliasObj path];
}

-(NSAttributedString*)targetLink
{
	NSString* targetPath = [self targetPath];
	if( targetPath == nil ) return nil;
	
	NSURL* url = [NSURL fileURLWithPath: targetPath];
	NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[url absoluteString], NSLinkAttributeName,nil];
	NSAttributedString* attr = [[NSAttributedString alloc] initWithString:targetPath attributes:dictionary ];
	
	return [attr autorelease];
}

-(void)revealInFinder
{
	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	NSString* targetPath = [self targetPath];
	
	if( targetPath == nil )
	{
		NSBeep();
		[self setEmpty:YES];
		return;
	}
	
	
	if( ! [[NSFileManager defaultManager] fileExistsAtPath: targetPath ] )
	{
		NSBeep();
		[self setEmpty:YES];
		return;
	}
	
	
	
	if( ! [ws selectFile:targetPath inFileViewerRootedAtPath:targetPath] )
	{
		NSBeep();
		[self setEmpty:YES];
		return;
	}			
}

-(void)open
{
	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	NSString* targetPath = [self targetPath];
	
	if( targetPath == nil )
	{
		NSBeep();
		[self setEmpty:YES];
		return;
	}
	
	
	if( ! [[NSFileManager defaultManager] fileExistsAtPath: targetPath ] )
	{
		NSBeep();
		[self setEmpty:YES];
		return;
	}
	
	
	
	if( ! [ws openFile:targetPath] )
	{
		NSBeep();
		[self setEmpty:YES];
		return;
	}			
}


-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <AliasAttachmentCellOwnerTextView> *)textView
{
	//NSLog(@"customizeContextualMenu");
	
	
	int piyo;
	NSMenuItem* customMenuItem;
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"AliasAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
	//add separator
	customMenuItem = [NSMenuItem separatorItem];
	[customMenuItem setRepresentedObject:@"AliasAttachmentCellItem"];
	[aContextMenu addItem:customMenuItem ];
	
	
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Alias Button Inspector",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"AliasAttachmentCellItem"];
	[customMenuItem setTarget: textView];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(openAliasInspector)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
		
		

	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Reveal in Finder",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"AliasAttachmentCellItem"];
	[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(revealInFinder)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
	
	
	///
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Open",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"AliasAttachmentCellItem"];
	[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(open)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
	
	
	///
	if( pathIsFolder( [self targetPath]) )
	{
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Create file in this Folder from clipboard contents",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"AliasAttachmentCellItem"];
		[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(dropClipboard)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem autorelease];	
	}
		
}

-(void)dropClipboard
{
	[[TagManager sharedTagManager] dropClipboardTo:[self targetPath] windowForSheet :[textView window] ];
	
	
}

-(BOOL)isEmpty
{
	return empty;
}
-(void)setEmpty:(BOOL)flag
{
	empty = flag;
	[[AliasInspector sharedAliasInspector] updateInspector];

	
	
}

-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"AliasAttachmentCell_buttonSize"];
	[ud setInteger:buttonPosition forKey:@"AliasAttachmentCell_buttonPosition"];
	[ud setBool:showButtonTitle forKey:@"AliasAttachmentCell_showButtonTitle"];
	[ud setBool:showBezel forKey:@"AliasAttachmentCell_showBezel"];
	[ud setObject:[NSArchiver archivedDataWithRootObject: [self font]] forKey:@"AliasAttachmentCell_font"];
	
	[ud synchronize];
}


-(void)loadDefaults
{
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"AliasAttachmentCell_buttonSize"];
	buttonSize = (value != nil ? [value floatValue] : 48); 
	
	value = [ud valueForKey: @"AliasAttachmentCell_buttonPosition"];
	buttonPosition = (value != nil ? [value intValue] : 0); 
	
	value = [ud valueForKey: @"AliasAttachmentCell_showButtonTitle"];
	showButtonTitle = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"AliasAttachmentCell_showBezel"];
	showBezel = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"AliasAttachmentCell_font"];
	if( value != nil ){
		NSFont* font = [NSUnarchiver unarchiveObjectWithData: value];
		if( font != nil && [font isKindOfClass:[NSFont class]] )
			[self setFont:font];
		
	}
	
	lock = NO;
}


-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	[textView  openAliasInspector];
}

#pragma mark legacy

-(BOOL)substantiateAliasWithNDAlias:(NDAlias*)alias
{
	
	
	BOOL flag = [[NSFileManager defaultManager] fileExistsAtPath: [alias path] ];
	if( flag == NO ) return NO;
	
	
	[self setTitle:[[alias path] lastPathComponent]];
	NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:[alias path]];
	[self setImage:iconImage];	

	[self setRepresentedObject:alias];
	
	[self setEmpty:NO];
	

	
	return YES;
}


#pragma mark -
#pragma mark @protocol AttachmentCellConverting 

-(int)stringRepresentation:(NSDictionary*)context
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	NSString* string = [self targetPath];
	
	if( !string ) string = @" ";
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = [string length] -1;
	
	return changeInLength;
}




-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	NSString* string = [self targetPath];
	
	if( !string ) string = @" ";
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = [string length] -1;
	
	
	//add link
	
	
	NSURL* url = [NSURL fileURLWithPath: string];
	if( url )
	{
	NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[url absoluteString], NSLinkAttributeName,nil];
	[mattr addAttributes:dictionary range:NSMakeRange(index,[string length])];
	}
	
	return changeInLength;
	

}

-(int)attributedStringRepresentation:(NSDictionary*)context //withimage 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	//add icon
	[self setHighlighted:NO];
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[self buttonImageWithAlpha:1.0  ]];
	
	
	//NSAttributedString* attr = [NSAttributedString attributedStringWithAttachment: [self attachment]];
	
	
	

	//add link
	
	NSString* string = [self targetPath];

	if( string )
	{
		NSURL* url = [NSURL fileURLWithPath: string] ;;
		
		if( url )
		{
		NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[url absoluteString], NSLinkAttributeName,nil];
		[mattr addAttributes:dictionary range:NSMakeRange(index,1)];
		}
	}
	
	
	return 0;
	
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	NSString* strpath = [self targetPath];
	if( !strpath ) strpath = @"";
	
	NSArray* array = [NSArray arrayWithObjects:strpath,[self title],nil ];
	NSArray* array2 = [NSArray arrayWithObjects:[NSArray arrayWithObject:strpath],[NSArray arrayWithObject:[self title]],nil ];
		NSDictionary* val = [NSDictionary dictionaryWithObjectsAndKeys:
			array2, @"WebURLsWithTitlesPboardType",
			array, @"Apple URL pasteboard type", 
			strpath, NSStringPboardType,	
			@"AliasAttachmentCell",  NSFilesPromisePboardType,	nil];
	
	return val;
}
		
-(NSString*)writeToFileAt:(NSURL*)url 
{
	BOOL success;

	NSString* filename = [[self title] safeFilename];

	NSString* operation;
	if( UD_CELLS_DRAG_OUT_AS_ALIAS  )//alias 
	{
		
		operation = NSWorkspaceLinkOperation;
		
	}else 
	{
		operation = NSWorkspaceMoveOperation;

	}
	
	//NSLog(@"writeToFileAt ");	
	
	success =  dropFile([self targetPath] , [url path] , operation  , [self title] );

	//NSLog(@"success %d",success);
	
	if(  success &&  UD_DELETE_CELLS_AFTER_DRAGGING_OUT )
		[self removeFromTextView];
		

	
	return ( success ? filename : nil );
}

-(BOOL)canAcceptDrop
{
	NSString* targetPath = [self targetPath];
	return  pathCanAcceptDrop( targetPath );
	
	
	/*
	 OSStatus LSCanURLAcceptURL (
								 CFURLRef inItemURL,
								 CFURLRef inTargetURL,
								 LSRolesMask inRolesMask,
								 LSAcceptanceFlags inFlags,
								 Boolean *outAcceptsItem
								 );
	 */
}


			  
@end
