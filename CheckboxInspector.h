/* ButtonInspector */

#import <Cocoa/Cocoa.h>
#import "CheckboxAttachmentCell.h"

@interface CheckboxInspector : NSObject
{
    IBOutlet id window;
	
	
	NSArray* /*ButtonAttachmentCell**/ activeCells;
}

+(CheckboxInspector*)sharedCheckboxInspector;
- (id) init ;
-(BOOL)multipleSelection;
-(BOOL)noSelection;
-(void)showCheckboxInspector;
-(id)imageData;
-(id)title;
-(NSString*)targetPath;
-(void)setTitle:(NSString*)title;
-(id)font;
-(void)setFont:(NSFont*)font;
-(id)buttonSize;
-(void)setButtonSize:(NSNumber*)size;
-(id)buttonPosition;
-(void)setButtonPosition:(NSNumber*)position;
-(id)showButtonTitle;
-(void)setShowButtonTitle:(id)showButtonTitle;
-(id)color;
-(void)setColor:(NSColor*)color;
-(id)state;
-(void)setState:(NSNumber*)state;
-(id)showBezel;
-(void)setShowBezel:(id)showBezel;
-(void)dealloc;
-(BOOL)lock;
-(void)setLock:(BOOL)flag;
-(void)selectionChanged:(NSNotification*)notification;
-(void)updateInspector;
-(void)activateForTextView:(NSTextView  <CheckboxAttachmentCellOwnerTextView> *)view;
-(IBAction)useAsDefault:(id)sender;


@end
