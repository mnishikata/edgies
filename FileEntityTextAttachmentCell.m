//
//  FileEntityTextAttachmentCell.m
//  fileEntityTextView
//
//  Created by __Name__ on 06/08/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//
//#import "FileEntityUtil.h"
#import "FileEntityTextAttachmentCell.h"
//#import "FileEntityTextView.h"

#import "ColorCheckbox.h"
#import "EdgiesLibrary.h"

#import "NSString (extension).h"




@implementation FileEntityTextAttachmentCell

+(NSAttributedString*)fileEntityTextAttachmentCellAttributedStringWithPath:(NSString*)path
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	FileEntityTextAttachmentCell* aCell = [[[FileEntityTextAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setTargetPath: path];
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	

#pragma mark -

- (id)initWithAttachment:(NSTextAttachment*)anAttachment
{
    self = [super initWithAttachment:(NSTextAttachment*)anAttachment];
    if (self) {
		
		
		fileStorageFolderPath = [FILE_FOLDER stringByAppendingPathComponent:UNIQUENAME ];
		fileStorageFolderPath = [fileStorageFolderPath stringByAppendingPathExtension:MY_FOLDER_SUFFIX];
		[fileStorageFolderPath retain];
		
		
		
		if( ! [[NSFileManager defaultManager] fileExistsAtPath: FILE_FOLDER ] )
			[[NSFileManager defaultManager] createDirectoryAtPath: FILE_FOLDER attributes:NULL];

		if( ! [[NSFileManager defaultManager] fileExistsAtPath: fileStorageFolderPath ] )
			[[NSFileManager defaultManager] createDirectoryAtPath: fileStorageFolderPath attributes:NULL];
		
		
	}
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	[encoder encodeObject:fileStorageFolderPath forKey:@"FileEntityTextAttachmentCell_fileStorageFolderPath"];
	
    return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		
		if( [coder containsValueForKey:@"FileEntityTextAttachmentCell_fileStorageFolderPath"] )
		{
			
			fileStorageFolderPath = [coder decodeObjectForKey:@"FileEntityTextAttachmentCell_fileStorageFolderPath"];
			[fileStorageFolderPath retain];

		}else
		{
			fileStorageFolderPath = [self legacy_myFolderPath];
			[fileStorageFolderPath retain];

		}

		


		
	}
	return self;
	
	
}

-(BOOL)textView:(NSTextView <AliasAttachmentCellOwnerTextView> *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString
{
	[self destroy];
	return [super textView:aTextView shouldChangeTextInRange:affectedCharRange  replacementString:replacementString];
}


-(void)destroy
{
	if( ![[NSFileManager defaultManager] fileExistsAtPath: fileStorageFolderPath ] )
	{
		[self setEmpty:YES];
		return;
	}
	
	
	if( ! [[NSFileManager defaultManager] fileExistsAtPath: RESCUE_FOLDER ] )
		[[NSFileManager defaultManager] createDirectoryAtPath: RESCUE_FOLDER attributes:NULL];
	
	
	
	NSString* _dest = [RESCUE_FOLDER stringByAppendingPathComponent:[fileStorageFolderPath lastPathComponent] ];
	
	_dest = [_dest uniquePathForFolder];
	
	
	[[NSFileManager defaultManager]
							movePath:fileStorageFolderPath
							  toPath:_dest
							 handler:nil];
	
	
}

-(BOOL)setTargetPath:(NSString*)path
{
	
	if(  ! [[NSFileManager defaultManager] fileExistsAtPath: path ] )
		return NO;
	
	NSString* destination = [fileStorageFolderPath stringByAppendingPathComponent: [path lastPathComponent] ];

	if( ! [[NSFileManager defaultManager]
								movePath:path
								  toPath:destination
								 handler:nil] )
	{
		return NO;
	}

	[super setTargetPath:destination];	
	
	return YES;
}

-(void)dealloc
{
	//[FileEntityUtil unlockInformationFileAtPath:legacy_myFolderPath ];
	//[myButton release];
	
	//[FileEntityUtil deallocate];
	[fileStorageFolderPath release];
	[super dealloc];
}


#pragma mark -

/*
- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
	////NSLog(@"awakeAfterUsingCoder %@",[self myFolderPath]);
	
	[super awakeAfterUsingCoder:aDecoder];
	
	BOOL lock = [FileEntityUtil informationIsLockedAtPath: legacy_myFolderPath  ];

	if( lock == YES ) // i am a copy
	{
			////NSLog(@"awakeAfterUsingCoder locked");
		
//		NSString* oldTitle = [self title];
		NSString* oldTargetPath = [self targetPath];
		
		
		NSImage* selfImage = [self image];
		[self setTitle: UNIQUENAME];
		[self setShowButtonTitle:  preferenceBoolValueForKey(@"showButtonTitle")  ];
		[self setImage:selfImage];
		
		
		BOOL flag = [self createMyFolderIfNotExists];
		if( flag == NO ) return NO;

		
		NSString* destination = legacy_myFolderPath;
		destination = [destination stringByAppendingPathComponent: [oldTargetPath lastPathComponent] ];
		
		flag = [[NSFileManager defaultManager]
							copyPath:oldTargetPath
							  toPath:destination
							 handler:nil];	
	//	[FileEntityUtil lockFileFolder:[self myFolderPath]];

		
		if( flag == YES )
			if( [[FileEntityUtil contentsInFolder:legacy_myFolderPath] count] != 1 )
				flag = NO;
		
		if( flag == YES ) // file moved successfully
		{
//			NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:destination];
//			[self setImage:iconImage];
			
			[self initializeButton];
			
			[FileEntityUtil lockInformationFileAtPath:legacy_myFolderPath ];
		}
		
		
		
		
	}
	else			// i am the original
	{
			////NSLog(@"awakeAfterUsingCoder not locked");
		
		[FileEntityUtil lockInformationFileAtPath:legacy_myFolderPath ];
		[self initializeButton];

		
	}
		
	
	return self;
}





-(void)initializeButton
{
	[myButton release];
	myButton = [[NSButton alloc] init];
	[myButton setTitle:@""];
	
	[myButton setBordered:YES];
	[myButton setButtonType:NSOnOffButton ];
	[myButton setBezelStyle:NSRegularSquareBezelStyle];
	
	[myButton setFrame:NSMakeRect(0,0,BUTTON_SIZE,BUTTON_SIZE)];

	
	if( [self image] != nil ) [myButton setImage:[self image]];
	
	
	NSString* targetPath = [self targetPath];
	NSString* title;
	
	if( [FileEntityUtil fileAtTargetPathIsKagemusha: targetPath ] )
	{
		title = [[[self targetPath] lastPathComponent] stringByDeletingPathExtension];
	}else
	{
		title  = [[self targetPath] lastPathComponent];
	}
	
	[myButton setAlternateTitle:title];
}





-(BOOL)substantiateWithFileAtPath:(NSString*)path copy:(BOOL)copyFlag
{
	[self setTitle: UNIQUENAME];
	
	[self setShowButtonTitle :  preferenceBoolValueForKey(@"showButtonTitle")  ];

	
	BOOL flag = [[NSFileManager defaultManager] fileExistsAtPath: path ];
	if( flag == NO ) return NO;
	
	flag = [self createMyFolderIfNotExists];
	if( flag == NO ) return NO;
	
	
	
	NSString* destination = legacy_myFolderPath;
	
	destination = [destination stringByAppendingPathComponent: [path lastPathComponent] ];
	
	//[FileEntityUtil unlockFileFolder:[self myFolderPath]];
	if(  preferenceIntValueForKey(@"commandDropBehaviour")  == 1 )
	{
		
		if( copyFlag == NO )
		{
			flag = [[NSFileManager defaultManager]
								movePath:path
								  toPath:destination
								 handler:nil];
		}else
		{
			flag = [[NSFileManager defaultManager]
								copyPath:path
								  toPath:destination
								 handler:nil];
			
		}
		
		if( flag == YES )
		{
			NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:destination];
			[self setImage:iconImage];	
		}
		
		
	}else //commandDropBehaviour =0
	{
		
		
		flag = [FileEntityUtil createKagemushaAtPath:destination];
		
		if( flag == YES )
		{
			NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:path];
			[self setImage:iconImage];	
		}
		
	}

	
//	//NSLog(@"%@ ",[[self contentsInMyFolder] description] );
	
	if( flag == YES )
		if( [[FileEntityUtil contentsInFolder:legacy_myFolderPath] count] != 1 )
			flag = NO;
	
	if( flag == YES ) // file moved successfully
	{



		
		[self initializeButton];
		
		[FileEntityUtil lockInformationFileAtPath:legacy_myFolderPath ];
	}
	
	
	//[FileEntityUtil lockFileFolder:[self myFolderPath]];

	return flag;
}
*/

/*
-(NSString*)myFolderPath
{
	NSString* title = [self title];
	
	if( [title isEqualToString: @"" ] || title == nil ) return nil;
	
	

	
	
	title = [NSString stringWithFormat:@"%@%@",[title substringToIndex:  [title length]-1 ] ,MY_FOLDER_SUFFIX]; 
	
	NSString* path = [FILE_FOLDER stringByAppendingPathComponent: title];
	
	return path;
	
}
*/

/*

-(BOOL)isEmpty
{
	NSString* string = [self targetPath] ;
	if( string == nil || [string isEqualToString:@""])
		return YES;
	
	BOOL returnFlag;
	
	
	if( [FileEntityUtil isAliasPath:string] )
		return NO;
	
	returnFlag = ! [[NSFileManager defaultManager] fileExistsAtPath: string ];

	return returnFlag;
}

-(NSString*)targetPath
{
	NSArray* array = [FileEntityUtil contentsInFolder:legacy_myFolderPath];
	if( array == nil || [array count] < 1 ) return nil;
	
	
	NSString* specificItem = [array objectAtIndex:0];
	NSString* targetPath = [legacy_myFolderPath stringByAppendingPathComponent:specificItem];

	////NSLog(@"targetPath %@", targetPath);
	return targetPath;
}

-(NSString*)targetFilename
{
	NSString* targetPath = [self targetPath];
	

	return [targetPath lastPathComponent];
}

-(BOOL)createMyFolderIfNotExists
{
	if( ! [[NSFileManager defaultManager] fileExistsAtPath: FILE_FOLDER ] )
	{
		[[NSFileManager defaultManager] createDirectoryAtPath: FILE_FOLDER attributes:NULL];
	}
	
	BOOL flag;
	
	NSString* path = legacy_myFolderPath;
	if( path == nil ) return NO;
	
	flag = [[NSFileManager defaultManager] fileExistsAtPath: path ];
	if( flag == NO  )
	{
		flag = [[NSFileManager defaultManager] createDirectoryAtPath: path attributes:NULL];
		//[FileEntityUtil lockFileFolder:path];

	}
	
	return flag;
}


-(BOOL)deleteMyFolderIfEmpty
{
	NSString* path = legacy_myFolderPath;
	if( path == nil ) return NO;
	
	
	NSArray* contents = [FileEntityUtil contentsInFolder:path ];
	if( [contents count] != 0 ) return NO;
	
	//[FileEntityUtil unlockFileFolder: path ];
	BOOL flag =  [[NSFileManager defaultManager]  removeFileAtPath:path  handler:nil];

	
	return flag;
}

*/
-(NSString*)legacy_myFolderPath
{
	//legacy_myFolderPath
	
	NSString* title = [self title];
	
	if( [title isEqualToString: @"" ] || title == nil ) return nil;
	else
		title = [NSString stringWithFormat:@"%@%@",[title substringToIndex:  [title length]-1 ] ,MY_FOLDER_SUFFIX]; 
	
	return [FILE_FOLDER stringByAppendingPathComponent: title] ;
	
	//

	
}


@end
