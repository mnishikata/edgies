/* LinkTextView */
//

#import <Cocoa/Cocoa.h>
#import "AttributeSafeTextView.h"
#import <WebKit/WebKit.h>
#import "NDAlias.h"

@interface LinkTextView : NSTextView
{
	
	id NDAliasToBeDropped;

	unsigned droppingInsertionPoint;

	NSMutableArray* impersistentAttributes;


	
	///
	WebView* aWebView;
	NSMutableArray* faviconQue;
	
	NSImage* defaultFavicon;
	

	
	/// Request Favicon
	
	
}
- (void)clickedOnLink:(id)link atIndex:(unsigned)charIndex;
-(void)awakeFromNib;
-(void)dealloc;
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender;
- (void)draggingExited:(id <NSDraggingInfo>)sender;
- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender;
-(void)drawRoundedRect:(NSRect)rect color:(NSColor*)aColor width:(float )width fill:(BOOL)fill ;
-(void)drawInsertionPoint;
-(NDAlias*)mouseOnNDAlias:(NSRange*)rangePointer;
//- (NSString *)textView:(NSTextView *)textView willDisplayToolTip:(NSString *)tooltip forCharacterAtIndex:(unsigned)characterIndex;
//-(void)closeBalloon;
-(void)pasteAsArchive:(id <NSDraggingInfo>)sender;
-(BOOL)pastePicture:(id <NSDraggingInfo>)sender;
-(BOOL)pasteSafariURL:(id <NSDraggingInfo>)sender;
-(BOOL)pasteFilenames:(id <NSDraggingInfo>)sender;
-(BOOL)pasteOtherURL:(id <NSDraggingInfo>)sender;
-(BOOL)pasteRTFD:(id <NSDraggingInfo>)sender;
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender;
-(void)addFaviconForLinksInRange:(NSRange)range;
-(void)receivedFavicon:(NSImage*) image  forURL:(NSURL*)originalUrl;
-(void)startQue;
-(void)requestFavicon:(NSURL*)url;
- (void)webView:(WebView *)sender didReceiveIcon:(NSImage *)image forFrame:(WebFrame *)frame;
- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame;
- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame;

@end

