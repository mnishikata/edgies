#import "LinkTextView.h"

#import "ColorCheckbox.h"
#import <HIToolbox/CarbonEventsCore.h>
#import "FileUtil.h"

#import "AppController.h"
#import "FileEntityTextAttachmentCell.h"
#import "AliasAttachmentCell.h"
#import "SoundManager.h"
#import "KeyModifierManager.h"
#import "ModifierKeyWell.h"
#import "SaferTextStorage.h"
#import "MNGraphicAttachmentCell.h"


#define AVAILABLE_TYPES [NSArray arrayWithObjects:  @"EdgesTextPboardType", NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType",NSFilenamesPboardType, NSFilesPromisePboardType, nil]

#define DROP_ANYTHING_PBTYPE @"DropAnythingPboardType"

#define DropAnythingAttributeName @"DropAnythingAttributeName"
#define DropAnythingDragImageAttributeName @"DropAnythingDragImageAttributeName"

#define AttributeSafeTextViewPboardType @"EdgesTextPboardType"

//#define BALLOON_CONTROLLER [[[NSApplication sharedApplication] delegate] valueForKey:@"balloon"]



@implementation LinkTextView


- (void)clickedOnLink:(id)link atIndex:(unsigned)charIndex
{
    if ([link isKindOfClass: [NDAlias class]])
    {
		[[NSWorkspace sharedWorkspace] openFile:[link path]];
			

		
			/*
        NSRunAlertPanel (@"Whee!",
						 [NSString stringWithFormat: @"link to ... %@", [link path]],
						 nil, nil, nil);
			 */
        return ;
    }
	
	[super clickedOnLink:link atIndex:charIndex];
}

-(void)awakeFromNib
{
	droppingInsertionPoint = 0;
	
	[[WebPreferences standardPreferences] setPlugInsEnabled:NO];
	[[WebPreferences standardPreferences] setLoadsImagesAutomatically:NO];
	[[WebPreferences standardPreferences] setJavaEnabled:NO];
	
	aWebView = [[WebView alloc] initWithFrame:NSMakeRect(0,0,600,300)
											frameName:nil
											groupName:nil] ;
	
	faviconQue = [[NSMutableArray array] retain];
	
	
	NSBundle* myBundle;
	myBundle = [NSBundle bundleForClass:[self class]];
	NSString* path;
	
	path = [myBundle pathForResource:@"defaultFavicon" ofType:@"tiff"];
	defaultFavicon = [[NSImage alloc] initWithContentsOfFile:path ];

	
	////
	
	
	SaferTextStorage* saferTextStorage = [[[SaferTextStorage alloc] init] autorelease];
	NSLayoutManager* lm = [[[NSLayoutManager alloc] init] autorelease];
	
	
	
	[[self textContainer] replaceLayoutManager:lm];
	
	
	
	
	impersistentAttributes = [[NSMutableArray alloc] init];
	
	[self addImpersistentAttribute:NSCursorAttributeName];
	[self addImpersistentAttribute:NSToolTipAttributeName];
	[self addImpersistentAttribute:NSLinkAttributeName];
	[self addImpersistentAttribute:DropAnythingAttributeName];

	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	BOOL flag = [ud boolForKey:@"isContinuousSpellChecking"];
	
	[self  setContinuousSpellCheckingEnabled:flag];
	
	
	

}
-(void)dealloc
{
	[impersistentAttributes release];

	[aWebView stopLoading:self];
	[aWebView release];
	//[aWebView release];
	[super dealloc];
}

- (unsigned int)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	
	return NSDragOperationCopy | NSDragOperationLink | NSDragOperationGeneric | NSDragOperationPrivate | NSDragOperationMove;
}


- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{

		
	[[self window] cacheImageInRect:[[self window] frame]];

	
    NSPasteboard *pboard = [sender draggingPasteboard];
	if (self == [sender draggingSource]) //from self
		return NSDragOperationGeneric  ;
	
	else //from other
	{

		if([[pboard types] containsObject:NSFilenamesPboardType]  )
		{
			return   NSDragOperationCopy   ;
			
		}
		else
			return NSDragOperationCopy ;
	}	
}

- (void)draggingExited:(id <NSDraggingInfo>)sender
{
	[[self window] restoreCachedImage];
	[[self window] flushWindow];
}



- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender
{
//MNDragOperationDropOnNDAlias
	

	
	//[super draggingUpdated:sender];

	NSPoint aPoint = [[self window] mouseLocationOutsideOfEventStream]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	BOOL needToBeUpdated = YES;
	

	if( droppingInsertionPoint == insertionCharIndex ) needToBeUpdated = NO;
	
	droppingInsertionPoint = insertionCharIndex;
	
	if( needToBeUpdated == YES )
	{
		[[self window] restoreCachedImage];
		[[self window] flushWindow];	
		[[self window] display];
		[[self window] cacheImageInRect:[[self window] frame]];
	}
	
    NSPasteboard *pboard = [sender draggingPasteboard];

	



	NSRange linkRange;
	NDAliasToBeDropped = [self mouseOnNDAlias:&linkRange];

	
	if( NDAliasToBeDropped != nil )
	{
		NSDictionary* fileAttr = [[NSFileManager defaultManager] 
								fileAttributesAtPath:[NDAliasToBeDropped path]
										traverseLink:NO];

		
		if( [[sender draggingSource] isKindOfClass:[LinkTextView class]] && [[pboard types] containsObject:NSFilenamesPboardType] )
			NDAliasToBeDropped = nil;
		
		
		else if( ! [[fileAttr objectForKey:NSFileType] isEqual:  NSFileTypeDirectory ] )
			NDAliasToBeDropped = nil;
		

		else if( [[NSWorkspace sharedWorkspace] isFilePackageAtPath:[NDAliasToBeDropped path]] ) //if application 
			if( ! [[pboard types] containsObject:NSFilenamesPboardType]  ) // not contain file icon
				NDAliasToBeDropped = nil;
		
		
	}
	
		
	if( NDAliasToBeDropped != NULL ) //dropping onto ndalias
	{
		
		
		
		if( needToBeUpdated == YES )
		{
			NSRange linkGlyphRange = [[self layoutManager] glyphRangeForCharacterRange:linkRange actualCharacterRange:NULL];
			NSRect rect = [[self layoutManager] boundingRectForGlyphRange:linkGlyphRange inTextContainer:[self textContainer]];
			
			//rect = [self convertRect:rect toView:[[self window] contentView]];
			
			//[[[self window] contentView] lockFocus];
			[self lockFocus];
			
			//draw rounded rectangle
			NSColor* aColor = [NSColor alternateSelectedControlColor];

			[self drawRoundedRect:rect color:[aColor colorWithAlphaComponent:0.2] width:3.0 fill:YES]; 
			[self drawRoundedRect:rect color:aColor width:3.0 fill:NO]; 

	
			[self unlockFocus];

		//	[[[self window] contentView] unlockFocus];
			
			[[self window] flushWindow];
			
		}
		

		
	}else
	{
		if (self == [sender draggingSource]) //from self
		{
			NSEvent* theEvent = [[self window] currentEvent];
			
			if( needToBeUpdated == YES )
				[self drawInsertionPoint];
			
			if( [theEvent modifierFlags] == 524576)
				return NSDragOperationCopy;
			else return NSDragOperationMove;
		}
		
		
		if( needToBeUpdated == YES )
			[self drawInsertionPoint];

	}
	
	
	
	if( [[pboard types] containsObject:NSFilenamesPboardType]  )
	{
		//int modKey = GetCurrentKeyModifiers( );
		////NSLog(@"%d",modKey);
		// ooption = 2048
		// option command = 2304
		// caps 1024
		
		
		//if ( (modKey | 1024) == (2048 | 1024) )  //option 
		//{
			////NSLog(@"copy");
		//			return NSDragOperationCopy ;
		//}
		
		if (  [MOD_PRESSED:@"modLink"]  )  //option 
		{
			////NSLog(@"link");

			return NSDragOperationLink  ;
		}
		return NSDragOperationCopy ;
	
	}else
	{
		return NSDragOperationCopy ;
	}
	
	
	/*
	
	//NSLog(@"mod %d",modKey);
	
	NSEvent* theEvent = [[self window] currentEvent];

	if( [theEvent modifierFlags] == 524576)
		return NSDragOperationCopy;
	
	else 	if( [theEvent modifierFlags] == 1573160 )
		return NSDragOperationLink;
	
	else return NSDragOperationMove;
	 */
		

	
}



-(void)drawRoundedRect:(NSRect)rect color:(NSColor*)aColor width:(float )width fill:(BOOL)fill 
{
	//
	float boxCurve = 8.0;
	rect.origin .x -= boxCurve/2; rect.size.width += boxCurve ;

	NSColor* _color = [aColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace ];
	
	float red, green, blue, alpha;
	[_color getRed:&red green:&green blue:&blue alpha:&alpha];
	
	

	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetRGBFillColor(context, red, green, blue, alpha );
	
	//set width
	CGContextSetLineWidth ( context, width   );
	
	//Fill 
	CGContextBeginPath(context);
	addRoundedRectToPath(context, CGRectMake(rect.origin.x ,rect.origin.y , rect.size.width, rect.size.height),boxCurve, boxCurve);	


	if( fill == NO )
		CGContextStrokePath(context);
	else
		CGContextFillPath(context);

}

-(void)drawInsertionPoint
{
	
	NSPoint aPoint = [[self window] mouseLocationOutsideOfEventStream]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	if( insertionCharIndex == NSNotFound ) return;
	

	NSRange glyphRange = [[self layoutManager] 
	glyphRangeForCharacterRange:NSMakeRange(insertionCharIndex,1) actualCharacterRange:NULL ];
	
	NSRect rect = [[self layoutManager] boundingRectForGlyphRange:glyphRange inTextContainer:[self textContainer]];
	
	//rect = [self convertRect:rect toView:[[self window]contentView]];
	
	rect.size.width = 1;
	
	//[[[self window] contentView] lockFocus];
	[self lockFocus];
	
	[[NSColor blackColor] set];
	[NSBezierPath setDefaultLineWidth:2.0];
	[NSBezierPath strokeRect:rect];
	//[[[self window] contentView] unlockFocus];
	[self unlockFocus];

	[[self window] flushWindow];	

}



-(NDAlias*)mouseOnNDAlias:(NSRange*)rangePointer
{
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	if( charIndex == NSNotFound || [self fullRange].length-1 < charIndex ) return NULL;
	
	id someLink = [[self textStorage] attribute:NSLinkAttributeName atIndex:charIndex longestEffectiveRange:rangePointer inRange:[self fullRange]];
	
	if ([someLink isKindOfClass: [NDAlias class]])
		return someLink;
	else return NULL;
	
}




- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	/* remove MN item */
	int piyo;
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if(  [[customMenuItem representedObject]  isKindOfClass:[NDAlias class]]  )
		{
			[aContextMenu removeItem:customMenuItem];
		}
		
	}
	
	
	
	
	
	NDAlias* ndalias = [self mouseOnNDAlias:nil];
	if( ndalias != nil )
	{
		
	
		//
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Reveal in Finder",@"")
													action:@selector(menu_selectedRevealInFinder:) keyEquivalent:@""];
		[customMenuItem setTag:0];
		[customMenuItem setRepresentedObject:ndalias];
		[aContextMenu insertItem:customMenuItem atIndex:0];
		
		[customMenuItem autorelease];	
	}
	
	return aContextMenu;
}
-(void)menu_selectedRevealInFinder:(id)sender
{
	NSString* path = [[sender representedObject] path];
	
	if( path == nil )
	{
	
		NSBeep();
		return;
	}
	
	[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath: path];

		
}

/*
- (NSString *)textView:(NSTextView *)textView willDisplayToolTip:(NSString *)tooltip forCharacterAtIndex:(unsigned)characterIndex
{
		if( ! [tooltip isEqualToString: DropAnythingDragImageAttributeName] )
		{
			[super textView:textView willDisplayToolTip:tooltip forCharacterAtIndex:characterIndex];
			return;
				
		}
	
	
	
	id something = [[self textStorage] attribute: DropAnythingDragImageAttributeName
										 atIndex: characterIndex
								  effectiveRange:nil];
	if( something != nil )
	{
		// show balloon
		
		[BALLOON_CONTROLLER showBalloonWithImage: something
								color:[NSColor whiteColor]
								   at:[NSEvent mouseLocation]];

		[NSTimer scheduledTimerWithTimeInterval:1.0 target:self
														 selector:@selector(closeBalloon) userInfo:NULL repeats:NO];
	}
	
}

-(void)closeBalloon
{
	//[TAB_CURSOR closeTimerCursor];

		
		[BALLOON_CONTROLLER hideBalloon];
		
		

}*/

#pragma mark -

-(void)addImpersistentAttribute:(NSString*)attribute
{
	if( [impersistentAttributes indexOfObject:attribute] == NSNotFound )
		[impersistentAttributes addObject:attribute];
}


-(IBAction)copyAsPlainText:(id)sender
{
	NSRange selRange = [self selectedRange];
	
	NSPasteboard* pboard = [NSPasteboard pasteboardWithName:NSGeneralPboard];
	
	[pboard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
	
	//NSString* aStr = [[[self textStorage] string] substringWithRange:selRange];
	NSString* aStr = [[[self convertCheckboxesInRange:selRange] string] substringWithRange:selRange];
	
	[pboard setString:aStr forType:NSStringPboardType];  //export string	
	
	
}


- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard types:(NSArray *)types
{
	
	
	
	NSData* aDragginData;
	NSRange selRange = [self selectedRange];
	
	
	////NSLog(@"%d",GetCurrentKeyModifiers( ));
	
	///check key modifier ... when dragging out 
	
	int modKey = GetCurrentKeyModifiers( );
	if(    ( (modKey | 1024) == (0x200|0x800 | 1024)  ) )
	{
		[pboard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
		
		NSString* aStr = [[[self convertCheckboxesInRange:selRange] string] substringWithRange:selRange];
		[pboard setString:aStr forType:NSStringPboardType];  //export string	
		
		return YES;
	}
	
	
	
	[pboard declareTypes:types owner:self];
	
	
	
	
	
	//TIFF or Drop Anything
	NSRange range = [self selectedRange];
	////NSLog(@"%d",range.length);
	if( range.length == 1 )
	{
		////NSLog(@"%@",[[[self textStorage] attributesAtIndex:range.location
		//								   effectiveRange:nil] description]);
		
		
		id something = [[self textStorage] attribute:NSAttachmentAttributeName
											 atIndex:range.location effectiveRange:NULL];
	
		if( [[something attachmentCell] isKindOfClass:[MNGraphicAttachmentCell class]]  )
		{
			
			id furtherSomething = [[self textStorage] attribute:DropAnythingAttributeName
														atIndex:range.location effectiveRange:NULL];
			if(  furtherSomething != nil )
			{
				
				
				////NSLog(@"here");
				
				NSDictionary* pbDic = [NSKeyedUnarchiver unarchiveObjectWithData:furtherSomething];
				
				NSArray* types = [pbDic allKeys];
				[pboard declareTypes:types owner:self];
				////NSLog(@"%@", [[pboard types]description] );
				
				int hoge;
				for( hoge = 0; hoge < [types count]; hoge++ )
				{
					NSString* type = [types objectAtIndex:hoge];
					////NSLog(@"Reading %@",type);
					
					[pboard setData:[pbDic objectForKey:type] forType:type];
				}
				
				
				//NSData* _rtfdData = [self portableDateFromRange:range ];
				//[pboard setData:_rtfdData forType:DROP_ANYTHING_PBTYPE];
				return YES;	
				
			}
			
			/*else // tiff
			{
				////NSLog(@"write tiff");
				
				NSImage* image =  [[something attachmentCell] image];
				
				NSData* tiffData = [image TIFFRepresentation];
				

				[pboard declareTypes:[NSArray arrayWithObject:NSTIFFPboardType ] owner:self];

				[pboard setData:tiffData forType:NSTIFFPboardType];
				return YES;	
			}*/
			
		}
		
		
	}
	
	
	
	// write to paste board
	
	NSAttributedString* attr = [[self textStorage] attributedSubstringFromRange:selRange];
	aDragginData =  [NSKeyedArchiver archivedDataWithRootObject:attr];
		
	[pboard setData:aDragginData forType:AttributeSafeTextViewPboardType];
	
	
	
	
	NSDictionary* docAttr = nil;
	 if( [[self window] respondsToSelector:@selector(ownerDocument)] )
	 {
		 id ownerDocument = [(MNTabWindow*) [self window] ownerDocument];
		 if( [ownerDocument isKindOfClass:[MiniDocument class]] )				
			 docAttr = [ownerDocument documentProperty];
		 
		 
	 }
	 
	//rtfd  //
	 
	 NSAttributedString* attrToWriteToPasteboard = [self convertAttributedStringForCopying:[attr attributedSubstringFromRange:selRange]
																				useGraphic:YES];

	 aDragginData = [attrToWriteToPasteboard RTFDFromRange:NSMakeRange(0,[[attrToWriteToPasteboard string] length])
						documentAttributes:docAttr ];
	
	 
	[pboard setData:aDragginData forType:NSRTFDPboardType]; 
	
	
	//rtf
	
	attrToWriteToPasteboard = [self convertAttributedStringForCopying:[attr attributedSubstringFromRange:selRange]
																			   useGraphic:NO];
			
	aDragginData = [attrToWriteToPasteboard RTFFromRange:NSMakeRange(0,[[attrToWriteToPasteboard string] length])
						   documentAttributes:docAttr ];
	
	
	[pboard setData:aDragginData forType:NSRTFPboardType]; 
	
	//String does not work ????****
	[pboard setString:[attrToWriteToPasteboard string] forType:NSStringPboardType];  //export string
	
	
	
	
	
	
	return YES;	
}


-(NSAttributedString*)expandDropAnythingArchive:(NSAttributedString*)org
{
	if( [org containsAttachments] == NO ) return org;
	
	NSRange range = {0,0};
	NSRange fullRange = NSMakeRange(0, [[org string] length] );
	NSMutableAttributedString* newAttr = [[[NSMutableAttributedString alloc] init] autorelease];
	
	unsigned hoge;
	for( hoge = 0; hoge < [[org string] length];  )
	{
		id something = [org attribute:DropAnythingAttributeName
							  atIndex:hoge
				longestEffectiveRange:&range
							  inRange:fullRange];
		
		
		if( something != nil )
		{
			
		}
		
		else
		{
			[newAttr appendAttributedString:[org attributedSubstringFromRange:range] ];
		}
		
		
	}
	
	return newAttr;
}

-(id)contentsInPaseteboard:(NSPasteboard *)pboard
{
	BOOL returnStatus = NO;
	
	
	if( [pboard availableTypeFromArray:AVAILABLE_TYPES] == DROP_ANYTHING_PBTYPE  ) 
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:DROP_ANYTHING_PBTYPE];
		if(aData == NULL) returnStatus = NO;
		
		else
		{
			 
			NSMutableAttributedString* anAttr = [NSKeyedUnarchiver unarchiveObjectWithData:aData];
			return anAttr;
		}
	}
	
	else if( [pboard availableTypeFromArray:AVAILABLE_TYPES] == AttributeSafeTextViewPboardType  )
	{	
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:AttributeSafeTextViewPboardType];
		if(aData == NULL) returnStatus = NO;
		
		else
		{
			NSMutableAttributedString* anAttr = [NSKeyedUnarchiver unarchiveObjectWithData:aData];
			return anAttr;
		}
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSRTFDPboardType)
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:NSRTFDPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTFD:aData documentAttributes:NULL];
		
		//paste 
		[anAttr autorelease];
		return anAttr;
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSRTFPboardType)
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:NSRTFPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
		
		//paste 
		[anAttr autorelease];
		return anAttr;
		
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSStringPboardType)
	{
		//get array data and text data from pboard 
		NSString* aStr = [pboard stringForType:NSStringPboardType];
		
		//paste 
		return aStr;
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSTIFFPboardType)
	{
		//get array data and text data from pboard 
		NSData* tiffData = [pboard dataForType: NSTIFFPboardType ];
		NSImage* image = [[[NSImage alloc] initWithData:tiffData] autorelease];
		
		
		MNGraphicAttachmentCell* newCell = 
			[[[MNGraphicAttachmentCell alloc] initImageCell: image ] autorelease];

		
		
		NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
		//add files
		
		
		NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
		
		[anAttachment setAttachmentCell:newCell]; //convert


		
		
		
		[wrapper setFilename:@"image.tiff"];
		[wrapper setPreferredFilename:[wrapper filename]];//self rename
			
			[wrapper setIcon:image ];
			
			NSMutableAttributedString* aStr = (NSMutableAttributedString*)[NSMutableAttributedString attributedStringWithAttachment:anAttachment];
			

			
		
		
		//paste 
		return aStr;
		
	}
	
	return NULL;
}

- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard  //Paste
{
	BOOL returnStatus = NO;
	
	id contents = [self contentsInPaseteboard:pboard];
	
	if( contents != NULL )
	{
		
		//-(void)fitGraphicsToWindowWidthInRange:(NSRange)aRange


		if(    [MOD_PRESSED: @"modPasteAsPlainText"]    )
		{
			if( [contents isKindOfClass:[NSAttributedString class]] )
			{
				contents = [contents string];
			}
			
			//NSLog(@"modPasteAsPlainText is pressed");
		}
		
		

		
		
		[self pasteText:contents];
		
		
		if( [self respondsToSelector:@selector(shrinkGraphicsToWindowWidthInRange:)] )
		{
			[self shrinkGraphicsToWindowWidthInRange:NSMakeRange([self selectedRange].location-[contents length], [contents length])];

		}
				
				
		returnStatus = YES;
		
	
	}
	else
		returnStatus = [super readSelectionFromPasteboard:pboard];

	
	return returnStatus;
}




- (BOOL)resignFirstResponder
{	
	BOOL flag = YES;//[super resignFirstResponder];
	
	// Inputting Kanji
	if( [self hasMarkedText] )
		flag = NO;
	
		
	return flag;
	
}

- (void)setContinuousSpellCheckingEnabled:(BOOL)flag
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setBool:flag forKey:@"isContinuousSpellChecking"];

	
	[super  setContinuousSpellCheckingEnabled:flag];
}


/////
-(IBAction)deleteColor:(id)sender
{
	NSRange range = [self selectedRange];
	
	if( range.length != 0 )
	{
		[[self textStorage] removeAttribute:NSForegroundColorAttributeName range:range];
		[[self textStorage] removeAttribute:NSBackgroundColorAttributeName range:range];
	
	}else
	{
		NSMutableDictionary* typingAttr = 
		[NSMutableDictionary dictionaryWithDictionary:[self typingAttributes]];
		
		[typingAttr removeObjectForKey:NSForegroundColorAttributeName];
		[typingAttr removeObjectForKey:NSBackgroundColorAttributeName];

		[self setTypingAttributes:typingAttr ];
		
		
	}
	
	
}


-(NSAttributedString*)convertAttributedStringForCopying:(NSAttributedString*)originalAttributedString useGraphic:(BOOL)useGraphic
{

	return [self ndaliasProofAttributedString:originalAttributedString];
	
}



#pragma mark -
#pragma mark Paste


-(NSAttributedString*)archiveAttributedStringWithPasteboard:(NSPasteboard*)pb
{
	
	// convert pb to data
	NSArray* types= [pb types];
	NSMutableDictionary* pbDic = [NSMutableDictionary dictionary];
	int hoge;
	for( hoge = 0; hoge < [types count]; hoge++ )
	{
		NSString* type = [types objectAtIndex:hoge];
		
		////NSLog(@"Writing %@",type);
		
		NSData* data = [pb dataForType:type];
		
		if( data != nil )
			[pbDic setObject:data forKey:type];
	}
	
	
	NSData* fwdata = [NSKeyedArchiver archivedDataWithRootObject:pbDic ];
	
	
	
	NSBundle* myBundle;
	myBundle = [NSBundle bundleForClass:[self class]];
	NSString* path;
	
	path = [myBundle pathForResource:@"dropanything" ofType:@"tiff"];
	
	NSData* _icon = [[[NSData alloc] initWithContentsOfFile:path] autorelease];
	NSFileWrapper* fw = [[NSFileWrapper alloc] initRegularFileWithContents:_icon ];
	[fw autorelease];
	
	[fw setFilename:@"dropanythingitem.tiff"];
	[fw setPreferredFilename:[fw filename]];//self rename
		
		
		NSTextAttachment* ta = [[NSTextAttachment alloc] initWithFileWrapper:fw];
		[ta autorelease];
		
		
		NSMutableAttributedString* as = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:ta]];
		[as autorelease];
		
		
		//create archive
		[as addAttribute:DropAnythingAttributeName
				   value:fwdata range:NSMakeRange(0,1)]; 
		
		
		
		//create iamge
		
		
		/*
		 NSImage* image = 
		 [as addAttribute:DropAnythingDragImageAttributeName
					value:draggingImage range:NSMakeRange(0,1)]; 
		 
		 */
		
		//create tooltip
		NSString* str = [pb stringForType:NSStringPboardType];
		
		
		if( str == nil ) str = @""; 
		
		
		
		[as addAttribute:NSToolTipAttributeName
				   value:str range:NSMakeRange(0,1)]; 

		return as;
}

-(IBAction)pasteAsArchiveActionMethod:(id)sender
{
	NSPasteboard* pb = [NSPasteboard generalPasteboard];
	NSAttributedString* as = [self archiveAttributedStringWithPasteboard:pb];		
	
	[self pasteText:as];
		
}



-(void)pasteAsArchive:(id <NSDraggingInfo>)sender
{
	    NSPasteboard *pb = [sender draggingPasteboard];
	//	//NSLog(@"modDropAnything");
	
	NSAttributedString* as = [self archiveAttributedStringWithPasteboard:pb];		
		//////// prepared as "as"
		
	
	
	
		NSRange rangeToBeDelete = {0,0};
		//delete original
		if( [sender draggingSource] == self )
		{
			// delete original if necessary.
			int modKey = GetCurrentKeyModifiers( );
			BOOL copyFlag = ( (modKey | 1024) == (2048 | 1024)  );
			if( ! copyFlag )
			{
				
				rangeToBeDelete = [self selectedRange];
				
			}		
			
		}
		
		
		
		
		NSPoint aPoint = [sender draggingLocation]; //point
		NSRange insertedRange = NSMakeRange([self charIndexAtPoint:aPoint],0);
		
		if( insertedRange.location == NSNotFound ) insertedRange.location = [[[self textStorage] string] length];
		
		// do nothing if dropped onto originally selected range
		if( ( rangeToBeDelete.location <= insertedRange.location )
			&& ( insertedRange.location <= NSMaxRange(rangeToBeDelete) ) 
			&& rangeToBeDelete.length > 0 )
		{
			return ;
		}
		
		[self setSelectedRange:insertedRange];
		
		
		unsigned _oldLength = [[self textStorage] length];
		
		[self pasteText:as];
		
		unsigned _newLength = [[self textStorage] length];
		unsigned changeInLength = _newLength - _oldLength;
		
		insertedRange.length = changeInLength;
		
		if( rangeToBeDelete.location < insertedRange.location )
		{
			insertedRange.location -= rangeToBeDelete.length;
		}else
		{
			rangeToBeDelete.location += insertedRange.length;
		}
		[self setSelectedRange:rangeToBeDelete];
		[self pasteText:@""];
		
		
		[self setSelectedRange:insertedRange];
		
		//[self  scrollRangeToVisible:insertedRange];
		
		
		
		
		
		//DropAnythingAttributeName 、
		//fwdata = [NSKeyedArchiver archivedDataWithRootObject:pbDic ]
		//を値として
		
}

-(BOOL)pastePicture:(id <NSDraggingInfo>)sender
{
	//NSLog(@"paste picture");
	    NSPasteboard *pboard = [sender draggingPasteboard];
	BOOL flag = YES;
//	int hoge;
	
	
	//**** drop **** write as picture
	
	if( NDAliasToBeDropped != nil  )
	{


		NSURL* dropLocation = [NSURL fileURLWithPath:[NDAliasToBeDropped path] ];
		
		//NSLog(@"promised file path %@",[NDAliasToBeDropped path]);
		
		if ( [[pboard types] containsObject:NSFilesPromisePboardType] ) {
			NSArray *filenames = [sender
                namesOfPromisedFilesDroppedAtDestination:dropLocation];
			// Perform operation using the files’ names, but without the
			// files actually existing yet
			
			//NSLog(@"NSFilesPromisePboardType");
			
			if( filenames == nil || [filenames count] == 0 )
			{
				
				NSData* tiffData = [pboard dataForType:NSTIFFPboardType ];
				flag = [FileUtil dropTIFF:tiffData  toPath: [NDAliasToBeDropped path] ];	
			}
		}
		
		else
		{
			
			
			NSData* tiffData = [pboard dataForType:NSTIFFPboardType ];
			flag = [FileUtil dropTIFF:tiffData  toPath: [NDAliasToBeDropped path] ];
			
		}
		
		
		NDAliasToBeDropped = NULL;
		[[self window] restoreCachedImage];
		[[self window] flushWindow];
		[[self window] display];
		
		//NSLog(@"bookmark write");
		if( flag == YES )
			[[SoundManager sharedSoundManager] playSoundWithName:@"exportSound"];
		
		
		return flag;
		
	}
	
	
	
	
	
	
	NSRange rangeToBeDelete = {0,0};
	//delete original
	if( [sender draggingSource] == self )
	{
		// delete original if necessary.
		int modKey = GetCurrentKeyModifiers( );
		BOOL copyFlag = ( (modKey | 1024) == (2048 | 1024)  );
		if( ! copyFlag )
		{
			
			rangeToBeDelete = [self selectedRange];
			
		}		
		
	}
	
	
	
	NSPoint aPoint = [sender draggingLocation]; //point
	NSRange insertedRange = NSMakeRange([self charIndexAtPoint:aPoint],0);
	
	if( insertedRange.location == NSNotFound ) insertedRange.location = [[[self textStorage] string] length];
	
	// do nothing if dropped onto originally selected range
	if( ( rangeToBeDelete.location <= insertedRange.location )
		&& ( insertedRange.location <= NSMaxRange(rangeToBeDelete) ) 
		&& rangeToBeDelete.length > 0 )
	{
		return NO;
	}
	
	[self setSelectedRange:insertedRange];
	

	
	unsigned _oldLength = [[self textStorage] length];
	
	flag = [self readSelectionFromPasteboard:[sender draggingPasteboard]];
	
	unsigned _newLength = [[self textStorage] length];
	unsigned changeInLength = _newLength - _oldLength;
	
	insertedRange.length = changeInLength;
	
	if( rangeToBeDelete.location < insertedRange.location )
	{
		insertedRange.location -= rangeToBeDelete.length;
	}else
	{
		rangeToBeDelete.location += insertedRange.length;
	}
	[self setSelectedRange:rangeToBeDelete];
	[self pasteText:@""];
	
	
	[self setSelectedRange:insertedRange];
	
	
	return flag;
}

-(BOOL)pasteSafariURL:(id <NSDraggingInfo>)sender
{
	
	    NSPasteboard *pboard = [sender draggingPasteboard];
	
	/*Structure
	(
	 (
	  "http://xtramail.xtra.co.nz/", 
	  "http://www.powerlogix.com/products2/bcg4pismo/index.html"
	  ), 
	 (xtra, "PowerLogix Products - BlueChip G4 \"Pismo\"")
	 )
	
	*/
	NSArray* pbArray = [pboard propertyListForType:@"WebURLsWithTitlesPboardType"]; 
	
	
	//**** drop **** write as url
	
	if( NDAliasToBeDropped != nil  )
	{
		int hoge;
		BOOL flag = YES;
		
		for(hoge = 0; hoge < [[pbArray objectAtIndex:0] count]; hoge++)
		{
			NSString* path = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
			NSString* URLtitle =  [[pbArray objectAtIndex:1] objectAtIndex:hoge];
			
			NSString* targetPath = [NDAliasToBeDropped path];
			flag = [FileUtil dropBookmark:path toPath:targetPath withName:URLtitle ];
			
		}
		
		
		NDAliasToBeDropped = NULL;
		[[self window] restoreCachedImage];
		[[self window] flushWindow];
		[[self window] display];
		
		//NSLog(@"bookmark write");
		if( flag == YES )
			[[SoundManager sharedSoundManager] playSoundWithName:@"exportSound"];
		
		
		return flag;
		
	}
	
	
	
	//get insertion point
	NSPoint aPoint = [sender draggingLocation]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;
	
	//set insertion point
	[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
	
	int hoge;
	for(hoge = 0; hoge < [[pbArray objectAtIndex:0] count]; hoge++)
	{
		NSString* path = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
		NSURL* aURL = [NSURL URLWithString:path];
		NSString* URLtitle =  [[pbArray objectAtIndex:1] objectAtIndex:hoge];
		
		
		if(hoge != 0)
		{
			NSAttributedString* delimiter = [[NSAttributedString alloc] initWithString:@"\n" attributes:NULL];
			[self pasteText:delimiter];
			[delimiter release];
		}
		
		
		//
		if([URLtitle isEqualToString:@""])  URLtitle = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
		
		NSMutableDictionary* aLinkAttr;
		aLinkAttr = [NSMutableDictionary dictionaryWithObject: aURL
													   forKey: NSLinkAttributeName];
		//	[aLinkAttr setObject: [NSColor blueColor]  forKey: NSForegroundColorAttributeName];
		//	[aLinkAttr setObject: [NSNumber numberWithBool: YES]  forKey: NSUnderlineStyleAttributeName];
		[aLinkAttr setObject: path  forKey: NSToolTipAttributeName];
		[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];
		
		
		NSAttributedString* str = [[NSAttributedString alloc] initWithString:URLtitle attributes:aLinkAttr];
		
		[self pasteText:str];
		
		
		//favicon 
		[self addFaviconForLinksInRange:NSMakeRange([self selectedRange].location -[str length],1)];
		
		
		
		[str release];
		
	}
	

	
	return YES;
	
}


-(BOOL)pasteFilenames:(id <NSDraggingInfo>)sender
{
	
	    NSPasteboard *pboard = [sender draggingPasteboard];
	
	
	//NSLog(@"filenamespboard %d",[sender draggingSourceOperationMask]);
	
	//NSFilenamesPboardType
	NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
	
	BOOL success = YES;
	
	//Dropping on NDAlias
	if( NDAliasToBeDropped != NULL )
	{
		
		NSString* op;
		if( [sender draggingSourceOperationMask] == 2  ) 
			op = NSWorkspaceLinkOperation;
		
		else if( [sender draggingSourceOperationMask] == 55 ) 
			op = NSWorkspaceMoveOperation;
		
		else if( [sender draggingSourceOperationMask] == 1 ) 
			op = NSWorkspaceCopyOperation;
		
		else op = NSWorkspaceMoveOperation;
		
		
		if( [(id)sender isKindOfClass:[NSTextView class]] )
			op = NSWorkspaceCopyOperation;
		
		//**** drop *****
		[FileUtil dropFiles:files toPath:[NDAliasToBeDropped path] operation:op];
		
		
		
		
		NDAliasToBeDropped = NULL;
		[[self window] restoreCachedImage];
		[[self window] flushWindow];
		[[self window] display];
		
		return success;
	}
	
	
	
	
	//get insertion point
	NSPoint aPoint = [sender draggingLocation]; //point
	unsigned insertionCharIndex = [self charIndexOn:aPoint];
	if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;
	
	//set insertion point
	[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
	
	int hoge;
	for(hoge = 0; hoge < [files count]; hoge++)
	{
		
		NSString* path =  [files objectAtIndex:hoge];
		//NSURL* aURL = [NSURL fileURLWithPath:path];	
		NSArray *pathArray = [path componentsSeparatedByString:@"/"];
		NSString* filename = [pathArray lastObject];
		pathArray = [path componentsSeparatedByString:@"."];
		//NSString* extension = [pathArray lastObject];
		
		
		NSData* aData;
		NSAttributedString* anAttrStr = [[NSAttributedString alloc] initWithString:filename];			
		aData = [anAttrStr RTFFromRange:NSMakeRange(0,[anAttrStr length]) documentAttributes:NULL];
		[anAttrStr release];
		anAttrStr = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
		filename = [NSString stringWithString:[anAttrStr string]];
		[anAttrStr release];
		
		
		
		
		
		//delimiter
		
		if(hoge != 0)
		{
			NSAttributedString* delimiter = [[NSAttributedString alloc] initWithString:@"\n" attributes:NULL];
			[self pasteText:delimiter];
			[delimiter release];
			insertionCharIndex++;
		}
		
		/*
		 if( ([sender draggingSourceOperationMask] == NSDragOperationLink && [[pboard types] containsObject:NSStringPboardType]) ||
			 ! [[pboard types] containsObject:NSStringPboardType] )
		 */
		if( 1 ) // $##$#$#####
		{
			
			// graphic 
			
			
			NSData* _pictureData; 
			NSImage* pictureImage;
			
			_pictureData = [[[NSData alloc] initWithContentsOfFile:path] autorelease];
			pictureImage = [[[NSImage alloc] initWithData:_pictureData] autorelease];
			
			
			@try {
				_pictureData = [pictureImage TIFFRepresentation];
			}
			@catch (NSException *exception) {
				_pictureData = nil;
			}
			
			
			
			if( _pictureData == NULL | 
				[[[path pathExtension] lowercaseString] isEqualToString: @"pdf"] ||
				 [MOD_PRESSED:@"modLink"]  )
				
			{
				//NSLog(@"make link");
				
				
				//create alias
				NDAlias* anAlias = [NDAlias aliasWithContentsOfFile:path]; //assume finder alias
				if( anAlias == NULL )//if not,
					anAlias = [NDAlias aliasWithPath:path];
				//NSLog(@"path %@", [anAlias path]);
				
				//create icon
				NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:[anAlias path]];
				
				if( iconImage != nil )
				{
					NSData* aData = [iconImage TIFFRepresentation];
					if( aData != nil )
					{
						NSFileWrapper* aWrapper = 
						[[NSFileWrapper alloc] initRegularFileWithContents:aData];
						[aWrapper setFilename:[NSString stringWithFormat:@"%@.tiff", filename]];
						NSTextAttachment* anAttachment = [[[NSTextAttachment alloc]
								initWithFileWrapper:aWrapper] autorelease];
						[[anAttachment fileWrapper] setPreferredFilename:
							[[anAttachment fileWrapper] filename]];//self rename
							NSAttributedString* aStr = [NSAttributedString 
									attributedStringWithAttachment:anAttachment];
							[aWrapper release];
							
							//paste icon
							[self insertText:aStr];
					}
				}
				//insert filename 		
				[self pasteText:filename];
				
				//create link
				NSMutableDictionary* aLinkAttr;
				aLinkAttr = [NSMutableDictionary dictionaryWithObject: anAlias
															   forKey: NSLinkAttributeName];
				//[aLinkAttr setObject: [NSColor blueColor]  forKey: NSForegroundColorAttributeName];
				//[aLinkAttr setObject: [NSNumber numberWithBool: YES]  forKey: NSUnderlineStyleAttributeName];
				[aLinkAttr setObject: path  forKey: NSToolTipAttributeName];
				[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];
				
				[[self textStorage] addAttributes:aLinkAttr range:NSMakeRange(insertionCharIndex , [filename length]+1)];
				insertionCharIndex = insertionCharIndex + [filename length]+1;
				
				
				
				//spacer
				NSAttributedString* spacer = [[NSAttributedString alloc] initWithString:@" \n" attributes:NULL];
				[self pasteText:spacer];
				[spacer release];
				insertionCharIndex++;
				
				
			}else //pasting image
			{ 	
				
				NSData* aData = _pictureData;
				
				NSFileWrapper* aWrapper = [[NSFileWrapper alloc] initRegularFileWithContents:aData];
				[aWrapper setFilename:[NSString stringWithFormat:@"%@.tiff", filename]];
				NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:aWrapper] autorelease];
				[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
					NSAttributedString* aStr = [NSAttributedString attributedStringWithAttachment:anAttachment];
					[aWrapper release];
					
					//paste icon
					[self insertText:aStr];
					
					
					if( [self respondsToSelector:@selector(shrinkGraphicsToWindowWidthInRange:)] )
					{
						[self shrinkGraphicsToWindowWidthInRange:NSMakeRange([self selectedRange].location-[aStr length], [aStr length])];
						
					}
			}
			
			
		}else // link
		{ //in case of text clipping, or  not link
			
			[super readSelectionFromPasteboard:[sender draggingPasteboard]];
			insertionCharIndex = insertionCharIndex + [filename length];
		}
	}
	
	return  YES;	
}

-(BOOL)pasteOtherURL:(id <NSDraggingInfo>)sender
{
	    NSPasteboard *pboard = [sender draggingPasteboard];
	
	NSURL* aURL = [NSURL URLFromPasteboard:pboard];
	
	//get insertion point
	NSPoint aPoint = [sender draggingLocation]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;
	
	//set insertion point
	[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
	
	NSMutableDictionary* aLinkAttr;
	aLinkAttr = [NSMutableDictionary dictionaryWithObject: aURL
												   forKey: NSLinkAttributeName];
	//[aLinkAttr setObject: [NSColor blueColor]  forKey: NSForegroundColorAttributeName];
	//[aLinkAttr setObject: [NSNumber numberWithBool: YES]  forKey: NSUnderlineStyleAttributeName];
	[aLinkAttr setObject: [aURL absoluteString]  forKey: NSToolTipAttributeName];
	[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];
	
	
	NSAttributedString* str = [[NSAttributedString alloc] initWithString:[aURL absoluteString] attributes:aLinkAttr];
	//NSLog(@"path", [str string]);
	
	[self pasteText:str];
	

	//favicon 
	[self addFaviconForLinksInRange:NSMakeRange(insertionCharIndex,[str length])];
	
	
	[str release];
	
	
	// debug
	str = [[NSAttributedString alloc] initWithString:[aURL resourceSpecifier]];
	//NSLog(@"specifier", [str string]);
	[str release];
	
	return  YES;
	
}

-(BOOL)pasteRTFD:(id <NSDraggingInfo>)sender
{
	    NSPasteboard *pboard = [sender draggingPasteboard];
	BOOL success = YES;
	
	//Dropping on NDAlias
	if( NDAliasToBeDropped != NULL ) // *** drop ***
	{
		
		if( [[pboard types] containsObject:NSRTFDPboardType] )
			success = [FileUtil dropRTFD:[pboard dataForType:NSRTFDPboardType]  toPath:[NDAliasToBeDropped path]];
		
		else if( [[pboard types] containsObject:NSRTFPboardType] )
			success = [FileUtil dropRTF:[pboard dataForType:NSRTFPboardType]  toPath:[NDAliasToBeDropped path]];
		
		else if( [[pboard types] containsObject:NSStringPboardType] )
			success = [FileUtil dropPlainText:[pboard stringForType:NSStringPboardType]  toPath:[NDAliasToBeDropped path]];
		
		
		NDAliasToBeDropped = NULL;
		[[self window] restoreCachedImage];
		[[self window] flushWindow];
		[[self window] display];
		
		
		if( success == YES )
			[[SoundManager sharedSoundManager] playSoundWithName:@"exportSound"];
		
		return success;
	}
	
	
	
	
 
	NSRange rangeToBeDelete = {0,0};
	//delete original
	if( [sender draggingSource] == self )
	{
		// delete original if necessary.
		int modKey = GetCurrentKeyModifiers( );
		BOOL copyFlag = ( (modKey | 1024) == (2048 | 1024)  );
		if( ! copyFlag )
		{
			
			rangeToBeDelete = [self selectedRange];
			
		}		
		
	}
	
	
	
	
	NSPoint aPoint = [sender draggingLocation]; //point
	NSRange insertedRange = NSMakeRange([self charIndexAtPoint:aPoint],0);
	
	if( insertedRange.location == NSNotFound )
		insertedRange.location = [[[self textStorage] string] length];
	
	
	// do nothing if dropped onto originally selected range
	if( ( rangeToBeDelete.location <= insertedRange.location )
		&& ( insertedRange.location <= NSMaxRange(rangeToBeDelete) ) 
		&& rangeToBeDelete.length > 0 )
	{
		return NO;
	}
	
	[self setSelectedRange:insertedRange];
	
	
	unsigned _oldLength = [[self textStorage] length];
	
	success = [self readSelectionFromPasteboard:[sender draggingPasteboard]];
	
	
	unsigned _newLength = [[self textStorage] length];
	unsigned changeInLength = _newLength - _oldLength;
	
	insertedRange.length = changeInLength;
	
	if( rangeToBeDelete.location < insertedRange.location )
	{
		insertedRange.location -= rangeToBeDelete.length;
	}else
	{
		rangeToBeDelete.location += insertedRange.length;
	}
	[self setSelectedRange:rangeToBeDelete];
	[self pasteText:@""];
	
	
	[self setSelectedRange:insertedRange];
	
	//[self  scrollRangeToVisible:insertedRange];
	
	return success;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
	
	
    NSPasteboard *pboard = [sender draggingPasteboard];
//	BOOL returnStatus = NO;
	
			
	//NSLog(@"%d", 	GetCurrentKeyModifiers( ));
	//
	//
	// command 256
	// shift 512
	// command+shift 256+512
	//
	// drop anything
	//
	
	
	if(  [MOD_PRESSED: @"modDropAnything"]    ) 
	{

		[self pasteAsArchive:sender];
		return YES;
	}
	
	
	

	

	
	
	//NSLog( @"types %@",[[pboard types] description] );
//NSLog(@"Drag style %d",[sender draggingSourceOperationMask]);
	

	
	if( [[pboard types] containsObject:NSTIFFPboardType] && 
		! [[pboard types] containsObject:DROP_ANYTHING_PBTYPE] )
	{

		return [self pastePicture:sender];

	}
	else if([[pboard types] containsObject:@"WebURLsWithTitlesPboardType"] 
			&& ! [[pboard types] containsObject:DROP_ANYTHING_PBTYPE] )  // safai  url
	{
		return [self pasteSafariURL:sender];
		
		
	}else if([[pboard types] containsObject:NSFilenamesPboardType]
			 && ! [[pboard types] containsObject:DROP_ANYTHING_PBTYPE] )  // file
	{

		return [self pasteFilenames:sender];
		
		
	}else if([[pboard types] containsObject:@"Apple URL pasteboard type"]
			 && ! [[pboard types] containsObject:DROP_ANYTHING_PBTYPE]  ) // basic url
	{
		return [self pasteOtherURL:sender];
		
		
	}else //rtfd
	{
		/*
		if( [[PREF modPasteAsPlainText] isPressed] )
		{
			[self pasteText:[pboard stringForType:NSStringPboardType]];
			return YES;
		}*/
		

		return [self pasteRTFD:sender];
		
	}
	
	
	return NO;
	
}

- (void)concludeDragOperation:(id <NSDraggingInfo>)sender
{
	if( sender != self )
	{
		[[self window] makeKeyAndOrderFront:self];
		[[self window] makeFirstResponder:self];
		
	}
}

-(NSMutableAttributedString*)ndaliasProofAttributedString:(NSAttributedString*)orgAttr
{
	/// 1 expand fileAttachmentCell
	
	NSMutableAttributedString* attr = [[[NSMutableAttributedString alloc] 
				initWithAttributedString: orgAttr] autorelease];
	
	NSRange range = NSMakeRange(0,[[orgAttr string] length]);
	NSRange effectRange;
	unsigned hoge;
	for( hoge = range.location; hoge < NSMaxRange(range);  )
	{
		range = NSMakeRange(0,[[attr string] length]);
		
		//NSLog(@"%d",hoge);
		
		id something = [attr attribute:NSAttachmentAttributeName
							   atIndex:hoge
				 longestEffectiveRange:&effectRange inRange:range];
		
		
		if( something != nil )
		{
			if( [[something attachmentCell] isKindOfClass:[FileEntityTextAttachmentCell class]]||
				[[something attachmentCell] isKindOfClass:[AliasAttachmentCell class]])
			{
				id cell = [something attachmentCell];
				
				if( [cell showButtonTitle] )
				{
					NSString* targetPath = [[attr attribute:NSLinkAttributeName
												   atIndex:hoge
									 longestEffectiveRange:nil inRange:range] path];
					
					NSString* filename = [targetPath lastPathComponent];
					
					NSDictionary* attrDict = [NSDictionary dictionaryWithObjectsAndKeys:
						[NSFont boldSystemFontOfSize:11],NSFontAttributeName ,
						[NSURL fileURLWithPath: targetPath] ,NSLinkAttributeName, NULL ];
					
					NSAttributedString* insertingAttr;
					

					NSTextAttachmentCell* newCell = 
						[[[NSTextAttachmentCell alloc] initImageCell: [cell image] ] autorelease];
					NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
					[wrapper setFilename:@"image.tiff"];
					[wrapper setPreferredFilename:[wrapper filename]];
					[wrapper setIcon: [cell image]];
					//add files
					NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];					
					[anAttachment setAttachmentCell:newCell]; //convert

					
					
					insertingAttr = [NSAttributedString attributedStringWithAttachment:anAttachment ];
					[attr replaceCharactersInRange:NSMakeRange(hoge,1)
							  withAttributedString:insertingAttr];

					
					insertingAttr = [[[NSAttributedString alloc] 
			initWithString:filename attributes:attrDict ] autorelease];
					[attr insertAttributedString:insertingAttr atIndex:hoge+1];
				}
			}
			
		}
		hoge = NSMaxRange(effectRange);
		
	}
	

	
	
	
	
	///expand link
	
	
	range = NSMakeRange(0,[[attr string] length]);
	for( hoge = range.location; hoge < NSMaxRange(range);  )
	{
		
		id something = [attr attribute:NSLinkAttributeName
							   atIndex:hoge
				 longestEffectiveRange:&effectRange inRange:range];
		
		
		if( something != nil )
		{
			if( [something isKindOfClass:[NDAlias class]] )
			{
				[attr addAttribute:NSLinkAttributeName
							 value:[NSURL fileURLWithPath: [something path]] 
							 range:effectRange];
			}
				
		}
		hoge = NSMaxRange(effectRange);

	}
	
	return attr;
	
}




#pragma mark -
#pragma mark Favicon

-(void)addFaviconForLinksInRange:(NSRange)range
{
	NSRange effectiveRange;
	unsigned hoge;
	for( hoge = range.location; hoge < NSMaxRange( range );     )
	{
		id link = [[self textStorage] attribute:NSLinkAttributeName atIndex:hoge longestEffectiveRange:&effectiveRange inRange:[self fullRange]];
		
		
		if( link != NULL  &&  ! [link isKindOfClass: [NDAlias class]]   )
		{

			[faviconQue addObject:link];
			
			//if( [faviconQue count] == 1 )

			
			////
			
			
			//create image
			NSData* aData = [defaultFavicon TIFFRepresentation];
			
			NSFileWrapper* aWrapper = [[NSFileWrapper alloc] initRegularFileWithContents:aData];
			[aWrapper setFilename:@"favicon.tiff"];
			NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:aWrapper] autorelease];
			[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
			NSMutableAttributedString* aStr = (NSMutableAttributedString*)[NSMutableAttributedString attributedStringWithAttachment:anAttachment];
			[aWrapper release];
			
			[aStr addAttribute:NSLinkAttributeName
						 value:link 
						 range:NSMakeRange(0, [[aStr string] length]) ];
			//paste icon
			//NSLog(@"paste default");
			[[self textStorage] insertAttributedString:aStr atIndex:effectiveRange.location];
		
			effectiveRange.length ++;
			
			//NSLog(@"link ");
			
			[[self textStorage] addAttribute:@"FaviconRequest" value:link range:effectiveRange];
			//NSLog(@"favicon request %@",[  link absoluteString]  );
			
			
			[self startQue];

		}
		
		hoge = NSMaxRange( effectiveRange );
	}
	
}


-(void)receivedFavicon:(NSImage*) image  forURL:(NSURL*)originalUrl
{
	//NSLog(@"receivedFavicon %@",[originalUrl absoluteString]);

	if( image != nil )
	{
		NSRange range = [self fullRange];
		NSRange effectiveRange;
		unsigned hoge;
		for( hoge = range.location; hoge < NSMaxRange( range );     )
		{
			id link = [[self textStorage] attribute:@"FaviconRequest" atIndex:hoge longestEffectiveRange:&effectiveRange inRange:[self fullRange]];
			
			if( link != NULL )
				if( [[link absoluteString] isEqualToString:[originalUrl absoluteString]] )
			{
				[self requestFavicon:link ];	
				[[self textStorage] removeAttribute:@"FaviconRequest" range:effectiveRange];
				
				
				//create image
				NSData* aData = [image TIFFRepresentation];
				
				NSFileWrapper* aWrapper = [[NSFileWrapper alloc] initRegularFileWithContents:aData];
				[aWrapper setFilename:@"favicon.tiff"];
				NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:aWrapper] autorelease];
				[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
				NSMutableAttributedString* aStr = (NSMutableAttributedString*)[NSMutableAttributedString attributedStringWithAttachment:anAttachment];
				[aWrapper release];
				
				[aStr addAttribute:NSLinkAttributeName
							 value:originalUrl 
							 range:NSMakeRange(0, [[aStr string] length]) ];
				
				
				/// replace old favicon to new favicon
				
				unsigned piyo;
				for( piyo = effectiveRange.location; piyo < NSMaxRange(effectiveRange) ; piyo ++ )
				{
					id ta = [[self textStorage] attribute:NSAttachmentAttributeName
								  atIndex:piyo 
										   effectiveRange:nil];
					if( [[[ta fileWrapper] filename] isEqualToString:@"favicon.tiff"] )
					{
					
				
				
			//	[[self textStorage] insertAttributedString:aStr atIndex:hoge];
				[[self textStorage] replaceCharactersInRange:NSMakeRange(piyo,1 )
										withAttributedString:aStr];
					}

				
				}
				
			}
			
			hoge = NSMaxRange( effectiveRange );
		}
		
	}
	
}
-(void)startQue
{
	if( [faviconQue count] == 0 ) return;
	
	NSURL* url = [faviconQue objectAtIndex:0];
	
	[self requestFavicon:url];
}

-(void)requestFavicon:(NSURL*)url
{
	//NSLog(@"requestFavicon");

		
		
	NSURLRequest* urlrequest = [NSURLRequest requestWithURL:url
								   cachePolicy:NSURLRequestReloadIgnoringCacheData
							   timeoutInterval:10.0 ];

	
	[aWebView setFrameLoadDelegate:self];
	[[aWebView mainFrame] loadRequest:urlrequest];
	
}

- (void)webView:(WebView *)sender didReceiveIcon:(NSImage *)image forFrame:(WebFrame *)frame
{
	//NSLog(@"didReceiveIcon");

	

		
	NSURL* originalUrl = [[[frame dataSource] initialRequest] URL];
	[sender stopLoading:self];

	[self receivedFavicon:(NSImage*) image  forURL:(NSURL*)originalUrl ]; 
	
	[faviconQue removeObject:originalUrl];
	
	if( [faviconQue count] == 0 ) return;
	else
	{
		NSURL* url = [faviconQue objectAtIndex:0];
		
		[self requestFavicon:url];
		
	}
	
}

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
	//NSLog(@"didFailLoadWithError");

	//[sender stopLoading:self];
	NSURL* originalUrl = [[[frame dataSource] initialRequest] URL];
	[self receivedFavicon:nil  forURL:(NSURL*)originalUrl ];
}
- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	

}




@end
