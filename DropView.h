//
//  DropView.h
//  sticktotheedge
//
//  Created by __Name__ on 06/05/07.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DragButton.h"



@interface DropView : NSButton {

	int style;
	int screenID;
	//NSCursor* cursorLeft;
	//NSCursor* cursorRight;
	//NSCursor* cursorBottom;
	
	BOOL dragging;
	BOOL onEdge;
	NSPoint currentCursor;
	NSTrackingRectTag	trackingRect;
	
	BOOL draggingEnteredSecondFlag;
	BOOL createNewMemo;
	

}
- (id)initWithFrame:(NSRect)frame ;
-(void)resetTrackingRect;
- (void)drawRect:(NSRect)aRect;
-(void)setScreenID:(int)aScreenNum;
-(int)screenID;
-(void)setStyle:(int)st;
-(int)style;
-(void)setCreateNewMemo:(BOOL)flag;
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender;
- (void)draggingExited:(id <NSDraggingInfo>)sender;
- (void)concludeDragOperation:(id <NSDraggingInfo>)sender;
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender;
-(void)bringToFront;
-(void)moveToBackmost;
-(void)edgeActivatingLoop;
- (void)mouseEntered:(NSEvent *)theEvent;
-(void)mouseEnteredMain;
-(void)	createNewMemoIsNoButUnhideMe;
-(void)showNewMemoCursor ;
- (void)mouseExited:(NSEvent *)event;
- (void)mouseDown:(NSEvent *)theEvent;

@end
