//
//  SearchAgent.m
//  SpotInside
//
//  Created by __Name__ on 06/11/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SearchAgent.h"
#import "ThreadWorker.h"

@implementation SearchAgent

- (id)initWithEndTarget:(id)target andSelector:(id)selector keys:(NSArray*)keys
// keys ... is dynamically used by this agent
{
    self = [super init];
    if (self) {
		

		
		endTarget = target;
		endSelector = selector;
		keysToRetrieve = keys;
		
		
		lock = [[NSLock alloc] init];
		cancel =NO;
		
		searchCount = 0;
		
    }
    return self;
}

-(void)end
{
	[self stopSearch];
	endTarget = nil;
	endSelector = nil;
	keysToRetrieve = nil;
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];

}

-(int)searchCount
{
	return searchCount;
}

-(void)dealloc
{

	[lock release];
	NSNotificationCenter *nf = [NSNotificationCenter defaultCenter];

	[super dealloc];
	
	
}

-(NSString*)escape:(NSString*)str
{
	NSMutableString* mstr = [[[NSMutableString alloc] initWithString:str] autorelease];
	
	[mstr replaceOccurrencesOfString:@"\"" withString:@"\\\""
		   options:1
			 range:NSMakeRange(0,[mstr length])];

	[mstr replaceOccurrencesOfString:@"*" withString:@"\\*"
							 options:1
							   range:NSMakeRange(0,[mstr length])];

	return (NSString*)mstr;
}



#pragma mark -

-(NSMutableArray*)search:(NSString*)query format:(NSString*)format scope:(NSArray*)scope cancelPreviousSearch:(BOOL)cancelFlag
/*
	検索式を与えられると、スレッド内で検索して検索結果を返す

 */

{
	
	if( cancelFlag == YES)
		[self stopSearch];

	
	cancel = NO;
	
	

	NSPredicate *predicateToRun = nil;
	
	query = [self escape:query];
	
	
	
	NSMutableString *predicateFormat = [NSMutableString stringWithString: format];
	[predicateFormat replaceOccurrencesOfString:@"%@"
									 withString:query
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0,[predicateFormat length])];
	

	 _query = MDQueryCreate (
							NULL,
							predicateFormat,
							nil, // value list
							[NSArray arrayWithObject:kMDQueryResultContentRelevance] // sort
							);	
	
	
	
	
	if( scope != nil && [scope count] != 0 )
	{
		MDQuerySetSearchScope ( _query, scope,  0  );
		
	}else
	{
		MDQuerySetSearchScope ( _query, [NSArray arrayWithObject:kMDQueryScopeComputer ],  0  );
	}
	

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finish:) name:kMDQueryDidFinishNotification object:_query];
	
	
	
	MDQueryExecute ( _query, kMDQuerySynchronous );
	searchCount++;
	
	//CFRelease(_query);

	
	////NSLog(@"MDQueryExecute");

}


-(void)finish:(NSNotification*)notif
{
	
	MDQueryRef finishedQuery = [notif object];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:kMDQueryDidFinishNotification
												  object:finishedQuery];
	
	[ThreadWorker  workOn:self
			 withSelector:@selector(processFoundData:) 
			   withObject:notif
		   didEndSelector:@selector(finishFinished:)];  // Optional
}

-(void)finishFinished:(NSMutableArray*)findResult
{
	// findResult retun
	
	if( cancel == YES) findResult = [NSMutableArray array];
	
	if( [endTarget respondsToSelector:endSelector])
		[endTarget performSelector:endSelector withObject:findResult afterDelay:0.2 ];
	
	cancel = NO;
	
	searchCount--;

}

-(NSMutableArray*)processFoundData:(NSNotification*)notif
{
	
	MDQueryRef finishedQuery = [notif object];

	
	MDItemRef miref;	
	CFIndex idx;
	
	NSMutableArray* findResult = [NSMutableArray array];
	// dictionaryからなるarray構造
	// dictionary には、kMDQueryResultContentRelevance, kMDItemPath
	
	idx = MDQueryGetResultCount(finishedQuery);
	
	
	if( idx == 0 ){
		[endTarget performSelector:endSelector withObject:[NSArray array] afterDelay:0.2 ];

	
		
		
	}else
	{
	
	
	//
	
	

	float max = 0.0;
	
	CFIndex hoge;
	for( hoge = 0; hoge < idx; hoge++ )
	{
		[lock lock];
		if( cancel == YES )
		{
			[lock unlock];

			return [NSMutableArray array];
		}
		
		
		miref = MDQueryGetResultAtIndex( finishedQuery, hoge);
		
		
		if( miref != nil )
		{
			NSNumber* score = (NSNumber*)MDQueryGetAttributeValueOfResultAtIndex (
								finishedQuery, kMDQueryResultContentRelevance, hoge  );
			NSNumber* score2;
			
			if( score != nil )
			{
				
				score2 = [NSNumber numberWithFloat:[score floatValue]];
				
				CFRelease(score);
			}else
			{
				
				score2 = [NSNumber numberWithFloat:0];
			}
			
			
			
			
			NSMutableDictionary* newDictionary = [self extractAttributesFrom:miref];

			
			[newDictionary setObject:score2 forKey:kMDQueryResultContentRelevance];
			

			
			[findResult insertObject: newDictionary  atIndex:0];
			
			
			float x = [score2 floatValue];
			if( max < x ) max = x;
			
			CFRelease(miref);
		}
		
		
		[lock unlock];

	}
	

	
	//regulate score
	
	
	for( hoge = 0; hoge < [findResult count]; hoge++ )
	{		
		[lock lock];
		if( cancel == YES )
		{
			[lock unlock];

			return [NSMutableArray array];
		}
		
		
		
		float x = [[[findResult objectAtIndex:hoge] objectForKey:kMDQueryResultContentRelevance] floatValue];
		
		x = x / max;
		
		
		[[findResult objectAtIndex:hoge] setObject:[NSNumber numberWithFloat:x] forKey:kMDQueryResultContentRelevance];
		
		[lock unlock];

	}
	
	
	

	}
	
	
	return findResult;
	
	

}

-(NSMutableDictionary*)extractAttributesFrom:(MDItemRef)ref
{
	NSMutableDictionary* newDictionary;
	
	NSMutableArray* tempAllKeys = [NSMutableArray arrayWithArray: keysToRetrieve];
	if( ! [tempAllKeys containsObject:kMDItemPath] )
		[tempAllKeys addObject:kMDItemPath];
	
	
	if( ! [tempAllKeys containsObject:kMDItemContentType] )
		[tempAllKeys addObject:kMDItemContentType];
	
	
	id value = MDItemCopyAttributes(ref, tempAllKeys );
	
	if( value != nil )
	{
		newDictionary = [NSMutableDictionary dictionaryWithDictionary: value];
		CFRelease(value);

	}else
	{
		newDictionary = [NSMutableDictionary  dictionary];
	}
	
	
	return newDictionary;
}




-(void)stopSearch
{
	[lock lock];
	
	if( _query != nil )
	{
		MDQueryStop(_query);
		////NSLog(@"stop");
		cancel = YES;
	}
	
	
	[lock unlock];

	
	//	[[NSNotificationCenter defaultCenter] removeObserver:self  name:kMDQueryDidFinishNotification object:nil];
	
//	MDQueryStop (_query  );	
	
}


@end
