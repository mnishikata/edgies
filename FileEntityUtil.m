//
//  FileEntityUtil.m
//  fileEntityTextView
//
//  Created by Masatoshi Nishikata on 06/08/14.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

//http://cocoa.karelia.com/Foundation_Categories/NSFileManager__Reso.m Resolve an alias

// todo


#import "FileEntityUtil.h"
#import "EdgiesLibrary.h"

#import "NDAlias.h"


@implementation FileEntityUtil


+(void)initialize
{// NSApp

	

    if ( self == [FileEntityUtil class] ) 
	{
		if( ! [[NSFileManager defaultManager] fileExistsAtPath: FILE_FOLDER ] )
			[[NSFileManager defaultManager] createDirectoryAtPath: FILE_FOLDER attributes:NULL];
		
		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		[ud setObject: UNIQUENAME forKey:SESSION_ID];
		/*
		 
		 Ëµ∑ÂãïÊôÇ„Å´Âëº„Çì„Åß‰∏ã„Åï„ÅÑ 
		 
		 */    
	}
	
}
+(NSString*)sessionID
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	

	
	return [ud stringForKey:SESSION_ID];
}

+(NSDictionary*)getFileEntityInformationAtPath:(NSString*)folderPath
{
	NSString* path = [folderPath stringByAppendingPathComponent: SETTING_FILE];
	
	NSDictionary* dic = [[[NSDictionary alloc] initWithContentsOfFile:path] autorelease];
	
	return dic;
}

+(void)setFileEntityInformation:(NSDictionary*)dic atPath:(NSString*)folderPath
{
	NSString* path = [folderPath stringByAppendingPathComponent: SETTING_FILE];
	
	[dic writeToFile:path atomically:YES];
}

+(void)lockInformationFileAtPath:(NSString*)folderPath
{
	
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithBool:YES], INFORMATION_LOCK,
		[FileEntityUtil sessionID] , SESSION_ID, nil];
	
	[FileEntityUtil setFileEntityInformation:dic atPath:folderPath];
	
}

+(void)unlockInformationFileAtPath:(NSString*)folderPath
{
	NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithBool:NO], INFORMATION_LOCK,
		[FileEntityUtil sessionID], SESSION_ID, nil];
	
	
	[FileEntityUtil setFileEntityInformation:dic atPath:folderPath];
}


+(BOOL)informationIsLockedAtPath:(NSString*)folderPath
{
	NSDictionary* dic = [FileEntityUtil getFileEntityInformationAtPath:folderPath];
	
	if( dic == nil ) return NO;
	
	BOOL lock = [[dic objectForKey:INFORMATION_LOCK] boolValue];
	NSString* sessionID = [dic objectForKey:SESSION_ID];
	
	
	if( lock == YES && [sessionID isEqualToString: [FileEntityUtil sessionID] ] )
	{
	//	NSLog(@"YES");
		return YES;
	}
	//NSLog(@"NO");

	return NO;
}

+(NSArray*)contentsInFolder:(NSString*)path //exclude @".DS_Store"
{
	
	if( path == nil ) return nil;
	
	NSMutableArray* array = [NSMutableArray arrayWithArray: 
		[[NSFileManager defaultManager] directoryContentsAtPath:path]];
	
	[array removeObject: @".DS_Store"];
	[array removeObject: SETTING_FILE ];
	
	return array;
}

+(void)deallocate
{

	
	NSArray* contents = [[NSFileManager defaultManager] directoryContentsAtPath: FILE_FOLDER ];
	int hoge;
	for( hoge = 0; hoge < [contents count]; hoge++)
	{
		NSString* file = [contents objectAtIndex:hoge];
		file = [FILE_FOLDER stringByAppendingPathComponent:file];
		
		if( [file hasSuffix:MY_FOLDER_SUFFIX ] )
		{
			NSArray* subContents =  [FileEntityUtil contentsInFolder:file];
			if( [subContents count] == 0 )
			{
				[FileEntityUtil unlockFileFolder:file];

				[[NSFileManager defaultManager]  removeFileAtPath:file  handler:nil];

			}
			
		}
	}
	

	
}

+ (NSString*) pathResolved:(NSString*) inPath
{
	CFStringRef resolvedPath = nil;
	CFURLRef	url = CFURLCreateWithFileSystemPath(NULL /*allocator*/, (CFStringRef)inPath, kCFURLPOSIXPathStyle, NO /*isDirectory*/);
	if (url != NULL) {
		FSRef fsRef;
		if (CFURLGetFSRef(url, &fsRef)) {
			Boolean targetIsFolder, wasAliased;
			if (FSResolveAliasFile (&fsRef, true /*resolveAliasChains*/, &targetIsFolder, &wasAliased) == noErr && wasAliased) {
				CFURLRef resolvedurl = CFURLCreateFromFSRef(NULL /*allocator*/, &fsRef);
				if (resolvedurl != NULL) {
					resolvedPath = CFURLCopyFileSystemPath(resolvedurl, kCFURLPOSIXPathStyle);
					CFRelease(resolvedurl);
				}
			}
		}
		CFRelease(url);
	}
	return (NSString *)resolvedPath;
}

+ (BOOL) isAliasPath:(NSString *)inPath
{
	return [FileEntityUtil pathResolved:inPath] != nil;
}

+ (NSString*) resolveAliasPath:(NSString*) inPath
{
	NSString *resolved = [FileEntityUtil pathResolved:inPath];
	return (nil != resolved) ? resolved : inPath;
}


+(BOOL)fileAtTargetPathIsKagemusha:(NSString*)path
{
	
	NSData* aliasfileData = [[[NSData alloc]  initWithContentsOfFile:path] autorelease];
	if( aliasfileData == nil ) return NO;
	
	NSString* str = [[[ NSString alloc] initWithData:aliasfileData
										   encoding:NSUTF8StringEncoding] autorelease];
	if( str == nil ) return NO;
	
	if( [str isEqualToString:KAGEMUSHA_STRING ] ) return YES;
	
	return NO;
}

+(BOOL)createKagemushaAtPath:(NSString*)destination
{
	NSData* aliasfileData = [KAGEMUSHA_STRING dataUsingEncoding:NSUTF8StringEncoding];
	
	
	return [aliasfileData writeToFile:[destination stringByAppendingPathExtension:KAGEMUSHA_EXT]
					atomically:YES];
}

+(void)unlockFileFolder:(NSString*)folderPath
{
	NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
	[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:folderPath];

}
+(void)lockFileFolder:(NSString*)folderPath
{
	NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:NSFileImmutable];
	[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:folderPath];
	
}

/*
+(NSString*)aliasToString:(NDAlias*)alias
{
	
	
	NSData* aliasData;
	NSString* aliasStr;
	
	@try {
		aliasData = [NSKeyedArchiver archivedDataWithRootObject:alias];
		aliasStr = [aliasData description];

	}
	@catch (NSException *exception) {
		aliasStr = nil;
	}
	
	NSLog(@"alias to string %@",aliasStr);

	
	return aliasStr;
}

+(NDAlias*)stringToAlias:(NSString*)string
{
	NSLog(@"string to alias %@",string);
	NSData* aliasData;
	NDAlias* alias;
	@try {
		aliasData = [string propertyList];
		alias = [NSKeyedUnarchiver unarchiveObjectWithData: aliasData];
	}
	@catch (NSException *exception) {
		alias = nil;
	}
			
				
	
	

	return alias;
}
*/

@end
