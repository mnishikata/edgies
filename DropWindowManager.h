/* DropWindowManager */

#import <Cocoa/Cocoa.h>
#import "DropView.h"

@interface DropWindowManager : NSObject
{
	NSArray*			dropWindows;
	NSTimer*		timer;
	
	NSTimer* callingTabTimer;
	NSTimer* activateTimer;

	NSTimeInterval lastEdgeEntering;
	
	BOOL active;
	
	BOOL edgeEffect;//	= preferenceBoolValueForKey(@"edgeEffect");

	//BOOL edgeHideOffEdge = preferenceBoolValueForKey(@"edgeHideOffEdge");

	BOOL edgeTop;// =  preferenceBoolValueForKey(@"edgeTop");
	BOOL edgeTopSecond;// =  preferenceBoolValueForKey(@"edgeTopSecond");

	BOOL edgeBottom;// =  preferenceBoolValueForKey(@"edgeBottom");
	BOOL edgeBottomSecond;// =  preferenceBoolValueForKey(@"edgeBottomSecond");

	BOOL edgeRight;// =  preferenceBoolValueForKey(@"edgeRight");
	BOOL edgeRightSecond;// =  preferenceBoolValueForKey(@"edgeRightSecond");

	BOOL edgeLeft;// =  preferenceBoolValueForKey(@"edgeLeft");
	BOOL edgeLeftSecond ;//=  preferenceBoolValueForKey(@"edgeLeftSecond");
		
		int alignOption;
		
		float sliderClickDelay;
		
		BOOL edgeActivatingKeySwitch;
		BOOL disableModifierToBringTabs;

}

+(DropWindowManager*)sharedDropWindowManager;
- (id) init ;
-(void)timer;
-(void)callTabs;
-(void)activate;
-(void)activateMain;
-(void)deactivate;
-(void)bringToFront;
-(void)moveToBackmost;
-(void)dealloc;
-(void)updateWindows;
-(NSPanel*)createDropWindowWithFrame:(NSRect)frame dropViewStyle:(int)style createNewMemo:(BOOL)createNewMemo screenID:(int)screenNum;
-(void)tile;
-(void)setupDropWindows;

@end
