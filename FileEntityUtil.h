//
//  FileEntityUtil.h
//  fileEntityTextView
//
//  Created by Masatoshi Nishikata on 06/08/14.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NDAlias.h"

@interface FileEntityUtil : NSObject {

}
+(void)initialize;
+(NSString*)sessionID;
+(NSDictionary*)getFileEntityInformationAtPath:(NSString*)folderPath;
+(void)setFileEntityInformation:(NSDictionary*)dic atPath:(NSString*)folderPath;
+(void)lockInformationFileAtPath:(NSString*)folderPath;
+(void)unlockInformationFileAtPath:(NSString*)folderPath;
+(BOOL)informationIsLockedAtPath:(NSString*)folderPath;
+(NSArray*)contentsInFolder:(NSString*)path ;////exclude @".DS_Store";
+(void)deallocate;
+ (NSString*) pathResolved:(NSString*) inPath;
+ (BOOL) isAliasPath:(NSString *)inPath;
+ (NSString*) resolveAliasPath:(NSString*) inPath;
+(BOOL)fileAtTargetPathIsKagemusha:(NSString*)path;
+(BOOL)createKagemushaAtPath:(NSString*)destination;
+(void)unlockFileFolder:(NSString*)folderPath;
+(void)lockFileFolder:(NSString*)folderPath;
+(NSString*)aliasToString:(NDAlias*)alias;
+(NDAlias*)stringToAlias:(NSString*)string;


@end
