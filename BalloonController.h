//
//  BalloonController.h
//  sticktotheedge
//
//  Created by __Name__ on 06/06/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface BalloonController : NSObject {

	NSWindow* balloonWindow;
	
	
	//NSBitmapImageRep* balloonGlass;
	//NSImage* balloonMask;

	
	id balloonOwner;
}
- (id) init ;
-(NSImage*)balloonWithContents:(NSImage*)contentImage color:(NSColor*)color;
-(void)setAlpha:(float)alpha;
-(void)showBalloonWithImage:(NSImage*)image color:(NSColor*)aColor at:(NSPoint)point sender:(id)sender;
-(void)hideBalloon;

@end
