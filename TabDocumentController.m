#import "TabDocumentController.h"
#import "EdgiesLibrary.h"
#import "ScreenUtil.h"
#import "NSString (extension).h"
#import "NSString+NDCarbonUtilities.h"
#import "MiniDocument.h"
#import "FontWell.h"
#import "DragButton.h"
#import "SoundManager.h"
#import "MNZip.h"
#import "NSData+CocoaDevUsersAdditions.h"

#define DOC_FILENAME @"DOC.edgydata"
#define DOC_BACKUP_FILENAME @"DOC_BACKUP_%d.edgydata"

#define RTFD_FILENAME @"DOC.rtfd"
#define TEXT_FILENAME @"DOC.txt"
#define THUMB_FILENAME @"Thumbnail.tiff"
#define PREVIEW_FILENAME @"Preview.pdf"

#define USE_SYNC

#ifdef USE_SYNC
	#import "SyncManager.h"
	#import <SyncServices/SyncServices.h>
#endif


@implementation TabDocumentController

#pragma mark Basic

+(TabDocumentController*)sharedTabDocumentController
{
	return [[NSApp delegate] tabDocumentController];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		[self awakeFromNib];
	}
	return self;
}
- (NSResponder *)nextResponder
{
	return [[NSApp delegate] nextResponderFor:self];
}

-(void)awakeFromNib
{
	if( !documents )
	{
	untitledCount = 1;
	
	documents = [[NSMutableArray alloc] init];
	
	[self readDocumentsFromPersistentStore];
	
	
	
	autoHide = preferenceBoolValueForKey(@"autoHide")	;
	autoHideTimer = nil;
	sliderHideDelay = (double)preferenceFloatValueForKey(@"sliderHideDelay");

	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(saveDocumentsToPersistentStoreBeforeQuitting) 
												 name:NSApplicationWillTerminateNotification 
											   object:[NSApplication sharedApplication]];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(preferenceChanged)
												 name:PreferenceDidChangeNotification
											   object:nil];
	}
}



-(void)dealloc
{
	if( [autoHideTimer isValid] )
		[autoHideTimer invalidate];
	[autoHideTimer release];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}
-(void)saveDocumentsToPersistentStoreBeforeQuitting
{
	/*
	if( !preferenceBoolValueForKey(@"saveEdgyFiles") ) 
	{
		return;
	}*/
	
	int backupCount = preferenceIntValueForKey(@"backupCount");
	
	unsigned int i, count = [documents count];
	for (i = 0; i < count; i++) {
		
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		MiniDocument * obj = [documents objectAtIndex:i];
	//	[obj save];
		
		
		
		if( [obj name] != nil )
		{	
			NSString* path = [obj filePath];
			
			if( path != nil )
				[obj setFilePath:path]; ///update alias
			
			
			[self saveThisData:[obj documentAttributes] atPath:path defaultName:[obj p_title] document:obj sync:NO backup:(backupCount > 0? YES:NO) backupCount: backupCount];

		}
		
		[pool release];
	}

	
	/// sync
	/*
#ifdef USE_SYNC

	if( preferenceBoolValueForKey(@"syncSwitch") )
	{
		
		[[SyncManager sharedSyncManager] syncDocumentFolder:DOCUMENT_FOLDER pull:NO];

		
	}
#endif*/
}



-(void)readDocumentsFromPersistentStore
{
	/*
#ifdef USE_SYNC
	if( preferenceBoolValueForKey(@"syncSwitch") )
	{
		if( preferenceBoolValueForKey(@"syncDialogue") )
		{
			NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@".Mac Sync",@"")
											 defaultButton:NSLocalizedString(@"Cancel",@"")
										   alternateButton:NSLocalizedString(@"Sync",@"")
											   otherButton:NSLocalizedString(@"",@"")
								 informativeTextWithFormat:NSLocalizedString(@"Edgies is syncing with Sync database. Local Edgy notes may be replaced or deleted by notes on Sync database",@"")];
			int result = [alert runModal];
			
			
			if( result == NSAlertAlternateReturn )
			{
				[[SyncManager sharedSyncManager] syncDocumentFolder:DOCUMENT_FOLDER pull:YES];

			}
			
		}else
		{
			[[SyncManager sharedSyncManager] syncDocumentFolder:DOCUMENT_FOLDER pull:YES];
		}
	}
#endif
	*/
	NSFileManager *fm = [NSFileManager defaultManager];
	
	//seach aplication support folder
	NSArray* contents = [fm subpathsAtPath:DOCUMENT_FOLDER];
	//NSMutableDictionary* documentAttributes = [NSMutableDictionary dictionary];
	
	
	////NSLog(@"%@",[contents description]);
	int hoge;
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

		
		NSString* item = [contents objectAtIndex:hoge];

		@try {

		
		
		
		if( [item hasSuffix:@".edgy"] )
		{
			//NSLog(@"Reading edgy file %@",item);

			NSString* itemPath = [DOCUMENT_FOLDER stringByAppendingPathComponent:item];
			
			NSData* readDocData = nil;
				
			BOOL isDir;
			
			[fm fileExistsAtPath:itemPath isDirectory: &isDir];
			if( isDir )
			{
				
				

			
				NSData *docData = [MNZip readDataInPackage:itemPath forFile: DOC_FILENAME ];
				
				
				if( docData )
				{
					readDocData = docData;
				}
				
			}else  // Read as old version
			{
				readDocData =[[[NSData alloc] initWithContentsOfFile:itemPath] autorelease];

				
				
				
			}
			
			
			if( readDocData != nil )
			{
				
				//NSString* name = [[NSKeyedUnarchiver unarchiveObjectWithData:readDocData] objectForKey:@"name"];
				//[documentAttributes setObject:readDocData forKey:name];
				
				
				
				
				MiniDocument* doc = [[[MiniDocument alloc] init] autorelease];
				[doc setDocumentAttributes:readDocData];
				
				[doc setFilePath:itemPath];
				
				[doc setValue:[NSNumber numberWithBool:NO] forKey:@"isDocumentEdited"];
				[[doc window] setNormalLevel];
				

				
				if( [[doc window] isTornOff] )
				{
					//NSLog(@"isTornOff");
					[[doc window] setAlphaValue:
						preferenceFloatValueForKey(@"sliderOpened") / 100.0
						];
					
					
				}
				else
				{
					
				}
				
				//[(MNTabWindow*) [doc window] redrawWithShadowProperly];
				
				[[doc window] redrawWithShadowProperly];
				if( [doc isHidden] )
					[[doc window] orderOut:self];
				else
					[[doc window] orderFront:self];
				
				
				[documents addObject:doc];
			}
				
			//NSLog(@"Done");

		}
		
		
		}
		@catch (NSException * e) {
			NSString *msg = [NSString stringWithFormat: @"Error occured when reading %@.  This may need to be removed from Edgies document folder... ~/Application Support/Edgies/Documents/   Try reverting in the next window if possible.",item];
			
			
			NSRunAlertPanel(@"Error",msg,@"OK",nil,nil);
			
			NSString* itemPath = [DOCUMENT_FOLDER stringByAppendingPathComponent:item];

			[self revert:itemPath doc:nil];

			NSLog(@"Error occured");
		}
		
		
		[pool release];
	}
	
	/*  When first launching edgies, the text view works strangely. This will avoid that problem
	[[[NSApp delegate] preferenceController] popTemporaryPreferencesValues];
	
	[[[NSApp delegate] preferenceController] apply];
	 MAYBE NOT
	  */
	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];

	
	
}



-(void)preferenceChanged
{
	//NSLog(@"tab document controller 1");

	
	//NSLog(@"tab document controller 2");

	autoHide = preferenceBoolValueForKey(@"autoHide");
	sliderHideDelay = (double)preferenceFloatValueForKey(@"sliderHideDelay");
	//NSLog(@"tab document controller 4");

}

#pragma mark Accessor

-(MiniDocument*)actionForcusedDocument
{
	
	MNTabWindow* returnValue;
	int openCount = 0;
	int hoge;
	for( hoge = 0; hoge < [documents count]; hoge++ )
	{
		id window = [[documents objectAtIndex:hoge] window];
		if( [window isOpened] && ![window isTornOff]  )
		{
			openCount++;
			returnValue = [documents objectAtIndex:hoge];
			
		}
		
	}
	if( openCount == 1 )
	{
		
		return returnValue;
	}
	
	return currentDocument;
	
	
	
}

-(MiniDocument*)currentDocument
{
	return currentDocument;	
}
-(void)setCurrentDocument:(MiniDocument*)object
{
	
	currentDocument = object;
}


-(NSMutableArray*)documents
{
	return documents;	
}

-(NSWindow*)mouseOnVisibleButton
{
	NSArray* _dc = [self visibleDocuments];
	
	int hoge;
	for( hoge = 0 ; hoge < [_dc count]; hoge++ )
	{
		
		
		NSWindow* win = [[_dc objectAtIndex:hoge] window];
		
		
		id tabButton = [win tabButton];
		
		// ||  ||  ||
		
		if( [ tabButton mouseOnButton] )
		{
			return win;
			
		}
	}
	
	return nil;
}


-(NSArray*)visibleDocuments
{
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"isHidden == NO"];
	
	return [documents filteredArrayUsingPredicate:predicate];	
}
-(NSArray*)hiddenDocuments
{
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"isHidden == YES"];
	
	return [documents filteredArrayUsingPredicate:predicate];	
}


-(BOOL)isBusy
{
	return isBusy;
}

-(void)setBusy:(BOOL)flag
{
	
	if( autoHide  )
	{
		[autoHideTimer invalidate];
		[autoHideTimer release];
		
		autoHideTimer = [NSTimer scheduledTimerWithTimeInterval:sliderHideDelay
										 target:self selector:@selector(autoHide) userInfo:nil repeats:NO];
		//NSLog(@"tab document controller 3");
		
		[autoHideTimer retain];
	}
	
	
	
	
	if( isBusy && flag )
		return;
	
	isBusy = flag;
	

}




#pragma mark File Utility

-(MiniDocument*)newDocumentForDropping
{
	
	
	MiniDocument* doc = [[[MiniDocument alloc] init] autorelease];
	[documents addObject:doc];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
	
	
	[self insertHeaderIn:doc];
	

	//set default font
	NSMutableDictionary* attrDic = [NSMutableDictionary dictionary];
	
	id fontvalue = preferenceValueForKey(@"defaultFontName");
	
	NSFont* font = nil;
	
	if( fontvalue != nil )
	{
		font = [NSFont fontWithName:preferenceValueForKey(@"defaultFontName") size:preferenceFloatValueForKey(@"defaultFontSize")];
		
		if( font != nil )
			[attrDic setObject:font forKey:NSFontAttributeName];
	}
	
	
	NSData* data = preferenceValueForKey(@"defaultTextColor") ;
	if( data != nil )
	{
		[attrDic setObject:  [NSUnarchiver unarchiveObjectWithData: data] forKey: NSForegroundColorAttributeName];
	}

	
	if( font != nil )
		[[doc textView] setTypingAttributes:attrDic];
	
	
	currentDocument = doc;
	return doc;	
}

-(IBAction)newDocument:(id)sender
{
	
	MiniDocument* doc = [[[MiniDocument alloc] init] autorelease];
	[documents addObject:doc];


	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
	
	
	
	//[[doc textView] setFont:[NSFont fontWithName:
	//										size:<#(float)fontSize#> size:<#(float)fontSize#>] ];
	
	
	[self insertHeaderIn:doc];
	
	/*
	 NSRange range = NSMakeRange( [[[[doc textView] textStorage] string] length], 0);
	 [[[doc textView] textStorage] addAttribute:NSFontAttributeName 
										  value:[preferenceController defaultFont] range:range];
	 */
	//set default font
	
	id fontvalue = preferenceValueForKey(@"defaultFontName");
	
	NSFont* font = nil;
	
	if( fontvalue != nil )
	{
		font = [NSFont fontWithName:preferenceValueForKey(@"defaultFontName") size:preferenceFloatValueForKey(@"defaultFontSize")];
		
		if( font != nil )
			[[doc textView] setTypingAttributes:[NSDictionary dictionaryWithObject: font
																			forKey:NSFontAttributeName]];
	}
	

	
	
	[[doc window] tearOffWithoutMovement];
	[[doc window] repositionToMouse];
	
	[[doc window] makeKeyAndOrderFront:self];
	[(MNTabWindow*) [doc window] redrawWithShadowProperly];
	
	currentDocument = doc;
}



-(IBAction)openAll:(id)sender
{
	NSArray* visibleDocuments = [self visibleDocuments];
	
	BOOL buf = preferenceBoolValueForKey(@"animateOpenClose") ;
	BOOL buf2 = preferenceBoolValueForKey(@"closeAutomatically");
	
	
	setPreferenceValueForKey([NSNumber numberWithBool:NO],@"animateOpenClose");
	setPreferenceValueForKey([NSNumber numberWithBool:NO],@"closeAutomatically");
	syncronizePreferences();

	
	
	MiniDocument* doc;
	
	int hoge;
	
	for( hoge = 0 ; hoge < [visibleDocuments count]; hoge++ )
	{
		
		doc = [visibleDocuments objectAtIndex:hoge];
		
		[[doc window] openTab];
		//[[doc window] orderFront:self]; 
		
	}
	
	setPreferenceValueForKey([NSNumber numberWithBool:buf],@"animateOpenClose");
	setPreferenceValueForKey([NSNumber numberWithBool:buf2],@"closeAutomatically");
	syncronizePreferences();

	
	
	[[NSApplication sharedApplication] arrangeInFront:self];	
	
}

-(IBAction)closeAll:(id)sender
{
	NSArray* visibleDocuments = [self visibleDocuments];

	BOOL buf = preferenceBoolValueForKey(@"animateOpenClose") ;
	BOOL buf2 = preferenceBoolValueForKey(@"closeAutomatically");
	

	
	int alignBuffer = preferenceIntValueForKey(@"alignment");

	setPreferenceValueForKey([NSNumber numberWithInt:0],@"alignment");

	setPreferenceValueForKey([NSNumber numberWithBool:NO],@"animateOpenClose");
	setPreferenceValueForKey([NSNumber numberWithBool:NO],@"closeAutomatically");
	syncronizePreferences();

	
	MiniDocument* doc;
	
	int hoge;
	
	for( hoge = 0 ; hoge < [visibleDocuments count]; hoge++ )
	{
		
		doc = [visibleDocuments objectAtIndex:hoge];
		
		[[doc window] closeTab];
		//[[doc window] orderFront:self]; 
		
	}
	
	setPreferenceValueForKey([NSNumber numberWithBool:buf],@"animateOpenClose");
	setPreferenceValueForKey([NSNumber numberWithBool:buf2],@"closeAutomatically");

	setPreferenceValueForKey([NSNumber numberWithInt:alignBuffer],@"alignment");
	syncronizePreferences();

	
	[[NSApplication sharedApplication] arrangeInFront:self];	
	
	
}

-(int)untitledCount
{
	return untitledCount ++;
}



-(void)insertHeaderIn:(MiniDocument*)doc
{
	
	
	// insert default header
	
	NSAttributedString* headerFormat = [NSUnarchiver unarchiveObjectWithData: preferenceValueForKey(@"header") ];
	if( headerFormat == nil || [headerFormat length] == 0 ) return;
	
	//NSLog(@"headerFormat %@",headerFormat);
	
	NSMutableAttributedString *ma = [[[NSMutableAttributedString alloc] initWithAttributedString:headerFormat] autorelease]; 
	
	NSString* _dateStr = [[NSDate date] 
    descriptionWithCalendarFormat: preferenceValueForKey(@"dateFormat")  timeZone:nil 
						   locale:nil];
	
	NSRange timeRange = [[ma string] rangeOfString:@"%time"];
	if( timeRange.location != NSNotFound )
	{
		[ma replaceCharactersInRange:timeRange withString:_dateStr];
	}
	
	[[doc textView] insertText: ma];
	
}


-(void)destroy:(MiniDocument*)doc
{

	
	[ (ButtonAttachmentTextView*)[doc textView] destroyButtonAttachmentTextView];

	if( preferenceBoolValueForKey( @"moveToTrash") == YES )
	{
	
		/*
		NSString* filename = [NSString stringWithFormat:@"%@.edgy", [doc name] ];
		NSString* filePath = [DOCUMENT_FOLDER stringByAppendingPathComponent:filename ];
	*/
		
		NSString* filename = [doc filePath];

	
 
		NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
		[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:DOCUMENT_FOLDER];
 
	
		//trash it rather than destroy it
		
	NSString* trashPath = [NSHomeDirectory() stringByAppendingPathComponent:@".Trash"];
	
	NSString* nameInTrash = [[trashPath stringByAppendingPathComponent: [filename lastPathComponent]] uniquePathForFolder];
								

		[[NSFileManager defaultManager] movePath:filename
										  toPath:nameInTrash 
										 handler:nil];

	}else
	{
		/*
		NSString* filename = [NSString stringWithFormat:@"%@.edgy", [doc name] ];
		NSString* filePath = [DOCUMENT_FOLDER stringByAppendingPathComponent:filename ];
		*/
		NSString* filename = [doc filePath];

		
		
		NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
		[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:DOCUMENT_FOLDER];
		
		

		//delete
		
		[[NSFileManager defaultManager] removeFileAtPath:filename handler:nil];
	}
	
	if( preferenceIntValueForKey(@"alignment") == 2 )
	{
		[[doc window] alignIgnoringMe:self]; // align 
		// ** これが原因？
	}
	
#ifdef USE_SYNC
	[self addDeletedRecord: doc ];
	if( preferenceIntValueForKey(@"syncOption") == 2  ) [[NSApp delegate] _sync];	

#endif

	[documents removeObject:doc];
	[[SoundManager sharedSoundManager] playSoundWithName:@"closeSound"];

	

	//sync
	/*
#ifdef USE_SYNC

	if( preferenceBoolValueForKey(@"syncSwitch") )
	{
		[[SyncManager sharedSyncManager] pushChangedFileAtPath:@"" name:[doc name] type:ISyncChangeTypeDelete];

	}
	
#endif
	*/
	[[NSNotificationCenter defaultCenter] postNotificationName:TabWindowNumberDidChange
														object:self];
	
	currentDocument = nil;
	
}


-(void)saveThisData:(NSData*)data atPath:(NSString*)path defaultName:(NSString*)name document:(MiniDocument*)doc
{
	// This method is called after finishing editing.
	
	
	int syncOption =   preferenceIntValueForKey(@"syncOption") ;
	
	int backupCount = preferenceIntValueForKey(@"backupCount");

	[self saveThisData:(NSData*)data atPath:(NSString*)path defaultName:(NSString*)name document:(MiniDocument*)doc sync:(syncOption==2?YES:NO) backup:NO backupCount:backupCount];

}
-(void)saveThisData:(NSData*)data atPath:(NSString*)path defaultName:(NSString*)name document:(MiniDocument*)doc sync:(BOOL)sync backup:(BOOL)backupFlag backupCount:(int)backupCount
{
	//NSLog(@"SaveThisData %@",name);
	//NSLog(@"at path %@",path);

	NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
	[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:DOCUMENT_FOLDER];

	BOOL flag = NO;
	
	////NSLog(@"path %@",path);
	
	if( path == nil || ![[path lowercaseString] hasSuffix:@".edgy"] )
	{
	
		NSString* filename = [[name safeFilename] stringByAppendingPathExtension:@"edgy"];
		NSString* filePath = [DOCUMENT_FOLDER stringByAppendingPathComponent:filename ];
		
		filePath = [filePath uniquePathForFolder];
		
		NSDictionary* unlockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:NSFileImmutable];
		[[NSFileManager defaultManager] changeFileAttributes:unlockFilename atPath:DOCUMENT_FOLDER];
		////NSLog(@"saving %@",filePath);

		
		flag = [self writeDocumentData:data AtPath:filePath forDocument:doc  backup:backupFlag backupCount:backupCount];
		
		
		[doc setFilePath: filePath];

		
#ifdef USE_SYNC
		
		[self addChangedRecord: doc];
		if( sync ) [[NSApp delegate] _sync];	
#endif
		
		/*
#ifdef USE_SYNC

		if( sync )
		{
			[[SyncManager sharedSyncManager] pushChangedFileAtPath:filePath name:[doc name] type:ISyncChangeTypeAdd];
			
		}
#endif
		*/

	}else
	{

		flag = [self writeDocumentData:data AtPath:path forDocument:doc  backup:backupFlag backupCount:backupCount];
		[doc setFilePath: path];

		
#ifdef USE_SYNC
		
		[self addChangedRecord: doc];
		if( sync ) [[NSApp delegate] _sync];	
#endif
		/*
#ifdef USE_SYNC

		if( sync )
		{
			[[SyncManager sharedSyncManager] pushChangedFileAtPath:path name:[doc name] type:ISyncChangeTypeModify];
			
		}
#endif	*/
		
	}
	
	
	
	if( flag == NO )
	{
		//NSLog(@"Edgies: writing failed");
		
			NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString( @"Saving Failed", @"")
							defaultButton:NSLocalizedString(@"Export...",@"") alternateButton:NSLocalizedString(@"Cancel",@"") otherButton:NSLocalizedString(@"",@"")
				informativeTextWithFormat:NSLocalizedString(@"The document was not saved. The file may be locked. This memo will be discarded if you don't export it.",@"")];
			
			int result = [alert runModal];
			
			
			
			
			if( result == NSAlertDefaultReturn  )
			{
				[doc export] ;

				[self destroy:doc];
				
				[[doc window] close];
				return;
			}
			
			//[self closeTab];
			
			
	}
	
	
}

-(BOOL)writeDocumentData:(NSData*)data AtPath:(NSString*)path forDocument:(MiniDocument*)doc backup:(BOOL)backupFlag backupCount:(int)backupCount
{
	/*
	backupFlag : indicates wheater if backup this time
	backupCount : number of backup files.
	 
	 */
	
	
	//NSLog(@"writeDocumentData at %@",path);
	
	/* OLD version
	return [data writeToFile:path atomically:YES];
	
	*/

	NSFileManager *fm = [NSFileManager defaultManager];
	BOOL success = NO;
	
	// (1) create dot folder in temp folder
	
	NSString* dottedFilename  =[NSString stringWithFormat:@".%@",[path lastPathComponent]];
	NSString* tempPath = [NSTemporaryDirectory() stringByAppendingPathComponent: dottedFilename ];
	
	
	if( [fm fileExistsAtPath:tempPath] )
	{
		[fm removeFileAtPath:tempPath handler:nil];	
	}

	success =  [fm createDirectoryAtPath:tempPath attributes:nil];
	
	if( !success )
	{
		NSLog(@"Error occurd during saving 1");
		return NO;
	}
	
	// (2) create files

	
	// Add rtfd
	NSFileWrapper *rtfdrep = [doc RTFDRepresentation];
		
		
	success = [MNZip writeData:[rtfdrep serializedRepresentation] inPackage:tempPath forFile:RTFD_FILENAME];
	if( !success )
	{
		NSLog(@"Error occurd during saving 2");
		return NO;
	}

/*
	// Add text
	NSString* txt = [doc stringRepresentation];
	success = [MNZip writeData:[txt dataUsingEncoding:NSUTF8StringEncoding] inPackage:tempPath forFile:TEXT_FILENAME];
	if( !success )
	{
		NSLog(@"Error occurd during saving 3");
		return NO;
	}
 */
	
	// Add thumb
	NSImage *img = [BALLOON_CONTROLLER balloonWithContents:[[doc window] balloonImage] color:[doc documentColor] ];
	//NSData *pdfData = [[doc textView] dataWithPDFInsideRect:[[doc textView] bounds] ];
	
	
		/*
	success = [MNZip writeData:pdfData inPackage:tempPath forFile:PREVIEW_FILENAME];
	if( !success )
	{
		NSLog(@"Error occurd during saving 4");
		return NO;
	}*/
	
	NSString* qlFolder = [tempPath stringByAppendingPathComponent:@"QuickLook"];

	

	success = [MNZip writeData:[img TIFFRepresentation] inPackage:qlFolder forFile:THUMB_FILENAME];
	if( !success )
	{
		NSLog(@"Error occurd during saving 4");
		return NO;
	}

	if( ! (backupFlag == NO && backupCount == 0) )
	{
		// Add Backup
		
		
		NSArray* filenames = [fm directoryContentsAtPath: path ];
		
			
			// Keep old backup files 
			int hoge;
			for( hoge = backupCount-(backupFlag?1:0); hoge > 0; hoge-- )
			{
				NSString* str = [NSString stringWithFormat:DOC_BACKUP_FILENAME, hoge];
				if( [filenames containsObject: str] )
				{
					success =  [fm movePath:[path stringByAppendingPathComponent:str]
						  toPath:[tempPath stringByAppendingPathComponent:[NSString stringWithFormat:DOC_BACKUP_FILENAME, hoge+(backupFlag?1:0)]] 
						 handler:nil];
					
					if( !success )
					{
						NSLog(@"Error occurd during saving 5");
						return NO;
					}
					
				}
				
			}
			
			if( backupFlag )
			{
			// Add latest backup
				if( [filenames containsObject: DOC_FILENAME] )
				{
					success =  [fm movePath:[path stringByAppendingPathComponent:DOC_FILENAME]
					  toPath:[tempPath stringByAppendingPathComponent:[NSString stringWithFormat:DOC_BACKUP_FILENAME, 1]] 
					 handler:nil];
					if( !success )
					{
						NSLog(@"Error occurd during saving 6");
						return NO;
					}
				}
			}
			
	}
		
	
		
	
	// Add doc
	
	
	success =  [MNZip writeData:data inPackage:tempPath forFile:DOC_FILENAME];
	if( !success )
	{
		NSLog(@"Error occurd during saving 7");
		return NO;
	}

	// Changename from temp to path
	NSString* oldPath = nil;
	if( [fm fileExistsAtPath: path ] )
	{
		oldPath = [NSTemporaryDirectory() stringByAppendingPathComponent: [NSString stringWithFormat:@".%@.edgyprevious",[[path lastPathComponent] stringByDeletingPathExtension]] ];
		
		if( [fm fileExistsAtPath:oldPath] )
		{
			success =  [fm removeFileAtPath:oldPath handler:nil];
			if( !success )
			{
				NSLog(@"Error occurd during saving 7.5");
				return NO;
			}
		}
		//NSLog(@"moving %@ -> %@", path, oldPath);
		success =  [fm movePath:path toPath:oldPath handler:nil];
		if( !success )
		{
			NSLog(@"Error occurd during saving 8");
			return NO;
		}
	}
	
	success = [fm movePath:tempPath toPath:path handler:nil];
	
	if( !success )
	{
		[fm movePath:oldPath toPath:path handler:nil];
		
		NSLog(@"Error occurd during saving 9");
		return NO;
	}
	
	if( oldPath && [fm fileExistsAtPath:oldPath] )
	[fm removeFileAtPath:oldPath handler:nil];
	
	return YES;

}




-(void)revert:(NSString*)filePath  doc:(MiniDocument*)docIfAvailable
{
	
	// Load nib
	if( !revertWindow )
	{
		[NSBundle loadNibNamed:@"Revert" owner:self];
	}
	
	
	// Setup popup
	
	revertableDocs = [[NSMutableDictionary alloc] init];
	[revertPopup removeAllItems];
	
	NSString* docname = nil;
	NSFileManager *fm = [NSFileManager defaultManager];
	BOOL isDir;
	[fm fileExistsAtPath:filePath
			 isDirectory:&isDir];
	
	if( isDir )
	{


		int hoge;
		for( hoge = 0; hoge < 99 ; hoge++ )
		{
			
			NSString* filename;
			NSString*  title;

			if( hoge == 0 )
			{
				filename = DOC_FILENAME;
				title = NSLocalizedString(@"Current Version",@"") ;

			}else
			{
				filename = [NSString stringWithFormat:DOC_BACKUP_FILENAME, hoge];
				title = [NSString stringWithFormat: NSLocalizedString(@"Revert: Backup %d",@"") , hoge];
			}
			
			if( [MNZip existsFileInPackage:filePath forFile: filename] )
			{
				
				// Reading Document data from Backup file
				
				NSData *readDocData = [MNZip readDataInPackage:filePath forFile:filename];


				NSAttributedString* ts = [self attributedStringFromEdgyDocData: readDocData titlePtr: &docname];
				
				if( ts )
				{
					NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys: ts,@"AttributedString", readDocData,@"Data",filename, @"Filename", filePath , @"EdgyPath",  docIfAvailable, @"Document", nil];
					
					[revertableDocs setObject:dictionary forKey: title];
					
					[revertPopup addItemWithTitle: title];
					[[revertPopup lastItem] setTag: (hoge==0? -1:0)];

				}else
				{
					NSAttributedString *attr = [[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Couldn't read document data",@"")] autorelease];
					
					NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys: attr,@"AttributedString", readDocData,@"Data",filename, @"Filename", filePath , @"EdgyPath", docIfAvailable, @"Document", nil];
					
					[revertableDocs setObject:dictionary forKey: title];

					
					
					[revertPopup addItemWithTitle: title];
					[[revertPopup lastItem] setTag: -1];
				}

			}
			
		}
		
	}else
	{
		NSRunAlertPanel(@"Revert Note",@"There's no revertable versions yet.",nil,nil,nil);
		return;
	}
	
	if( docname )
		[revertWindow setTitle: [NSString stringWithFormat:NSLocalizedString(@"Revert document '%@'",@""),docname ] ];
	
	[revertPopup selectItemAtIndex:0];
		
	
	NSMenuItem *item = [revertPopup selectedItem];
	
	NSDictionary *dict = [revertableDocs objectForKey:[item title]  ];
	
	[[revertTextView textStorage] setAttributedString: [dict objectForKey:@"AttributedString"  ] ];
	
	if( [item tag] == -1 )
	{
		[revertButton setEnabled:NO];
	}else
	{
		[revertButton setEnabled:YES];
		
	}
	
	
	
	// Start sheet
	//[NSApp beginSheet:revertWindow modalForWindow:nil
	//	modalDelegate:nil didEndSelector:nil contextInfo:nil];
	
	
	[NSApp runModalForWindow: revertWindow ];
	
	

}

-(NSAttributedString*)attributedStringFromEdgyDocData:(NSData*)data titlePtr:(NSString**)titlePtr
{
	id dic;
	
	@try {
		
		dic = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		if( dic )
		{
			(*titlePtr) = [dic objectForKey:@"title"];
			
			
			NSTextStorage* ts = [NSKeyedUnarchiver unarchiveObjectWithData:[[dic objectForKey:@"contentArchive"] inflate] ] ;
			if( ts )
				return ts;
			
		}			
		
	}
	@catch (NSException * e) {

		return nil;
	
	}

				

	return nil;
}

-(void)revertAction:(id)sender
{
	int tag = [sender tag];
	
	if( tag == -1 )
	{
		[NSApp stopModal];
		//[NSApp endSheet:revertWindow];
		[revertWindow orderOut:self];
		[revertableDocs release]; revertableDocs = nil;
	}
	
	if( tag == 0 )
	{
		[NSApp stopModal];

		//[NSApp endSheet:revertWindow];
		
		
		
		int result = NSRunAlertPanel( NSLocalizedString(@"ConfirmRevert",@""), 
									  NSLocalizedString(@"This command will overwrite current contents. Do you want to continue?",@""),
									  NSLocalizedString(@"OK",@""),
									  NSLocalizedString(@"Cancel",@""),nil);
		
		if( result = NSAlertDefaultReturn )
		{
			NSMenuItem *item = [revertPopup selectedItem];

			NSDictionary *dict = [revertableDocs objectForKey:[item title]  ];

			NSAttributedString* attr = [dict objectForKey:@"AttributedString"];
			
			
			NSString *filePath = [dict objectForKey:@"EdgyPath"];
			//NSLog(@"reading %@",filePath);

			NSString *filename  = [dict objectForKey:@"Filename"];
			//NSLog(@"filename %@",filename);

			NSData *data  = [dict objectForKey:@"Data"];
			MiniDocument *doc  = [dict objectForKey:@"Document"];

			
			NSFileManager *fm = [NSFileManager defaultManager];
			BOOL isDir;
			[fm fileExistsAtPath:filePath
					 isDirectory:&isDir];
			
			if( isDir )
			{
				//(1) modify edgy file filename -> DOC_FILENAME
				
				[fm removeFileAtPath:[filePath stringByAppendingPathComponent:DOC_FILENAME]
					handler:nil];
				
				[fm movePath:[filePath stringByAppendingPathComponent:filename]
					  toPath:[filePath stringByAppendingPathComponent:DOC_FILENAME] 
					 handler:nil];

				
				
				//(2)  set data

				if( doc )
				{
					[doc setDocumentAttributes: data];
					[doc setValue:[NSNumber numberWithBool:YES] forKey:@"isDocumentEdited"];
					
				}
				
				else
				{
					//oepn doc 
					
					doc = [[[MiniDocument alloc] init] autorelease];
					[doc setDocumentAttributes: data];
					
					[doc setFilePath:filePath];
					
					[doc setValue:[NSNumber numberWithBool:NO] forKey:@"isDocumentEdited"];
					[[doc window] setNormalLevel];
					
					
					
					if( [[doc window] isTornOff] )
					{
						//NSLog(@"isTornOff");
						[[doc window] setAlphaValue:
							preferenceFloatValueForKey(@"sliderOpened") / 100.0
							];
						
						
					}
					else
					{
						
					}
					
					//[(MNTabWindow*) [doc window] redrawWithShadowProperly];
					
					[[doc window] redrawWithShadowProperly];
					if( [doc isHidden] )
						[[doc window] orderOut:self];
					else
						[[doc window] orderFront:self];
					
					
					[documents addObject:doc];
					
				}
				
			}
				
			
			
			
		}

		
		[revertWindow orderOut:self];
		[revertableDocs release]; revertableDocs = nil;


	}
	
	if( tag == 100 )
	{
		NSMenuItem *item = [revertPopup selectedItem];
		
		NSDictionary *dict = [revertableDocs objectForKey:[item title]  ];
		
		NSAttributedString* attr = [dict objectForKey:@"AttributedString"];

		
		[[revertTextView textStorage] setAttributedString: attr ];

		if( [item tag] == -1 )
		{
			[revertButton setEnabled:NO];
		}else
		{
			[revertButton setEnabled:YES];

		}
	
	}
}



#pragma mark Methods for SyncIt 

-(NSDictionary*)recordsToSync
{
	
	
	NSMutableDictionary* allRecords = [NSMutableDictionary dictionary];
	
	
	//NSLog(@"%@",[contents description]);
	int hoge;
	for( hoge = 0 ; hoge < [documents count]; hoge++ )
	{
		
		MiniDocument* oneDocument = [documents objectAtIndex:hoge];
		
			
			NSString* name = [oneDocument name];
			
			NSDictionary* dic =
				[NSDictionary dictionaryWithObjectsAndKeys:
					name, @"Name", 
					[oneDocument p_title]	,@"Title",
					[oneDocument documentAttributes] , @"Data",
					[oneDocument filePath], @"Path",
					[oneDocument recordIdentifier], @"SyncRecordIdentifier",

					@"com.pukeko.edgies.sync.entity", ISyncRecordEntityNameKey, nil];
			
			
			[allRecords setObject:dic forKey: [oneDocument recordIdentifier] ];
			
	}
	
	return (NSDictionary *)allRecords;
}

-(void)addChangedRecord:(MiniDocument*)doc
{
	if( changedDocouments == nil ) changedDocouments = [[NSMutableDictionary alloc] init];
	
	
	NSString* name = [doc name];
	
	NSDictionary* dic =
		[NSDictionary dictionaryWithObjectsAndKeys:
			name, @"Name", 
			[doc p_title]	,@"Title",
			[doc documentAttributes] , @"Data",
			[doc filePath], @"Path",
			[doc recordIdentifier], @"SyncRecordIdentifier",
			@"com.pukeko.edgies.sync.entity", ISyncRecordEntityNameKey, nil];
	
	
	[changedDocouments setObject:dic forKey:[doc recordIdentifier]];
				
}

-(NSDictionary*)changedRecordsToSync
{
	if( changedDocouments == nil ) changedDocouments = [[NSMutableDictionary alloc] init];

	return changedDocouments;
	
}
-(void)addDeletedRecord:(MiniDocument*)doc
{
	if( deletedDocouments == nil ) deletedDocouments = [[NSMutableArray alloc] init];

	
	NSString* name = [doc name];
	
	
	[deletedDocouments addObject:[doc recordIdentifier]];
	[changedDocouments removeObjectForKey: [doc recordIdentifier]];

}

-(NSArray*)deletedRecordsToSync
{
	if( deletedDocouments == nil ) deletedDocouments = [[NSMutableArray alloc] init];

	return deletedDocouments;

}

-(MiniDocument*)miniDocumentWithName:(NSString*)name
{
	NSEnumerator *en = [documents objectEnumerator];
	MiniDocument* obj;
	while( (obj = [en nextObject]) != nil )
	{
		if( [[obj name] isEqualToString:name] )
		{
			return obj;
		}
	}
	return nil;
}

-(BOOL)deleteRecordWithName:(NSString*)name
{
	BOOL flag = NO;
	MiniDocument* doc = [self miniDocumentWithName:name];
	if( doc ) {
		flag = YES;
		[self destroy:doc];
	}
		
	return flag;
	
}
-(void)clearAllChanges
{
	[deletedDocouments release];
	deletedDocouments = [[NSMutableArray alloc] init];
	
	[changedDocouments release];
	changedDocouments = [[NSMutableDictionary alloc] init];
}

#pragma mark Arrangement

-(NSArray*)documentsAtEdge:(int)edgeID
	// edgeID = screenNumber * 0x10 + tabPosition
{
	NSArray* visibleDocuments = [self visibleDocuments];

	int hoge;
	MiniDocument* doc;
	BOOL flag = NO;
	
	
	for( hoge = 0 ; hoge < [visibleDocuments count]; hoge++ )
	{
		
		doc = [visibleDocuments objectAtIndex:hoge];
		
		if( edgeID == [[doc window] edgeThatThisWindowBelongsTo] )
		{
			flag = YES;
			break;
		}
		
	}
	
	if( flag == NO )
		return [NSArray array];
	
	else
		return [self siblingsFor:doc];
	
}

-(NSArray*)siblingsFor:(MiniDocument*)doc
{
	NSArray* visibleDocuments = [self visibleDocuments];
	
	NSMutableArray* array = [NSMutableArray array];
	
	int edgeID = [[doc window] edgeThatThisWindowBelongsTo];
	int hoge;
	
	for( hoge = 0 ; hoge < [visibleDocuments count]; hoge++ )
	{
		
		doc = [visibleDocuments objectAtIndex:hoge];
		
		if( edgeID == [[doc window] edgeThatThisWindowBelongsTo] && ![[doc window] isTornOff] && ![doc isHidden])
		{
			
			[array addObject: doc ];
		}
		
	}
	

	return [array sortedArrayUsingSelector:@selector(positionCompare:)];
}

-(NSArray*)rightSiblingsFor:(MiniDocument*)doc
{
	NSArray* siblings = [self siblingsFor:doc];
	NSArray* returnArray = [NSArray array];
	
	int hoge;
	for( hoge = 0 ; hoge < [siblings count]; hoge++ )
	{
		MiniDocument* aDoc = [siblings objectAtIndex:hoge];
		if( aDoc == doc )
		{
			if( hoge != [siblings count]-1 )
				returnArray = [siblings subarrayWithRange:NSMakeRange(hoge+1, [siblings count]-hoge-1 )];
			
			break;
		}
	}
	
	return returnArray;
}


-(NSArray*)leftSiblingsFor:(MiniDocument*)doc
{
	NSArray* siblings = [self siblingsFor:doc];
	NSArray* returnArray = [NSArray array];
	
	int hoge;
	for( hoge = 0 ; hoge < [siblings count]; hoge++ )
	{
		MiniDocument* aDoc = [siblings objectAtIndex:hoge];
		if( aDoc == doc )
		{
			if( hoge != 0 )
				returnArray = [siblings subarrayWithRange:NSMakeRange(0, hoge)];
			
			break;
		}
	}
	
	return returnArray;
}



-(MiniDocument*)documentToRight:(MiniDocument*)doc
{
	NSArray* array = [self siblingsFor:doc];
	
	MiniDocument* rDoc;
	
	int hoge;
	
	for( hoge = 0 ; hoge < [array count]; hoge++ )
	{
		
		MiniDocument* aDocument = [array objectAtIndex:hoge];
		
		if( aDocument == doc )
		{		
			if( hoge+1 < [array count] )
				rDoc = [array objectAtIndex:hoge+1];
			else
				rDoc = nil;
			
			break;
		}
		
	}
	
	return rDoc;
}

-(MiniDocument*)documentToLeft:(MiniDocument*)doc
{
	NSArray* array = [self siblingsFor:doc];
	
	MiniDocument* rDoc;
	
	int hoge;
	
	for( hoge = 0 ; hoge < [array count]; hoge++ )
	{
		
		MiniDocument* aDocument = [array objectAtIndex:hoge];
		
		if( aDocument == doc )
		{		
			if( hoge > 0 )
				rDoc = [array objectAtIndex:hoge-1];
			else
				rDoc = nil;
			
			break;
		}
		
	}
	
	return rDoc;
}

-(MiniDocument*)findClosestTo:(NSRect)baseRect inSiblings:(NSArray*)siblings 
{
	if( [siblings count] == 0 ) return nil;
	
	
	NSPoint baseCenter = NSMakePoint( baseRect.origin.x + baseRect.size.width/2,
									  baseRect.origin.y + baseRect.size.height/2 );
	
	NSMutableArray* evaluation = [NSMutableArray array];
	int hoge;
	for( hoge = 0 ; hoge < [siblings count]; hoge++ )
	{
		MiniDocument* doc = [siblings objectAtIndex:hoge];
		NSRect aRect = [(MNTabWindow*)[doc window] tabBoundsInScreen];
		
		//corner 1; aRect.origin
		NSPoint corner = aRect.origin;
		float length1 = (baseCenter.x - corner.x )*(baseCenter.x - corner.x ) + 
			(baseCenter.y - corner.y )*(baseCenter.y - corner.y );
		
		
		//corner 2;aRect.origin
		corner = NSMakePoint(aRect.origin.x, NSMaxY(aRect));
		float length2 = (baseCenter.x - corner.x )*(baseCenter.x - corner.x ) + 
			(baseCenter.y - corner.y )*(baseCenter.y - corner.y );
		
		//corner 3; aRect.origin
		corner = NSMakePoint(NSMaxX(aRect), aRect.origin.y);
		float length3 = (baseCenter.x - corner.x )*(baseCenter.x - corner.x ) + 
			(baseCenter.y - corner.y )*(baseCenter.y - corner.y );
		
		
		//corner 2;aRect.origin
		corner = NSMakePoint(NSMaxX(aRect), NSMaxY(aRect));
		float length4 = (baseCenter.x - corner.x )*(baseCenter.x - corner.x ) + 
			(baseCenter.y - corner.y )*(baseCenter.y - corner.y );
		
		float length = length1;
		if( length2 < length ) length = length2;
		if( length3 < length ) length = length3;
		if( length4 < length ) length = length4;
		
		
		[evaluation addObject:[NSNumber numberWithFloat:length]];
	}
	
	
	//get minimum 
	float min = -1;
	float minIndex = -1;
	for( hoge = 0 ; hoge < [evaluation count]; hoge++ )
	{
		float value = [[evaluation objectAtIndex:hoge] floatValue];
		if( value < min || min == -1)
		{
			min = value;
			minIndex = hoge;
		}
	}
	
	if( minIndex >= 0 )
		return [siblings objectAtIndex:minIndex];
	
	return nil;
}

-(void)autoHide
{
	//NSLog(@"%@",[[NSRunLoop currentRunLoop] description]);
	
	[self arrangeToBack:self];
	
	
	isBusy = NO;
	
}


-(IBAction)arrangeToFront:(id)sender
{
	////NSLog(@"arrangeToFront");
	
	NSArray* visibleDocuments = [self visibleDocuments];

	[self arrangeToFrontDocuments: 	visibleDocuments ];
	
}


-(IBAction)arrangeToFrontDocuments:(NSArray*)documentsToMove
{
	//NSLog(@"arrangeToFront");
	
	
	[self setBusy:YES];
	
	MNTabWindow* window;
	
	int hoge;
	
	for( hoge = 0 ; hoge < [documentsToMove count]; hoge++ )
	{
		
		window = [(MiniDocument*)[documentsToMove objectAtIndex:hoge] window];
		
		
		
		if( ![window isVisible] )
		{
			
			[window orderFront:self];
			
			
			//[window flushWindow];
		}
		
		
		if( ! [window isOpened] &&  ! [window isTornOff] )
		{
			NSRect rect = [window closedFrame];
			
			[window setFrameUsingAnimation:rect
								   display:NO
								   animate:YES
							   endSelector:nil
				  canStopPreviousAnimation:YES
								  duration:[window animationResizeTime:NSZeroRect]];
		}else // 開いているまたは切り離し
		{
		
			//[[window frame] isVisible]
			
		}
		
		/*
		if( [window mouseOnTab] )
		{
			[[window tabButton] mouseEntered:nil];
		}	*/

	}
	
	
	
	[[NSApplication sharedApplication] arrangeInFront:self];	
	
}

-(IBAction)arrangeToBack:(id)sender
{
	if( mouseIsOnTheEdges() && sender != nil ) return;

	NSArray* visibleDocuments = [self visibleDocuments];
	
	[self arrangeToBackDocuments: 	visibleDocuments ];
	
}


-(IBAction)arrangeToBackDocuments:(NSArray*)documentsToMove
{
	// nil force it even mouse is on the edge.
	
	////NSLog(@"arrangeToBack");

	

	MNTabWindow* window;
	int hoge;
	
	for( hoge = 0 ; hoge < [documentsToMove count]; hoge++ )
	{
		
		window = [(MiniDocument*)[documentsToMove objectAtIndex:hoge] window];
		
		
		
		if( [window isVisible] &&
			![window isOpened] && ![window isTornOff] &&
			[window attachedSheet] == nil )
		{
			if(  ![window mouseOnTab] )
			{
				
				
				
				//[window orderOut:self];
				
				
				[window  setNormalLevel];
				NSRect rect = [window hideFrame];
				
				[window setFrameUsingAnimation:rect
									   display:NO
									   animate:YES
								   endSelector:@selector(orderOut:)
					  canStopPreviousAnimation:YES
									  duration:[window animationResizeTime:NSZeroRect]];
				
			}
			
			
		}
		
	}
	
	
}


-(IBAction)bringCenter:(id)sender
{
	NSArray* visibleDocuments = [self visibleDocuments];

	unsigned hoge;
	for( hoge = 0 ; hoge < [visibleDocuments count]; hoge++ )
	{
		
		MiniDocument* doc = [visibleDocuments objectAtIndex:hoge];
		//	[(MNTabWindow*)[doc window] tearOffWithMovement];
		[[doc window] center];
		[(MNTabWindow*)[doc window] tearOffWithoutMovement ];
		
	}
}


#pragma mark Navigation



-(void)alignTabOnEdge:(int)edgeID to:(int)centerIdx option:(int)option
//option 0=none, 1=ignore centerIex...used when deleting centerIdx tab

{
	if( centerIdx  == NSNotFound){
		//NSLog(@"alignTabOnEdge centerIdx is not found");
 
		return;
	}
	
	NSArray* documentsAtEdge = [self documentsAtEdge:edgeID];
	
	//orderfront windows
	unsigned int i, count = [documentsAtEdge count];
	for (i = 0; i < count; i++) {
		[[[documentsAtEdge objectAtIndex:i] window]	orderFront:nil];
	}
	
	
	
	float alignSpeed = preferenceFloatValueForKey(@"alignSpeed");//0.1~3
		
	double duration = (1/alignSpeed) / 5;
		
	//NSTimeInterval duration = (NSTimeInterval) preferenceFloatValueForKey(@"alignSpeed");//0.1~3
	
	
	MiniDocument* doc = [documentsAtEdge objectAtIndex:centerIdx];
	
	NSScreen* targetScreen = [[doc window] screen];
	NSRect rect = [[doc window] tabBoundsInScreen];
	
	NSArray* leftSiblings = [self leftSiblingsFor:doc];
	NSArray* rightSiblings = [self rightSiblingsFor:doc];

	int _tabPosition = 0xF & edgeID;


	float leftWidth = 0;
	float rightWidth = 0;
	
	int hoge;
	for( hoge = 0; hoge < [leftSiblings count]; hoge++ )
	{
		if( _tabPosition == tab_right || _tabPosition == tab_left )
			leftWidth +=  [(MNTabWindow*)[[leftSiblings objectAtIndex:hoge] window] tabBoundsInScreen].size.height;
		
		else
			leftWidth +=  [(MNTabWindow*)[[leftSiblings objectAtIndex:hoge] window] tabBoundsInScreen].size.width;

	}
	
	for( hoge = 0; hoge < [rightSiblings count]; hoge++ )
	{
		if( _tabPosition == tab_right || _tabPosition == tab_left )
			rightWidth +=  [(MNTabWindow*)[[rightSiblings objectAtIndex:hoge] window] tabBoundsInScreen].size.height;
		
		else
			rightWidth +=  [(MNTabWindow*)[[rightSiblings objectAtIndex:hoge] window] tabBoundsInScreen].size.width;
		
	}
	
	//calculate overwrap 
	float DEFAULT_OVERWRAP_MARGIN  = 20.0 -   preferenceFloatValueForKey(@"alignmentMargin")  ;
	
	
	leftWidth -= DEFAULT_OVERWRAP_MARGIN * [leftSiblings count];
	rightWidth -= DEFAULT_OVERWRAP_MARGIN * [rightSiblings count];

	
	float modifiedMargin_l = DEFAULT_OVERWRAP_MARGIN;
	float modifiedMargin_r = DEFAULT_OVERWRAP_MARGIN;

	
	if( _tabPosition == tab_right || _tabPosition == tab_left )
	{
		//if( NSMaxY(rect) > NSMaxY( [targetScreen frame] ) ) return; //はみだしている
		
		
		float over = NSMaxY(rect) + leftWidth - NSMaxY( [targetScreen frame] ) ;
		if(  over > 0 )
		{
			modifiedMargin_l = DEFAULT_OVERWRAP_MARGIN + over / [leftSiblings count];
			
			if( modifiedMargin_l > 50 ) modifiedMargin_l = 50;
		}
		
		over = [targetScreen frame].origin.y -( rect.origin.y - rightWidth) ;
		if(  over > 0 )
		{
			modifiedMargin_r = DEFAULT_OVERWRAP_MARGIN + over / [rightSiblings count];
			
			if( modifiedMargin_r > 50 ) modifiedMargin_r = 50;

		}
		
			
		
		/// move 
		
		if( option == 0 )
			leftWidth = NSMaxY(rect);
		else if( option == 1 )
			leftWidth = NSMaxY(rect) - rect.size.height/2 + DEFAULT_OVERWRAP_MARGIN/2;
		
		for( hoge = [leftSiblings count]-1; hoge >=0; hoge-- )
		{
			MNTabWindow* leftWin = [ (MiniDocument*)[leftSiblings objectAtIndex:hoge] window];
			NSRect leftRect = [leftWin tabBoundsInScreen];
			float modifiedY = leftWidth - modifiedMargin_l;
			
			float dif = modifiedY -  leftRect.origin.y;
			
			leftWidth = modifiedY + leftRect.size.height;

			
			NSRect winRect = [leftWin frame];
			winRect.origin.y += dif;
			
			if( [leftWin isOpened] )
				winRect = convertRectNotToStickOutFromScreenWithEdgeMargin(winRect,[leftWin screen],-20);
				
			[leftWin setFrameUsingAnimation:winRect
									display:NO animate:YES canStopPreviousAnimation:NO duration:duration];
			
		}
		

		if( option == 0 )
			rightWidth = rect.origin.y;
		else if( option == 1 )
			rightWidth = rect.origin.y + rect.size.height/2 - DEFAULT_OVERWRAP_MARGIN/2;
		
		
		for( hoge = 0; hoge < [rightSiblings count]; hoge++ )
		{
			MNTabWindow* rightWin = [[rightSiblings objectAtIndex:hoge] window];
			NSRect rightRect = [rightWin tabBoundsInScreen];
			float modifiedY = rightWidth + modifiedMargin_r - rightRect.size.height;
			
			float dif = modifiedY - rightRect.origin.y;
			
			rightWidth = modifiedY ;
			
			
			NSRect winRect = [rightWin frame];
			winRect.origin.y += dif;
			
			if( [rightWin isOpened] )
				winRect = convertRectNotToStickOutFromScreenWithEdgeMargin(winRect,[rightWin screen],-20);


			[rightWin setFrameUsingAnimation:winRect
									 display:NO animate:YES canStopPreviousAnimation:NO duration:duration];
			
		}
		
		
		
	}
	
	
	
	else if( _tabPosition == tab_top || _tabPosition == tab_bottom )
	{
		
		
		float over = NSMaxX(rect) + rightWidth - NSMaxX( [targetScreen frame] ) ;
		if(  over > 0 )
		{
			modifiedMargin_r = DEFAULT_OVERWRAP_MARGIN + over / [rightSiblings count];
			
			if( modifiedMargin_r > 50 ) modifiedMargin_r = 50;
		}
		
		over = [targetScreen frame].origin.x -( rect.origin.x - leftWidth) ;
		if(  over > 0 )
		{
			modifiedMargin_l = DEFAULT_OVERWRAP_MARGIN + over / [leftSiblings count];
			
			if( modifiedMargin_l > 50 ) modifiedMargin_l = 50;
			
		}
		
		
		
		/// move 
		
		if( option == 0 )
			leftWidth = rect.origin.x;
		else if( option == 1 )
			leftWidth = rect.origin.x + rect.size.width/2 - DEFAULT_OVERWRAP_MARGIN/2;
		

		for( hoge = [leftSiblings count]-1; hoge >=0; hoge-- )
		{
			MNTabWindow* leftWin = [ (MiniDocument*)[leftSiblings objectAtIndex:hoge] window];
			NSRect leftRect = [leftWin tabBoundsInScreen];
			float modifiedX = leftWidth + modifiedMargin_l - leftRect.size.width;
			
			float dif = modifiedX -  leftRect.origin.x;
			
			leftWidth = modifiedX ;
			
			
			NSRect winRect = [leftWin frame];
			winRect.origin.x += dif;
			
			if( [leftWin isOpened] )
				winRect = convertRectNotToStickOutFromScreenWithEdgeMargin(winRect,[leftWin screen],-20);


			
			[leftWin setFrameUsingAnimation:winRect
									display:NO animate:YES canStopPreviousAnimation:NO  duration:duration];
			
		}
		
		
		if( option == 0 )
			rightWidth = NSMaxX(rect);
		else if( option == 1 )
			rightWidth = NSMaxX(rect) - rect.size.width/2 + DEFAULT_OVERWRAP_MARGIN/2;

		
		
		for( hoge = 0; hoge < [rightSiblings count]; hoge++ )
		{
			MNTabWindow* rightWin = [ (MiniDocument*)[rightSiblings objectAtIndex:hoge] window];
			NSRect rightRect = [rightWin tabBoundsInScreen];
			float modifiedX = rightWidth - modifiedMargin_r ;
			
			float dif = modifiedX - rightRect.origin.x;
			
			rightWidth = modifiedX + rightRect.size.width;
			
			
			NSRect winRect = [rightWin frame];
			winRect.origin.x += dif;
			
			if( [rightWin isOpened] )
				winRect = convertRectNotToStickOutFromScreenWithEdgeMargin(winRect,[rightWin screen],-20);

	
			
			[rightWin setFrameUsingAnimation:winRect
									 display:NO animate:YES canStopPreviousAnimation:NO  duration:duration];
			
		}
	}
	


	
}





-(void)rearrangeTabsOnEdge:(int)edgeID fromIndex:(int)fromIdx toIndex:(int)toIdx  movingDocument:(MiniDocument*)doc originalRect:(NSRect)rect
{
// should anmate
// call this method when dragging a tab
	//NSLog(@"rearrangeTabsOnEdge");
	
	if( fromIdx == toIdx ) return;
	

	NSTimeInterval duration = (NSTimeInterval) preferenceFloatValueForKey(@"alignSpeed");


	////NSLog(@"from %d, to %d, rect %@", fromIdx, toIdx, NSStringFromRect(rect));
	

	

	
	int _tabPosition = 0xF & edgeID;

	NSArray* siblings = [self siblingsFor:doc ];
	
	if( fromIdx == NSNotFound )
		fromIdx = [siblings count]-1;
			
			
	float dif = 0.0;
	
	if( fromIdx < toIdx ) //moving target moves to right.  Others move to left
	{
		if( _tabPosition == tab_right || _tabPosition == tab_left )
		{
			
			int hoge;

			for( hoge = fromIdx ; hoge < toIdx; hoge++ )
			{
				MiniDocument* aDoc = [siblings objectAtIndex:hoge];
				MNTabWindow* window = [aDoc window];
				
				if( ![window isVisible] ) [window orderFront:nil];
			
				if( dif == 0.0 ) //set difference
					dif = rect.size.height; //NSMaxY(rect) - NSMaxY( [window frame] );
				
				//
				NSRect frame = [window frame];
				frame.origin.y += dif;
				//[window setFrameOrigin: frame.origin];
				[window setFrameUsingAnimation:frame display:NO animate:YES
					canStopPreviousAnimation:YES   duration:duration];
				
			}
			
		}else if( _tabPosition == tab_top || _tabPosition == tab_bottom )
		{
			int hoge;

			for( hoge = fromIdx ; hoge < toIdx; hoge++ )
			{
				MiniDocument* aDoc = [siblings objectAtIndex:hoge];
				MNTabWindow* window = [aDoc window];
				
				if( ![window isVisible] ) [window orderFront:nil];

				if( dif == 0.0 ) //set difference
					dif =  rect.size.width;//[window frame].origin.x - rect.origin.x;
				
				//
				NSRect frame = [window frame];
				frame.origin.x -= dif;
				//[window setFrameOrigin: frame.origin ];
				
				[window setFrameUsingAnimation:frame display:NO animate:YES
					canStopPreviousAnimation:YES   duration:duration];

			}
		}
	}
	
	else if( fromIdx > toIdx ) //moving target moves to left.  Others move to right
	{
		if( _tabPosition == tab_right || _tabPosition == tab_left )
		{
			
			int hoge;

			for( hoge = toIdx+1 ; hoge <= fromIdx; hoge++ )
			{
				MiniDocument* aDoc = [siblings objectAtIndex:hoge];
				MNTabWindow* window = [aDoc window];
				
				if( ![window isVisible] ) [window orderFront:nil];

				
				if( dif == 0.0 ) //set difference
					dif = rect.size.height;//[window frame].origin.y - rect.origin.y;
				
				//
				NSRect frame = [window frame];
				frame.origin.y -= dif;
				//[window setFrameOrigin: frame.origin];
				[window setFrameUsingAnimation:frame display:NO animate:YES
					canStopPreviousAnimation:YES   duration:duration];

				
			}
			
		}else if( _tabPosition == tab_top || _tabPosition == tab_bottom )
		{
			int hoge;
			for( hoge = toIdx+1 ; hoge <= fromIdx; hoge++ )
			{
				MiniDocument* aDoc = [siblings objectAtIndex:hoge];
				MNTabWindow* window = [aDoc window];
				
				if( ![window isVisible] ) [window orderFront:nil];

				if( dif == 0.0 ) //set difference
					dif =  rect.size.width;//NSMaxX(rect) - NSMaxX ( [window frame] );
				//
				NSRect frame = [window frame];
				frame.origin.x += dif;
				//[window setFrameOrigin: frame.origin];
				[window setFrameUsingAnimation:frame display:NO animate:YES canStopPreviousAnimation:YES   duration:duration];

				
			}
		}
	}

}
-(IBAction)navigate:(id)sender
{
	//TabDocumentController* tabDocumentController = [TabDocumentController sharedTabDocumentController];

	[NSApp activateIgnoringOtherApps:YES];
	
	int tag = [sender tag];
	
	
	//一つだけ開いていたら、それを基準にする

	MNTabWindow* window = [[self actionForcusedDocument] window];
	
	if( window == nil )
	{
		if( [documents count] == 0 ) return;
		
		[self setCurrentDocument:[documents objectAtIndex:0] ];

		
	}else
	{
		[self setCurrentDocument:[window ownerDocument] ];


	}
	
	int tabPosition = [currentDocument tabPosition];
	
	
	// migi hidari
	if( tabPosition == tab_top || tabPosition == tab_bottom )
	{
		
		
		if( tag == -1 )
		{
			MiniDocument* doc = [self documentToLeft: [self currentDocument] ];
			
			if( doc == nil ) //transition to left
			{
				int newEdge =  edgeLeftTo( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
				
				if( newEdge != NSNotFound )
				{
					doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
												 inSiblings:[self documentsAtEdge:newEdge] ];
				}
			}// 
			
			
			if( doc != nil )
			{
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}

			
		}
		if( tag == 1 )
		{
			MiniDocument* doc = [self documentToRight:[self currentDocument] ];
			
			if( doc == nil ) //transition to right
			{
				int newEdge =  edgeRightTo( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
				
				if( newEdge != NSNotFound )
				{
					doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
								   inSiblings:[self documentsAtEdge:newEdge] ];
				}
			}// 
			
			
			if( doc != nil )
			{
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
			
		}
		
	}else if( tabPosition == tab_right || tabPosition == tab_left )
	{
		if( tag == -1 ) //left
		{
			
			int newEdge = edgeLeftTo( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
			
			if( newEdge != NSNotFound )
			{
				MiniDocument* doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
											 inSiblings:[self documentsAtEdge:newEdge] ];
				
				
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
			
			
		}
		if( tag == 1 ) //right
		{
			int newEdge =  edgeRightTo( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
			
			if( newEdge != NSNotFound )
			{
				MiniDocument* doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
											 inSiblings:[self documentsAtEdge:newEdge] ];
				
				
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
			
		}
	}
	
	
	
	
	
	
	// ue shita
	if( tabPosition == tab_top || tabPosition == tab_bottom )
	{
		if( tag == -2 ) //ue
		{

			int newEdge =  edgeAbove( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
			
			if( newEdge != NSNotFound )
			{
				MiniDocument* doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
							   inSiblings:[self documentsAtEdge:newEdge] ];
				
				
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
		
			
		}
		if( tag == 2 ) //shita
		{
			int newEdge =  edgeBelow( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
			
			if( newEdge != NSNotFound )
			{
				MiniDocument* doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
											 inSiblings:[self documentsAtEdge:newEdge] ];
				
				
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
			
		}
	}else if( tabPosition == tab_right || tabPosition == tab_left )
	{
		
		
		if( tag == -2 )
		{
			MiniDocument* doc = [self documentToLeft:[[[self currentDocument] window] ownerDocument]];
			
			if( doc == nil ) //transition to left
			{
				int newEdge = edgeAbove( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
				
				if( newEdge != NSNotFound )
				{
					doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
								   inSiblings:[self documentsAtEdge:newEdge] ];
				}
			}// 
			
			
			if( doc != nil )
			{
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
			
			
		}
		if( tag == 2 )
		{
			MiniDocument* doc = [self documentToRight:[[[self currentDocument] window] ownerDocument]];
			
			if( doc == nil ) //transition to right
			{
				int newEdge = edgeBelow( [[[self currentDocument] window] edgeThatThisWindowBelongsTo] );
				
				if( newEdge != NSNotFound )
				{
					doc = [self findClosestTo:[[[self currentDocument] window] closedFrame]
								   inSiblings:[self documentsAtEdge:newEdge] ];
				}
			}// 
			
			
			if( doc != nil )
			{
				MNTabWindow* window = [doc window];
				[window makeKeyAndOrderFront:self];
				[window openTab];
				[self setCurrentDocument:[window ownerDocument] ];
			}
			
		}
		
	}
	
}


@end
