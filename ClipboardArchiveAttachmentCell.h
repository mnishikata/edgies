//
//  FaviconAttachmentCell.h
//  sticktotheedge
//
//  Created by __Name__ on 07/05/23.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ButtonAttachmentCell.h"
#import "AttachmentCellConverter.h"

NSData* dataFromClipboard(NSPasteboard* pb);

NSDictionary* pbdictionaryFromData(NSData* data);


@interface ClipboardArchiveAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{


}
+(NSAttributedString*)clipboardArchiveAttachmentCellAttributedStringWithDropAnythingValue:(id)value;
+(NSAttributedString*)clipboardArchiveAttachmentCellAttributedStringWithPasteboard:(NSPasteboard*)pboard;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)dealloc;
-(void)copyToClipboard;
-(void)useAsDefault;
-(void)loadDefaults;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)textView;
-(NSImage*)imageValue;
-(NSData*)imageData;
-(NSString*)stringValue;
-(NSAttributedString*)attributedStringValue;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context; //withimage ;
-(NSDictionary*)pasteboardRepresentation:(id)context;
-(NSString*)writeToFileAt:(NSURL*)url    ;
-(BOOL)canAcceptDrop;

@end


@protocol ClipboardArchiveAttachmentCellOwnerTextView 
//-(void)faviconAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)faviconAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)clipboardArchiveAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)clipboardArchiveAttachmentCellArrayInSelectedRange;
-(void)openClipboardArchiveInspector;


@end
