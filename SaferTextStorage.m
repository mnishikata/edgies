//
//  FugoCompletionTextStorage.m
//  SampleApp
//
//  Created by __Name__ on 13/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SaferTextStorage.h"



@implementation SaferTextStorage



- (id)init {
    self = [super init];
    if (self) {
		
		// fundamental
		m_attributedString = [[NSMutableAttributedString alloc] init];
		
		
		
		// subclass
		
		
		
    }
    
    return self;
}

- (id)initWithText:(NSString*)str
{
	[self init];
	
	[self replaceCharactersInRange:NSMakeRange(0,[[self string] length]) withString:str];
	
	return self;
}


- (id)initWithAttributedString:(NSAttributedString*)astr
{
	[self init];
	
	[m_attributedString setAttributedString:astr];
	
	return self;
}

- (void)dealloc
{
	
	[m_attributedString release];
	
	[super dealloc];
}


- (NSString *)string
{
    return [m_attributedString string];
}

- (NSDictionary *)attributesAtIndex:(unsigned)index effectiveRange:(NSRangePointer)aRange
{
    return [m_attributedString attributesAtIndex:index effectiveRange:aRange];
}

- (void)replaceCharactersInRange:(NSRange)range withString:(NSString *)str
{
	
	if( [[m_attributedString attributedSubstringFromRange:range] containsAttachments] )
	{
		int idx;
		for( idx = range.location; idx < NSMaxRange(range); idx++ )
		{
			id something = [m_attributedString attribute:NSAttachmentAttributeName
												 atIndex:idx
								   longestEffectiveRange:nil
												 inRange:range];
			
			
			if( something != nil )
			{
				id cell = [something attachmentCell];
				if( [cell respondsToSelector:@selector(removeSubview)] )
				{
					[cell removeSubview];
				}
			}			
		}
		
	}
	
	
	
    [m_attributedString replaceCharactersInRange:range withString:str];
    
    int lengthChange = [str length] - range.length;
    [self edited:NSTextStorageEditedCharacters range:range changeInLength:lengthChange];
	
	
}

- (void)setAttributes:(NSDictionary *)attributes range:(NSRange)range
{
    [m_attributedString setAttributes:attributes range:range];
    [self edited:NSTextStorageEditedAttributes range:range changeInLength:0];
}



/////



@end

