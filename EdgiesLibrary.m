#import "EdgiesLibrary.h"



void syncronizePreferences(void)
{
	CFPreferencesAppSynchronize(appID);	
}
void postPreferencesChangedNotification(void)
{
	CFNotificationCenterRef center;
	
    center = CFNotificationCenterGetDistributedCenter();
    CFNotificationCenterPostNotification(center,
										 (CFStringRef)EdgiesPreferencesChangedNotificationName , appID, NULL, TRUE);
	
}
id preferenceValueForKey(NSString* key)
{
	//CFPreferencesAppSynchronize(appID);
	
	CFPropertyListRef prop  = CFPreferencesCopyAppValue (  (CFStringRef)key, appID  );
	if( prop == nil )
		return nil;
	
	id prop2 = [[(id)prop copy] autorelease];
	CFRelease( prop );
	
	return prop2;
	
}

BOOL preferenceBoolValueForKey(NSString* key)
{
	
	//CFPreferencesAppSynchronize(appID);
	BOOL val = NO;
	
	CFPropertyListRef prop  = CFPreferencesCopyAppValue (  (CFStringRef)key, appID  );
	if( prop == nil )
		return NO;
	
	if( ( CFGetTypeID(prop)  == CFNumberGetTypeID() ) || ( CFGetTypeID(prop) == CFStringGetTypeID() ) )  //number
	{
		val = [prop boolValue];

	}
	else if( CFGetTypeID(prop) == CFBooleanGetTypeID () )
	{
		
		val =  ( CFBooleanGetValue (prop  ) == true ? YES: NO );
		
	}
	
	
	CFRelease( prop );
	
	return val;
	
}

int preferenceIntValueForKey(NSString* key)
{
	
	int val = 0;
	
	CFPropertyListRef prop  = CFPreferencesCopyAppValue (  (CFStringRef)key, appID  );
	if( prop == nil )
		return 0;
	
	CFTypeID type = CFGetTypeID(prop);
	
	if( type  == CFNumberGetTypeID() ) 
		CFNumberGetValue(prop, kCFNumberIntType , &val);
	
	else if ( type == CFStringGetTypeID()  )  //number
		val = CFStringGetIntValue(prop);		
	
	
	
	CFRelease( prop );
	
	return val;
	
}


double preferenceDoubleValueForKey(NSString* key)
{
	//CFPreferencesAppSynchronize(appID);
	double val = 0;
	
	CFPropertyListRef prop  = CFPreferencesCopyAppValue (  (CFStringRef)key, appID  );
	if( prop == nil )
		return 0;
	
	CFTypeID type = CFGetTypeID(prop);
	
	
	if( type  == CFNumberGetTypeID() )
		CFNumberGetValue(prop, kCFNumberDoubleType , &val);
	
	else if ( type == CFStringGetTypeID()  )  //number
		val = CFStringGetDoubleValue(prop);		
	
	
	CFRelease( prop );
	
	return val;
	
}
float preferenceFloatValueForKey(NSString* key)
{
	//CFPreferencesAppSynchronize(appID);
	float val = 0;
	
	CFPropertyListRef prop  = CFPreferencesCopyAppValue (  (CFStringRef)key, appID  );
	if( prop == nil )
		return 0;
	
	CFTypeID type = CFGetTypeID(prop);
	
	if( type == CFNumberGetTypeID() )
		CFNumberGetValue(prop, kCFNumberFloatType , &val);
	
	else if ( type == CFStringGetTypeID()  )  //number
		val = (float)CFStringGetDoubleValue(prop);		
	
	
	CFRelease( prop );
	
	return val;
	
}

void setPreferenceValueForKey(id value, NSString* key)
{
	//NSLog(@"key %@",key);
	
	
	NSData* plistData = [NSPropertyListSerialization dataFromPropertyList:value
																   format:NSPropertyListBinaryFormat_v1_0
														 errorDescription:nil];
	
	
	
	
	CFPropertyListRef prop;
	prop =  CFPropertyListCreateFromXMLData (
											 nil,
											 (CFDataRef)plistData,
											 kCFPropertyListMutableContainersAndLeaves,
											 nil
											 );
	
	CFPreferencesSetAppValue( (CFStringRef)key, prop, appID );
	
	if( prop != nil )
		CFRelease(prop);
	
	
	//syncronizePreferences();
}



NSString* applicationSupportFolder(void) 
{
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();
    return [basePath stringByAppendingPathComponent:@"Cog"];
}

NSString* avoidNillString(NSString* string)
{
		return (string == NULL ? @"" : string );

}
