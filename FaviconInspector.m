#import "FaviconInspector.h"
#import "FaviconAttachmentCell.h"

@implementation FaviconInspector

+(FaviconInspector*)sharedFaviconInspector
{
	return [[NSApp delegate] faviconInspector];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		activeCells = nil;

		[NSBundle loadNibNamed:@"FaviconInspector" owner:self];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(selectionChanged:)
													 name:NSTextViewDidChangeSelectionNotification
												   object:nil];
		[window setFloatingPanel:YES];

	}
	return self;
}
-(BOOL)multipleSelection
{
	if( [activeCells count] >1) return YES;
	return NO;
}
-(BOOL)noSelection
{
	if( [activeCells count] == 0 ) return YES;
	return NO;
}
-(void)showFaviconInspector
{
	[window orderFront:self];	
	[self updateInspector];
}
-(id)imageData
{
	if( [activeCells count] == 1) return [[activeCells objectAtIndex:0] valueForKey:@"imageData"];

	else return nil;
}
-(id)title
{
	if( [activeCells count] != 1) return @"Multiple Selection";

	return [[activeCells objectAtIndex:0] valueForKey:@"title"];
}
-(NSString*)targetPath
{
	if( [activeCells count] != 1) return @"";
	
	return [[activeCells objectAtIndex:0] valueForKey:@"targetPath"];
}
-(void)setTitle:(NSString*)title
{
	[activeCells setValue:title forKey:@"title"];
}

-(id)font
{
	if( [activeCells count] == 0) return nil;

	return [[activeCells objectAtIndex:0] valueForKey:@"font"];
	
}
-(void)setFont:(NSFont*)font
{
	[activeCells setValue:font forKey:@"font"];
}

-(id)buttonSize
{
	if( [activeCells count] != 1) return nil;

	
	return 	 [[activeCells objectAtIndex:0] valueForKey:@"buttonSize"];

}
-(void)setButtonSize:(NSNumber*)size
{
	
	[activeCells setValue:size forKey:@"buttonSize"];
}
-(id)buttonPosition
{
	if( [activeCells count] != 1) return nil;

	return 	 [[activeCells objectAtIndex:0] valueForKey:@"buttonPosition"];
}
-(void)setButtonPosition:(NSNumber*)position
{
	[activeCells setValue:position forKey:@"buttonPosition"];
}
-(id)showButtonTitle
{
	if( [activeCells count] != 1) return nil;

	return  [[activeCells objectAtIndex:0] valueForKey:@"showButtonTitle"];
}
-(void)setShowButtonTitle:(id)showButtonTitle
{
	[activeCells setValue:showButtonTitle forKey:@"showButtonTitle"];
}



//		[self setBordered:showBezel];
-(id)showBezel
{
	if( [activeCells count] != 1) return nil;
	
	return  [[activeCells objectAtIndex:0] valueForKey:@"showBezel"];
}
-(void)setShowBezel:(id)showBezel
{
	[activeCells setValue:showBezel forKey:@"showBezel"];
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	
	[super dealloc];
}

-(BOOL)lock
{
	BOOL flag = NO;
	unsigned int i, count = [activeCells count];
	for (i = 0; i < count; i++) {
		NSObject * obj = [activeCells objectAtIndex:i];
		if( [[obj valueForKey:@"lock"] boolValue] == YES )
		{
			flag = YES;
			break;
		}
	}
	
	return  flag;
}
-(void)setLock:(BOOL)flag
{
	[activeCells setValue:[NSNumber numberWithBool:flag] forKey:@"lock"];

}

-(void)selectionChanged:(NSNotification*)notification
{
	//NSLog(@"selectionChanged");
	
	if( ! [window isVisible] ) return;
	
	NSTextView* view = [notification object];
	if( view == nil ) return;

	if( ! [view conformsToProtocol:@protocol(FaviconAttachmentCellOwnerTextView)] ) return;

	
	[self activateForTextView:view];
	
	
	////NSLog(@"selectionChanged %@",[view className]);


}

-(void)updateInspector
{	  
	if( ! [window isVisible] ) return;

	[self willChangeValueForKey:@"title"];
	[self didChangeValueForKey:@"title"];
	
	[self willChangeValueForKey:@"font"];
	[self didChangeValueForKey:@"font"];
	
	[self willChangeValueForKey:@"buttonSize"];
	[self didChangeValueForKey:@"buttonSize"];
	
	[self willChangeValueForKey:@"buttonPosition"];
	[self didChangeValueForKey:@"buttonPosition"];
	
	[self willChangeValueForKey:@"imageData"];
	[self didChangeValueForKey:@"imageData"];
	
	[self willChangeValueForKey:@"showButtonTitle"];
	[self didChangeValueForKey:@"showButtonTitle"];

	[self willChangeValueForKey:@"showBezel"];
	[self didChangeValueForKey:@"showBezel"];
		
	[self willChangeValueForKey:@"targetPath"];
	[self didChangeValueForKey:@"targetPath"];	
	
	[self willChangeValueForKey:@"lock"];
	[self didChangeValueForKey:@"lock"];	
	
	[self willChangeValueForKey:@"noSelection"];
	[self didChangeValueForKey:@"noSelection"];	
	
	[self willChangeValueForKey:@"multipleSelection"];
	[self didChangeValueForKey:@"multipleSelection"];	

	
}



-(void)activateForTextView:(NSTextView  <FaviconAttachmentCellOwnerTextView> *)view
{
		
	if( [view conformsToProtocol:@protocol(FaviconAttachmentCellOwnerTextView)] )
	{
	//	NSRange range = [view selectedRange];
		
		[activeCells release];
		activeCells = [[NSMutableArray arrayWithArray: [view faviconAttachmentCellArrayInSelectedRange]] retain];
		
			
		[self updateInspector];

		//[objectController setContent:sender];
		[self showFaviconInspector];
	}else
	{
		[activeCells release];
		activeCells = nil;
	}
}

-(IBAction)useAsDefault:(id)sender
{
	if( [activeCells count] > 0 )
		[[activeCells objectAtIndex:0] useAsDefault];
}


-(IBAction)loadFavicon:(id)sender
{
	[activeCells makeObjectsPerformSelector:@selector(loadFavicon)];
}
@end
