//
//  SyncManager.m
//  sticktotheedge
//
//  Created by __Name__ on 07/01/31.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SyncManager.h"
#import "EdgiesLibrary.h"
#import "TabDocumentController.h"
#import "NSString (extension).h"

@implementation SyncManager

+(SyncManager*)sharedSyncManager
{
	return [[NSApp delegate] syncManager];
}

- (id) init {
	self = [super init];
	if (self != nil) {
		lock = [[NSLock alloc] init];
	}
	return self;
}

-(void)syncDocumentFolder:(NSString*)documentFolderPath pull:(BOOL)pull
{
	

	
	ISyncManager *manager = [ISyncManager sharedManager];
	ISyncClient *syncClient;
	
	
	
	// ******  register client ***** //
	
	
	
	NSString* schemaPath;
	NSString* schemaPlist ;
	NSString* appIcon =  [[NSBundle bundleForClass:[self class]] pathForResource:@"appIcon" ofType:@"icns"];

	NSString* appPath = [[[appIcon stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
	schemaPath = [NSString stringWithFormat:@"%@/Contents/Library/EdgiesSyncSchema.syncschema",appPath];
	
	schemaPlist = [NSString stringWithFormat:@"%@/Contents/Library/EdgiesSyncSchema.syncschema/Contents/Resources/EdgiesMemoClientDescription.plist",appPath];
		
	
	//NSLog(@"schemaPath %@",schemaPath);
	//NSLog(@"schemaPlist %@",schemaPlist);
	
	
	
	
	// Register the schema.
	[manager registerSchemaWithBundlePath:schemaPath ];
	
	//NSLog(@"register");
	
	
	// See if our client has already registered
	if (!(syncClient = [manager clientWithIdentifier:@"com.pukeko.edgies.sync.client"])) {
		// and if it hasn't, register the client.
		// We use the filename as part of the identifier, so that each file represents a separate sync client
		syncClient = [manager registerClientWithIdentifier:@"com.pukeko.edgies.sync.client" descriptionFilePath:schemaPlist];
	}
	
	
	[syncClient setImagePath :appIcon];
		
	//NSLog(@"register finished");
	
	
	
	BOOL enabled = [syncClient isEnabledForEntityName:@"com.pukeko.edgies.sync.entity"];
	if( ![[ISyncManager sharedManager] isEnabled] || !enabled )
	{
		return;
	}
	
	// ******  initialize client ***** //
	
	//NSLog(@"initialize");
	
	ISyncSession *syncSession;
	
	// Open a session with the sync server
	syncSession = [ISyncSession beginSessionWithClient:syncClient
										   entityNames:[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]
	/* How long I'm willing to wait - pretty much forever */ beforeDate:[NSDate distantFuture]];
	
	// If we failed to get a sync session, we either timed out, or syncing isn't enabled.
	if (!syncSession) {
		if (![[ISyncManager sharedManager] isEnabled])
		{
		}else
		{
		}
		
		return ;
	}
	
	
	

	/*
	 // If we're refreshing our data, it's okay to overwrite previous client ids.
	 if ( !_isRefresh ) {
		 // Using our custom data, determine the highest record Id the server should know about
		 NSNumber *highestServerId = (NSNumber *)[syncClient objectForKey:@"HighestId"];
		 //   If we've got a higher recordId in our local document, update our info on the server
		 if ([highestServerId intValue] < _highestLocalId) {
			 [syncClient setObject:[NSNumber numberWithInt:_highestLocalId] forKey:@"HighestId"];
		 } else {
			 _highestLocalId = [highestServerId intValue];
		 }
	 }
	 */
	
	
	
	// Using the custom data, get the number of the last sync, so we can bump it and store it
//	int serverLastSync = [(NSNumber *)[syncClient objectForKey:@"LastSyncNumber"] intValue];
	//_lastSyncNumber = (_lastSyncNumber > serverLastSync) ? _lastSyncNumber + 1 : serverLastSync + 1;
	//NSLog(@"serverLastSync %d",serverLastSync);
	
	
	/*
	 // If we've been asked to do a refresh sync, inform the server, and skip the push phase altogether
	 if ( _isRefresh ) {
		 [syncSession clientDidResetEntityNames:[NSArray arrayWithObject:ISyncEntity_Com_MyCompany_SyncExamples_Zensync_entity]];
	 } else if ( ! _changes ) {
		 // If there were no change lines (not even blanks), we're pushing everything (slow sync).
		 // Tell the server our request, and let it determine if we can.
		 [syncSession clientWantsToPushAllRecordsForEntityNames:
			 [NSArray arrayWithObject:ISyncEntity_Com_MyCompany_SyncExamples_Zensync_entity]];
	 }
	 return syncSession;
	 */
	
	
	//Slow Syncing
	[syncSession clientWantsToPushAllRecordsForEntityNames:
		[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];
	
	
	
	
	//*** push ***/
	//NSLog(@"push");
	
	
	/// prepare data

	
	NSFileManager* fm = [NSFileManager defaultManager];
	NSArray* contents = [fm subpathsAtPath:documentFolderPath];
	NSMutableDictionary* allRecords = [NSMutableDictionary dictionary];
	
	
	//NSLog(@"%@",[contents description]);
	int hoge;
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		
		NSString* item = [contents objectAtIndex:hoge];
		
		if( [item hasSuffix:@".edgy"] )
		{
			
			NSString* itemPath = [documentFolderPath stringByAppendingPathComponent:item];
			
			NSData* fileRep =[[[NSData alloc] initWithContentsOfFile:itemPath] autorelease];
			if( fileRep != nil )
			{				
				NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:fileRep];
				NSString* name = [dict objectForKey:@"name"];
				
				NSDictionary* dic =
					[NSDictionary dictionaryWithObjectsAndKeys:
						name, @"Name", 
						[dict objectForKey:@"title"],@"Title",
						fileRep , @"Data",
						itemPath, @"Path",
						@"com.pukeko.edgies.sync.entity", ISyncRecordEntityNameKey, nil];
				
				
				[allRecords setObject:dic forKey:name];
				
			}
			
			
		}
	}
	
	
	
	
	
	
	// Ask the server how it wants us to sync
	if ([syncSession shouldPushAllRecordsForEntityName:
		@"com.pukeko.edgies.sync.entity"]) 
	{
		
		NSArray* allValues = [allRecords allValues];
		//[[NSFileManager defaultManager] subpathsAtPath:DOCUMENT_FOLDER];
		int hoge;
		for( hoge = 0 ; hoge < [allValues count]; hoge++ )
		{

			
			//NSLog(@"pushing all records ... ");
			NSDictionary* aRecord = [allValues objectAtIndex:hoge];
			
			[syncSession pushChangesFromRecord: aRecord
								withIdentifier:[aRecord objectForKey:@"Name"] ];
		}
		
		
		
	} else if ([syncSession shouldPushChangesForEntityName:
		@"com.pukeko.edgies.sync.entity"]) 
	{
		// If the server would rather have us push up our changes, pull any changes from our change store, and push them
		//    Those that were specified with add / delete / modify, push as ISyncChange s
		//    All others, push as modified records
		
		/*
		 [syncSession pushChange:[ISyncChange changeWithType:ISyncChangeTypeAdd
											recordIdentifier:_ID HERE
													 changes:_CHANGED RECORD HERE];
			 
			 or ISyncChangeTypeDelete  ISyncChangeTypeModify 				
			 */
		
		
		//NSLog(@"pushin changed records ... do nothing here");
		
	}
	// If the server wouldn't let us push anything, do nothing
	
	
	
	
	
	// *** pull *** 
	
	
	
	BOOL shouldPull = pull && [syncSession shouldPullChangesForEntityName:@"com.pukeko.edgies.sync.entity"];
	
	//NSLog(@"pull %d", shouldPull);
	
	
	// If we're allowed to pull changes, tell the engine to prepare them.  If not, we're still okay
	// (one example is when we're supposed to push the truth.  We won't be asked to pull changes in that case)
	if (shouldPull) 
	{
		BOOL serverResponse = [syncSession prepareToPullChangesForEntityNames:
			[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]
		  /* How long I'm willing to wait - pretty much forever */ beforeDate:[NSDate distantFuture]];
		
		// If the server tells us the mingling didn't go okay, bail out of this methood
		if ( ! serverResponse ) {
			[syncSession cancelSyncing];
			return ;
		}
		
		
		// Determine if the server will push everything
		if ([syncSession shouldReplaceAllRecordsOnClientForEntityName:
			@"com.pukeko.edgies.sync.entity"])
			
			// And if so, whack our data
			[allRecords removeAllObjects];
		
		
		
		// Enumerate over the available changes
		NSEnumerator *changeEnumerator = [syncSession changeEnumeratorForEntityNames:
			[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];
		
		ISyncChange *currentChange;
		
		// Apply each change to the data store, and report success or failure back to the sync server
		while (( currentChange = (ISyncChange *)[changeEnumerator nextObject] )) {
			switch ( [currentChange type] ) {
				case ISyncChangeTypeDelete:
				{
					//NSLog(@"delete %@", [currentChange recordIdentifier] );
					
					
					NSDictionary* aRecord = [allRecords objectForKey: [currentChange recordIdentifier]];
						
					NSString* path = [aRecord objectForKey:@"Path"];
					
					if( [path hasSuffix:@".edgy"] )
					{
						[allRecords removeObjectForKey:[currentChange recordIdentifier]];
						[fm removeFileAtPath:path handler:nil];
					}
						[syncSession clientAcceptedChangesForRecordWithIdentifier:[currentChange recordIdentifier]
																  formattedRecord:nil
															  newRecordIdentifier:nil];
					break;
				}
					
					
				case ISyncChangeTypeAdd:
				{
					
					
					
					NSString *newRecordId = [currentChange recordIdentifier];
					//NSLog(@"add obj  %@",newRecordId);

					NSData* dataToWrite = [[currentChange record] objectForKey: @"Data"];
					
					
					
					NSString* path = [documentFolderPath  stringByAppendingPathComponent:documentFolderPath];
					path = [[path stringByAppendingPathExtension:@"edgy"] uniquePathForFolder];
					
	
					[dataToWrite writeToFile:path atomically:nil];
					[allRecords setObject:[currentChange record] forKey:newRecordId];

					
					[syncSession clientAcceptedChangesForRecordWithIdentifier:[currentChange recordIdentifier]
															  formattedRecord:nil
														  newRecordIdentifier:newRecordId];
					break;
				}
					
					
				case ISyncChangeTypeModify:
				{
					
					
					NSString *newRecordId = [currentChange recordIdentifier];
					
					//NSLog(@"modify obj %@  ",newRecordId);

					
					NSData* dataToWrite = [[currentChange record] objectForKey: @"Data"];
					NSDictionary* dic = [allRecords objectForKey:newRecordId];
					
					if( dic != nil && dataToWrite != nil )
					{
						NSString* path = [dic objectForKey:@"Path"];
						[dataToWrite writeToFile:path atomically:nil];
						[allRecords setObject:[currentChange record] forKey:[currentChange recordIdentifier]];
					
					}
					
					
					[syncSession clientAcceptedChangesForRecordWithIdentifier:[currentChange recordIdentifier]
															  formattedRecord:nil
														  newRecordIdentifier:nil];
					break;
				}
			}
		}
		// If we were asked to pull something from the server, we got changes.
		// Once all have been applied, tell the server we're done modifying our data, for better or worse
		[syncSession clientCommittedAcceptedChanges];
	}	
	
	/*
	 // Update our HighestId info (we might have incremented it while applying an add from the server)
	 [syncClient setObject:[NSNumber numberWithInt:_highestLocalId] forKey:@"HighestId"];
	 
	 // Update our LastSyncNumber info, now that we know the sync was successful
	 [syncClient setObject:[NSNumber numberWithInt:_lastSyncNumber] forKey:@"LastSyncNumber"];
	 
	 */
	
	// tell the sync server we're done with this session
	[syncSession finishSyncing];
	
	
	
	
	
	
	//NSLog(@"return YES");
	
	return ;
	
}

-(void)pushChangedFileAtPath:(NSString*)edgyDocumentPath name:(NSString*)name type:(ISyncChangeType)type 
{
	[lock lock];
	
	[NSThread detachNewThreadSelector:@selector(pushChangedFileAtPathThread:)
							 toTarget:self
   withObject:[NSDictionary dictionaryWithObjectsAndKeys:edgyDocumentPath,@"Path", name, @"Name",[NSNumber numberWithInt:type] ,@"Type", nil] ];

	[lock unlock];

}
-(void)pushChangedFileAtPathThread:(NSDictionary*)threadDictionary
{
	[lock lock];

	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	
	NSString* edgyDocumentPath = [threadDictionary objectForKey:@"Path"];
	NSString* name = [threadDictionary objectForKey:@"Name"];
	ISyncChangeType type = [[threadDictionary objectForKey:@"Type"] intValue];

	
	ISyncManager *manager = [ISyncManager sharedManager];
	ISyncClient *syncClient;
	
	syncClient = [manager clientWithIdentifier:@"com.pukeko.edgies.sync.client"];
	
	
	// ******   client enabled? ***** //
	
	
	BOOL enabled = [syncClient isEnabledForEntityName:@"com.pukeko.edgies.sync.entity"];
	if( ![[ISyncManager sharedManager] isEnabled] || !enabled )
	{
		goto Skip;

	}

	
	
	
	// ******  initialize client ***** //
	
	//NSLog(@"initialize");
	
	ISyncSession *syncSession;
	
	// Open a session with the sync server
	syncSession = [ISyncSession beginSessionWithClient:syncClient
										   entityNames:[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]
	/* How long I'm willing to wait - pretty much forever */ beforeDate:[NSDate distantFuture]];
	
	// If we failed to get a sync session, we either timed out, or syncing isn't enabled.
	if (!syncSession) {
		if (![[ISyncManager sharedManager] isEnabled])
		{
		}else
			
		{
		}
		
		goto Skip;
	}
	
	
	

	/*
	 // If we're refreshing our data, it's okay to overwrite previous client ids.
	 if ( !_isRefresh ) {
		 // Using our custom data, determine the highest record Id the server should know about
		 NSNumber *highestServerId = (NSNumber *)[syncClient objectForKey:@"HighestId"];
		 //   If we've got a higher recordId in our local document, update our info on the server
		 if ([highestServerId intValue] < _highestLocalId) {
			 [syncClient setObject:[NSNumber numberWithInt:_highestLocalId] forKey:@"HighestId"];
		 } else {
			 _highestLocalId = [highestServerId intValue];
		 }
	 }
	 */
	
	
	
	// Using the custom data, get the number of the last sync, so we can bump it and store it
//	int serverLastSync = [(NSNumber *)[syncClient objectForKey:@"LastSyncNumber"] intValue];
	//_lastSyncNumber = (_lastSyncNumber > serverLastSync) ? _lastSyncNumber + 1 : serverLastSync + 1;
	//NSLog(@"serverLastSync %d",serverLastSync);
	
	
	/*
	 // If we've been asked to do a refresh sync, inform the server, and skip the push phase altogether
	 if ( _isRefresh ) {
		 [syncSession clientDidResetEntityNames:[NSArray arrayWithObject:ISyncEntity_Com_MyCompany_SyncExamples_Zensync_entity]];
	 } else if ( ! _changes ) {
		 // If there were no change lines (not even blanks), we're pushing everything (slow sync).
		 // Tell the server our request, and let it determine if we can.
		 [syncSession clientWantsToPushAllRecordsForEntityNames:
			 [NSArray arrayWithObject:ISyncEntity_Com_MyCompany_SyncExamples_Zensync_entity]];
	 }
	 return syncSession;
	 */
	
	
	//Slow Syncing
	//[syncSession clientWantsToPushAllRecordsForEntityNames:
	//	[NSArray arrayWithObject:@"com.pukeko.edgies.sync.entity"]];
	
	
	
	
	//*** push ***/
	//NSLog(@"push");
	
	
	/// prepare data

	
	NSDictionary* aRecord = nil;
	NSData* fileRep;
	
	if( type !=  ISyncChangeTypeDelete )
	{
		fileRep =[[[NSData alloc] initWithContentsOfFile:edgyDocumentPath] autorelease];
		if( fileRep != nil )
		{
			
			//name = [[NSKeyedUnarchiver unarchiveObjectWithData:fileRep] objectForKey:@"name"];
			
			aRecord =
				[NSDictionary dictionaryWithObjectsAndKeys:
					name, @"Name", 
					fileRep , @"Data",
					edgyDocumentPath, @"Path",
					@"com.pukeko.edgies.sync.entity", ISyncRecordEntityNameKey, nil];
			

		}else
		{
		}
	}
	
	
	
	
	
	// Ask the server how it wants us to sync
	if ([syncSession shouldPushAllRecordsForEntityName:
		@"com.pukeko.edgies.sync.entity"]) 
	{
		
		//NSLog(@"pushing all records ... Do nothing");

		
	} else if ([syncSession shouldPushChangesForEntityName:
		@"com.pukeko.edgies.sync.entity"]) 
	{
		// If the server would rather have us push up our changes, pull any changes from our change store, and push them
		//    Those that were specified with add / delete / modify, push as ISyncChange s
		//    All others, push as modified records
		
		/*
		 [syncSession pushChange:[ISyncChange changeWithType:ISyncChangeTypeAdd
											recordIdentifier:_ID HERE
													 changes:_CHANGED RECORD HERE];
			 
			 or ISyncChangeTypeDelete  ISyncChangeTypeModify 				
			 */
		
		if( type ==  ISyncChangeTypeDelete )
			[syncSession deleteRecordWithIdentifier:name];
		else if( aRecord != nil )
		{
			
			NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:
					ISyncChangePropertySet, ISyncChangePropertyActionKey,
					@"Data",ISyncChangePropertyNameKey,
					fileRep,ISyncChangePropertyValueKey,nil];
			
			NSArray* array = [NSArray arrayWithObjects:dict,nil];

				
			[syncSession pushChange:[ISyncChange changeWithType:type
										   recordIdentifier:name
													changes:array ]];	
			
			
		}
	}
	// If the server wouldn't let us push anything, do nothing
	
	
	
	

	/*
	 // Update our HighestId info (we might have incremented it while applying an add from the server)
	 [syncClient setObject:[NSNumber numberWithInt:_highestLocalId] forKey:@"HighestId"];
	 
	 // Update our LastSyncNumber info, now that we know the sync was successful
	 [syncClient setObject:[NSNumber numberWithInt:_lastSyncNumber] forKey:@"LastSyncNumber"];
	 
	 */
	
	// tell the sync server we're done with this session
	[syncSession finishSyncing];
	
	
	
	
	
	
	//NSLog(@"return YES");

Skip:
	[pool release];
	[lock unlock];

	return ;
	
}



@end
