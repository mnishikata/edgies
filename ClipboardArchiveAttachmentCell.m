//
//  FaviconAttachmentCell.m
//  sticktotheedge
//
//  Created by __Name__ on 07/05/23.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ClipboardArchiveAttachmentCell.h"
#import "ClipboardArchiveInspector.h"
#import "FileLibrary.h"
#import "NSString (extension).h"
#import "NSData+CocoaDevUsersAdditions.h"
#import "MNGraphicAttachmentCell.h"

extern BOOL UD_DELETE_CELLS_AFTER_DRAGGING_OUT;

@implementation ClipboardArchiveAttachmentCell

+(NSAttributedString*)clipboardArchiveAttachmentCellAttributedStringWithDropAnythingValue:(id)value
{
	NSData* deflateData = [value deflate];
	
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	ClipboardArchiveAttachmentCell* aCell = [[[ClipboardArchiveAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setRepresentedObject: deflateData ];
	[aCell setTitle:@"ClipboardArchive"];
	
	
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
	
}

+(NSAttributedString*)clipboardArchiveAttachmentCellAttributedStringWithPasteboard:(NSPasteboard*)pboard
{

	
	
	
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	ClipboardArchiveAttachmentCell* aCell = [[[ClipboardArchiveAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setRepresentedObject:dataFromClipboard (pboard) ];
	[aCell setTitle:@"ClipboardArchive"];
		
	
		
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


#pragma mark -
- (id)initWithAttachment:(NSTextAttachment*)anAttachment
{
    self = [super initWithAttachment:(NSTextAttachment*)anAttachment];
    if (self) {
		
		
		//[self setTarget:self];
		//[self setAction:@selector(copyToClipboard)];
		[self setTitle:@"ClipboardArchive"];
		
		
		[self setImage:[NSImage imageNamed:@"dropanything"]];

		[self loadDefaults];


	}
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	
	
    return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {

		//[self setTarget:self];
		//[self setAction:@selector(copyToClipboard)];
	}
	return self;
	
	
}
-(void)dealloc
{
	[super dealloc];
}

-(void)action:(id)sender
{

	[self copyToClipboard];
}


-(void)copyToClipboard
{
	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSGeneralPboard];
	NSDictionary* dict = pbdictionaryFromData([self representedObject]);

	NSMutableArray* allKeys = [NSMutableArray arrayWithArray: [dict allKeys]];
	
	[allKeys removeObject: NSFilesPromisePboardType];
	[pb declareTypes:allKeys owner:self];

	unsigned int i, count = [allKeys count];
	for (i = 0; i < count; i++) {
		NSString * key = [allKeys objectAtIndex:i];

		[pb setData:[dict objectForKey:key] forType:key];
	
	}
	
	
	return;
}


-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"ClipboardArchiveAttachmentCell_buttonSize"];
	[ud setInteger:buttonPosition forKey:@"ClipboardArchiveAttachmentCell_buttonPosition"];
	[ud setBool:showButtonTitle forKey:@"ClipboardArchiveAttachmentCell_showButtonTitle"];
	[ud setBool:showBezel forKey:@"ClipboardArchiveAttachmentCell_showBezel"];
	[ud setObject:[NSArchiver archivedDataWithRootObject: [self font]] forKey:@"ClipboardArchiveAttachmentCell_font"];
	
	[ud synchronize];
}


-(void)loadDefaults
{
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"ClipboardArchiveAttachmentCell_buttonSize"];
	buttonSize = (value != nil ? [value floatValue] : 48); 
	
	value = [ud valueForKey: @"ClipboardArchiveAttachmentCell_buttonPosition"];
	buttonPosition = (value != nil ? [value intValue] : 0); 
	
	value = [ud valueForKey: @"ClipboardArchiveAttachmentCell_showButtonTitle"];
	showButtonTitle = (value != nil ? [value boolValue] : NO); 
	
	value = [ud valueForKey: @"ClipboardArchiveAttachmentCell_showBezel"];
	showBezel = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"ClipboardArchiveAttachmentCell_font"];
	if( value != nil ){
		NSFont* font = [NSUnarchiver unarchiveObjectWithData: value];
		if( font != nil && [font isKindOfClass:[NSFont class]] )
			[self setFont:font];
		
	}
	
	lock = NO;
}






-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <ClipboardArchiveAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;

	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"ClipboardArchiveAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		

	//add separator
	customMenuItem = [NSMenuItem separatorItem];
	[customMenuItem setRepresentedObject:@"ClipboardArchiveAttachmentCellItem"];
	[aContextMenu addItem:customMenuItem ];



	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Clipboard Archive Inspector",@"")
										action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"ClipboardArchiveAttachmentCellItem"];
	[customMenuItem setTarget: aTextView];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(openClipboardArchiveInspector)];

	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
			

			

		

	
	///
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Copy to Clipboard",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"ClipboardArchiveAttachmentCellItem"];
	[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(copyToClipboard)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
		
}


+(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <ClipboardArchiveAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
	
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"ClipboardArchiveAttachmentCellItemClassMethod"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
	

	
	
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Paste as archive",@"")
												action:nil keyEquivalent:@"V"];
		[customMenuItem setKeyEquivalentModifierMask:1048576];

	[customMenuItem setRepresentedObject:@"ClipboardArchiveAttachmentCellItemClassMethod"];
	[customMenuItem setTarget: aTextView];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(pasteAsArchive:)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem autorelease];	
			
							
}

-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	[textView  openClipboardArchiveInspector];
}


#pragma mark -
#pragma mark @protocol AttachmentCellConverting 

-(NSImage*)imageValue
{
	NSDictionary* dict = pbdictionaryFromData([self representedObject]);

	if( [dict objectForKey:NSTIFFPboardType] != nil )
	{
		NSImage* image = [[NSImage alloc] initWithData: [dict objectForKey:NSTIFFPboardType]];
		return [image autorelease];		
	}
	
	return nil;
}

-(NSData*)imageData
{
	NSDictionary* dict = pbdictionaryFromData([self representedObject]);
	
	if( [dict objectForKey:NSTIFFPboardType] != nil )
	{
		return [dict objectForKey:NSTIFFPboardType];		
	}
	
	return nil;
}

-(NSString*)stringValue
{
	
	NSString* string = nil;

	NSDictionary* dict = pbdictionaryFromData([self representedObject]);
	if( [dict objectForKey:@"ClipboardArchiveAttachmentCell_NSString" ] != nil )
	{
		string = [[NSString alloc] initWithData: [dict objectForKey:@"ClipboardArchiveAttachmentCell_NSString" ] encoding:NSUTF8StringEncoding];
		
		
	}else if( [dict objectForKey:NSRTFPboardType] != nil )
	{
		
		NSData* aData = [dict objectForKey:NSRTFPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
		
		string = [anAttr string];
		
	}else if( [dict objectForKey:NSRTFDPboardType] != nil )
	{
		NSData* aData = [dict objectForKey:NSRTFDPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTFD:aData documentAttributes:NULL];
		
		string = [anAttr string];
	}
	
	return string;
}
-(NSAttributedString*)attributedStringValue
{
	
	NSAttributedString* attrrep = nil;
	
	NSDictionary* dict = pbdictionaryFromData([self representedObject]);
	
	if( [dict objectForKey:NSRTFDPboardType] != nil )
	{
		
		NSData* aData = [dict objectForKey:NSRTFDPboardType];
		
		attrrep = [[NSAttributedString alloc] initWithRTFD:aData documentAttributes:NULL];
		
		
	}else if( [dict objectForKey:NSRTFPboardType] != nil )
	{
		NSData* aData = [dict objectForKey:NSRTFPboardType];
		
		attrrep = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
		
	}else
	{
		
		NSString* str = [self stringValue];
		
		if( str == nil ) str = @" ";
		attrrep = [[[NSAttributedString alloc] initWithString: str] autorelease];
	}
	
	//NSLog(@"attributedStringValue %@",[attrrep string]);
	return attrrep;
}
-(int)stringRepresentation:(NSDictionary*)context
{
	
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
		
	NSString* string = [self stringValue];
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = [string length] -1;
	
	return changeInLength;
}




-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	
	int changeInLength = 0;
	
	NSAttributedString* attrrep = [self attributedStringValue];
	if( attrrep != nil )
	{
		[mattr replaceCharactersInRange:NSMakeRange(index,1) withAttributedString:attrrep];
		 changeInLength = [attrrep length] -1;
	}
	
		
	
	return changeInLength;
	

}


-(int)attributedStringRepresentation:(NSDictionary*)context //withimage 
{
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];

	
	
	NSImage* image = [self imageValue];
	if( image != nil )
	{
		
		NSAttributedString* attr = [MNGraphicAttachmentCell graphicAttachmentCellAttributedStringWithImage:image];
		
		if( attr == nil )
		{
			//NSLog(@"failed to create attr");	
		}
		else
		{
			[mattr replaceCharactersInRange:NSMakeRange(index,1) withAttributedString:attr];
			return 0;

		}
		
	}
		
	
	return [self attributedStringWithoutImageRepresentation: context];
	
	
}


-(NSDictionary*)pasteboardRepresentation:(id)context
{
	

	
	return pbdictionaryFromData([self representedObject]);
}

-(NSString*)writeToFileAt:(NSURL*)url    
{
	BOOL success = NO;
	NSString* filename = [[self title] safeFilename];
	
	NSImage* image = [self imageValue];
	if( image != nil )
	{
		success = dropTIFF( [image TIFFRepresentation]
							  ,[url path]
							, filename );
			
	}else
	{
		NSAttributedString* attr = [self attributedStringValue ];
		
		if( attr != nil )
		{
			NSData* filedata = [attr RTFDFromRange:NSMakeRange(0,[attr length]) documentAttributes:nil];
			NSArray* savedFiles = dropRTFD(filedata  , [url path]);

			success = (savedFiles?YES:NO);

			
		}
	}

	
	
		
	if( success && UD_DELETE_CELLS_AFTER_DRAGGING_OUT )
		[self removeFromTextView];

	
	return ( success ? filename : nil );
}

-(BOOL)canAcceptDrop
{
	return NO;
}

#pragma mark Library

NSData* dataFromClipboard(NSPasteboard* pb)
{
	// convert pb to data
	NSArray* types= [pb types];
	NSMutableDictionary* pbDic = [NSMutableDictionary dictionary];
	int hoge;
	for( hoge = 0; hoge < [types count]; hoge++ )
	{
		NSString* type = [types objectAtIndex:hoge];
		
		
		NSData* data = [pb dataForType:type];
		
		if( data != nil )
			[pbDic setObject:data forKey:type];
		
		if( [type isEqualToString:NSStringPboardType] )
		{
			NSString* readableString = [pb stringForType: NSStringPboardType];
			[pbDic setObject:[readableString dataUsingEncoding:NSUTF8StringEncoding] forKey:@"ClipboardArchiveAttachmentCell_NSString"];
			
		}
	}
	
	
	NSData* fwdata = [[NSKeyedArchiver archivedDataWithRootObject:pbDic ] deflate];	
	return fwdata;
}

NSDictionary* pbdictionaryFromData(NSData* data)
{
	NSData* inflatedData = [data inflate];
	
	
	NSDictionary* pbDic = [NSKeyedUnarchiver unarchiveObjectWithData:inflatedData];
	return pbDic;
	
				
}

@end
