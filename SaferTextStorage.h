
#import <Cocoa/Cocoa.h>



@interface  SaferTextStorage : NSTextStorage
{
	NSMutableAttributedString* m_attributedString;
}

- (NSString *)string;
- (NSDictionary *)attributesAtIndex:(unsigned)index effectiveRange:(NSRangePointer)aRange;
- (void)replaceCharactersInRange:(NSRange)aRange withString:(NSString *)str;
- (void)setAttributes:(NSDictionary *)attributes range:(NSRange)aRange;
-(BOOL)rangeIsValid:(NSRange)range;
@end
