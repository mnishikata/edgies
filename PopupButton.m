//
//  PopupButton.m
//  USBCat
//
//  Created by __Name__ on 08/07/23.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "PopupButton.h"


@implementation PopupButton



- (void)mouseDown:(NSEvent *)theEvent
{
	NSEvent	*aMenuEvent;
	NSPoint	aMenuLoc;
	
	// Viewに合わせてメニューを表示させる場所を再計算
	aMenuLoc = 	[[[self window] contentView] convertPoint:NSMakePoint(0,0) fromView:self];
	aMenuLoc.y -= 24;
	// 新しいイベントを作り（ほとんどが継承で、場所だけ変更）
	aMenuEvent = [ NSEvent mouseEventWithType:[ theEvent type ] 
									 location: aMenuLoc
								modifierFlags:[ theEvent modifierFlags ] 
									timestamp:[ theEvent timestamp ] 
								 windowNumber:[ theEvent windowNumber ]
									  context:[ theEvent context ] 
								  eventNumber:[ theEvent eventNumber ] 
								   clickCount:[ theEvent clickCount ] 
									 pressure:[ theEvent pressure ]];
	
	// ボタンをハイライトさせる
	[ self highlight:YES ];
	// ポップアップメニューを表示させる
	[ NSMenu popUpContextMenu:[ self menu ] withEvent:aMenuEvent forView:self ];
	// ハイライトを戻す
	[ self highlight:NO ];
	
}

@end