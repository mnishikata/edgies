//
//  AliasAttachmentCell.h
//  sticktotheedge
//
//  Created by __Name__ on 06/08/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ButtonAttachmentCell.h"
#import "NDAlias.h"
#import "AttachmentCellConverter.h"
@protocol TaggingAttachmentCellOwnerTextView ;

@interface TaggingAttachmentCell : ButtonAttachmentCell <AttachmentCellConverting>{

	NSArray* tagTitles; // string array
	NSString* targetPath;
	BOOL skipDialogue;
}

+(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TaggingAttachmentCellOwnerTextView> *)aTextView;
+(NSAttributedString*)taggingAttachmentCellAttributedString;
-(NSString*)targetPath;
-(NSArray*)tagTitles;
-(BOOL)skipDialogue;
-(void)setSkipDialogue:(BOOL)flag;

-(void)setTagTitles:(NSArray*)titles;
-(NSImage*)image;
-(void)useAsDefault;
- (id)initWithAttachment:(NSTextAttachment*)attachment;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
-(void)openDialogue;
-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TaggingAttachmentCellOwnerTextView> *)textView;
-(void)loadDefaults;
-(void)activateInspectorForTextView:(NSTextView  *)aTextView;
-(int)stringRepresentation:(NSDictionary*)context;
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image ;
-(int)attributedStringRepresentation:(NSDictionary*)context; //withimage ;
-(NSDictionary*)pasteboardRepresentation:(id)context;
-(BOOL)canAcceptDrop;


@end

@protocol TaggingAttachmentCellOwnerTextView 
//-(void)aliasAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
//-(void)aliasAttachmentCellClicked:(ButtonAttachmentCell*)cell;
//-(void)aliasAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
-(NSArray*)taggingAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)taggingAttachmentCellArrayInSelectedRange;
-(void)openTaggingInspector;


@end



/*
@protocol AttachmentCellConverting 
-(NSString*)stringRepresentation:(id)context;
-(NSAttributedString*)attributedStringWithoutImageRepresentation:(id)context; //without image 
-(NSAttributedString*)attributedStringRepresentation:(id)context; //without image 
-(NSImage*)imageRepresentation:(id)context;


@end
 */