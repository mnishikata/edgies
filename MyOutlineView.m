#import "MyOutlineView.h"


#define OutlineRowType @"outline"
#define OutlineIndexesType @"outlineIndexes"
#define OutlineRowTypes [NSArray arrayWithObjects:@"outline", nil]


@implementation MyOutlineView
-(void)drawRect:(NSRect)rect
{
	[super drawRect:rect];
	
	// draw selected row count number
	NSIndexSet* selectedRowIndexes =   [self selectedRowIndexes];
	if( [ selectedRowIndexes count ] > 0 )
	{
		unsigned hoge;
		for( hoge = 0; hoge < [self numberOfRows] ; hoge++ )
		{
			if( [selectedRowIndexes containsIndex:hoge] )
			{
				id item = [self itemAtRow:hoge ];
				[[self delegate]  outlineView:self willDisplayCell:NULL 
							   forTableColumn:[self tableColumnWithIdentifier:@"C1" ] item:item];
			}
		}
	}
	
	
}
- (BOOL)acceptsFirstResponder
{
	id firstResponder = [[self window] firstResponder];
	
	if( [firstResponder isKindOfClass:[NSTextView class]] )
	{
		////NSLog(@"Check?");
		
		if( [firstResponder hasMarkedText] )
		{
			NSBeep();
			return NO;
		}
	}
	return YES;
}

@end
