//
//  MNAttributedString.h
//  sticktotheedge
//
//  Created by __Name__ on 07/05/22.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface  AttachmentCellConverter : NSObject // (AttachmentCellConverting)
{
	NSMutableArray* cellsWantToWriteToFile;
	
	

}
+(AttachmentCellConverter*)sharedAttachmentCellConverter;
- (id) init ;
-(void)dealloc;
-(void)preferenceChanged;
-(NSString*)convertAttributedStringToString:(NSAttributedString*)attr ;
-(NSData*)convertAttributedStringToRTF:(NSAttributedString*)attr documentAttributes:(NSDictionary *)docAttributes;
-(NSData*)convertAttributedStringToRTFD:(NSAttributedString*)attr documentAttributes:(NSDictionary *)docAttributes;
-(NSFileWrapper*)convertAttributedStringToRTFDFileWrapper:(NSAttributedString*)attr documentAttributes:(NSDictionary *)docAttributes;;
-(NSAttributedString*)convertCellsInAttributedString:(NSAttributedString*)attr usingSelector:(SEL)selector;;
-(NSArray*)extractPasteboardRepresentationsFromCellsInAttributedString:(NSAttributedString*)attr ;
-(NSDictionary*)combinePasteboardRepresentations:(NSArray*)pasteboardDictionaries;
-(BOOL)writeAttributedString:(NSAttributedString*)attr toPasteboard:(NSPasteboard *)pboard;
- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination senderTextView:(NSTextView*)textView;
-(id)readTextFromPasteboard:(NSPasteboard *)pboard draggingSource:(id <NSDraggingInfo>)sender;
-(NSAttributedString*)attributedStringFromTIFFData:(NSData*)data;
-(NSAttributedString*)attributedStringFromSafariURL:(NSArray*)pbArray;
-(NSAttributedString*)attributedStringFromFilenames:(NSArray*)files;
-(NSAttributedString*)attributedStringFromOtherURL:(NSURL*)aURL;
-(BOOL)fileIsPicture:(NSString*)path;


@end

@protocol AttachmentCellConverting 
-(int)stringRepresentation:(NSDictionary*)context; // context MutableAttributedString, CellIndex
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context; //without image 
-(int)attributedStringRepresentation:(NSDictionary*)context; //with image 
+(NSAttributedString*)convertPasteboard:(NSPasteboard*)pboard;

-(NSDictionary*)pasteboardRepresentation:(id)context;
-(NSString*)writeToFileAt:(NSURL*)url ;

-(BOOL)canAcceptDrop;
/*
 context
 MutableAttributedString
 CellIndex
 
 */
@end