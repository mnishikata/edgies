/* MemoManager */

#import <Cocoa/Cocoa.h>
#import "GraphicInspector.h"
#import "MiniDocument.h"
#import "MemoManagerTableView.h"

@interface MemoManager : NSObject
{
    IBOutlet id panel;
    IBOutlet MemoManagerTableView* table;
    IBOutlet id textView;
	IBOutlet id searchView;
	IBOutlet id searchField;
	IBOutlet id indicator;
	IBOutlet id arrayController;

	
	//id documentBank_ds;
	id fillPatternImage;
	
#if MAC_OS_X_VERSION_MAX_ALLOWED < 1040
	
#else
		MDQueryRef _query;
#endif

		
		NSMutableArray* allKeys;
		IBOutlet id metaList; //controller;
		IBOutlet id metaListWindow;
		NSMutableArray* metaListArray;
	
}
+(GraphicInspector*)sharedMemoManager;
- (id) init ;
-(IBAction)showMemoManager:(id)sender;
-(void)setupTable;
-(void)endMemoList;
-(void)reloadData:(id)sender;
-(void)dealloc;
-(void)showPanel;
-(void)hidePanel;
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification;
- (void)tableView:(NSTableView*)tableView willDisplayCell:(id)cell forTableColumn:(NSTableColumn*)tableColumn row:(int)row;
-(void)showPopupMenuForRow:(int)row;
-(void)setupMetaList;
-(IBAction)showViewOption:(id)sender;
-(void)metaListClosed:(id)sender;
-(NSString*)titleForColumnIdentifier:(NSString*)identifier;
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)item;;
- (IBAction)metaListFilter:(id)sender;
- (IBAction)search:(id)sender;
-(NSString*)searchFormatWithQuery:(NSString*)aQuery;;
-(void)setupToolbar;
- (NSArray*)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar;
- (NSArray*)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar;
- (NSToolbarItem*)toolbar:(NSToolbar*)toolbar itemForItemIdentifier:(NSString*)itemId  willBeInsertedIntoToolbar:(BOOL)willBeInserted;
- (BOOL) validateToolbarItem: (NSToolbarItem *) toolbarItem;
-(MiniDocument*)selectedDocument;
-(void)toggle;
-(void)newDocumentInList;


@end
