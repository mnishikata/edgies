//
//  ZoomTextView.h
//  sticktotheedge
//
//  Created by __Name__ on 06/07/11.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "PrintingTextView.h"

@interface ZoomTextView : PrintingTextView {

	float zoomFactor;
}
- (id)initWithFrame:(NSRect)frame ;
- (void)awakeFromNib;
-(void)dealloc;
-(void)setZoom:(float)newZoomFactor;
-(float)zoom;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)menu_selectedZoomTextView:(id)sender;


@end
