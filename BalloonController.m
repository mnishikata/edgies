//
//  BalloonController.m
//  sticktotheedge
//
//  Created by __Name__ on 06/06/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "BalloonController.h"

#import "ScreenUtil.h"
#import "NSWindow+Transforms.h"
#import "EdgiesLibrary.h"
#import "SoundManager.h"

#import "AppController.h"

#import "ThemeManager.h"

//#define PREF [[[NSApplication sharedApplication] delegate] preferenceController]

@implementation BalloonController

- (id) init {
	self = [super init];
	if (self != nil) {
		
		NSRect frame = NSMakeRect(0,0,100,100);
		balloonWindow = [[NSWindow alloc] initWithContentRect: frame
													styleMask:NSBorderlessWindowMask 
													  backing:NSBackingStoreBuffered defer:NO];
		
		[balloonWindow setLevel:NSPopUpMenuWindowLevel];
		[balloonWindow setBackgroundColor:[NSColor clearColor]];
		[balloonWindow setOpaque:NO];
		[self setAlpha:  preferenceFloatValueForKey(@"sliderBalloon")   / 100.0];
		
		[balloonWindow setIgnoresMouseEvents:YES];
		[balloonWindow setHasShadow:YES];

		NSImageView* imv = [[NSImageView alloc] initWithFrame:frame];
		[imv setAutoresizingMask: NSViewHeightSizable | NSViewWidthSizable];
		
		[balloonWindow setContentView:imv];
		
		//

		//NSString* path;
		//path = [[NSBundle bundleForClass:[self class]] pathForResource:@"balloonGlass" ofType:@"tif"];
	/*	
		balloonGlass =	[[[APP_CONTROLLER themeManager] balloonGlass] retain];
			//;
		balloonMask =	[[APP_CONTROLLER themeManager] balloonMask]
			//[[NSImage imageNamed:@"balloonMask"] retain];
*/
		
	}
	return self;
}

-(NSImage*)balloonWithContents:(NSImage*)contentImage color:(NSColor*)color
{
	
	NSBitmapImageRep* balloonGlassCopy = [NSBitmapImageRep imageRepWithData:[[[ThemeManager sharedManager] balloonGlass] TIFFRepresentation]] ;
	
	//NSBitmapImageRep* balloonGlassCopy = [[ copy] autorelease];
	
	[balloonGlassCopy colorizeByMappingGray:0.95
									toColor:color
							   blackMapping:[NSColor blackColor]
							   whiteMapping:[NSColor whiteColor]];
	
	
	NSImage* balloonGlassImage = [[[NSImage alloc] initWithData:[balloonGlassCopy TIFFRepresentation]] autorelease];
	
	NSSize balloonSize = NSZeroSize;
	balloonSize.width = [contentImage size].width + 12;
	balloonSize.height = [contentImage size].height + 12;

	NSRect balloonRect = NSZeroRect;
	balloonRect.size = balloonSize;
	
	int mar = 10;
	
	NSRect topLeftCorner = NSMakeRect(0,0,mar,mar);			// 69 x 44
	NSRect topRightCorner = NSMakeRect(69-mar,0,mar,mar);
	NSRect bottomLeftCorner = NSMakeRect(0,44-mar,mar,mar);
	NSRect bottomRightCorner = NSMakeRect(69-mar,44-mar,mar,mar);
	NSRect topBar = NSMakeRect(mar,0,69-mar*2,mar);
	NSRect bottomBar = NSMakeRect(mar,44-mar,69-mar*2,mar);
	NSRect leftBar = NSMakeRect(0,mar,mar,44-mar*2);
	NSRect rightBar = NSMakeRect(69-mar,mar,mar,44-mar*2);
	NSRect contentArea = NSMakeRect(mar,mar,69-mar*2,44-mar*2);
	
	//
	
	NSRect b_topLeftCorner = NSMakeRect(0,0,mar,mar);			// 69 x 44
	NSRect b_topRightCorner = NSMakeRect( NSMaxX(balloonRect)-mar ,0,mar,mar);
	NSRect b_bottomLeftCorner = NSMakeRect(0, NSMaxY(balloonRect)-mar ,mar,mar);
	NSRect b_bottomRightCorner = NSMakeRect( NSMaxX(balloonRect)-mar  , NSMaxY(balloonRect)-mar ,mar,mar);
	NSRect b_topBar = NSMakeRect(mar,0, balloonRect.size.width -mar*2 ,mar);
	NSRect b_bottomBar = NSMakeRect(mar, NSMaxY(balloonRect)-mar ,  balloonRect.size.width -mar*2 ,mar);
	NSRect b_leftBar = NSMakeRect(0,mar,mar, balloonRect.size.height -mar*2 );
	NSRect b_rightBar = NSMakeRect( NSMaxX(balloonRect)-mar ,mar,mar,   balloonRect.size.height -mar*2 );
	NSRect b_contentArea = NSMakeRect(mar,mar, balloonRect.size.width -mar*2 ,  balloonRect.size.height -mar*2 );
	
	///

	
	NSImage* bimage = [[[NSImage alloc] initWithSize:balloonSize] autorelease];
	
	[bimage lockFocus];
	//[color set];
	//[NSBezierPath fillRect:balloonRect ];
	
	


	///
	
	
	[balloonGlassImage drawInRect:b_topLeftCorner
				   fromRect:topLeftCorner operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_topRightCorner
				   fromRect:topRightCorner operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_bottomLeftCorner
				   fromRect:bottomLeftCorner operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_bottomRightCorner
				   fromRect:bottomRightCorner operation:NSCompositeSourceOver fraction:1.0];
	
	
	[balloonGlassImage drawInRect:b_topBar
				   fromRect:topBar operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_bottomBar
				   fromRect:bottomBar operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_leftBar
				   fromRect:leftBar operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_rightBar
				   fromRect:rightBar operation:NSCompositeSourceOver fraction:1.0];
	
	[balloonGlassImage drawInRect:b_contentArea
				   fromRect:contentArea operation:NSCompositeSourceOver fraction:1.0];
	
	///
	
	
	

	
	
	
	/*
	[balloonMask drawInRect:b_topLeftCorner
				   fromRect:topLeftCorner operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_topRightCorner
				   fromRect:topRightCorner operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_bottomLeftCorner
				   fromRect:bottomLeftCorner operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_bottomRightCorner
				   fromRect:bottomRightCorner operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_topBar
				   fromRect:topBar operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_bottomBar
				   fromRect:bottomBar operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_leftBar
				   fromRect:leftBar operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_rightBar
				   fromRect:rightBar operation:NSCompositeXOR fraction:1.0];
	
	[balloonMask drawInRect:b_contentArea
				   fromRect:contentArea operation:NSCompositeXOR fraction:1.0];
	*/
	
	
	///
	[contentImage compositeToPoint:NSMakePoint(6,6) operation:NSCompositeSourceOver];

	
	
	[bimage unlockFocus];
	
	return bimage;
	 
}

-(void)setAlpha:(float)alpha
{
	[balloonWindow setAlphaValue:alpha];
	
}

-(void)showBalloonWithImage:(NSImage*)image color:(NSColor*)aColor at:(NSPoint)point sender:(id)sender
{
	if( balloonOwner == sender ) return;
	
	
	balloonOwner = sender;
	
	
	
	//check if it is already opened
	//NSImage* currentImage = [[balloonWindow contentView] image];
	//if( [balloonWindow isVisible ] && currentImage == image ) return;
	
	
	//[self hideBalloon];
	
		
//	int mar = 20;
	
	image = [self balloonWithContents:image color:aColor ];
	
	NSRect frame = NSZeroRect;
	frame.size = [image size];
	frame.origin = point;


	frame = convertRectNotToStickOutFromScreenWithEdgeMargin(frame, screenWherePointIs(point),20);

	

	
	[[balloonWindow contentView] setImage:image];

	NSRect startRect = NSZeroRect;
	startRect.origin = [NSEvent mouseLocation];
	startRect.size = NSMakeSize(10,10);

	
	
	
	
	
	
	if(   preferenceBoolValueForKey(@"animateBalloon")    == YES )
	{
		[balloonWindow setFrame:frame display:YES];

		//[balloonWindow orderFront:self];
		//[balloonWindow setFrame:frame display:YES animate:YES];
		
		[balloonWindow balloonAnimation:startRect to:frame speed:   preferenceFloatValueForKey(@"balloonSpeed") ];
		
	}else
	{
		[[SoundManager sharedSoundManager] playSoundWithName:@"soundBalloonOpen"];

		[balloonWindow setFrame:frame display:YES animate:NO];
		[balloonWindow orderFront:self];

	}
	

	[balloonWindow display];
	[balloonWindow flushWindow];

	
}

-(void)hideBalloon
{
	if( ![balloonWindow isVisible ] ) return;
	if(   preferenceBoolValueForKey(@"animateBalloon")  == YES )
	{
	[balloonWindow balloonCloseAnimationWithSpeed:   preferenceFloatValueForKey(@"balloonSpeed") ];
	}
	else
	{
		[[SoundManager sharedSoundManager] playSoundWithName:@"soundBalloonClose"];

	}
	
	[balloonWindow orderOut:self];
	
	balloonOwner = nil;
}

@end
