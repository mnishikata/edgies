//
//  DragView.m
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "DragView.h"
#import "DragButton.h"
#import "EdgiesLibrary.h"

#import "EdgiesLibrary.h"
#import "MNTabWindow.h"
#import "MiniDocument.h"

#define DRAG_MARGIN 16

#define MY_WINDOW (MNTabWindow*)[self window]
#define THEME_COLOR [[MY_WINDOW ownerDocument] documentColor]

@implementation DragView

-(void)awakeFromNib
{



	

	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}
-(void)dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}


-(void)drawRect:(NSRect)frame
{


	NSColor *fadedColor = [THEME_COLOR colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
	
	frame = [self bounds];
	drawDragArea( frame, [self window], [ (MNTabWindow*)[self window] tabPosition], THEME_COLOR, [MY_WINDOW tabOpenStatus ] );

	

}

- (void)mouseDown:(NSEvent *)theEvent
{
	////NSLog(@"mouse down");
	
	
	// check if clicked on an image
	
	NSPoint mouseLoc = [MY_WINDOW mouseLocationOutsideOfEventStream];  // mouse location in the window
	mouseLoc = [ self  convertPoint:mouseLoc fromView:[MY_WINDOW contentView]];

	////NSLog(@"mouse %f, %f", mouseLoc.x,  [ self frame].size.height - mouseLoc.y);

	TAB_POSITION tabPosition = (TAB_POSITION) [MY_WINDOW tabPosition];
	

	
	if( [MY_WINDOW tabOpenStatus ] == tab_tornOff )
	{
		
		if(  mouseLoc.y <  DRAG_MARGIN    ) 
		{
			if( mouseLoc.x <  DRAG_MARGIN   )
				[self trackMouse:d_leftBottom];
			
			else if( [ self frame].size.width  - mouseLoc.x <  DRAG_MARGIN    )
				[self trackMouse:d_rightBottom];
		}
		
		else if(  [ self frame].size.height - mouseLoc.y <  DRAG_MARGIN    ) 
		{
			if( mouseLoc.x <  DRAG_MARGIN   )
				[self trackMouse:d_leftTop];
			else if( [ self frame].size.width  - mouseLoc.x <  DRAG_MARGIN    )
				[self trackMouse:d_rightTop];
		}
		else
			[super mouseDown:theEvent];
		
		
	}else
	{
		
		if(  mouseLoc.y <  DRAG_MARGIN  && tabPosition != tab_top  ) 
		{
			if( mouseLoc.x <  DRAG_MARGIN  && tabPosition != tab_right )
				[self trackMouse:d_leftBottom];
			
			else if( [ self frame].size.width  - mouseLoc.x <  DRAG_MARGIN  &&   tabPosition != tab_left  )
				[self trackMouse:d_rightBottom];
		}
		
		else if(  [ self frame].size.height - mouseLoc.y <  DRAG_MARGIN   && tabPosition != tab_bottom  ) 
		{
			if( mouseLoc.x <  DRAG_MARGIN  &&  tabPosition != tab_right )
				[self trackMouse:d_leftTop];
			else if( [ self frame].size.width  - mouseLoc.x <  DRAG_MARGIN   &&   tabPosition != tab_left  )
				[self trackMouse:d_rightTop];
		}
		else
			[super mouseDown:theEvent];
		
		
	}
	
	

}

-(void)trackMouse:(DRAGMODE)mode
{
	

	////NSLog(@"trackMouse");
	NSEvent* theEvent;
	
	NSPoint op = [NSEvent mouseLocation];
		
	
	TIMEOUT_WHILE(30)
	{
		
		
		theEvent = [MY_WINDOW 
            nextEventMatchingMask:NSAnyEventMask
						untilDate:[NSDate dateWithTimeIntervalSinceNow:30.0]
						   inMode:NSEventTrackingRunLoopMode
						  dequeue:YES];
		
		if( theEvent == nil ) break;
		
		
		
		NSPoint currentMouseLoc = [NSEvent mouseLocation];
		
		int dif_x = currentMouseLoc.x - op.x;
		int dif_y = - currentMouseLoc.y + op.y;

		//////////////
		if( mode == d_rightBottom )
		{
			NSRect windowFrame = [MY_WINDOW frame] ;
			windowFrame.size.width += dif_x;
			windowFrame.size.height += dif_y;
			windowFrame.origin.y -= dif_y;

			
			if( windowFrame.size.height >[MY_WINDOW minSize].height + 50
				&& windowFrame.size.width > [MY_WINDOW minSize].width + 50 )
				[MY_WINDOW setFrame:windowFrame display:YES ];
			
			
			//[MY_WINDOW adjustTabs];

		}
		else if( mode == d_leftBottom )
		{
			NSRect windowFrame = [MY_WINDOW frame] ;
			windowFrame.size.width -= dif_x;
			windowFrame.origin.x += dif_x;
			windowFrame.size.height += dif_y;
			windowFrame.origin.y -= dif_y;
		
			if( windowFrame.size.height >[MY_WINDOW minSize].height + 50
				&& windowFrame.size.width > [MY_WINDOW minSize].width + 50 )
			[MY_WINDOW setFrame:windowFrame display:YES ];
		
			//[MY_WINDOW adjustTabs];

			
		}else if( mode == d_rightTop )
		{
			NSRect windowFrame = [MY_WINDOW frame] ;
			windowFrame.size.width += dif_x;
			
			windowFrame.size.height -= dif_y;

			
			if( windowFrame.size.height >[MY_WINDOW minSize].height + 50
				&& windowFrame.size.width > [MY_WINDOW minSize].width + 50 )
				[MY_WINDOW setFrame:windowFrame display:YES ];
			
			//[MY_WINDOW adjustTabs];

		}
		else if( mode == d_leftTop )
		{
			NSRect windowFrame = [MY_WINDOW frame] ;
			windowFrame.size.width -= dif_x;
			windowFrame.origin.x += dif_x;
			

			windowFrame.size.height -= dif_y;

			
			if( windowFrame.size.height >[MY_WINDOW minSize].height + 50
				&& windowFrame.size.width > [MY_WINDOW minSize].width + 50 )
				[MY_WINDOW setFrame:windowFrame display:YES ];
			
			//[MY_WINDOW adjustTabs];

		}
		
		////////////////////
		
		
		
		if ([theEvent type] == NSLeftMouseUp)
		{		
			
            break;
		}
		op = currentMouseLoc;
		
		
	}
	

	
	//redraw balloon
	//[MY_WINDOW createBalloonImage];
}


@end
