/* DynamicSharewareInformationMenu */

#import <Cocoa/Cocoa.h>

@interface DynamicSharewareInformationMenu  : NSMenu
{


	IBOutlet id window;
	IBOutlet id snField;
	IBOutlet id nameField;
}
- (void)menuNeedsUpdate:(NSMenu *)menu;
- (int)numberOfItems;
-(void)buy;
-(void)inputSerialNumber;
-(void)retrieveSerialNumber;
-(IBAction)buttonClicked:(id)sender;

@end
