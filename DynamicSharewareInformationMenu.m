#import "DynamicSharewareInformationMenu.h"
#import "PreferenceController.h"
#import "InstallLibrary.h"
#import "AppController.h"

#define APP_SERIAL @"OneRiverEdgiesSerialNumber"
#define APP_USERNAME @"OneRiverEdgiesRegisterdUsername"


extern BOOL registered;
extern BOOL expired;


@implementation DynamicSharewareInformationMenu



- (void)menuNeedsUpdate:(NSMenu *)menu
{
	
	unsigned int i, count = [menu numberOfItems];
	for (i = 0; i < count; i++) {
		[menu removeItemAtIndex:0];
	}
	
	NSMenuItem * item;
	
	if( !registered )
	{
		item = [[NSMenuItem  alloc] initWithTitle:NSLocalizedString(@"Buy License from Online Store",@"")
														action:nil keyEquivalent:@""];
		[item setTarget:self];
		[item setAction:@selector(buy)];
		[menu addItem:item];
		[item release];
		
		item = [[NSMenuItem  alloc] initWithTitle:NSLocalizedString(@"Input serial number",@"")
										   action:nil keyEquivalent:@""];
		
		[item setTarget:self];
		[item setAction:@selector(inputSerialNumber)];
		[menu addItem:item];
		[item release];
		
		
		
		item = [[NSMenuItem  alloc] initWithTitle:NSLocalizedString(@"Forgot serial number?",@"")
										   action:@selector(retrieveSerialNumber) keyEquivalent:@""];
		
		
		[item setTarget:self];
		[item setAction:@selector(retrieveSerialNumber)];
		[menu addItem:item];
		[item release];
	
	
	}else
	{
		NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
		
		
//		NSString* serialNumber = [ud objectForKey: APP_SERIAL ];
		
		NSString* registeredUsername = [ud objectForKey:APP_USERNAME];
		
		
		
		item = [[NSMenuItem  alloc] initWithTitle:NSLocalizedString(@"This program is licensed to:",@"")
										   action:nil keyEquivalent:@""];
		[menu addItem:item];
		[item release];
		
		item = [[NSMenuItem  alloc] initWithTitle:registeredUsername
										   action:nil keyEquivalent:@""];
		[menu addItem:item];
		[item release];
		
		item = [[NSMenuItem  alloc] initWithTitle:NSLocalizedString(@"Purchase Support",@"")
										   action:nil keyEquivalent:@""];
		[item setTarget:self];
		[item setAction:@selector(retrieveSerialNumber)];
		[menu addItem:item];
		[item release];
		
	}
}

- (int)numberOfItems
{
	[self setDelegate:self];

	return [super numberOfItems];
}


-(void)buy
{
	NSURL* url = [NSURL URLWithString:@"http://www.oneriver.jp/Store/"];
	[[NSWorkspace sharedWorkspace] openURL: url ];
}


-(void)inputSerialNumber
{
	
	[NSBundle loadNibNamed:@"Register" owner:self];
	
	[window center];
	[window makeKeyAndOrderFront:self];
	
}


-(void)retrieveSerialNumber
{
	//https://store.esellerate.net/support
	NSURL* url = [NSURL URLWithString:@"https://store.esellerate.net/support"];
	[[NSWorkspace sharedWorkspace] openURL: url ];
}

-(IBAction)buttonClicked:(id)sender
{
	//NSLog(@"buttonClicked");
	
	NSString* serialnumber = [snField stringValue];
	NSString* username = [nameField stringValue];

	if( serialnumber == nil || username == nil ) return;
	
	
	
	//chomp return code
	
	NSMutableString* musername = [NSMutableString stringWithString: username];
	NSMutableString* mserialnumber = [NSMutableString stringWithString: serialnumber];
	
	
	if( [mserialnumber rangeOfString:@"\n"].length != 0 )
	{
		[mserialnumber replaceOccurrencesOfString:@"\n" withString:@""
								 options:0 range:NSMakeRange(0, [mserialnumber length])];
		
	}
	
	
	if( [musername rangeOfString:@"\n"].length != 0 )
	{
		[musername replaceOccurrencesOfString:@"\n" withString:@""
								 options:0 range:NSMakeRange(0, [musername length])];
	
	}
	
	BOOL success = validSerialNumber(mserialnumber , musername);
	
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];


	
	
	if( success  ) // ok
	{
		[ud setObject:musername forKey:APP_USERNAME];
		[ud setObject:mserialnumber forKey: APP_SERIAL ];
		[ud setObject:musername forKey: APP_USERNAME ];
		[ud synchronize];

		if( checkSN() )
		{
		
		
			registered = YES;
			
			
			NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Thank you for purchasing Edgies.",@"")
											 defaultButton:NSLocalizedString(@"OK",@"")
										   alternateButton:@"" otherButton:@"" informativeTextWithFormat:@""];
			
			
			
			[alert runModal];
			
			[window orderOut:nil];
		}
	}else {
		
		NSRunAlertPanel(NSLocalizedString(@"Incorrect Serial Number",@""), @"", @"OK", nil, nil);
		[ud setObject:@"Edgies Demo User" forKey:@"p_author"];
		[ud removeObjectForKey: APP_SERIAL ];
		[ud removeObjectForKey: APP_USERNAME ];
	}
	
	
	[ud synchronize];



	
}
@end
