#import "AppController.h"

@implementation AppController


-(IBAction)clicked:(id)sender
{
	NSOpenPanel* panel = [NSOpenPanel openPanel];
	
	int ok = [panel runModalForTypes:[NSArray arrayWithObject:@"plist"]];
	
	if( ok != NSOKButton ) return;
	
	NSString* path = [[panel filenames] objectAtIndex:0];
	
	
	
	NSMutableDictionary* saveddocs;


	
	NSDictionary* plistDic = [[NSDictionary alloc] initWithContentsOfFile:path];
	
	NSLog(@"Loading %@",[plistDic description]);
	
	id _data = [plistDic objectForKey:@"documents"];
	if( _data != NULL ) //read documents from ud
	{
		
		saveddocs  = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData: _data ]];
		if( saveddocs == nil ) saveddocs = [NSDictionary dictionary];
		
		NSArray* keys = [saveddocs allKeys];
			
		unsigned hoge;
		
		for( hoge = 0; hoge < [saveddocs count]; hoge++ )
		{
			NSString* docName = [keys objectAtIndex:hoge];
			
			NSData* oneDocData = [saveddocs objectForKey:docName];
			{
				NSDictionary* oneDocDic = [NSKeyedUnarchiver unarchiveObjectWithData:oneDocData];
				
				NSString* title = [oneDocDic objectForKey:@"title"];
				NSData* content = [oneDocDic objectForKey:@"content"]; //portable
				
				NSAttributedString* attr = [self unpackPortable:[oneDocDic objectForKey:@"content"]];
				
				if( title != nil )  	[view insertText: title];
				[view insertText:@"\n"];
				if( attr != nil )		[view insertText: attr];
				[view insertText:@"\n"];
				//	NSLog(@"TITLE %@\nCONTENT %@",title,[attr string]);
				
				
				[AppController saveThisData:oneDocData withName:title];
			}

			
		}
		
		
	}	
}
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

}


-(NSMutableAttributedString*)unpackPortable:(NSData*)portableData
{
	
	NSFileWrapper* wrapper = [[NSFileWrapper alloc] initWithSerializedRepresentation:portableData];
	
	//check validity
	if( ! [wrapper isDirectory] )	return NULL;
	
	
	NSDictionary* aDic  = [wrapper fileWrappers];
	
	NSData* textData = [[aDic objectForKey:@"StringData"] regularFileContents];
	NSData* attrData = [[aDic objectForKey:@"AttributesData"] regularFileContents];
	
	
	NSString* plainText = [[[NSString alloc] initWithData:textData encoding:NSUTF8StringEncoding] autorelease];
	NSMutableAttributedString* unpackedString = [[ NSMutableAttributedString alloc ] initWithString:plainText];
	
	
	
	//unarchive
	
	NSKeyedUnarchiver* unarchiver; 
	unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:attrData];
	NSMutableDictionary* attributeSaver	= [unarchiver decodeObjectForKey:@"attributes"];
	[unarchiver finishDecoding];
	
	
	
	unsigned hoge;
	NSArray* keys = [attributeSaver allKeys];
	
	for( hoge = 0; hoge < [keys count]; hoge++ )
	{
		
		NSString* key = [keys objectAtIndex:hoge];
		//NSLog(@"key %@",key);
		NSRange effectiveRange = NSRangeFromString( key );
		NSDictionary* attributes = [attributeSaver objectForKey:key];
		
		[unpackedString setAttributes:attributes range:effectiveRange];
		
	}
	
	
	/*
	 //adjust graphic size for graphic resizable textView
	 NSRange range;
	 for( hoge = 0; hoge < [unpackedString length]; hoge++ )
	 {
		 id something = [unpackedString attribute:MNGraphicSizeAttributeName 
										  atIndex:hoge
							longestEffectiveRange:&range inRange:NSMakeRange(0,[unpackedString length])];
		 
		 if( something != NULL)
			 
		 {
			 NSLog(@"graphic size found");
			 
			 NSTextAttachment* selectedImage = [unpackedString attribute:NSAttachmentAttributeName 
																 atIndex:hoge
														  effectiveRange:NULL];
			 NSSize newSize = NSSizeFromString(something);
			 
			 
			 // resize graphic
			 NSImage* image = [[selectedImage attachmentCell] image];
			 [image setScalesWhenResized:YES];
			 
			 NSBitmapImageRep* imageRep = [image bestRepresentationForDevice:nil] ;
			 
			 NSSize size = [imageRep size];
			 NSLog(@"size %@",NSStringFromSize(size));
			 
			 [imageRep setSize:newSize];
			 [image setSize:newSize];
			 
			 [[selectedImage attachmentCell] setImage:image];
			 
			 [unpackedString addAttribute:NSAttachmentAttributeName value:selectedImage
									range:NSMakeRange(hoge, 1)];
			 
		 }
		 
		 hoge = NSMaxRange(range);
	 }
	 
	 */
	
	
	[unarchiver release];
	[wrapper release];
	[unpackedString autorelease];
	
	return unpackedString;
}


+(void)saveThisData:(NSData*)data withName:(NSString*)name
{
	/*
	 NSMutableDictionary* saveddocs;
	 NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	 
	 id _data = [ud objectForKey:@"documents"];
	 if( _data != NULL )
	 saveddocs  = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData: _data ]];
	 
	 else
	 saveddocs = [NSMutableDictionary dictionary];
	 
	 
	 [saveddocs setObject:data forKey:name];
	 
	 [ud setObject:[NSKeyedArchiver archivedDataWithRootObject:saveddocs ] forKey:@"documents"];
	 
	 
	 */

	
	
	NSSavePanel* panel = [NSSavePanel savePanel];
	int ok = [panel runModal ];
	
	if( ok != NSFileHandlingPanelOKButton ) return;
	
	
	NSString* filename = [NSString stringWithFormat:@"%@.edgy",[panel filename]];

	
	
	[data writeToFile:filename atomically:YES];
	
	/*
	 NSDictionary* lockFilename = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:NSFileImmutable];
	 [[NSFileManager defaultManager] changeFileAttributes:lockFilename atPath:SCRATCH_FOLDER];
	 */
	
	
	//	NSLog(@"saved %@. Count %d",name, [[saveddocs allKeys] count ]);
}

@end
