/* AppDelegate */

#import <Cocoa/Cocoa.h>
#import "AliasInspector.h"
#import "FaviconInspector.h"

#import "GraphicInspector.h"
#import "CheckboxInspector.h"

#import "AttachmentCellConverter.h"

@interface AppDelegate : NSObject
{
	AliasInspector* aliasInspector;
	FaviconInspector* faviconInspector;

	AttachmentCellConverter* attachmentCellConverter;
	GraphicInspector* graphicInspector;
	CheckboxInspector* checkboxInspector;
	
	IBOutlet id MISC_UD_CONTROLLER;
	


}
@end
