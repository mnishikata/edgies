//
//  TextView(printing).h
//  SampleApp
//
//  Created by __Name__ on 18/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PrintingTextView : NSTextView

{
	int		currentPageNumber;
	int		totalPageNumber;
	
	PrintingTextView* pview;
	NSMutableDictionary* rectForPageCache;
}
- (id)initWithFrame:(NSRect)frame ;
-(void)dealloc;
- (void)awakeFromNib;
+(PrintingTextView*)printingTextViewModel:(id)sender;
- (BOOL)knowsPageRange:(NSRangePointer)aRange;
- (void)didChangeText;
- (NSRect)rectForPage:(int)pageNumber;
-(NSRect)properPageRectFor:(NSRect)aRect;
- (NSPoint)locationOfPrintRect:(NSRect)aRect;
- (void)print:(id)sender;
- (void)drawRect:(NSRect)aRect;

@end
