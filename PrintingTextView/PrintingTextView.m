//
//  TextView(printing).m
//  SampleApp
//
//  Created by __Name__ on 18/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "PrintingTextView.h"
#import "AppController.h"
#import "PrintLibrary.h"

@implementation PrintingTextView



- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		
		//[self awakeFromNib];
		
		pview = NULL;
    }
    return self;
}



-(void)dealloc
{
	[pview release];
	[super dealloc];
	
}


- (void)awakeFromNib
{
	
	[rectForPageCache release];
	rectForPageCache = [[NSMutableDictionary alloc] init];
	
	
	

}

+(PrintingTextView*)printingTextViewModel:(id)sender
{
	
	float	contentWidth =  documentWidth()  ;
	
	
	PrintingTextView* pTextView
		= [[PrintingTextView alloc]
								initWithFrame:NSMakeRect(0,0,contentWidth  , 200)];
	
	[pTextView setVerticallyResizable:YES];
	[pTextView setAutoresizingMask:NSViewHeightSizable];
	[[pTextView layoutManager] replaceTextStorage: [sender textStorage]];
	[pTextView sizeToFit];
	
	return [pTextView autorelease];
}


- (BOOL)knowsPageRange:(NSRangePointer)aRange
{
	
	// managing printing info
	NSPrintOperation *op = [NSPrintOperation currentOperation];
	NSPrintInfo *pInfo = [op printInfo];
	
	
	unsigned width;
	unsigned height;
	width = [pInfo paperSize].width - [pInfo leftMargin] - [pInfo rightMargin];
	height = [pInfo paperSize].height - [pInfo topMargin] - [pInfo bottomMargin];
	
	NSRect r = NSZeroRect;
	NSRect previousRect = NSMakeRect(0,-1,0,0);
	
	unsigned hoge = 1;
	while( 1 )
	{
		
		r = NSMakeRect(0, NSMaxY(previousRect) +1 , width, height );
		
		
		r = [self properPageRectFor:r];
		////NSLog(@"%f, %f page rect %@",[self bounds].size.height, NSMaxY([self bounds]), NSStringFromRect(r));
		
		if(  NSMaxY([self bounds]) < r.origin.y )
			break;
		
		previousRect = r;
		hoge++;
	}
	
	totalPageNumber =  hoge-1;
	
	*aRange = NSMakeRange(1, hoge-1);
	////NSLog(@"%d Range %@", hoge,NSStringFromRange(*aRange) );
	
	
	return YES;
}

- (void)didChangeText
{
	[rectForPageCache removeObjectForKey:[NSNumber numberWithInt:currentPageNumber]];
		
	[super didChangeText];
}

- (NSRect)rectForPage:(int)pageNumber
{
//	//NSLog(@"%@ %d",[rectForPageCache description], pageNumber);
	id value = [rectForPageCache objectForKey:[NSNumber numberWithInt:pageNumber]] ;
	if( value != NULL )
	{
	//	//NSLog(@"use cache");
		return NSRectFromString( value );
	}
//	//NSLog(@"Not use cache");

	
	
	
	// managing printing info
//	NSPrintOperation *op = [NSPrintOperation currentOperation];
	
	NSPrintInfo *pInfo = [NSPrintInfo sharedPrintInfo];
	
	
	float width;
	float height;
	//width = [pInfo paperSize].width  - [pInfo leftMargin] - [pInfo rightMargin];
	//height = [pInfo paperSize].height - [pInfo topMargin] - [pInfo bottomMargin];
	
	width =  documentWidth()   ;
	height =  documentHeight()   ;
	
	
	NSRect r = NSZeroRect;
	NSRect previousRect = NSMakeRect(0,-1,0,0);
	
	unsigned hoge = 1;
	while( 1 )
	{
		
		r = NSMakeRect(0, NSMaxY(previousRect) +1 , width, height );
		
		
		r = [self properPageRectFor:r];
		
		
		if( hoge == pageNumber )
			break;
		
		if(  ! NSIntersectsRect( r, [self bounds] ) )
			break;
		
		previousRect = r;
		hoge++;
	}
	////NSLog(@"page %d  rect %@",pageNumber, NSStringFromRect(r) );
	
	
	if( ! NSIntersectsRect( r, [self bounds] ) )
		return NSZeroRect;
	
	
	currentPageNumber = pageNumber;
	
	
	//Thus r was calculated. However, return full paper size rect to make the whole area draw area.	
	NSRect retunValue = NSMakeRect(r.origin.x, r.origin.y, [pInfo paperSize].width, [pInfo paperSize].height );
	
	[rectForPageCache setObject:NSStringFromRect(retunValue) forKey:[NSNumber numberWithInt:pageNumber]];
	
	return retunValue;

	
}

-(NSRect)properPageRectFor:(NSRect)aRect
	// cut paper rect into glyph range to be printed

{
	
	// return if empty
	
	if( [[self layoutManager] numberOfGlyphs] == 0 ) return aRect;
	
	
	
	
	
	
	
//	NSPrintOperation *op = [NSPrintOperation currentOperation];
	NSPrintInfo *pInfo = [NSPrintInfo sharedPrintInfo];
	
	NSRect actualDrawingRectForPage = NSMakeRect( aRect.origin.x, aRect.origin.y , 0,0 );
	actualDrawingRectForPage.size = [pInfo paperSize];
	actualDrawingRectForPage.size.width -=  leftMargin()  +  rightMargin() ;
	actualDrawingRectForPage.size.height -= topMargin() +  bottomMargin(); 
		
	
	
	//modify last line
	
	NSRect rectForCheck = NSMakeRect(0, NSMaxY(actualDrawingRectForPage), actualDrawingRectForPage.size.width, 1  );
	NSRange range = [[self layoutManager] glyphRangeForBoundingRect:rectForCheck
													inTextContainer:[self textContainer]];
	NSRect lastLineRect = [[self layoutManager] lineFragmentRectForGlyphAtIndex:range.location
																 effectiveRange:NULL];
	
	if( lastLineRect.origin.y != actualDrawingRectForPage.origin.y && lastLineRect.origin.y != 0 )
		actualDrawingRectForPage.size.height = lastLineRect.origin.y - actualDrawingRectForPage.origin.y;
	
	////NSLog(@"RECT %@",NSStringFromRect(actualDrawingRectForPage));
	
	// if graphic is bigger than the page, dont crop
	if(  actualDrawingRectForPage.size.height <= 0 )
		actualDrawingRectForPage = aRect;
			
	return actualDrawingRectForPage;
}

- (NSPoint)locationOfPrintRect:(NSRect)aRect
{
	NSPrintOperation *op = [NSPrintOperation currentOperation];
	NSPrintInfo *pInfo = [op printInfo];
	
	NSSize paperSize = [pInfo paperSize];
	
	NSPoint p= [super locationOfPrintRect:aRect];
	////NSLog(@"point %@",NSStringFromPoint(p));
	
	p.y = paperSize.height - aRect.origin.y - aRect.size.height;
	p.x = 0;
	return p;
}
/*
//-(void)setPrintPageNumber:(BOOL)flag margin:(float)margin format:(NSMutableAttributedString*)format
{
	[pageNumberFormat release];
	
	printPageNumber = flag;
	pageNumberMargin = margin;
	pageNumberFormat = format;
	
	[pageNumberFormat retain];
	//NSLog(@"setting print page number %@",[pageNumberFormat string]);
	
}*/

- (void)print:(id)sender
{
	
	if( ![sender isKindOfClass:[NSTextView class]] ) 
	{
		
		BOOL printFromMemoList = ! [[self window] isKindOfClass:[MNTabWindow class]];
		
		if( printFromMemoList == NO )
		{
			int levelBuffer = [[self window] level];
			int windowLevelBuffer =  preferenceIntValueForKey(@"windowLevel")  ;
			int windowLevelOpenedBuffer =  preferenceIntValueForKey(@"windowLevelOpened")  ;
			
			[[self window] setLevel:NSNormalWindowLevel];
			[[self window] setValue:[NSNumber numberWithInt:  NSNormalWindowLevel] forKey: @"windowLevel"];
			[[self window] setValue:[NSNumber numberWithInt:  NSNormalWindowLevel] forKey: @"windowLevelOpened"];
			
			[[self window] display];
			[NSApp activateIgnoringOtherApps:YES];
			
			[[PrintingTextView printingTextViewModel:self] print:self];

			[[self window] setLevel:levelBuffer];
			
			[[self window] setValue:[NSNumber numberWithInt:  windowLevelBuffer] forKey: @"windowLevel"];
			[[self window] setValue:[NSNumber numberWithInt:  windowLevelOpenedBuffer]
							 forKey: @"windowLevelOpened"];
		}else
		{

			[NSApp activateIgnoringOtherApps:YES];
			[[PrintingTextView printingTextViewModel:self] print:self];
		
		}
		
		
	}else
	{
		[super print:self];
	}
	
	
}

- (void)drawRect:(NSRect)aRect
{
	
	
	if ( ![NSGraphicsContext currentContextDrawingToScreen]  ) // for printing
	{
		
		// managing printing info
		NSPrintOperation *op = [NSPrintOperation currentOperation];
		NSPrintInfo *pInfo = [op printInfo];
		
		
		
		///# debug
		////NSLog(@"Paper size %@",NSStringFromSize( [pInfo paperSize] ) );
		////NSLog(@"Imageable rect %@",NSStringFromRect([pInfo imageablePageBounds]) ); 
		////NSLog(@"Margin t%f b%f l%f r%f ",[pInfo topMargin],[pInfo bottomMargin],
		//	  [pInfo leftMargin],[pInfo rightMargin] );
		
		//[NSBezierPath fillRect:[pInfo imageablePageBounds]];
		
		
		//# debug end	
		
		
		
		// aRect represents paper size.
		// Cut aRect into appropriate size here.
		
		NSRect actualDrawingRectForPage = [self properPageRectFor:aRect];
		
		
		
		//
		
		NSAffineTransform* printaffine = [NSAffineTransform transform];
		// move axis
		[printaffine translateXBy:  leftMargin()   yBy:  topMargin()   ]; 
		[printaffine concat];	
		
		
		//
		
		
		NSRange printingGlyphRange = [[self layoutManager] 
							glyphRangeForBoundingRect:actualDrawingRectForPage
									  inTextContainer:[self textContainer]];
		
		// drawing glyphs	
		[[self layoutManager] drawGlyphsForGlyphRange:printingGlyphRange 
											  atPoint:[self textContainerOrigin]];
		
		
		[printaffine invert];
		[printaffine concat];
		
		
		
		//draw page number
		////NSLog(@"print page num ? %d",printPageNumber);
		if(  preferenceBoolValueForKey(@"printPageNumber")  == YES )
		{
			//get page number
			
			NSMutableAttributedString* pageStr;
			pageStr = 
				[[NSMutableAttributedString alloc] initWithAttributedString:   pageNumberFormat()   ];
			
			
			// add page #
			NSRange range = [[pageStr string] rangeOfString:@"#"];
			if( range.length != 0 )
				[pageStr replaceCharactersInRange:range 
									   withString:[NSString stringWithFormat:@"%d", currentPageNumber]];
			
			
			
			// add total page *
			range = [[pageStr string] rangeOfString:@"*"];
			if( range.length != 0 )
				[pageStr replaceCharactersInRange:range 
									   withString:[NSString stringWithFormat:@"%d", totalPageNumber]];
			
			
			
			
			//align // aRect = page rect
			NSPoint pointToDrawPageNumberAt = NSZeroPoint;
			pointToDrawPageNumberAt.x = [pInfo paperSize].width /2 - [pageStr size].width /2;
			pointToDrawPageNumberAt.y = aRect.origin.y + [pInfo paperSize].height -
				[pageStr size].height -   preferenceFloatValueForKey(@"pageNumberMargin")   ;
			
			//convert to relative coordinate
			
			////NSLog(@"page %d  %@",[pageStr string],NSStringFromPoint(pointToDrawPageNumberAt) );
			
			
			
			[pageStr drawAtPoint:pointToDrawPageNumberAt];
			
			
			[pageStr release];
		}
		

		
		
		
		return;
	}else
	{
	
		
		[super drawRect:aRect]; // display -- not printing
	
		/*
		if( pview == NULL ) pview = [PrintingTextView printingTextViewModel:self];
		
		
		//draw page break

		
		int page = 1;
		NSRect pageRect = NSZeroRect;
		
		
		while(1)
		{
			
			pageRect = [pview rectForPage:page];
			
			//NSLog(@"page %@",NSStringFromRect(pageRect));

			
			if(  NSIsEmptyRect( pageRect )  || NSMaxY( aRect ) < pageRect.origin.y )
				break;
			
			
			if( NSIntersectsRect( pageRect , aRect ) ) //drawe page line
			{
								

				[[NSColor blueColor ] set];
				[NSBezierPath setDefaultLineWidth:2.0];
				[NSBezierPath strokeLineFromPoint:NSMakePoint(0, NSMaxY(pageRect))
			toPoint:NSMakePoint([[self enclosingScrollView] frame].size.width, NSMaxY(pageRect)) ]; 
				
			}
			
			page++;
		}
		 */
		
	}
	
	
}



@end
