//
//  MNIconizedCell.m
//  WordProcessor
//
//  Created by __Name__ on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//


#import "MNCheckboxCell.h"
#import "ColorCheckbox.h"

#import "AppController.h"
#define PREF [ (AppController*)[[NSApplication sharedApplication] delegate] preferenceController]


#define DEFAULT_ATTRIBUTES  [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:11.0], NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, NULL]

@implementation MNCheckboxCell


 - (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
 
{
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: cellFrame.origin.x + cellFrame.size.width/2
					 yBy: cellFrame.origin.y + cellFrame.size.height/2 ]; 
	
	// rotate axis
	//[affine rotateByDegrees:180];
	[affine scaleXBy:1.0 yBy:-1.0];
	[affine concat];
	
	
	
	

	NSPoint point = cellFrame.origin;
	
	point.y += cellFrame.size.height;
	
	NSImage* image = [ColorCheckbox colorCheckbox:[self color] checkFlag:( [self state]==NSOnState? YES:NO )];
	//[image compositeToPoint:point operation:NSCompositeSourceOver];
	
	[image setFlipped:NO];
	cellFrame.origin = NSZeroPoint;

	[image drawAtPoint:NSMakePoint( (-cellFrame.size.width/2 )  , (-cellFrame.size.height/2 ) )
					 fromRect:cellFrame operation:NSCompositeSourceOver fraction:1.0];
	
	
	NSAttributedString* name = [[[NSAttributedString alloc] initWithString:[self name]
																attributes:DEFAULT_ATTRIBUTES] autorelease];

	point.x += 17;
	point.y -= [name size].height;
	
//	[name drawAtPoint:point];
	
	
	[context restoreGraphicsState];
}

-(NSImage*)image
{
	NSRect frame = NSZeroRect;
	frame.size = [self cellSize];
	NSImage* img = [[[NSImage alloc] initWithSize:frame.size] autorelease];
	
	[img setFlipped:YES];
	[img lockFocus];
	[self drawWithFrame:frame inView:nil];
	[img unlockFocus];

	return img;
	
}




- (id)initWithAttachment:(NSTextAttachment*)attachment
{
    self = [super init];
    if (self) {

		NSColor* DEF_C = [NSColor blueColor];
		
		//NSFileWrapper* wrapper = [attachment fileWrapper];
		[self setAttachment:attachment];
		
		[self setState:NSOffState];
		
		/*
		[self setTitle: [NSString stringWithFormat:@"%f,%f,%f,%f",
			[DEF_C redComponent],[DEF_C greenComponent],[DEF_C blueComponent],[DEF_C alphaComponent] ] ];
		*/
		[self setColor:DEF_C andName:@"Check"];
    }
    return self;
}

- (NSSize)cellSize
{
	
	NSSize size = NSMakeSize(16,17);
	
	/*
	NSAttributedString* name = [[[NSAttributedString alloc] initWithString:[self name]
																attributes:DEFAULT_ATTRIBUTES] autorelease];
	*/
//	size.width += [name size].width;
	
	return size;
}

-(void)dealloc
{

	
	
	[super dealloc];
}

/*
-(void)setState:(BOOL)flag
{
	state = flag;
	
}

-(BOOL)state
{
	return state;
	
}
*/

-(void)setColor:(NSColor*)newColor andName:(NSString*)name
{

	[self setTitle:[NSString stringWithFormat:@"%f,%f,%f,%f,%@",
		[newColor redComponent],[newColor greenComponent],[newColor blueComponent],[newColor alphaComponent],name ] ];

	
}

-(NSColor*)color
{

	NSString* colorname = [self title];
	NSArray* array = [colorname componentsSeparatedByString:@","];
	
	float red = [[array objectAtIndex:0] floatValue];
	float green = [[array objectAtIndex:1] floatValue];
	float blue = [[array objectAtIndex:2] floatValue];
	float alpha = [[array objectAtIndex:3] floatValue];
	
	
	return  [NSColor colorWithCalibratedRed:red green:green blue:blue alpha:alpha];
	
}

-(NSString*)name
{
	
	NSString* colorname = [self title];
	NSArray* array = [colorname componentsSeparatedByString:@","];

	if( [array count] > 4 )
	return  [array objectAtIndex:4];
	
	else return @"";
	
}




- (BOOL)wantsToTrackMouse
{
	return YES;	
}





- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView untilMouseUp:(BOOL)flag
{
	if([theEvent type] == NSOtherMouseDown) // de iconize
	{
		NSBeep();
		
			
	}else if([theEvent type] == NSLeftMouseDown) 
	{
		//[PREF playSoundClickButton];

		( [self state] == NSOnState? [self setState:NSOffState]:[self setState:NSOnState] ); 
		
		[aTextView display];
		
		NSFileWrapper* wrapper = [[self attachment] fileWrapper];
			
		[wrapper setIcon:[self image  ]];
		
		//[PREF playSoundReleaseButton];

	}
	return NO;
}


@end
