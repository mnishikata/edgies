#import "InstallLibrary.h"
#import "EdgiesLibrary.h"
#import "PreferenceController.h"

//#import "AQDataExtensions.h"

//#import "EWSLib.h"
//#import "validate.h"
#import "NDAlias.h"
#import "FileEntityTextAttachmentCell.h"
#import "AliasAttachmentCell.h"
#import "ClipboardArchiveAttachmentCell.h"
#import "CheckboxAttachmentCell.h"
#import "MNCheckboxCell.h"

#define APP_SERIAL @"OneRiverEdgiesSerialNumber"
#define APP_USERNAME @"OneRiverEdgiesRegisterdUsername"



void setDefaultPreferencesValues()
{
	syncronizePreferences();

	NSDictionary* dic = defaultPreferencesDictionary();
	
	NSArray* allkeys = [dic allKeys];
	
	unsigned int i, count = [allkeys count];
	for (i = 0; i < count; i++) {
		NSString* key = [allkeys objectAtIndex:i];
		
		if( preferenceValueForKey(key) == nil )
		{
			id  val = [dic objectForKey:key ];
			
			setPreferenceValueForKey(val,key);
		}
	}
	

	if( preferenceValueForKey(@"documentFolderPath") == nil )
	{
		
		setPreferenceValueForKey( DEFAULT_DOCUMENT_FOLDER ,@"documentFolderPath");

	}


	syncronizePreferences();
}


NSDictionary* defaultPreferencesDictionary()
{	
	NSString* path = [[NSBundle mainBundle] pathForResource:@"EdgiesDefaultSetting" ofType:@"plist"];
	NSDictionary* dic = [[[NSDictionary alloc] initWithContentsOfFile:path ] autorelease];
	
	return dic;
}

void loadAdditionalUDWithFilename(NSString* name )
{
	NSArray* udarray;
	
	
	NSString *path;
	if (path = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"])  
	{
		
		udarray = [[NSArray alloc] initWithContentsOfFile: path];
		
		////NSLog(@"AdditionalUD_AttachmentCellConverter %@",[udarray description]);
		[[PreferenceController standardUserDefaults] registerAdditionalUD:udarray];
		
		
		[udarray release];
		
	}
	
	
	

}


NSAttributedString* convertEdgies1point1(NSAttributedString* attr)
{
	NSMutableAttributedString* mattr = [[[NSMutableAttributedString alloc] initWithAttributedString:attr] autorelease];

	NSRange range;
	unsigned hoge;
	
	
	// Convert archive
	for( hoge = 0; hoge < [mattr length];  )
	{
		id something = [mattr attribute:DropAnythingAttributeName
								atIndex:hoge
						 effectiveRange:&range ];
		
		
		if( something != nil )
		{
			NSAttributedString* converted = [ClipboardArchiveAttachmentCell clipboardArchiveAttachmentCellAttributedStringWithDropAnythingValue:something];
			[mattr replaceCharactersInRange:range withAttributedString:converted];
			
		}
		
		hoge = NSMaxRange(range);
		
	}
	
	
	//Convert Alias Button
	for( hoge = 0; hoge < [mattr length];  )
	{
		id something = [mattr attribute:NSAttachmentAttributeName
								atIndex:hoge
						 effectiveRange:&range ];
		
		if( something != nil &&  [[something attachmentCell] isMemberOfClass:[AliasAttachmentCell class]] )
		{
			NDAlias* alias =  [mattr attribute:NSLinkAttributeName
													atIndex:hoge
											 effectiveRange:nil ];
			if( alias != nil )
			{
				NSAttributedString* converted = 
				[AliasAttachmentCell aliasAttachmentCellAttributedStringWithPath: [alias path]  ];
				[mattr replaceCharactersInRange:range withAttributedString:converted];
			}
		}
		
		hoge = NSMaxRange(range);
		
	}
	
	
	//Convert File Button
	for( hoge = 0; hoge < [mattr length];  )
	{
		id something = [mattr attribute:NSAttachmentAttributeName
								atIndex:hoge
						 effectiveRange:&range ];
		
		if( something != nil &&   [[something attachmentCell] isMemberOfClass:[FileEntityTextAttachmentCell class]])
		{
			NDAlias* alias =  [mattr attribute:NSLinkAttributeName
									   atIndex:hoge
								effectiveRange:nil ];
			if( alias != nil )
			{
				NSAttributedString* converted = 
				[AliasAttachmentCell aliasAttachmentCellAttributedStringWithPath: [alias path]  ];
				[mattr replaceCharactersInRange:range withAttributedString:converted];
			}
		}
		
		hoge = NSMaxRange(range);
		
	}
	
	
	
	//Convert Checkbox Button
	for( hoge = 0; hoge < [mattr length];  )
	{
		id something = [mattr attribute:NSAttachmentAttributeName
								atIndex:hoge
						 effectiveRange:&range ];
		
		if( something != nil &&   [[something attachmentCell] isMemberOfClass:[MNCheckboxCell class]])
		{
			MNCheckboxCell* cell = [something attachmentCell];
			
			NSAttributedString* converted = [CheckboxAttachmentCell checkboxWithColor:[cell color] state:[cell state]];
			[mattr replaceCharactersInRange:range withAttributedString:converted];
		}
		
		hoge = NSMaxRange(range);
		
	}
	
	
	return mattr;
}

/*
void installEngine()
{
	return;
//	
	
	NSString *mypath;
	
	
	mypath = [[NSBundle mainBundle] pathForResource:@"EWSMacCompress.tar.gz" ofType:nil];
	
	
	
	if( mypath == nil )
	{
		NSRunAlertPanel(@"Application was unable to install the eSellerate engine. Please contact the vendor", @"", @"OK", nil, nil);
		return;
	}
	

	
	// EWS SDK Install Engine From Path function     
	OSStatus error =  eWeb_InstallEngineFromPath([mypath UTF8String]);
	
	if (error < 0 ) 
		NSRunAlertPanel(@"Application was unable to install the eSellerate engine. Please contact the vendor", @"", @"OK", nil, nil);
	
}

void nslogInvalidSerialNumber()
{
	//NSLog(@"ps %@",simpleRokku());
	NSString* tes = @"EDGE-PPQF-HD40-F0M2-1EJC-62PG";
	NSString* enc = encodeStr(tes);
	//NSLog(@"ENC %@",enc);
	NSString* dec = decodeStr(enc);
	//NSLog(@"RESULT %@",dec);
	
	//NSLog(@"ps %@",simpleRokku());
	tes = @"EDGIESN000-3AB4-QG0E-PB1F-05Y1-7J01";
	enc =  encodeStr(tes);
	//NSLog(@"ENC %@",enc);
	dec = decodeStr(enc);
	//NSLog(@"RESULT %@",dec);
}

BOOL checkSN()
{
	
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	
	
    NSString* serialNumber = [ud objectForKey: APP_SERIAL ];
	
	NSString* username = [ud objectForKey:APP_USERNAME];
	
	
	
	
	if( ! [serialNumber hasPrefix:@"EDGIESN"] ) return NO;

		
	//check disabled serial
	
	NSArray* disabledSerials = [ NSArray arrayWithObjects:
		//EDGIESN000-1L01-G891-CJMP-45F2-5D45
		@"<afcaae31 74ac11ec badabb24 b96c59af 8f796c41 0e9e21d2 a0926702 f9ac9ca4 6c91549e 9aa26201 ce5d1c39 64d4e17a b6495730 2ab2ade6 7c0d0d7f 11d30530 8331df89 69027ce4 570a996c 99a5f112 >",
		
		//EDGE-PPQF-HD40-F0M2-1EJC-62PG
		@"<48882773 591e7388 cf63d3b9 7361fda4 0e4363ae 06ef96d8 1d277c5f 8f817884 6a92ce6d 71160e1f 4275d78c f8b51931 c43c1369 0749a82e abaf586c 79ca0a85 >",
			
		nil];
	
	
	
	
	
	int bar;
	for( bar = 0; bar < [disabledSerials count]; bar++ )
	{
		NSString* str = [disabledSerials objectAtIndex:bar];
		str =  decodeStr(str);
			
		
		if( [serialNumber isEqualToString: str ] ) return NO;
	
	}
	
	
	
	
	///////////
	
	
	
	
    if( serialNumber != nil && ![serialNumber isEqualToString:@""] && 
		username != nil && ![ username isEqualToString:@""] )
    {
		
		if( validSerialNumber( serialNumber , username) ) return YES;

		
		else return NO;

		
		
	}else return NO;

}


BOOL validSerialNumber(NSString* aSn , NSString* aName)
{
	
	
	eSellerate_DaysSince2000 validSN;
	
	
	
	validSN =  eWeb_ValidateSerialNumber ([aSn UTF8String],		// Serial number (read from User Defaults)
										  [aName UTF8String],	// No name based key in this example
										  nil,					// No extra data in this example
										  "56260");			// Publisher Key (used for stronger validation)
	
	
	return validSN;
}
	


#pragma mark -
#pragma mark Encryption

NSString* encodeStr(NSString* strToEncode)
{
	NSData* data = [strToEncode dataUsingEncoding:NSUTF8StringEncoding];
	data = [data dataEncryptedWithPassword: simpleRokku() ];
	return [data description];

	
}
NSString* decodeStr(NSString* strToDecode)
{
	NSData* data = [strToDecode propertyList];
	data = [data dataDecryptedWithPassword:simpleRokku()];
	NSString* string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	return [string autorelease];

	
}

NSString* simpleRokku() // iwayuru angou password
{
	float hoge = 0;
	NSMutableString* str1 = [NSMutableString stringWithString: [PreferenceController className] ];
	
	hoge = 10.0/[str1 length];
	hoge++;
	
	[str1 replaceOccurrencesOfString:@"e" withString:[[NSNumber numberWithFloat:hoge] stringValue]
							 options:0
							   range:NSMakeRange(0, [str1 length]) ];
	return str1;
}*/

#pragma mark -

NSDate* installDate()
{
	NSDate* today = [NSDate date];
	NSDate* date = preferenceValueForKey(@"InstalledDate");
	if( date == nil || [date compare:today] == NSOrderedDescending )
	{
		setPreferenceValueForKey(today,@"InstalledDate");
		date = today;
	}
	return date;
}

BOOL firstRun()
{
	if( preferenceBoolValueForKey(@"exist") ) return NO;
	
	setPreferenceValueForKey([NSNumber numberWithBool:YES],@"exist");
	
	syncronizePreferences();
	return YES;
}


/*
void showDemoDialogue()
{
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
	
	
	
	NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Shareware Information",@"")
									 defaultButton:NSLocalizedString(@"Not Now",@"")
								   alternateButton:NSLocalizedString(@"Online Store",@"")
									   otherButton:@"" 
						 informativeTextWithFormat:NSLocalizedString(@"Thank you for using Edgies.  To continue using over 15 days, please purchase a license at the online store.",@"")];
	int result = [alert runModal];
	
	
	if( result == NSAlertAlternateReturn )
	{
		
		visitWebsite();
		
		
	}	
}*/


void visitWebsite()
{
	
	NSURL* url = [NSURL URLWithString:@"http://www.oneriver.jp/"];
	[[NSWorkspace sharedWorkspace] openURL: url ];
	
}