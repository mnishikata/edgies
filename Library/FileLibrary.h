/*
 *  FileLibrary.h
 *  Cog
 *
 *  Created by __Name__ on 07/05/01.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#include <Cocoa/Cocoa.h>

NSMutableAttributedString* unpackPortable(NSData* portableData);
NSData* portableDateFromRange(NSTextView* textView, NSRange range );
NSMutableDictionary* attributesDataFromRange(NSTextView* textView, NSRange range);

///

BOOL  pathCanAcceptDrop(NSString* targetPath);
BOOL  pathIsFolder(NSString* targetPath);


void postNotification(NSString* path);


NSArray* dropRTFD(NSData* data , NSString* targetPath);
NSArray* dropRTF(NSData* data ,NSString* targetPath);

NSArray* dropPlainText(NSString* string , NSString* targetPath);
NSArray* dropTIFF(NSData* data ,NSString* targetPath ,NSString* filename);
NSArray* dropBookmark(NSString* urlString , NSString* targetPath , NSString* filename);
NSArray* dropFile(NSString* file ,NSString* targetPath , NSString* operation , NSString* newName);
NSArray* dropFiles(NSArray* files , NSString* targetPath , NSString* operation);
