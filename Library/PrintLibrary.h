//
//  PrintUtil.h
//  SampleApp
//
//  Created by __Name__ on 06/09/23.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>



void updateMinimumMargin(void);

float topMargin(void);

float bottomMargin(void);

float leftMargin(void);
float rightMargin(void);

float documentWidth(void);

float documentHeight(void);

BOOL printPageNumber(void);

float pageNumberMargin(void);

NSAttributedString* pageNumberFormat(void);

float topMargin_min(void);

void setTopMargin_min(float dummy);

float bottomMargin_min(void);

void setBottomMargin_min(float dummy);

float leftMargin_min(void);

void setLeftMargin_min(float dummy);
float rightMargin_min(void);
void setRightMargin_min(float dummy);
