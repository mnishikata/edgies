//
//  ScreenUtil.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "ScreenUtil.h"

#define MARGIN 20
//#define MENUBAR_HEIGHT [[NSApp mainMenu] menuBarHeight] /* 22 */ defined in EdgiesLibrary


BOOL mouseIsInScreen(NSScreen* screen)
{
	
	NSRect sf = [screen frame];
	NSPoint mp = [NSEvent mouseLocation];
	
	if( sf.origin.x <= mp.x && mp.x <= NSMaxX(sf) && 
		sf.origin.y <= mp.y && mp.y <= NSMaxY(sf) )
		return YES;
	else return NO;
	
	//return NSPointInRect( [NSEvent mouseLocation] , [screen frame] );
}


NSScreen* screenWhereMouseIs(void)
{
	NSArray* sa = [NSScreen screens];
	NSScreen* sc;
	int hoge;
	for( hoge = 0; hoge < [sa count]; hoge++ )
	{
		sc = [sa objectAtIndex:hoge];
		if( mouseIsInScreen(sc)  )
		{
			break;
		}
	}
	
	return sc;
}


BOOL pointIsInScreen(NSPoint point, NSScreen* screen)
{
	return NSPointInRect( point , [screen frame] );
}


NSScreen* screenWherePointIs(NSPoint point)
{
	NSArray* sa = [NSScreen screens];
	NSScreen* sc;
	int hoge;
	for( hoge = 0; hoge < [sa count]; hoge++ )
	{
		sc = [sa objectAtIndex:hoge];
		if(  pointIsInScreen(point,sc) )
		{
			break;
		}
	}
	
	return sc;
}


NSScreen* screenWhereRectIs(NSRect rect)
{
	
	// center de handan
	NSPoint centerPoint = NSMakePoint(rect.origin.x + rect.size.width/2, rect.origin.y + rect.size.height/2);
	BOOL flag = NO;
	
	NSArray* sa = [NSScreen screens];
	NSScreen* sc;
	
	int hoge;
	for( hoge = 0; hoge < [sa count]; hoge++ )
	{
		sc = [sa objectAtIndex:hoge];
		if(  pointIsInScreen( centerPoint,sc) )
		{
			flag = YES;
			break;
		}
	}
	
	// rect de handan
	if( flag == NO )
	{
		for( hoge = 0; hoge < [sa count]; hoge++ )
		{
			sc = [sa objectAtIndex:hoge];
			if( NSIntersectsRect([sc frame],rect) )
			{
				break;
			}
		}
	}
	
	return sc;
}
int screenIDWhereRectIs(NSRect rect)
{
	NSArray* screens =  [NSScreen screens];
	
	return [screens indexOfObject: screenWhereRectIs(rect) ];
}
int screenIDWhereMouseIs(void)
{
	NSArray* screens =  [NSScreen screens];
	
	return [screens indexOfObject: screenWhereMouseIs() ];
}
NSScreen* screenWithScreenID(int screenID)
{
	NSArray* screens =  [NSScreen screens];
	
	return [screens objectAtIndex: screenID ];
}







SCREEN_REGION regionWherePointIs(NSPoint point)
{
	NSScreen* screen = screenWherePointIs(point);
	NSRect screenFrame = [screen frame];
	
	//detect nearest wall
	int top, bottom, left, right; 
	
	top = NSMaxY(screenFrame) - point.y;
	bottom = point.y - screenFrame.origin.y;
	right = NSMaxX(screenFrame) - point.x;
	left = point.x - screenFrame.origin.x;
	
	SCREEN_REGION min = bottomArea;
	int minValue = bottom;
	
	if( top < bottom )
	{
		min = topArea;
		minValue = top;
	}
	if( left < minValue )
	{
		min = leftArea;
		minValue = left;
	}
	if( right < minValue )
	{
		min = rightArea;
		minValue = right;
	}
	
	if( minValue > MARGIN ) return centerArea;
	
	
	return min;
}


SCREEN_REGION regionWhereMouseIs(void)
{

	NSPoint mouseLoc = [NSEvent mouseLocation];

	return regionWherePointIs(mouseLoc);
	
}
int regionIDAtMousePoint(void)
{
	return regionIDAtPoint([NSEvent mouseLocation]);
	
}
int regionIDAtPoint(NSPoint point)
// return unique id showing the region the point belongs to
{

	SCREEN_REGION region = regionWherePointIs(point);

	NSArray* screenArray = [NSScreen screens];
	NSScreen* screen = screenWherePointIs(point) ;
	
	int idx = [screenArray indexOfObject:screen];
	if( idx == NSNotFound ) idx = 1000;
	
	idx = idx * 10 + region;
	
	return idx;
		
}


BOOL mouseIsOnTheEdges(void)
{
	NSScreen* screen = screenWhereMouseIs();
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect screenFrame = [screen frame];

	screenFrame.origin.x ++;
	screenFrame.origin.y +=2;
	screenFrame.size.height -= 2;
	screenFrame.size.width -= 2;
	
	
	return ! NSPointInRect( mouseLoc , screenFrame);
}


BOOL mouseIsOnTheRightEdge(void)
{
	NSScreen* screen = screenWhereMouseIs();
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect screenFrame = [screen frame];
	
	screenFrame.size.width -= 1;
	
	return ! NSPointInRect( mouseLoc , screenFrame);
}
BOOL mouseIsOnTheLeftEdge(void)
{
	NSScreen* screen = screenWhereMouseIs();
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect screenFrame = [screen frame];
	
	screenFrame.origin.x ++;
	
	screenFrame.size.width -= 1;	
	
	
	return ! NSPointInRect( mouseLoc , screenFrame);
}
BOOL mouseIsOnTheTopEdge(void)
{
	NSScreen* screen = screenWhereMouseIs();
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect screenFrame = [screen frame];
	
	
	screenFrame.size.height -= 1;	
	
	
	return ! NSPointInRect( mouseLoc , screenFrame);
}
BOOL mouseIsOnTheBottomEdge(void)
{
	NSScreen* screen = screenWhereMouseIs();
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect screenFrame = [screen frame];
	
	screenFrame.origin.y +=2;
	screenFrame.size.height -= 2;
	
	
	return ! NSPointInRect( mouseLoc , screenFrame);
}





NSRect convertRectNotToStickOutFromScreenWithEdgeMargin(NSRect rect,  NSScreen* screen, int edgeMargin)

	// edgeMargin „ÅåÊ≠£„Å™„Çâ„ÄÅÁîªÈù¢„Çà„ÇäÂÜÖÂÅ¥„Å´
{
	NSRect screenFrame = [screen frame];
	
	if( MENUBAR_SCREEN == screen )
	{
		screenFrame.size.height -= MENUBAR_HEIGHT;
		
	}
	
	
	if( rect.origin.x < screenFrame.origin.x +edgeMargin  )
		rect.origin.x = screenFrame.origin.x +edgeMargin;
	
	if( rect.origin.y < screenFrame.origin.y +edgeMargin )
		rect.origin.y = screenFrame.origin.y +edgeMargin;
	
	if( NSMaxX( rect ) > NSMaxX( screenFrame ) -edgeMargin) 
		rect.origin.x = NSMaxX( screenFrame ) - rect.size.width - edgeMargin;
	
	if( NSMaxY( rect ) > NSMaxY( screenFrame ) -edgeMargin )
		rect.origin.y = NSMaxY( screenFrame ) - rect.size.height - edgeMargin;


	return rect;
}
NSRect convertRectToStickOnEdgeWithEdgeMargin(NSRect rect,  NSScreen* screen, SCREEN_REGION side, int edgeMargin)

	// edgeMargin „ÅåÊ≠£„Å™„Çâ„ÄÅÁîªÈù¢„Çà„ÇäÂÜÖÂÅ¥„Å´
{
	NSRect screenFrame = [screen frame];
	
	if( MENUBAR_SCREEN == screen )
	{
		screenFrame.size.height -= MENUBAR_HEIGHT;
		
	}
		
	
	if( side == centerArea ) return rect;
	
	
	//
	
	
	else if( side == bottomArea )
		rect.origin.y = screenFrame.origin.y + edgeMargin;
	
	else if( side == leftArea )
		rect.origin.x = screenFrame.origin.x + edgeMargin;
	
	else if( side == topArea )
		rect.origin.y = NSMaxY( screenFrame ) - rect.size.height - edgeMargin;

	else if( side == rightArea )
		rect.origin.x = NSMaxX( screenFrame ) - rect.size.width - edgeMargin;

	
	
	return rect;
}
NSRect convertRectWithPointToMouse(NSPoint pointInFrame, NSRect frameInScreen)
//NSRect alignPointToMouse(NSPoint pointInFrame, NSRect frameInScreen)
// point is a position in window's BOUNDS
{

		
	NSScreen* screen = screenWhereMouseIs();
	
		
		
	NSPoint mouseLoc = [NSEvent mouseLocation];
	
	NSPoint targetPointInScreen = NSZeroPoint;
	
	targetPointInScreen.x = frameInScreen.origin.x + pointInFrame.x;
	targetPointInScreen.y = frameInScreen.origin.y + pointInFrame.y;
	
	int deltaX = mouseLoc.x - targetPointInScreen.x ;
	int deltaY = mouseLoc.y - targetPointInScreen.y ;
	
	frameInScreen.origin.x += deltaX;
	frameInScreen.origin.y += deltaY;
	
	// if target point is over menubar, do something
	if( screen == MENUBAR_SCREEN )
	{
		if( frameInScreen.origin.y > NSMaxY( [screen frame] )- MENUBAR_HEIGHT -5 )
		{
			frameInScreen.origin.y = NSMaxY( [screen frame] )- MENUBAR_HEIGHT -5 ;
		}
	}

	return frameInScreen;
	
}


#pragma mark

int edgeRightTo(int edgeID)
{
	//get first digit
	int _tabPosition = 0xF & edgeID;
	int _screenID = edgeID / 0x10;
	
	
	if( _tabPosition == tab_top ||  _tabPosition == tab_bottom || _tabPosition == tab_right )
		return _screenID*0x10 + tab_left;
	
	
	
	
	if( _tabPosition == tab_left )
	{
		NSArray* sa = [NSScreen screens];
		NSScreen* screen = [sa objectAtIndex:_screenID];
		
		NSRect frame = [screen frame];
		float x = NSMaxX(frame);
		
		int hoge;
		for( hoge = 0; hoge < [sa count]; hoge++ )
		{
			if( hoge != _screenID )
			{
				screen = [sa objectAtIndex:hoge];
				
				float nx = [screen frame].origin.x;
				if( x -5 < nx && nx < x+5  )
				{
					//screen is right screen
					return hoge*0x10 + tab_left;
				}
			}
			
		}
	}
	
	
	return NSNotFound;
	
}


int edgeLeftTo(int edgeID)
{
	//get first digit
	int _tabPosition = 0xF & edgeID;
	int _screenID = edgeID / 0x10;
	
	
	if( _tabPosition == tab_top ||  _tabPosition == tab_bottom || _tabPosition == tab_left)
		return _screenID*0x10 + tab_right;
	
	if( _tabPosition == tab_right)
	{
		NSArray* sa = [NSScreen screens];
		NSScreen* screen = [sa objectAtIndex:_screenID];
		
		NSRect frame = [screen frame];
		float x = frame.origin.x;
		
		int hoge;
		for( hoge = 0; hoge < [sa count]; hoge++ )
		{
			if( hoge != _screenID )
			{
				screen = [sa objectAtIndex:hoge];
				
				float nx = NSMaxX( [screen frame] );
				if( x -5 < nx && nx < x+5  )
				{
					//screen is right screen
					return hoge*0x10 + tab_right;
				}
			}
			
		}
	}
	
	
	return NSNotFound;
	
}
int edgeBelow(int edgeID)
{
	//get first digit
	int _tabPosition = 0xF & edgeID;
	int _screenID = edgeID / 0x10;
	
	
	if( _tabPosition == tab_right ||  _tabPosition == tab_bottom || _tabPosition == tab_left)
		return _screenID*0x10 + tab_top;
	
	if( _tabPosition == tab_top )
	{
		NSArray* sa = [NSScreen screens];
		NSScreen* screen = [sa objectAtIndex:_screenID];
		
		NSRect frame = [screen frame];
		float y = NSMaxY(frame);
		
		int hoge;
		for( hoge = 0; hoge < [sa count]; hoge++ )
		{
			if( hoge != _screenID )
			{
				screen = [sa objectAtIndex:hoge];
				
				float ny = [screen frame].origin.y;
				if( y -5 < ny && ny < y+5  )
				{
					//screen is right screen
					return hoge*0x10 + tab_bottom;
				}
			}
			
		}
	}
	
	
	return NSNotFound;
	
}


int edgeAbove(int edgeID)
{
	//get first digit
	int _tabPosition = 0xF & edgeID;
	int _screenID = edgeID / 0x10;
	
	
	if( _tabPosition == tab_top ||  _tabPosition == tab_left || _tabPosition == tab_right)
		return _screenID*0x10 + tab_bottom;
	
	if( _tabPosition == tab_bottom )
	{
		NSArray* sa = [NSScreen screens];
		NSScreen* screen = [sa objectAtIndex:_screenID];
		
		NSRect frame = [screen frame];
		float y = frame.origin.y;
		
		int hoge;
		for( hoge = 0; hoge < [sa count]; hoge++ )
		{
			if( hoge != _screenID )
			{
				screen = [sa objectAtIndex:hoge];
				
				float ny = NSMaxY( [screen frame] );
				if( y -5 < ny && ny < y+5  )
				{
					//screen is right screen
					return hoge*0x10 + tab_top;
				}
			}
			
		}
	}
	
	
	return NSNotFound;
	
}


