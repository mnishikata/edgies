/*
 *  FileLibrary.m
 *  Cog
 *
 *  Created by __Name__ on 07/05/01.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#import "FileLibrary.h"
#import "EdgiesLibrary.h"
#import "NSString (extension).h"
#import "NDAlias.h"
#import "SpotlightUtil.h"
#import "NDAlias+AliasFile.h"
#import <WebKit/WebKit.h>

NSMutableAttributedString* unpackPortable(NSData* portableData)
{
	
	NSFileWrapper* wrapper = [[NSFileWrapper alloc] initWithSerializedRepresentation:portableData];
	
	//check validity
	if( ! [wrapper isDirectory] )	return NULL;
	
	
	NSDictionary* aDic  = [wrapper fileWrappers];
	
	NSData* textData = [[aDic objectForKey:@"StringData"] regularFileContents];
	NSData* attrData = [[aDic objectForKey:@"AttributesData"] regularFileContents];
	
	
	NSString* plainText = [[[NSString alloc] initWithData:textData encoding:NSUTF8StringEncoding] autorelease];
	NSMutableAttributedString* unpackedString = [[ NSMutableAttributedString alloc ] initWithString:plainText];
	
	
	
	//unarchive
	
	NSKeyedUnarchiver* unarchiver; 
	unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:attrData];
	NSMutableDictionary* attributeSaver	= [unarchiver decodeObjectForKey:@"attributes"];
	[unarchiver finishDecoding];
	
	
	
	unsigned hoge;
	NSArray* keys = [attributeSaver allKeys];
	
	for( hoge = 0; hoge < [keys count]; hoge++ )
	{
		
		NSString* key = [keys objectAtIndex:hoge];
		////NSLog(@"key %@",key);
		NSRange effectiveRange = NSRangeFromString( key );
		NSDictionary* attributes = [attributeSaver objectForKey:key];
		
		[unpackedString setAttributes:attributes range:effectiveRange];
		
	}
	
	
	/*
	 //adjust graphic size for graphic resizable textView
	 NSRange range;
	 for( hoge = 0; hoge < [unpackedString length]; hoge++ )
	 {
		 id something = [unpackedString attribute:MNGraphicSizeAttributeName 
										  atIndex:hoge
							longestEffectiveRange:&range inRange:NSMakeRange(0,[unpackedString length])];
		 
		 if( something != NULL)
			 
		 {
			 //NSLog(@"graphic size found");
			 
			 NSTextAttachment* selectedImage = [unpackedString attribute:NSAttachmentAttributeName 
																 atIndex:hoge
														  effectiveRange:NULL];
			 NSSize newSize = NSSizeFromString(something);
			 
			 
			 // resize graphic
			 NSImage* image = [[selectedImage attachmentCell] image];
			 [image setScalesWhenResized:YES];
			 
			 NSBitmapImageRep* imageRep = [image bestRepresentationForDevice:nil] ;
			 
			 NSSize size = [imageRep size];
			 //NSLog(@"size %@",NSStringFromSize(size));
			 
			 [imageRep setSize:newSize];
			 [image setSize:newSize];
			 
			 [[selectedImage attachmentCell] setImage:image];
			 
			 [unpackedString addAttribute:NSAttachmentAttributeName value:selectedImage
									range:NSMakeRange(hoge, 1)];
			 
		 }
		 
		 hoge = NSMaxRange(range);
	 }
	 
	 */
	
	
	[unarchiver release];
	[wrapper release];
	[unpackedString autorelease];
	
	return unpackedString;
}



NSData* portableDateFromRange(NSTextView* textView, NSRange range )
{
	// create plain text data
	NSAttributedString* textStr = [[textView textStorage] attributedSubstringFromRange:range];
	NSData* aTextData = [[textStr string] dataUsingEncoding:NSUTF8StringEncoding];
	
	
	
	//create attributes archive
	
	NSMutableData* archivedData = [NSMutableData data];
	NSKeyedArchiver* archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:archivedData];
	
	[archiver encodeObject:attributesDataFromRange(textView, range) forKey:@"attributes"];
	////NSLog(@"portable...att");
	
	
	[archiver finishEncoding];
	[archiver release];
	
	
	
	
	NSFileWrapper* wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL] autorelease];
	
	//Add data to wrapper
	[wrapper   addRegularFileWithContents:aTextData preferredFilename:@"StringData"];
	[wrapper   addRegularFileWithContents:archivedData preferredFilename:@"AttributesData"];
	
	
	
	return [wrapper serializedRepresentation];
	
	
}


NSMutableDictionary* attributesDataFromRange(NSTextView* textView, NSRange range)
{
	NSMutableDictionary* attributesSaver = [NSMutableDictionary dictionary];
	
	unsigned hoge;
	NSDictionary* attributes;
	NSRange longestEffectiveRange;
	
	for(hoge = range.location; hoge < NSMaxRange(range);   )
	{
		
		attributes = [[textView textStorage] attributesAtIndex:hoge 
									 longestEffectiveRange:&longestEffectiveRange inRange:range];
		
		NSRange attributesRange;
		attributesRange.location = longestEffectiveRange.location - range.location;
		attributesRange.length = longestEffectiveRange.length;
		
		
		//update icon
		id textAttachment = [attributes objectForKey:NSAttachmentAttributeName];
		if(   textAttachment != nil )
		{
			NSImage*  cellImage = [[textAttachment attachmentCell] image];
			NSFileWrapper* fw = [textAttachment fileWrapper];
			
			if( fw != nil && cellImage != nil )
			{
				[fw setIcon:cellImage ];
			}
			
		}
		
		
		
		
		
		//delete impersistentAttributes
		NSDictionary* _attributes = [NSDictionary dictionaryWithDictionary:attributes];
		//[_attributes removeObjectsForKeys: impersistentAttributes ];
		// do not delete when copy or saving
		
		[attributesSaver setObject:_attributes forKey:NSStringFromRange(attributesRange)];
		
		hoge = NSMaxRange(longestEffectiveRange);
	}
	
	return attributesSaver;
}

#pragma mark -


void postNotification(NSString* path)
{
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ItemDroppedOnFolderInEdgiesNotification
														object:path ];	
}



NSArray* dropRTFD(NSData* data , NSString* targetPath)
{
	NSDictionary* docAttr;
	NSString* title;
	
	NSAttributedString* atr = [[[NSAttributedString alloc] initWithRTFD:data documentAttributes:&docAttr] autorelease];


	title = [docAttr objectForKey:NSTitleDocumentAttribute];


	if( title == nil || [title isEqualToString:@""] )
	{
		
		title = [[atr string] firstWords: 5];
		if( [title isEqualToString:@""] ) title = @"Edgies Text";
		
			
	}
	
	NSFileWrapper* fw = [[[NSFileWrapper alloc ] initWithSerializedRepresentation:data] autorelease];	
	
	
	NSString* filename = [title stringByAppendingPathExtension:@"rtfd"];
	filename = [filename uniqueFilenameForFolder:targetPath];
	NSString* fullPath = [targetPath stringByAppendingPathComponent:filename];
	
	BOOL flag = 	[fw writeToFile:fullPath
						 atomically:YES  updateFilenames:YES];
	
	
	
	if( flag == NO ) return nil;
	
	
	postNotification( fullPath );
	
	return [NSArray arrayWithObject:fullPath];
	
}

NSArray* dropRTF(NSData* data ,NSString* targetPath)
{
	NSAttributedString* atr = [[[NSAttributedString alloc] initWithRTF:data
											documentAttributes:nil] autorelease];
	
	NSString* filename = [[atr string] firstWords: 5];
	if( [filename isEqualToString:@""] ) filename = @"Edgies Text";

	filename = [filename stringByAppendingPathExtension:@"rtf"];
	
	filename = [filename uniqueFilenameForFolder:targetPath];
	
	NSString* fullPath = [targetPath stringByAppendingPathComponent:filename];

	
	BOOL flag = 	[data writeToFile:fullPath
						   atomically:YES];
	
	
	if( flag == NO ) return nil;
	
	postNotification( fullPath );

	return [NSArray arrayWithObject:fullPath];
	
}

NSArray* dropPlainText(NSString* string , NSString* targetPath)
{
	NSString* filename = [string firstWords: 5];
	if( [filename isEqualToString:@""] ) filename = @"Edgies Text";
	
	filename = [filename stringByAppendingPathExtension:@"txt"];

	
	filename = [filename uniqueFilenameForFolder:targetPath];
	
	NSString* fullPath = [targetPath stringByAppendingPathComponent:filename];

	
	BOOL flag = 	[string writeToFile:fullPath
							 atomically:YES];
	
	
	if( flag == NO ) return nil;
	
	postNotification( fullPath );

	return [NSArray arrayWithObject:fullPath];
	
}

NSArray* dropTIFF(NSData* data ,NSString* targetPath ,NSString* filename)
{

	NSString* fullPath = [targetPath stringByAppendingPathComponent:filename];

	BOOL flag = 	[data writeToFile:fullPath
						   atomically:YES];
	
	
	if( flag == NO ) return nil;
	
	postNotification( fullPath );

	return [NSArray arrayWithObject:fullPath];
	
}

NSArray* dropBookmark(NSString* urlString , NSString* targetPath , NSString* filename)
{
	
	
	filename = [[filename safeFilename] stringByAppendingPathExtension:@"url"];
	filename = [filename uniqueFilenameForFolder: targetPath];
	
	NSString* fullPath = [targetPath stringByAppendingPathComponent:filename];

	
	NSString* content = @"[InternetShortcut]\nURL=%@\n";
	content = [NSString stringWithFormat:content, urlString ]; 
	
	BOOL flag = [content writeToFile:fullPath
						  atomically:YES];
	
	if( flag == NO ) return nil;
	
	postNotification( fullPath );

	
	return [NSArray arrayWithObject:fullPath];
}




NSArray* dropFile(NSString* file ,NSString* targetPath , NSString* operation , NSString* newName)
{
	//operation = NSWorkspaceLinkOperation, etc

	//NSLog(@"dropping file %@",file);
	//NSLog(@"targetPath %@",targetPath);
	//NSLog(@"operation %@",operation);
	//NSLog(@"newName %@",newName);

	
	BOOL success = NO;
	

	
	//check if folder
	BOOL isDir;
	
	BOOL targetExists = [[NSFileManager defaultManager] fileExistsAtPath:targetPath isDirectory:&isDir ];
	
	if( !targetExists  )
		return nil;
	
	
	if( isDir )
		if( [[NSWorkspace sharedWorkspace] isFilePackageAtPath:targetPath ] )
		{
			//check targetPath accept file
			BOOL acceptable;
			OSStatus err = LSCanURLAcceptURL (
											  [NSURL URLWithString:file],
											  [NSURL URLWithString:targetPath],
											  kLSRolesAll,
											  kLSAcceptDefault,
											  &acceptable
											  );
			
			
			if( acceptable )
			{
				//NSLog(@"launching");
				[[NSWorkspace sharedWorkspace] openFile:file withApplication:targetPath];
				
				return nil	;
				
			}
			
			return nil;
		}
	
		
	
	
	//NSLog(@"drop on folder");
	// drop on folder
	
		
	NSFileManager* fm = [NSFileManager defaultManager];
	
	NSString* destFullPath =	[[targetPath stringByAppendingPathComponent: newName ] uniquePathForFolder];
	
	
	if( [operation isEqualToString:  NSWorkspaceLinkOperation ] )
	{
		NDAlias* alias = [NDAlias aliasWithPath: file];
		BOOL successLink = [alias writeToFile: destFullPath];
		


		
		if( successLink == NO )
		{
			[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
			
			NSRunAlertPanel (NSLocalizedString(@"Error", @""),
							 [NSString stringWithFormat: NSLocalizedString(@"Failed to create an alias for %@", @"") , file],
							 nil, nil, nil);
			
			success = NO;
		}
		
		
	}else if( [operation isEqualToString:  NSWorkspaceMoveOperation ] )
	{
		success = [[NSFileManager defaultManager] movePath:file toPath:destFullPath handler:nil];
		
	}else if( [operation isEqualToString:  NSWorkspaceCopyOperation  ] )
	{
		success = [[NSFileManager defaultManager] copyPath:file toPath:destFullPath handler:nil];
	
	}
	
	if( !success ) return nil;
		 postNotification( destFullPath );

	
	
	
	return [NSArray arrayWithObject:destFullPath];
}



NSArray* dropFiles(NSArray* files , NSString* targetPath , NSString* operation)
{
	BOOL success = NO;
	unsigned piyo;
	NSMutableArray* savedFiles = [NSMutableArray array];
	
	for( piyo = 0; piyo < [files count]; piyo ++ )
	{
		NSString* file = [files objectAtIndex:piyo];
		id obj = dropFile( file , targetPath , operation , [file lastPathComponent] );
		
		if( obj )
		{
			success = YES;
			[savedFiles addObject: [obj objectAtIndex:0]];
		}
	}
	if( !success ) return nil;
	
	return savedFiles;
}



NSArray* dropWebArchive(NSData* data , NSString* targetPath )
{
	// create title
	NSString* filename;
	WebArchive* wa = [[WebArchive alloc] initWithData: data];
	NSString* title = [[wa mainResource] frameName];
	
	if( title == nil ) title = [[[wa mainResource] URL] path];
	filename = [[NSString stringWithFormat:@"%@.webarchive",title] safeFilename];
	
	[wa release];
	
	
	NSString* fullPath = [targetPath stringByAppendingPathComponent:filename];
	fullPath = [fullPath uniquePathForFolder];
	
	BOOL flag = 	[data writeToFile:fullPath
						   atomically:YES];
	
	
	if( flag == NO ) return nil;
	
	postNotification( fullPath );
	
	return [NSArray arrayWithObject:fullPath];
}

BOOL  pathIsFolder(NSString* targetPath)
{
	BOOL isDir;
	
	BOOL targetExists = [[NSFileManager defaultManager] fileExistsAtPath:targetPath isDirectory:&isDir ];
	
	if( !targetExists  )
		return NO;
	
	
	if( isDir )
		if( ![[NSWorkspace sharedWorkspace] isFilePackageAtPath:targetPath ] )
			return YES;
	
	return NO;
}

BOOL  pathCanAcceptDrop(NSString* targetPath)
{
	BOOL isDir;

	BOOL targetExists = [[NSFileManager defaultManager] fileExistsAtPath:targetPath isDirectory:&isDir ];
	
	if( !targetExists  )
		return NO;
	

	if( isDir )
		//if( ![[NSWorkspace sharedWorkspace] isFilePackageAtPath:targetPath ] )
			return YES;
	
	
	NSArray* utiTree = getUTITree( targetPath );
	//NSLog(@"utiTree %@ for %@",[utiTree description], targetPath);
	if( [ utiTree containsObject: @"public.executable" ] || 
		[ utiTree containsObject:@"public.folder"]) return YES;
	
	return NO;
}

