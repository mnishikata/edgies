//
//  DrawLibrary.m
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "DrawLibrary.h"
#import "ThemeManager.h"
#import "ColorCheckbox.h"

#define TITLE_ATTRIBUTE [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12],NSFontAttributeName , NULL ]

NSColor* convertColorSpace(NSColor* color)
{
	return 	[color colorUsingColorSpaceName:NSCalibratedRGBColorSpace];

}

#pragma mark -
NSAttributedString* MNTruncate(NSAttributedString* ogn, int inWidth)
{
	////NSLog(@"truncate %@ %d",[ogn string],inWidth);

	if( inWidth < 2  ) return [[[NSAttributedString alloc] initWithString:@""] autorelease];
		if ([ogn size].width < inWidth) return ogn;
		
	
	NSAttributedString *result = ogn;

	
	NSMutableAttributedString *newString = [[[NSMutableAttributedString alloc] init] autorelease];
	int curLength = [ogn length] - 1;	// start by chopping off at least one
	
	[newString appendAttributedString:ogn];
	while ([newString size].width > inWidth && [newString length] >1 )
	{
		unichar dots = 0x2026;
		
		NSRange range = NSMakeRange( curLength - 1, 2);	// replace 2 characters with "‚Äö√Ñ√∂‚àö√ë¬¨‚àÇ"
		[newString replaceCharactersInRange:range withString:[NSString stringWithCharacters:&dots  length:1]];
		curLength--;
		
		if( curLength == 0 )
		{
			newString = [[[NSAttributedString alloc] initWithString:@""] autorelease];
			break;
		}
	}
	result = newString;
	
	
	
	
	return result;
	
	
	
}


void __drawDragArea(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, NSColor* fadedColor, TAB_OPEN_STATUS tabOpenStatus)
{	//
	////NSLog(@"drawDragArea ss ");


	ThemeManager* themeManager = [ThemeManager sharedManager];
	NSImage* tearOffImage = [themeManager tearOffImage];

	[color set];

	
	
	if( tabOpenStatus == 3 )
	{
		
		if( tabPosition == tab_right )
		{
			NSRect edgeRect = NSMakeRect(frame.origin.x,frame.origin.y, 5, frame.size.height);
			
			//NSRect restRect = NSMakeRect(frame.origin.x+5,frame.origin.y, frame.size.width -5, frame.size.height);	
			
			[NSBezierPath fillRect:frame];


//			[[NSColor colorWithPatternImage: tearOffImage ] set];
			
//			[NSBezierPath fillRect:edgeRect];

			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(frame.origin.x,frame.origin.y+hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);

		}
		
		else if( tabPosition == tab_left )
		{
			NSRect edgeRect = NSMakeRect(NSMaxX(frame) -5,frame.origin.y, 5, frame.size.height);
			
			//NSRect restRect = NSMakeRect(frame.origin.x,frame.origin.y, frame.size.width -5, frame.size.height);	
			
			[NSBezierPath fillRect:frame];
			

			
			
			NSGraphicsContext *context = [NSGraphicsContext currentContext];
			[context saveGraphicsState];
			
			
			NSAffineTransform* affine = [NSAffineTransform transform];
			
			// move axis
			[affine translateXBy:  NSMaxX(frame)
							 yBy: NSMaxY(frame)  ]; 
			
			// rotate axis
			[affine rotateByDegrees:  180];
			
			
			
			[affine concat];	
			
			// draw
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(0,hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			[context restoreGraphicsState];
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);


		}
		
		else if( tabPosition == tab_bottom )
		{
			NSRect edgeRect = NSMakeRect(frame.origin.x,frame.origin.y, 5, frame.size.width);
			
			//NSRect restRect = NSMakeRect(frame.origin.x,frame.origin.y, frame.size.width , frame.size.height-5);	
			
			[NSBezierPath fillRect:frame];

			
			/////
			
			
			NSGraphicsContext *context = [NSGraphicsContext currentContext];
			[context saveGraphicsState];
			
			
			NSAffineTransform* affine = [NSAffineTransform transform];
			
			
			
			// move axis
			[affine translateXBy: frame.origin.x
							 yBy: NSMaxY(frame) ]; 
			
			// rotate axis
			[affine rotateByDegrees:  -90];
			
			
			[affine concat];	
			
			// draw
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{

				[tearOffImage drawAtPoint:NSMakePoint(0,hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
			
			}
			
			[context restoreGraphicsState];
			
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);

		}

		else if( tabPosition == tab_top)
		{
			NSRect edgeRect = NSMakeRect(frame.origin.x,frame.origin.y, 5, frame.size.width);
			
			//NSRect restRect = NSMakeRect(frame.origin.x,frame.origin.y+5, frame.size.width , frame.size.height );	
			
			[NSBezierPath fillRect:frame];

			
			/////
			
			
			NSGraphicsContext *context = [NSGraphicsContext currentContext];
			[context saveGraphicsState];
			
			
			NSAffineTransform* affine = [NSAffineTransform transform];
			
			
			
			// move axis
			[affine translateXBy: NSMaxX(frame)
							 yBy: frame.origin.y]; 
			
			// rotate axis
			[affine rotateByDegrees:  90];
			
			
			[affine concat];	
			
			// draw
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(0,0+hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			[context restoreGraphicsState];
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);

		}
	

	}else
	{
		[NSBezierPath fillRect:frame];
		
		_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);

		
	}
	
	////NSLog(@"drawDragArea e ");
	
}

void _drawDragCorner(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, TAB_OPEN_STATUS tabOpenStatus)
{
	_drawSides(frame, window,  tabPosition,  color,  tabOpenStatus);

	
	id themeManager = [ThemeManager sharedManager];
	
	NSImage* cornerRT= [themeManager cornerRT];
	NSImage* cornerLT= [themeManager cornerLT];
	NSImage* cornerRB= [themeManager cornerRB];
	NSImage* cornerLB= [themeManager cornerLB];
	
	
	
	///// drawing draggable corner

	[color set];
	
	
	
	if( tabOpenStatus == tab_tornOff )
	{
		/*
		[NSBezierPath fillRect:NSMakeRect(frame.origin.x, frame.origin.y ,[cornerLB size].width ,[cornerLB size].height)];
		[NSBezierPath fillRect:NSMakeRect(NSMaxX(frame)-[cornerRB size].width,frame.origin.y,[cornerRB size].width ,[cornerLB size].height)];
		[NSBezierPath fillRect:NSMakeRect(frame.origin.x,NSMaxY(frame) - [cornerLT size].height,[cornerLT size].width ,[cornerLT size].height)];
		[NSBezierPath fillRect:NSMakeRect(NSMaxX(frame)-[cornerRT size].width,NSMaxY(frame)-[cornerRT size].height,[cornerRT size].width ,[cornerRT size].height )];
		*/
		
		[cornerLB compositeToPoint:frame.origin operation:NSCompositeSourceOver];
		[cornerRB compositeToPoint:NSMakePoint(NSMaxX(frame)-[cornerLB size].width, frame.origin.y)
						 operation:NSCompositeSourceOver];
		[cornerLT compositeToPoint:NSMakePoint(frame.origin.x,NSMaxY(frame)-[cornerLT size].height)
						 operation:NSCompositeSourceOver];
		[cornerRT compositeToPoint:NSMakePoint(NSMaxX(frame)-[cornerRT size].width,NSMaxY(frame)-[cornerRT size].height)
						 operation:NSCompositeSourceOver];
		
	}else
	{
		
		if(  tabPosition == tab_top  ) 
		{
			[cornerLT compositeToPoint:NSMakePoint( frame.origin.x , NSMaxY(frame) -[cornerLT size].height)
							 operation:NSCompositeSourceOver];
			[cornerRT compositeToPoint:NSMakePoint( NSMaxX(frame) -[cornerRT size].width, NSMaxY(frame) -[cornerRT size].height)
							 operation:NSCompositeSourceOver];
		}
		else if(  tabPosition == tab_bottom  )  
		{
			
			[cornerLB compositeToPoint:frame.origin operation:NSCompositeSourceOver];
			[cornerRB compositeToPoint:NSMakePoint( NSMaxX(frame) -[cornerLB size].width,frame.origin.y)
							 operation:NSCompositeSourceOver];
		}
		else if(  tabPosition == tab_left  )  
		{
			[cornerLB compositeToPoint:frame.origin operation:NSCompositeSourceOver];
			[cornerLT compositeToPoint:NSMakePoint(frame.origin.x , NSMaxY(frame) -[cornerLT size].height)
							 operation:NSCompositeSourceOver];
			
		}		else if(  tabPosition == tab_right )  
		{
			[cornerRB compositeToPoint:NSMakePoint(NSMaxX(frame)-[cornerLB size].width, frame.origin.y)
							 operation:NSCompositeSourceOver];
			[cornerRT compositeToPoint:NSMakePoint( NSMaxX(frame) -[cornerRT size].width,NSMaxY(frame)-[cornerRT size].height)
							 operation:NSCompositeSourceOver];
			
		}
		
		
		
		
		
	}
	
}


void _drawSides(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, TAB_OPEN_STATUS tabOpenStatus)
{
	id themeManager = [ThemeManager sharedManager];
	
	
	NSImage* topSide= [themeManager topSide];
	NSImage* bottomSide= [themeManager bottomSide];
	NSImage* leftSide= [themeManager leftSide];
	NSImage* rightSide= [themeManager rightSide];
	
	
	
	///// drawing draggable corner

	
	if(  tabPosition != tab_top  ) 
	{
		[bottomSide drawInRect:NSMakeRect( frame.origin.x,  frame.origin.y ,frame.size.width,5) 
					  fromRect:NSMakeRect(0,0,[bottomSide size].width,[bottomSide size].height)
					 operation:NSCompositeSourceOver fraction:1.0];
	}
	
	if(  tabPosition != tab_bottom  ) 
	{
		[topSide drawInRect:NSMakeRect(frame.origin.x ,NSMaxY(frame)-5,frame.size.width,5) 
				   fromRect:NSMakeRect(0,0,[topSide size].width,[topSide size].height)
				  operation:NSCompositeSourceOver fraction:1.0];
	}
	
	if(  tabPosition != tab_right  ) 
	{
		[leftSide drawInRect:NSMakeRect(frame.origin.x,frame.origin.y,5,frame.size.height) 
					fromRect:NSMakeRect(0,0,[leftSide size].width,[leftSide size].height)
				   operation:NSCompositeSourceOver fraction:1.0];
	}
	
	if(  tabPosition != tab_left  )  
	{
		[rightSide drawInRect:NSMakeRect( NSMaxX(frame)-5, frame.origin.y, 5,frame.size.height) 
					 fromRect:NSMakeRect(0,0,[rightSide size].width,[rightSide size].height)
					operation:NSCompositeSourceOver fraction:1.0];
	}
	
}




void drawButton(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, 
				BOOL mouseOnTab, NSRect customizeButtonRect, 	NSRect closeButtonRect,
				BOOL disableMenuButton, BOOL disableCloseButton)

{
	id themeManager = [ThemeManager sharedManager];
	
	NSImage* tabLMask = [themeManager tabLMask];
	NSImage* tabRMask = [themeManager tabRMask];
	NSImage* tabMMask = [themeManager tabMMask];

	NSSize tabLSize = [tabLMask size];
	NSSize tabRSize = [tabRMask size];
	NSSize tabMSize = [tabMMask size];
	
	
	NSImage*	tabLOverlayForTopTab = [themeManager tabLOverlayForTopTab];
	NSImage* 	tabMOverlayForTopTab = [themeManager tabMOverlayForTopTab];
	NSImage* 	tabROverlayForTopTab = [themeManager tabROverlayForTopTab];
	NSImage* 	tabLOverlayForBottomTab = [themeManager tabLOverlayForBottomTab];
	NSImage* 	tabMOverlayForBottomTab = [themeManager tabMOverlayForBottomTab];
	NSImage* 	tabROverlayForBottomTab = [themeManager tabROverlayForBottomTab];
	NSImage* 	tabLOverlayForLeftTab = [themeManager tabLOverlayForLeftTab];
	NSImage* 	tabMOverlayForLeftTab = [themeManager tabMOverlayForLeftTab];
	NSImage* 	tabROverlayForLeftTab = [themeManager tabROverlayForLeftTab];
	NSImage* 	tabLOverlayForRightTab = [themeManager tabLOverlayForRightTab];
	NSImage* 	tabMOverlayForRightTab = [themeManager tabMOverlayForRightTab];
	NSImage* 	tabROverlayForRightTab = [themeManager tabROverlayForRightTab];
	
	
	NSImage* customizeButton = [themeManager customizeButtonImage];
	NSImage* closeButton = [themeManager closeButtonImage];

 	

	
	NSAttributedString* _title;
	
	if( mouseOnTab == YES )
	{
		_title = [[ [NSAttributedString alloc]
				initWithString:[window title] 
					attributes:[themeManager highlightAttributes]] autorelease];
	}else
	{
	
		_title = [[ [NSAttributedString alloc]
			initWithString:[window title] 
				attributes:[themeManager titleAttributes]] autorelease];
	}		
		
	
	//fill
	[color set];
	[NSBezierPath fillRect:frame];


		
	//	NSRect windowFrame = [MY_WINDOW frame];

	if( tabPosition == tab_bottom || tabPosition == tab_top )
		_title = MNTruncate(_title , [window frame].size.width  + [themeManager titleRelativeWidth]);
	
	
	else if( tabPosition == tab_right || tabPosition == tab_left )
		_title = MNTruncate( _title , [window frame].size.height  + [themeManager titleRelativeWidth]);
	
	
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	
	
	
	// move axis
	[affine translateXBy: frame.origin.x 
					 yBy: frame.origin.y ]; 
	
	
	if( tabPosition == tab_bottom )
	{
		[affine concat];

		
		// overlay
		
		[tabLOverlayForBottomTab drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		[tabMOverlayForBottomTab drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.width - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		
		
		[tabROverlayForBottomTab drawInRect:NSMakeRect( frame.size.width - tabRSize.width, 0, tabRSize.width, tabRSize.height)
					fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		
		
		// mask
		
		[tabLMask drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeXOR fraction:1.0];
		
		[tabMMask drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.width - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeXOR fraction:1.0];
		
		
		
		[tabRMask drawInRect:NSMakeRect( frame.size.width - tabRSize.width, 0, tabRSize.width, tabRSize.height)
					 fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeXOR fraction:1.0];
		
		
		//title
		[_title drawAtPoint:[themeManager titleOffsetForBottomTab] ];

		
		
	}else if ( tabPosition == tab_top )
	{
		[affine concat];

		//overlay
		[tabLOverlayForTopTab setFlipped:YES];
		[tabLOverlayForTopTab drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		[tabLOverlayForTopTab setFlipped:NO];
		
		[tabMOverlayForTopTab setFlipped:YES];
		[tabMOverlayForTopTab drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.width - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeSourceOver fraction:1.0];
		[tabMOverlayForTopTab setFlipped:NO];
		
		
		
		[tabROverlayForTopTab setFlipped:YES];
		[tabROverlayForTopTab drawInRect:NSMakeRect( frame.size.width - tabRSize.width, 0, tabRSize.width, tabRSize.height)
					fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		
		[tabROverlayForTopTab setFlipped:NO];
		
		
		//mask
		
		[tabLMask setFlipped:YES];
		[tabLMask drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeXOR fraction:1.0];
		
		[tabLMask setFlipped:NO];
		
		[tabMMask setFlipped:YES];
		[tabMMask drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.width - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeXOR fraction:1.0];
		[tabMMask setFlipped:NO];

		
		
		[tabRMask setFlipped:YES];
		[tabRMask drawInRect:NSMakeRect( frame.size.width - tabRSize.width, 0, tabRSize.width, tabRSize.height)
					 fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeXOR fraction:1.0];
		
		
		[tabRMask setFlipped:NO];
		
		
		//title
		[_title drawAtPoint:[themeManager titleOffsetForTopTab]  ];

		
	}
	else if( tabPosition == tab_right  )
	{
		
		
	
		
		// move axis
		[affine translateXBy: 0
						 yBy: frame.size.height ]; 
		
		// rotate axis
		[affine rotateByDegrees:  -90];
		
		
		[affine concat];	
		
		// draw
		
		
		
		//overlya
		[tabLOverlayForRightTab setFlipped:YES];
		
		[tabLOverlayForRightTab drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		[tabLOverlayForRightTab setFlipped:NO];
		
		
		[tabMOverlayForRightTab setFlipped:YES];
		[tabMOverlayForRightTab drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.height - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeSourceOver fraction:1.0];
		[tabMOverlayForRightTab setFlipped:NO];
		
		
		
		[tabROverlayForRightTab setFlipped:YES];
		
		[tabROverlayForRightTab drawInRect:NSMakeRect( frame.size.height - tabRSize.width, 0, tabRSize.width, tabRSize.height)
					fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeSourceOver fraction:1.0];
		[tabROverlayForRightTab setFlipped:NO];
		
		
		
		//mask
		[tabLMask setFlipped:YES];
		
		[tabLMask drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeXOR fraction:1.0];
		
		[tabLMask setFlipped:NO];
		
		
		[tabMMask setFlipped:YES];
		[tabMMask drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.height - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeXOR fraction:1.0];
		[tabMMask setFlipped:NO];

		
		
		[tabRMask setFlipped:YES];
		
		[tabRMask drawInRect:NSMakeRect( frame.size.height - tabRSize.width, 0, tabRSize.width, tabRSize.height)
					 fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeXOR fraction:1.0];
		[tabRMask setFlipped:NO];
		
		
		
		//title
		[_title drawAtPoint:[themeManager titleOffsetForRightTab]  ];

		
		
	}
	else if( tabPosition == tab_left  )
	{
		

		
		
		// move axis
		[affine translateXBy:  frame.size.width
						 yBy: 0 ]; 
		
		// rotate axis
		[affine rotateByDegrees:  90];
		
		
		[affine concat];	
		
		// draw
		
		

		
		
		//overlay
		//[tabLOverlayForLeftTab compositeToPoint:NSMakePoint(0,0) operation:NSCompositeXOR];
		
		[tabLOverlayForLeftTab setFlipped:YES];
		[tabLOverlayForLeftTab drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeSourceOver fraction:1.0];
		[tabLOverlayForLeftTab setFlipped:NO];
		
		
		[tabMOverlayForLeftTab setFlipped:YES];
		[tabMOverlayForLeftTab drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.height - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeSourceOver fraction:1.0];
		[tabMOverlayForLeftTab setFlipped:NO];
		
		
		[tabROverlayForLeftTab setFlipped:YES];
		[tabROverlayForLeftTab drawInRect:NSMakeRect( frame.size.height -  tabRSize.width, 0, tabRSize.width, tabRSize.height)
					fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeSourceOver fraction:1.0];
		
		
		[tabROverlayForLeftTab setFlipped:NO];
		
		
		//mask
		//[tabLMask compositeToPoint:NSMakePoint(0,0) operation:NSCompositeXOR];
		
		[tabLMask setFlipped:YES];
		[tabLMask drawInRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height)
					fromRect:NSMakeRect(0, 0, tabLSize.width, tabLSize.height) operation:NSCompositeXOR fraction:1.0];
		[tabLMask setFlipped:NO];
		
		
		[tabMMask setFlipped:YES];
		[tabMMask drawInRect:NSMakeRect(tabLSize.width, 0, 
										frame.size.height - tabLSize.width - tabRSize.width, tabMSize.height)
					fromRect:NSMakeRect(0, 0, tabMSize.width, tabMSize.height) operation:NSCompositeXOR fraction:1.0];
		[tabMMask setFlipped:NO];
		
		
		[tabRMask setFlipped:YES];
		[tabRMask drawInRect:NSMakeRect( frame.size.height -  tabRSize.width, 0, tabRSize.width, tabRSize.height)
					 fromRect:NSMakeRect(0, 0, tabRSize.width, tabRSize.height) operation:NSCompositeXOR fraction:1.0];
		
		
		[tabRMask setFlipped:NO];
		
		
		//title
		[_title drawAtPoint:[themeManager titleOffsetForLeftTab]  ];

		
		
		
		
		
	}
	
	[context restoreGraphicsState];

	
	
	
	if( mouseOnTab == NO )
	{
		//[[NSColor grayColor] set];
		//[NSBezierPath fillRect:customizeButtonRect];
		
	}else
	{
		//[[NSColor grayColor] set];
		//[NSBezierPath fillRect:customizeButtonRect];
		
		if( ! disableMenuButton )
			[customizeButton compositeToPoint:customizeButtonRect.origin
									operation:NSCompositeSourceOver];
		
		if( ! disableCloseButton )
			[closeButton compositeToPoint:closeButtonRect.origin
								operation:NSCompositeSourceOver];
	}
	

}

#pragma mark -
#pragma mark Draw Text View

void drawRoundedRectangle(NSColor* frameColor, NSColor* fillColor, NSTextView* textView, NSRange range )
{
	
	NSLayoutManager* layoutManager = [textView layoutManager];
	
	//highlight longestRange
	
	NSRect highlightFrame;
	NSRange longestGlyphRange = [layoutManager glyphRangeForCharacterRange:range
											 actualCharacterRange:nil];
	
	highlightFrame = [layoutManager boundingRectForGlyphRange:longestGlyphRange
									 inTextContainer:[textView textContainer]];
	
	
	NSArray* rangeArray =  boundingRectArrayForGlyphRange( longestGlyphRange , textView);
	
	int piyo;
	for( piyo = 0; piyo < [rangeArray count]; piyo ++ )
	{
		NSRange aRange = NSRangeFromString( [rangeArray objectAtIndex:piyo] );
		NSRect aRect = [layoutManager boundingRectForGlyphRange:aRange inTextContainer:[textView textContainer] ];
		
		
		int openEnd = 0;
		
		if( piyo != 0 ) openEnd = 1;
		if( piyo != [rangeArray count]-1 ) openEnd = openEnd + 2;
		
		
		aRect.size.height -= 1;
		aRect.size.width -= 1;
		
		
		//NSLog(@"drawing rect %@ openEnd %d",NSStringFromRect(aRect),openEnd);
		drawRoundedBox( frameColor, aRect, 3.0, fillColor, openEnd, 3);
			
	}	
}



NSArray* boundingRectArrayForGlyphRange(NSRange glyphRange, NSTextView* textView)
{
	
	NSRect frame;

//	NSTextContainer* tc = [textView textContainer];
	NSLayoutManager* lm = [textView layoutManager];
	frame = [lm lineFragmentRectForGlyphAtIndex:NSMaxRange(glyphRange)-1
								   effectiveRange:nil];
	
	
	
	
	NSRect firstGlyphFrame = [lm lineFragmentRectForGlyphAtIndex:glyphRange.location
													effectiveRange:nil];
	
	
	// one line
	if( NSEqualRects(frame,firstGlyphFrame) ) 
	{
		//	//NSLog(@"equal %@ %@", NSStringFromRect(frame) , NSStringFromRect(firstGlyphFrame) );
		return [NSArray arrayWithObject:NSStringFromRange(glyphRange)];
	}
	
	////NSLog(@"not equal");
	
	//
	NSMutableArray* array = [NSMutableArray array];
	
	
	int hoge;
	for( hoge = glyphRange.location; hoge < NSMaxRange(glyphRange)+1; hoge ++ )
	{
		NSRange rangeInTheLine;
		NSRect oneRect = [lm lineFragmentRectForGlyphAtIndex:hoge effectiveRange:&rangeInTheLine];
		
		
		NSRange intersectionRange =  NSIntersectionRange(rangeInTheLine,glyphRange) ;
		
		if( intersectionRange.length != 0 )
			[array addObject: NSStringFromRange(intersectionRange) ];
		
		hoge = NSMaxRange(rangeInTheLine);
		
	}
	return (NSArray*)array;
}



void drawRoundedBox(NSColor* aColor , NSRect frame , float boxCurve, NSColor* fillColor , int openEnd, float strokeWidth)
	//openEnd = 0  no
	//openEnd = 1 left open
	//openEnd = 2 right open
	//openEnd = 3 both open

{
	aColor =		convertColorSpace(aColor);
	fillColor = convertColorSpace(fillColor);
	
	//	float boxSize = 30;
	//	float boxCurve = 5;
	
	//
	
	float red, green, blue;
	float redF, greenF, blueF;
	
	float hue, sat, brt, alpha;
	[aColor getHue:&hue saturation:&sat brightness:&brt alpha:&alpha];
	[aColor getRed:&red green:&green blue:&blue alpha:&alpha];
	
	
	
	[fillColor getRed:&redF green:&greenF blue:&blueF alpha:&alpha];
	
	
	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetRGBFillColor(context, redF, greenF, blueF, alpha);
	
	
	CGContextSetLineWidth ( context, 0.1 );
	
	//Fill 
	CGContextBeginPath(context);
	addRoundedRectWithOpenEndToPath(context, CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height),boxCurve, boxCurve, openEnd);	
	
	CGContextFillPath(context);
	
	
	
	//draw
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetLineWidth ( context, strokeWidth );
	
	//stroke 
	CGContextBeginPath(context);
	addRoundedRectWithOpenEndToPath(context, CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height),boxCurve, boxCurve, openEnd);	
	
	CGContextStrokePath(context);
	
}




void addRoundedRectToPath(CGContextRef context, CGRect rect, 
						  float ovalWidth,float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
		// 1
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
	// 2
    CGContextTranslateCTM (context, CGRectGetMinX(rect),
						   // 3
						   CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
	// 4
    fw = CGRectGetWidth (rect) / ovalWidth;
	// 5
    fh = CGRectGetHeight (rect) / ovalHeight;
	// 6
    CGContextMoveToPoint(context, fw, fh/2); 
	// 7
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
	// 8
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
	// 9
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
	// 10
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); 
	// 11
    CGContextClosePath(context);
	// 12
    CGContextRestoreGState(context);
	// 13
}



void addRoundedRectWithOpenEndToPath(CGContextRef context, CGRect rect, 
									 float ovalWidth,float ovalHeight, int openEnd)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
		// 1
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
	// 2
    CGContextTranslateCTM (context, CGRectGetMinX(rect),
						   // 3
						   CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
	
	
	// 4
    fw = CGRectGetWidth (rect) / ovalWidth;
	// 5
    fh = CGRectGetHeight (rect) / ovalHeight;
	
	
	
	
	////NSLog(@"%d",openEnd);
	
	if( openEnd == 0 )
	{
		// 6
		CGContextMoveToPoint(context, fw, fh/2); 
		// 7
		CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
		// 8
		CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
		// 9
		CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
		// 10
		CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); 
		// 11
		CGContextClosePath(context);
		
	}
	
	else if( openEnd == 1 )
	{
		
		// 6
		CGContextMoveToPoint(context, 0, 0); 
		CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); 
		
		// 7
		CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
		CGContextAddLineToPoint(context, 0, fh); 
		
		
	}else if( openEnd == 2 )
	{
		
		CGContextMoveToPoint(context, fw, fh); 
		// 7
		// 8
		CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
		// 9
		CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
		// 10
		CGContextAddLineToPoint(context, fw, 0);
		
		
	}else if( openEnd == 3 )
	{
		CGContextMoveToPoint(context, fw, fh); 
		CGContextAddLineToPoint(context, 0, fh);
		
		CGContextMoveToPoint(context, fw, 0); 
		CGContextAddLineToPoint(context, 0, 0);
		
	}
	
	
	// 12
    CGContextRestoreGState(context);
	// 13
	
}


#pragma mark -
// Gradient

static void myCalculateShadingValues (void *info, 
									  const float *in, 
									  float *out)
{
	float* cc = info;

	
	
    float v;
    size_t k;
	size_t components = 4;
     float c0[] = {cc[0], cc[1], cc[2], 0 };
	 float c1[] = {cc[3], cc[4], cc[5], 0 };

    //components = (size_t)info;
	
    v = *in;
    for (k = 0; k < components -1; k++)
        *out++ = c0[k]*(1.0-v)+ c1[k] * v;   
	*out++ = 1;
}


static CGFunctionRef myGetFunction (CGColorSpaceRef colorspace, void* cc)// 1
{
   // size_t components;
    static const float input_value_range [2] = { 0, 1 };
    static const float output_value_ranges [8] = { 0, 1, 0, 1, 0, 1, 0, 1 };
    static const CGFunctionCallbacks callbacks = { 0,// 2
		&myCalculateShadingValues, 
		NULL };
	
  //  components = 1 + CGColorSpaceGetNumberOfComponents (colorspace);// 3
		return CGFunctionCreate ((void *) cc, // 4
								 1, // 5
								 input_value_range, // 6
								 4, // 7
								 output_value_ranges, // 8
								 &callbacks);// 9
}


void myPaintAxialShading (CGContextRef myContext,// 1
						  CGRect bounds, NSColor* themeColor, NSColor *tintColor)
{
    CGPoint     startPoint, 
	endPoint;
    CGAffineTransform myTransform;
	CGColorSpaceRef colorspace;
	CGFunctionRef	myShadingFunction;
	CGShadingRef  shading;
	
	float width = bounds.size.width;
    float height = bounds.size.height;
	
	
    startPoint = CGPointMake(0.5,0); // 2
    endPoint = CGPointMake(0.5,1);// 3
		
	colorspace = CGColorSpaceCreateDeviceRGB();// 4
		
	
		float r1,g1,b1,r2,g2,b2;
		[[themeColor colorUsingColorSpaceName:NSDeviceRGBColorSpace] getRed:&r1 green:&g1 blue:&b1 alpha:nil];
		[[tintColor colorUsingColorSpaceName:NSDeviceRGBColorSpace] getRed:&r2 green:&g2 blue:&b2 alpha:nil];

		
		float tintedColorR, tintedColorG, tintedColorB;
		tintedColorR = fminf(1.0 ,r1+r2);
		tintedColorG = fminf(1.0 ,g1+g2);
		tintedColorB = fminf(1.0 ,b1+b2);

	float cc[] = {tintedColorR, tintedColorG, tintedColorB,r1,g1,b1};
		
	myShadingFunction = myGetFunction(colorspace, cc);// 5
				
	shading = CGShadingCreateAxial (colorspace, // 6
	startPoint, endPoint,
	myShadingFunction,
	false, false);

	myTransform = CGAffineTransformMakeScale (width, height);// 7
	CGContextConcatCTM (myContext, myTransform);// 8
	CGContextSaveGState (myContext);// 9

	CGContextClipToRect (myContext, CGRectMake(0, 0, 1, 1));// 10
	CGContextSetRGBFillColor (myContext, 1, 1, 1, 1);
	CGContextFillRect (myContext, CGRectMake(0, 0, 1, 1));

	//CGContextBeginPath (myContext);// 11
	//CGContextAddArc (myContext, .5, .5, .3, 0, 
	// pi/2, 0);
	//CGContextClosePath (myContext);
	//CGContextClip (myContext);

	CGContextDrawShading (myContext, shading);// 12
	CGColorSpaceRelease (colorspace);// 13
	CGShadingRelease (shading);
	CGFunctionRelease (myShadingFunction);

	CGContextRestoreGState (myContext); // 14
}


void drawDragArea(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, TAB_OPEN_STATUS tabOpenStatus)
{	//
	////NSLog(@"drawDragArea ss ");
	
	
	ThemeManager* themeManager = [ThemeManager sharedManager];
	NSImage* tearOffImage = [themeManager tearOffImage];
	
	[color set];
	

	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSaveGState(context);

	CGRect bounds = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,frame.size.height);
	
	myPaintAxialShading (context,
						 bounds, color , [themeManager tintColor]);
	
	CGContextRestoreGState(context);
	
	//[NSBezierPath fillRect:frame];

				
	if( tabOpenStatus == 3 )
	{
		
		if( tabPosition == tab_right )
		{
			NSRect edgeRect = NSMakeRect(frame.origin.x,frame.origin.y, 5, frame.size.height);
			
			
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(frame.origin.x,frame.origin.y+hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);
			
		}
		
		else if( tabPosition == tab_left )
		{
			NSRect edgeRect = NSMakeRect(NSMaxX(frame) -5,frame.origin.y, 5, frame.size.height);
			
			//NSRect restRect = NSMakeRect(frame.origin.x,frame.origin.y, frame.size.width -5, frame.size.height);	
			
			
			
			
			
			NSGraphicsContext *context = [NSGraphicsContext currentContext];
			[context saveGraphicsState];
			
			
			NSAffineTransform* affine = [NSAffineTransform transform];
			
			// move axis
			[affine translateXBy:  NSMaxX(frame)
							 yBy: NSMaxY(frame)  ]; 
			
			// rotate axis
			[affine rotateByDegrees:  180];
			
			
			
			[affine concat];	
			
			// draw
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(0,hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			[context restoreGraphicsState];
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);
			
			
		}
		
		else if( tabPosition == tab_bottom )
		{
			NSRect edgeRect = NSMakeRect(frame.origin.x,frame.origin.y, 5, frame.size.width);
			
			//NSRect restRect = NSMakeRect(frame.origin.x,frame.origin.y, frame.size.width , frame.size.height-5);	
			
			
			
			/////
			
			
			NSGraphicsContext *context = [NSGraphicsContext currentContext];
			[context saveGraphicsState];
			
			
			NSAffineTransform* affine = [NSAffineTransform transform];
			
			
			
			// move axis
			[affine translateXBy: frame.origin.x
							 yBy: NSMaxY(frame) ]; 
			
			// rotate axis
			[affine rotateByDegrees:  -90];
			
			
			[affine concat];	
			
			// draw
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(0,hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			[context restoreGraphicsState];
			
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);
			
		}
		
		else if( tabPosition == tab_top)
		{
			NSRect edgeRect = NSMakeRect(frame.origin.x,frame.origin.y, 5, frame.size.width);
			
			//NSRect restRect = NSMakeRect(frame.origin.x,frame.origin.y+5, frame.size.width , frame.size.height );	
			
			
			
			/////
			
			
			NSGraphicsContext *context = [NSGraphicsContext currentContext];
			[context saveGraphicsState];
			
			
			NSAffineTransform* affine = [NSAffineTransform transform];
			
			
			
			// move axis
			[affine translateXBy: NSMaxX(frame)
							 yBy: frame.origin.y]; 
			
			// rotate axis
			[affine rotateByDegrees:  90];
			
			
			[affine concat];	
			
			// draw
			
			int hoge;
			NSRect _rect = NSZeroRect;
			_rect.size = [tearOffImage size];
			for( hoge = 0; hoge < edgeRect.size.height; hoge += _rect.size.height )
			{
				
				[tearOffImage drawAtPoint:NSMakePoint(0,0+hoge)
								 fromRect:_rect
								operation:NSCompositeXOR 
								 fraction:1.0];
				
			}
			
			[context restoreGraphicsState];
			_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);
			
		}
		
		
	}else
	{
		
		_drawDragCorner(frame, window,  tabPosition,  color,  tabOpenStatus);
		
		
	}
	
	////NSLog(@"drawDragArea e ");
	
}



