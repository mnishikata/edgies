//
//  ScreenUtil.h
//  sticktotheedge
//
//  Created by __Name__ on 06/07/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "EdgiesLibrary.h"

#define MENUBAR_SCREEN [[NSScreen screens] objectAtIndex:0]


BOOL mouseIsInScreen(NSScreen* screen);

NSScreen* screenWhereMouseIs(void);

BOOL pointIsInScreen(NSPoint point, NSScreen* screen);

NSScreen* screenWherePointIs(NSPoint point);

NSScreen* screenWhereRectIs(NSRect rect);

int screenIDWhereRectIs(NSRect rect);

int screenIDWhereMouseIs(void);

NSScreen* screenWithScreenID(int screenID);

SCREEN_REGION regionWherePointIs(NSPoint point);

SCREEN_REGION regionWhereMouseIs(void);

int regionIDAtMousePoint(void);

int regionIDAtPoint(NSPoint point);

BOOL mouseIsOnTheEdges(void);

BOOL mouseIsOnTheRightEdge(void);

BOOL mouseIsOnTheLeftEdge(void);

BOOL mouseIsOnTheTopEdge(void);

BOOL mouseIsOnTheBottomEdge(void);

NSRect convertRectNotToStickOutFromScreenWithEdgeMargin(NSRect rect,  NSScreen* screen, int edgeMargin);

NSRect convertRectToStickOnEdgeWithEdgeMargin(NSRect rect,  NSScreen* screen, SCREEN_REGION side, int edgeMargin);

NSRect convertRectWithPointToMouse(NSPoint pointInFrame, NSRect frameInScreen);




int edgeAbove(int edgeID);
int edgeBelow(int edgeID);
int edgeLeftTo(int edgeID);
int edgeRightTo(int edgeID);



