#import <Cocoa/Cocoa.h>

BOOL registered;
BOOL expired;

void setDefaultPreferencesValues(void);
NSDictionary* defaultPreferencesDictionary();
void loadAdditionalUDWithFilename(NSString* name );

/*
void installEngine();
BOOL checkSN();
BOOL validSerialNumber(NSString* aSn , NSString* aName);
NSString* encodeStr(NSString* strToEncode);
NSString* decodeStr(NSString* strToDecode);
NSString* simpleRokku();
NSAttributedString* convertEdgies1point1(NSAttributedString* attr);


void nslogInvalidSerialNumber();
*/

NSDate* installDate();
BOOL firstRun();
//void showDemoDialogue();
void visitWebsite();