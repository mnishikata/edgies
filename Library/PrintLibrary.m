//
//  PrintUtil.m
//  SampleApp
//
//  Created by __Name__ on 06/09/23.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "PrintLibrary.h"
#import "EdgiesLibrary.h"






void updateMinimumMargin(void)
{
	setTopMargin_min(0);
	setBottomMargin_min(0);
	setRightMargin_min(0);
	setLeftMargin_min(0);	
}


float topMargin(void)
{
	return   preferenceFloatValueForKey( @"topMargin" ) ;
}

float bottomMargin(void)
{
	return preferenceFloatValueForKey( @"bottomMargin" ) ;
}

float leftMargin(void)
{
	
	return preferenceFloatValueForKey( @"leftMargin" ) ;

}

float rightMargin(void)
{
	return preferenceFloatValueForKey( @"rightMargin" ) ;

}

float documentWidth(void)
{
	NSPrintInfo *pInfo = [NSPrintInfo sharedPrintInfo];
	return  [pInfo paperSize].width -  leftMargin() -  rightMargin();
}
float documentHeight(void)
{
	NSPrintInfo *pInfo = [NSPrintInfo sharedPrintInfo];
	return  [pInfo paperSize].height -  topMargin() - bottomMargin();
}



BOOL printPageNumber(void)
{
	return preferenceBoolValueForKey( @"printPageNumber" );
}

float pageNumberMargin(void)
{
	
	return preferenceFloatValueForKey( @"pageNumberMargin" ) ;

}

NSAttributedString* pageNumberFormat(void)
{
	id attr = preferenceValueForKey( @"pageNumberFormat" );
		
		
	NSAttributedString* pageNumberAttr;
	if( ! [attr isKindOfClass:[NSData class]] || attr == nil )
	{
		pageNumberAttr = [[[NSAttributedString alloc] initWithString:@""] autorelease];

	}
	else
	{
		pageNumberAttr = [NSUnarchiver  unarchiveObjectWithData: attr];

	}
	
	
	return  pageNumberAttr;
}



//



float topMargin_min(void)
{
	NSPrintInfo* pi  = [NSPrintInfo sharedPrintInfo];
	NSRect rect = [pi imageablePageBounds];
	NSSize paperSize = [pi paperSize];
	
	return paperSize.height - (rect.origin.y + rect.size.height);
}
void setTopMargin_min(float dummy)
{

	setPreferenceValueForKey( [NSNumber numberWithFloat:topMargin_min()] ,@"topMargin_min");
	
	syncronizePreferences();
}



float bottomMargin_min(void)
{
	NSPrintInfo* pi  = [NSPrintInfo sharedPrintInfo];
	NSRect rect = [pi imageablePageBounds];
	//NSSize paperSize = [pi paperSize];
	
	
	return rect.origin.y;
}
void setBottomMargin_min(float dummy)
{
	
	setPreferenceValueForKey( [NSNumber numberWithFloat:bottomMargin_min()] ,@"bottomMargin_min");
	syncronizePreferences();

}



float leftMargin_min(void)
{
	NSPrintInfo* pi  = [NSPrintInfo sharedPrintInfo];
	NSRect rect = [pi imageablePageBounds];
//	NSSize paperSize = [pi paperSize];
	
	
	
	return rect.origin.x;
}
void setLeftMargin_min(float dummy)
{
	
	setPreferenceValueForKey( [NSNumber numberWithFloat:leftMargin_min()] ,@"leftMargin_min");
	syncronizePreferences();

}


float rightMargin_min(void)
{
	NSPrintInfo* pi  = [NSPrintInfo sharedPrintInfo];
	NSRect rect = [pi imageablePageBounds];
	NSSize paperSize = [pi paperSize];
	
	
	return paperSize.width - (rect.origin.x + rect.size.width);
}

void setRightMargin_min(float dummy)
{
	setPreferenceValueForKey( [NSNumber numberWithFloat:rightMargin_min()] ,@"rightMargin_min");
	syncronizePreferences();

}


