//
//  SpotlightUtil.m
//  Jikeiretsu
//
//  Created by __Name__ on 07/03/02.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SpotlightUtil.h"



NSString* getUTI(NSString* targetFilePath)
{
	// Get UTI of the given file

	targetFilePath = [targetFilePath stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	NSURL* anUrl = [NSURL URLWithString: targetFilePath];
	FSRef ref;
	CFURLGetFSRef( (CFURLRef)anUrl,&ref);
	CFTypeRef outValue;
	LSCopyItemAttribute (
						 &ref,
						 kLSRolesAll,
						 kLSItemContentType,
						 &outValue
						 );

	if( outValue == nil ) return nil;

	
	if( [ (NSString*)outValue hasPrefix:@"dyn."] ) return nil;
	
	
	
	return [NSString stringWithString: (NSString*)outValue];
}

NSArray* getUTITree(NSString* targetFilePath)
{
	NSArray* returnArray = nil;
	
	MDItemRef item = MDItemCreate(kCFAllocatorDefault, (CFStringRef)targetFilePath);
	if(item != NULL)
	{
		// Can't just get a dictionary of everything. Oh well.
		CFArrayRef attrNames = MDItemCopyAttribute( item, (CFStringRef)@"kMDItemContentTypeTree" );
		if(attrNames != NULL)
		{
			returnArray = [[NSArray alloc ]initWithArray:(NSArray*)attrNames];
			CFRelease(attrNames);
			
		}
		
		CFRelease(item);
	}
	return [returnArray autorelease];
}
