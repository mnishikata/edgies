#import "FolderLocationWell.h"




@implementation FolderLocationWell

- (void) awakeFromNib
{

	[self setTarget : self];
	[self setAction : @selector(_pushed:)];
}

- (void)_pushed:(NSEvent *)theEvent
{
	
	NSOpenPanel* aPanel = (NSOpenPanel*)[NSOpenPanel openPanel]; 
	
	[aPanel setTitle:NSLocalizedString(@"Choose Folder",@"")];
	[aPanel setPrompt:NSLocalizedString(@"Choose",@"")];
	[aPanel setCanChooseFiles:NO];
	[aPanel setCanChooseDirectories:YES];
	[aPanel setCanCreateDirectories:YES];
	
	
	if( [aPanel runModalForDirectory:nil file:nil types:nil] == NSFileHandlingPanelOKButton )
	{ 

		//TITLE BINDING
		
		NSDictionary* observerInfo = [self infoForBinding:@"title"];
		id observer = [observerInfo objectForKey:@"NSObservedObject"];
		if( observer != nil ) [observer setValue:[aPanel filename] forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];


		return ;
	} 
	
	
}


@end
