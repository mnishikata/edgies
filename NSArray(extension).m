//
//  NSArray(extension).m
//  sticktotheedge
//
//  Created by __Name__ on 06/10/31.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "NSArray(extension).h"



@implementation NSArray (extension)  

- (NSComparisonResult)compare:(id)string
{

	
	if( [self count] == 0 ) return NSOrderedAscending;
		
	NSString* desc = [[self description] lowercaseString];
	
	if( [string isKindOfClass:[NSString class]] )
	{

		return [[self description] compare:string]; 
		
		
		

	}

	else if( [string isKindOfClass:[NSArray class]] )
	{
		return [[self objectAtIndex:0] compare:[string objectAtIndex:0]];
	}
	
	 return NSOrderedAscending;
}
-(NSString*)_fastCharacterContents
{
	return [self description];
}
-(unsigned)length
{
		return [[self description] length];
}

@end
