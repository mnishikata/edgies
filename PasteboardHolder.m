//
//  PasteboardHolder.m
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 06/08/26.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "PasteboardHolder.h"


@implementation PasteboardHolder

-(void)setPasteboard:(NSPasteborad*)pb
{
	pasteboard = pb;
	[pasteboard retain];
}

-(NSPasteboard*)draggingPasteboard
{
	return pasteboard;
}

- (void) dealloc {
	
	[pasteboard release];
	
	[super dealloc];
}


@end
