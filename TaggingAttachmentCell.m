//
//  FileEntityTextAttachmentCell.m
//  fileEntityTextView
//
//  Created by __Name__ on 06/08/13.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "TaggingInspector.h"
#import "FileLibrary.h"
#import "NSString (extension).h"
#import <WebKit/WebKit.h>
#import "TagManager.h"
#import "TagAttachmentCell.h"
#import "TagAttachmentTextView.h"

extern BOOL UD_DELETE_CELLS_AFTER_DRAGGING_OUT;
extern BOOL UD_CELLS_DRAG_OUT_AS_ALIAS;

@implementation TaggingAttachmentCell


+(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TaggingAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
	
	BOOL clickedOnCell = NO;
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"TaggingAttachmentCellItemClassMethodMenu"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}else if( [[customMenuItem representedObject] isEqualToString:@"TaggingAttachmentCellItem"]  )
			{
				clickedOnCell= YES;
			}
				
				
	}
		if( clickedOnCell ) return;
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Insert Tagging Button",@"")
													action:nil keyEquivalent:@""];
		//[customMenuItem setKeyEquivalentModifierMask:1048576];
		[customMenuItem setRepresentedObject:@"TaggingAttachmentCellItemClassMethodMenu"];
		
		[customMenuItem setTarget: nil];//[ButtonInspector sharedButtonInspector]];
			[customMenuItem setAction: @selector(insertTagging:)];
			
			[aContextMenu addItem:customMenuItem ];
			[customMenuItem autorelease];	
			
}

+(NSAttributedString*)taggingAttachmentCellAttributedString
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	TaggingAttachmentCell* aCell = [[[TaggingAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	[aCell setAttachment: anAttachment];
	
	NSBundle*   bundle;
	bundle = [NSBundle bundleForClass:[self class]];
	NSImage* cellImage = [[NSImage alloc] initWithContentsOfFile:
		[bundle pathForImageResource:@"tagCell"]];

	
	[aCell setImage: [cellImage autorelease]  ];

	[aCell loadDefaults];
	
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[aCell image  ]];
		
		return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


#pragma mark -

-(NSString*)targetPath
{
	return targetPath;
}

-(void)setTargetPath:(NSString*)path
{
	[targetPath release];
	targetPath = [path retain];
	
}

-(NSArray*)tagTitles
{
	return tagTitles;
}

-(BOOL)skipDialogue
{
	return skipDialogue;
}

-(void)setSkipDialogue:(BOOL)flag
{
	skipDialogue= flag;
}

-(void)setTagTitles:(NSArray*)titles
//string array
{
	[tagTitles release];
	tagTitles = [titles retain];
}
/*
-(NSImage*)image
{
	return [NSImage imageNamed:@"AdvancedPreferences"];

}*/

-(void)useAsDefault
{

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"TaggingAttachmentCell_buttonSize"];
	[ud setInteger:buttonPosition forKey:@"TaggingAttachmentCell_buttonPosition"];
	[ud setBool:showButtonTitle forKey:@"TaggingAttachmentCell_showButtonTitle"];
	[ud setBool:showBezel forKey:@"TaggingAttachmentCell_showBezel"];
	[ud setObject:[NSArchiver archivedDataWithRootObject: [self font]] forKey:@"TaggingAttachmentCell_font"];
	
	[ud synchronize];
}


- (id)initWithAttachment:(NSTextAttachment*)attachment
{
    self = [super initWithAttachment:(NSTextAttachment*)attachment];
    if (self) {
		

		
		[self loadDefaults];
		
		
		[self setTitle: NSLocalizedString(@"Tagging Button",@"")];

		[self setTarget:self];
		[self setAction:@selector(openDialogue)];
	}
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	if( tagTitles )
	[encoder encodeObject:tagTitles forKey:@"TaggingAttachmentCell_tagTitles"];
	
	if( targetPath )
	[encoder encodeObject:targetPath forKey:@"TaggingAttachmentCell_targetPath"];

	[encoder encodeBool:skipDialogue forKey:@"TaggingAttachmentCell_skipDialogue"];


    return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		
		if( [coder containsValueForKey:@"TaggingAttachmentCell_tagTitles"] )
			tagTitles = [[coder decodeObjectForKey:@"TaggingAttachmentCell_tagTitles"] retain];
	

		if( [coder containsValueForKey:@"TaggingAttachmentCell_targetPath"] )
			targetPath = [[coder decodeObjectForKey:@"TaggingAttachmentCell_targetPath"] retain];

		if( [coder containsValueForKey:@"TaggingAttachmentCell_skipDialogue"] )
			skipDialogue = [coder decodeBoolForKey:@"TaggingAttachmentCell_skipDialogue"];
		
		
		
		//[self setImage:[NSImage imageNamed:@"tagCell"]];
		//[self setButtonSize: buttonSize];
		
		[self setTarget:self];
		[self setAction:@selector(openDialogue)];
		[self setButtonSize:buttonSize];

	}
	return self;
	
	
}


#pragma mark -






-(void)openDialogue
{
	if( [textView conformsToProtocol:@protocol(TaggingAttachmentCellOwnerTextView)] )
	{
		//[[TaggingInspector sharedTaggingInspector]  showTaggingInspector];
		[[TaggingInspector sharedTaggingInspector]  modifyTagging:self];
		
		
	}
}


-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TaggingAttachmentCellOwnerTextView> *)textView
{
	
	
	//NSLog(@"customizeContextualMenu");
	
	
	int piyo;
	NSMenuItem* customMenuItem;
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"TaggingAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
	//add separator
	customMenuItem = [NSMenuItem separatorItem];
	[customMenuItem setRepresentedObject:@"TaggingAttachmentCellItem"];
	[aContextMenu addItem:customMenuItem ];
	
	
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Tagging Inspector",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"TaggingAttachmentCellItem"];
	[customMenuItem setTarget: textView];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(openTaggingInspector)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
		

		
		///
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Create file from clipboard contents and add tags",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"TaggingAttachmentCellItem"];
		[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
			[customMenuItem setAction: @selector(dropClipboard)];
			
			[aContextMenu addItem:customMenuItem ];
			[customMenuItem autorelease];
		
}
-(void)dropClipboard
{
	[[TagManager sharedTagManager] dropClipboardTo:[self targetPath] windowForSheet :[textView window]];
	
	
}

-(void)loadDefaults
{
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"TaggingAttachmentCell_buttonSize"];
	buttonSize = (value != nil ? [value floatValue] : 48); 
	
	value = [ud valueForKey: @"TaggingAttachmentCell_buttonPosition"];
	buttonPosition = (value != nil ? [value intValue] : 0); 
	
	value = [ud valueForKey: @"TaggingAttachmentCell_showButtonTitle"];
	showButtonTitle = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"TaggingAttachmentCell_showBezel"];
	showBezel = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"TaggingAttachmentCell_font"];
	if( value != nil ){
		NSFont* font = [NSUnarchiver unarchiveObjectWithData: value];
		if( font != nil && [font isKindOfClass:[NSFont class]] )
			[self setFont:font];
		
	}
	
	id tagListArchive = preferenceValueForKey(@"tagList");
	if( tagListArchive )
	{
		NSTextStorage* ts = [NSKeyedUnarchiver unarchiveObjectWithData:tagListArchive];
		if( ts )
		{
			tagTitles = [TagAttachmentTextView  selectedTagsInAtributedString:ts];
			[tagTitles retain];
			
		}
	}
	
	
	targetPath = preferenceValueForKey(@"TaggingAttachmentCell_targetPath");
	if( targetPath == nil )
		targetPath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Desktop/"];
	[targetPath retain];
	
	
	skipDialogue = NO;
	
	lock = NO;
}




-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	[[TaggingInspector sharedTaggingInspector]  modifyTagging:self];
}

#pragma mark -
#pragma mark @protocol AttachmentCellConverting 

-(int)stringRepresentation:(NSDictionary*)context
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	NSString* string = @" ";
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = 0;
	
	return changeInLength;
}




-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	NSString* string = @" ";
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = 0;
	
	
	return changeInLength;
	

}

-(int)attributedStringRepresentation:(NSDictionary*)context //withimage 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	//add icon
	[self setHighlighted:NO];
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[self buttonImageWithAlpha:1.0  ]];
	
	
	//NSAttributedString* attr = [NSAttributedString attributedStringWithAttachment: [self attachment]];
	
	

	
	
	return 0;
	
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	
		NSDictionary* val = [NSDictionary dictionaryWithObjectsAndKeys:

			@" ", NSStringPboardType,		nil];
	
	return val;
}
		

-(BOOL)canAcceptDrop
{
	return YES;
}


			  
@end
