//
//  DragButton.m
//  sticktotheedge
//
//  Created by __Name__ on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "DragButton.h"
#import <HIToolbox/CarbonEventsCore.h>

#import "ScreenUtil.h"
#import "ModifierKeyWell.h"
#import "ThemeManager.h"
#import "AppController.h"
#import "MNTabWindow.h"
#import "MiniDocument.h"
#import "EdgiesLibrary.h"
#import "ScreenUtil.h"
#import "KeyModifierManager.h"
#import "TabDocumentController.h"

#define PBTypes [NSArray arrayWithObjects: NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType",NSFilenamesPboardType, NSTIFFPboardType, nil]

#define TITLE_ATTRIBUTE [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12],NSFontAttributeName , NULL ]

#define MY_WINDOW (MNTabWindow*)[self window]
#define THEME_COLOR [[MY_WINDOW ownerDocument] documentColor]
#define OWNER_DOCUMENT [MY_WINDOW ownerDocument]


@implementation DragButton

-(void)awakeFromNib
{
	
	
	
	tabPosition = tab_bottom;
	mouseOnTab = NO;
	trackingRect = NSNotFound;
	
	//backgroundColor = [ [NSColor grayColor] retain];
	

	
	[self registerForDraggedTypes:PBTypes ];
	
	/*
	NSBundle* myBundle;
	myBundle = [NSBundle bundleForClass:[self class]];
	NSString* path;
	
	path = [myBundle pathForResource:@"customizeButton" ofType:@"tiff"];
	customizeButtonImage = [[NSImage alloc] initWithContentsOfFile:path ];
	
	*/
	/*
	path = [myBundle pathForResource:@"tab" ofType:@"tiff"];
	[tabImage release];
	tabImage = [[NSImage alloc] initWithContentsOfFile:path ];
	
	path = [myBundle pathForResource:@"tabR" ofType:@"tiff"];
	[tabRImage release];
	tabRImage = [[NSImage alloc] initWithContentsOfFile:path ];
	*/
	
	/*
	path = [myBundle pathForResource:@"closeButton" ofType:@"tiff"];
	closeButtonImage = [[NSImage alloc] initWithContentsOfFile:path ];
	*/
	
	balloonDelay = 0;
	
	//[self setup];
	[self setPreference];
	
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupTrackRect) name:NSWindowDidResizeNotification object:MY_WINDOW];
	
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
	
}
- (BOOL)isFlipped
{
	return NO;
}


-(void)setPreference
{
	openAutomatically =  preferenceBoolValueForKey(@"openAutomatically")   ;
	
	disableCloseButton =   preferenceBoolValueForKey(@"disableCloseButton")   ;
	disableMenuButton =   preferenceBoolValueForKey(@"disableMenuButton")   ;
}

-(void)setup /// must be called when ready
{

	
	
	[self setupTrackRect:NO];
	
	
	[self setAction:@selector(mouseDown:)];
	
	//[self setKeyEquivalent:@"o"];
	
	NSString* title = [MY_WINDOW title] ;
	
	
	
	if( title != NULL )
		[self adjustButtonSizeWhenWindowWasResized:nil];
	
	
	[[NSNotificationCenter defaultCenter] removeObserver:self ];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(adjustButtonSizeWhenWindowWasResized:)
												 name:NSWindowDidResizeNotification
											   object:MY_WINDOW];

	
	
	/*
	 {
		 NSSize titleSize = [title sizeWithAttributes:TITLE_ATTRIBUTE];
		 NSRect myFrame = [self frame];
		 
		 
		 if( tabPosition == tab_bottom || tabPosition == tab_top )
		 {
			 myFrame.origin.x += myFrame.size.width /2;
			 
			 
			 myFrame.size.width = titleSize.width + 40;
			 
			 if( myFrame.size.width > [MY_WINDOW frame].size.width -50 )
				 myFrame.size.width = [MY_WINDOW frame].size.width -50;
			 
			 
			 myFrame.origin.x -= myFrame.size.width /2;
			 
			 
		 }else	if( tabPosition == tab_right || tabPosition == tab_left )
		 {
			 myFrame.origin.y += myFrame.size.height /2;
			 
			 myFrame.size.height = titleSize.width + 40;
			 
			 if( myFrame.size.height > [MY_WINDOW frame].size.height -50  )
				 myFrame.size.height = [MY_WINDOW frame].size.height -50 ;
			 
			 
			 myFrame.origin.y -= myFrame.size.height /2;
			 
			 
		 }
		 
		 
		 
		 
		 
		 
		 
		 [self setFrame:myFrame];
	 }
	 
	 
	 if( tabPosition == tab_bottom || tabPosition == tab_top )
	 {
		 customizeButtonRect = NSMakeRect( [self frame].size.width-20,4,16,16);
		 closeButtonRect = NSMakeRect( 10,4,16,16);
		 
	 }
	 
	 else if( tabPosition == tab_left )
	 {
		 closeButtonRect = NSMakeRect(6,10,16,16);
		 customizeButtonRect  = NSMakeRect( 8,[self frame].size.height-18,16,16);
		 
	 }
	 else if( tabPosition == tab_right )
	 {
		 customizeButtonRect = NSMakeRect(4,8,16,12);
		 closeButtonRect = NSMakeRect( 4,[self frame].size.height-18,16,16);
		 
	 }
	 
	 */
	
}

-(void)setupTrackRect:(BOOL)assumeInside
{
	//////NSLog(@"button setupTrackRect %d",trackingRect ); 
	if( trackingRect != NSNotFound )
		[self removeTrackingRect:trackingRect];
	
	trackingRect = [self addTrackingRect:[self bounds] owner:self userData:NULL assumeInside:assumeInside];
}



-(void)endUsing
{
	/*
	 
	 NSTimer* balloonOnTimer;
	 NSTimer* balloonOffTimer;
	 
	 */
	if( [balloonOnTimer isValid] ){
		[balloonOnTimer invalidate];
		[balloonOnTimer release];
	}
	if( [balloonOffTimer isValid] ){
		[balloonOffTimer invalidate];
		[balloonOffTimer release];
	}
	if( [windowOpenTimer isValid] ){
		[windowOpenTimer invalidate];
		[windowOpenTimer release];
	}
	
	
}
-(void)dealloc
{
	//////NSLog(@"# button dealloc");
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	trackingRect = NSNotFound;
	
	[super dealloc];
}






-(void)adjustButtonSizeWhenWindowWasResized:(NSNotification*)notif
{	
	ThemeManager* themeManager = [ThemeManager sharedManager];

		
	NSAttributedString* _title = [[ [NSAttributedString alloc]
										initWithString:[MY_WINDOW title] 
									attributes:[themeManager titleAttributes]] autorelease];
	
	NSRect windowFrame = [MY_WINDOW frame];
	NSRect myFrame  = [self frame];
	
	int requiredButtonWidth = [_title size].width + 40;
	
	if( tabPosition == tab_bottom   )
	{
		if( windowFrame.size.width -40 <= requiredButtonWidth )
		{
			
			NSSize _size = NSMakeSize( windowFrame.size.width-40, 20 );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( (int)(windowFrame.size.width/2 -_size.width/2), 0)];
			
			
		}else
		{
			
			NSSize _size = NSMakeSize((int)([_title size].width+40), 20);
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( (int)(windowFrame.size.width/2 -_size.width/2), 0 )];
		}
	}
	else if( tabPosition == tab_top )
	{
		if( windowFrame.size.width -40 <= requiredButtonWidth )
		{
			
			NSSize _size = NSMakeSize( windowFrame.size.width-40, 20 );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( (int)(windowFrame.size.width/2 -_size.width/2), windowFrame.size.height-20 )];
			
			
		}else
		{
			
			NSSize _size = NSMakeSize((int)([_title size].width+40), 20 );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( (int)(windowFrame.size.width/2 -_size.width/2), windowFrame.size.height-20 )];
		}
		
	}		
	
	
	
	
	else if( tabPosition == tab_right   )
	{
		if( windowFrame.size.height -40 <= requiredButtonWidth )
		{
			
			NSSize _size = NSMakeSize( 20 , windowFrame.size.height-40 );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint(windowFrame.size.width-20, (int)(windowFrame.size.height/2 -_size.height/2))];
			
		}else
		{
			
			NSSize _size = NSMakeSize( 20 , (int)([_title size].width+40) );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( windowFrame.size.width-20, (int)(windowFrame.size.height/2 -_size.height/2))];
			
		}
	}		
	
	
	else if(   tabPosition == tab_left )
	{
		if( windowFrame.size.height -40 <= requiredButtonWidth )
		{
			
			NSSize _size = NSMakeSize( 20, windowFrame.size.height-40 );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( 0, (int)(windowFrame.size.height/2 -_size.height/2))];
			
		}else
		{
			
			NSSize _size = NSMakeSize( 20 , (int)([_title size].width+40) );
			[self setFrameSize:_size];
			[self setFrameOrigin:NSMakePoint( 0, (int)(windowFrame.size.height/2 -_size.height/2))];
			
		}
	}
	
	
	myFrame = [self frame];
	
	
	
	
	//setup button rect
	
	
	NSRect	customizeButtonRectForTopTab = [themeManager customizeButtonRectForTopTab];
	NSRect	closeButtonRectForTopTab = [themeManager closeButtonRectForTopTab];
	NSRect	customizeButtonRectForBottomTab = [themeManager customizeButtonRectForBottomTab];
	NSRect	closeButtonRectForBottomTab = [themeManager closeButtonRectForBottomTab];
	NSRect	customizeButtonRectForLeftTab = [themeManager customizeButtonRectForLeftTab];
	NSRect	closeButtonRectForLeftTab = [themeManager closeButtonRectForLeftTab];
	NSRect	customizeButtonRectForRightTab = [themeManager customizeButtonRectForRightTab];
	NSRect	closeButtonRectForRightTab = [themeManager closeButtonRectForRightTab];
	
	
	if( customizeButtonRectForTopTab.origin.x < 0 ) 
		customizeButtonRectForTopTab.origin.x += myFrame.size.width;
	if( closeButtonRectForTopTab.origin.x < 0 ) 
		closeButtonRectForTopTab.origin.x += myFrame.size.width;
	if( customizeButtonRectForBottomTab.origin.x < 0 ) 
		customizeButtonRectForBottomTab.origin.x += myFrame.size.width;
	if( closeButtonRectForBottomTab.origin.x < 0 ) 
		closeButtonRectForBottomTab.origin.x += myFrame.size.width;
	if( customizeButtonRectForLeftTab.origin.x < 0 ) 
		customizeButtonRectForLeftTab.origin.x += myFrame.size.width;
	if( closeButtonRectForLeftTab.origin.x < 0 ) 
		closeButtonRectForLeftTab.origin.x += myFrame.size.width;
	if( customizeButtonRectForRightTab.origin.x < 0 ) 
		customizeButtonRectForRightTab.origin.x += myFrame.size.width;
	if( closeButtonRectForRightTab.origin.x < 0 ) 
		closeButtonRectForRightTab.origin.x += myFrame.size.width;
	

	if( customizeButtonRectForTopTab.origin.y < 0 ) 
		customizeButtonRectForTopTab.origin.y += myFrame.size.height;
	if( closeButtonRectForTopTab.origin.x < 0 ) 
		closeButtonRectForTopTab.origin.y += myFrame.size.height;
	if( customizeButtonRectForBottomTab.origin.y < 0 ) 
		customizeButtonRectForBottomTab.origin.y += myFrame.size.height;
	if( closeButtonRectForBottomTab.origin.y < 0 ) 
		closeButtonRectForBottomTab.origin.y += myFrame.size.height;
	if( customizeButtonRectForLeftTab.origin.y < 0 ) 
		customizeButtonRectForLeftTab.origin.y += myFrame.size.height;
	if( closeButtonRectForLeftTab.origin.y < 0 ) 
		closeButtonRectForLeftTab.origin.y += myFrame.size.height;
	if( customizeButtonRectForRightTab.origin.y < 0 ) 
		customizeButtonRectForRightTab.origin.y += myFrame.size.height;
	if( closeButtonRectForRightTab.origin.y < 0 ) 
		closeButtonRectForRightTab.origin.y += myFrame.size.height;

	
	if(  tabPosition == tab_top )
	{
		customizeButtonRect = customizeButtonRectForTopTab;
		closeButtonRect = closeButtonRectForTopTab;
		
	}else 	if( tabPosition == tab_bottom  )
	{
		customizeButtonRect = customizeButtonRectForBottomTab;
		closeButtonRect = closeButtonRectForBottomTab;
		
	}
	
	else if( tabPosition == tab_left )
	{
		customizeButtonRect  = customizeButtonRectForLeftTab;
		closeButtonRect = closeButtonRectForLeftTab;

	}
	else if( tabPosition == tab_right )
	{
		customizeButtonRect  = customizeButtonRectForRightTab;
		closeButtonRect = closeButtonRectForRightTab;
		
	}
	
	
	/*
	if( tabPosition == tab_bottom || tabPosition == tab_top )
	{
		customizeButtonRect = NSMakeRect( myFrame.size.width-20,4,16,16);
		closeButtonRect = NSMakeRect( 10,4,16,16);
		
	}
	
	else if( tabPosition == tab_left )
	{
		closeButtonRect = NSMakeRect(6,10,16,16);
		customizeButtonRect  = NSMakeRect( 8,myFrame.size.height-18,16,16);
		
	}
	else if( tabPosition == tab_right )
	{
		customizeButtonRect = NSMakeRect(4,8,16,12);
		closeButtonRect = NSMakeRect( 4,myFrame.size.height-18,16,16);
		
	}
	 */
	
	
	[self setupTrackRect:NO];
	
	

	
}


-(void)drawRect:(NSRect)frame
{
	drawButton(  frame, [self window],  tabPosition, THEME_COLOR, 
					 mouseOnTab,  customizeButtonRect, 	 closeButtonRect,
				 disableMenuButton,  disableCloseButton);
		
}

-(NSImage*)  modifyBalloonImageForDraggingOut:(NSImage*)balloonImage
{
	
	NSRect rect = NSZeroRect;
	rect.size = [balloonImage size];
	
	NSImage* image = [[[NSImage alloc] initWithSize:rect.size] autorelease];
	
	
	
	[image lockFocus];
	
	[balloonImage compositeToPoint:NSMakePoint(0,0) fromRect:rect
						 operation:NSCompositeSourceOver fraction:0.6];
	[image unlockFocus];
	
	
	return image;
}

- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination
{
	NSString* path = [dropDestination path];
	
	NSString* _name = [OWNER_DOCUMENT exportInFolder:path plainText:dragoutAsPlainText preferredName:nil changeFilename:YES];
	
	if( _name != nil )
		return [NSArray arrayWithObject:_name];
	else
		return [NSArray array];
	
}

#pragma mark Tracking

-(BOOL)canStartTracking:(NSEvent*)theEvent
{
	/// check option + command drag
	if( [MOD_PRESSED:@"modDragContent"]  ) 
	{ 
		[self dragAsRichText:theEvent];
		return NO;
	}
	
	
	/// check option + command drag
	else if(   [MOD_PRESSED:@"modDragContentAsPlainText"]  ) 
	{ 		
		[self dragAsPlainText:theEvent];		
		return NO;
	}	
	
	return YES;
}

-(BOOL)tearOffWindowIfNecessary:(NSPoint)currentMouseLoc fromPoint:(NSPoint)originalPoint
{
	BOOL windowTorn = NO;
	
	if( tabPosition == tab_top )
	{
		if(  currentMouseLoc.y  - originalPoint.y > AUTO_TEAROFF_MARGIN  )
			windowTorn = YES;

	}
	
	else if( tabPosition == tab_bottom )
	{
		if(   originalPoint.y - currentMouseLoc.y > AUTO_TEAROFF_MARGIN  )
			windowTorn = YES;

	}
	
	else if( tabPosition == tab_right )
	{
		if(  currentMouseLoc.x  - originalPoint.x > AUTO_TEAROFF_MARGIN  )
			windowTorn = YES;

	}
	
	else if( tabPosition == tab_left )
	{
		if(   originalPoint.x - currentMouseLoc.x > AUTO_TEAROFF_MARGIN  )
				windowTorn = YES;
	}

	
	if( windowTorn == YES )
	{
		[MY_WINDOW tearOffWithoutMovement];
		[MY_WINDOW repositionToMouse];
	
	}
	
	return windowTorn;
}
-(TAB_POSITION)changePosition:(SCREEN_REGION)newRegion;
{
	TAB_POSITION tempTabPosition = 0;
	
	if( newRegion == leftArea )
	{
		[MY_WINDOW setTabPosition:tab_right];
		tempTabPosition  = tab_right ;
		

	}else if( newRegion == rightArea )
	{
		[MY_WINDOW setTabPosition:tab_left];
		tempTabPosition  = tab_left ;
		
	}else if( newRegion == topArea )
	{
		[MY_WINDOW setTabPosition:tab_bottom];
		tempTabPosition  = tab_bottom ;
		
	}else if( newRegion == bottomArea )
	{
		[MY_WINDOW setTabPosition:tab_top];
		tempTabPosition  = tab_top;
		
	}
				
				
	[MY_WINDOW repositionToMouse];	
	
	return tempTabPosition;
}

-(BOOL)clickedOnTab:(NSEvent*)theEvent
{
	//NSLog(@"clickedOnTab");
	
	
	/// CHECK MODIFEIR
	
	//check deleting
	
	if(  [MOD_PRESSED:@"modDiscard"]  )
	{
		[MY_WINDOW performClose:self];
		return YES;		
		
	}
	
	
	//check ctrl click
	if( [MOD_PRESSED:@"modShowMenu"]  )
	{
		[self mouseExited:NULL];		
		
		[MY_WINDOW showMenu];
		
		return YES;
	}
	
	
	//check export and close
	if( [MOD_PRESSED:@"modExport"]  )
	{
		
		
		[OWNER_DOCUMENT export];
			
		[MY_WINDOW closeTab];
		
		[[[NSApplication sharedApplication] delegate] destroy:OWNER_DOCUMENT];
		
		[MY_WINDOW setDelegate:nil];
		
		[MY_WINDOW close];
		return YES;
	}
	// option customize title
	
	 if(   [MOD_PRESSED:@"modRename"]  )
	{
		[OWNER_DOCUMENT showProperty];
		return YES;		
		
	}
	
	 if(  [MOD_PRESSED:@"modTearOff"])
	{
		[MY_WINDOW openTab];
		[MY_WINDOW tearOffWithMovement];
		return YES;		
		
	}
	
	// check if clicked on the button

	NSPoint _point = [self convertPoint:[[self window] mouseLocationOutsideOfEventStream] fromView:[MY_WINDOW contentView]];
	

	if( NSPointInRect( _point , customizeButtonRect ) && !disableMenuButton )
	{

		[self customizeButtonClicked];
		return YES;
		
	}else	if( NSPointInRect( _point , closeButtonRect ) && !disableCloseButton )
	{

		[MY_WINDOW performClose:self];
		return YES;
	}
	
	
	[MY_WINDOW toggleTab];
	[self mouseExited:NULL];

	


	
	return NO;
}


- (void)mouseDown:(NSEvent *)theEvent
{
	
	
	TabDocumentController* tabDocumentController = [TabDocumentController sharedTabDocumentController];
	MNTabWindow* myWindow = MY_WINDOW;
//	TAB_POSITION tempTabPosition = tabPosition;
	
	
	//NSPoint op = [NSEvent mouseLocation];
	NSPoint originalPoint = [NSEvent mouseLocation];
	
	NSPoint draggedPointInsideWindow = [myWindow mouseLocationOutsideOfEventStream];
	
	
	/*
	if( ! NSPointInRect( [myWindow mouseLocationOutsideOfEventStream], [self frame] ) )
	{
		//////NSLog(@"trackMouse quit");
		[self mouseExited:nil];
		return;
	}
	*/
	
	
	
	[BALLOON_CONTROLLER hideBalloon];
	
	
	
	// set start status
	
	BOOL moved = NO;
	BOOL mouseDownOnClosedTab = ![myWindow isOpened];

	int alignmentOption = preferenceIntValueForKey(@"alignment");


	int originalRegionID = regionIDAtMousePoint();
	NSRect originalTabBounds = [myWindow tabBoundsInScreen];
	int orderInSiblings = [[tabDocumentController  siblingsFor:OWNER_DOCUMENT] indexOfObject:OWNER_DOCUMENT];
	
	
	
	// check key binding click
	if( ! [self canStartTracking:theEvent] ) return;
	
	
	
	//Tracking
	
	////NSLog(@"Tracking");
	
	TIMEOUT_WHILE(30)
	{
		
        theEvent = [[self window] 
            nextEventMatchingMask:NSAnyEventMask
						untilDate:[NSDate dateWithTimeIntervalSinceNow:30.0]
						   inMode:NSEventTrackingRunLoopMode
						  dequeue:YES];
		
		if( theEvent == nil ) break;
		
		
		NSPoint currentMouseLoc = [NSEvent mouseLocation];
		
		
		
		//auto tear off
		TAB_OPEN_STATUS tabOpenStatus = [myWindow tabOpenStatus];

		if( tabOpenStatus != tab_tornOff )
		{
			
			if( [self tearOffWindowIfNecessary:currentMouseLoc fromPoint: originalPoint ] )
			{
				originalPoint = currentMouseLoc;
				draggedPointInsideWindow = [myWindow mouseLocationOutsideOfEventStream];
				originalRegionID = regionIDAtMousePoint() ;
				originalTabBounds = [myWindow tabBoundsInScreen];
				
				moved = YES;
				
				////NSLog(@"tear off");
				goto SkipLoop;
			}
		}
		
		
		
		
		// change position & 		//stick automatically
		int newRegionID = regionIDAtMousePoint();
		if( newRegionID != originalRegionID ) 
		{
			SCREEN_REGION newRegion = regionWhereMouseIs();
			int _screenID = screenIDWhereMouseIs();
			
			[myWindow setScreenID: _screenID];
			
			if( newRegion != centerArea )//center√á‚âà√á¬ª√á¬¢
			{
				tabPosition = [self changePosition:newRegion];

				//stick automatically
				if( tabOpenStatus != tab_tornOff && ! [MOD_PRESSED:@"modTearOff"] )
				{
					[myWindow closeTabBlocking:YES];					
					[myWindow setFloatingLevel];
				}
				
					
				//reset status
				
				originalPoint = currentMouseLoc;
				draggedPointInsideWindow = [myWindow mouseLocationOutsideOfEventStream];
				originalRegionID = newRegionID;
				originalRegionID = regionIDAtMousePoint() ;
				originalTabBounds = [myWindow tabBoundsInScreen];
				orderInSiblings = [[tabDocumentController  siblingsFor:OWNER_DOCUMENT] indexOfObject:OWNER_DOCUMENT];

				moved = YES;
				//NSLog(@"change area");
				goto SkipLoop;

			}
		}
		
		
		//set window position to mouse
		NSRect windowFrame = [myWindow frame] ;
		
		windowFrame = convertRectWithPointToMouse( draggedPointInsideWindow,windowFrame );

		
		//command 
		
		if( tabOpenStatus == tab_tornOff ||  [MOD_PRESSED:@"modTearOff"]  ) // free move
		{

			// do not change rect
			
		}else
		{
			
			windowFrame = [myWindow frameForProposedFrame:windowFrame];
				
		}
		
		
		// set frame
		
		[myWindow setFrameOrigin:windowFrame.origin];// display:YES animate:NO ];
		
		
		
		float dif_x = currentMouseLoc.x - originalPoint.x;
		float dif_y = currentMouseLoc.y - originalPoint.y;
		
		if( dif_x*dif_x > 1  ||  dif_y*dif_y > 1 )//|| [myWindow isTornOff]  ) 
			moved = YES;			
		
		
		////NSLog(@"moved %d",moved);
		


		/// AUTO ALIGN HERE
		
		if( (tabOpenStatus != tab_tornOff) && moved == YES)
		{
			////NSLog(@"AUTO ALIGN %d",alignmentOption);
			
			if( alignmentOption == 1 )
			{
			
				int newOrderInSiblings = 
					[[tabDocumentController  siblingsFor:OWNER_DOCUMENT] indexOfObject:OWNER_DOCUMENT];
				
				if( newOrderInSiblings != orderInSiblings )
				{
					////NSLog(@"rearrange");
					
					[tabDocumentController rearrangeTabsOnEdge:[myWindow edgeThatThisWindowBelongsTo] 
											  fromIndex:orderInSiblings 
												toIndex:newOrderInSiblings 
										 movingDocument:OWNER_DOCUMENT
										   originalRect:originalTabBounds];
				
					//set current state original
					orderInSiblings = newOrderInSiblings;
					
					originalTabBounds = [myWindow tabBoundsInScreen];
									
				}
				
			}else if( alignmentOption == 2 )
			{
					
	
				// pattern 2			
			
				int idx = [[tabDocumentController siblingsFor:OWNER_DOCUMENT] indexOfObject:OWNER_DOCUMENT] ;
				
				////NSLog(@"align %d , %d",[self edgeThatThisWindowBelongsTo] , idx);
				
				
				if( idx != NSNotFound )
				[tabDocumentController alignTabOnEdge:[myWindow edgeThatThisWindowBelongsTo]
								to: idx option:0 ];
			}
		}
		
		
		
		 
		 ////
		 

SkipLoop:
			
	if ( [theEvent type] == NSLeftMouseUp  /*|| ! GetCurrentButtonState()*/ )
		break;
		
		
	}//while end
	
	//NSLog(@"while end");
	

	
	if( moved == NO ) // 
	{
		if( [self clickedOnTab:theEvent] ) return;			
			
		if( mouseDownOnClosedTab ) 
		{
			[myWindow makeFirstResponder: [[myWindow ownerDocument] textView]];
			
			if( preferenceBoolValueForKey(@"orderFrontAutomatically") )
				[NSApp activateIgnoringOtherApps:YES];
		}

			//[MY_WINDOW makeMainWindow];
	}
}



-(BOOL)mouseOnButton
{
	NSRect rect = [self frame];
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];
	
	return NSPointInRect( mouseLoc , rect );
	
}

#pragma mark -

- (void)mouseEntered:(NSEvent *)theEvent
{
	
	if( ! [[self window] isVisible] ) return;
	
	[self setupTrackRect:YES];
	
	////NSLog(@"mouseEntered button");
	[[TabDocumentController sharedTabDocumentController] setBusy:YES];
	
	
	//dragging?
	
	if( GetCurrentButtonState() != 0 )
	{
		////NSLog(@"return");	
		return;
	}
	
	/*
	 // inputting title ?
	 
	 if( [MY_WINDOW isInputtingTitle] )
	 return;
	 */
	
	[MY_WINDOW setFloatingLevel];
	[MY_WINDOW orderFront:self];
	
	
	
	// tear off?
	
	if(  [MY_WINDOW isTornOff] )
	{
		// i hope this can cache the window contents.  but i don't think so

		[[[MY_WINDOW ownerDocument] textView] display];
		
		
	}
	
	
	//	//NSLog(@"mouseEntered");
	balloonDelay =   preferenceFloatValueForKey(@"sliderBalloonDelay")     / 1000.0;
	openDelay =   preferenceFloatValueForKey(@"sliderOpenDelay")    / 1000.0;
	
	//	[[NSApplication sharedApplication] arrangeInFront:self];
	
	
	[[TabDocumentController sharedTabDocumentController] setCurrentDocument:[MY_WINDOW ownerDocument] ];
	
	
	
	
	mouseOnTab = YES;
	if( openAutomatically == YES && ! [MY_WINDOW isOpened])
	{
		
		balloonCursor = NSZeroPoint;
		[self openTabWithDelay];
		
	}
	
	
	
	
	
	
	
	// balloon
	if( ! [MY_WINDOW isOpened] &&  preferenceBoolValueForKey(@"showBalloon")  == YES )
	{
		
		
		balloonCursor = NSZeroPoint;
		
		[self openBalloon];
		
		/*
		 NSImage* balloonImage = [MY_WINDOW balloonImage];
		 
		 [BALLOON_CONTROLLER showBalloonWithImage:balloonImage
											color:backgroundColor
											   at:[NSEvent mouseLocation] ];
		 */
	}
	
	//[MY_WINDOW makeKeyAndOrderFront:self];
	
	[self display]; 
}

-(void)openTabWithDelay
{
	////NSLog(@"openTabWithDelay");
	
	// do nothing if window is not front most
	if( 	[[TabDocumentController sharedTabDocumentController] currentDocument]
 != [MY_WINDOW ownerDocument] || [MY_WINDOW isOpened] )
	{
		return;
	}
	//[TAB_CURSOR showTimerCursor];
	
	
	if( mouseOnTab == YES && NSEqualPoints(  balloonCursor  , [NSEvent mouseLocation] ) )
	{
		////NSLog(@"openTabWithDelay actually");
		/*
		 [self performKeyEquivalent:
			 
			 [NSEvent  keyEventWithType:NSKeyDown
							   location:NSZeroPoint
						  modifierFlags:0 
							  timestamp:0
						   windowNumber:0
								context:nil
							 characters:@"o"
			charactersIgnoringModifiers:@"o"
							  isARepeat:NO
								keyCode:0]
			 ];
		 */
		
		//[TAB_CURSOR closeTimerCursor];
		
		[MY_WINDOW openTab];
		
		[MY_WINDOW orderFront:self ];
		
		/// set close timer
		
		
		if(  preferenceBoolValueForKey(@"closeAutomatically")   )
			[MY_WINDOW closeTimer];
		
		
		
	}else if( mouseOnTab == YES )
	{
		balloonCursor = [NSEvent mouseLocation];
		[windowOpenTimer release];
		windowOpenTimer = [NSTimer scheduledTimerWithTimeInterval:openDelay target:self
														 selector:@selector(openTabWithDelay) userInfo:NULL repeats:NO];
		[windowOpenTimer retain];
		
	}else
	{
		//[TAB_CURSOR closeTimerCursor];
		
	}
	
}


-(void)openBalloon
{
	////NSLog(@"openBalloon");
	
	if( [[TabDocumentController sharedTabDocumentController] currentDocument] != [MY_WINDOW ownerDocument] )
	{
		return;
	}
	
	//[TAB_CURSOR showTimerCursor];
	
	//check mouse is REALLY on self
	
	
	if( NSPointInRect( [MY_WINDOW mouseLocationOutsideOfEventStream], [self frame] ) )
		mouseOnTab = YES;
	else
		mouseOnTab = NO;
	
	////NSLog(@"%d",mouseOnTab);
	
	NSColor* balloonColor;
	if(  preferenceBoolValueForKey(@"balloonColor")   == YES )
		balloonColor = THEME_COLOR;
	else
		balloonColor =  [NSColor whiteColor];
	
	if( mouseOnTab == YES && NSEqualPoints(  balloonCursor  , [NSEvent mouseLocation] ) )
	{
		NSImage* balloonImage = [MY_WINDOW balloonImage];
		
		////NSLog(@"openBalloon actually");
		
		[BALLOON_CONTROLLER showBalloonWithImage:balloonImage
										   color:balloonColor
											  at:balloonCursor
										  sender:self];
		
		if( [balloonOffTimer isValid] ) [balloonOffTimer invalidate];
		[balloonOffTimer release];
		balloonOffTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self
														 selector:@selector(closeBalloon) userInfo:NULL repeats:NO];
		[balloonOffTimer retain];
		
		//[TAB_CURSOR closeTimerCursor];
		
	}else if( mouseOnTab == YES )
	{
		balloonCursor = [NSEvent mouseLocation];
		
		if( [balloonOnTimer isValid] ) [balloonOnTimer invalidate];
		
		[balloonOnTimer release];
		balloonOnTimer = [NSTimer scheduledTimerWithTimeInterval:balloonDelay target:self
														selector:@selector(openBalloon) userInfo:NULL repeats:NO];
		[balloonOnTimer retain];
		
	}else
	{
		//[TAB_CURSOR closeTimerCursor];
		
	}
	
}

-(void)closeBalloon
{
	//[TAB_CURSOR closeTimerCursor];
	
	
	if( ! NSPointInRect( [MY_WINDOW mouseLocationOutsideOfEventStream]  , [self frame] ) )
	{
		
		[BALLOON_CONTROLLER hideBalloon];
		
		
		
	}else
	{
		
		[balloonOffTimer release];
		balloonOffTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
														 selector:@selector(closeBalloon) userInfo:NULL repeats:NO];	
		[balloonOffTimer retain];
	}
}

- (void)mouseExited:(NSEvent *)event
{
	//[TAB_CURSOR closeTimerCursor];
	
	
	/*
	 
	 // inputting title ?
	 
	 if( [MY_WINDOW isInputtingTitle] )
	 return;
	 */
	
	
	
	mouseOnTab = NO;
	
	if( ! [MY_WINDOW isOpened ] )
	{
		////NSLog(@"resign");
		
		
	}
	
	
	if(   ! [MY_WINDOW isTornOff]  )
	{
		
		[MY_WINDOW setNormalLevel];
	}
	
	
	[self display]; 
	
	
	//balloon
	
	if( [balloonOffTimer isValid] ) [balloonOffTimer invalidate];
	if( [balloonOnTimer isValid] ) [balloonOnTimer invalidate];
	
	//[self closeBalloon];
	[BALLOON_CONTROLLER hideBalloon];
	
	
	[[TabDocumentController sharedTabDocumentController] setBusy:YES];

}

-(void)setTabPosition:(TAB_POSITION)pos
{
	if( pos < 0 || pos > 3 ) pos = 1;
	
	tabPosition = pos;
	[self setup];
	
}



- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
	if( [sender draggingSource] == self ) return NSDragOperationNone;
	
	
	if(  [MY_WINDOW isTornOff] )
		return NSDragOperationNone;
	
	
	[MY_WINDOW orderFront:self];
	[MY_WINDOW setFloatingLevel];
	
	////NSLog(@"draggingEntered");
	[MY_WINDOW openTab];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeByNotification:) name:@"EdgiesTextViewDraggingExitedNotification"
											   object:[OWNER_DOCUMENT textView]];
	[NSTimer scheduledTimerWithTimeInterval:0.5 target:MY_WINDOW selector:@selector(closeTimer) userInfo:NULL repeats:NO];
	
	
	return NSDragOperationGeneric;
	
}
-(void)closeByNotification:(NSNotification*)notification
{
	//NSLog(@"closeByNotification");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[MY_WINDOW closeTab];
}
- (void)draggingExited:(id <NSDraggingInfo>)sender
{
	//[self closeByNotification:self];
	////NSLog(@"draggingExited");

}
-(void)customizeButtonClicked
{
	////NSLog(@"customizeButtonClicked");
	[MY_WINDOW showMenu];
	
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	if( [theEvent type] != NSRightMouseDown )
		return nil;
	
	
	[self mouseExited:NULL];		
	
	[MY_WINDOW showMenu];
	
	return nil;
}

-(void)dragAsPlainText:(NSEvent*)theEvent
{
		
	NSString* str = [[AttachmentCellConverter sharedAttachmentCellConverter] 
			convertAttributedStringToString:[[OWNER_DOCUMENT textView] textStorage]];
	
	if( str == nil )
		str = @"";

	

	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard];
	[pb declareTypes:[NSArray arrayWithObjects:NSStringPboardType, NSFilesPromisePboardType,nil]
			   owner:self];
	
	[pb setString:str forType:NSStringPboardType];
	
	
	[pb setPropertyList:[[[NSArray arrayWithObject:@"txt" ] description] propertyList]
				forType:NSFilesPromisePboardType];
	
	dragoutAsPlainText = YES;
	
	//NSImage* balloonImage = [MY_WINDOW balloonImage];
	//balloonImage = [self modifyBalloonImageForDraggingOut:balloonImage];
	
	//calc image loc
	
	
	
	NSRect contentRect;
	
	
	
	contentRect.origin = [[MY_WINDOW contentView] 
				convertPoint:[MY_WINDOW mouseLocationOutsideOfEventStream] toView:self ];
	
	NSBundle* myBundle;
	myBundle = [NSBundle bundleForClass:[self class]];
	NSString* path;
	
	path = [myBundle pathForResource:@"text" ofType:@"tif"];
	NSImage* textIcon = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	
	
	//[balloonImage setFlipped:NO];
	[self dragImage:textIcon
				 at:NSZeroPoint
			 offset:NSMakeSize(0,0)
			  event:theEvent
		 pasteboard:[NSPasteboard pasteboardWithName:NSDragPboard]
			 source:self
		  slideBack:YES];
	//[balloonImage setFlipped:YES];	
}

-(void)dragAsRichText:(NSEvent*)theEvent
{
	
	
	id tv = [OWNER_DOCUMENT textView];
	if( tv == nil ) return;
	
	
	
	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard];
	[pb declareTypes:[NSArray arrayWithObjects:NSRTFDPboardType, NSFilesPromisePboardType,nil]
			   owner:self];
	
	[[AttachmentCellConverter sharedAttachmentCellConverter] writeAttributedString:[tv textStorage] toPasteboard:pb];
	
	
	[pb setPropertyList:[[[NSArray arrayWithObject:@"rtfd" ] description] propertyList]
				forType:NSFilesPromisePboardType];
	
	dragoutAsPlainText = NO;
	
	NSImage* balloonImage = [MY_WINDOW balloonImage];
	
	balloonImage = [self modifyBalloonImageForDraggingOut:balloonImage];
	//calc image loc
	
	
	
	NSRect contentRect;
	
	
	
	contentRect.origin = [[MY_WINDOW contentView] convertPoint:NSMakePoint(25,25) toView:self ];
	
	
	[balloonImage setFlipped:NO];
	[self dragImage:balloonImage
				 at:contentRect.origin
			 offset:NSMakeSize(0,0)
			  event:theEvent
		 pasteboard:[NSPasteboard pasteboardWithName:NSDragPboard]
			 source:self
		  slideBack:YES];
	[balloonImage setFlipped:YES];
	
}
@end

