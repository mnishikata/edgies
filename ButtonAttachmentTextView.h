//
//  ButtonAttachmentTextView.h
//  sticktotheedge
//
//  Created by __Name__ on 07/05/21.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ButtonAttachmentCell.h"

#import "FaviconInspector.h"
#import "FaviconAttachmentCell.h"

#import "AliasInspector.h"
#import "AliasAttachmentCell.h"

#import "FileEntityTextAttachmentCell.h"
#import "CheckBoxTextView.h"

#import "ClipboardArchiveInspector.h"
#import "ClipboardArchiveAttachmentCell.h"

#import "AlarmInspector.h"
#import "AlarmAttachmentCell.h"

#import "TaggingInspector.h"
#import "TaggingAttachmentCell.h"

#import "MovieAttachmentCell.h"


@interface ButtonAttachmentTextView :  CheckboxTextView <ClipboardArchiveAttachmentCellOwnerTextView, GraphicAttachmentCellOwnerTextView, ButtonAttachmentCellOwnerTextView, AliasAttachmentCellOwnerTextView, FaviconAttachmentCellOwnerTextView, FileEntityTextAttachmentCellOwnerTextView, AlarmAttachmentCellOwnerTextView, TaggingAttachmentCellOwnerTextView, MovieAttachmentCellOwnerTextView >{

	id droppingTarget;
	
	unsigned droppingInsertionPoint;
}
-(IBAction)copyAsPlainText:(id)sender;
-(IBAction)pasteAsArchive:(id)sender;


- (BOOL)resignFirstResponder;
-(IBAction)deleteColor:(id)sender;
-(NDAlias*)mouseOnNDAlias:(NSRange*)rangePointer;
-(ButtonAttachmentCell*)mouseOnButtonAttachmentCell:(NSRange*)rangePointer;
- (void)clickedOnLink:(id)link atIndex:(unsigned)charIndex;
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender;
- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard types:(NSArray *)types;
- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard;
- (BOOL)dragSelectionWithEvent:(NSEvent *)event offset:(NSSize)mouseOffset slideBack:(BOOL)slideBack;
-(void)drawInsertionPoint;
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender;
- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender;
- (void)draggingExited:(id <NSDraggingInfo>)sender;
-(BOOL)dropText:(id)text draggingInfo:(id <NSDraggingInfo>)sender;
- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)revealInFinder:(id)sender;
-(void)buttonAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
-(void)buttonAttachmentCellClicked:(ButtonAttachmentCell*)cell;
-(void)buttonAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;

-(NSArray*)aliasAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)aliasAttachmentCellArrayInSelectedRange;
-(void)openAliasInspector;
- (BOOL)shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString;
-(NSArray*)faviconAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)faviconAttachmentCellArrayInSelectedRange;
-(void)openFaviconInspector;
-(void)rescueFileEntityTextAttachmentCells;
-(void)rescueFileEntityTextAttachmentCellsInRange:(NSRange)range;
-(NSArray*)clipboardArchiveAttachmentCellArrayInRange:(NSRange)range;
-(NSArray*)clipboardArchiveAttachmentCellArrayInSelectedRange;
-(void)openClipboardArchiveInspector;


@end
