#import "MemoManager.h"

#import "ColorCheckbox.h"

#import "MiniDocument.h"
#import "MNTabWindow.h"
#import "AppController.h"
#import "ZoomTextView.h"

#import "EdgiesLibrary.h"



static NSString*    MemoManagerSearchIdentifier = @"MemoManagerSearchIdentifier";
static NSString*    MemoManagerOpenOptionIdentifier = @"MemoManagerOpenOptionIdentifier";
static NSString*    MemoManagerAddNewIdentifier = @"MemoManagerAddNewIdentifier";
static NSString*    MemoManagerDiscardIdentifier = @"MemoManagerDiscardIdentifier";
static NSString*    MemoManagerExportIdentifier = @"MemoManagerExportIdentifier";
static NSString*    MemoManagerToggleIdentifier = @"MemoManagerToggleIdentifier";

static NSString*    MemoManagerFontIdentifier = @"MemoManagerFontIdentifier";
static NSString*    MemoManagerRulerIdentifier = @"MemoManagerRulerIdentifier";
static NSString*    MemoManagerColorIdentifier = @"MemoManagerColorIdentifier";



@implementation MemoManager

+(GraphicInspector*)sharedMemoManager
{
	return [[NSApp delegate] memoManager	];
}

- (id) init {
	self = [super init];
	if (self != nil) {

		
		//NSLog(@"a");
		
		[NSBundle loadNibNamed:@"MemoManager"  owner:self];
		
		[arrayController setContent:[[TabDocumentController sharedTabDocumentController] documents]];
		
		[self setupMetaList];
		[self setupTable];
		
		[[textView layoutManager]retain];
		[table setAutosaveName:@"MemoManagerTable"];
		
		[panel setFrameUsingName: @"MemoManagerMyDocumentWindow"];
		[panel setFrameAutosaveName: @"MemoManagerMyDocumentWindow"];
		
		
		
		
		
		fillPatternImage = [[NSImage alloc] initByReferencingFile:
			[[NSBundle bundleForClass:[self class]] pathForResource:@"fillPattern" ofType:@"tiff"]];
		
		[textView setEditable:NO];
		
		
		
#if MAC_OS_X_VERSION_MAX_ALLOWED < 1040
		
#else
		[self setupToolbar];
#endif
		
		//if( [AppController OSVersion] >= 104 )
		//	[self setupToolbar];
		
		
		
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(reloadData:)
													 name:TabWindowNumberDidChange
												   object:nil];
		
		/*
		 [[NSNotificationCenter defaultCenter] addObserver:self 
												  selector:@selector(queryNote:)
													  name:nil 
													object:NSMetadataQueryDidStartGatheringNotification];	
		 [[NSNotificationCenter defaultCenter] addObserver:self 
												  selector:@selector(queryNote:)
													  name:nil 
													object:NSMetadataQueryDidFinishGatheringNotification];	
		 */
		
		
		//[self setupTable];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endMemoList) 
													 name:NSApplicationWillTerminateNotification object:[NSApplication sharedApplication]];
		
		
	
	}
	return self;
}



-(IBAction)showMemoManager:(id)sender
{
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];	
	[self showPanel];	
}


-(void)setupTable
{

	// load keys
    
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	NSArray* tcArray = [ud objectForKey:@"defaultTableColumns"];
	if( tcArray == nil )
	{
		tcArray =  [NSMutableArray arrayWithObjects: @"hidden", @"p_color", @"p_title", @"creationDate", nil];
		
	}
	
	allKeys =  [NSMutableArray arrayWithArray: tcArray];
	[allKeys retain];
	
	
	
	
	//setup table
	int hoge;
	
	NSArray* array = [table tableColumns];
	for( hoge = 0; hoge < [array count]; hoge++ )
	{
		[table removeTableColumn:[array objectAtIndex:hoge]];
	}
	
	for( hoge = 0; hoge < [allKeys count]; hoge++ )
	{
		NSString* attr = [allKeys objectAtIndex:hoge];
		
		////NSLog(@"add %@",attr);
		
		[table addColumnWithIdentifier:attr
					 title:[self titleForColumnIdentifier:attr] bindToObject:arrayController ];
	}	
	
	
	//[table setDoubleAction:@selector(launch:)];
	
	
	
}



-(void)endMemoList
{
	////NSLog(@"defaultTableColumns");
	
	/// save default table column.  
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	
	NSMutableArray* array = [NSMutableArray array];
	
	NSArray* columns = [table tableColumns];
	
	
	int hoge;
	for( hoge = 0; hoge < [columns count]; hoge++)
	{
		[array addObject:[[columns objectAtIndex:hoge] identifier]];
		
		////NSLog(@"%@",[[columns objectAtIndex:hoge] identifier]);

	}
	[ud setObject:array forKey:@"defaultTableColumns"];
	
	
	////
	
	

	
	[table saveTableColumn];
	
	
	[ud synchronize];
	
	//[super close];
}

-(void)reloadData:(id)sender
{
	[arrayController rearrangeObjects];
//	[table deselectAll:self];
	
	
	[table reloadData];
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
 	[super dealloc];
}

-(void)showPanel
{
	[panel makeKeyAndOrderFront:self];

	[table reloadData];
}


-(void)hidePanel
{
	[panel orderOut:self];
}


- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	if( ! [panel isVisible] ) return;
		
		
	NSArray* selectedArray = [arrayController selectedObjects];
	
	if( [selectedArray count] == 0 ) return;
	
	MiniDocument* doc = [selectedArray objectAtIndex:0];
		
		[textView setSelectedRange:NSMakeRange(0,0)];
		//
		
		if( [[[textView textStorage] layoutManagers] count] > 1   )
			[[textView textStorage] removeLayoutManager:[textView layoutManager] ];

		
			NSTextStorage* ts = [ [doc valueForKey: @"textView" ] textStorage]; 

			if( ts != nil )
			{
				[[textView layoutManager] replaceTextStorage: ts];
			}
			
			[textView setEditable:YES];
			[[textView undoManager] removeAllActions];
			[textView setAllowsUndo:YES];


			/*
		}else
		{
			
			if( [textView layoutManager] == nil ) //NSLog(@"layoutmanager is empty");
			
			NSLayoutManager* lm = [[NSLayoutManager alloc] init];
			NSTextStorage* ts = [ [doc valueForKey: @"textView" ] textStorage]; 
			
			[lm replaceTextStorage: ts];
			[textView addLayoutManager:lm ];

		}*/
		
		
		[textView setSelectedRange:NSMakeRange(0,0)];

		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		if( [ud boolForKey:@"memoManagerOpenTab"] )
			[(MNTabWindow*) [doc window] openTab];
			

	
	
}

- (void)tableView:(NSTableView*)tableView willDisplayCell:(id)cell forTableColumn:(NSTableColumn*)tableColumn row:(int)row
{
	
	if([[tableColumn identifier] isEqualToString:@"p_color"])
	{
		
		/*
		float score = [[[data objectAtIndex:row] objectForKey:@"score"] floatValue];
		
		
		
		NSRect frame = [tableView frameOfCellAtColumn:[tableView columnWithIdentifier:@"score"] row:row ];
		
		//trim 
		frame.origin.x += 3;
		frame.origin.y += 1;
		frame.size.width -= 6;
		frame.size.height -= 2;
		
		
		frame.size.width = frame.size.width * score;
		
		[[NSColor colorWithPatternImage:fillPatternImage] set];
		[NSBezierPath fillRect: frame];
		
		*/
		
		NSRect frame = [tableView frameOfCellAtColumn:[tableView columnWithIdentifier:@"p_color"] row:row ];
		
		//trim 
		frame.origin.x += 3;
		frame.origin.y += frame.size.height -2;
		frame.size.width -= 6;
		frame.size.height -= 4;

		
		MiniDocument* doc = [[arrayController arrangedObjects] objectAtIndex:row];
		

		
		NSColor* color = [[[doc window] ownerDocument] documentColor ];
		NSColor* frameColor = [[NSColor darkGrayColor] colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
		NSImage* icon = [ColorCheckbox roundedBox:color
										  size:frame.size
										 curve:4 
									frameColor:frameColor];

		[icon compositeToPoint:frame.origin  operation:NSCompositeSourceOver];
		[cell setTitle:@""];

		
	}
	
	
}

-(void)showPopupMenuForRow:(int)row
{
		
	
	
	MiniDocument* doc = [[arrayController arrangedObjects] objectAtIndex:row];
	
	[[doc window] showMenu];
	
}

#pragma mark -
#pragma mark Meta

-(void)setupMetaList
{

	metaListArray = [[NSMutableArray alloc] init]; 
	[metaList setContent:metaListArray];
	
	
	// add optional
	NSMutableDictionary* dic;
	
	/*
	 dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:kMDQueryResultContentRelevance, @"attribute",NSLocalizedString(@"Score",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	 [metaListArray  addObject:dic];
	 */
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"hidden", @"attribute",NSLocalizedString(@"Enabled",@""),@"attributeDisplayName",[NSNumber numberWithBool:YES ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_color", @"attribute",NSLocalizedString(@"Color",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_title", @"attribute",NSLocalizedString(@"Title",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"creationDate", @"attribute",NSLocalizedString(@"Creation Date",@""),@"attributeDisplayName",[NSNumber numberWithBool:YES ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_subject", @"attribute",NSLocalizedString(@"Subject",@""),@"attributeDisplayName",[NSNumber numberWithBool:YES ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_author", @"attribute",NSLocalizedString(@"Author",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_company", @"attribute",NSLocalizedString(@"Company",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_copyright", @"attribute",NSLocalizedString(@"Copyright",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_keywords", @"attribute",NSLocalizedString(@"Keywords",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	
	//
	dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"p_comment", @"attribute",NSLocalizedString(@"Comment",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
	[metaListArray  addObject:dic];
	
	
	
	
	
	//
	[metaList setContent:metaListArray];
	[metaList rearrangeObjects];
	
	//if( allAttr != nil ) CFRelease(allAttr);
		
		
	
}

-(IBAction)showViewOption:(id)sender
{
	
	//metalist
	

	
	
	//check and uncheck
	
	int hoge;
	for( hoge = 0; hoge < [metaListArray count]; hoge++ )
	{
		NSMutableDictionary* dic = [metaListArray objectAtIndex:hoge];
		NSString* attr = [dic objectForKey:@"attribute"];
		
		if( [allKeys containsObject:attr] ) [dic setObject:[NSNumber numberWithBool:YES ] forKey:@"enable"];
		else	[dic setObject:[NSNumber numberWithBool:NO ] forKey:@"enable"];
		
	}
	
	
	//[metaList rearrangeObjects];
	
	[metaListWindow setSpacesBehavior:1/*NSWindowCollectionBehaviorCanJoinAllSpaces*/];

	[NSApp beginSheet:metaListWindow modalForWindow:panel
		modalDelegate:self didEndSelector:@selector(sheetDidEnd:returnCode:contextInfo:) contextInfo:nil];
	
}
-(void)metaListClosed:(id)sender
{
	[metaListWindow close];
	[NSApp endSheet:metaListWindow returnCode:[sender tag] ];
	
	
	int hoge;
	for( hoge = 0; hoge < [[metaList content] count]; hoge++ )
	{
		NSDictionary* dic = [[metaList content] objectAtIndex:hoge];
		NSString* attr = [dic objectForKey:@"attribute"];
		BOOL enable =  [[dic objectForKey:@"enable"] boolValue];
		
		if( enable == YES )
		{
			if( ! [allKeys containsObject:attr] )
			{
				
				
				
				[table addColumnWithIdentifier:attr title:[self titleForColumnIdentifier:attr] bindToObject:arrayController ];
				[allKeys addObject:attr];
			}
		}else //disable
		{
			if(  [allKeys containsObject:attr] )
			{
				[table removeColumnWithIdentifier:attr];
				[allKeys removeObject:attr];
			}
			
		}
	}
	
	
}

-(NSString*)titleForColumnIdentifier:(NSString*)identifier
{

	int hoge;
	
	for( hoge = 0 ; hoge < [metaListArray count]; hoge ++ )
	{
		id dic = [metaListArray objectAtIndex:hoge];	
		if( [[dic objectForKey:@"attribute"] isEqualToString: identifier] )
			return [dic objectForKey:@"attributeDisplayName"];
			
	}
	
	return identifier;
}

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)item;
{
	
	
}

- (IBAction)metaListFilter:(id)sender
{
	
	
	NSString* searchString = [sender stringValue];
	
	
	
	if( [searchString isEqualToString:@"" ] )
	{
		
		[metaList setFilterPredicate:nil];
		[metaList rearrangeObjects];
		
		return;
		
	}
	
	
	
	
	NSPredicate *predicateToRun = nil;
	
	predicateToRun = [NSPredicate predicateWithFormat: @"(attributeDisplayName contains[wc] %@)|| (attribute contains[wc] %@)", searchString,searchString];
	
	[metaList setFilterPredicate:nil];
	[metaList setFilterPredicate:predicateToRun];
	[metaList rearrangeObjects];
	return;
	
}



#pragma mark -
#pragma mark Search


#if MAC_OS_X_VERSION_MAX_ALLOWED < 1040

#else



- (IBAction)search:(id)sender
{
	//NSLog(@"search");
	
	
	NSString* searchString = [searchField stringValue];


	
	if( [searchString isEqualToString:@"" ] )
	{

		[arrayController setFilterPredicate:nil];
		[arrayController rearrangeObjects];

		return;
		
	}
	

	
	
	
	
	///folder search
	
	
	
	
	
	NSPredicate *predicateToRun = nil;
	
	NSString *predicateFormat =  [self searchFormatWithQuery: searchString] ;
	predicateToRun = [NSPredicate predicateWithFormat: @"(string contains[wc] %@) || (p_author contains[wc] %@) ||(p_copyright contains[wc] %@) ||(p_company contains[wc] %@)||(p_title contains[wc] %@)||(p_subject contains[wc] %@)||(p_comment contains[wc] %@)||(p_keywords contains[wc] %@)", searchString, searchString, searchString, searchString, searchString, searchString,searchString, searchString];
	
	[arrayController setFilterPredicate:nil];
	[arrayController setFilterPredicate:predicateToRun];
	[arrayController rearrangeObjects];
	return;
	
	
	_query = MDQueryCreate (
							NULL,
							predicateFormat,
							(CFArrayRef)[NSArray arrayWithObjects:kMDQueryResultContentRelevance,
								kMDItemPath, kMDItemDisplayName, kMDItemFSName,nil ],
							(CFArrayRef)[NSArray arrayWithObject:kMDQueryResultContentRelevance]
							);	
	
	
	
		
		
		
		MDQuerySetSearchScope (
							   _query,
							   
							   (CFArrayRef)[NSArray arrayWithObject: DOCUMENT_FOLDER ],
							   0
							   );
	


	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finish) name:(NSString*)kMDQueryDidFinishNotification object:_query];
	
	
	[indicator startAnimation:self];
	
	MDQueryExecute (
					_query,
					kMDQuerySynchronous
					);
	
	

	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSFindPboard];
	[pb declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
	[pb setString: searchString  forType:NSStringPboardType];
	
}



-(NSString*)searchFormatWithQuery:(NSString*)aQuery;
{
	NSString* format =  @" (string	== \"%@\"wc) || (p_author	== \"%@\"wc) ||(p_copyright		== \"%@\"wc) ||(p_title			== \"%@*\"wc) ";
	
	
	return [NSString stringWithFormat:format, aQuery, aQuery, aQuery, aQuery];
	
	
	
}


#pragma mark -
#pragma mark Toolbar
//--------------------------------------------------------------//
// NSToolbar delegate methods
//--------------------------------------------------------------//

-(void)setupToolbar
{
	// NSToolbarのインスタンスを作ります
	NSToolbar*  toolbar;
	toolbar = [[[NSToolbar alloc] initWithIdentifier:@"MemoManagerToolbar"] autorelease];
	
	
	// ツールバーを設定します
    [toolbar setAllowsUserCustomization: YES];
    [toolbar setAutosavesConfiguration: YES];
    [toolbar setDisplayMode: NSToolbarDisplayModeIconOnly];
	
	[toolbar setDelegate:self];
	[panel setToolbar:toolbar];
	//[panel setShowsToolbarButton:NO];	
}

- (NSArray*)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
	// 識別子の配列を返します
    return [NSArray arrayWithObjects:
		MemoManagerAddNewIdentifier,
		MemoManagerDiscardIdentifier,
		//MemoManagerExportIdentifier,
		MemoManagerToggleIdentifier,
		MemoManagerOpenOptionIdentifier,
		
		NSToolbarFlexibleSpaceItemIdentifier,
		
		MemoManagerSearchIdentifier, 
		NSToolbarCustomizeToolbarItemIdentifier,
		
		
		nil];
}

- (NSArray*)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
    return [NSArray arrayWithObjects:
		MemoManagerAddNewIdentifier,
		MemoManagerDiscardIdentifier,
		//MemoManagerExportIdentifier,
		MemoManagerToggleIdentifier,
		MemoManagerOpenOptionIdentifier,
		
		MemoManagerFontIdentifier,
		MemoManagerRulerIdentifier,
		MemoManagerColorIdentifier,
		
		MemoManagerSearchIdentifier, 
		NSToolbarCustomizeToolbarItemIdentifier,
		
		
		nil];
	
	
}



- (NSToolbarItem*)toolbar:(NSToolbar*)toolbar itemForItemIdentifier:(NSString*)itemId  willBeInsertedIntoToolbar:(BOOL)willBeInserted
{
	// NSToolbarItem を作ります
    NSToolbarItem*  toolbarItem;
    toolbarItem = [[[NSToolbarItem alloc] initWithItemIdentifier:itemId] autorelease];
	//	[toolbarItem setTarget:self];
	//	[toolbarItem setAction:@selector(_showTab:)];
    
	// NSToolbarItemを設定します
    if ([itemId isEqualToString:  MemoManagerSearchIdentifier ]) {
        [toolbarItem setLabel:NSLocalizedString(@"Find",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"Find",@"")];
		
		[toolbarItem setMinSize:NSMakeSize(150,24)];
        [toolbarItem setView:searchView];
        
        return toolbarItem;
    }
	if ([itemId isEqualToString:MemoManagerOpenOptionIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"View Options",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"View Options...",@"")];
		
        [toolbarItem setImage:[NSImage imageNamed:@"GeneralPreferences"]];
        [toolbarItem setTarget:self];
		[toolbarItem setAction:@selector(showViewOption:)];
        return toolbarItem;
    }
	if ([itemId isEqualToString:MemoManagerAddNewIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"New",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"New",@"")];
        [toolbarItem setImage:[NSImage imageNamed:@"add"]];
        [toolbarItem setTarget:self];
		[toolbarItem setAction:@selector(newDocumentInList)];
        return toolbarItem;
    }
	if ([itemId isEqualToString:MemoManagerDiscardIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"Discard",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"Discard",@"")];
		
        [toolbarItem setImage:[NSImage imageNamed:@"delete"]];
        [toolbarItem setTarget:table];
		[toolbarItem setAction:@selector(discard:)];
        return toolbarItem;
    }
	/*
	 if ([itemId isEqualToString:MemoManagerExportIdentifier]) {
		 [toolbarItem setLabel:@"Export"];
		 [toolbarItem setPaletteLabel:@"Export"];
		 
		 [toolbarItem setImage:[NSImage imageNamed:@"GeneralPreferences"]];
		 [toolbarItem setTarget:table];
		 [toolbarItem setAction:@selector(export:)];
		 return toolbarItem;
	 }
	 */
	if ([itemId isEqualToString:MemoManagerToggleIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"Open/Close",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"Open/Close",@"")];
		
        [toolbarItem setImage:[NSImage imageNamed:@"openclose"]];
        [toolbarItem setTarget:self];
		[toolbarItem setAction:@selector(toggle)];
        return toolbarItem;
    }
	
	if ([itemId isEqualToString:MemoManagerFontIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"Font",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"Font",@"")];
		
        [toolbarItem setImage:[NSImage imageNamed:@"font"]];
        [toolbarItem setTarget:nil];
		[toolbarItem setAction:@selector(orderFrontFontPanel:)];
        return toolbarItem;
		
    }	if ([itemId isEqualToString:MemoManagerRulerIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"Ruler",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"Show Ruler",@"")];
		
        [toolbarItem setImage:[NSImage imageNamed:@"ruler"]];
        [toolbarItem setTarget:nil];
		[toolbarItem setAction:@selector(toggleRuler:)];
        return toolbarItem;
		
    }	if ([itemId isEqualToString:MemoManagerColorIdentifier]) {
        [toolbarItem setLabel:NSLocalizedString(@"Color",@"")];
		[toolbarItem setPaletteLabel:NSLocalizedString(@"Color Panel",@"")];
		
        [toolbarItem setImage:[NSImage imageNamed:@"color"]];
        [toolbarItem setTarget:nil];
		[toolbarItem setAction:@selector(orderFrontColorPanel:)];
        return toolbarItem;
    }
	
	
    return nil;
}
- (BOOL) validateToolbarItem: (NSToolbarItem *) toolbarItem
{
	NSString* itemId = [toolbarItem itemIdentifier];
	
	
	
	if ([itemId isEqualToString:MemoManagerDiscardIdentifier]||
		[itemId isEqualToString:MemoManagerExportIdentifier]||
		[itemId isEqualToString:MemoManagerToggleIdentifier]) 
	{
        if( [table selectedRow] == -1 ) return NO;
    }
	
	return YES;
}

-(MiniDocument*)selectedDocument
{


	 NSArray* array = [arrayController selectedObjects];
	 if( [array count] > 0 )
	 {
		 return	[array objectAtIndex:0];
		 
	 }
	 
	 return nil;
	
}

-(void)toggle
{
	[[[self selectedDocument] window] toggleTab];
	
}
-(void)newDocumentInList
{
	[[TabDocumentController sharedTabDocumentController] newDocument:self];
}


#endif


#pragma mark DEBUG

-(IBAction)debug:(id)sender
{

	[NSThread detachNewThreadSelector:@selector(_debug_move)
							 toTarget:self withObject:nil];
	
}


-(void)_debug_move
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	int count = 500;
	int hoge;
	for(hoge= 0; hoge < count; hoge++ )
	{
		//NSLog(@"%d:%d",hoge,count);
		
		[[TabDocumentController sharedTabDocumentController] performSelectorOnMainThread:@selector(arrangeToFront:) withObject:nil waitUntilDone:NO];
		
		[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
		
		[[TabDocumentController sharedTabDocumentController] performSelectorOnMainThread:@selector(arrangeToBack:) withObject:nil waitUntilDone:NO];

		[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
		
		
	}
	[pool release];
}


@end
