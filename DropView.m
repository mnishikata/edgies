//
//  DropView.m
//  sticktotheedge
//
//  Created by __Name__ on 06/05/07.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "MNTabWindow.h"
#import "DropView.h"
#import <HIToolbox/CarbonEventsCore.h>
#import "KeyModifierManager.h"
#import "TabCursor.h"
#import "ScreenUtil.h"
#import "ModifierKeyWell.h"

//#import "FileEntityTextView.h"
#import "DropWindowManager.h"
#import "EdgiesLibrary.h"
#import "TabDocumentController.h"

#define PBTypes [NSArray arrayWithObjects: NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType",NSFilenamesPboardType, NSTIFFPboardType,  @"DropAnythingPboardType",  nil]




#define TITLE_ATTRIBUTE [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12],NSFontAttributeName , NULL ]


#define WINDOW_ALPHA  0.07
#define WINDOW_WHITE 0.0


@implementation DropView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

    
		[self registerForDraggedTypes:PBTypes ];
		
		/*
		// setting cursor images
		NSBundle* myBundle;
		myBundle = [NSBundle bundleForClass:[self class]];
		NSString* path;
		
		path = [myBundle pathForResource:@"cursorLeft" ofType:@"tiff"];
		NSImage* _cursorLeft = [[NSImage alloc] initWithContentsOfFile:path ];
		
		path = [myBundle pathForResource:@"cursorRight" ofType:@"tiff"];
		NSImage* _cursorRight = [[NSImage alloc] initWithContentsOfFile:path ];
		
		path = [myBundle pathForResource:@"cursorBottom" ofType:@"tiff"];
		NSImage* _cursorBottom = [[NSImage alloc] initWithContentsOfFile:path ];

	
		cursorLeft = [[NSCursor alloc] initWithImage:_cursorLeft
											 hotSpot:NSMakePoint(0,50)];
		
		cursorRight = [[NSCursor alloc] initWithImage:_cursorRight
											 hotSpot:NSMakePoint(19,50)];
		
		cursorBottom = [[NSCursor alloc] initWithImage:_cursorBottom
											 hotSpot:NSMakePoint(50, -19)];
		*/
		
		trackingRect  = NSNotFound;	
		
		dragging = NO;
		onEdge = NO;
		createNewMemo = YES;
	}
    return self;
}


-(void)resetTrackingRect
{
	
	if( trackingRect != NSNotFound )
		[self removeTrackingRect: trackingRect];
	
	trackingRect = [self addTrackingRect:[self bounds] owner:self userData:NULL assumeInside:NO];

}
- (void)drawRect:(NSRect)aRect
{
	//NSLog(@"drawRect dropview");
	[self resetTrackingRect];
	/*
	if( trackingRect == NSNotFound )
		trackingRect = [self addTrackingRect:[self bounds] owner:self userData:NULL assumeInside:NO];
	*/
	[[NSColor clearColor] set];
	[NSBezierPath fillRect:aRect];

	
	[[NSColor colorWithCalibratedWhite:WINDOW_WHITE alpha:WINDOW_ALPHA] set];
	[NSBezierPath fillRect:aRect];
}


/*
//-(void)addTrackingRect
{
	NSRect rect = [self bounds];
	
	if( style == tab_right )
		rect.size.width = 1;
	
	else if( style == tab_left )
		rect.origin.x ++;
	
	else if( style == tab_top )
		rect.size.height = 1;
	
	trackingRect = [self addTrackingRect:rect owner:self userData:NULL assumeInside:YES];
}*/

-(void)setScreenID:(int)aScreenNum
{
	screenID = aScreenNum;
}
-(int)screenID
{
	return screenID;
}
-(void)setStyle:(int)st
{
	style = st;
	
	[self setTag: style];
}
-(int)style
{
	return style;
}

-(void)setCreateNewMemo:(BOOL)flag
{
	createNewMemo = flag;
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
	//NSLog(@"draggingEntered");
	
	
	if( preferenceBoolValueForKey(@"edgeActivatingKeySwitch") && !preferenceBoolValueForKey(@"disableModifierToBringTabs"))
	{
		if( ![MOD_PRESSED:@"edgeActivatingKeyModifier"] )
		{
			return NSDragOperationNone;
		}
	}
	
	[[DropWindowManager sharedDropWindowManager] bringToFront];

	
	
/*
	if( preferenceBoolValueForKey(@"edgeActivatingKeySwitch") )
	{
		NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
		while(1)
		{
			if( [NSDate timeIntervalSinceReferenceDate] > time +10 )
			{
				[[self window] setIgnoresMouseEvents:NO];
				return;	
			}
			
			if( ! mouseIsOnTheEdges() )
			{
				[[self window] setIgnoresMouseEvents:NO];
				return;
			}
			
			
			if( [MOD_PRESSED:@"edgeActivatingKeyModifier"] )
			{
				break;
			}
			
			usleep( 100000 );
		}
		
	}
	*/
	if( ! mouseIsOnTheEdges() ) return NSDragOperationNone;
	
	/*
	//NSLog(@"draggingEntered");
	if(  draggingEnteredSecondFlag == NO )
	{
		[self startSession];
		
		return NSDragOperationNone;
	}
	
	draggingEnteredSecondFlag = NO;
	 */
	
	
	
	/*
//check where
		[[NSCursor currentCursor] push];
	
if( style == tab_right )
	[ cursorLeft set];

else if( style == tab_left )
	[ cursorRight set];

else if( style == tab_top )
	[ cursorBottom set];
	*/
	
	
	////NSLog(@"drag enter");
	
	[[TabDocumentController sharedTabDocumentController] setBusy:YES];

	
	/*
	if(  preferenceIntValueForKey(@"bringToFront")   != 3  )
	{
		[self bringToFront];
	}
	*/
	
	
	// when do not create memo
	if( createNewMemo == NO )
	{
		[[self window] setIgnoresMouseEvents:YES];

		[self performSelector:@selector(createNewMemoIsNoButUnhideMe)
				   withObject:nil afterDelay:0.05];
		return NSDragOperationNone;
		
	}
	
	
	
	
	if( style == tab_right )
		[ TAB_CURSOR  showLeftCursor ];
	
	else if( style == tab_left )
		[ TAB_CURSOR showRightCursor];
	
	else if( style == tab_top )
		[ TAB_CURSOR showBottomCursor];
	
	else if( style == tab_bottom )
		[ TAB_CURSOR showTopCursor];

	
	dragging = YES;

	return NSDragOperationGeneric;
}

- (void)draggingExited:(id <NSDraggingInfo>)sender
{
	draggingEnteredSecondFlag = NO;

	
	////NSLog(@"drag exit");
	
	if( style == tab_right )
		[ TAB_CURSOR closeLeftCursor ];
	
	else if( style == tab_left )
		[ TAB_CURSOR closeRightCursor ];
	
	else if( style == tab_top )
		[ TAB_CURSOR closeBottomCursor];
	
	else if( style == tab_bottom )
		[ TAB_CURSOR closeTopCursor];
	
	
	
	////NSLog(@"exit");
	dragging = NO;
	
	//[NSCursor pop];

}
- (void)concludeDragOperation:(id <NSDraggingInfo>)sender
{
	[self resetTrackingRect];

	[[DropWindowManager sharedDropWindowManager] tile];
	dragging = NO;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{

	if( createNewMemo == NO )	return NO;

	
	MiniDocument* doc = [[TabDocumentController sharedTabDocumentController] newDocumentForDropping];
	MNTabWindow* docWindow = [doc window];

	NSPasteboard* pb = [sender draggingPasteboard];
	
	NSTextView* textView = [doc textView];
	[textView readSelectionFromPasteboard: pb ];
	
	
	
	
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect frame = [docWindow frame];
	
	
	[docWindow setTabPosition:style];
	
	[docWindow setFrame:NSMakeRect(mouseLoc.x -frame.size.width/2,  mouseLoc.y -frame.size.height/2,
								   frame.size.width, frame.size.height)
				display:YES animate:NO];
	
	[docWindow openTab];
	
	[docWindow makeKeyAndOrderFront:self];
	
	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];

	
	
	
	return YES;
	
	
	
}

-(void)bringToFront
{
	NSArray* documentsToMove;

	if( preferenceBoolValueForKey(@"edgeEffectiveIndependently") )
	{
		int edgeID = screenIDWhereRectIs( [[self window] frame] )*0x10 + style;
		
		documentsToMove = 
			[[TabDocumentController sharedTabDocumentController] documentsAtEdge:edgeID];
	}else
	{
		documentsToMove = 
			[[TabDocumentController sharedTabDocumentController] visibleDocuments];
	}
	
	[[TabDocumentController sharedTabDocumentController] arrangeToFrontDocuments:documentsToMove ];

	
}

-(void)moveToBackmost
{
	NSArray* documentsToMove;
	
	if( preferenceBoolValueForKey(@"edgeEffectiveIndependently") )
	{
		int edgeID = screenIDWhereRectIs( [[self window] frame] )*0x10 + style;
		
		documentsToMove = 
			[[TabDocumentController sharedTabDocumentController] documentsAtEdge:edgeID];
	}else
	{
		documentsToMove = 
		[[TabDocumentController sharedTabDocumentController] visibleDocuments];
	}
	
	
	
	[[TabDocumentController sharedTabDocumentController] arrangeToBackDocuments:documentsToMove];
	
}

-(void)edgeActivatingLoop
{
	
	NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
	
	if( [NSDate timeIntervalSinceReferenceDate] > time +10 )
	{
		[[self window] orderFront:nil];
		return;	
	}
	
	if( ! mouseIsOnTheEdges() )
	{
		[[self window] orderFront:nil];
		return;
	}
	
	
	if( [MOD_PRESSED:@"edgeActivatingKeyModifier"] )
	{
		[self mouseEnteredMain];
		return;
	}
	
	
	
	[self performSelector:@selector(edgeActivatingLoop) withObject:nil afterDelay:0.1];
}


- (void)mouseEntered:(NSEvent *)theEvent
{
	if( dragging || GetCurrentButtonState() != 0) return;
	
	//NSLog(@"mouseEntered");

	[[self window] orderOut:self];

	
	
	if( preferenceBoolValueForKey(@"edgeActivatingKeySwitch")  )
	{
		BOOL keyIsPressed = [MOD_PRESSED:@"edgeActivatingKeyModifier"];
		BOOL bringsTabAnyway = preferenceBoolValueForKey(@"disableModifierToBringTabs");

		
		if( !keyIsPressed )
		{
			
			if( bringsTabAnyway )
			{
				[[DropWindowManager sharedDropWindowManager] bringToFront];
			}
			
			[self performSelector:@selector(edgeActivatingLoop) withObject:nil afterDelay:0.1]; 
			return;
		}
	}
	[self 	mouseEnteredMain];
}

-(void)mouseEnteredMain{	
	
	if( ! mouseIsOnTheEdges() )
	{
		[[self window] orderFront:nil];

		return;
	}
	//NSLog(@"enter");
	onEdge = YES;
	
	[[TabDocumentController sharedTabDocumentController] setBusy:YES];
	
	
	
	
	// do not create new memo if the flag is NO
	if( createNewMemo == NO )
		[[self window] setIgnoresMouseEvents:YES];

	

	

	// do not create new memo if the flag is NO
	if( createNewMemo == NO )
	{
		
		[[self window] setIgnoresMouseEvents:YES];
		[NSTimer scheduledTimerWithTimeInterval:0.05 target:self
									   selector:@selector(createNewMemoIsNoButUnhideMe) userInfo:NULL repeats:NO];
		
		
		return;
	
	}
	
	

	


	
	
	
	currentCursor = [NSEvent mouseLocation];
	[NSTimer scheduledTimerWithTimeInterval:   preferenceFloatValueForKey(@"sliderClickDelay")   / 1000.0
							  target:self selector:@selector(showNewMemoCursor) userInfo:NULL repeats:NO];

	
	


}//

-(void)	createNewMemoIsNoButUnhideMe
{
	if( mouseIsOnTheEdges() )
	{
		[NSTimer scheduledTimerWithTimeInterval:0.1 target:self
			   selector:@selector(createNewMemoIsNoButUnhideMe) userInfo:NULL repeats:NO];
	}else
	{
		[[self window] setIgnoresMouseEvents:NO];

	}
	
}





-(void)showNewMemoCursor 
{
	//NSLog(@"showNewMemoCursor");
	
	[[self window] orderFront:self];

	
	if( createNewMemo == YES )
		[[self window] setIgnoresMouseEvents:NO];
	
//	NSWindow* currentWindow = [[[TabDocumentController sharedTabDocumentController] currentDocument] window];
	
	if( dragging == NO && onEdge == YES  && NSEqualPoints( currentCursor , [NSEvent mouseLocation]) /* && ![currentWindow mouseOnTab] */ )
	{
		//[[NSCursor currentCursor] push];
		
		if( createNewMemo == YES )
		{
			if( style == tab_right )
				[ TAB_CURSOR showLeftCursor ];
			
			else if( style == tab_left )
				[ TAB_CURSOR showRightCursor];
			
			else if( style == tab_top )
				[ TAB_CURSOR showBottomCursor];
			
			else if( style == tab_bottom )
				[ TAB_CURSOR showTopCursor];
		}
		
		


/*		
		NSString* str = @"Create New Memo";
		NSSize size = [str sizeWithAttributes:TITLE_ATTRIBUTE];
		
		NSImage* image = [[NSImage alloc] initWithSize:size ];
		[image lockFocus];
		[str drawAtPoint:NSZeroPoint withAttributes:TITLE_ATTRIBUTE];
		[image unlockFocus];
	
		
		[BALLOON_CONTROLLER showBalloonWithImage:image color:[NSColor whiteColor]
											  at:[NSEvent mouseLocation]];
		*/
		
		
	}else if(  dragging == NO && onEdge == YES )
	{
		currentCursor = [NSEvent mouseLocation];
		[NSTimer scheduledTimerWithTimeInterval:0.1 target:self
														selector:@selector(showNewMemoCursor) userInfo:NULL repeats:NO];
	}
	
}



- (void)mouseExited:(NSEvent *)event
{
	onEdge = NO;

	if( dragging == NO )
	{
	if( style == tab_right )
		[ TAB_CURSOR closeLeftCursor];
	
	else if( style == tab_left )
		[ TAB_CURSOR closeRightCursor ];
	
	else if( style == tab_top )
		[ TAB_CURSOR closeBottomCursor];
	
	
	else if( style == tab_bottom )
		[ TAB_CURSOR closeTopCursor];

	}
	
	/*
	 
	[BALLOON_CONTROLLER hideBalloon];
	
	
	if( dragging == NO )
	{
		[NSCursor pop];
	}
	*/
}
- (void)mouseDown:(NSEvent *)theEvent
{
	
	
	id doc = [[TabDocumentController sharedTabDocumentController] newDocumentForDropping];
	
	MNTabWindow* docWindow = (MNTabWindow*)[doc window];
	
	NSPoint mouseLoc = [NSEvent mouseLocation];
	NSRect frame = [docWindow frame];
	[docWindow setFrame:NSMakeRect(mouseLoc.x -frame.size.width/2,  mouseLoc.y -frame.size.height/2,
									  frame.size.width, frame.size.height)
				   display:NO];

	[docWindow setTabPosition:style];
	[docWindow setScreenID:[[NSScreen screens] indexOfObject:screenWhereMouseIs() ]];

	[[NSApplication sharedApplication] activateIgnoringOtherApps:YES];

	[docWindow makeFirstResponder:[doc textView]];

	[docWindow selectNextKeyView:self];

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	BOOL animateBuffer = [ud boolForKey:@"animateOpenClose"];
	[ud setBool:NO forKey:@"animateOpenClose"];
	
	[docWindow openTab];
	[ud setBool:animateBuffer forKey:@"animateOpenClose"];
	
	[docWindow makeKeyAndOrderFront:self];

	[[DropWindowManager sharedDropWindowManager] tile];

}

@end
