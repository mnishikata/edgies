//
//  GrayGlassView.m
//  sticktotheedge
//
//  Created by __Name__ on 06/07/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GrayGlassView.h"

#import "ColorCheckbox.h"

@implementation GrayGlassView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {

	NSColor* color = [NSColor colorWithDeviceRed:0.5 green:0.5 blue:0.5 alpha:0.9];
	NSSize size = rect.size;
	size. height += 10.0;
	NSImage* image  = [ColorCheckbox roundedBox:color
										   size:size
										  curve:10.0
									 frameColor:color];
	[image drawAtPoint:NSZeroPoint
			  fromRect:rect 
			 operation:NSCompositeSourceOver
			  fraction:1.0];
	
	[super drawRect:rect];
}

@end
