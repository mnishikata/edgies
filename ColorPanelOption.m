#import "ColorPanelOption.h"

/*
 Color Panel problem in Leopard.
 
 Once a first responder (Text View) responds changeColor, NSColorPanel keeps n sending changeColor to the text view.
 
 Probably, text view changes color panel automatically when changing selection
 
 */
@implementation ColorPanelOption

- (void)awakeFromNib
{

	
	//ColorPanelOption* cpoption = [[[ColorPanelOption alloc] init] autorelease];
	
	NSColorPanel* cp = [NSColorPanel sharedColorPanel];
	[cp setAccessoryView:view];
	//[cp setDelegate:self];
	//[cp makeKeyAndOrderFront:self];
	
	
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}





@end

/*
@interface NSColorPanel (LessPersistentOnLeopard)


@end

@implementation NSColorPanel (LessPersistentOnLeopard)
-(void)setColor:(NSColor*)color
{
	NSLog(@"LessPersistentOnLeopard %@",[color description]);
}
@end
*/