//
//  FaviconAttachmentCell.m
//  sticktotheedge
//
//  Created by __Name__ on 07/05/23.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "FaviconAttachmentCell.h"
#import "FaviconInspector.h"
#import "FileLibrary.h"
#import "NSString (extension).h"

extern BOOL UD_DELETE_CELLS_AFTER_DRAGGING_OUT;

@implementation FaviconAttachmentCell


+(NSAttributedString*)faviconAttachmentCellAttributedStringWithPath:(NSString*)path andTitle:(NSString*)title hasTitle:(BOOL)hasTitle loadFavicon:(BOOL)loadFavicon
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	[wrapper setPreferredFilename:@"file"];
	//add files
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	FaviconAttachmentCell* aCell = [[[FaviconAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	
	[aCell setTargetPath: path];
	[aCell setTitle:title];
		
//	[aCell setValue:[NSNumber numberWithBool:hasTitle] forKey:@"showButtonTitle"];
	
	if( loadFavicon ) [aCell loadFavicon];
		
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	


#pragma mark -
- (id)initWithAttachment:(NSTextAttachment*)anAttachment
{
    self = [super initWithAttachment:(NSTextAttachment*)anAttachment];
    if (self) {
		
		aWebView = nil;
		
		[self setTarget:self];
		[self setAction:@selector(open)];
		[self setTitle:@""];
		
		
		[self setImage:[NSImage imageNamed:@"defaultFavicon"]];

		[self loadDefaults];


	}
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	
	
    return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {


		aWebView = nil;
		
		[self setTarget:self];
		[self setAction:@selector(open)];
	}
	return self;
	
	
}
-(void)dealloc
{
	[aWebView setFrameLoadDelegate:nil];
	[aWebView release];
	[super dealloc];
}

#pragma mark Favicon

-(void)loadFavicon
{
	[[WebPreferences standardPreferences] setPlugInsEnabled:NO];
	[[WebPreferences standardPreferences] setLoadsImagesAutomatically:NO];
	[[WebPreferences standardPreferences] setJavaEnabled:NO];
	
	aWebView = [[WebView alloc] initWithFrame:NSMakeRect(0,0,600,300)
									frameName:nil
									groupName:nil] ;	
	
	NSString* targetPath = [self targetPath];
	if( targetPath == nil || [targetPath isEqualToString:@""] ) return;
	
	NSURLRequest* urlrequest = [NSURLRequest requestWithURL:[NSURL URLWithString:targetPath]
												cachePolicy:NSURLRequestReloadIgnoringCacheData
											timeoutInterval:15.0 ];
	
	iconLoaded = NO;
	titleLoaded = NO;
	
	[aWebView setFrameLoadDelegate:self];
	[[aWebView mainFrame] loadRequest:urlrequest];
	
}


- (void)webView:(WebView *)sender didReceiveIcon:(NSImage *)image forFrame:(WebFrame *)frame
{
	[image retain];
	
	////NSLog(@"didReceiveIcon");
	
//	NSURL* originalUrl = [[[frame dataSource] initialRequest] URL];
	iconLoaded = YES;
	
	if( iconLoaded && titleLoaded )
		[sender stopLoading:self];
	
	if( image )
	{
		[self setImage:image];
		[[self image] setSize:[self cellSize]];
		[image release];

	}
	

}	

- (void)webView:(WebView *)sender didReceiveTitle:(NSString *)title forFrame:(WebFrame *)frame
{
	if( [aWebView mainFrame] != frame ) return;
	titleLoaded = YES;
	
	if( iconLoaded && titleLoaded )
		[sender stopLoading:self];
	
	[self setTitle:title];
}


#pragma mark -

-(void)setTargetPath:(NSString*)path
{
	[self setRepresentedObject:path];
}

-(NSString*)targetPath
{
	
	return [self representedObject];
}

-(NSAttributedString*)targetLink
{
	NSString* targetPath = [self targetPath];
	if( targetPath == nil ) return nil;
	
	NSURL* url = [NSURL URLWithString:targetPath];
	NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[url path], NSLinkAttributeName,nil];
	NSAttributedString* attr = [[NSAttributedString alloc] initWithString:targetPath attributes:dictionary ];
	
	return [attr autorelease];
}


-(void)open
{
	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	NSString* targetPath = [self targetPath];
	
	if( targetPath == nil )
	{
		NSBeep();
		return;
	}
	
	NSURL* url = [NSURL URLWithString:targetPath];
	[ws openURL:url ];
}


-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"FaviconAttachmentCell_buttonSize"];
	[ud setInteger:buttonPosition forKey:@"FaviconAttachmentCell_buttonPosition"];
	[ud setBool:showButtonTitle forKey:@"FaviconAttachmentCell_showButtonTitle"];
	[ud setBool:showBezel forKey:@"FaviconAttachmentCell_showBezel"];
	[ud setObject:[NSArchiver archivedDataWithRootObject: [self font]] forKey:@"FaviconAttachmentCell_font"];
	
	[ud synchronize];
}


-(void)loadDefaults
{
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"FaviconAttachmentCell_buttonSize"];
	buttonSize = (value != nil ? [value floatValue] : 16); 
	
	value = [ud valueForKey: @"FaviconAttachmentCell_buttonPosition"];
	buttonPosition = (value != nil ? [value intValue] : 0); 
	
	value = [ud valueForKey: @"FaviconAttachmentCell_showButtonTitle"];
	showButtonTitle = (value != nil ? [value boolValue] : NO); 
	
	value = [ud valueForKey: @"FaviconAttachmentCell_showBezel"];
	showBezel = (value != nil ? [value boolValue] : NO); 
	
	value = [ud valueForKey: @"FaviconAttachmentCell_font"];
	if( value != nil ){
		NSFont* font = [NSUnarchiver unarchiveObjectWithData: value];
		if( font != nil && [font isKindOfClass:[NSFont class]] )
			[self setFont:font];
	}
	
	lock = NO;
}






-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <FaviconAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;

	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"FaviconAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
	//add separator
	customMenuItem = [NSMenuItem separatorItem];
	[customMenuItem setRepresentedObject:@"FaviconAttachmentCellItem"];
	[aContextMenu addItem:customMenuItem ];
	
	
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Favicon Inspector",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"FaviconAttachmentCellItem"];
	[customMenuItem setTarget: aTextView];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(openFaviconInspector)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
			

			

		

	
	///
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Open URL",@"")
												action:nil keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"FaviconAttachmentCellItem"];
	[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
	[customMenuItem setAction: @selector(open)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
		
}


-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	[textView  openFaviconInspector];
}




#pragma mark -
#pragma mark @protocol AttachmentCellConverting 

-(int)stringRepresentation:(NSDictionary*)context
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	NSString* string = [NSString stringWithFormat:@"%@ <%@>",[self title], [self targetPath]];
		
			
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = [string length] -1;
	
	return changeInLength;
}




-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	
	NSString* string = [self title];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withString:string];
	
	int changeInLength = [string length] -1;
	
	
	//add link
	
	
	NSURL* url = [NSURL fileURLWithPath: [self targetPath]];
	NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[url path], NSLinkAttributeName,nil];
	[mattr addAttributes:dictionary range:NSMakeRange(index,[string length])];
	
	
	return changeInLength;
	

}
-(NSRect)buttonFrame:(NSRect)cellFrame
{
	return cellFrame;
}
-(int)attributedStringRepresentation:(NSDictionary*)context //withimage 
{
	
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];
	
	//add icon
	[self setHighlighted:NO];
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[self image]];
	
	
	//NSAttributedString* attr = [NSAttributedString attributedStringWithAttachment: [self attachment]];
		
	NSURL* url = [NSURL URLWithString: [self targetPath]];
	NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[url path], NSLinkAttributeName,nil];

	
	
	NSString* string = [self title];
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:string attributes:dictionary] autorelease];
	
	[mattr insertAttributedString:attr atIndex:index+1];
	
	
	int changeInLength = [string length] ;
	

	
	return changeInLength;
	
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	
	NSArray* array = [NSArray arrayWithObjects:[self targetPath],[self title],nil ];
	NSArray* array2 = [NSArray arrayWithObjects:[NSArray arrayWithObject:[self targetPath]],[NSArray arrayWithObject:[self title]],nil ];
	NSDictionary* val = [NSDictionary dictionaryWithObjectsAndKeys:
		array2, @"WebURLsWithTitlesPboardType",
		array, @"Apple URL pasteboard type", 
		[self targetPath], NSStringPboardType,	
		@"FaviconAttachmentCell",  NSFilesPromisePboardType,	nil];
		
	
	
	return val;
}

-(NSString*)writeToFileAt:(NSURL*)url    
{
	NSString* filename = [[self title] safeFilename];
	BOOL succes  =
		 dropBookmark( [self targetPath] , [url path] , filename);
		
	if(  succes &&  UD_DELETE_CELLS_AFTER_DRAGGING_OUT )
		[self removeFromTextView];

	
	return ( succes ? filename : nil );
}

-(BOOL)canAcceptDrop
{
	return NO;
}



@end
